/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "gtk_pairs.h"
#include "pairsModeling/externalGtkPairsExtensions.h"
#include "extraGtkFunctions/gtk_colorComboBoxWidget.h"
#include "extraGtkFunctions/gtk_curveWidget.h"
#include "extraGtkFunctions/gtk_elementComboBox.h"
#include "gtk_renderingWindowWidget.h"

#include "interface.h"
#include "support.h"
#include <string.h>
#include <stdlib.h>

#include "visu_tools.h"
#include "visu_object.h"
#include "visu_data.h"
#include "visu_pairs.h"
#include "extensions/pairs.h"

/**
 * SECTION: gtk_pairs
 * @short_description: The pairs dialog.
 *
 * <para>The pair dialog provides a list of pairs as min/max
 * distances between species to draw pairs. A set of two species can
 * have several pairs drawn.</para>
 * <para>This dialog also hosts widgets that depend on the pair method
 * that is used for a given link.</para>
 * <para>Finally, it has also a second tab where a graph of g(r) can
 * be drawn.</para>
 */

/**
 * VisuUiPairsIter:
 * @ele1: the #VisuElement on one side ;
 * @ele2: the #VisuElement on the other side ;
 * @data: the #VisuPairLink information on the link between @ele1 and
 * @ele2 ;
 * @iter: the #GtkTreeIter corresponding to this link ;
 * @selected: a private attribute ;
 * @current: an other private attribute.
 *
 * An iterator on pairs.
 */

static GtkWidget *vboxPairsDialog;
static gulong unit_signal;

/* Current pair list is out of date. */
static gboolean pairsNeedBuild;


struct specBuildFunc_struct
{
  GtkWidget *wd;
  VisuUiPairsBuildWidgetsFunc area;
  VisuUiPairsToLabelFunc label;
  VisuUiPairsSetValuesFunc selected;
};

static GtkWidget *gtkPairs_treeView;
static GtkWidget *widgetColor;
static GtkWidget *parameterWidget, *alignWidget;
static GtkWidget *comboFilter, *btAdd, *btRemove, *btHide;
static gulong widgetColorSignalId, methodSignalId;
static GtkWidget *curve;
static GtkWidget *spinDMin, *spinDMax, *spinDZoom;
static GtkWidget *rangeHlStart, *rangeHlEnd, *checkHighlight,
  *labelIntegralDistance, *labelMeanDistance;

static gulong gtkPairs_spinMinSignalId, gtkPairs_spinMaxSignalId;
static GtkWidget *gtkPairs_spinMin, *gtkPairs_spinMax;

static gboolean disableCallbacks = FALSE;

/* This enum is used to access the column
   of the liststore have the data of pairs. */
enum
  {
    GTK_PAIRS_COLUMN_DRAWN,
/*     GTK_PAIRS_COLUMN_LABEL_DISTANCE, */
    GTK_PAIRS_COLUMN_DISTANCE_FROM,
    GTK_PAIRS_COLUMN_DISTANCE_TO,
    GTK_PAIRS_COLUMN_PIXBUF_COLOR,
    GTK_PAIRS_COLUMN_LABEL_USER,
    GTK_PAIRS_COLUMN_PRINT_LENGTH,
    GTK_PAIRS_COLUMN_POINTER_TO_LINK,
    GTK_PAIRS_COLUMN_POINTER_TO_ELE1,
    GTK_PAIRS_COLUMN_POINTER_TO_ELE2,
    N_GTK_PAIRS_COLUMN
  };
static GtkTreeStore *pairsTreeStore;
/* The list store that controls the drawing of the pairs data. */
static GtkTreeModel *pairsFilter, *pairsSortable;
static GtkTreeSelection *pairsSelection;

/* Combobox model. */
static GtkListStore *gtkPairs_comboModel;
enum
  {
    GTK_PAIRS_COLUMN_PAIRS_ICON, /* A pixmap to represent the pairs */
    GTK_PAIRS_COLUMN_PAIRS_NAME, /* The label shown */
    GTK_PAIRS_COLUMN_PAIRS_POINTER,  /* Pointer to the pairs extension. */
    GTK_PAIRS_COLUMN_PAIRS_SPEC_BUILD,  /* Pointer to the pairs specBuildFunc_struct. */
    N_GTK_PAIRS_METHODS_PAIRS
  };

/* Local routines. */
static void minMaxChanged(GtkSpinButton *spin, GtkTreeSelection *tree, int minMax);
static void addPairAllData(VisuElement *ele1, VisuElement *ele2);
static gint sortPairs(GtkTreeModel *model, GtkTreeIter *a,
		      GtkTreeIter *b, gpointer user_data);
static gboolean curveReplot(gpointer data);
static void curveSetData(VisuData *dataObj);
static gboolean setHlRange(gboolean set);
static void selectByPairMethod(GtkComboBox *combo, VisuPairExtension *meth);
static struct specBuildFunc_struct* getSpecBuildFuncs(VisuPairLink *data);

/* Callbacks. */
static void onGtkPairsDrawnToggled(GtkCellRendererToggle *cell_renderer,
				   gchar *path, gpointer user_data);
static void onDistanceValueEdited(GtkCellRendererText *cellrenderertext,
				  gchar *path, gchar *text, gpointer user_data);
static void onGtkPairsLengthToggled(GtkCellRendererToggle *cell_renderer,
				    gchar *path, gpointer user_data);
static void selectionChanged(GtkTreeSelection *tree, gpointer data);
static void minChanged(GtkSpinButton *spin, gpointer userData);
static void maxChanged(GtkSpinButton *spin, gpointer userData);
static void pairsMethodChanged(GtkComboBox *combobox, gpointer userData);
static void filterChanged(VisuUiElementCombobox *combobox, GList *elements,
                          gpointer userData);
static void colorChanged(VisuUiColorCombobox *colorWd, ToolColor *color, gpointer userData);
static void onDataReady(GObject *obj, VisuData *dataObj,
                        VisuGlView *view, gpointer data);
static void onDataNotReady(GObject *obj, VisuData *dataObj,
                           VisuGlView *view, gpointer data);
static void rebuildPairsOnResources(GObject *obj, VisuData *dataObj, gpointer bool);
static gboolean pairsRowIsVisible(GtkTreeModel *model, GtkTreeIter *iter,
				  gpointer data);
static void onBtAdd(GtkButton *button, gpointer user_data);
static void onBtRemove(GtkButton *button, gpointer user_data);
static void onBtHide(GtkToggleButton *button, gpointer user_data);
static void onDistanceAutoSet(GtkButton *button, gpointer data);
static void onDistSpanChanged(GtkSpinButton *spin, gpointer userData);
static void onHlRangeToggled(GtkToggleButton *button, gpointer user_data);
static void onRangeChanged(GtkRange *rg, gpointer data);
static gboolean onElementChanged(GSignalInvocationHint *ihint, guint n_param_values,
                                 const GValue *param_values, gpointer data);
static void onUnitChanged(VisuData *dataObj, gfloat factor, gpointer data);

#define GTK_PAIRS_HELP_TEXT _("Modifications, as color or wire width, are applied only to selected rows. Use <control> to select more than one row at a time.")
#define NO_DIST_INTEGRAL _("<i>No distance analysis</i>")
#define DIST_INTEGRAL _("\342\200\242 there are %.3f neighbours per '%s'")

#define NO_DIST_MEAN ""
#define DIST_MEAN _("\342\200\242 mean distance is %.3f")

/* Create the label for the distance, need to free result. */
gchar* createDistanceLabel(VisuPairLink *data);

/* Connect the signal and initialise values. */
void visu_ui_pairs_init()
{
  GList *pnt;
  int i;
  struct specBuildFunc_struct *specBuild;
  GtkTreeIter iter;

  /* Create the listModel for pairs method*/
  gtkPairs_comboModel = gtk_list_store_new(N_GTK_PAIRS_METHODS_PAIRS,
					   GDK_TYPE_PIXBUF,
					   G_TYPE_STRING,
					   G_TYPE_POINTER,
					   G_TYPE_POINTER);
  gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(gtkPairs_comboModel),
				       GTK_PAIRS_COLUMN_PAIRS_NAME,
				       GTK_SORT_ASCENDING);
  for (i = 0, pnt = visu_pair_extension_getAllMethods(); pnt;
       i++, pnt = g_list_next(pnt))
    if (listVisuUiPairsInitFunc[i] && listGtkPairsBuildFunc[i] &&
        listGtkPairsLabelFunc[i] && listGtkPairsSignalFunc[i])
      {
        listVisuUiPairsInitFunc[i]();
        specBuild = g_malloc(sizeof(struct specBuildFunc_struct));
        specBuild->wd       = (GtkWidget*)0;
        specBuild->area     = listGtkPairsBuildFunc[i];
        specBuild->label    = listGtkPairsLabelFunc[i];
        specBuild->selected = listGtkPairsSignalFunc[i];
        gtk_list_store_append(gtkPairs_comboModel, &iter);
        DBG_fprintf(stderr, " | '%s' method...\n",
                    visu_pair_extension_getName((VisuPairExtension*)pnt->data, TRUE));
        gtk_list_store_set(gtkPairs_comboModel, &iter,
                           GTK_PAIRS_COLUMN_PAIRS_ICON, (GdkPixbuf*)0,
                           GTK_PAIRS_COLUMN_PAIRS_NAME,
                           visu_pair_extension_getName((VisuPairExtension*)pnt->data, TRUE),
                           GTK_PAIRS_COLUMN_PAIRS_POINTER, pnt->data,
                           GTK_PAIRS_COLUMN_PAIRS_SPEC_BUILD, (gpointer)specBuild,
                           -1);
        DBG_fprintf(stderr, " | method OK.\n");
      }
  pairsSelection = (GtkTreeSelection*)0;
}

static void formatDistances(GtkTreeViewColumn *column _U_, GtkCellRenderer *cell,
			    GtkTreeModel *model, GtkTreeIter *iter, gpointer data)
{
  gchar dist[8];
  float val;
  
  gtk_tree_model_get(model, iter, GPOINTER_TO_INT(data), &val, -1);
  sprintf(dist, "%6.3f", val);
  g_object_set(G_OBJECT(cell), "text", dist, NULL);
}
static void formatElements(GtkTreeViewColumn *column _U_, GtkCellRenderer *cell,
			   GtkTreeModel *model, GtkTreeIter *iter, gpointer data _U_)
{
  gchar lbl[128];
  VisuElement *ele1, *ele2;
  GtkTreeIter parent;
  
  if (gtk_tree_model_iter_parent(model, &parent, iter))
    lbl[0] = '\0';
  else
    {
      gtk_tree_model_get(model, iter,
			 GTK_PAIRS_COLUMN_POINTER_TO_ELE1, &ele1,
			 GTK_PAIRS_COLUMN_POINTER_TO_ELE2, &ele2, -1);
      g_return_if_fail(ele1 && ele2);
      sprintf(lbl, "%s - %s", ele1->name, ele2->name);
    }
  g_object_set(G_OBJECT(cell), "text", lbl, NULL);
}
/**
 * visu_ui_pairs_show:
 * @main: the container for pairs dialog.
 *
 * Shows the pairs dialog and run possible update of the interface.
 *
 * Since: 3.6
 */
void visu_ui_pairs_show(VisuUiMain *main _U_)
{
  VisuData *dataObj;

  dataObj = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());
  g_return_if_fail(dataObj);

  if (!visu_ui_curve_frame_hasData(VISU_UI_CURVE_FRAME(curve)))    
    g_idle_add(curveReplot, (gpointer)dataObj);
}
void visu_ui_pairs_initBuild(VisuUiMain *main)
{
  GtkWidget *wd, *image, *vbox, *hbox, *vbox2;
  GtkWidget *viewport, *align, *combo;
  GtkWidget *label;

  VisuData *dataObj;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
  GtkTreeSelection *sel;
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
  GList *lst;
#endif
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;
  tooltips = gtk_tooltips_new ();
#endif

  dataObj = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());

  /* Create the listModel for pairs data*/
  pairsTreeStore = gtk_tree_store_new(N_GTK_PAIRS_COLUMN, G_TYPE_BOOLEAN,
				      G_TYPE_FLOAT, G_TYPE_FLOAT,
				      GDK_TYPE_PIXBUF, G_TYPE_STRING,
				      G_TYPE_BOOLEAN, G_TYPE_POINTER,
				      G_TYPE_POINTER, G_TYPE_POINTER);

  main->pairsDialog = create_pairsDialog();
  gtk_window_set_type_hint(GTK_WINDOW(main->pairsDialog),
			   GDK_WINDOW_TYPE_HINT_UTILITY);
  
  gtk_widget_set_name(main->pairsDialog, "message");

  wd = lookup_widget(main->pairsDialog, "labelTitlePairs");
  gtk_widget_set_name(wd, "message_title");

  wd = lookup_widget(main->pairsDialog, "scrolledwindowPairs");
  gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(wd),
				      GTK_SHADOW_ETCHED_IN);

  viewport = lookup_widget(main->pairsDialog, "viewport1");
  gtk_widget_set_name(viewport, "message_viewport");

  gtk_widget_set_name(lookup_widget(main->pairsDialog, "notebookPairs"),
		      "message_notebook");

  wd = gtk_bin_get_child(GTK_BIN(viewport));
  if (wd)
    gtk_widget_destroy(wd);

  /* Create the TreeView to represent the pairs data. */
  gtkPairs_treeView = gtk_tree_view_new();
/*   gtk_tooltips_set_tip(tooltips, gtkPairs_treeView, GTK_PAIRS_HELP_TEXT, NULL); */
  g_object_set(G_OBJECT(gtkPairs_treeView), "rules-hint", TRUE, NULL);
  pairsSelection = gtk_tree_view_get_selection(GTK_TREE_VIEW(gtkPairs_treeView));
  gtk_widget_show(gtkPairs_treeView);
  gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(gtkPairs_treeView), TRUE);
  gtk_container_add(GTK_CONTAINER(viewport), gtkPairs_treeView);

  vboxPairsDialog = lookup_widget(main->pairsDialog, "vboxPairsDialog");

  /* Create a filter for the treeview. */
  pairsFilter = gtk_tree_model_filter_new(GTK_TREE_MODEL(pairsTreeStore), NULL);
  pairsSortable = gtk_tree_model_sort_new_with_model(GTK_TREE_MODEL(pairsFilter));
  gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(pairsSortable),
				       GTK_PAIRS_COLUMN_POINTER_TO_ELE1,
				       GTK_SORT_ASCENDING);
  gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(pairsSortable),
				  GTK_PAIRS_COLUMN_POINTER_TO_ELE1, sortPairs,
				  (gpointer)pairsSortable, NULL);

  /* Set parameters for the tree */
  gtk_tree_selection_set_mode(gtk_tree_view_get_selection(GTK_TREE_VIEW(gtkPairs_treeView)),
			      GTK_SELECTION_MULTIPLE);
  gtk_tree_view_set_model(GTK_TREE_VIEW(gtkPairs_treeView), pairsSortable);

  /* Create a filter selector for the shown pairs. */
  wd = lookup_widget(main->pairsDialog, "hbox73");
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(wd), vbox, FALSE, FALSE, 0);

#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  label = gtk_label_new(_("Manage links: "));
  gtk_label_set_angle(GTK_LABEL(label), 90.);
  gtk_box_pack_end(GTK_BOX(vbox), label, FALSE, FALSE, 0);
#endif

  btRemove = gtk_button_new();
  gtk_widget_set_sensitive(btRemove, FALSE);
  image = gtk_image_new_from_stock(GTK_STOCK_REMOVE, GTK_ICON_SIZE_MENU);
  gtk_container_add(GTK_CONTAINER(btRemove), image);
  gtk_box_pack_end(GTK_BOX(vbox), btRemove, FALSE, FALSE, 3);  
  g_signal_connect(G_OBJECT(btRemove), "clicked",
		   G_CALLBACK(onBtRemove), (gpointer)0);
  gtk_tooltips_set_tip(tooltips, btRemove,
		       _("Remove a link definition."), NULL);

  btAdd = gtk_button_new();
  gtk_widget_set_sensitive(btAdd, FALSE);
  image = gtk_image_new_from_stock(GTK_STOCK_ADD, GTK_ICON_SIZE_MENU);
  gtk_container_add(GTK_CONTAINER(btAdd), image);
  gtk_box_pack_end(GTK_BOX(vbox), btAdd, FALSE, FALSE, 3);  
  g_signal_connect(G_OBJECT(btAdd), "clicked",
		   G_CALLBACK(onBtAdd), (gpointer)0);
  gtk_tooltips_set_tip(tooltips, btAdd,
		       _("Add a new link definition using (0., 0.) if"
			 " it does not already exist."), NULL);

  btHide = gtk_toggle_button_new();
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(btHide), FALSE);
  image = gtk_image_new_from_stock(GTK_STOCK_FIND, GTK_ICON_SIZE_MENU);
  gtk_container_add(GTK_CONTAINER(btHide), image);
  gtk_box_pack_end(GTK_BOX(vbox), btHide, FALSE, FALSE, 8);
  g_signal_connect(G_OBJECT(btHide), "toggled",
		   G_CALLBACK(onBtHide), (gpointer)pairsFilter);
  gtk_tooltips_set_tip(tooltips, btHide,
		       _("Show/hide the undrawn pairs."), NULL);

  gtk_widget_show_all(vbox);

  /* Create the part for the link parameters. */
  label = gtk_label_new(_("<b>Link parameters:</b>\t <span size=\"small\">"
			  "(apply to one or more selected rows)</span>"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(vboxPairsDialog), label, FALSE, FALSE, 5);

  gtk_widget_show_all(label);

  /* Create widgets that apply to all checked pairs. */
  alignWidget = gtk_alignment_new(0.5, 0.5, 1.0, 1.0);
  gtk_widget_set_sensitive(alignWidget, FALSE);
  gtk_alignment_set_padding(GTK_ALIGNMENT(alignWidget), 0, 0, 10, 10);
  gtk_box_pack_start(GTK_BOX(vboxPairsDialog), alignWidget, FALSE, FALSE, 0);

  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(alignWidget), vbox);

  hbox = gtk_hbox_new(FALSE, 3);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  label = gtk_label_new(_("From: "));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtkPairs_spinMin = gtk_spin_button_new_with_range(0, 100, 0.05);
  gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(gtkPairs_spinMin), TRUE);
  gtk_entry_set_width_chars(GTK_ENTRY(gtkPairs_spinMin), 4);
  gtk_box_pack_start(GTK_BOX(hbox), gtkPairs_spinMin, FALSE, FALSE, 0);

  label = gtk_label_new(_(" to: "));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtkPairs_spinMax = gtk_spin_button_new_with_range(0, 100, 0.05);
  gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(gtkPairs_spinMax), TRUE);
  gtk_entry_set_width_chars(GTK_ENTRY(gtkPairs_spinMax), 4);
  gtk_box_pack_start(GTK_BOX(hbox), gtkPairs_spinMax, FALSE, FALSE, 0);

  wd = gtk_button_new_with_mnemonic(_("_Auto set"));
  gtk_box_pack_start(GTK_BOX(hbox), wd, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(wd), "clicked",
		   G_CALLBACK(onDistanceAutoSet), (gpointer)0);
  gtk_tooltips_set_tip(tooltips, wd,
		       _("Set the distance criterion to select the first"
			 " pick in the distance distribution between all"
			 " nodes of the selected types."), NULL);

/*   label = gtk_label_new (_("Color: ")); */
/*   gtk_box_pack_start (GTK_BOX (hboxCheckWidgets), label, TRUE, TRUE, 0); */
/*   gtk_misc_set_alignment (GTK_MISC (label), 1, 0.5); */

  widgetColor = visu_ui_color_combobox_new(FALSE);
  gtk_box_pack_end(GTK_BOX(hbox), widgetColor, FALSE, FALSE, 0);

  /* The pair method line. */
  hbox = gtk_hbox_new(FALSE, 3);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  align = gtk_alignment_new(0.5, 0., 1.0, 0.);
  gtk_box_pack_start(GTK_BOX(hbox), align, FALSE, FALSE, 0);
  combo = gtk_combo_box_new();
  /* renderer = gtk_cell_renderer_pixbuf_new(); */
  /* gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(combo), renderer, FALSE); */
  /* gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(combo), */
  /*                               renderer, "pixbuf", */
  /*       			GTK_PAIRS_COLUMN_PAIRS_ICON); */
  renderer = gtk_cell_renderer_text_new();
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(combo), renderer, FALSE);
  gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(combo),
                                renderer, "text",
				GTK_PAIRS_COLUMN_PAIRS_NAME);
  gtk_combo_box_set_model(GTK_COMBO_BOX(combo),
			  GTK_TREE_MODEL(gtkPairs_comboModel));
  gtk_container_add(GTK_CONTAINER(align), combo);

  parameterWidget = gtk_vbox_new(FALSE, 0);
  gtk_box_pack_end(GTK_BOX(hbox), parameterWidget, TRUE, TRUE, 0);

  methodSignalId = g_signal_connect(G_OBJECT(combo), "changed",
                                    G_CALLBACK(pairsMethodChanged), (gpointer)0);
  selectByPairMethod(GTK_COMBO_BOX(combo),
                     visu_pair_extension_getDefault());

  gtk_widget_show_all(alignWidget);

  /* Render the associated tree */
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_title(column, _("Pair"));
  gtk_tree_view_column_set_sort_indicator(column, TRUE);
  gtk_tree_view_column_set_sort_order(column, GTK_SORT_ASCENDING);
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func
    (column, renderer, formatElements,
     GINT_TO_POINTER(GTK_PAIRS_COLUMN_POINTER_TO_ELE1), (GDestroyNotify)0);
  gtk_tree_view_column_set_sort_column_id(column, GTK_PAIRS_COLUMN_POINTER_TO_ELE1);
  gtk_tree_view_append_column(GTK_TREE_VIEW(gtkPairs_treeView), column);

  renderer = gtk_cell_renderer_toggle_new ();
  g_signal_connect(G_OBJECT(renderer), "toggled",
		   G_CALLBACK(onGtkPairsDrawnToggled), (gpointer)0);
  column = gtk_tree_view_column_new_with_attributes ("", renderer,
						     "active", GTK_PAIRS_COLUMN_DRAWN,
						     NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(gtkPairs_treeView), column);

  renderer = gtk_cell_renderer_text_new();
  g_object_set(G_OBJECT(renderer), "editable", TRUE, "foreground", "blue", NULL);
  g_signal_connect(G_OBJECT(renderer), "edited", G_CALLBACK(onDistanceValueEdited),
		   GINT_TO_POINTER(GTK_PAIRS_COLUMN_DISTANCE_FROM));
  column = gtk_tree_view_column_new_with_attributes(_("From"), renderer, "text",
						    GTK_PAIRS_COLUMN_DISTANCE_FROM,
						    NULL);
  gtk_tree_view_column_set_cell_data_func
    (column, renderer, formatDistances,
     GINT_TO_POINTER(GTK_PAIRS_COLUMN_DISTANCE_FROM), (GDestroyNotify)0);
  gtk_tree_view_append_column(GTK_TREE_VIEW(gtkPairs_treeView), column);
  renderer = gtk_cell_renderer_text_new();
  g_object_set(G_OBJECT(renderer), "editable", TRUE, "foreground", "blue", NULL);
  g_signal_connect(G_OBJECT(renderer), "edited", G_CALLBACK(onDistanceValueEdited),
		   GINT_TO_POINTER(GTK_PAIRS_COLUMN_DISTANCE_TO));
  column = gtk_tree_view_column_new_with_attributes(_("To"), renderer, "text",
						    GTK_PAIRS_COLUMN_DISTANCE_TO,
						    NULL);
  gtk_tree_view_column_set_cell_data_func
    (column, renderer, formatDistances,
     GINT_TO_POINTER(GTK_PAIRS_COLUMN_DISTANCE_TO), (GDestroyNotify)0);
  gtk_tree_view_append_column(GTK_TREE_VIEW(gtkPairs_treeView), column);

  renderer = gtk_cell_renderer_toggle_new ();
  g_signal_connect(G_OBJECT(renderer), "toggled",
		   G_CALLBACK(onGtkPairsLengthToggled), (gpointer)0);
  column = gtk_tree_view_column_new_with_attributes (_("Lg."), renderer, "active",
						     GTK_PAIRS_COLUMN_PRINT_LENGTH,
						     NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(gtkPairs_treeView), column);

  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_title(column, _("Parameters"));
  renderer = gtk_cell_renderer_pixbuf_new();
  gtk_tree_view_column_pack_start(column, renderer, FALSE);
  gtk_tree_view_column_set_attributes(column, renderer,
				      "pixbuf", GTK_PAIRS_COLUMN_PIXBUF_COLOR, NULL);
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start(column, renderer, TRUE);
  gtk_tree_view_column_set_attributes(column, renderer,
				      "text", GTK_PAIRS_COLUMN_LABEL_USER, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(gtkPairs_treeView), column);

  wd = lookup_widget(main->pairsDialog, "hboxPairsModel");
  label = gtk_label_new("]");
  gtk_box_pack_end(GTK_BOX(wd), label, FALSE, FALSE, 0);

  align = gtk_alignment_new(0.5, 0.5, 1., 0.);
  gtk_box_pack_end(GTK_BOX(wd), align, FALSE, FALSE, 0);

  comboFilter = visu_ui_element_combobox_new(FALSE, TRUE, (const gchar*)0);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
  lst = gtk_cell_layout_get_cells(GTK_CELL_LAYOUT(comboFilter));
  if (lst)
    {
      g_object_set(G_OBJECT(lst->data), "scale", 0.75, NULL);
      g_list_free(lst);
    }
#endif
/*   gtk_box_pack_end(GTK_BOX(wd), comboFilter, FALSE, FALSE, 0); */
  gtk_container_add(GTK_CONTAINER(align), comboFilter);
  g_signal_connect(G_OBJECT(comboFilter), "element-selected",
		   G_CALLBACK(filterChanged), (gpointer)pairsFilter);
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(pairsFilter),
					 pairsRowIsVisible, (gpointer)comboFilter,
					 (GDestroyNotify)0);

  label = gtk_label_new(_("filter: "));
  gtk_box_pack_end(GTK_BOX(wd), label, FALSE, FALSE, 0);

  align = gtk_alignment_new(0.5, 0.5, 1., 1.);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 0, 0, 10, 0);
  gtk_box_pack_end(GTK_BOX(wd), align, FALSE, FALSE, 0);

  label = gtk_label_new("[");
  gtk_container_add(GTK_CONTAINER(align), label);

  gtk_widget_show_all(wd);

  /* Connect local signals. */
  sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(gtkPairs_treeView));
  g_signal_connect(G_OBJECT(sel), "changed",
		   G_CALLBACK(selectionChanged), (gpointer)combo);
  widgetColorSignalId = g_signal_connect(G_OBJECT(widgetColor), "color-selected",
					 G_CALLBACK(colorChanged), (gpointer)0);
  gtkPairs_spinMinSignalId =
    g_signal_connect(G_OBJECT(gtkPairs_spinMin), "value_changed",
		     G_CALLBACK(minChanged), (gpointer)sel);
  gtkPairs_spinMaxSignalId =
    g_signal_connect(G_OBJECT(gtkPairs_spinMax), "value_changed",
		     G_CALLBACK(maxChanged), (gpointer)sel);

  /* The "Distances" page. */
  vbox = lookup_widget(main->pairsDialog, "vboxDistances");

  curve = visu_ui_curve_frame_new(0., 5.);
  gtk_widget_set_name(curve, "message_notebook");
  visu_ui_curve_frame_setStyle(VISU_UI_CURVE_FRAME(curve), CURVE_LINEAR);
  gtk_box_pack_start(GTK_BOX(vbox), curve, TRUE, TRUE, 0);
  g_signal_add_emission_hook(g_signal_lookup("ElementMaterialChanged", VISU_TYPE_ELEMENT),
                             0, onElementChanged, (gpointer)curve, (GDestroyNotify)0);

  label = gtk_label_new(_("<b>Parameters</b>:"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
  hbox = gtk_hbox_new(FALSE, 3);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("min:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  spinDMin = gtk_spin_button_new_with_range(0., 50., 0.1);
  gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spinDMin), 2);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinDMin), 0.);
  g_signal_connect(G_OBJECT(spinDMin), "value_changed",
		   G_CALLBACK(onDistSpanChanged), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hbox), spinDMin, FALSE, FALSE, 0);
  label = gtk_label_new(_("max:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  spinDMax = gtk_spin_button_new_with_range(0., 50., 0.1);
  gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spinDMax), 2);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinDMax), 5.);
  g_signal_connect(G_OBJECT(spinDMax), "value_changed",
		   G_CALLBACK(onDistSpanChanged), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hbox), spinDMax, FALSE, FALSE, 0);
  label = gtk_label_new(_("step size:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  spinDZoom = gtk_spin_button_new_with_range(1., 20., 1.);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinDZoom), 5.);
  g_signal_connect(G_OBJECT(spinDZoom), "value_changed",
		   G_CALLBACK(onDistSpanChanged), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hbox), spinDZoom, FALSE, FALSE, 0);

  label = gtk_label_new(_("<b>Measurement tools</b>:"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
  hbox = gtk_hbox_new(FALSE, 3);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("Range"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);
  vbox2 = gtk_vbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), vbox2, TRUE, TRUE, 0);
  rangeHlStart = gtk_hscale_new_with_range(0., 5., .05);
  gtk_range_set_value(GTK_RANGE(rangeHlStart), 2.);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
  gtk_range_set_restrict_to_fill_level(GTK_RANGE(rangeHlStart), TRUE);
  gtk_range_set_fill_level(GTK_RANGE(rangeHlStart), 3.5);
  gtk_range_set_show_fill_level(GTK_RANGE(rangeHlStart), TRUE);
#endif
  gtk_scale_set_draw_value(GTK_SCALE(rangeHlStart), FALSE);
  gtk_box_pack_start(GTK_BOX(vbox2), rangeHlStart, FALSE, FALSE, 0);
  rangeHlEnd = gtk_hscale_new_with_range(0., 5., .05);
  gtk_range_set_value(GTK_RANGE(rangeHlEnd), 3.5);
  gtk_scale_set_draw_value(GTK_SCALE(rangeHlEnd), FALSE);
  g_signal_connect(G_OBJECT(rangeHlStart), "value-changed",
		   G_CALLBACK(onRangeChanged), (gpointer)rangeHlEnd);
  g_signal_connect(G_OBJECT(rangeHlEnd), "value-changed",
		   G_CALLBACK(onRangeChanged), (gpointer)rangeHlStart);
  gtk_box_pack_start(GTK_BOX(vbox2), rangeHlEnd, FALSE, FALSE, 0);
  vbox2 = gtk_vbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), vbox2, FALSE, FALSE, 0);
  checkHighlight = gtk_check_button_new_with_mnemonic(_("_Highlight nodes in range"));
  g_signal_connect(G_OBJECT(checkHighlight), "toggled",
		   G_CALLBACK(onHlRangeToggled), (gpointer)curve);
  gtk_box_pack_start(GTK_BOX(vbox2), checkHighlight, FALSE, FALSE, 0);
  labelIntegralDistance = gtk_label_new(NO_DIST_INTEGRAL);
  gtk_label_set_use_markup(GTK_LABEL(labelIntegralDistance), TRUE);
  gtk_misc_set_alignment(GTK_MISC(labelIntegralDistance), 0., 0.5);
  gtk_misc_set_padding(GTK_MISC(labelIntegralDistance), 10, 0);
  gtk_box_pack_start(GTK_BOX(vbox), labelIntegralDistance, FALSE, FALSE, 0);
  labelMeanDistance = gtk_label_new(NO_DIST_MEAN);
  gtk_label_set_use_markup(GTK_LABEL(labelMeanDistance), TRUE);
  gtk_misc_set_alignment(GTK_MISC(labelMeanDistance), 0., 0.5);
  gtk_misc_set_padding(GTK_MISC(labelMeanDistance), 10, 0);
  gtk_box_pack_start(GTK_BOX(vbox), labelMeanDistance, FALSE, FALSE, 0);

  gtk_widget_show_all(vbox);
  
  /* Set the flag that indicates that interior is not built yet. */
  pairsNeedBuild = TRUE;
  setHlRange(TRUE);

  g_signal_connect(VISU_OBJECT_INSTANCE, "dataRendered",
		   G_CALLBACK(onDataReady), (gpointer)main);
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataUnRendered",
		   G_CALLBACK(onDataNotReady), (gpointer)main);
  g_signal_connect(VISU_OBJECT_INSTANCE, "resourcesLoaded",
		   G_CALLBACK(rebuildPairsOnResources), (gpointer)main);

  if (dataObj)
    onDataReady((GObject*)0, dataObj, (VisuGlView*)0, (gpointer)main);

  DBG_fprintf(stderr, "Gtk Pairs: Pairs are first built.\n");
}

static void addPairAllData(VisuElement *ele1, VisuElement *ele2)
{
  gboolean first;
  GList *tmpLst;
  GtkTreeIter iter, iterParent;
  gboolean drawn;
  ToolColor *color;
  GdkPixbuf *pixColor;
  gchar *label;
  VisuPairLink *data;
  struct specBuildFunc_struct *spec;

  first = TRUE;
  for (tmpLst = visu_pair_link_getAll(ele1, ele2);
       tmpLst; tmpLst = g_list_next(tmpLst))
    {
      data = (VisuPairLink*)tmpLst->data;
      /* Add the VisuPairLink to the liststore. */
      drawn = visu_pair_link_getDrawn(data);
      /* 	  lblDist = createDistanceLabel(data); */
      color = visu_pair_link_getColor(data);
      pixColor = visu_ui_color_combobox_getPixbufFromColor(VISU_UI_COLOR_COMBOBOX(widgetColor),
						  color);
      spec = getSpecBuildFuncs(data);
      if (spec && spec->label)
	label = spec->label(data);
      else
	label = (gchar*)0;

      if (first)
	{
	  DBG_fprintf(stderr, "Gtk Pairs: add all pairs (%d) for %s - %s.\n",
		      g_list_length(tmpLst), ele1->name, ele2->name);
	  DBG_fprintf(stderr, " | pairs %s - %s (%g/%g).\n",
		      ele1->name, ele2->name,
                      visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MIN),
                      visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MAX));

#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 9
	  gtk_tree_store_insert_with_values
	    (pairsTreeStore, &iterParent, (GtkTreeIter*)0, 0,
	     GTK_PAIRS_COLUMN_POINTER_TO_ELE1, ele1,
	     GTK_PAIRS_COLUMN_POINTER_TO_ELE2, ele2,
	     GTK_PAIRS_COLUMN_DRAWN, drawn,
	     GTK_PAIRS_COLUMN_DISTANCE_FROM,
	     visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MIN),
	     GTK_PAIRS_COLUMN_DISTANCE_TO,
	     visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MAX),
	     GTK_PAIRS_COLUMN_PIXBUF_COLOR, pixColor,
	     GTK_PAIRS_COLUMN_LABEL_USER, label,
	     GTK_PAIRS_COLUMN_PRINT_LENGTH,
	     visu_pair_link_getPrintLength(data),
	     GTK_PAIRS_COLUMN_POINTER_TO_LINK, (gpointer)data,
	     -1);
#else
	  gtk_tree_store_append(pairsTreeStore, &iterParent, (GtkTreeIter*)0);
	  gtk_tree_store_set(pairsTreeStore, &iterParent,
			     GTK_PAIRS_COLUMN_POINTER_TO_ELE1, ele1,
			     GTK_PAIRS_COLUMN_POINTER_TO_ELE2, ele2,
			     GTK_PAIRS_COLUMN_DRAWN, drawn,
			     GTK_PAIRS_COLUMN_DISTANCE_FROM,
			     visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MIN),
			     GTK_PAIRS_COLUMN_DISTANCE_TO,
			     visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MAX),
			     GTK_PAIRS_COLUMN_PIXBUF_COLOR, pixColor,
			     GTK_PAIRS_COLUMN_LABEL_USER, label,
			     GTK_PAIRS_COLUMN_PRINT_LENGTH,
			     visu_pair_link_getPrintLength(data),
			     GTK_PAIRS_COLUMN_POINTER_TO_LINK, (gpointer)data,
			     -1);
#endif
	  first = FALSE;
	}
      else
	{
	  DBG_fprintf(stderr, " | subpair %s - %s (%g/%g).\n",
		      ele1->name, ele2->name,
                      visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MIN),
                      visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MAX));
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 9
	  gtk_tree_store_insert_with_values
	    (pairsTreeStore, &iter, &iterParent, 0,
	     GTK_PAIRS_COLUMN_POINTER_TO_ELE1, ele1,
	     GTK_PAIRS_COLUMN_POINTER_TO_ELE2, ele2,
	     GTK_PAIRS_COLUMN_DRAWN, drawn,
	     GTK_PAIRS_COLUMN_DISTANCE_FROM,
	     visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MIN),
	     GTK_PAIRS_COLUMN_DISTANCE_TO,
	     visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MAX),
	     GTK_PAIRS_COLUMN_PIXBUF_COLOR, pixColor,
	     GTK_PAIRS_COLUMN_LABEL_USER, label,
	     GTK_PAIRS_COLUMN_PRINT_LENGTH,
	     visu_pair_link_getPrintLength(data),
	     GTK_PAIRS_COLUMN_POINTER_TO_LINK, (gpointer)data,
	     -1);
#else
	  gtk_tree_store_append(pairsTreeStore, &iter, &iterParent);
	  gtk_tree_store_set(pairsTreeStore, &iter,
			     GTK_PAIRS_COLUMN_POINTER_TO_ELE1, ele1,
			     GTK_PAIRS_COLUMN_POINTER_TO_ELE2, ele2,
			     GTK_PAIRS_COLUMN_DRAWN, drawn,
			     GTK_PAIRS_COLUMN_DISTANCE_FROM,
			     visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MIN),
			     GTK_PAIRS_COLUMN_DISTANCE_TO,
			     visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MAX),
			     GTK_PAIRS_COLUMN_PIXBUF_COLOR, pixColor,
			     GTK_PAIRS_COLUMN_LABEL_USER, label,
			     GTK_PAIRS_COLUMN_PRINT_LENGTH,
			     visu_pair_link_getPrintLength(data),
			     GTK_PAIRS_COLUMN_POINTER_TO_LINK, (gpointer)data,
			     -1);
#endif
	}
      if (label)
        g_free(label);
    }
  DBG_fprintf(stderr, " | pair done.\n");
}

static void selectByPairMethod(GtkComboBox *combo, VisuPairExtension *meth)
{
  GtkTreeModel *model;
  GtkTreeIter iter;
  gboolean valid;
  VisuPairExtension *meth_;

  model = gtk_combo_box_get_model(combo);
  valid = gtk_tree_model_get_iter_first(model, &iter);
  while (valid)
    {
      gtk_tree_model_get(model, &iter,
			 GTK_PAIRS_COLUMN_PAIRS_POINTER, &meth_, -1);
      if (meth_ == meth)
	{
	  gtk_combo_box_set_active_iter(combo, &iter);
	  valid = FALSE;
	}
      else
	valid = gtk_tree_model_iter_next(model, &iter);
    }
}

static gint sortPairs(GtkTreeModel *model, GtkTreeIter *a,
		      GtkTreeIter *b, gpointer data)
{
  VisuElement *a_ele1, *a_ele2;
  VisuElement *b_ele1, *b_ele2;
  int cmp1, cmp2;
  gint id, diff;
  float a_from, a_to, b_from, b_to;
  GtkSortType sortDir;

  id = 0;
  sortDir = GTK_SORT_ASCENDING;
  gtk_tree_sortable_get_sort_column_id(GTK_TREE_SORTABLE(data),
				       &id, &sortDir);
  gtk_tree_model_get(model, a,
		     GTK_PAIRS_COLUMN_POINTER_TO_ELE1, &a_ele1,
		     GTK_PAIRS_COLUMN_POINTER_TO_ELE2, &a_ele2,
		     GTK_PAIRS_COLUMN_DISTANCE_FROM, &a_from,
		     GTK_PAIRS_COLUMN_DISTANCE_TO, &a_to,
		     -1);
  gtk_tree_model_get(model, b,
		     GTK_PAIRS_COLUMN_POINTER_TO_ELE1, &b_ele1,
		     GTK_PAIRS_COLUMN_POINTER_TO_ELE2, &b_ele2,
		     GTK_PAIRS_COLUMN_DISTANCE_FROM, &b_from,
		     GTK_PAIRS_COLUMN_DISTANCE_TO, &b_to,
		     -1);
  g_return_val_if_fail(a_ele1 && a_ele2 && b_ele1 && b_ele2, 0);

  cmp1 = strcmp(a_ele1->name, b_ele1->name);
  cmp2 = strcmp(a_ele2->name, b_ele2->name);
  DBG_fprintf(stderr, "Gtk Pairs: comparing %d - %d.\n", cmp1, cmp2);

  if (sortDir == GTK_SORT_ASCENDING)
    diff = (cmp1 == 0)?cmp2:cmp1;
  else
    diff = (cmp2 == 0)?cmp1:cmp2;

  if (diff == 0)
    {
      DBG_fprintf(stderr, "Gtk Pairs: compare distances %g / %g.\n",
                  (a_to + a_from), (b_to + b_from));
      if ((a_to + a_from) == (b_to + b_from))
        diff = 0;
      else
        diff = ((a_to + a_from) < (b_to + b_from))?-1:+1;
    }

  if (sortDir != GTK_SORT_ASCENDING)
    diff *= -1;

  DBG_fprintf(stderr, "Gtk Pairs: sort '%s - %s' with '%s - %s' column %d -> %d.\n",
	      a_ele1->name, a_ele2->name, b_ele1->name, b_ele2->name, id, diff);
  return diff;
}

void visu_ui_pairs_update(VisuUiMain *main, VisuData *dataObj, gboolean force)
{
  gboolean visible;
  VisuNodeArrayIter iter1, iter2;
/*   GtkTreeIter iter; */
/*   gboolean validIter; */

  DBG_fprintf(stderr, "Gtk Pairs: build the list of pairs.\n");

  /* Not show and not force, so we do nothing. */
  g_object_get(G_OBJECT(main->pairsDialog), "visible", &visible, NULL);
  if (!force && (!main->pairsDialog || !visible))
    {
      DBG_fprintf(stderr, "Gtk Pairs: Pairs become out of date.\n");
      pairsNeedBuild = TRUE;
      return;
    }

  /* We are visible, or forced but we are up to date, so we return. */
  if (!pairsNeedBuild)
    return;

  pairsNeedBuild = FALSE;

  DBG_fprintf(stderr, "Gtk Pairs: Reset all lists.\n");
  /* We free the list of pairs. */
  gtk_tree_store_clear(pairsTreeStore);
  DBG_fprintf(stderr, " | OK.\n");

  /* If there is no data, we return. */
  if (!dataObj)
    return;

  visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter1);
  visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter2);
  for(visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter1); iter1.element;
      visu_node_array_iterNextElement(VISU_NODE_ARRAY(dataObj), &iter1))
    for(visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter2);
        iter2.element && iter2.iElement <= iter1.iElement ;
        visu_node_array_iterNextElement(VISU_NODE_ARRAY(dataObj), &iter2))
      addPairAllData(iter1.element, iter2.element);
  DBG_fprintf(stderr, "Gtk Pairs: Pairs tree rebuilt, expanding.\n");
  gtk_tree_view_expand_all(GTK_TREE_VIEW(gtkPairs_treeView));
  /* If we have only one element, we make the filter insensitive. */
  gtk_widget_set_sensitive(comboFilter, (iter1.nElements > 1));
  /* We select the first link, if any. */
/*   validIter = gtk_tree_model_get_iter_first(pairsSortable, &iter); */
/*   if (validIter) */
/*     gtk_tree_selection_select_iter(pairsSelection, &iter); */

  DBG_fprintf(stderr, "Gtk Pairs: Pairs are rebuilt.\n");
}
static void rebuildPairsOnResources(GObject *obj _U_, VisuData *dataObj,
				    gpointer data)
{
  DBG_fprintf(stderr, "Gtk Pairs: caught the 'resourcesLoaded' signal.\n");
  pairsNeedBuild = TRUE;
  /* Update the viewport with the first list element only. */
  if (dataObj)
    visu_ui_pairs_update(VISU_UI_MAIN(data), dataObj, FALSE);

  /* TODO: Update the pair specific area. */
  g_message("TODO: update the specific area on resources.\n");
}
static void onDataReady(GObject *obj _U_, VisuData *dataObj,
                        VisuGlView *view _U_, gpointer data)
{
  gboolean visible;

  DBG_fprintf(stderr, "Gtk Pairs: caught the 'dataRendered' signal.\n");
  pairsNeedBuild = TRUE;
  visu_ui_pairs_update(VISU_UI_MAIN(data), dataObj, FALSE);

  /* Unset previous g(r). */
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 17
  visible = gtk_widget_get_visible(VISU_UI_MAIN(data)->pairsDialog);
#else
  visible = GTK_WIDGET_VISIBLE(VISU_UI_MAIN(data)->pairsDialog);
#endif
  DBG_fprintf(stderr, "Gtk Pairs: curve is visible (%d), replot.\n", visible);
  if (visible)
    g_idle_add(curveReplot, (gpointer)dataObj);
  else
    visu_ui_curve_frame_setData(VISU_UI_CURVE_FRAME(curve), 0.f, 0., 10.);

  if (dataObj)
    unit_signal = g_signal_connect(G_OBJECT(visu_boxed_getBox(VISU_BOXED(dataObj))), "UnitChanged",
                                   G_CALLBACK(onUnitChanged), (gpointer)0);
}
static void onDataNotReady(GObject *obj _U_, VisuData *dataObj,
                           VisuGlView *view _U_, gpointer data _U_)
{
  g_signal_handler_disconnect(G_OBJECT(visu_boxed_getBox(VISU_BOXED(dataObj))), unit_signal);
}

gchar* createDistanceLabel(VisuPairLink *data)
{
  GString *label;

  g_return_val_if_fail(data, (gchar*)0);

  label = g_string_new("");
  g_string_printf(label, _("from %7.3f to %7.3f"),
                  visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MIN),
                  visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MAX));
  return g_string_free(label, FALSE);
}

static struct specBuildFunc_struct* getSpecBuildFuncs(VisuPairLink *data)
{
  gboolean valid;
  GtkTreeIter iter;
  GtkTreeModel *model;
  VisuPairExtension *meth;
  gpointer meth_;
  struct specBuildFunc_struct *spec;
  
  meth = visu_gl_ext_pairs_getDrawMethod(visu_gl_ext_pairs_getDefault(), data);
  model = GTK_TREE_MODEL(gtkPairs_comboModel);
  for (valid = gtk_tree_model_get_iter_first(model, &iter); valid;
       valid = gtk_tree_model_iter_next(model, &iter))
    {
      gtk_tree_model_get(model, &iter,
                         GTK_PAIRS_COLUMN_PAIRS_POINTER, &meth_,
                         GTK_PAIRS_COLUMN_PAIRS_SPEC_BUILD, &spec, -1);
      if ((VisuPairExtension*)meth_ == meth)
        break;
    }
  if (valid)
    return spec;
  else
    return (struct specBuildFunc_struct*)0;
}

static void hideSpecAreas()
{
  gboolean valid;
  GtkTreeIter iter;
  GtkTreeModel *model;
  struct specBuildFunc_struct *spec;

  model = GTK_TREE_MODEL(gtkPairs_comboModel);
  for (valid = gtk_tree_model_get_iter_first(model, &iter); valid;
       valid = gtk_tree_model_iter_next(model, &iter))
    {
      gtk_tree_model_get(model, &iter,
                         GTK_PAIRS_COLUMN_PAIRS_SPEC_BUILD, &spec, -1);
      if (spec->wd)
        gtk_widget_hide(spec->wd);
    }
}
static void showSpecArea(struct specBuildFunc_struct *spec)
{
  if (!spec->wd && spec->area)
    {
      DBG_fprintf(stderr, "Gtk Pairs: create a new widget for this method.\n");
      spec->wd = spec->area();
      gtk_widget_show_all(spec->wd);
      gtk_box_pack_end(GTK_BOX(parameterWidget), spec->wd, FALSE, FALSE, 0);
    }
  hideSpecAreas();
  gtk_widget_show(spec->wd);
}

/**
 * visu_ui_pairs_newIter:
 * @iter: (out caller-allocates): an iterator.
 *
 * This method is used by bindings to initialise a new
 * #VisuUiPairsIter object.
 *
 * Since: 3.7
 */
void visu_ui_pairs_newIter(VisuUiPairsIter *iter)
{
  visu_ui_pairs_iter_startSelected(iter);
}
/**
 * visu_ui_pairs_iter_empty:
 * @iter: an iterator.
 *
 * Free internals of the iterator. This routine is automatically
 * called by visu_ui_pairs_iter_nextSelected() when arriving at the
 * end of selection.
 *
 * Since: 3.7
 */
void visu_ui_pairs_iter_empty(VisuUiPairsIter *iter)
{
  g_list_free_full(iter->selected, (GDestroyNotify) gtk_tree_path_free);
}
/**
 * visu_ui_pairs_iter_startSelected:
 * @iter: (out caller-allocates): an iterator.
 *
 * Initialise the given iterator @iter on the first selected pair or
 * set iter->data to NULL if none. If @iter->data is not NULL,
 * visu_ui_pairs_iter_nextSelected() must be called until @iter->data is
 * NULL to be sure that @iter->selected list is freed.
 */
void visu_ui_pairs_iter_startSelected(VisuUiPairsIter *iter)
{
  g_return_if_fail(iter);

  iter->data = (VisuPairLink*)0;
  iter->current = (GList*)0;
  
  if (!pairsSelection)
    return;
  iter->selected = gtk_tree_selection_get_selected_rows(pairsSelection, NULL);
  if (!iter->selected)
    return;

  iter->current = iter->selected;
  visu_ui_pairs_iter_nextSelected(iter);
}
/**
 * visu_ui_pairs_iter_nextSelected:
 * @iter: an iterator.
 *
 * Go to the next selected pair or set iter->data to NULL if none.
 */
void visu_ui_pairs_iter_nextSelected(VisuUiPairsIter *iter)
{
  gboolean validIter;
  GtkTreeIter iterFilter, iterSort;

  g_return_if_fail(iter && iter->selected);

  iter->data = (VisuPairLink*)0;

  if (!iter->current)
    {
      visu_ui_pairs_iter_empty(iter);
      return;
    }

  validIter = gtk_tree_model_get_iter(GTK_TREE_MODEL(pairsSortable), &iterSort,
				      (GtkTreePath*)iter->current->data);
  if (!validIter)
    {
      g_warning("Wrong 'path' variable in 'visu_ui_pairs_iter_startSelected' method.\n");
      return;
    }
  gtk_tree_model_sort_convert_iter_to_child_iter
    (GTK_TREE_MODEL_SORT(pairsSortable), &iterFilter, &iterSort);
  gtk_tree_model_filter_convert_iter_to_child_iter
    (GTK_TREE_MODEL_FILTER(pairsFilter), &(iter->iter), &iterFilter);

  gtk_tree_model_get(GTK_TREE_MODEL(pairsTreeStore), &(iter->iter),
		     GTK_PAIRS_COLUMN_POINTER_TO_LINK, &(iter->data),
		     -1);
  
  iter->current = g_list_next(iter->current);
}

void visu_ui_pairs_setSpecificLabels(GtkTreeIter *iter, const gchar *label)
{
  gtk_tree_store_set(pairsTreeStore, iter,
		     GTK_PAIRS_COLUMN_LABEL_USER, label,
		     -1);
}

/**
 * visu_ui_pairs_select:
 * @data: a #VisuPairLink object.
 *
 * Select in the UI the given @data.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the @data pair is indeed selected.
 **/
gboolean visu_ui_pairs_select(const VisuPairLink *data)
{
  GtkTreeIter iter, iterPair;
  gboolean valid, validPair;
  VisuPairLink *data_;

  DBG_fprintf(stderr, "Gtk Pairs: select a pair (%d).\n",
              gtk_tree_model_iter_n_children(GTK_TREE_MODEL(pairsSortable), NULL));
  for (valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(pairsSortable), &iter);
       valid;
       valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(pairsSortable), &iter))
    {
      for (validPair = gtk_tree_model_iter_children(GTK_TREE_MODEL(pairsSortable),
                                                    &iterPair, &iter);
           validPair;
           validPair = gtk_tree_model_iter_next(GTK_TREE_MODEL(pairsSortable),
                                                &iterPair))
        {
          gtk_tree_model_get(GTK_TREE_MODEL(pairsSortable), &iterPair,
                             GTK_PAIRS_COLUMN_POINTER_TO_LINK, &data_, -1);
          DBG_fprintf(stderr, " | %p %p.\n", (gpointer)data, (gpointer)data_);
          if (data_ == data)
            {
              gtk_tree_selection_select_iter(pairsSelection, &iterPair);
              return TRUE;
            }
        }
    }
  return FALSE;
}


/*************/
/* Callbacks */
/*************/

static void colorChanged(VisuUiColorCombobox *colorWd, ToolColor *color, gpointer userData _U_)
{
  int res;
  VisuUiPairsIter iter;
  GdkPixbuf *pixColor;

  pixColor = visu_ui_color_combobox_getPixbufFromColor(colorWd, color);
  res = 0;
  for (visu_ui_pairs_iter_startSelected(&iter); iter.data;
       visu_ui_pairs_iter_nextSelected(&iter))
    {
      res = visu_pair_link_setColor(iter.data, color) || res;
      gtk_tree_store_set(pairsTreeStore, &(iter.iter),
			 GTK_PAIRS_COLUMN_PIXBUF_COLOR, pixColor,
			 -1);
    }
  if (res)
    {
      visu_gl_ext_pairs_draw(visu_gl_ext_pairs_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void minChanged(GtkSpinButton *spin, gpointer userData)
{
  minMaxChanged(spin, GTK_TREE_SELECTION(userData), VISU_PAIR_DISTANCE_MIN);
}
static void maxChanged(GtkSpinButton *spin, gpointer userData)
{
  minMaxChanged(spin, GTK_TREE_SELECTION(userData), VISU_PAIR_DISTANCE_MAX);
}
static void filterChanged(VisuUiElementCombobox *combobox, GList *elements,
                          gpointer userData)
{
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 6
  GtkTreeIter iter;
#endif
  gchar *label;
  gboolean redraw;

  DBG_fprintf(stderr, "Gtk Pairs: change the filter to '%d'.\n",
	      gtk_combo_box_get_active(GTK_COMBO_BOX(combobox)));

  gtk_tree_model_filter_refilter(GTK_TREE_MODEL_FILTER(userData));

  if (elements)
    {
      label = ((VisuElement*)elements->data)->name;
      redraw = visu_ui_curve_frame_setFilter(VISU_UI_CURVE_FRAME(curve), label);
    }
  else
    redraw = visu_ui_curve_frame_setFilter(VISU_UI_CURVE_FRAME(curve), (const gchar*)0);

  if (redraw)
    {
      setHlRange(TRUE);
      visu_ui_curve_frame_draw(VISU_UI_CURVE_FRAME(curve));
    }

  gtk_tree_view_expand_all(GTK_TREE_VIEW(gtkPairs_treeView));
}
static gboolean pairsRowIsVisible(GtkTreeModel *model, GtkTreeIter *iter,
				  gpointer data)
{
  VisuElement *ele1, *ele2;
  gchar *label;
  gboolean visible;
  float start, stop;
  GList *elements;

  gtk_tree_model_get(model, iter,
		     GTK_PAIRS_COLUMN_POINTER_TO_ELE1, &ele1,
		     GTK_PAIRS_COLUMN_POINTER_TO_ELE2, &ele2,
		     GTK_PAIRS_COLUMN_DISTANCE_FROM, &start,
		     GTK_PAIRS_COLUMN_DISTANCE_TO, &stop,
		     -1);
  if (!ele1 || !ele2)
    return FALSE;

  elements = visu_ui_element_combobox_getSelection(VISU_UI_ELEMENT_COMBOBOX(data));
  if (elements)
    {
      label = ((VisuElement*)elements->data)->name;
      visible = !strcmp(label, ele1->name) || !strcmp(label, ele2->name);
      g_list_free(elements);
    }
  else
    visible = TRUE;

  /* Add the visibility tag due to the hide button. */
  visible = visible && (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(btHide)) ||
			(stop > start));
  DBG_fprintf(stderr, "Gtk Pairs: set visibility for '%s %s': %d\n",
	  ele1->name, ele2->name, visible);

  return visible;
}
static void pairsMethodChanged(GtkComboBox *combobox, gpointer userData _U_)
{
  gchar *label;
  struct specBuildFunc_struct *specBuild;
  GtkTreeIter it;
  VisuUiPairsIter iter;
  gboolean validIter, res;
  VisuPairExtension *pairsMethod;

  validIter = gtk_combo_box_get_active_iter(combobox, &it);
  g_return_if_fail(validIter);

  specBuild = (struct specBuildFunc_struct*)0;
  gtk_tree_model_get(GTK_TREE_MODEL(gtkPairs_comboModel), &it,
		     GTK_PAIRS_COLUMN_PAIRS_POINTER, &pairsMethod,
		     GTK_PAIRS_COLUMN_PAIRS_SPEC_BUILD, &specBuild,
		     -1);
  DBG_fprintf(stderr, "Gtk Pairs: set combobox to '%s'.\n",
              visu_pair_extension_getName(pairsMethod, FALSE));

  /* change the specific area. */
  showSpecArea(specBuild);

  /* change the label area and the method per pair. */
  DBG_fprintf(stderr, "Gtk Pairs: changing specific labels.\n");
  res = FALSE;
  for (visu_ui_pairs_iter_startSelected(&iter); iter.data;
       visu_ui_pairs_iter_nextSelected(&iter))
    {
      DBG_fprintf(stderr, " | pair %p\n", (gpointer)iter.data);
      if (specBuild && specBuild->label)
        {
          label = specBuild->label(iter.data);
          visu_ui_pairs_setSpecificLabels(&(iter.iter), label);
          g_free(label);
        }
      res = visu_gl_ext_pairs_setDrawMethod(visu_gl_ext_pairs_getDefault(),
                                            iter.data, pairsMethod) || res;
    }
  if (res)
    {
      visu_gl_ext_pairs_draw(visu_gl_ext_pairs_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void onGtkPairsDrawnToggled(GtkCellRendererToggle *cell_renderer _U_,
				   gchar *path, gpointer user_data _U_)
{
  gboolean valid, drawn;
  GtkTreeIter iter, iterFilter, iterSort;
  gboolean res;
  VisuPairLink *pdata;

  valid = gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(pairsSortable),
					      &iterSort, path);
  g_return_if_fail(valid);

  gtk_tree_model_sort_convert_iter_to_child_iter
    (GTK_TREE_MODEL_SORT(pairsSortable), &iterFilter, &iterSort);
  gtk_tree_model_filter_convert_iter_to_child_iter
    (GTK_TREE_MODEL_FILTER(pairsFilter), &iter, &iterFilter);
  gtk_tree_model_get(GTK_TREE_MODEL(pairsTreeStore), &iter,
		     GTK_PAIRS_COLUMN_DRAWN, &drawn,
		     GTK_PAIRS_COLUMN_POINTER_TO_LINK, &pdata,
		     -1);
  gtk_tree_store_set(pairsTreeStore, &iter,
		     GTK_PAIRS_COLUMN_DRAWN, !drawn,
		     -1);
  res = visu_pair_link_setDrawn(pdata, !drawn);

  if (res)
    {
      visu_gl_ext_pairs_draw(visu_gl_ext_pairs_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void onGtkPairsLengthToggled(GtkCellRendererToggle *cell_renderer _U_,
				    gchar *path, gpointer user_data _U_)
{
  gboolean valid, length;
  GtkTreeIter iter, iterFilter, iterSort;
  gboolean res;
  VisuPairLink *pdata;

  valid = gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(pairsSortable),
					      &iterSort, path);
  g_return_if_fail(valid);

  gtk_tree_model_sort_convert_iter_to_child_iter
    (GTK_TREE_MODEL_SORT(pairsSortable), &iterFilter, &iterSort);
  gtk_tree_model_filter_convert_iter_to_child_iter
    (GTK_TREE_MODEL_FILTER(pairsFilter), &iter, &iterFilter);
  gtk_tree_model_get(GTK_TREE_MODEL(pairsTreeStore), &iter,
		     GTK_PAIRS_COLUMN_PRINT_LENGTH, &length,
		     GTK_PAIRS_COLUMN_POINTER_TO_LINK, &pdata,
		     -1);
  gtk_tree_store_set(pairsTreeStore, &iter,
		     GTK_PAIRS_COLUMN_PRINT_LENGTH, !length,
		     -1);
  res = visu_pair_link_setPrintLength(pdata, !length);

  if (res)
    {
      visu_gl_ext_pairs_draw(visu_gl_ext_pairs_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void onDistanceValueEdited(GtkCellRendererText *cellrenderertext _U_,
				  gchar *path, gchar *text, gpointer user_data)
{
  gboolean valid;
  GtkTreeIter iter, iterFilter, iterSort;
  float value;
  char *error;
  VisuPairLink *pdata;
  int res, id;

  DBG_fprintf(stderr, "Gtk Pairs: distance (%d) edited '%s'.\n",
	      GPOINTER_TO_INT(user_data), text);

  g_return_if_fail(GPOINTER_TO_INT(user_data) == GTK_PAIRS_COLUMN_DISTANCE_FROM ||
		   GPOINTER_TO_INT(user_data) == GTK_PAIRS_COLUMN_DISTANCE_TO);

  value = (float)strtod(text, &error);
  g_return_if_fail(!(value == 0. && error == text));

  valid = gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(pairsSortable),
					      &iterSort, path);
  g_return_if_fail(valid);
  gtk_tree_model_sort_convert_iter_to_child_iter
    (GTK_TREE_MODEL_SORT(pairsSortable), &iterFilter, &iterSort);
  gtk_tree_model_filter_convert_iter_to_child_iter
    (GTK_TREE_MODEL_FILTER(pairsFilter), &iter, &iterFilter);
  
  gtk_tree_store_set(pairsTreeStore, &iter,
		     GPOINTER_TO_INT(user_data), value,
		     -1);

  gtk_tree_model_get(GTK_TREE_MODEL(pairsTreeStore), &iter,
		     GTK_PAIRS_COLUMN_POINTER_TO_LINK, &pdata,
		     -1);
  id = (GPOINTER_TO_INT(user_data) == GTK_PAIRS_COLUMN_DISTANCE_TO)?
    VISU_PAIR_DISTANCE_MAX:VISU_PAIR_DISTANCE_MIN;
  res = visu_pair_link_setDistance(pdata, value, id);

  if (GPOINTER_TO_INT(user_data) == GTK_PAIRS_COLUMN_DISTANCE_FROM)
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkPairs_spinMin), value);
  else
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkPairs_spinMax), value);

  if (res)
    {
      visu_gl_ext_pairs_draw(visu_gl_ext_pairs_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void minMaxChanged(GtkSpinButton *spin, GtkTreeSelection *tree _U_, int minMax)
{
  int res, id;
  float value;
  VisuUiPairsIter iter;

  value = (float)gtk_spin_button_get_value(spin);
  res = 0;
  for (visu_ui_pairs_iter_startSelected(&iter); iter.data;
       visu_ui_pairs_iter_nextSelected(&iter))
    {
      res = visu_pair_link_setDistance(iter.data, value, minMax) || res;
      id = (minMax == VISU_PAIR_DISTANCE_MIN)?GTK_PAIRS_COLUMN_DISTANCE_FROM:
	GTK_PAIRS_COLUMN_DISTANCE_TO;
      gtk_tree_store_set(pairsTreeStore, &(iter.iter), id, value, -1);
    }
  if (res)
    {
      visu_gl_ext_pairs_draw(visu_gl_ext_pairs_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void selectionChanged(GtkTreeSelection *tree _U_, gpointer data)
{
  gboolean select, isChild, hasChild;
  VisuUiPairsIter iter;
  GtkTreeIter iterParent;
  struct specBuildFunc_struct *spec;

  DBG_fprintf(stderr, "Gtk Pairs: selection changed.\n");
  select = FALSE;
  isChild = FALSE;
  hasChild = FALSE;
  for (visu_ui_pairs_iter_startSelected(&iter); iter.data;
       visu_ui_pairs_iter_nextSelected(&iter))
    {
      select = TRUE;
      /* Modify the values with the last selected iter. */
      if (!iter.current)
	{
	  /* Set the min/max values */
	  g_signal_handler_block(G_OBJECT(gtkPairs_spinMin),
				 gtkPairs_spinMinSignalId);
	  g_signal_handler_block(G_OBJECT(gtkPairs_spinMax),
				 gtkPairs_spinMaxSignalId);
	  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkPairs_spinMin),
				    visu_pair_link_getDistance(iter.data, VISU_PAIR_DISTANCE_MIN));
	  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkPairs_spinMax),
				    visu_pair_link_getDistance(iter.data, VISU_PAIR_DISTANCE_MAX));
	  g_signal_handler_unblock(G_OBJECT(gtkPairs_spinMax),
				   gtkPairs_spinMaxSignalId);
	  g_signal_handler_unblock(G_OBJECT(gtkPairs_spinMin),
				   gtkPairs_spinMinSignalId);
	      
	  /* Set the color */
          g_signal_handler_block(G_OBJECT(widgetColor), widgetColorSignalId);
          visu_ui_color_combobox_setSelection(VISU_UI_COLOR_COMBOBOX(widgetColor),
                                              visu_pair_link_getColor(iter.data));
          g_signal_handler_unblock(G_OBJECT(widgetColor), widgetColorSignalId);

	  /* Show and set the method parameters */
          spec = getSpecBuildFuncs(iter.data);
          if (spec)
            {
              showSpecArea(spec);
              if (spec->selected)
                spec->selected(iter.data);
	      g_signal_handler_block(G_OBJECT(data), methodSignalId);
              selectByPairMethod(GTK_COMBO_BOX(data),
                                 visu_gl_ext_pairs_getDrawMethod(visu_gl_ext_pairs_getDefault(), iter.data));
	      g_signal_handler_unblock(G_OBJECT(data), methodSignalId);
            }

	  isChild = gtk_tree_model_iter_parent(GTK_TREE_MODEL(pairsTreeStore),
						&iterParent, &(iter.iter));
	  hasChild = gtk_tree_model_iter_has_child(GTK_TREE_MODEL(pairsTreeStore),
						   &(iter.iter));
	}
    }

  /* Set sensitive or not the modifing widgets. */
  gtk_widget_set_sensitive(alignWidget, select);
  gtk_widget_set_sensitive(btAdd, select);
  gtk_widget_set_sensitive(btRemove, select && (isChild || hasChild));
}
static void onDistanceAutoSet(GtkButton *button _U_, gpointer data _U_)
{
  VisuUiPairsIter iter;
  VisuPair *pair;
  VisuElement *ele1, *ele2;
  VisuPairDistribution *dd;
  VisuData *dataObj;
  float start, stop;
  guint sum, startStopId[2];
  gboolean rebuild;

  rebuild = FALSE;
  g_signal_handler_block(G_OBJECT(gtkPairs_spinMin), gtkPairs_spinMinSignalId);
  g_signal_handler_block(G_OBJECT(gtkPairs_spinMax), gtkPairs_spinMaxSignalId);
  start = -1.f;
  stop = -1.f;
  for (visu_ui_pairs_iter_startSelected(&iter); iter.data;
       visu_ui_pairs_iter_nextSelected(&iter))
    {  
      /* Get the two elements and the corresponding pair. */
      gtk_tree_model_get(GTK_TREE_MODEL(pairsTreeStore), &(iter.iter),
			 GTK_PAIRS_COLUMN_POINTER_TO_ELE1, &ele1,
			 GTK_PAIRS_COLUMN_POINTER_TO_ELE2, &ele2,
			 -1);
      pair = visu_pair_getPair(ele1, ele2);

      dataObj = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());
      dd = visu_pair_getDistanceDistribution(pair, dataObj, -1.f, -1.f, -1.f);
      g_return_if_fail(dd);
      startStopId[0] = 0;
      startStopId[1] = dd->nValues - 1;

      if (visu_pair_distribution_getNextPick(dd, startStopId, &sum, (guint*)0, (guint*)0))
	{
          start = dd->initValue + startStopId[0] * dd->stepValue;
          stop  = dd->initValue + startStopId[1] * dd->stepValue;
	  gtk_tree_store_set(pairsTreeStore, &(iter.iter),
			     GTK_PAIRS_COLUMN_DISTANCE_FROM, start,
			     GTK_PAIRS_COLUMN_DISTANCE_TO, stop,
			     -1);

	  rebuild = visu_pair_link_setDistance(iter.data, start, VISU_PAIR_DISTANCE_MIN) | rebuild;
	  rebuild = visu_pair_link_setDistance(iter.data, stop, VISU_PAIR_DISTANCE_MAX) | rebuild;
	}
    }
  if (start > 0.f)
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkPairs_spinMin), start);
  if (stop > 0.f)
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkPairs_spinMax), stop);
  g_signal_handler_unblock(G_OBJECT(gtkPairs_spinMin), gtkPairs_spinMinSignalId);
  g_signal_handler_unblock(G_OBJECT(gtkPairs_spinMax), gtkPairs_spinMaxSignalId);

  if (rebuild)
    {
      visu_gl_ext_pairs_draw(visu_gl_ext_pairs_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void onBtAdd(GtkButton *button _U_, gpointer user_data _U_)
{
  VisuUiPairsIter iter;
  GtkTreeIter iterParent, iterChild;
  VisuElement *ele1, *ele2;
  VisuPairLink *data, *dataRef;
  float minMax[2] = {0.f, 0.f};
  gboolean valid;
  struct specBuildFunc_struct *specBuild;
  gboolean drawn;
  ToolColor *color;
  GdkPixbuf *pixColor;
  gchar *label;

  /* Go to last selected iter. */
  for (visu_ui_pairs_iter_startSelected(&iter); iter.current;
       visu_ui_pairs_iter_nextSelected(&iter));
  g_return_if_fail(iter.data);
  
  /* Get the ancestor of this iter. */
  if (!gtk_tree_model_iter_parent(GTK_TREE_MODEL(pairsTreeStore), &iterParent,
				  &(iter.iter)))
    iterParent = iter.iter;

  /* Get the two elements. */
  gtk_tree_model_get(GTK_TREE_MODEL(pairsTreeStore), &iterParent,
		     GTK_PAIRS_COLUMN_POINTER_TO_ELE1, &ele1,
		     GTK_PAIRS_COLUMN_POINTER_TO_ELE2, &ele2,
		     GTK_PAIRS_COLUMN_POINTER_TO_LINK, &dataRef,
		     -1);

  /* Get a new data. */
  data = visu_pair_link_new(ele1, ele2, minMax);

  /* Check that this data is not already in the tree. */
  if (data == dataRef)
    return;
  valid = gtk_tree_model_iter_children(GTK_TREE_MODEL(pairsTreeStore),
				       &iterChild, &iterParent);
  while (valid)
    {
      gtk_tree_model_get(GTK_TREE_MODEL(pairsTreeStore), &iterChild,
			 GTK_PAIRS_COLUMN_POINTER_TO_LINK, &dataRef,
			 -1);
      if (data == dataRef)
	return;
      valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(pairsTreeStore), &iterChild);
    }
  DBG_fprintf(stderr, "Gtk Pairs: ready to add a new pair definition.\n");

  /* Ok, add this new data to the tree. */
  specBuild = getSpecBuildFuncs(data);
  if (specBuild && specBuild->label)
    label = specBuild->label(data);
  else
    label = (gchar*)0;
  drawn = visu_pair_link_getDrawn(data);
  color = visu_pair_link_getColor(data);
  pixColor = visu_ui_color_combobox_getPixbufFromColor(VISU_UI_COLOR_COMBOBOX(widgetColor),
					      color);

  DBG_fprintf(stderr, "Gtk Pairs: add a new pair definition.\n");
  /* Add the VisuPairLink to the liststore. */
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 9
  gtk_tree_store_insert_with_values
    (pairsTreeStore, &iterChild, &iterParent, -1,
     GTK_PAIRS_COLUMN_POINTER_TO_ELE1, (gpointer)ele1,
     GTK_PAIRS_COLUMN_POINTER_TO_ELE2, (gpointer)ele2,
     GTK_PAIRS_COLUMN_DRAWN, drawn,
     GTK_PAIRS_COLUMN_DISTANCE_FROM,
     visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MIN),
     GTK_PAIRS_COLUMN_DISTANCE_TO,
     visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MAX),
     GTK_PAIRS_COLUMN_PIXBUF_COLOR, pixColor,
     GTK_PAIRS_COLUMN_LABEL_USER, label,
     GTK_PAIRS_COLUMN_PRINT_LENGTH,
     visu_pair_link_getPrintLength(data),
     GTK_PAIRS_COLUMN_POINTER_TO_LINK, (gpointer)data,
     -1);
#else
  gtk_tree_store_append(pairsTreeStore, &iterChild, &iterParent);
  gtk_tree_store_set(pairsTreeStore, &iterChild,
		     GTK_PAIRS_COLUMN_POINTER_TO_ELE1, (gpointer)ele1,
		     GTK_PAIRS_COLUMN_POINTER_TO_ELE2, (gpointer)ele2,
		     GTK_PAIRS_COLUMN_DRAWN, drawn,
		     GTK_PAIRS_COLUMN_DISTANCE_FROM,
		     visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MIN),
		     GTK_PAIRS_COLUMN_DISTANCE_TO,
		     visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MAX),
		     GTK_PAIRS_COLUMN_PIXBUF_COLOR, pixColor,
		     GTK_PAIRS_COLUMN_LABEL_USER, label,
		     GTK_PAIRS_COLUMN_PRINT_LENGTH,
		     visu_pair_link_getPrintLength(data),
		     GTK_PAIRS_COLUMN_POINTER_TO_LINK, (gpointer)data,
		     -1);
  gtk_tree_model_filter_refilter(GTK_TREE_MODEL_FILTER(pairsFilter));
#endif
  DBG_fprintf(stderr, " | OK (%s)\n", label);

  if (label)
    g_free(label);

  gtk_tree_view_expand_all(GTK_TREE_VIEW(gtkPairs_treeView));
  VISU_REDRAW_ADD;
}
static void onBtRemove(GtkButton *button _U_, gpointer user_data _U_)
{
  VisuUiPairsIter iter;
  GtkTreeIter iterParent;
  VisuPairLink *data;
  VisuElement *ele1, *ele2;
  gboolean rebuild;

  /* Go to last iter. */
  for (visu_ui_pairs_iter_startSelected(&iter); iter.current;
       visu_ui_pairs_iter_nextSelected(&iter));
  g_return_if_fail(iter.data);
  
  /* Get the ancestor of this iter. */
  rebuild = (!gtk_tree_model_iter_parent(GTK_TREE_MODEL(pairsTreeStore), &iterParent,
					 &(iter.iter)));

  /* Get the two elements. */
  gtk_tree_model_get(GTK_TREE_MODEL(pairsTreeStore), &(iter.iter),
		     GTK_PAIRS_COLUMN_POINTER_TO_ELE1, &ele1,
		     GTK_PAIRS_COLUMN_POINTER_TO_ELE2, &ele2,
		     GTK_PAIRS_COLUMN_POINTER_TO_LINK, &data,
		     -1);

  /* Remove the data. */
  visu_pair_removePairLink(ele1, ele2, data);
  gtk_tree_store_remove(pairsTreeStore, &(iter.iter));
  
  /* Recreate the entry if parent was removed. */
  if (rebuild)
    addPairAllData(ele1, ele2);

  gtk_tree_view_expand_all(GTK_TREE_VIEW(gtkPairs_treeView));

  visu_gl_ext_pairs_draw(visu_gl_ext_pairs_getDefault());
  VISU_REDRAW_ADD;
}
static void onBtHide(GtkToggleButton *button _U_, gpointer user_data)
{
  gtk_tree_model_filter_refilter(GTK_TREE_MODEL_FILTER(user_data));
}

static gboolean curveReplot(gpointer data)
{
  curveSetData(VISU_DATA(data));
  setHlRange(TRUE);
  visu_ui_curve_frame_draw(VISU_UI_CURVE_FRAME(curve));

  return FALSE;
}

static void curveSetData(VisuData *dataObj)
{
  float span[2], zoom, step;
  VisuPair *pair;
  VisuPairDistribution *dd;
  VisuNodeArrayIter iter1, iter2;

  span[0] = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinDMin));
  span[1] = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinDMax));

  if (!dataObj)
    {
      visu_ui_curve_frame_setData(VISU_UI_CURVE_FRAME(curve), 0.f, span[0], span[1]);
      return;
    }

  zoom = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinDZoom)) * 50.f;
  step = (span[1] - span[0]) / zoom;
  visu_ui_curve_frame_setData(VISU_UI_CURVE_FRAME(curve), step, span[0], span[1]);

  visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter1);
  visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter2);
  for (visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter1); iter1.element;
       visu_node_array_iterNextElement(VISU_NODE_ARRAY(dataObj), &iter1))
    for(visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter2);
        iter2.element && iter2.iElement <= iter1.iElement ;
        visu_node_array_iterNextElement(VISU_NODE_ARRAY(dataObj), &iter2))
      {
        DBG_fprintf(stderr, "Gtk Pairs: compute distribution for pair '%s' '%s'.\n",
                    iter1.element->name, iter2.element->name);
        pair = visu_pair_getPair(iter1.element, iter2.element);
        dd = visu_pair_getDistanceDistribution(pair, dataObj,
                                               step, span[0], span[1]);
        visu_ui_curve_frame_addData(VISU_UI_CURVE_FRAME(curve), dd->ele1->name, dd->ele2->name,
                           dd->histo, dd->nValues, dd->initValue, dd->stepValue);
        if (iter1.element == iter2.element)
          visu_ui_curve_frame_setNNodes(VISU_UI_CURVE_FRAME(curve), dd->ele1->name, dd->nNodesEle1);
      }
}
static gboolean setHlRange(gboolean set)
{
  float range[2];
  gboolean redraw;
  float val;
  gchar *label, *buf;
  
  if (set)
    {
      range[0] = (float)gtk_range_get_value(GTK_RANGE(rangeHlStart));
      range[1] = (float)gtk_range_get_value(GTK_RANGE(rangeHlEnd));
    }
  else
    range[0] = -1.f;

  redraw = visu_ui_curve_frame_setHighlightRange(VISU_UI_CURVE_FRAME(curve), range);
  val = visu_ui_curve_frame_getIntegralInRange(VISU_UI_CURVE_FRAME(curve), &label);
  if (val > 0.f)
    {
      buf = g_strdup_printf(DIST_INTEGRAL, val * 2.f, label);
      gtk_label_set_markup(GTK_LABEL(labelIntegralDistance), buf);
      g_free(buf);
    }
  else
    gtk_label_set_markup(GTK_LABEL(labelIntegralDistance), NO_DIST_INTEGRAL);
  val = visu_ui_curve_frame_getMeanInRange(VISU_UI_CURVE_FRAME(curve), (gchar**)0);
  if (val > 0.f)
    {
      buf = g_strdup_printf(DIST_MEAN, val);
      gtk_label_set_markup(GTK_LABEL(labelMeanDistance), buf);
      g_free(buf);
    }
  else
    gtk_label_set_markup(GTK_LABEL(labelMeanDistance), NO_DIST_MEAN);
  
  return redraw;
}
static void onHlRangeToggled(GtkToggleButton *button, gpointer data)
{
  float range[2];
  GList *lst;
  gboolean redraw;
  gfloat d2, xyz1[3], xyz2[3];
  VisuElement *ele;
  VisuData *dataObj;
  VisuNodeArrayIter iter1, iter2;
  VisuGlExtMarks *marks;
  VisuUiRenderingWindow *window;

  if (!visu_ui_curve_frame_getHighlightRange(VISU_UI_CURVE_FRAME(data), range))
    return;
  range[0] *= range[0];
  range[1] *= range[1];

  ele = (VisuElement*)0;
  lst = visu_ui_element_combobox_getSelection(VISU_UI_ELEMENT_COMBOBOX(comboFilter));
  if (lst)
    {
      g_return_if_fail(g_list_length(lst) == 1);
      ele = VISU_ELEMENT(lst->data);
      g_list_free(lst);
    }

  redraw = FALSE;
  window = visu_ui_main_class_getDefaultRendering();
  marks = visu_ui_rendering_window_getMarks(window);
  lst = g_object_get_data(G_OBJECT(button), "HighlightRangeIds");
  if (lst)
    {
      DBG_fprintf(stderr, "Gtk Pairs: remove %d highlights.\n", g_list_length(lst));
      redraw = visu_gl_ext_marks_setHighlightedList(marks, lst, MARKS_STATUS_UNSET);
      g_list_free(lst);
      g_object_set_data(G_OBJECT(button), "HighlightRangeIds", (gpointer)0);
    }

  if (gtk_toggle_button_get_active(button))
    {
      lst = (GList*)0;
      dataObj = visu_ui_rendering_window_getData(window);
      visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter1);
      visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter2);
      for(visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter1),
            visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter2);
          iter1.node && iter2.node; visu_node_array_iter_next2(&iter1, &iter2))
        if (visu_element_getRendered(iter1.element) &&
            visu_element_getRendered(iter2.element) &&
            iter1.node->rendered && iter2.node->rendered &&
            (!ele || (ele && (iter1.element == ele ||
                              iter2.element == ele))))
          {
            visu_data_getNodePosition(dataObj, iter1.node, xyz1);
            visu_data_getNodePosition(dataObj, iter2.node, xyz2);
            d2 = (xyz1[0] - xyz2[0]) * (xyz1[0] - xyz2[0]) + 
              (xyz1[1] - xyz2[1]) * (xyz1[1] - xyz2[1]) + 
              (xyz1[2] - xyz2[2]) * (xyz1[2] - xyz2[2]);
            if (d2 >= range[0] && d2 < range[1])
              {
                lst = g_list_prepend(lst, GINT_TO_POINTER(iter1.node->number));
                lst = g_list_prepend(lst, GINT_TO_POINTER(iter2.node->number));
                /* DBG_fprintf(stderr, "Test pair %d-%d %f\n", */
                /*             iter1.node->number, iter2.node->number, d2); */
                visu_node_array_iterNext(VISU_NODE_ARRAY(dataObj), &iter1);
                visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter2);
              }
          }
      g_object_set_data(G_OBJECT(button), "HighlightRangeIds", lst);
      DBG_fprintf(stderr, "Gtk Pairs: add %d highlights.\n", g_list_length(lst));
      redraw = visu_gl_ext_marks_setHighlightedList(marks, lst, MARKS_STATUS_SET) || redraw;
    }
  if (redraw)
    {
      visu_gl_ext_pairs_draw(visu_gl_ext_pairs_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void onRangeChanged(GtkRange *rg, gpointer data)
{
  float val;

  val = (float)gtk_range_get_value(rg);
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 11
  if (GTK_WIDGET(rg) == rangeHlStart &&
      val > gtk_range_get_value(GTK_RANGE(data)))
    gtk_range_set_value(rg, gtk_range_get_value(GTK_RANGE(data)));
#endif
  if (GTK_WIDGET(rg) == rangeHlEnd)
    {
      if (val < gtk_range_get_value(GTK_RANGE(data)))
	{
	  gtk_range_set_value(rg, gtk_range_get_value(GTK_RANGE(data)));
	  val = gtk_range_get_value(GTK_RANGE(data));
	}
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
      gtk_range_set_fill_level(GTK_RANGE(data), val);
#endif
    }
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkHighlight), FALSE);
  
  if (setHlRange(TRUE))
    visu_ui_curve_frame_draw(VISU_UI_CURVE_FRAME(curve));
}

static gboolean onElementChanged(GSignalInvocationHint *ihint _U_, guint n_param_values _U_,
                                 const GValue *param_values, gpointer data)
{
  VisuElement *ele;

  ele = VISU_ELEMENT(g_value_get_object(param_values));
  DBG_fprintf(stderr, "Gtk Curve: element '%s' has been modified.\n", ele->name);

  visu_ui_curve_frame_draw(VISU_UI_CURVE_FRAME(data));

  return TRUE;
}
static void onDistSpanChanged(GtkSpinButton *spin _U_, gpointer userData _U_)
{
  float span[2];
  VisuData *dataObj;

  if (disableCallbacks)
    return;

  span[0] = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinDMin));
  span[1] = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinDMax));
  visu_ui_curve_frame_setSpan(VISU_UI_CURVE_FRAME(curve), span);

  /* Update highlight ranges. */
  gtk_range_set_range(GTK_RANGE(rangeHlStart),
		      gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinDMin)),
		      gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinDMax)));
  gtk_range_set_range(GTK_RANGE(rangeHlEnd),
		      gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinDMin)),
		      gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinDMax)));

  dataObj = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());
  g_idle_add(curveReplot, (gpointer)dataObj);
}
static void onUnitChanged(VisuData *dataObj _U_, gfloat factor, gpointer data _U_)
{
  float span[2];
  
  span[0] = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinDMin));
  span[1] = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinDMax));

  disableCallbacks = TRUE;
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinDMin), span[0] * factor);
  disableCallbacks = FALSE;
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinDMax), span[1] * factor);
}
