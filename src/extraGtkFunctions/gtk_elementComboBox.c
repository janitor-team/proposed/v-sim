/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include <gtk/gtk.h>

#include <string.h>

#include "gtk_elementComboBox.h"
#include <visu_tools.h>
#include <visu_object.h>
#include <visu_elements.h>
#include <visu_data.h>

/**
 * SECTION:gtk_elementComboBox
 * @short_description: Defines a specialised #GtkComboBox to choose #VisuElement.
 *
 * <para>This widget looks like a #GtkComboBox and it displays a list
 * of #VisuElement currently used by the displayed data.</para>
 *
 * Since: 3.6
 */

enum {
  ELEMENT_SELECTED_SIGNAL,
  LAST_SIGNAL
};

/* This enum is used to access the column of the GtkListStore
   that contains the informations of stroed shades. */
enum
  {
    /* This is a pointer to a label that describes the element. */
    COLUMN_ELEMENT_LABEL,
    /* This a pointer to the VisuElement */
    COLUMN_ELEMENT_POINTER_TO,
    /* This column control the visibility of the VisuElement. */
    COLUMN_ELEMENT_VISIBLE,

    N_COLUMN_ELEMENT
  };

static void visu_ui_element_combobox_dispose (GObject *obj);
static void visu_ui_element_combobox_finalize(GObject *obj);

static guint visu_ui_element_combobox_signals[LAST_SIGNAL] = { 0 };

/**
 * VisuUiElementCombobox:
 *
 * An opaque structure defining a #VisuUiElementCombobox widget.
 *
 * Since: 3.6
 */
struct _VisuUiElementCombobox
{
  GtkComboBox comboElement;
  GtkTreeModel *filter;

  gulong onChanged;
  gpointer previousSelection;

  gboolean hasAllSelector;
  gboolean hasNoneSelector;
  gboolean showUnPhysical;
  gchar *format;

  /* Memory gestion. */
  gboolean dispose_has_run;
};

/**
 * VisuUiElementComboboxClass:
 *
 * An opaque structure defining the class of a #VisuUiElementCombobox widget.
 *
 * Since: 3.6
 */
struct _VisuUiElementComboboxClass
{
  GtkComboBoxClass parent_class;

  void (*elementComboBox) (VisuUiElementCombobox *shadeCombo);

  /* This listStore contains all the elements
     known by widgets of this class. It is used
     as TreeModel for the combobox in the widget. */
  GtkListStore *storedElements;

  gulong newElementSignalId, popDef_signal;
  gboolean disableChangeSignal;
};
static VisuUiElementComboboxClass *my_class = (VisuUiElementComboboxClass*)0;

/* Local callbacks. */
static void onChanged(GtkComboBox *widget, gpointer user_data);
static gboolean onElementNewHook(GSignalInvocationHint *ihint, guint nvalues,
                                 const GValue *param_values, gpointer data);
static void onDataReady(VisuUiElementComboboxClass *klass, VisuData *data,
                        VisuGlView *view, gpointer user_data);
static void onDataNotReady(VisuUiElementComboboxClass *klass, VisuData *data,
                           VisuGlView *view, gpointer user_data);
static void onPopulationDefined(VisuNodeArray *array, guint nEle, gpointer data);
static void onDataReadySelection(VisuUiElementCombobox *combo, VisuData *data,
                                 VisuGlView *view, gpointer user_data);

/* Local methods. */
static void addElementToModel(GtkTreeIter *iter, VisuUiElementComboboxClass* klass,
                              gpointer element);
static void printLabel(GtkCellLayout *layout, GtkCellRenderer *cell,
                       GtkTreeModel *model, GtkTreeIter *iter, gpointer data);
static gboolean showLabel(GtkTreeModel *model, GtkTreeIter *iter, gpointer data);

/**
 * visu_ui_element_combobox_get_type:
 *
 * Internal routine, retrieves the type of #VisuUiElementCombobox
 * objects. Use VISU_UI_TYPE_ELEMENT_COMBOBOX macro instead.
 *
 * Since: 3.6
 */
G_DEFINE_TYPE(VisuUiElementCombobox, visu_ui_element_combobox, GTK_TYPE_COMBO_BOX)

static void visu_ui_element_combobox_class_init(VisuUiElementComboboxClass *klass)
{
  GtkTreeIter iter;
  GList *elementLst;
  
  DBG_fprintf(stderr, "VisuUi ElementCombobox: creating the class of the widget.\n");
  DBG_fprintf(stderr, "                     - adding new signals ;\n");
  /**
   * VisuUiElementCombobox::element-selected:
   * @combo: the #VisuUiElementCombobox that emits the signal ;
   * @element: the newly selected #VisuElement.
   *
   * This signal is emitted when a new element is selected.
   *
   * Since: 3.6
   */
  visu_ui_element_combobox_signals[ELEMENT_SELECTED_SIGNAL] =
    g_signal_new ("element-selected",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
		  G_STRUCT_OFFSET (VisuUiElementComboboxClass, elementComboBox),
		  NULL, 
		  NULL,                
		  g_cclosure_marshal_VOID__POINTER,
		  G_TYPE_NONE, 1, G_TYPE_POINTER);

  DBG_fprintf(stderr, "                     - initializing the listStore of elements.\n");
  /* Init the listStore of elements. */
  klass->storedElements = gtk_list_store_new(N_COLUMN_ELEMENT,
                                             G_TYPE_STRING,
                                             G_TYPE_POINTER,
                                             G_TYPE_BOOLEAN);
  addElementToModel(&iter, klass, (gpointer)0);
  addElementToModel(&iter, klass, GINT_TO_POINTER(1));
  for (elementLst = (GList*)visu_element_getAllElements(); elementLst;
       elementLst = g_list_next(elementLst))
    addElementToModel(&iter, klass, elementLst->data);

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_ui_element_combobox_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_ui_element_combobox_finalize;

  g_signal_add_emission_hook(g_signal_lookup("ElementNew", VISU_TYPE_ELEMENT),
                             0, onElementNewHook, (gpointer)klass, (GDestroyNotify)0);
  g_signal_connect_swapped(VISU_OBJECT_INSTANCE, "dataRendered",
                           G_CALLBACK(onDataReady), (gpointer)klass);
  g_signal_connect_swapped(VISU_OBJECT_INSTANCE, "dataUnRendered",
                           G_CALLBACK(onDataNotReady), (gpointer)klass);

  my_class = klass;
  klass->disableChangeSignal = FALSE;
}

static void visu_ui_element_combobox_dispose(GObject *obj)
{
  DBG_fprintf(stderr, "VisuUi ElementCombobox: dispose object %p.\n", (gpointer)obj);

  if (VISU_UI_ELEMENT_COMBOBOX(obj)->dispose_has_run)
    return;

  VISU_UI_ELEMENT_COMBOBOX(obj)->dispose_has_run = TRUE;
  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_element_combobox_parent_class)->dispose(obj);
}
static void visu_ui_element_combobox_finalize(GObject *obj)
{
  g_return_if_fail(obj);

  DBG_fprintf(stderr, "VisuUi ElementCombobox: finalize object %p.\n", (gpointer)obj);
  if (VISU_UI_ELEMENT_COMBOBOX(obj)->format)
    g_free(VISU_UI_ELEMENT_COMBOBOX(obj)->format);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_element_combobox_parent_class)->finalize(obj);

  DBG_fprintf(stderr, " | freeing ... OK.\n");
}


static void visu_ui_element_combobox_init(VisuUiElementCombobox *elementComboBox)
{
  DBG_fprintf(stderr, "VisuUi ElementCombobox: initializing new object (%p).\n",
	      (gpointer)elementComboBox);

  elementComboBox->hasAllSelector    = FALSE;
  elementComboBox->hasNoneSelector   = FALSE;
  elementComboBox->showUnPhysical    = FALSE;
  elementComboBox->format            = g_strdup("%s");
  elementComboBox->dispose_has_run   = FALSE;
  elementComboBox->previousSelection = (gpointer)0;

  g_signal_connect_swapped(VISU_OBJECT_INSTANCE, "dataRendered",
                           G_CALLBACK(onDataReadySelection),
                           (gpointer)elementComboBox);
}

/**
 * visu_ui_element_combobox_new:
 * @hasAllSelector: a boolean.
 * @hasNoneSelector: a boolean.
 * @format: (allow-none): a string (can be NULL).
 *
 * Creates a #GtkComboBox with a list of available #VisuElement. This
 * list can contain in addition a "all" value if @hasAllSelector is
 * TRUE, or a "None" value if @hasNoneSelector is TRUE. The @format
 * parameter is used to specify the text for each row of the
 * #GtkComboBox. If @formt is NULL, just the name of the element is printed.
 * 
 * Since: 3.6
 *
 * Returns: (transfer full): a newly created widget.
 */
GtkWidget* visu_ui_element_combobox_new(gboolean hasAllSelector, gboolean hasNoneSelector,
                               const gchar *format)
{
  VisuUiElementCombobox *wd;
  GObjectClass *klass;
  GtkCellRenderer *renderer;

  DBG_fprintf(stderr, "VisuUi ElementCombobox: creating new object with format: '%s'.\n",
	      format);
  wd = VISU_UI_ELEMENT_COMBOBOX(g_object_new(visu_ui_element_combobox_get_type (), NULL));
  wd->hasAllSelector  = hasAllSelector;
  wd->hasNoneSelector = hasNoneSelector;
  if (format)
    {
      g_free(wd->format);
      wd->format = g_strdup(format);
    }

  DBG_fprintf(stderr, "VisuUi ElementCombobox: build widgets.\n");
  klass = G_OBJECT_GET_CLASS(wd);
  wd->filter = gtk_tree_model_filter_new
    (GTK_TREE_MODEL(VISU_UI_ELEMENT_COMBOBOX_CLASS(klass)->storedElements), (GtkTreePath*)0);
  gtk_combo_box_set_model(GTK_COMBO_BOX(wd), wd->filter);
  g_object_unref(wd->filter);
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(wd->filter),
                                         showLabel, wd, (GDestroyNotify)0);
  gtk_tree_model_filter_refilter(GTK_TREE_MODEL_FILTER(wd->filter));
  DBG_fprintf(stderr, "VisuUi ElementCombobox: use filter %p.\n", (gpointer)wd->filter);

  renderer = gtk_cell_renderer_text_new();
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(wd), renderer, TRUE);
  gtk_cell_layout_set_cell_data_func(GTK_CELL_LAYOUT(wd),
                                     renderer, printLabel,
                                     (gpointer)wd, (GDestroyNotify)0);

  wd->onChanged = g_signal_connect(G_OBJECT(wd), "changed",
                                   G_CALLBACK(onChanged), (gpointer)wd);
  gtk_combo_box_set_active(GTK_COMBO_BOX(wd), (hasAllSelector)?1:0);

  return GTK_WIDGET(wd);
}

static gboolean onElementNewHook(GSignalInvocationHint *ihint _U_, guint nvalues _U_,
                                 const GValue *param_values, gpointer data)
{
  GtkTreeIter iter;
  VisuElement *ele;

  DBG_fprintf(stderr, "VisuUi ElementCombobox: found a new element.\n");
  ele = VISU_ELEMENT(g_value_get_object(param_values));
  addElementToModel(&iter, VISU_UI_ELEMENT_COMBOBOX_CLASS(data), (gpointer)ele);
  return TRUE;
}

static void onChanged(GtkComboBox *widget, gpointer user_data _U_)
{
  GtkTreeIter iter;
  gpointer *data;
  GList *lst;

  if (VISU_UI_ELEMENT_COMBOBOX_GET_CLASS(widget)->disableChangeSignal)
    return;

  if (!gtk_combo_box_get_active_iter(widget, &iter))
    return;

  DBG_fprintf(stderr, "VisuUi ElementCombobox: internal combobox changed signal.\n");
  gtk_tree_model_get(VISU_UI_ELEMENT_COMBOBOX(widget)->filter, &iter,
                     COLUMN_ELEMENT_POINTER_TO, &data, -1);

  if (data != VISU_UI_ELEMENT_COMBOBOX(widget)->previousSelection)
    {
      VISU_UI_ELEMENT_COMBOBOX(widget)->previousSelection = data;

      lst = visu_ui_element_combobox_getSelection(VISU_UI_ELEMENT_COMBOBOX(widget));
      DBG_fprintf(stderr, "VisuUi ElementCombobox: emitting 'element-selected'"
                  " signal with list %p.\n", (gpointer)lst);
      g_signal_emit(G_OBJECT(widget),
		    visu_ui_element_combobox_signals[ELEMENT_SELECTED_SIGNAL],
                    0, (gpointer)lst, NULL);
      DBG_fprintf(stderr, " | free list\n");
      if (lst)
        g_list_free(lst);
      DBG_fprintf(stderr, " | OK\n");
    }
  else
    DBG_fprintf(stderr, "VisuUi ElementCombobox: aborting 'element-selected' signal.\n");
}

static void addElementToModel(GtkTreeIter *iter, VisuUiElementComboboxClass* klass,
                              gpointer element)
{
  gboolean visible;

  g_return_if_fail(iter && klass);

  DBG_fprintf(stderr, "VisuUi ElementCombobox: appending a new element '%p'.\n",
	      element);
  visible = (element && GPOINTER_TO_INT(element) != 1)?FALSE:TRUE;
  gtk_list_store_append(klass->storedElements, iter);
  gtk_list_store_set(klass->storedElements, iter,
		     COLUMN_ELEMENT_VISIBLE    , visible,
		     COLUMN_ELEMENT_POINTER_TO , element,
		     -1);
}

static void printLabel(GtkCellLayout *layout _U_, GtkCellRenderer *cell,
                       GtkTreeModel *model, GtkTreeIter *iter, gpointer data)
{
  gpointer pt;
  gchar *str;

  gtk_tree_model_get(model, iter, COLUMN_ELEMENT_POINTER_TO, &pt, -1);
  if (!pt)
    g_object_set(G_OBJECT(cell), "text", _("None"), NULL);
  else if (GPOINTER_TO_INT(pt) == 1)
    g_object_set(G_OBJECT(cell), "text", _("All elements"), NULL);
  else
    {
      str = g_strdup_printf(VISU_UI_ELEMENT_COMBOBOX(data)->format, ((VisuElement*)pt)->name);
      g_object_set(G_OBJECT(cell), "text", str, NULL);
      g_free(str);
    }
}

static gboolean showLabel(GtkTreeModel *model, GtkTreeIter *iter, gpointer data)
{
  gpointer pt;
  gboolean visible;

  gtk_tree_model_get(model, iter, COLUMN_ELEMENT_POINTER_TO, &pt,
                     COLUMN_ELEMENT_VISIBLE, &visible, -1);
  DBG_fprintf(stderr, "VisuUi ElementCombobox: filter entry '%s' %p for model %p -> %d.\n",
              (GPOINTER_TO_INT(pt) != 0 &&
               GPOINTER_TO_INT(pt) != 1)?VISU_ELEMENT(pt)->name:"None", (gpointer)pt,
              (gpointer)model, (!pt && VISU_UI_ELEMENT_COMBOBOX(data)->hasNoneSelector) ||
              (pt && GPOINTER_TO_INT(pt) == 1 && VISU_UI_ELEMENT_COMBOBOX(data)->hasAllSelector) ||
              (pt && GPOINTER_TO_INT(pt) != 1 && visible));
  return (!pt && VISU_UI_ELEMENT_COMBOBOX(data)->hasNoneSelector) ||
    (pt && GPOINTER_TO_INT(pt) == 1 && VISU_UI_ELEMENT_COMBOBOX(data)->hasAllSelector) ||
    (pt && GPOINTER_TO_INT(pt) != 1 &&
     visible && (VISU_UI_ELEMENT_COMBOBOX(data)->showUnPhysical ||
                 visu_element_getPhysical(VISU_ELEMENT(pt))));
}

/**
 * visu_ui_element_combobox_setSelection:
 * @wd: a #VisuUiElementCombobox widget.
 * @name: a string.
 *
 * Select a #VisuElement by providing its name.
 *
 * Since: 3.6
 *
 * Returns: TRUE if the given element exists.
 */
gboolean visu_ui_element_combobox_setSelection(VisuUiElementCombobox* wd, const gchar *name)
{
  GtkTreeIter iter;
  gboolean valid;
  gpointer *pt;

  g_return_val_if_fail(VISU_UI_IS_ELEMENT_COMBOBOX(wd) && name, FALSE);

  DBG_fprintf(stderr, "VisuUi ElementCombobox: select a new element '%s'.\n",
              name);

  for (valid = gtk_tree_model_get_iter_first(wd->filter, &iter);
       valid; valid = gtk_tree_model_iter_next(wd->filter, &iter))
    {
      gtk_tree_model_get(wd->filter, &iter, COLUMN_ELEMENT_POINTER_TO, &pt, -1);
      if (pt && GPOINTER_TO_INT(pt) != 1 && !strcmp(((VisuElement*)pt)->name, name))
	{
	  gtk_combo_box_set_active_iter(GTK_COMBO_BOX(wd), &iter);
	  return TRUE;
	}
    }
  return FALSE;
}
/**
 * visu_ui_element_combobox_getSelection:
 * @wd: a #VisuUiElementCombobox widget.
 *
 * Provide a list of selected elements.
 *
 * Since: 3.6
 *
 * Returns: (transfer container) (element-type VisuElement*): a newly
 * created list of #VisuElement. It should be freed later with
 * g_list_free().
 */
GList* visu_ui_element_combobox_getSelection(VisuUiElementCombobox *wd)
{
  GtkTreeIter iter;
  gpointer *data;
  GList *lst;
  gboolean valid;

  g_return_val_if_fail(VISU_UI_IS_ELEMENT_COMBOBOX(wd), (GList*)0);

  if (!gtk_combo_box_get_active_iter(GTK_COMBO_BOX(wd), &iter))
    return (GList*)0;

  gtk_tree_model_get(wd->filter, &iter,
                     COLUMN_ELEMENT_POINTER_TO, &data, -1);

  lst = (GList*)0;
  if (GPOINTER_TO_INT(data) == 1)
    for (valid = gtk_tree_model_get_iter_first(wd->filter, &iter); valid;
         valid = gtk_tree_model_iter_next(wd->filter, &iter))
      {
        gtk_tree_model_get(wd->filter, &iter,
                           COLUMN_ELEMENT_POINTER_TO, &data, -1);
        if (data && GPOINTER_TO_INT(data) != 1 &&
            visu_element_getPhysical(VISU_ELEMENT(data)))
          lst = g_list_prepend(lst, data);
      }
  else if (data)
    lst = g_list_prepend(lst, data);

  DBG_fprintf(stderr, "VisuUi ElementCombobox: return a list of %d elements.\n",
              g_list_length(lst));

  return lst;
}

static void refreshCombo(VisuUiElementComboboxClass *klass, VisuNodeArray *array)
{
  gboolean valid;
  GtkTreeIter child;
  GtkTreeModel *model;
  gpointer pt;

  klass->disableChangeSignal = TRUE;

  model = GTK_TREE_MODEL(klass->storedElements);
  for (valid = gtk_tree_model_get_iter_first(model, &child); valid;
       valid = gtk_tree_model_iter_next(model, &child))
    {
      gtk_tree_model_get(model, &child, COLUMN_ELEMENT_POINTER_TO, &pt, -1);
      if (pt && GPOINTER_TO_INT(pt) != 1 && array &&
          visu_node_array_getElementId(array, VISU_ELEMENT(pt)) >= 0)
        gtk_list_store_set(klass->storedElements, &child,
                           COLUMN_ELEMENT_VISIBLE, TRUE, -1);
      else
        gtk_list_store_set(klass->storedElements, &child,
                           COLUMN_ELEMENT_VISIBLE, FALSE, -1);
    }

  klass->disableChangeSignal = FALSE;
}
static void onDataReady(VisuUiElementComboboxClass *klass, VisuData *data,
                        VisuGlView *view _U_, gpointer user_data _U_)
{  
  DBG_fprintf(stderr, "VisuUi ElementCombobox: set visible elements from data %p.\n",
              (gpointer)data);

  if (data)
    refreshCombo(klass, VISU_NODE_ARRAY(data));
  else
  refreshCombo(klass, (VisuNodeArray*)0);
  if (data)
    klass->popDef_signal =
      g_signal_connect(G_OBJECT(data), "PopulationDefined",
                       G_CALLBACK(onPopulationDefined), (gpointer)klass);
}
static void onDataNotReady(VisuUiElementComboboxClass *klass, VisuData *data,
                           VisuGlView *view _U_, gpointer user_data _U_)
{  
  DBG_fprintf(stderr, "VisuUi ElementCombobox: unset visible elements from data %p.\n",
              (gpointer)data);
  g_signal_handler_disconnect(G_OBJECT(data), klass->popDef_signal);
}
static void onPopulationDefined(VisuNodeArray *array, guint nEle _U_, gpointer data)
{
  DBG_fprintf(stderr, "VisuUi ElementCombobox: refresh list on population defined.\n");
  refreshCombo(VISU_UI_ELEMENT_COMBOBOX_CLASS(data), array);
}

static void onDataReadySelection(VisuUiElementCombobox *combo, VisuData *data,
                                 VisuGlView *view _U_, gpointer user_data _U_)
{
  if (!data)
    return;

  if (gtk_combo_box_get_active(GTK_COMBO_BOX(combo)) < 0)
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo),
                             MIN((combo->hasAllSelector)?1:0,
                                 gtk_tree_model_iter_n_children(combo->filter,
                                                                (GtkTreeIter*)0) - 1));
}
/**
 * visu_ui_element_combobox_setUnphysicalStatus:
 * @wd: a #VisuUiElementCombobox object ;
 * @status: a boolean
 *
 * If @status is TRUE, the combobox will also show elements that are
 * tagged unphysical, see visu_element_getPhysical().
 *
 * Since: 3.7
 */
void visu_ui_element_combobox_setUnphysicalStatus(VisuUiElementCombobox* wd,
                                                  gboolean status)
{
  g_return_if_fail(VISU_UI_IS_ELEMENT_COMBOBOX(wd));

  wd->showUnPhysical = status;
}
