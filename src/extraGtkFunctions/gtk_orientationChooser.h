/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef GTK_ORIENTATIONCHOOSER_H
#define GTK_ORIENTATIONCHOOSER_H

#include <glib.h>

#include <visu_data.h>

G_BEGIN_DECLS
/**
 * VISU_UI_TYPE_ORIENTATION_CHOOSER:
 *
 * Return the associated #GType to the VisuUiOrientationChooser objects.
 */
#define VISU_UI_TYPE_ORIENTATION_CHOOSER         (visu_ui_orientation_chooser_get_type ())
/**
 * VISU_UI_ORIENTATION_CHOOSER:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuUiOrientationChooser object.
 */
#define VISU_UI_ORIENTATION_CHOOSER(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_UI_TYPE_ORIENTATION_CHOOSER, VisuUiOrientationChooser))
/**
 * VISU_UI_ORIENTATION_CHOOSER_CLASS:
 * @klass: the class to cast.
 *
 * Cast the given class to a #VisuUiOrientationChooserClass object.
 */
#define VISU_UI_ORIENTATION_CHOOSER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_UI_TYPE_ORIENTATION_CHOOSER, VisuUiOrientationChooserClass))
/**
 * VISU_UI_IS_ORIENTATION_CHOOSER:
 * @obj: the object to test.
 *
 * Return if the given object is a valid #VisuUiOrientationChooser object.
 */
#define VISU_UI_IS_ORIENTATION_CHOOSER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_UI_TYPE_ORIENTATION_CHOOSER))
/**
 * VISU_UI_IS_ORIENTATION_CHOOSER_CLASS:
 * @klass: the class to test.
 *
 * Return if the given class is a valid #VisuUiOrientationChooserClass class.
 */
#define VISU_UI_IS_ORIENTATION_CHOOSER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_UI_TYPE_ORIENTATION_CHOOSER))

typedef struct _VisuUiOrientationChooser VisuUiOrientationChooser;
typedef struct _VisuUiOrientationChooserClass VisuUiOrientationChooserClass;

/**
 * VisuUiOrientationChooserKind:
 * @VISU_UI_ORIENTATION_DIRECTION: this flag specifies that the box coordinates
 *                                are used to identity a direction ;
 * @VISU_UI_ORIENTATION_NORMAL: this flag specifies that the box coordinates
 *                             are used to identify a normal direction.
 *
 * These values are used when creating an #VisuUiOrientationChooser, to specify the
 * behavior of the box coordinates. This is due to the fact that the box coordinates
 * are not always orthoggonal.
 */
typedef enum
  {
    VISU_UI_ORIENTATION_DIRECTION,
    VISU_UI_ORIENTATION_NORMAL
  } VisuUiOrientationChooserKind;

GType          visu_ui_orientation_chooser_get_type        (void);

GtkWidget* visu_ui_orientation_chooser_new(VisuUiOrientationChooserKind kind,
                                           gboolean liveUpdate,
                                           VisuData *data, GtkWindow *parent);
void visu_ui_orientation_chooser_setOrthoValues(VisuUiOrientationChooser *orientation,
				       float values[3]);
void visu_ui_orientation_chooser_setBoxValues(VisuUiOrientationChooser *orientation,
				     float values[3]);
void visu_ui_orientation_chooser_setAnglesValues(VisuUiOrientationChooser *orientation,
					float values[2]);
void visu_ui_orientation_chooser_getOrthoValues(VisuUiOrientationChooser *orientation,
				       float values[3]);
void visu_ui_orientation_chooser_getBoxValues(VisuUiOrientationChooser *orientation,
				     float values[3]);
void visu_ui_orientation_chooser_getAnglesValues(VisuUiOrientationChooser *orientation,
					float values[2]);

G_END_DECLS

#endif
