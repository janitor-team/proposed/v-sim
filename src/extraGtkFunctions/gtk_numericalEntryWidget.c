/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include <gtk/gtk.h>

#include "gtk_numericalEntryWidget.h"

/**
 * SECTION:gtk_numericalEntryWidget
 * @short_description: Defines a widget to enter numerical values without
 * any boundary or precision constrains.
 * 
 * <para>This widget is based on the #GtkEntry widget but behaves more
 * like a #GtkSpinButton is fact. It is designed to enter numerical
 * values, but without any boundary or precision constrains. One can
 * use either plain or scientific notations.</para>
 */

enum {
  VALUE_CHANGED_SIGNAL,
  LAST_SIGNAL
};

static guint visu_ui_numerical_entry_signals[LAST_SIGNAL] = { 0 };

#define VISU_UI_NUMERICAL_ENTRY_FORMAT_DEFAULT "%g"

struct _VisuUiNumericalEntry
{
  GtkEntry entry;

  double value;
  double printed_value;
  gchar *format;
/*   VisuUiNumericalEntryPrivate *private; */

  gboolean dispose_has_run;
};

struct _VisuUiNumericalEntryClass
{
  GtkEntryClass parent_class;

  void (*numericalEntry) (VisuUiNumericalEntry *numEntry);

  gulong valueChangedSignalId;
};

static void visu_ui_numerical_entry_dispose (GObject *obj);
static void visu_ui_numerical_entry_finalize(GObject *obj);

G_DEFINE_TYPE(VisuUiNumericalEntry, visu_ui_numerical_entry, GTK_TYPE_ENTRY)

/* Local methods. */
void printStoredValue(VisuUiNumericalEntry* numericalEntry);
gboolean parsePrintedValue(VisuUiNumericalEntry *numericalEntry, double *value);
void onEntryValueChange(VisuUiNumericalEntry *numericalEntry, gpointer data);
gboolean onEntryLeaveFocus(VisuUiNumericalEntry *numericalEntry,
			   GdkEventFocus *event, gpointer data);
gboolean onEntryGetFocus(VisuUiNumericalEntry *numericalEntry,
			 GdkEventFocus *event, gpointer data);

static void visu_ui_numerical_entry_class_init(VisuUiNumericalEntryClass *klass)
{
  DBG_fprintf(stderr, "Gtk VisuUiNumericalEntry : creating the class of the widget.\n");

  DBG_fprintf(stderr, "                     - adding new signals ;\n");
  /**
   * VisuUiNumericalEntry::value-changed:
   * @entry: the #VisuUiNumericalEntry that emits the signal ;
   * @oldValue: the previous value.
   *
   * This signal is emitted when a new valid numerical value is entered.
   *
   * Since: 3.1
   */
  visu_ui_numerical_entry_signals[VALUE_CHANGED_SIGNAL] =
    g_signal_new ("value-changed",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
		  G_STRUCT_OFFSET (VisuUiNumericalEntryClass, numericalEntry),
		  NULL, 
		  NULL,                
		  g_cclosure_marshal_VOID__DOUBLE,
		  G_TYPE_NONE, 1, G_TYPE_DOUBLE);

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose = visu_ui_numerical_entry_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_ui_numerical_entry_finalize;
}

static void visu_ui_numerical_entry_dispose(GObject *obj)
{
  DBG_fprintf(stderr, "Gtk VisuUiNumericalEntry: dispose object %p.\n", (gpointer)obj);

  if (VISU_UI_NUMERICAL_ENTRY(obj)->dispose_has_run)
    return;

  VISU_UI_NUMERICAL_ENTRY(obj)->dispose_has_run = TRUE;
  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_numerical_entry_parent_class)->dispose(obj);
}
static void visu_ui_numerical_entry_finalize(GObject *obj)
{
  VisuUiNumericalEntry *entry;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Gtk VisuUiNumericalEntry: finalize object %p.\n", (gpointer)obj);

  entry = VISU_UI_NUMERICAL_ENTRY(obj);
  g_free(entry->format);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_numerical_entry_parent_class)->finalize(obj);

  DBG_fprintf(stderr, "Gtk VisuUiNumericalEntry: freeing ... OK.\n");
}

static void visu_ui_numerical_entry_init(VisuUiNumericalEntry *numericalEntry)
{
  DBG_fprintf(stderr, "Gtk VisuUiNumericalEntry : initializing new object (%p).\n",
	      (gpointer)numericalEntry);

  numericalEntry->dispose_has_run = FALSE;
  numericalEntry->format = g_strdup(VISU_UI_NUMERICAL_ENTRY_FORMAT_DEFAULT);

  g_signal_connect_swapped(G_OBJECT(numericalEntry), "activate",
			   G_CALLBACK(onEntryValueChange), (gpointer)numericalEntry);
  g_signal_connect_swapped(G_OBJECT(numericalEntry), "focus-out-event",
			   G_CALLBACK(onEntryLeaveFocus), (gpointer)numericalEntry);
  g_signal_connect_swapped(G_OBJECT(numericalEntry), "focus-in-event",
			   G_CALLBACK(onEntryGetFocus), (gpointer)numericalEntry);
}

/**
 * visu_ui_numerical_entry_new :
 * @value: the initial value.
 *
 * A #VisuUiNumericalEntry widget is like a #GtkEntry widget, but it only accepts
 * double precision values (written in plain format, e.g. 1.23456, or scientific
 * notation, e.g. 1.2345e6). The widget can't be blank and there is always
 * a value printed in it. If the user erase the current value or enter something
 * that is not a recognised double precision value, the widget returns to its previous
 * valid value.
 *
 * Returns: a newly created #VisuUiNumericalEntry widget.
 */
GtkWidget* visu_ui_numerical_entry_new(double value)
{
  VisuUiNumericalEntry *numericalEntry;

  DBG_fprintf(stderr, "Gtk VisuUiNumericalEntry : creating new object with value : %g.\n",
	      value);

  numericalEntry = VISU_UI_NUMERICAL_ENTRY(g_object_new(visu_ui_numerical_entry_get_type(), NULL));
  numericalEntry->value = value;
  
  printStoredValue(numericalEntry);

  return GTK_WIDGET(numericalEntry);
}

void printStoredValue(VisuUiNumericalEntry* numericalEntry)
{
  gchar *str;

  g_return_if_fail(VISU_UI_IS_NUMERICAL_ENTRY(numericalEntry));

  str = g_strdup_printf(numericalEntry->format, numericalEntry->value);
  gtk_entry_set_text(GTK_ENTRY(numericalEntry), str);
  g_free(str);

  if (!parsePrintedValue(numericalEntry, &numericalEntry->printed_value))
    numericalEntry->printed_value = G_MAXFLOAT;
}
gboolean parsePrintedValue(VisuUiNumericalEntry *numericalEntry, double *value)
{
  double valueDouble;
  gchar *last;

  g_return_val_if_fail(VISU_UI_IS_NUMERICAL_ENTRY(numericalEntry) && value, FALSE);
  
  valueDouble = g_ascii_strtod(gtk_entry_get_text(GTK_ENTRY(numericalEntry)),
			  &last);
  if (*last != '\0')
    {
      /* Wrong number. */
      g_warning("Can't read a number from '%s'.\n",
		gtk_entry_get_text(GTK_ENTRY(numericalEntry)));
      return FALSE;
    }

  *value = valueDouble;
  return TRUE;
}

/**
 * visu_ui_numerical_entry_setValue:
 * @numericalEntry: a #VisuUiNumericalEntry widget ;
 * @value: a double precision value.
 *
 * Use this method to set the value for the given #numericalEntry widget.
 */
void visu_ui_numerical_entry_setValue(VisuUiNumericalEntry* numericalEntry, double value)
{
  double valueOld;

  g_return_if_fail(VISU_UI_IS_NUMERICAL_ENTRY(numericalEntry));

  if (value == numericalEntry->value)
    return;

  valueOld = numericalEntry->value;
  numericalEntry->value = value;

  printStoredValue(numericalEntry);

  DBG_fprintf(stderr, "Gtk VisuUiNumericalEntry : emitting 'value-changed' signal.\n");
  g_signal_emit(G_OBJECT(numericalEntry),
		visu_ui_numerical_entry_signals[VALUE_CHANGED_SIGNAL], 0, valueOld, NULL);
}
/**
 * visu_ui_numerical_entry_getValue:
 * @numericalEntry: a #VisuUiNumericalEntry widget.
 *
 * You can get the value contained in the given @numericalEntry using this method.
 *
 * Returns: the double precision value printed in the #VisuUiNumericalEntry.
 */
double visu_ui_numerical_entry_getValue(VisuUiNumericalEntry *numericalEntry)
{
  g_return_val_if_fail(VISU_UI_IS_NUMERICAL_ENTRY(numericalEntry), 0.);

  return numericalEntry->value;
}

void onEntryValueChange(VisuUiNumericalEntry *numericalEntry, gpointer data _U_)
{
  gboolean valid;
  double valueDouble;

  valid = parsePrintedValue(numericalEntry, &valueDouble);

  if (!valid)
    printStoredValue(numericalEntry);
  else
    {
      if (valueDouble != numericalEntry->printed_value)
        visu_ui_numerical_entry_setValue(numericalEntry, valueDouble);
    }
}
gboolean onEntryLeaveFocus(VisuUiNumericalEntry *numericalEntry,
			   GdkEventFocus *event _U_, gpointer data _U_)
{
  onEntryValueChange(numericalEntry, (gpointer)0);
  return FALSE;
}
gboolean onEntryGetFocus(VisuUiNumericalEntry *numericalEntry _U_,
			 GdkEventFocus *event _U_, gpointer data _U_)
{
  /* gtk_editable_select_region(GTK_EDITABLE(numericalEntry), 0, -1); */
  return FALSE;
}
