/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include <gtk/gtk.h>
#include <math.h>
#include <string.h>

#include "gtk_curveWidget.h"
#include <support.h>
#include <visu_tools.h>
#include <visu_elements.h>
#include <visu_pairs.h>
#include <coreTools/toolMatrix.h>

/**
 * SECTION: gtk_curveWidget
 * @short_description: A specialised curve widget to draw distance
 * distribution for pairs.
 *
 * <para>This is a psecialised widget to display g(r)
 * information.</para>
 *
 * Since: 3.6
 */

/**
 * VisuUiCurveFrame:
 *
 * An opaque structure defining a #VisuUiCurveFrame widget.
 *
 * Since: 3.6
 */
struct _VisuUiCurveFrame
{
  GtkDrawingArea parent;

  gboolean dispose_has_run;
  
  ToolMatrixScalingFlag scale;
  float dists[2];
  float heights[2];
  float hlRange[2];

  VisuUiCurveFrameStyle style;
  gchar *filter;
  GHashTable *data;
  guint nSteps;
  float init, step;
};

/**
 * VisuUiCurveFrameClass:
 *
 * An opaque structure defining the class of a #VisuUiCurveFrame widget.
 *
 * Since: 3.6
 */
struct _VisuUiCurveFrameClass
{
  GtkDrawingAreaClass parent_class;
};

#define SPACER "\302\240-\302\240"
#define ALL    _("All")

/**
 * visu_ui_curve_frame_get_type:
 *
 * Internal routine to get #VISU_UI_TYPE_CURVE_FRAME value.
 *
 * Since: 3.6
 */
G_DEFINE_TYPE (VisuUiCurveFrame, visu_ui_curve_frame, GTK_TYPE_DRAWING_AREA)

#if GTK_MAJOR_VERSION < 3 && GTK_MINOR_VERSION < 26
static gboolean visu_ui_curve_frame_expose(GtkWidget *curve, GdkEventExpose *event);
#endif
static gboolean visu_ui_curve_frame_drawCairo(GtkWidget *curve, cairo_t *cr);
static void visu_ui_curve_frame_dispose(GObject *obj);
static void visu_ui_curve_frame_finalize(GObject *obj);
static void redraw(GtkWidget *widget);
static void hashDraw(gpointer key, gpointer value, gpointer usre_data);
static void drawData(VisuUiCurveFrame *curve, cairo_t *cr, const gchar *name,
		     const guint *data, double w, double h, double dw,
		     VisuUiCurveFrameStyle style);

static void visu_ui_curve_frame_class_init (VisuUiCurveFrameClass *class)
{
  GtkWidgetClass *widget_class;

  widget_class = GTK_WIDGET_CLASS (class);

#if GTK_MAJOR_VERSION < 3 && GTK_MINOR_VERSION < 26
  widget_class->expose_event = visu_ui_curve_frame_expose;
#else
  widget_class->draw = visu_ui_curve_frame_drawCairo;
#endif
  G_OBJECT_CLASS(class)->dispose  = visu_ui_curve_frame_dispose;
  G_OBJECT_CLASS(class)->finalize = visu_ui_curve_frame_finalize;
}

static void visu_ui_curve_frame_init(VisuUiCurveFrame *curve)
{
  curve->dispose_has_run = FALSE;

  curve->style = CURVE_LINEAR;
  curve->scale = TOOL_MATRIX_SCALING_LOG;

  curve->heights[0] = 0.;
  curve->heights[1] = 5.;

  curve->hlRange[0] = -1.f;

  curve->filter = (gchar*)0;
  curve->data = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
}

static void visu_ui_curve_frame_dispose(GObject *obj)
{
  DBG_fprintf(stderr, "Gtk Curve: dispose object %p.\n", (gpointer)obj);

  if (VISU_UI_CURVE_FRAME(obj)->dispose_has_run)
    return;

  VISU_UI_CURVE_FRAME(obj)->dispose_has_run = TRUE;
  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_curve_frame_parent_class)->dispose(obj);
}
static void visu_ui_curve_frame_finalize(GObject *obj)
{
  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Gtk Curve: finalize object %p.\n", (gpointer)obj);

  g_hash_table_destroy(VISU_UI_CURVE_FRAME(obj)->data);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_curve_frame_parent_class)->finalize(obj);

  DBG_fprintf(stderr, " | freeing ... OK.\n");
}
static void redraw(GtkWidget *widget)
{
  cairo_region_t *region;
  GdkWindow *window;

  if (!gtk_widget_get_mapped(widget)) return;

  window = gtk_widget_get_window(widget);
  region = gdk_window_get_clip_region(GDK_WINDOW(window));
  /* redraw the cairo canvas completely by exposing it */
  gdk_window_invalidate_region(window, region, TRUE);
  gdk_window_process_updates(window, TRUE);

  cairo_region_destroy (region);
}

static double tickDist(float dists[2], int length, ToolMatrixScalingFlag scale)
{
#define TICK_SPACE 50.f
  float s, l, n;

  n = (float)length / TICK_SPACE;
  s = (dists[1] - dists[0]) / n;
  l = log10(s);
  l = (int)((l < 0.f)?l - 1.f:l);
  s = s / exp(l * log(10));

  if (scale == TOOL_MATRIX_SCALING_LINEAR)
    {
      if (s <= 1.25) n = 1.f;
      else if (s <= 3.75) n = 2.5f;
      else if (s <= 7.5) n = 5.f;
      else s = 10.f;
    }
  else
    {
      if (s <= 5.f) n = 1.f;
      else n = 10.f;
    }
  s = n * exp(l * log(10));

  return (double)s;
}

static gboolean isValidData(const gchar* filter, const gchar *data)
{
  gchar *tmp;
  gboolean res;
  
  if (filter)
    {
      tmp = g_strdup_printf("%s"SPACER, filter);
      res = (!strcmp(data, filter) ||
	     strstr(data, tmp));
      g_free(tmp);
    }
  else
    res = (!strcmp(data, _("All")) ||
	   !strstr(data, SPACER));
  return res;
}

static void setCairoColor(cairo_t *cr, const gchar *filter, const gchar *data)
{
  VisuElement *ele;
  gchar *tmp;
  const gchar *name;

  if (!filter)
    name = data;
  else
    {
      tmp = g_strdup_printf("%s"SPACER, filter);
      name = data + strlen(tmp);
      g_free(tmp);
    }

  ele = visu_element_lookup(name);
  if (ele)
    cairo_set_source_rgb (cr, ele->rgb[0], ele->rgb[1], ele->rgb[2]);
  else
    cairo_set_source_rgb (cr, 0.5, 0.5, 0.5);
}

struct _drawData
{
  VisuUiCurveFrame *curve;
  cairo_t *cr;
  double w;
  double h;
  double dw;
};
static gboolean visu_ui_curve_frame_drawCairo(GtkWidget *wd, cairo_t *cr)
{
#define BUF 3.
  double x, s, f, g, w, h, dw, dh, xp, wl, hl, yp;
  float *dists, *heights;
  gchar val[64];
  cairo_text_extents_t ext;
  struct _drawData  dt;
  guint *data;
  VisuUiCurveFrame *curve;
  GList *eles, *tmpLst;
  GtkAllocation allocation;

  DBG_fprintf(stderr, "Gtk Curve: general redraw.\n");

  curve = VISU_UI_CURVE_FRAME(wd);
  dists = curve->dists;
  heights = curve->heights;
  gtk_widget_get_allocation(wd, &allocation);
        
  cairo_select_font_face(cr, "Sans",
			 CAIRO_FONT_SLANT_NORMAL,
			 CAIRO_FONT_WEIGHT_NORMAL);
  cairo_set_font_size(cr, 10.0);
  cairo_text_extents(cr, "10000.", &ext);
  dw = ext.width + BUF * 2.;
  dh = ext.height + BUF * 2.;
  w = (double)allocation.width - dw;
  h = (double)allocation.height - dh;
  data = (guint*)g_hash_table_lookup(curve->data, (curve->filter)?curve->filter:ALL);

  /* the frame itself. */
  cairo_set_line_width(cr, 1.);
  cairo_rectangle(cr, dw - 0.5, 0.5, w, h);
  cairo_set_source_rgb (cr, 1, 1, 1);
  cairo_fill_preserve (cr);
  cairo_set_source_rgb (cr, 0, 0, 0);
  cairo_stroke (cr);
  cairo_set_line_width(cr, 2.);
  DBG_fprintf(stderr, "Gtk Curve: frame done.\n");

  /* The highlight zone. */
  if (curve->hlRange[0] >= 0.f && data)
    {
      wl = w * CLAMP((curve->hlRange[1] - curve->hlRange[0]) /
		     (dists[1] - dists[0]), 0.f, 1.f);
      cairo_set_source_rgba(cr, 0.05, 0., 1., 0.25);
      cairo_rectangle(cr, dw + w * CLAMP((curve->hlRange[0] - dists[0])/
					 (dists[1] - dists[0]), 0.f, 1.f),
		      0.5, wl, h);
      cairo_fill(cr);
    }

  /* The plots. */
  dt.curve = curve;
  dt.cr    = cr;
  dt.w     = w;
  dt.dw    = dw;
  dt.h     = h;
  if (data && curve->style != CURVE_BAR)
    drawData(curve, cr, (curve->filter)?curve->filter:ALL, data, w, h, dw, CURVE_BAR);
  g_hash_table_foreach(curve->data, hashDraw, &dt);
  if (data && curve->style == CURVE_BAR)
    drawData(curve, cr, (curve->filter)?curve->filter:ALL, data, w, h, dw, CURVE_LINEAR);
  DBG_fprintf(stderr, "Gtk Curve: plots done.\n");

  /* the ticks & the labels */
  cairo_set_source_rgb (cr, 0, 0, 0);
  cairo_set_line_width(cr, 2.);
  f = w / (dists[1] - dists[0]);
  g = 1.f / f;
  s = tickDist(dists, w, TOOL_MATRIX_SCALING_LINEAR) * f;
  for (x = - dists[0] * f; x <= w; x += s)
    if (x >= 0.)
      {
	cairo_move_to(cr, dw + x, 0);
	cairo_line_to(cr, dw + x, 3);
	cairo_move_to(cr, dw + x, h);
	cairo_line_to(cr, dw + x, h - 3);
	cairo_stroke(cr);
	sprintf(val, "%g", x * g + dists[0]);
	cairo_text_extents(cr, val, &ext);
	cairo_move_to(cr, dw + MIN(MAX(0., x - ext.width/2 - ext.x_bearing),
				   w - ext.width - ext.x_bearing),
		      allocation.height - ext.height - ext.y_bearing - BUF);
	cairo_show_text(cr, val);
      }
  if (curve->scale == TOOL_MATRIX_SCALING_LOG)
    f = h / log(heights[1] + 0.5f);
  else
    {
      f = h / heights[1];
      s = tickDist(heights, h, curve->scale);
    }
  for (x = (curve->scale == TOOL_MATRIX_SCALING_LOG)?1.:0.; x <= heights[1];
       x = (curve->scale == TOOL_MATRIX_SCALING_LOG)?x * 10:x + s)
    {
      if (curve->scale == TOOL_MATRIX_SCALING_LOG)
	{
	  cairo_set_line_width(cr, 1.);
	  for (xp = x * 2.; xp < x * 10.; xp += x)
	    {
	      yp = h - log(xp + 0.5f) * f;
	      cairo_move_to(cr, dw + 0, yp);
	      cairo_line_to(cr, dw + 1.5, yp);
	      cairo_move_to(cr, allocation.width, yp);
	      cairo_line_to(cr, allocation.width - 2.5, yp);
	      cairo_stroke(cr);
	    }
	  xp = h - log(x + 0.5f) * f;
	}
      else
	xp = h - x * f;
      cairo_set_line_width(cr, 2.);
      cairo_move_to(cr, dw + 0, xp);
      cairo_line_to(cr, dw + 3, xp);
      cairo_move_to(cr, allocation.width, xp);
      cairo_line_to(cr, allocation.width - 4, xp);
      cairo_stroke(cr);
      sprintf(val, "%g", x);
      cairo_text_extents(cr, val, &ext);
      cairo_move_to(cr, dw - BUF - ext.width - ext.x_bearing,
		    MIN(MAX(- ext.y_bearing,
			    xp - ext.height/2 - ext.y_bearing), h));
      cairo_show_text(cr, val);
    }
  DBG_fprintf(stderr, "Gtk Curve: ticks done.\n");

  /* The legend. */
  eles = g_hash_table_get_keys(curve->data);
  if (eles)
    {
      DBG_fprintf(stderr, "Gtk Curve: rebuild the legend (%d).\n",
		  g_list_length(eles));
      wl = 0;
      s  = 0;
      hl = BUF;
      for (tmpLst = eles; tmpLst; tmpLst = g_list_next(tmpLst))
	if (isValidData(curve->filter, (gchar*)tmpLst->data))
	  {
	    cairo_text_extents(cr, (gchar*)tmpLst->data, &ext);
	    DBG_fprintf(stderr, " | '%s' -> %fx%fx%f\n", (gchar*)tmpLst->data,
			ext.width, ext.height, ext.y_bearing);
	    wl = MAX(wl, ext.width);
	    s  = MAX(s, -ext.y_bearing - 2.);
	    hl += BUF - ext.y_bearing;
	  }
      wl += 2 * BUF + BUF + s;
      hl += BUF;
      cairo_set_line_width(cr, 1.);
      cairo_rectangle(cr, dw + 3 + BUF + 0.5, 0.5 + 3 + BUF, wl, hl);
      cairo_set_source_rgb(cr, 0.67, 0.67, 0.67);
      cairo_fill_preserve (cr);
      cairo_set_source_rgb (cr, 0, 0, 0);
      cairo_stroke (cr);
      hl = BUF;
      for (tmpLst = eles; tmpLst; tmpLst = g_list_next(tmpLst))
	if (isValidData(curve->filter, (gchar*)tmpLst->data))
	  {
	    cairo_text_extents(cr, (gchar*)tmpLst->data, &ext);
	    hl -= ext.y_bearing;
	    cairo_move_to(cr, dw + 3 + 3 * BUF + 0.5 + ext.x_bearing + s,
			  0.5 + 3 + BUF + hl);
	    cairo_set_source_rgb(cr, 0, 0, 0);
	    cairo_show_text(cr, (gchar*)tmpLst->data);
	    cairo_arc(cr, dw + 3 + 2 * BUF + 0.5 + s / 2.,
		      0.5 + 3 + BUF + hl - s / 2., s / 2., 0, 2 * G_PI);
	    setCairoColor(cr, curve->filter, (gchar*)tmpLst->data);
	    cairo_fill(cr);
	    hl += BUF/*  + ext.y_bearing + ext.height */;
	  }
      g_list_free(eles);
    }
  DBG_fprintf(stderr, "Gtk Curve: redraw done.\n");

  return FALSE;
}

static void hashDraw(gpointer key, gpointer value, gpointer user_data)
{
  struct _drawData *dt;
  gchar *tmp;
  gboolean ret;

  if (!strcmp(key, ALL))
    return;
  
  dt = (struct _drawData*)user_data;
  if (!dt->curve->filter)
    {
      if (strstr((gchar*)key, SPACER))
	return;
    }
  else
    {
      tmp = g_strdup_printf("%s"SPACER, dt->curve->filter);
      ret = (!strstr((gchar*)key, tmp));
      g_free(tmp);
      if (ret) return;
    }
  drawData(dt->curve, dt->cr, (gchar*)key, (guint*)value,
	   dt->w, dt->h, dt->dw, dt->curve->style);
}
static void drawData(VisuUiCurveFrame *curve, cairo_t *cr, const gchar *name,
		     const guint *data, double w, double h, double dw,
		     VisuUiCurveFrameStyle style)
{
  float step, fx, x, fy, y, val, init;
  int nSteps, i;
  double bw;
  VisuPairDistribution dd;
  guint startStopId[2], pos;
  cairo_text_extents_t ext;
  gchar str[25];
  
  init = curve->init;
  step = curve->step;
  nSteps = curve->nSteps;

  setCairoColor(cr, curve->filter, name);

  bw = MAX(w * step / (curve->dists[1] - curve->dists[0]) - 1., 0.5);
  if (style == CURVE_BAR)
    cairo_set_line_width(cr, bw);
  else
    cairo_set_line_width(cr, 1.5);

  cairo_move_to(cr, dw, h - 0.5);
  fx = w / (curve->dists[1] - curve->dists[0]);
  if (curve->scale == TOOL_MATRIX_SCALING_LINEAR)
    fy = (h - 1.) / (curve->heights[1] - curve->heights[0]);
  else
    fy = (h - 1.) / log(curve->heights[1] + 0.5);
  for (i = 0; i < nSteps - 1; i++)
    if ((val = (step * i + init - curve->dists[0])) >= 0.)
      {
	if (style != CURVE_BAR || data[i] > 0)
	  {
	    x = dw + val * fx + bw / 2.;
	    if (style == CURVE_BAR)
	      cairo_move_to(cr, x, h - 0.5);
	    if (curve->scale == TOOL_MATRIX_SCALING_LINEAR)
	      cairo_line_to(cr, x, h - 0.5 - data[i] * fy);
	    else
	      cairo_line_to(cr, x, h - 0.5 - log(MAX(data[i] + 0.5, 1)) * fy);
	    if (style == CURVE_BAR)
	      cairo_stroke(cr);
	  }
      }
  if (style != CURVE_BAR)
    cairo_stroke(cr);

  if (style != CURVE_BAR)
    {
      dd.histo = (guint*)data;
      dd.nValues = (guint)nSteps;
      dd.initValue = init;
      dd.stepValue = step;
      if (curve->scale == TOOL_MATRIX_SCALING_LINEAR)
        dd.nNodesEle1 = dd.nNodesEle2 = h / fy / 1.5;
      else
        dd.nNodesEle1 = dd.nNodesEle2 = exp(h / fy / 1.5);
      startStopId[0] = 0;
      startStopId[1] = (guint)(nSteps - 1);
      while (visu_pair_distribution_getNextPick(&dd, startStopId, (guint*)0, (guint*)0, &pos))
        {
          sprintf(str, "%6.3f", init + step * pos);
          val = step * pos + init - curve->dists[0];
          cairo_text_extents(cr, str, &ext);
          x = val * fx + bw / 2. + 1.;
          x = (x + ext.width >= w)?w - ext.width:x;
          y = h - 0.5;
          if (curve->scale == TOOL_MATRIX_SCALING_LINEAR)
            y -= data[pos] * fy;
          else
            y -= log(MAX(data[pos] + 0.5, 1)) * fy;
          y += ext.height / 2.;
          y = MAX(y, ext.height + 1.5);
          cairo_move_to(cr, dw + x, y);
          cairo_set_source_rgb (cr, 0., 0., 0.);
          cairo_show_text(cr, str);
          startStopId[0] = startStopId[1];
          startStopId[1] = (guint)(nSteps - 1);
        }
    }
}

#if GTK_MAJOR_VERSION < 3 && GTK_MINOR_VERSION < 26
static gboolean visu_ui_curve_frame_expose(GtkWidget *curve, GdkEventExpose *event)
{
  cairo_t *cr;
  gboolean res;

  /* get a cairo_t */
  cr = gdk_cairo_create(GDK_DRAWABLE(gtk_widget_get_window(curve)));

  cairo_rectangle (cr,
		   event->area.x, event->area.y,
		   event->area.width, event->area.height);
  cairo_clip (cr);
        
  res = visu_ui_curve_frame_drawCairo(curve, cr);

  cairo_destroy (cr);

  return res;
}
#endif
/**
 * visu_ui_curve_frame_new:
 * @distMin: a float.
 * @distMax: a float (bigger than @distMin).
 *
 * It creates a graph that can display distances distribution for
 * #VisuElement pairing. The display span is given by @distMin and @distMax.
 *
 * Since: 3.6
 *
 * Returns: a newly craeted #VisuUiCurveFrame widget.
 */
GtkWidget *visu_ui_curve_frame_new(float distMin, float distMax)
{
  GtkWidget *obj;

  g_return_val_if_fail(distMin >= 0.f && distMax > distMin, (GtkWidget*)0);

  obj = g_object_new(VISU_UI_TYPE_CURVE_FRAME, NULL);
  g_return_val_if_fail(obj, (GtkWidget*)0);

  visu_ui_curve_frame_init(VISU_UI_CURVE_FRAME(obj));
  VISU_UI_CURVE_FRAME(obj)->dists[0] = distMin;
  VISU_UI_CURVE_FRAME(obj)->dists[1] = distMax;

  return obj;
}
/**
 * visu_ui_curve_frame_setSpan:
 * @curve: a #VisuUiCurveFrame widget.
 * @span: two floats.
 *
 * Changes the distance range that is displayed on the curve.
 *
 * Since: 3.6
 *
 * Returns: TRUE if the distance displayed is actually changed.
 */
gboolean visu_ui_curve_frame_setSpan(VisuUiCurveFrame *curve, float span[2])
{
  gboolean update;

  g_return_val_if_fail(VISU_UI_IS_CURVE_FRAME(curve), FALSE);
  g_return_val_if_fail(span[0] >= 0.f && span[1] > span[0], FALSE);

  update = (curve->dists[0] != span[0] ||
	    curve->dists[1] != span[1]);

  curve->dists[0] = span[0];
  curve->dists[1] = span[1];

  return update;
}
/**
 * visu_ui_curve_frame_getSpan:
 * @curve: a #VisuUiCurveFrame widget.
 * @span: a location for two floats.
 *
 * Retrieves the distances inside which the distribution is displayed.
 *
 * Since: 3.6
 */
void visu_ui_curve_frame_getSpan(VisuUiCurveFrame *curve, float span[2])
{
  g_return_if_fail(VISU_UI_IS_CURVE_FRAME(curve));

  span[0] = curve->dists[0];
  span[1] = curve->dists[1];
}
/**
 * visu_ui_curve_frame_hasData:
 * @curve: a #VisuUiCurveFrame widget.
 *
 * Retrieve if some distance data have been added to the @curve.
 *
 * Since: 3.6
 *
 * Returns: TRUE if the @curve has some data associated.
 */
gboolean visu_ui_curve_frame_hasData(VisuUiCurveFrame *curve)
{
  g_return_val_if_fail(VISU_UI_IS_CURVE_FRAME(curve), FALSE);

  return (g_hash_table_size(curve->data) > 0 && curve->step > 0.f);
}
/**
 * visu_ui_curve_frame_setData:
 * @curve: a #VisuUiCurveFrame widget.
 * @step: the stepping distance.
 * @min: the minimum distance for the distribution.
 * @max: the maximum distance for the distribution.
 *
 * Compute and allocate the required size to store distributions, see
 * visu_ui_curve_frame_addData() to actually setup the distribution values.
 *
 * Since: 3.6
 */
void visu_ui_curve_frame_setData(VisuUiCurveFrame *curve, float step, float min, float max)
{
  guint *storage;

  g_return_if_fail(VISU_UI_IS_CURVE_FRAME(curve));
  g_return_if_fail(min >= 0.f && step >= 0.f && max > min);

  g_hash_table_remove_all(curve->data);

  curve->heights[1] = 5.;
  curve->init = min;
  curve->step = step;
  if (step > 0.f)
    {
      curve->nSteps = (int)((max - min) / step) + 1;
      storage = g_malloc0(sizeof(guint) * curve->nSteps);
      g_hash_table_insert(curve->data, g_strdup(ALL), storage);
    }
  else
    redraw(GTK_WIDGET(curve));
}
/**
 * visu_ui_curve_frame_addData:
 * @curve: a #VisuUiCurveFrame widget.
 * @eleName: a string.
 * @lkName: a string.
 * @data: an array of frequencies.
 * @nSteps: the size of @data.
 * @init: the initial x value for array @data.
 * @step: the step value to increase x for array @data.
 *
 * This routine changes the distribution for element @eleName, with
 * respect to element @lkName. @data is an array that gives the number @data[i]
 * of pairs @eleName - @lkName which distance is in (@init + @step * i).
 * 
 * Since: 3.6
 */
void visu_ui_curve_frame_addData(VisuUiCurveFrame *curve, const gchar *eleName, const gchar *lkName,
			const guint *data, guint nSteps, float init, float step)
{
  guint max, i;
  guint *storage, *all, *ele1, *ele2;
  gchar *link1, *link2;

  g_return_if_fail(VISU_UI_IS_CURVE_FRAME(curve));
  g_return_if_fail(eleName && eleName[0] && lkName && lkName[0] && data);
  g_return_if_fail(curve->nSteps == nSteps &&
		   curve->init == init &&
		   curve->step == step);

  link1 = g_strdup_printf("%s"SPACER"%s", eleName, lkName);
  if (strcmp(eleName, lkName))
    link2 = g_strdup_printf("%s"SPACER"%s", lkName, eleName);
  else
    link2 = (gchar*)0;
  DBG_fprintf(stderr, "Gtk Curve: add data for link '%s'.\n", link1);

  ele1 = (guint*)g_hash_table_lookup(curve->data, eleName);
  ele2 = (guint*)g_hash_table_lookup(curve->data, lkName);
  all = (guint*)g_hash_table_lookup(curve->data, ALL);

  /* Create or replace the link data themselves. */
  storage = (guint*)g_hash_table_lookup(curve->data, link1);
  if (storage)
    {
      /* Remove it from the element data and all data. */
      g_return_if_fail(all && ele1 && ele2);
      DBG_fprintf(stderr, "Gtk Curve: remove previously stored"
                  " values for link '%s'.\n", link1);
      for (i = 0; i < nSteps; i++)
	{
	  ele1[i] -= storage[i];
	  if (ele1 != ele2)
	    ele2[i] -= storage[i];
	  all[i] -= storage[i];
	}
    }
  DBG_fprintf(stderr, "Gtk Curve: copy histo values.\n");
  storage = g_memdup(data, sizeof(int) * nSteps);
  g_hash_table_insert(curve->data, link1, (gpointer)storage);
  if (link2)
    {
      storage = g_memdup(data, sizeof(int) * nSteps);
      g_hash_table_insert(curve->data, link2, (gpointer)storage);
    }

  /* Add the data to the element data and all data. */
  if (!ele1)
    {
      ele1 = g_memdup(data, sizeof(int) * nSteps);
      g_hash_table_insert(curve->data, g_strdup(eleName), (gpointer)ele1);
    }
  else
    for (i = 0; i < nSteps - 1; i++)
      ele1[i] += data[i];
  if (link2)
    {
      if (!ele2)
	{
	  ele2 = g_memdup(data, sizeof(int) * nSteps);
	  g_hash_table_insert(curve->data, g_strdup(lkName), (gpointer)ele2);
	}
      else
	for (i = 0; i < nSteps - 1; i++)
	  ele2[i] += data[i];
    }
      
  max = 0;
  for (i = 0; i < nSteps - 1; i++)
    {
      max = MAX(max, data[i]);
      all[i] += data[i];
    }
  curve->heights[1] = MAX(curve->heights[1], max * 1.1f);
  DBG_fprintf(stderr, " | max %g becomes %g.\n", (double)max, curve->heights[1]);
}
/**
 * visu_ui_curve_frame_setNNodes:
 * @curve: a #VisuUiCurveFrame widget.
 * @ele: a string.
 * @n: a number.
 *
 * Modify the number of ... TODO
 *
 * Since: 3.6
 */
void visu_ui_curve_frame_setNNodes(VisuUiCurveFrame *curve, const gchar *ele, guint n)
{
  guint *data;

  g_return_if_fail(VISU_UI_IS_CURVE_FRAME(curve));

  data = (guint*)g_hash_table_lookup(curve->data, ele);
  g_return_if_fail(data);
  data[curve->nSteps - 1] = n;

  data = (guint*)g_hash_table_lookup(curve->data, ALL);
  g_return_if_fail(data);
  data[curve->nSteps - 1] += n;

  DBG_fprintf(stderr, "Gtk Curve: set %d nodes to '%s' (all = %d).\n",
	      n, ele, data[curve->nSteps - 1]);
}
/**
 * visu_ui_curve_frame_setStyle:
 * @curve: a #VisuUiCurveFrame object.
 * @style: a style id.
 *
 * Modify the rendering style of the graph.
 *
 * Since: 3.5
 *
 * Returns: TRUE if the style is actually changed.
 */
gboolean visu_ui_curve_frame_setStyle(VisuUiCurveFrame *curve, VisuUiCurveFrameStyle style)
{
  g_return_val_if_fail(VISU_UI_IS_CURVE_FRAME(curve), FALSE);

  if (curve->style == style)
    return FALSE;

  curve->style = style;

  return TRUE;
}
/**
 * visu_ui_curve_frame_setFilter:
 * @curve: a #VisuUiCurveFrame widget.
 * @filter: a string.
 *
 * Modify the filter used to draw all or single #VisuElement
 * distribution.
 *
 * Since: 3.6
 *
 * Returns: TRUE if filter is actually changed.
 */
gboolean visu_ui_curve_frame_setFilter(VisuUiCurveFrame *curve, const gchar* filter)
{
  g_return_val_if_fail(VISU_UI_IS_CURVE_FRAME(curve), FALSE);

  if (curve->filter && filter && !strcmp(curve->filter, filter))
    return FALSE;

  if (curve->filter)
    g_free(curve->filter);
  curve->filter = (gchar*)0;

  if (filter)
    curve->filter = g_strdup(filter);

  return TRUE;
}
/**
 * visu_ui_curve_frame_setHighlightRange:
 * @curve: a #VisuUiCurveFrame widget.
 * @range: two floats.
 *
 * Modify the distance span that is used for highlight rendering and
 * calculation, see visu_ui_curve_frame_getMeanInRange().
 *
 * Since: 3.6
 *
 * Returns: TRUE if range is actually changed.
 */
gboolean visu_ui_curve_frame_setHighlightRange(VisuUiCurveFrame *curve, float range[2])
{
  g_return_val_if_fail(VISU_UI_IS_CURVE_FRAME(curve), FALSE);

  DBG_fprintf(stderr, "Gtk Curve: set highlight range to %gx%g.\n",
	      range[0], range[1]);
  if (range[0] >= 0.f && range[1] > range[0])
    {
      if (curve->hlRange[0] == range[0] && curve->hlRange[1] == range[1])
	return FALSE;
      curve->hlRange[0] = range[0];
      curve->hlRange[1] = range[1];
    }
  else
    {
      if (curve->hlRange[0] == -1.f)
	return FALSE;
      curve->hlRange[0] = -1.f;
    }

  return TRUE;
}
/**
 * visu_ui_curve_frame_getHighlightRange:
 * @curve: a #VisuUiCurveFrame widget.
 * @range: a location for two floats.
 *
 * Retrieves the distance span that is used for highlight rendering and
 * calculation, see visu_ui_curve_frame_setHighlightRange().
 *
 * Since: 3.6
 *
 * Returns: TRUE if range has been set already.
 */
gboolean visu_ui_curve_frame_getHighlightRange(VisuUiCurveFrame *curve, float range[2])
{
  g_return_val_if_fail(VISU_UI_IS_CURVE_FRAME(curve), FALSE);

  DBG_fprintf(stderr, "Gtk Curve: get highlight range of %gx%g.\n",
	      curve->hlRange[0], curve->hlRange[1]);
  if (curve->hlRange[0] < 0.f || curve->hlRange[1] <= curve->hlRange[0])
    return FALSE;

  range[0] = curve->hlRange[0];
  range[1] = curve->hlRange[1];
  return TRUE;
}
/**
 * visu_ui_curve_frame_draw:
 * @curve: a #VisuUiCurveFrame widget.
 *
 * Forces to redraw the widget.
 *
 * Since: 3.6
 */
void visu_ui_curve_frame_draw(VisuUiCurveFrame *curve)
{
  g_return_if_fail(VISU_UI_IS_CURVE_FRAME(curve));

  redraw(GTK_WIDGET(curve));
}
/**
 * visu_ui_curve_frame_getIntegralInRange:
 * @curve: a #VisuUiCurveFrame widget.
 * @label: a location to a string.
 *
 * Calculates the integral of the displayed distribution (see
 * visu_ui_curve_frame_setFilter()) in the given range (see
 * visu_ui_curve_frame_setHighlightRange()). If @label is present, it will points on a
 * string labeling the displayed distribution. The string is owned by V_Sim.
 *
 * Since: 3.6
 *
 * Returns: the integral.
 */
float visu_ui_curve_frame_getIntegralInRange(VisuUiCurveFrame *curve, gchar **label)
{
  guint *data;
  float val, d;
  guint i;
  
  g_return_val_if_fail(VISU_UI_IS_CURVE_FRAME(curve), 0.f);

  if (label && curve->filter)
    *label = curve->filter;
  else if (label)
    *label = ALL;

  if (curve->filter)
    data = (guint*)g_hash_table_lookup(curve->data, curve->filter);
  else
    data = (guint*)g_hash_table_lookup(curve->data, ALL);
  if (!data)
    return 0.f;

  val = 0.f;
  d = curve->init;
  for (i = 0; i < curve->nSteps - 1; i++)
    {
      if (d >= curve->hlRange[0] && d < curve->hlRange[1])
	val += (float)data[i];
      d += curve->step;
    }

  DBG_fprintf(stderr, "Gtk Curve: get %d pairs for %d elements.\n",
	      (int)val, data[curve->nSteps - 1]);
  return val / (float)data[curve->nSteps - 1];
}
/**
 * visu_ui_curve_frame_getMeanInRange:
 * @curve: a #VisuUiCurveFrame widget.
 * @label: a location to a string.
 *
 * Calculates the average distance value of the displayed distribution (see
 * visu_ui_curve_frame_setFilter()) in the given range (see
 * visu_ui_curve_frame_setHighlightRange()). If @label is present, it will points on a
 * string labeling the displayed distribution. The string is owned by V_Sim.
 *
 * Since: 3.6
 *
 * Returns: the average distance value.
 */
float visu_ui_curve_frame_getMeanInRange(VisuUiCurveFrame *curve, gchar **label)
{
  guint *data;
  float val, d;
  guint i, n;
  
  g_return_val_if_fail(VISU_UI_IS_CURVE_FRAME(curve), 0.f);

  if (label && curve->filter)
    *label = curve->filter;
  else if (label)
    *label = ALL;

  if (curve->filter)
    data = (guint*)g_hash_table_lookup(curve->data, curve->filter);
  else
    data = (guint*)g_hash_table_lookup(curve->data, ALL);
  if (!data)
    return 0.f;

  val = 0.f;
  n = 0;
  for (i = 0, d = curve->init; i < curve->nSteps - 1; i++, d += curve->step)
    if (d >= curve->hlRange[0] && d < curve->hlRange[1])
      {
        val += (float)data[i] * (d + curve->step * 0.5f);
        n += data[i];
      }

  DBG_fprintf(stderr, "Gtk Curve: get mean (%f) for %d elements.\n",
	      val / (float)n, n);
  return val / (float)n;
}
