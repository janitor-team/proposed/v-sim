/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include <gtk/gtk.h>

#include "gtk_valueIOWidget.h"
#include <support.h>
#include <visu_tools.h>
#include <visu_gtk.h>

/**
 * SECTION:gtk_valueIOWidget
 * @short_description: Defines a specialised #GtkHBox with three
 * button to open, save and save as XML value files.
 *
 * <para></para>
 *
 * Since: 3.5
 */

static void visu_ui_value_io_dispose (GObject *obj);
static void visu_ui_value_io_finalize(GObject *obj);

/**
 * VisuUiValueIo:
 *
 * Private structure to store informations of a #VisuUiValueIo object.
 *
 * Since: 3.5
 */
struct _VisuUiValueIo
{
  GtkHBox hbox;

  GtkWidget *btExportOpen, *btExportSave, *btExportSaveAs;
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkWidget *dialog;
#endif
  GtkWindow *parent;

  VisuUiValueIoCallback ioOpen;
  gulong openSignalId;
  VisuUiValueIoCallback ioSave;
  gulong saveSignalId;
  gulong saveAsSignalId;

  /* Memory gestion. */
  gboolean dispose_has_run;
};
/**
 * VisuUiValueIoClass
 *
 * Private structure to store informations of a #VisuUiValueIoClass object.
 *
 * Since: 3.5
 */
struct _VisuUiValueIoClass
{
  GtkComboBoxClass parent_class;

  void (*valueio) (VisuUiValueIo *hbox);
};

/* Local callbacks. */
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
static void onImportXML(GtkFileChooserButton *filechooserbutton, gpointer data);
#else
static void onImportXML(GtkDialog *dialog, gint response, gpointer data);
#endif
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 6
static void onOpenXMLClicked(GtkButton *bt, gpointer data);
#endif
static void onExportXML(GtkButton *button, gpointer data);

/* Local methods. */

/**
 * visu_ui_value_io_get_type
 *
 * #GType are unique numbers to identify objects.
 *
 * Returns: the #GType associated with #VisuUiValueIo objects.
 *
 * Since: 3.5
 */
G_DEFINE_TYPE(VisuUiValueIo, visu_ui_value_io, GTK_TYPE_HBOX)

static void visu_ui_value_io_class_init(VisuUiValueIoClass *klass)
{
  DBG_fprintf(stderr, "Gtk VisuUiValueIo: creating the class of the widget.\n");
  
  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose = visu_ui_value_io_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_ui_value_io_finalize;
}

static void visu_ui_value_io_dispose(GObject *obj)
{
  DBG_fprintf(stderr, "Gtk VisuUiValueIo: dispose object %p.\n", (gpointer)obj);

  if (VISU_UI_VALUE_IO(obj)->dispose_has_run)
    return;

  VISU_UI_VALUE_IO(obj)->dispose_has_run = TRUE;
  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_value_io_parent_class)->dispose(obj);
}
static void visu_ui_value_io_finalize(GObject *obj)
{
  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Gtk VisuUiValueIo: finalize object %p.\n", (gpointer)obj);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_value_io_parent_class)->finalize(obj);

  DBG_fprintf(stderr, " | freeing ... OK.\n");
}


static void visu_ui_value_io_init(VisuUiValueIo *valueio)
{
  DBG_fprintf(stderr, "Gtk VisuUiValueIo: initializing new object (%p).\n",
	      (gpointer)valueio);

  valueio->ioOpen         = (VisuUiValueIoCallback)0;
  valueio->openSignalId   = 0;
  valueio->ioSave         = (VisuUiValueIoCallback)0;
  valueio->saveSignalId   = 0;
  valueio->saveAsSignalId = 0;
}

static void buildWidgets(VisuUiValueIo *valueio, GtkWindow *parent,
			 const gchar*tipsOpen, const gchar*tipsSave,
			 const gchar*tipsSaveAs)
{
  GtkFileFilter *filter1, *filter2;
  char *directory;
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;

  tooltips = gtk_tooltips_new();
#endif

  valueio->parent = parent;

  /* The save as button. */
  valueio->btExportSaveAs = gtk_button_new();
  gtk_widget_set_tooltip_text(valueio->btExportSaveAs, tipsSaveAs);
  gtk_widget_set_sensitive(valueio->btExportSaveAs, FALSE);
  gtk_container_add(GTK_CONTAINER(valueio->btExportSaveAs),
		    gtk_image_new_from_stock(GTK_STOCK_SAVE_AS, GTK_ICON_SIZE_MENU));
  gtk_box_pack_end(GTK_BOX(valueio), valueio->btExportSaveAs, FALSE, FALSE, 0);

#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  /* The save button. */
  valueio->btExportSave = gtk_button_new();
  gtk_widget_set_tooltip_text(valueio->btExportSave, tipsSave);
  gtk_widget_set_sensitive(valueio->btExportSave, FALSE);
  gtk_container_add(GTK_CONTAINER(valueio->btExportSave),
		    gtk_image_new_from_stock(GTK_STOCK_SAVE, GTK_ICON_SIZE_MENU));
  gtk_box_pack_end(GTK_BOX(valueio), valueio->btExportSave, FALSE, FALSE, 0);
#endif

  /* The open button. */
  filter1 = gtk_file_filter_new();
  gtk_file_filter_set_name(filter1, _("V_Sim value file (*.xml)"));
  gtk_file_filter_add_pattern(filter1, "*.xml");
  filter2 = gtk_file_filter_new ();
  gtk_file_filter_set_name(filter2, _("All files"));
  gtk_file_filter_add_pattern (filter2, "*");
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
  valueio->btExportOpen = gtk_file_chooser_button_new(_("Open a V_Sim value file"),
						      GTK_FILE_CHOOSER_ACTION_OPEN);
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(valueio->btExportOpen), filter1);
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(valueio->btExportOpen), filter2);
#else
  valueio->dialog = gtk_file_chooser_dialog_new
    (_("Open a V_Sim value file"), parent,
     GTK_FILE_CHOOSER_ACTION_OPEN, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
     GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT, NULL);
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(valueio->dialog), filter1);
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(valueio->dialog), filter2);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  valueio->btExportOpen = gtk_file_chooser_button_new_with_dialog(valueio->dialog);
#else
  valueio->btExportOpen = gtk_button_new_from_stock(GTK_STOCK_OPEN);
#endif
#endif
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  directory = visu_ui_getLastOpenDirectory();
  if (directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(valueio->btExportOpen),
					directory);
#endif
  gtk_widget_set_tooltip_text(valueio->btExportOpen, tipsOpen);
  gtk_widget_set_sensitive(valueio->btExportOpen, FALSE);
  gtk_box_pack_end(GTK_BOX(valueio), valueio->btExportOpen, TRUE, TRUE, 0);

  /* The label. */
  gtk_box_pack_end(GTK_BOX(valueio), gtk_label_new(_("I/O:")), FALSE, FALSE, 0);
}

/**
 * visu_ui_value_io_new:
 * @parent: the parent used to show the file dialog.
 * @tipsOpen: a tooltip to show on open button.
 * @tipsSave: a tooltip to show on save button.
 * @tipsSaveAs: a tooltip to show on save-as button.
 *
 * A #VisuUiValueIo widget is like a #GtkComboBox widget, but it is already filled
 * with predefined line patterns (call stipple). Using this widget is
 * a convienient way to share stipples between all part of V_Sim and
 * to give a consistent look of all stipple selection.
 *
 * Since: 3.5
 *
 * Returns: (transfer full): a newly created #VisuUiValueIo widget.
 */
GtkWidget* visu_ui_value_io_new(GtkWindow *parent, const gchar*tipsOpen,
                                const gchar*tipsSave, const gchar*tipsSaveAs)
{
  VisuUiValueIo *valueio;

  DBG_fprintf(stderr, "Gtk VisuUiValueIo: creating new object.\n");

  valueio = VISU_UI_VALUE_IO(g_object_new(visu_ui_value_io_get_type (), NULL));

  DBG_fprintf(stderr, "Gtk VisuUiValueIo: build widgets.\n");

  buildWidgets(valueio, parent, tipsOpen, tipsSave, tipsSaveAs);

  return GTK_WIDGET(valueio);
}
/**
 * visu_ui_value_io_connectOnOpen:
 * @valueio: a #VisuUiValueIo widget.
 * @open: a method.
 * 
 * Set the function to call when the open button is clicked.
 * 
 * Since: 3.5
 */
void visu_ui_value_io_connectOnOpen(VisuUiValueIo *valueio, VisuUiValueIoCallback open)
{
  g_return_if_fail(VISU_UI_IS_VALUE_IO(valueio));

  if (valueio->openSignalId)
    {
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
      g_signal_handler_disconnect(G_OBJECT(valueio->btExportOpen),
				  valueio->openSignalId);
#else
      g_signal_handler_disconnect(G_OBJECT(valueio->dialog),
				  valueio->openSignalId);
#endif
#else
      g_signal_handler_disconnect(G_OBJECT(valueio->btExportOpen),
				  valueio->openSignalId);
#endif
    }
  
  valueio->ioOpen = open;
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
  valueio->openSignalId =
    g_signal_connect(G_OBJECT(valueio->btExportOpen), "file-set",
		     G_CALLBACK(onImportXML), (gpointer)valueio);
#else
  valueio->openSignalId =
    g_signal_connect(G_OBJECT(valueio->dialog), "response",
		     G_CALLBACK(onImportXML), (gpointer)valueio);
#endif
#else
  valueio->openSignalId =
    g_signal_connect(G_OBJECT(valueio->btExportOpen), "clicked",
		     G_CALLBACK(onOpenXMLClicked), (gpointer)valueio);
#endif
}
/**
 * visu_ui_value_io_connectOnSave:
 * @valueio: a #VisuUiValueIo widget.
 * @save: a method.
 * 
 * Set the function to call when the save or save-as button is clicked.
 * 
 * Since: 3.5
 */
void visu_ui_value_io_connectOnSave(VisuUiValueIo *valueio, VisuUiValueIoCallback save)
{
  g_return_if_fail(VISU_UI_IS_VALUE_IO(valueio));

#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  if (valueio->saveSignalId)
    g_signal_handler_disconnect(G_OBJECT(valueio->btExportSave),
				valueio->saveSignalId);
#endif
  if (valueio->saveAsSignalId)
    g_signal_handler_disconnect(G_OBJECT(valueio->btExportSaveAs),
				valueio->saveAsSignalId);
  
  valueio->ioSave = save;
  valueio->saveAsSignalId = 
    g_signal_connect(G_OBJECT(valueio->btExportSaveAs), "clicked",
		     G_CALLBACK(onExportXML), (gpointer)valueio);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  valueio->saveSignalId =
    g_signal_connect(G_OBJECT(valueio->btExportSave), "clicked",
		     G_CALLBACK(onExportXML), (gpointer)valueio);
#endif
}

static gboolean loadXMLFile(gpointer data)
{
  gchar *filename;
  GError *error;

  g_return_val_if_fail(VISU_UI_IS_VALUE_IO(data), FALSE);
  g_return_val_if_fail(VISU_UI_VALUE_IO(data)->ioOpen, FALSE);

  filename = g_object_get_data(G_OBJECT(data), "filename");
  g_return_val_if_fail(filename, FALSE);

  error = (GError*)0;
  if (!VISU_UI_VALUE_IO(data)->ioOpen(filename, &error))
    {
      visu_ui_raiseWarning
	(_("Import V_Sim values from a file."),
	 error->message, VISU_UI_VALUE_IO(data)->parent);
      g_error_free(error);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
      gtk_file_chooser_unselect_all(GTK_FILE_CHOOSER(VISU_UI_VALUE_IO(data)->btExportOpen));
      gtk_widget_set_sensitive(VISU_UI_VALUE_IO(data)->btExportSave, FALSE);
#endif
    }
  g_free(filename);
  g_object_set_data(G_OBJECT(data), "filename", (gpointer)0);

  return FALSE;
}

#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
static void onImportXML(GtkFileChooserButton *filechooserbutton, gpointer data)
{
  gchar *filename;

  filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filechooserbutton));
  if (!filename)
    return;
  
  g_object_set_data(G_OBJECT(data), "filename", filename);
  g_idle_add(loadXMLFile, (gpointer)data);
}
#else
static void onImportXML(GtkDialog *dialog, gint response, gpointer data)
{
  gchar *filename;

  if (response != GTK_RESPONSE_ACCEPT)
    return;

  filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));

  g_object_set_data(G_OBJECT(data), "filename", filename);
  g_idle_add(loadXMLFile, (gpointer)data);
}
#endif
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 6
static void onOpenXMLClicked(GtkButton *bt _U_, gpointer data)
{
  gchar *filename;

  if (gtk_dialog_run(GTK_DIALOG(VISU_UI_VALUE_IO(data)->dialog)) != GTK_RESPONSE_ACCEPT)
    return;

  filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(VISU_UI_VALUE_IO(data)->dialog));

  g_object_set_data(G_OBJECT(data), "filename", filename);
  g_idle_add(loadXMLFile, (gpointer)data);
}
#endif
/**
 * visu_ui_value_io_getFilename:
 * @parent: a parent to display the dialog on.
 *
 * Open a save dialog window  with XML filter to choose the name of a
 * file. This is the default action that can be connect to a #VisuUiValueIo
 * widget using visu_ui_value_io_connectOnSave().
 *
 * Since: 3.5
 *
 * Returns: a filename that should be freed later with g_free() by the caller.
 */
gchar* visu_ui_value_io_getFilename(GtkWindow *parent)
{
  GtkWidget *saveDialog;
  gchar *filename, *directory;
  GtkFileFilter *filter;

  saveDialog = gtk_file_chooser_dialog_new
    (_("Export V_Sim values to a file."), parent,
     GTK_FILE_CHOOSER_ACTION_SAVE,
     GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
     GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT, NULL);
  gtk_window_set_modal(GTK_WINDOW(saveDialog), TRUE);
  gtk_window_set_transient_for(GTK_WINDOW(saveDialog), GTK_WINDOW(parent));
  gtk_window_set_position(GTK_WINDOW(saveDialog), GTK_WIN_POS_CENTER_ON_PARENT);
  directory = visu_ui_getLastOpenDirectory();
  if (directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(saveDialog), directory);
  gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(saveDialog), FALSE);
  gtk_dialog_set_default_response(GTK_DIALOG(saveDialog), GTK_RESPONSE_ACCEPT);

  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name(filter, _("V_Sim value files (*.xml)"));
  gtk_file_filter_add_pattern(filter, "*.xml");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(saveDialog), filter);
  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name(filter, _("All files"));
  gtk_file_filter_add_pattern (filter, "*");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(saveDialog), filter);

  /* A suggested filename. */
  gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(saveDialog),
				    _("values.xml"));

  if (gtk_dialog_run(GTK_DIALOG(saveDialog)) != GTK_RESPONSE_ACCEPT)
    {
      gtk_widget_destroy(saveDialog);
      return (gchar*)0;
    }
  filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(saveDialog));
  gtk_widget_destroy(saveDialog);
  
  return filename;
}

static gboolean saveXMLFile(gpointer data)
{
  gchar *filename;
  GError *error;

  g_return_val_if_fail(VISU_UI_IS_VALUE_IO(data), FALSE);
  g_return_val_if_fail(VISU_UI_VALUE_IO(data)->ioSave, FALSE);

  filename = g_object_get_data(G_OBJECT(data), "filename");
  g_return_val_if_fail(filename, FALSE);

  error = (GError*)0;
  if (!VISU_UI_VALUE_IO(data)->ioSave(filename, &error))
    {
      visu_ui_raiseWarning
	(_("Export V_Sim values to a file."),
	 error->message, VISU_UI_VALUE_IO(data)->parent);
      g_error_free(error);
    }
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  else
    {
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
      g_signal_handler_block(G_OBJECT(VISU_UI_VALUE_IO(data)->btExportOpen),
			     VISU_UI_VALUE_IO(data)->openSignalId);
#endif
      gtk_file_chooser_set_filename
	(GTK_FILE_CHOOSER(VISU_UI_VALUE_IO(data)->btExportOpen), filename);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
      g_signal_handler_unblock(G_OBJECT(VISU_UI_VALUE_IO(data)->btExportOpen),
			       VISU_UI_VALUE_IO(data)->openSignalId);
#endif
    }
#endif
  g_free(filename);
  g_object_set_data(G_OBJECT(data), "filename", (gpointer)0);

  return FALSE;
}

static void onExportXML(GtkButton *button, gpointer data)
{
  gchar *filename;

  if (button == GTK_BUTTON(VISU_UI_VALUE_IO(data)->btExportSave))
    filename =
      gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(VISU_UI_VALUE_IO(data)->btExportOpen));
  else
    filename =
      visu_ui_value_io_getFilename(VISU_UI_VALUE_IO(data)->parent);

  if (filename)
    {
      g_object_set_data(G_OBJECT(data), "filename", filename);
      g_idle_add(saveXMLFile, (gpointer)data);
    }
}
/**
 * visu_ui_value_io_setSensitiveOpen:
 * @valueio: a #VisuUiValueIo widget.
 * @status: a boolean.
 *
 * Modify the sensitivity of the open button, depending on @status.
 * 
 * Since: 3.5
 */
void visu_ui_value_io_setSensitiveOpen(VisuUiValueIo *valueio, gboolean status)
{
  g_return_if_fail(VISU_UI_IS_VALUE_IO(valueio));

  gtk_widget_set_sensitive(valueio->btExportOpen, status);
}
/**
 * visu_ui_value_io_setSensitiveSave:
 * @valueio: a #VisuUiValueIo widget.
 * @status: a boolean.
 *
 * Modify the sensitivity of the save button, depending on @status.
 * 
 * Since: 3.5
 */
void visu_ui_value_io_setSensitiveSave(VisuUiValueIo *valueio, gboolean status)
{
  gchar *filename;

  g_return_if_fail(VISU_UI_IS_VALUE_IO(valueio));

#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(valueio->btExportOpen));
  gtk_widget_set_sensitive(valueio->btExportSave, status && filename);
  if (filename)
    g_free(filename);
#endif
  gtk_widget_set_sensitive(valueio->btExportSaveAs, status);
}
/**
 * visu_ui_value_io_setFilename:
 * @valueio: a #VisuUiValueIo widget.
 * @filename: a location on disk.
 *
 * Call the open routine previously set by visu_ui_value_io_connectOnOpen() on
 * @filename and update the buttons accordingly.
 *
 * Since: 3.5
 *
 * Returns: TRUE on success of the open routine.
 */
gboolean visu_ui_value_io_setFilename(VisuUiValueIo *valueio, const gchar *filename)
{
  gboolean valid;
  GError *error;

  g_return_val_if_fail(VISU_UI_IS_VALUE_IO(valueio), FALSE);
  g_return_val_if_fail(valueio->ioOpen, FALSE);
  
  error = (GError*)0;
  valid = valueio->ioOpen(filename, &error);
  if (!valid)
    {
      visu_ui_raiseWarning
	(_("Export V_Sim values to a file."),
	 error->message, valueio->parent);
      g_error_free(error);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
      gtk_file_chooser_unselect_all(GTK_FILE_CHOOSER(valueio->btExportOpen));
      gtk_widget_set_sensitive(valueio->btExportSave, FALSE);
#endif
    }
  else
    {
      gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(valueio->btExportOpen), filename);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
      gtk_widget_set_sensitive(valueio->btExportSave, TRUE);
#endif
      gtk_widget_set_sensitive(valueio->btExportSaveAs, TRUE);
    }

  return valid;
}
