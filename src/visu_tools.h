/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISUTOOLS_H
#define VISUTOOLS_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#ifndef DEBUG
  #define DEBUG 0
#endif
#define DBG_fprintf if(DEBUG) (void)fprintf

#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif

/**
 * TOOL_MAX_LINE_LENGTH
 *
 * This is the maximum number of characters read on a
 * line of an input file.
 */
#define TOOL_MAX_LINE_LENGTH 256

/**
 * ToolVoidDataFunc:
 * @data: a pointer to some user defined object.
 *
 * These methods are used when no specific argument is required except
 * a user-defined object and when void is the return type.
 */
typedef void (*ToolVoidDataFunc)(gpointer data);

/**
 * ToolInitFunc:
 *
 * These methods are used by V_Sim to initialise some part of the
 * program. They are called once on start-up.
 */
typedef void (*ToolInitFunc)(void);

gchar* tool_getValidPath(GList **pathList, const char **filenames, int accessMode);

float tool_modulo_float(float a, int b);

gchar* tool_path_normalize(const gchar* path);

#if GLIB_MINOR_VERSION < 5
gboolean g_file_set_contents(const gchar *fileName, const gchar *str,
			     gsize len, GError **error);
#endif
#if GLIB_MINOR_VERSION < 27
void g_list_free_full(GList *list, GDestroyNotify free_func);
#endif

gboolean tool_XML_substitute(GString *output, const gchar *filename,
				 const gchar *tag, GError **error);

G_END_DECLS

#endif
