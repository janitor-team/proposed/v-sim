/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include <gtk/gtk.h>
#ifdef HAVE_LIBGTKGLEXT_X11_1_0
#include <gtk/gtkgl.h>
#endif
#include <unistd.h> /* For the access markers R_OK, W_OK ... */
#include <string.h>

#include "support.h"
#include "opengl.h"
#include "visu_gtk.h"
#include "visu_object.h"
#include "visu_basic.h"
#include "visu_commandLine.h"
#include "visu_configFile.h"
#include "visu_rendering.h"
#include "visu_extension.h"
#include "visu_plugins.h"
#include "renderingMethods/renderingAtomic.h"
#include "renderingMethods/renderingSpin.h"
#include "coreTools/toolShade.h"
#include "coreTools/toolConfigFile.h"
#include "extraFunctions/plane.h"
#include "extraFunctions/dataFile.h"
#include "panelModules/panelSurfaces.h"
#include "gtk_renderingWindowWidget.h"
#include "extraGtkFunctions/gtk_colorComboBoxWidget.h"
#include "OSOpenGL/visu_openGL.h"
#include "extensions/fogAndBGColor.h"
#include "extensions/nodes.h"
#include "extensions/box.h"
#include "extensions/scale.h"

#include <pixmaps/icone-observe.xpm>

/**
 * SECTION: visu_gtk
 * @short_description: Basic GUI routines, for preview, standard
 * dialogs...
 *
 * <para>There are some common UI routines here. The preview widget
 * can be called with visu_ui_preview_add(). Error messages can be
 * displayed via dialogs, see visu_ui_raiseWarning()...</para>
 */

/* Parameters. */
#define FLAG_PARAMETER_PREVIEW "main_usePreview"
#define DESC_PARAMETER_PREVIEW "Automatically compute preview in filechooser ; boolean"
static gboolean usePreview;

/* Static variables. */
static gint gtkFileChooserWidth = -1, gtkFileChooserHeight = -1;

/* This hashtable associate a #RenderingMethod with a Gtk dialog to
   choose files. */
static GHashTable *visuGtkLoadMethods = NULL;
/* Store the last open directory. It is initialised
   to current working directory. */
static gchar *visuGtkLastDir;

static GtkWindow *visuGtkPanel;
static GtkWindow *visuGtkRender;
static GtkWidget *visuGtkRenderArea = (GtkWidget*)0;

/* Local routines. */
static void exportParameters(GString *data, VisuData *dataObj, VisuGlView *view);
static void onPreviewToggled(GtkToggleButton *button, gpointer data);
static void initVisuGtk();


/**
 * visu_ui_raiseWarning:
 * @action: a string ;
 * @message: another string ;
 * @window: the parent window to raise the warning on.
 *
 * Raise a warning window with the action in bold and the message written
 * underneath.
 */
void visu_ui_raiseWarning(gchar *action, gchar *message, GtkWindow *window)
{
  GtkWidget *alert;
  gchar *str;
  
  if (!window)
    window = visuGtkRender;

  DBG_fprintf(stderr, "Visu Gtk: raise the error dialog (parent %p).\n",
	      (gpointer)window);

#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  str = action;
#else
  str = message;
#endif
  alert = gtk_message_dialog_new(GTK_WINDOW(window),
				 GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
				 GTK_MESSAGE_WARNING, GTK_BUTTONS_OK, "%s", str);
  gtk_window_set_title(GTK_WINDOW(alert), _("V_Sim error message"));
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  gtk_widget_set_name(alert, "error");
#else
  gtk_widget_set_name(alert, action);
#endif
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(alert), "%s", message);
#endif
  gtk_widget_show_all(alert);

  /* block in a loop waiting for reply. */
  gtk_dialog_run(GTK_DIALOG(alert));
  gtk_widget_destroy(alert);
}
/**
 * visu_ui_raiseWarningLong:
 * @action: a string ;
 * @message: another string ;
 * @window: the parent window to raise the warning on.
 *
 * Same as visu_ui_raiseWarning() except that the message is displayed
 * in a text buffer, ideal for a log.
 */
void visu_ui_raiseWarningLong(gchar *action, gchar *message, GtkWindow *window)
{
  GtkWidget *alert;
  GtkWidget *text, *scroll;
  GtkTextBuffer *buf;

  if (!window)
    window = visuGtkRender;

  alert = gtk_message_dialog_new(GTK_WINDOW(window),
				 GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
				 GTK_MESSAGE_WARNING,
				 GTK_BUTTONS_OK,
				 "%s", action);
  gtk_window_set_resizable(GTK_WINDOW(alert), TRUE);
  gtk_widget_set_name(alert, "error");
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(alert),
					   _("Output errors:"));
#endif
  scroll = gtk_scrolled_window_new((GtkAdjustment*)0, (GtkAdjustment*)0);
  gtk_widget_set_size_request(scroll, 300, 200);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(scroll),
				      GTK_SHADOW_ETCHED_IN);
  gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG(alert))),
		     scroll, TRUE, TRUE, 2);
  text = gtk_text_view_new();
  gtk_text_view_set_editable(GTK_TEXT_VIEW(text), FALSE);
  gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(text), FALSE);
  gtk_container_add(GTK_CONTAINER(scroll), text);
  buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW(text));
  gtk_text_buffer_set_text(GTK_TEXT_BUFFER(buf), message, -1);
  gtk_widget_show_all(alert);

  /* block in a loop waiting for reply. */
  gtk_dialog_run (GTK_DIALOG (alert));
  gtk_widget_destroy(alert);
}
/**
 * visu_ui_wait:
 *
 * It runs the Gtk event loop, flushing it before returning.
 */
void visu_ui_wait(void)
{
  while(gtk_events_pending())
    gtk_main_iteration();
}

struct _VisuUiSetFilesFunc
{
  VisuUiSetFilesFunc load;
};
/**
 * visu_ui_setRenderingSpecificMethod:
 * @method: a #RenderingMethod object;
 * @methodLoad: (scope call): a #VisuUiSetFilesFunc method (can be NULL).
 *
 * This function is used by a client to add gtk methods to a rendering method.
 * The @methodLoad argument is called to when the 'load' button is pushed.
 */
void visu_ui_setRenderingSpecificMethod(VisuRendering *method,
					 VisuUiSetFilesFunc methodLoad)
{
  struct _VisuUiSetFilesFunc *ct;

  g_return_if_fail(method);

  if (!visuGtkLoadMethods)
    initVisuGtk();

  DBG_fprintf(stderr, "Visu Gtk: set rendering specific for method '%s'.\n",
	      visu_rendering_getName(method, FALSE));
  ct = g_malloc(sizeof(struct _VisuUiSetFilesFunc));
  ct->load = methodLoad;
  g_hash_table_insert(visuGtkLoadMethods, method, ct);
}
/**
 * visu_ui_getRenderingSpecificOpen: (skip)
 * @method: a #RenderingMethod object.
 *
 * This method is used to retrieve the #VisuUiSetFilesFunc associate with
 * the specified @method.
 *
 * Returns: a load method if one has been specified for @method
 *          or visu_ui_getFileFromDefaultFileChooser().
 */
VisuUiSetFilesFunc visu_ui_getRenderingSpecificOpen(VisuRendering *method)
{
  struct _VisuUiSetFilesFunc *ct;

  g_return_val_if_fail(method, (VisuUiSetFilesFunc)0);
  
  if (!visuGtkLoadMethods)
    initVisuGtk();

  DBG_fprintf(stderr, "Visu Gtk: looking for a specific load interface for rendering"
	      " method '%s'...\n", visu_rendering_getName(method, FALSE));
  
  ct = (struct _VisuUiSetFilesFunc *)g_hash_table_lookup(visuGtkLoadMethods, method);
  return (ct && ct->load)?ct->load:visu_ui_getFileFromDefaultFileChooser;
}
/**
 * visu_ui_getSelectedDirectory:
 * @parent: (allow-none): if NULL, the command panel window is used ;
 * @multiple: if TRUE, multiple directories can be selected ;
 * @dir: (allow-none): if not NULL, give the opening directory.
 *
 * General procedure to get the location of one or more directories.
 * In the multiple case, all selected directories shares a common ancestor
 * since the selection is done through the same selector that for single
 * directory but with multiple selection activated. If @dir is NULL
 * the last opened (and stored) directory is used (see
 * visu_ui_getLastOpenDirectory() and visu_ui_setLastOpenDirectory()).
 *
 * Returns: (array zero-terminated=1) (transfer full): a newly
 * allocated array of paths, NULL terminated. Use g_strfreev() to free
 * it after use. If no directory is selected, then NULL is returned.
 */
gchar** visu_ui_getSelectedDirectory(GtkWindow *parent, gboolean multiple,
				     const gchar *dir)
{
  GtkWidget *file_selector, *hbox, *wd;
  gchar **dirnames;
  char *directory;
  GSList* names, *tmpLst;
  int i;

  /* Create the selector */
  if (!parent)
    parent = visuGtkRender;

  file_selector = gtk_file_chooser_dialog_new(_("Choose a directory"), parent,
					      GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OPEN, GTK_RESPONSE_OK,
					      NULL);
  if (gtkFileChooserWidth > 0 || gtkFileChooserHeight > 0)
    gtk_window_set_default_size(GTK_WINDOW(file_selector), gtkFileChooserWidth,
				gtkFileChooserHeight);
  if (multiple)
    {
      hbox = gtk_hbox_new(FALSE, 0);
      gtk_file_chooser_set_extra_widget(GTK_FILE_CHOOSER(file_selector), hbox);
      wd = gtk_image_new_from_stock(GTK_STOCK_HELP, GTK_ICON_SIZE_MENU);
      gtk_box_pack_start(GTK_BOX(hbox), wd, FALSE, FALSE, 0);
      wd = gtk_label_new("");
      gtk_box_pack_start(GTK_BOX(hbox), wd, TRUE, TRUE, 5);
      gtk_misc_set_alignment(GTK_MISC(wd), 0., 0.5);
      gtk_label_set_use_markup(GTK_LABEL(wd), TRUE);
      gtk_label_set_markup(GTK_LABEL(wd), _("<span size=\"smaller\">Choose several"
					    " directories using the"
					    " <span font_desc=\"courier\">"
					    "Control</span> key.</span>"));
      gtk_widget_show_all(hbox);
    }
  if (!dir)
    directory = visu_ui_getLastOpenDirectory();
  else
    directory = (char*)dir;
  if (directory)
    {
      DBG_fprintf(stderr, "Visu Gtk: open a directory chooser, set on '%s'.\n",
		  directory);
      gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(file_selector),
					  directory);
    }
  gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(file_selector), multiple);

  gtk_widget_set_name(file_selector, "filesel");
  gtk_window_set_position(GTK_WINDOW(file_selector), GTK_WIN_POS_CENTER_ON_PARENT);
  gtk_window_set_modal(GTK_WINDOW (file_selector), TRUE);

  if (gtk_dialog_run (GTK_DIALOG (file_selector)) == GTK_RESPONSE_OK)
    {
      names = gtk_file_chooser_get_filenames(GTK_FILE_CHOOSER(file_selector));
      dirnames = g_malloc(sizeof(gchar*) * (g_slist_length(names) + 1));
      tmpLst = names;
      i = 0;
      while(tmpLst)
	{
	  dirnames[i] = (gchar*)tmpLst->data;
	  i += 1;
	  tmpLst = g_slist_next(tmpLst);
	}
      dirnames[i] = (gchar*)0;
      g_slist_free(names);
    }
  else
    dirnames = (gchar**)0;

  gtk_window_get_size(GTK_WINDOW(file_selector), &gtkFileChooserWidth,
		      &gtkFileChooserHeight);

  gtk_widget_destroy (file_selector);

  return dirnames;
}

static void free_image(guchar *image, gpointer data _U_)
{
  DBG_fprintf(stderr, "Visu Gtk: free the preview image data.\n");
  g_free(image);
}
static gboolean preview_update(gpointer user_data)
{
  VisuUiPreview *preview = (VisuUiPreview*)user_data;
  gchar *text, *comment;
  VisuData *data;
  gboolean valid;
  GError *error;
  VisuPixmapContext *dumpData;
  GArray* image;
  GdkPixbuf *pixbuf;
  VisuRendering *method;
  VisuNodeArrayIter iter;
  GtkWidget *wd;
  VisuUiRenderingWindow *currentWindow;
  VisuGlView *view;
  float centre[3];
  VisuGlExtNodes *nodes;
  VisuGlExtBox *box;
  VisuGlExtBg *bg;

  g_return_val_if_fail(preview, FALSE);
  DBG_fprintf(stderr, "Visu Gtk: do a preview %p for VisuData %p.\n",
              user_data, (gpointer)preview->data);
  data = preview->data;

  method = visu_object_getRendering(VISU_OBJECT_INSTANCE);
  g_return_val_if_fail(method, FALSE);

  /* We save the current rendering context. */
  currentWindow = VISU_UI_RENDERING_WINDOW(visuGtkRenderArea);

  /* We change the context since loading a new data will
     generate some changes on the OpenGL rendering. */
  dumpData = visu_pixmap_context_new((guint)150, (guint)150);
  if (!dumpData)
    {
      /* We free the VisuData. */
      g_object_unref(data);

      gtk_image_set_from_stock(GTK_IMAGE(preview->image),
                               GTK_STOCK_DIALOG_ERROR,
                               GTK_ICON_SIZE_DIALOG);
      wd = gtk_label_new(_("Internal error,\nno preview available"));
      gtk_box_pack_start(GTK_BOX(preview->vbox), wd,
                         FALSE, FALSE, 0);
      gtk_widget_show_all(wd);
      preview->table = wd;

      /* Set the rendering window current for OpenGL. */
      visu_ui_rendering_window_setCurrent(currentWindow, TRUE);

      return FALSE;
    }

  error = (GError*)0;
  valid = visu_rendering_load(method, data, 0, (GCancellable*)0, &error);
  if (!valid)
    {
      gtk_image_set_from_stock(GTK_IMAGE(preview->image),
                               GTK_STOCK_DIALOG_QUESTION,
                               GTK_ICON_SIZE_DIALOG);
      wd = gtk_label_new(_("Not a V_Sim file"));
      gtk_box_pack_start(GTK_BOX(preview->vbox), wd,
                         FALSE, FALSE, 0);
      gtk_widget_show_all(wd);
      preview->table = wd;
    }
  else
    {
      if (error)
        {
          gtk_image_set_from_stock(GTK_IMAGE(preview->image),
                                   GTK_STOCK_DIALOG_ERROR,
                                   GTK_ICON_SIZE_DIALOG);
          wd = gtk_label_new(_("This file has errors"));
          gtk_box_pack_start(GTK_BOX(preview->vbox), wd,
                             FALSE, FALSE, 0);
          gtk_widget_show_all(wd);
          preview->table = wd;
          g_error_free(error);
        }
      else
        {
          view = preview->view;
          g_return_val_if_fail(view, FALSE);
          visu_gl_initContext();
          visu_boxed_setBox(VISU_BOXED(view), VISU_BOXED(data), TRUE);
          /* Setup extensions for the preview. */
          nodes = visu_gl_ext_nodes_new();
          visu_gl_ext_nodes_setData(nodes, view, data);
          box = visu_gl_ext_box_new("box preview");
          visu_gl_ext_box_setBox(box, visu_boxed_getBox(VISU_BOXED(data)));
          bg = visu_gl_ext_bg_new("bg preview");
          visu_gl_ext_rebuild(VISU_GL_EXT(nodes));
          visu_gl_ext_rebuild(VISU_GL_EXT(box));
          visu_gl_ext_fog_create(view, visu_boxed_getBox(VISU_BOXED(data)));
          visu_gl_ext_rebuild(VISU_GL_EXT(bg));

          /* We call the given draw method. */
          glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
          glMatrixMode(GL_MODELVIEW);
          visu_box_getCentre(visu_boxed_getBox(VISU_BOXED(data)), centre);
          glTranslated(-centre[0], -centre[1], -centre[2]);
          visu_gl_ext_call(VISU_GL_EXT(bg), FALSE);
          visu_gl_ext_call(VISU_GL_EXT(nodes), FALSE);
          visu_gl_ext_call(VISU_GL_EXT(box), FALSE);
          /* We copy the pixmap into generic data. */
          image = visu_pixmap_getData((guint)150, (guint)150, FALSE);
          pixbuf =
            gdk_pixbuf_new_from_data((guchar*)image->data, GDK_COLORSPACE_RGB,
                                     FALSE, 8, 150, 150, 3 * 150,
                                     free_image, (gpointer)0);
          g_array_free(image, FALSE);
          gtk_image_set_from_pixbuf(GTK_IMAGE(preview->image), pixbuf);
          g_object_unref(pixbuf);
          DBG_fprintf(stderr, "Visu GTK: copy data to pixbuf.\n");
          /* We reset the flag of material. */
          comment = visu_data_getFileCommentary(data, 0);
          visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
          preview->table = gtk_table_new(iter.nElements + 1, 2, FALSE);
          wd = gtk_label_new(_("<i>Box composition:</i>"));
          gtk_label_set_use_markup(GTK_LABEL(wd), TRUE);
          gtk_misc_set_alignment(GTK_MISC(wd), 0., 0.5);
          gtk_table_attach(GTK_TABLE(preview->table), wd, 0, 2, 0, 1,
                           GTK_FILL | GTK_EXPAND, GTK_SHRINK, 2, 5);
          for (visu_node_array_iterStart(VISU_NODE_ARRAY(data), &iter); iter.element;
               visu_node_array_iterNextElement(VISU_NODE_ARRAY(data), &iter))
            {
              DBG_fprintf(stderr, "Visu GTK: setup comment for element '%s' (%p).\n",
                          iter.element->name, (gpointer)iter.element);
              iter.element->materialIsUpToDate = FALSE;
              wd = gtk_label_new("");
              text = g_markup_printf_escaped
                (_("<span size=\"small\"><b>%s:</b></span>"),
                 iter.element->name);
              gtk_label_set_markup(GTK_LABEL(wd), text);
              gtk_misc_set_alignment(GTK_MISC(wd), 1., 0.5);
              g_free(text);
              gtk_table_attach(GTK_TABLE(preview->table), wd, 0, 1,
                               iter.iElement + 1, iter.iElement + 2,
                               GTK_FILL, GTK_SHRINK, 2, 0);
              wd = gtk_label_new("");
              if (iter.nStoredNodes > 1)
                text = g_markup_printf_escaped
                  (_("<span size=\"small\">%d nodes</span>"),
                   iter.nStoredNodes);
              else if (iter.nStoredNodes == 1)
                text = g_strdup(_("<span size=\"small\">1 node</span>"));
              else
                text = g_strdup_printf("negative node number %d", iter.nStoredNodes);
              gtk_label_set_markup(GTK_LABEL(wd), text);
              gtk_misc_set_alignment(GTK_MISC(wd), 0., 0.5);
              g_free(text);
              gtk_table_attach(GTK_TABLE(preview->table), wd, 1, 2,
                               iter.iElement + 1, iter.iElement + 2,
                               GTK_FILL | GTK_EXPAND, GTK_SHRINK, 2, 0);
            }
          if (comment && comment[0])
            {
              wd = gtk_label_new(_("<i>Description:</i>"));
              gtk_label_set_use_markup(GTK_LABEL(wd), TRUE);
              gtk_misc_set_alignment(GTK_MISC(wd), 0., 0.5);
              gtk_table_attach(GTK_TABLE(preview->table), wd, 0, 2,
                               iter.nElements + 2, iter.nElements + 3,
                               GTK_FILL | GTK_EXPAND, GTK_SHRINK, 2, 5);
              wd = gtk_label_new("");
              text = g_markup_printf_escaped("<span size=\"small\">%s</span>",
                                             comment);
              gtk_label_set_markup(GTK_LABEL(wd), text);
              g_free(text);
              gtk_misc_set_alignment(GTK_MISC(wd), 0., 0.5);
              gtk_label_set_justify(GTK_LABEL(wd), GTK_JUSTIFY_FILL);
              gtk_label_set_line_wrap(GTK_LABEL(wd), TRUE);
              gtk_widget_set_size_request(wd, 150, -1);
              gtk_table_attach(GTK_TABLE(preview->table), wd, 0, 2,
                               iter.nElements + 3, iter.nElements + 4,
                               GTK_FILL | GTK_EXPAND, GTK_SHRINK, 2, 0);
            }
          gtk_box_pack_start(GTK_BOX(preview->vbox), preview->table,
                             FALSE, FALSE, 0);
          gtk_widget_show_all(preview->table);
          g_object_unref(nodes);
          g_object_unref(box);
          g_object_unref(bg);
        }
    }
  /* Set the rendering window current for OpenGL. */
  visu_ui_rendering_window_setCurrent(currentWindow, TRUE);

  /* We free the pixmap context. */
  visu_pixmap_context_free(dumpData);

  /* We free the VisuData. */
  g_object_unref(data);
  g_object_unref(preview->view);
  DBG_fprintf(stderr, "Visu Gtk: end of preview creation.\n");

  return FALSE;
}
/**
 * visu_ui_preview_update:
 * @preview: a location on #VisuUiPreview (initialised) ;
 * @filenames: an array of filenames.
 *
 * This routine update the content of the given @preview by making a
 * rendering in a pixmap of the given filenames. The number of given
 * filenames must match the number required by the current rendering
 * method. Only the nodes, the box and the background (plus fog) is
 * actually previewed.
 */
void visu_ui_preview_update(VisuUiPreview *preview, const char *filenames[])
{
  VisuRendering *method;
  gint nFiles, nb;
  VisuData *data;
  VisuGlView *view;

  DBG_fprintf(stderr, "Visu Gtk: update preview with given filenames.\n");
  if (preview->table)
    {
      gtk_widget_destroy(preview->table);
      preview->table = (GtkWidget*)0;
    }
  if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(preview->check)))
    {
      gtk_image_set_from_pixbuf(GTK_IMAGE(preview->image), (GdkPixbuf*)0);
      return;
    }

  method = visu_object_getRendering(VISU_OBJECT_INSTANCE);
  g_return_if_fail(method);

  nb = visu_rendering_getNFileTypes(method);
  for (nFiles = 0; filenames[nFiles]; nFiles++);

  if (nFiles < nb)
    gtk_image_set_from_pixbuf(GTK_IMAGE(preview->image), (GdkPixbuf*)0);
  else
    {
      view = visu_gl_view_new_withSize(150, 150);
      data = visu_data_new();
      for (nFiles = 0; filenames[nFiles]; nFiles++)
	visu_data_addFile(data, (char*)filenames[nFiles], nFiles, (ToolFileFormat*)0);
      DBG_fprintf(stderr, "Visu Gtk: create a local"
                  " VisuData %p for preview %p.\n", (gpointer)data, (gpointer)preview);

      preview->data = data;
      preview->view = view;
      preview_update((gpointer)preview);
    }
}
static void onPreviewToggled(GtkToggleButton *button, gpointer data _U_)
{
  usePreview = gtk_toggle_button_get_active(button);
}
/**
 * visu_ui_preview_add:
 * @preview: a location on #VisuUiPreview (uninitialised) ;
 * @chooser: the filechooser the preview must be attached to.
 *
 * Create the widgets to have a preview attached to @filechooser. But
 * the signal raised by @filechooser when previewing is needed is not
 * attached and this must be done by the user with custom routine.
 *
 * Returns: @preview itself.
 */
VisuUiPreview* visu_ui_preview_add(VisuUiPreview *preview, GtkFileChooser *chooser)
{
  GtkWidget *wd, *frame;

  g_return_val_if_fail(preview, (VisuUiPreview*)0);

  preview->vbox = gtk_vbox_new(FALSE, 0);
  preview->check = gtk_check_button_new_with_mnemonic(_("_Preview:"));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(preview->check), usePreview);
  g_signal_connect(G_OBJECT(preview->check), "toggled",
		   G_CALLBACK(onPreviewToggled), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(preview->vbox), preview->check, FALSE, FALSE, 5);
  wd = gtk_alignment_new(0.5, 0.5, 0., 0.);
  gtk_box_pack_start(GTK_BOX(preview->vbox), wd, TRUE, TRUE, 0);
  frame = gtk_frame_new(NULL);
  gtk_widget_set_size_request(frame, 150, 150);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_ETCHED_IN);
  gtk_container_add(GTK_CONTAINER(wd), frame);
  preview->image = gtk_image_new();
  gtk_container_add(GTK_CONTAINER(frame), preview->image);
  preview->table = NULL;
  gtk_widget_show_all(preview->vbox);
  
  gtk_file_chooser_set_preview_widget(chooser, preview->vbox);
  gtk_file_chooser_set_use_preview_label(chooser, FALSE);
  gtk_file_chooser_set_preview_widget_active(chooser, TRUE);

  return preview;
}

static void update_preview(GtkFileChooser *chooser, VisuUiPreview *preview)
{
  const char *filenames[2];

  filenames[0] = gtk_file_chooser_get_preview_filename(chooser);
  filenames[1] = (char*)0;

  /* We test if the selected filename is a directory or not. */
  if (filenames[0] && !g_file_test(filenames[0], G_FILE_TEST_IS_DIR))
    visu_ui_preview_update(preview, filenames);
  if (filenames[0])
    g_free((char*)filenames[0]);
}
/**
 * visu_ui_getFileFromDefaultFileChooser:
 * @data: an allocated #VisuData to store the file names ;
 * @parent: the parent window of the dialog.
 *
 * This method is a default method to launch a file chooser that select
 * a single file, with the filters of the current RenderingMethod. It creates a
 * modal dialog, and wait for its reponse. If OK is clicked, the selected
 * filename is stored. If Cancel is clicked, nothing is done. The
 * dialog is destroyed after use.
 *
 * Returns: FALSE if no file has been set, TRUE if the calling method should
 *          call visu_ui_rendering_window_loadFile().
 */
gboolean visu_ui_getFileFromDefaultFileChooser(VisuData *data, GtkWindow *parent)
{
  GtkWidget *fileSelection;
  GList *filters, *tmpLst;
  VisuRendering *method;
  gchar* directory, *filename;
  ToolFileFormat *selectedFormat;
  GtkFileFilter *filterDefault;
  gboolean res;
  VisuUiPreview preview;

  g_return_val_if_fail(data, FALSE);

  DBG_fprintf(stderr, "Visu Gtk: default filechooser for file opening.\n");

  method = visu_object_getRendering(VISU_OBJECT_INSTANCE);
  g_return_val_if_fail(method, FALSE);

  fileSelection = gtk_file_chooser_dialog_new(_("Load session"),
					      GTK_WINDOW(parent),
					      GTK_FILE_CHOOSER_ACTION_OPEN,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OPEN, GTK_RESPONSE_OK,
					      NULL);
  if (gtkFileChooserWidth > 0 || gtkFileChooserHeight > 0)
    gtk_window_set_default_size(GTK_WINDOW(fileSelection), gtkFileChooserWidth,
				gtkFileChooserHeight);
  directory = visu_ui_getLastOpenDirectory();
  if (directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(fileSelection), directory);

  gtk_widget_set_name(fileSelection, "filesel");
  gtk_window_set_position(GTK_WINDOW(fileSelection), GTK_WIN_POS_CENTER_ON_PARENT);
  gtk_window_set_modal(GTK_WINDOW(fileSelection), TRUE);

  /* Create and add the filters. */
  tmpLst = visu_rendering_getFileFormat(method, 0);
  filters = visu_ui_createFilter(tmpLst, fileSelection);
  g_list_free(tmpLst);

  /* Create and add the preview. */
  visu_ui_preview_add(&preview, GTK_FILE_CHOOSER(fileSelection));
  g_signal_connect(GTK_FILE_CHOOSER(fileSelection), "update-preview",
		   G_CALLBACK(update_preview), &preview);

  if (gtk_dialog_run(GTK_DIALOG(fileSelection)) == GTK_RESPONSE_OK)
    {
      /* Get the selected filter. */
      selectedFormat = (ToolFileFormat*)0;
      filterDefault = gtk_file_chooser_get_filter(GTK_FILE_CHOOSER(fileSelection));
      tmpLst = filters;
      while(tmpLst)
	{
	  if (filterDefault == ((VisuUiFileFilter*)tmpLst->data)->gtkFilter)
	    selectedFormat = ((VisuUiFileFilter*)tmpLst->data)->visuFilter;
	  tmpLst = g_list_next(tmpLst);
	}
      filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(fileSelection));
      visu_data_addFile(data, filename, 0, selectedFormat);
      g_free(filename);
      res = TRUE;
    }
  else
    res = FALSE;

  directory =
    gtk_file_chooser_get_current_folder(GTK_FILE_CHOOSER(fileSelection));
  visu_ui_setLastOpenDirectory((char*)directory, VISU_UI_DIR_FILE);
  g_free(directory);

  gtk_window_get_size(GTK_WINDOW(fileSelection), &gtkFileChooserWidth,
		      &gtkFileChooserHeight);

  gtk_widget_destroy(fileSelection);

  /* Free the filters list. */
  tmpLst = filters;
  while(tmpLst)
    {
      g_free(tmpLst->data);
      tmpLst = g_list_next(tmpLst);
    }
  g_list_free(filters);
  DBG_fprintf(stderr, "Visu Gtk: free load dialog OK.\n");

  return res;
}
static void onDataRendered(VisuObject *obj _U_, VisuData *dataObj,
                           VisuGlView *view _U_, gpointer data)
{
  visu_ui_setWindowTitle(GTK_WINDOW(data), dataObj);
}
/**
 * visu_ui_setWindowTitle:
 * @window: a #GtkWindow object.
 * @dataObj: a #VisuData object.
 *
 * Change the title of the given window, according to @dataObj
 * (setting usually the name of the loaded file).
 *
 * Since: 3.7
 **/
void visu_ui_setWindowTitle(GtkWindow *window, const VisuData *dataObj)
{
  gchar *file;

  if (dataObj)
    {
      file = visu_data_getFilesAsLabel(dataObj);
      if (!file)
        {
          g_warning("Can't find the filename to label the rendering window.");
          file = g_strdup(_("No filename"));
        }
      gtk_window_set_title(window, file);
      g_free(file);
    }
  else
    gtk_window_set_title(window, _("No file loaded"));
}
/**
 * visu_ui_createInterface:
 * @panel: always NULL here.
 * @renderWindow: a location for a #GtkWindow ;
 * @renderArea: a location for a #GtkWidget.
 *
 * A convenient routine that creates a #VisuUiRenderingWindow alone. To
 * create also a command panel, visu_ui_main_class_createMain() should be
 * used instead.
 */
void visu_ui_createInterface(GtkWindow **panel,
                             GtkWindow **renderWindow, GtkWidget **renderArea)
{
  int width, height;

  /* Force the creation of the Scale class. */
  commandLineGet_XWindowGeometry(&width, &height);
  DBG_fprintf(stderr,"Visu Gtk: create a rendering window (%dx%d).\n",
	      width, height);
  *renderArea = visu_ui_rendering_window_new(width, height, FALSE, TRUE);
  *renderWindow = GTK_WINDOW(visu_ui_buildRenderingWindow(VISU_UI_RENDERING_WINDOW(*renderArea)));
  g_signal_connect(G_OBJECT(*renderWindow), "delete-event",
		   G_CALLBACK(gtk_main_quit), (gpointer)0);
  g_signal_connect(G_OBJECT(*renderWindow), "destroy-event",
		   G_CALLBACK(gtk_main_quit), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataRendered",
                   G_CALLBACK(onDataRendered), (gpointer)(*renderWindow));
  gtk_widget_show(GTK_WIDGET(*renderWindow));
  *panel = (GtkWindow*)0;

  return;
}
static gboolean onFocus(GtkWidget *wd _U_, GdkEvent *event _U_, gpointer data)
{
  visu_ui_rendering_window_setCurrent(VISU_UI_RENDERING_WINDOW(data), FALSE);

  return FALSE;
}
/**
 * visu_ui_buildRenderingWindow:
 * @renderWindow: a #VisuUiRenderingWindow object.
 *
 * Create a #GtkWindow with V_sim render window icon and wmclass set
 * to "V_Sim:v_sim_render". It also set the accelerators of
 * @renderWindow to the newly created window and pack @renderWindow inside.
 *
 * Returns: (transfer full): a newly created #GtkWindow.
 *
 * Since: 3.7
 **/
GtkWidget* visu_ui_buildRenderingWindow(VisuUiRenderingWindow *renderWindow)
{
  GtkWidget *window;
  gchar *windowRef = "V_Sim";
  gchar *window_name = "v_sim_render";
  gchar *class_name = "V_Sim";
  GdkPixbuf *iconPixBuf;

  /* We create the rendering window. */
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(window), windowRef);
#if GTK_MAJOR_VERSION > 2
  gtk_window_set_has_resize_grip(GTK_WINDOW(window), FALSE);
#endif
  gtk_window_set_wmclass(GTK_WINDOW(window), window_name, class_name);
  iconPixBuf = gdk_pixbuf_new_from_xpm_data((const char**)icone_observe_xpm);
  gtk_window_set_icon(GTK_WINDOW(window), iconPixBuf);
  g_object_unref(iconPixBuf);
  gtk_window_add_accel_group(GTK_WINDOW(window),
                             visu_ui_rendering_window_getAccelGroup(renderWindow));

  gtk_container_add(GTK_CONTAINER(window), GTK_WIDGET(renderWindow));
  g_signal_connect(G_OBJECT(window), "enter-notify-event",
                   G_CALLBACK(onFocus), (gpointer)renderWindow);

  return window;
}

static void initVisuGtk(void)
{
  VisuConfigFileEntry *entry;

  /* Retrieve the current working directory. */
  visuGtkLastDir = g_get_current_dir();

  /* Set private variables. */
  usePreview = TRUE;
  entry = visu_config_file_addBooleanEntry(VISU_CONFIG_FILE_PARAMETER,
                                           FLAG_PARAMETER_PREVIEW,
                                           DESC_PARAMETER_PREVIEW,
                                           &usePreview);
  visu_config_file_entry_setVersion(entry, 3.4f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_PARAMETER,
				   exportParameters);
  visuGtkLoadMethods = g_hash_table_new_full(g_direct_hash, g_direct_equal,
					     NULL, g_free);
}
/**
 * visu_ui_mainCreate:
 * @panelFunc: (scope call): function to be called to create the different windows.
 *
 * It initializses the GTK part of V_Sim. During this initialisation,
 * the @panelFunc is called. It should create all the windows needed
 * by V_Sim, like the command panel and the rendering area. The return
 * widget is the widget returned itself by @panelFunc. It must be the
 * main widget: it is the command panel if used, the window containing
 * the rendering area if no command panel or the rendering area itself
 * if no container window.
 *
 * Returns: the main widget for V_Sim, returned itself by @panelFunc.
 */
void visu_ui_mainCreate(VisuUiInitWidgetFunc panelFunc)
{
  gchar *message;

  g_return_if_fail(panelFunc);

  if (!visuGtkLoadMethods)
    initVisuGtk();

  panelFunc(&visuGtkPanel, &visuGtkRender, &visuGtkRenderArea);
  g_return_if_fail(visuGtkRender && visuGtkRenderArea);

  DBG_fprintf(stderr,"--- Initialising plugins ---\n");
  visu_plugins_init();

  message = visu_basic_parseConfigFiles(visu_ui_rendering_window_getGlView(VISU_UI_RENDERING_WINDOW(visuGtkRenderArea)));
  if (message)
    {
      visu_ui_raiseWarningLong(_("Reading the configuration files"), message,
			       visuGtkRender);
      g_free(message);
    }

  return;
}
/**
 * visu_ui_runCommandLine:
 * @data: a pointer to the command panel (see #VisuUiMain).
 *
 * Call the get routines from the command line module and deal with
 * them. This method is not aware of the panels and is intended to be
 * called only when the command panel is not used. In the opposite
 * case, use visu_ui_main_runCommandLine() instead.
 *
 * Returns: FALSE always.
 */
gboolean visu_ui_runCommandLine(gpointer data _U_)
{
  VisuData *obj;
  VisuGlView *view;
  VisuBasicCLISet *set;
  GError *error;

  obj = visu_ui_rendering_window_getData(VISU_UI_RENDERING_WINDOW(visuGtkRenderArea));
  view = visu_ui_rendering_window_getGlView(VISU_UI_RENDERING_WINDOW(visuGtkRenderArea));
  if (!obj || !view)
    return FALSE;

  set = g_malloc(sizeof(VisuBasicCLISet));
  error = (GError*)0;
  if (!visu_basic_applyCommandLine(obj, view, set, &error))
    {
      visu_ui_raiseWarning(_("Parsing command line"), error->message, (GtkWindow*)0);
      g_error_free(error);
    }
  else
    visu_basic_createExtensions(obj, view, set, TRUE);

  VISU_REDRAW_ADD;
  
  return FALSE;
}
/**
 * visu_ui_createFilter:
 * @list: (element-type ToolFileFormat*): a GList of #ToolFileFormat ;
 * @fileChooser: a file chooser to associate filters with.
 *
 * Create a list of GtkFileFilter created from the given list of file formats
 * and attach it to the given @fileChooser.
 *
 * Returns: (element-type VisuUiFileFilter) (transfer full): a list of
 * #VisuUiFileFilter. This list should be freed after use.
 */
GList* visu_ui_createFilter(GList *list, GtkWidget *fileChooser)
{
  GtkFileFilter *filter, *filterAll;
  GList *tmpLst;
  const GList *tmpLst2;
  const char *name;
  VisuUiFileFilter *data;
  GList *returnedFilters;

  DBG_fprintf(stderr, "Visu Gtk: creating list of filters.\n");
  returnedFilters = (GList*)0;
  filterAll = gtk_file_filter_new ();
  gtk_file_filter_set_name(filterAll, _("All supported formats"));
  for (tmpLst = list; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      filter = gtk_file_filter_new ();
      name = tool_file_format_getLabel((ToolFileFormat*)tmpLst->data);
      if (name)
	gtk_file_filter_set_name(filter, name);
      else
	gtk_file_filter_set_name(filter, _("No description"));
      for (tmpLst2 = tool_file_format_getFilePatterns((ToolFileFormat*)tmpLst->data);
           tmpLst2; tmpLst2 = g_list_next(tmpLst2))
	{
	  gtk_file_filter_add_pattern (filter, (gchar*)tmpLst2->data);
	  gtk_file_filter_add_pattern (filterAll, (gchar*)tmpLst2->data);
	}
      data = g_malloc(sizeof(VisuUiFileFilter));
      data->gtkFilter = filter;
      data->visuFilter = (ToolFileFormat*)tmpLst->data;
      returnedFilters = g_list_append(returnedFilters, (gpointer)data);
    }
  data = g_malloc(sizeof(VisuUiFileFilter));
  data->gtkFilter = filterAll;
  data->visuFilter = (ToolFileFormat*)0;
  returnedFilters = g_list_append(returnedFilters, (gpointer)data);
  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name(filter, _("All files"));
  gtk_file_filter_add_pattern (filter, "*");
  data = g_malloc(sizeof(VisuUiFileFilter));
  data->gtkFilter = filter;
  data->visuFilter = (ToolFileFormat*)0;
  returnedFilters = g_list_append(returnedFilters, (gpointer)data);
  
  DBG_fprintf(stderr, "Gtk Main : attach list to the given filechooser.\n");
  tmpLst = returnedFilters;
  while(tmpLst)
    {
      gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(fileChooser),
				  ((VisuUiFileFilter*)tmpLst->data)->gtkFilter);
      tmpLst = g_list_next(tmpLst);
    }
  gtk_file_chooser_set_filter(GTK_FILE_CHOOSER(fileChooser), filterAll);

  return returnedFilters;
}

static void exportParameters(GString *data, VisuData *dataObj _U_, VisuGlView *view _U_)
{
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_PREVIEW);
  g_string_append_printf(data, "%s[gtk]: %d\n\n", FLAG_PARAMETER_PREVIEW,
			 usePreview);
}

/**
 * visu_ui_getLastOpenDirectory:
 *
 * V_Sim stores the last open directory to set the file chooser to
 * this one the next time it will come. Use
 * visu_ui_setLastOpenDirectory() to store it after a GTK_RESPONSE_OK
 * has been returned by a file chooser and then initialise each new
 * with this routine.
 *
 * Returns: (transfer none): a string owned by V_Sim.
 */
char* visu_ui_getLastOpenDirectory(void)
{
  DBG_fprintf(stderr, "Visu Gtk: get the last open directory : '%s'.\n",
	      visuGtkLastDir);
  return visuGtkLastDir;
}
/**
 * visu_ui_setLastOpenDirectory:
 * @directory: a full path to a directory ;
 * @type: the kind of directory to set the remember flag on.
 *
 * V_Sim stores the last open directory to set the file chooser to
 * this one the next time it will come. Use this routine each time a
 * file chooser returns GTK_RESPONSE_OK. The given string will be
 * copied and can be freed after use.
 */
void visu_ui_setLastOpenDirectory(const char* directory, VisuUiDirectoryType type)
{
  if (visuGtkLastDir)
    g_free(visuGtkLastDir);

  if (!g_path_is_absolute(directory))
    visuGtkLastDir = g_build_filename(g_get_current_dir(),
					 directory, NULL);
  else
    visuGtkLastDir = g_build_filename(directory, NULL);
  DBG_fprintf(stderr, "Visu Gtk: set the last open directory to '%s', emit signal.\n",
	      visuGtkLastDir);

  g_signal_emit_by_name(VISU_OBJECT_INSTANCE, "DirectoryChanged", type, NULL);
  DBG_fprintf(stderr, "Visu Gtk: emission done (DirectoryChanged).\n");
}
/**
 * visu_ui_createPixbuf:
 * @filename: (type filename): a file name (must be a base name).
 *
 * Replace the create_pixbuf() routine from Glade. It looks only in
 * the default pixmap directory of V_Sim to find the given file.
 *
 * Returns: (transfer full): a newly created GdkPixbuf on success.
 */
GdkPixbuf* visu_ui_createPixbuf(const gchar *filename)
{
  gchar *pathname = NULL;
  GdkPixbuf *pixbuf;
  GError *error = NULL;

  g_return_val_if_fail(filename && filename[0], (GdkPixbuf*)0);

  pathname = g_build_filename(V_SIM_PIXMAPS_DIR, filename, NULL);

  pixbuf = gdk_pixbuf_new_from_file(pathname, &error);
  if (!pixbuf)
    {
      g_warning(_("failed to load pixbuf file '%s': %s\n"),
		pathname, error->message);
      g_error_free(error);
    }
  g_free(pathname);

  return pixbuf;
}

/**
 * visu_ui_getRenderWindow:
 *
 * A convenient function to get the rendering area window.
 *
 * Returns: (transfer none): a #GtkWindow.
 */
GtkWindow* visu_ui_getRenderWindow(void)
{
  return visuGtkRender;
}
/**
 * visu_ui_getPanel:
 *
 * A convenient function to get the command panel window.
 *
 * Returns: (transfer none): a #GtkWindow.
 */
GtkWindow* visu_ui_getPanel(void)
{
  return visuGtkPanel;
}
/**
 * visu_ui_getRenderWidget:
 *
 * A convenient function to get the rendering area widget.
 *
 * Returns: (transfer none): a #GtkWidget.
 */
GtkWidget* visu_ui_getRenderWidget(void)
{
  return visuGtkRenderArea;
}
/**
 * visu_ui_setRenderWidget:
 * @render: a #VisuUiRenderingWindow widget.
 *
 * A convenient function to set the rendering area widget.
 *
 * Since: 3.7
 */
void visu_ui_setRenderWidget(VisuUiRenderingWindow *render)
{
  visuGtkRenderArea = GTK_WIDGET(render);
}

/* Store a tree model to remember colors. */
#define COLOR_BOX_WIDTH  16
#define COLOR_BOX_HEIGHT 16
#define COLOR_BOX_BITS   8
/**
 * tool_color_get_stamp:
 * @color: a #ToolColor object ;
 * @alpha: a boolean.
 *
 * This method is used by #VisuUiColorCombobox object to create little stamps
 * representing the color. If the pixbuf of such stamps are needed, use
 * visu_ui_color_combobox_getPixbufFromColor() if the color is registered in an
 * already existing #VisuUiColorCombobox object or use this method to create
 * a new stamp.
 *
 * Returns: (transfer full): a pixbuf pointer corresponding to the
 * little image as shown on a @colorComboBox (use g_object_unref() to
 * free this pixbuf).
 *
 * Since: 3.7
 */
GdkPixbuf* tool_color_get_stamp(const ToolColor *color, gboolean alpha)
{
  GdkPixbuf *pixbufColorBox;
  int rowstride, x, y;
  guchar *pixels, *p;
  float grey;

  pixbufColorBox = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE,
				  COLOR_BOX_BITS,
				  COLOR_BOX_WIDTH,
				  COLOR_BOX_HEIGHT);
  rowstride = gdk_pixbuf_get_rowstride(pixbufColorBox);
  pixels = gdk_pixbuf_get_pixels(pixbufColorBox);
  for (y = 0; y < COLOR_BOX_HEIGHT; y++)
    for (x = 0; x < COLOR_BOX_WIDTH; x++)
      {
	p = pixels + y * rowstride + x * 3;
	if (x < COLOR_BOX_WIDTH / 2)
	  {
	    if (y < COLOR_BOX_HEIGHT / 2)
	      grey = 0.75;
	    else
	      grey = 0.5;
	  }
	else
	  {
	    if (y < COLOR_BOX_HEIGHT / 2)
	      grey = 0.5;
	    else
	      grey = 0.75;
	  }
	if (alpha)
	  {
	    p[0] = (guchar)((color->rgba[0] * color->rgba[3] +
			     (1. - color->rgba[3]) * grey) * 255.);
	    p[1] = (guchar)((color->rgba[1] * color->rgba[3] +
			     (1. - color->rgba[3]) * grey) * 255.);
	    p[2] = (guchar)((color->rgba[2] * color->rgba[3] +
			     (1. - color->rgba[3]) * grey) * 255.);
	  }
	else
	  {
	    p[0] = (guchar)(color->rgba[0] * 255.);
	    p[1] = (guchar)(color->rgba[1] * 255.);
	    p[2] = (guchar)(color->rgba[2] * 255.);
	  }
      }
  return pixbufColorBox;
}
