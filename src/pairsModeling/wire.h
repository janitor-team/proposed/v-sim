/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef WIRE_H
#define WIRE_H

#include <visu_pairs.h>
#include <extensions/pairs.h>
#include <coreTools/toolShade.h>

VisuPairExtension* visu_gl_pairs_wire_init();

VisuPairExtension* visu_gl_pairs_wire_getStatic();

gboolean   visu_gl_pairs_wire_setGeneralWidth(guint val);
guint      visu_gl_pairs_wire_getGeneralWidth();
gboolean   visu_gl_pairs_wire_setWidth(VisuPairLink *data, guint val);
guint      visu_gl_pairs_wire_getWidth(VisuPairLink *data);
gboolean   visu_gl_pairs_wire_setShade(VisuPairLink *data, ToolShade *shade);
ToolShade* visu_gl_pairs_wire_getShade(VisuPairLink *data);
gboolean   visu_gl_pairs_wire_setStipple(VisuPairLink *data, guint16 stipple);
guint16    visu_gl_pairs_wire_getStipple(VisuPairLink *data);


#endif
