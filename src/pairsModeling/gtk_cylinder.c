/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "gtk_cylinder.h"

#include <gtk/gtk.h>

#include <visu_object.h>
#include <visu_tools.h>
#include <gtk_pairs.h>
#include <extensions/pairs.h>

#include "cylinder.h"

/**
 * SECTION: gtk_cylinder
 * @short_description: The additional widgets displayed in the pair
 * dialog to handle cylinder pairs.
 *
 * <para>Some additional widgets to be displayed in the pairs dialog
 * window. It contains a tool to choose the colourisation scheme of
 * pairs and the radius of cylinder representation.</para>
 */

int colorForCurve;
int colorDescr;

GtkWidget *spinCylinderRadius;
gulong signalSpinCylinderRadiusId;
GtkWidget *radioCylinderUser, *radioCylinderElement;

#define RESOURCES_CYLINDER_RADIUS_STEP 0.02

/* Callbacks */
void changeCylinderRadius(GtkSpinButton *spin, gpointer data);
void changeCylinderColorType(GtkToggleButton *button, gpointer data);
/* static void cylinderValuesChangedOnResources(GObject *obj, gpointer data); */

/**
 * visu_ui_pairs_cylinder_init: (skip)
 *
 * This routine is used by V_Sim to initialise the Gtk part of
 * cylinder pair model. This should be called once on start-up.
 */
void visu_ui_pairs_cylinder_init(void)
{
/*   g_signal_connect(VISU_OBJECT_INSTANCE, "resourcesLoaded", */
/* 		   G_CALLBACK(cylinderValuesChangedOnResources), (gpointer)0); */
}

/**
 * visu_ui_pairs_cylinder_initBuild: (skip)
 *
 * This routine create a container #GtkWidget that hold all widgets
 * necessary for handling cylinder pair model.
 *
 * Returns: a newly created #GtkWidget.
 */
GtkWidget* visu_ui_pairs_cylinder_initBuild()
{
  GtkWidget *hbox, *vbox;
  GtkWidget *label;
  GSList *radioCylinderUser_group = NULL;

  vbox = gtk_vbox_new(FALSE, 0);

  hbox = gtk_hbox_new(FALSE, 10);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  label = gtk_label_new(_("Radius:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);
  spinCylinderRadius = gtk_spin_button_new_with_range(VISU_GL_PAIRS_CYLINDER_RADIUS_MIN,
                                                      VISU_GL_PAIRS_CYLINDER_RADIUS_MAX,
                                                      RESOURCES_CYLINDER_RADIUS_STEP);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinCylinderRadius), visu_gl_pairs_cylinder_getGeneralRadius());
  gtk_box_pack_start(GTK_BOX(hbox), spinCylinderRadius, FALSE, FALSE, 0);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  label = gtk_label_new(_("Color:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);

  radioCylinderUser = gtk_radio_button_new_with_mnemonic(NULL, _("_user defined"));
  gtk_widget_set_name(radioCylinderUser, "message_radio");
  gtk_box_pack_start(GTK_BOX(hbox), radioCylinderUser, FALSE, FALSE, 0);
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(radioCylinderUser),
                             radioCylinderUser_group);
  radioCylinderUser_group =
    gtk_radio_button_get_group(GTK_RADIO_BUTTON(radioCylinderUser));
  if(visu_gl_pairs_cylinder_getColorType() == 0)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioCylinderUser), TRUE);

  radioCylinderElement = gtk_radio_button_new_with_mnemonic(NULL, _("_elements"));
  gtk_widget_set_name(radioCylinderElement, "message_radio");
  gtk_box_pack_start(GTK_BOX(hbox), radioCylinderElement, FALSE, FALSE, 0);
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(radioCylinderElement),
                             radioCylinderUser_group);
  if (visu_gl_pairs_cylinder_getColorType() == 1)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioCylinderElement), TRUE);

  signalSpinCylinderRadiusId =
    g_signal_connect(G_OBJECT(spinCylinderRadius), "value-changed",
                     G_CALLBACK(changeCylinderRadius), (gpointer)0);

  g_signal_connect(G_OBJECT(radioCylinderUser), "toggled",
		   G_CALLBACK(changeCylinderColorType), GINT_TO_POINTER(0));
  g_signal_connect(G_OBJECT(radioCylinderElement), "toggled",
		   G_CALLBACK(changeCylinderColorType), GINT_TO_POINTER(1));

  return vbox;
}

void changeCylinderRadius(GtkSpinButton *spin, gpointer data _U_)
{
  int res;
  VisuUiPairsIter iter;
  gchar* label;

  res = FALSE;
  /* Currently, the label depends only on the radius, so
     we can build only one label and use it every time. */
  label = (gchar*)0;
  /* We run on all selected pairs. */
  for (visu_ui_pairs_iter_startSelected(&iter); iter.data;
       visu_ui_pairs_iter_nextSelected(&iter))
    {
      /* We change the value for this pair. */
      res = visu_gl_pairs_cylinder_setRadius(iter.data,
			      (float)gtk_spin_button_get_value(spin)) || res;
      if (!label)
	label = visu_ui_pairs_cylinder_getValues(iter.data);
      /* We change the drawn label. */
      visu_ui_pairs_setSpecificLabels(&(iter.iter), label);
    }
  g_free(label);

  if (res)
    {
      visu_gl_ext_pairs_draw(visu_gl_ext_pairs_getDefault());
      VISU_REDRAW_ADD;
    }
}

void changeCylinderColorType(GtkToggleButton *button, gpointer data)
{
  int color;
  int res;

  if (!gtk_toggle_button_get_active(button))
    return;
  
  color = GPOINTER_TO_INT(data);
  g_return_if_fail(color >= 0 && color < VISU_GL_PAIRS_CYLINDER_N_COLOR);

  res = visu_gl_pairs_cylinder_setColorType(color);
  if (res)
    {
      visu_gl_ext_pairs_draw(visu_gl_ext_pairs_getDefault());
      VISU_REDRAW_ADD;
    }
}

/* static void cylinderValuesChangedOnResources(GObject *obj, gpointer data) */
/* { */
/*   int val; */

/*   if (!gtkPairs_isPairExtensionInUse(pointerToPairExtension_cylinder)) */
/*     return; */

/*   DBG_fprintf(stderr, "Gtk Cylinder : catch the 'resourcesLoaded' signal, updating values.\n"); */
/*   gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinCylinderRadius), visu_gl_pairs_cylinder_getGeneralRadius()); */
/*   val = visu_gl_pairs_cylinder_getColorType(); */
/*   switch (val) */
/*     { */
/*     case VISU_GL_PAIRS_CYLINDER_COLOR_USER: */
/*       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioCylinderUser), TRUE); */
/*       break; */
/*     case VISU_GL_PAIRS_CYLINDER_COLOR_ELEMENT: */
/*       gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioCylinderElement), TRUE); */
/*       break; */
/*     } */
/* } */

/**
 * visu_ui_pairs_cylinder_setValues:
 * @data: information about a pair.
 *
 * Change the widget values with the given information. This change trigger
 * no signals.
 */
void visu_ui_pairs_cylinder_setValues(VisuPairLink *data)
{
  g_signal_handler_block(G_OBJECT(spinCylinderRadius), signalSpinCylinderRadiusId);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinCylinderRadius),
                            visu_gl_pairs_cylinder_getRadius(data));
  g_signal_handler_unblock(G_OBJECT(spinCylinderRadius), signalSpinCylinderRadiusId);
}
/**
 * visu_ui_pairs_cylinder_getValues:
 * @data: information about a pair
 *
 * Create an internationalised UTF-8 label that describes the
 * information about a pair.
 *
 * Returns: (transfer full): a newly allocated string.
 */
gchar* visu_ui_pairs_cylinder_getValues(VisuPairLink *data)
{
  gchar *str;

  /* a.u. is for arbitrary units. */
  str = g_strdup_printf("%s %3.2f%s", _("cyl.:"),
                        visu_gl_pairs_cylinder_getRadius(data), _("a.u."));

  return str;
}
