/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef CYLINDER_H
#define CYLINDER_H

#include <visu_pairs.h>
#include <extensions/pairs.h>

/**
 * VISU_GL_PAIRS_CYLINDER_RADIUS_MIN:
 *
 * Minimum value for the radius of cylinder pairs.
 */
#define VISU_GL_PAIRS_CYLINDER_RADIUS_MIN 0.01f
/**
 * VISU_GL_PAIRS_CYLINDER_RADIUS_MAX:
 *
 * Maximum value for the radius of cylinder pairs.
 */
#define VISU_GL_PAIRS_CYLINDER_RADIUS_MAX 3.f

/**
 * VisuGlPairsCylinderColorId:
 * @VISU_GL_PAIRS_CYLINDER_COLOR_USER: color is chosen by the user.
 * @VISU_GL_PAIRS_CYLINDER_COLOR_ELEMENT: color is chosen according to
 * the color of the #VisuElement the pair is linked to.
 * @VISU_GL_PAIRS_CYLINDER_N_COLOR: number of choices for the colourisation.
 *
 * Possible flags to colourise the cylinder pairs.
 */
typedef enum
  {
    VISU_GL_PAIRS_CYLINDER_COLOR_USER,
    VISU_GL_PAIRS_CYLINDER_COLOR_ELEMENT,
    VISU_GL_PAIRS_CYLINDER_N_COLOR
  } VisuGlPairsCylinderColorId;

VisuPairExtension* visu_gl_pairs_cylinder_getStatic();

VisuPairExtension* visu_gl_pairs_cylinder_init();
gboolean visu_gl_pairs_cylinder_setGeneralRadius(float val);
float visu_gl_pairs_cylinder_getGeneralRadius();
gboolean visu_gl_pairs_cylinder_setRadius(VisuPairLink *data, float val);
float visu_gl_pairs_cylinder_getRadius(VisuPairLink *data);

gboolean visu_gl_pairs_cylinder_setColorType(VisuGlPairsCylinderColorId val);
VisuGlPairsCylinderColorId visu_gl_pairs_cylinder_getColorType();

#endif
