/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "wire.h"

#include <stdlib.h>
#include <math.h>

#include <GL/gl.h>
#include <GL/glu.h> 

#include <visu_tools.h>
#include <visu_data.h>
#include <visu_configFile.h>
#include <opengl.h>
#include <coreTools/toolColor.h>
#include <coreTools/toolConfigFile.h>

/**
 * SECTION:wire
 * @short_description:Gives methods to draw lines as OpenGl objects
 * pairing two elements.
 *
 * <para>The wire pair module is the simplest way to draw pairs
 * between elements : using lines. The size of lines can be chosen but
 * is limited by OpenGL implementation that usually only allow line
 * width of 10 pixels as maximum size.</para>
 */

static VisuPairExtension* pointerToPairExtension_wire;

/* This function details how to read what is needed by PairsData. */
static gboolean readWireGeneralWidth(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				     VisuData *dataObj, VisuGlView *view, GError **error);
static gboolean readWireWidth(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			      VisuData *dataObj, VisuGlView *view, GError **error);
static gboolean readLinkWidth(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			      VisuData *dataObj, VisuGlView *view, GError **error);
static gboolean readLinkStipple(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				VisuData *dataObj, VisuGlView *view, GError **error);
static gboolean readLinkShade(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                              VisuData *dataObj, VisuGlView *view, GError **error);
/* This function details how to read the parameter FLAG_PARAMETER_PAIRS. */
static gboolean readWireNonLinear(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				  VisuData *dataObj, VisuGlView *view, GError **error);
static gboolean readWireToolShade(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                  VisuData *dataObj, VisuGlView *view, GError **error);
/* This function save the resources. */
static void exportResourcesWire(GString *data, VisuData *dataObj, VisuGlView *view);

#define FLAG_PARAMETER_WIRE   "wire_nonLinear"
#define DESC_PARAMETER_WIRE   "If the color of the pairs are corrected by their length ; boolean 0 or 1"
#define PARAMETER_WIRE_DEFAULT 0

#define FLAG_PARAMETER_SHADE  "wire_shade"
#define DESC_PARAMETER_SHADE  "If positive, use a shade to colourise pairs depending on length ; -1 or a positive shade id"

#define FLAG_RESOURCES_SHADE  "pairWire_linkShade"
#define DESC_RESOURCES_SHADE  "For each given pair, if positive, use a shade to colourise pairs depending on length ; \"ele1\" \"ele2\" -1 or a positive shade id"

#define FLAG_RESOURCES_PAIR_WIDTH "pairWire_pairWidth"
#define DESC_RESOURCES_PAIR_WIDTH "Widths detail for each pair drawn ; \"ele1\" \"ele2\" 0 < integer < 10"
#define FLAG_RESOURCES_LINK_WIDTH "pairWire_linkWidth"
#define DESC_RESOURCES_LINK_WIDTH "Widths detail for each drawn link ; \"ele1\" \"ele2\" 0 < integer < 10"
#define FLAG_RESOURCES_WIRE_WIDTH "pairWire_width"
#define DESC_RESOURCES_WIRE_WIDTH "This value is the width for all pairs drawn ; \"ele1\" \"ele2\" 0 < integer < 10"
#define RESOURCES_WIRE_WIDTH_DEFAULT 2
#define RESOURCES_WIRE_WIDTH_MAX 10
static guint wireWidth;

#define FLAG_RESOURCES_LINK_STIPPLE "pairWire_linkStipple"
#define DESC_RESOURCES_LINK_STIPPLE "Dot scheme detail for each drawn link ; \"ele1\" \"ele2\" 0 < integer < 2^16"

/**
 * visu_gl_pairs_wire_getStatic:
 *
 * Internal function to access the #VisuPairExtension wire.
 * 
 * Since: 3.6
 *
 * Returns: (transfer none): the #VisuPairExtension wire.
 */
VisuPairExtension* visu_gl_pairs_wire_getStatic()
{
  return pointerToPairExtension_wire;
}

static void beginPairLink(VisuElement *ele1 _U_, VisuElement *ele2 _U_,
			  VisuPairLink *data, VisuGlView *view _U_)
{
  ToolColor *color;
  int *width;
  guint16 *stipple;

  width = (int*)g_object_get_data(G_OBJECT(data), "width");
  if (width)
    glLineWidth(*width);
  else
    glLineWidth(wireWidth);
  color = visu_pair_link_getColor(data);
  glColor3fv(color->rgba);
  stipple = (guint16*)g_object_get_data(G_OBJECT(data), "stipple");
  if (stipple)
    {
      glEnable(GL_LINE_STIPPLE);
      glLineStipple(1, *stipple);
    }
  glDisable(GL_LIGHTING);
  glDisable(GL_DITHER);
}

static void endPairLink(VisuElement *ele1 _U_, VisuElement *ele2 _U_,
			VisuPairLink *data _U_, VisuGlView *view _U_)
{
  glDisable(GL_LINE_STIPPLE);
  glEnable(GL_DITHER); /* WARNING: it is the default! */
  glEnable(GL_LIGHTING);
}

static void drawPairLink(VisuElement *ele1 _U_, VisuElement *ele2 _U_,
			 VisuPairLink *data, VisuGlView *view _U_,
			 double x1, double y1, double z1,
			 double x2, double y2, double z2, float d2, float alpha)
{
  float ratio;
  ToolColor *color;
  float rgba[4], mM[2];
  ToolShade *shade;

  shade = (ToolShade*)g_object_get_data(G_OBJECT(data), "shade");
  if (shade)
    {
      mM[0] = visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MIN);
      mM[1] = visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MAX);
      ratio = (sqrt(d2) - mM[0]) / (mM[1] - mM[0]);
      tool_shade_valueToRGB(shade, rgba, ratio);
      rgba[3] = alpha;
      glColor4fv(rgba);
    }
  else
    {
      color = visu_pair_link_getColor(data);
      glColor4f(color->rgba[0],
                color->rgba[1],
                color->rgba[2],
                alpha);
    }

  glBegin(GL_LINES);
  glVertex3d(x1, y1, z1);
  glVertex3d(x2, y2, z2);
  glEnd();
}

/**
 * visu_gl_pairs_wire_init:
 *
 * This method is used by V_Sim on startup, don't use it on your own.
 *
 * Returns: the wire pair extension.
 */
VisuPairExtension* visu_gl_pairs_wire_init()
{
  char *name = _("Wire pairs");
  char *desc = _("Pairs are rendered by flat lines."
		 " The color and the width can by chosen.");
  VisuPairExtension *extension;
  VisuConfigFileEntry *resourceEntry, *oldEntry, *olderEntry;
  VisuPairDrawFuncs meth;

  meth.start = beginPairLink;
  meth.stop  = endPairLink;
  meth.main  = drawPairLink;
  extension = visu_pair_extension_new("Wire pairs", name, desc, FALSE, &meth);

  wireWidth = RESOURCES_WIRE_WIDTH_DEFAULT;

  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_RESOURCES_WIRE_WIDTH,
					  DESC_RESOURCES_WIRE_WIDTH,
					  1, readWireGeneralWidth);
  oldEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
				     FLAG_RESOURCES_PAIR_WIDTH,
				     DESC_RESOURCES_PAIR_WIDTH,
				     1, readWireWidth);
  visu_config_file_entry_setVersion(resourceEntry, 3.1f);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_RESOURCES_LINK_WIDTH,
					  DESC_RESOURCES_LINK_WIDTH,
					  1, readLinkWidth);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  visu_config_file_entry_setReplace(resourceEntry, oldEntry);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_RESOURCES_LINK_STIPPLE,
					  DESC_RESOURCES_LINK_STIPPLE,
					  1, readLinkStipple);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                     exportResourcesWire);
  olderEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
                                         FLAG_PARAMETER_WIRE,
                                         DESC_PARAMETER_WIRE,
                                         1, readWireNonLinear);
  oldEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
                                       FLAG_PARAMETER_SHADE,
                                       DESC_PARAMETER_SHADE,
                                       1, readWireToolShade);
  visu_config_file_entry_setVersion(oldEntry, 3.6f);
  visu_config_file_entry_setReplace(oldEntry, olderEntry);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
                                            FLAG_RESOURCES_SHADE,
                                            DESC_RESOURCES_SHADE,
                                            1, readLinkShade);
  visu_config_file_entry_setVersion(resourceEntry, 3.7f);
  visu_config_file_entry_setReplace(resourceEntry, oldEntry);

  pointerToPairExtension_wire = extension;
  return extension;
}
/**
 * visu_gl_pairs_wire_setStipple:
 * @data: a #VisuPairLink object ;
 * @stipple: a pattern.
 *
 * Change the line pattern of @data.
 *
 * Returns: TRUE if the value is different from previous.
 */
gboolean visu_gl_pairs_wire_setStipple(VisuPairLink *data, guint16 stipple)
{
  guint16 *stipple_;

  g_return_val_if_fail(data && stipple, FALSE);

  DBG_fprintf(stderr, "Pairs Wire: set the wire pattern to %d.\n", stipple);
  stipple_ = (guint16*)g_object_get_data(G_OBJECT(data), "stipple");
  if (!stipple_)
    {
      stipple_ = g_malloc(sizeof(guint16));
      g_object_set_data_full(G_OBJECT(data), "stipple", (gpointer)stipple_, g_free);
      *stipple_ = stipple + 1;
    }
  if (*stipple_ != stipple)
    {
      *stipple_ = stipple;
      if (visu_pair_link_isDrawn(data))
        g_signal_emit_by_name(G_OBJECT(data), "ParameterChanged", NULL);
      return TRUE;
    }

  return FALSE;
}
/**
 * visu_gl_pairs_wire_getStipple:
 * @data: a #VisuPairLink object.
 *
 * Get the line pattern of @data.
 *
 * Returns: a line pattern (default is 65535).
 */
guint16 visu_gl_pairs_wire_getStipple(VisuPairLink *data)
{
  guint16 *stipple_;

  g_return_val_if_fail(data, (guint16)1);

  stipple_ = g_object_get_data(G_OBJECT(data), "stipple");
  if (stipple_)
    return *stipple_;
  else
    return (guint16)65535;
}
/**
 * visu_gl_pairs_wire_setGeneralWidth:
 * @val: a positive integer.
 *
 * The width of line between elements can be chosen by kinds of pairs,
 * but can use a default value for all kinds of pairs when no individual value
 * is available. Use this method to set the default width.
 *
 * Returns: TRUE if the value is different from previous.
 */
gboolean visu_gl_pairs_wire_setGeneralWidth(guint val)
{
  DBG_fprintf(stderr, "Pairs Wire: set the wire thickness to %d.\n", val);
  if (val == 0 || val > RESOURCES_WIRE_WIDTH_MAX || val == wireWidth)
    return FALSE;

  wireWidth = val;
  return TRUE;
}
/**
 * visu_gl_pairs_wire_getGeneralWidth:
 *
 * Get the default width for wire pairs (see visu_gl_pairs_wire_setGeneralWidth() to set this value).
 *
 * Returns: the default width.
 */
guint visu_gl_pairs_wire_getGeneralWidth()
{
  return wireWidth;
}
/**
 * visu_gl_pairs_wire_setWidth:
 * @data: a #VisuPairLink object ;
 * @val: a positive integer.
 *
 * This method allows to change the width of line for a specific pair.
 * When a pair is rendered via with a line, it first checks if that pairs has
 * a specific width value set by this method. If not, it uses the default value.
 *
 * Returns: TRUE if the value is different from previous.
 */
gboolean visu_gl_pairs_wire_setWidth(VisuPairLink *data, guint val)
{
  guint *width;

  if (!data)
    return FALSE;

  
  width = (guint*)g_object_get_data(G_OBJECT(data), "width");
  if (!width)
    {
      width = g_malloc(sizeof(guint));
      g_object_set_data_full(G_OBJECT(data), "width", (gpointer)width, g_free);
      *width = val + 1;
    }
  val = (val > RESOURCES_WIRE_WIDTH_MAX)?RESOURCES_WIRE_WIDTH_MAX:val;
  if (*width != val)
    {
      *width = val;
      if (visu_pair_link_isDrawn(data))
        g_signal_emit_by_name(G_OBJECT(data), "ParameterChanged", NULL);
      return TRUE;
    }

  return FALSE;
}
/**
 * visu_gl_pairs_wire_getWidth:
 * @data: a #VisuPairLink object.
 *
 * Get the width of the given pair @data. If the given pair has no
 * specific width, the defaul value is returned.
 *
 * Returns: the width of the given pair.
 */
guint visu_gl_pairs_wire_getWidth(VisuPairLink *data)
{
  guint *width;

  if (!data)
    return 0;

  width = (guint*)g_object_get_data(G_OBJECT(data), "width");
  if (width)
    return *width;
  else
    return wireWidth;
}
/**
 * visu_gl_pairs_wire_setShade:
 * @data: a #VisuPairLink object.
 * @shade: (allow-none): a #ToolShade object (can be NULL).
 *
 * If @shade is not NULL, make the colour of each pair varies with its
 * length according to @shade colour scheme.
 *
 * Since: 3.6
 *
 * Returns: TRUE if shade is changed.
 */
gboolean visu_gl_pairs_wire_setShade(VisuPairLink *data, ToolShade *shade)
{
  ToolShade *val;

  if (!data)
    return FALSE;

  DBG_fprintf(stderr, "Pairs Wire: set the shade %p for the wire color.\n",
	      (gpointer)shade);

  val = (ToolShade*)g_object_get_data(G_OBJECT(data), "shade");
  if (val != shade)
    {
      g_object_set_data(G_OBJECT(data), "shade", (gpointer)shade);
      if (visu_pair_link_isDrawn(data))
        g_signal_emit_by_name(G_OBJECT(data), "ParameterChanged", NULL);
      return TRUE;
    }

  return FALSE;
}
/**
 * visu_gl_pairs_wire_getShade:
 * @data: a #VisuPairLink object.
 *
 * Colour of wires can depend on length, following a #ToolShade scheme.
 *
 * Since: 3.6
 *
 * Returns: (transfer none): the #ToolShade scheme if used, or NULL.
 */
ToolShade* visu_gl_pairs_wire_getShade(VisuPairLink *data)
{
  if (!data)
    return (ToolShade*)0;
  return (ToolShade*)g_object_get_data(G_OBJECT(data), "shade");
}

/*****************************************/
/* Dealing with parameters and resources */
/*****************************************/

static gboolean readLinkInteger(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				int position, GError **error,
				VisuPairLink **data, int *value)
{
  gchar **tokens;
  int id;
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  tokens = g_strsplit_set(lines[0], " \n", TOOL_MAX_LINE_LENGTH);
  id = 0;
  if (!visu_pair_readLinkFromTokens(tokens, &id, data, position, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }
  /* Read the associated integer. */
  if (!tool_config_file_readIntegerFromTokens(tokens, &id, value, 1, position, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }
  g_strfreev(tokens);

  return TRUE;
}
static gboolean readLinkShade(VisuConfigFileEntry *entry, gchar **lines, int nbLines, int position,
                              VisuData *dataObj _U_, VisuGlView *view _U_,
                              GError **error)
{
  int value;
  VisuPairLink *data;
  GList *list;

  if (!readLinkInteger(entry, lines, nbLines, position, error, &data, &value))
    return FALSE;

  list = tool_shade_getList();
  if (value >= (int)g_list_length(list))
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: shade id must be in %d-%d.\n"),
			   position, 0, g_list_length(list) - 1);
      return FALSE;
    }
  else
    visu_gl_pairs_wire_setShade(data, (ToolShade*)g_list_nth_data(list, value));

  return TRUE;
}
static gboolean readLinkStipple(VisuConfigFileEntry *entry, gchar **lines, int nbLines, int position,
				VisuData *dataObj _U_, VisuGlView *view _U_,
                                GError **error)
{
  int value;
  VisuPairLink *data;

  if (!readLinkInteger(entry, lines, nbLines, position, error, &data, &value))
    return FALSE;

  
  if ((guint16)value == 0)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: stipple must be in 1-65535.\n"),
			   position);
      return FALSE;
    }
  else
    visu_gl_pairs_wire_setStipple(data, (guint16)value);

  return TRUE;
}
static gboolean readLinkWidth(VisuConfigFileEntry *entry, gchar **lines, int nbLines, int position,
			      VisuData *dataObj _U_, VisuGlView *view _U_,
                              GError **error)
{
  int width;
  VisuPairLink *data;
  
  if (!readLinkInteger(entry, lines, nbLines, position, error, &data, &width))
    return FALSE;

  /* Check. */
  if (width <= 0 || width > RESOURCES_WIRE_WIDTH_MAX)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: width must be in %d-%d.\n"),
			   position, 0, RESOURCES_WIRE_WIDTH_MAX);
      return FALSE;
    }
  visu_gl_pairs_wire_setWidth(data, width);

  return TRUE;
}
/* This function details how to read what is needed by PairsData. */
static gboolean readWireGeneralWidth(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				     VisuData *dataObj _U_, VisuGlView *view _U_,
                                     GError **error)
{
  int width;

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readInteger(lines[0], position, &width, 1, error))
    return FALSE;
  if (width <= 0 || width > RESOURCES_WIRE_WIDTH_MAX)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: width must be in %d-%d.\n"),
			   position, 0, RESOURCES_WIRE_WIDTH_MAX);
      return FALSE;
    }
  visu_gl_pairs_wire_setGeneralWidth(width);
  return TRUE;
}
void exportPairsWidth(VisuElement *ele1, VisuElement *ele2,
		      VisuPairLink *data, gpointer userData)
{
  struct _VisuConfigFileForeachFuncExport *str;
  int *width;
  guint16 *stipple;
  ToolShade *shade;
  gboolean output;
  GList *list;
  gchar *buf;

  buf = g_strdup_printf("%s %s  %4.3f %4.3f", ele1->name, ele2->name,
                        visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MIN),
                        visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MAX));

  width = (int*)g_object_get_data(G_OBJECT(data), "width");
  if (width)
    {
      str = ( struct _VisuConfigFileForeachFuncExport*)userData;
      output = TRUE;
      /* We export the resource only if the elements are
	 part of the given VisuData. */
      if (str->dataObj)
        output = visu_node_array_getElementId(VISU_NODE_ARRAY(str->dataObj), ele1) >= 0&&
          visu_node_array_getElementId(VISU_NODE_ARRAY(str->dataObj), ele2) >= 0;
      if (output)
        visu_config_file_exportEntry(str->data, FLAG_RESOURCES_LINK_WIDTH, buf,
                                     "%d", *width);
    }
  stipple = (guint16*)g_object_get_data(G_OBJECT(data), "stipple");
  if (stipple)
    {
      str = ( struct _VisuConfigFileForeachFuncExport*)userData;
      output = TRUE;
      /* We export the resource only if the elements are
	 part of the given VisuData. */
      if (str->dataObj)
        output = visu_node_array_getElementId(VISU_NODE_ARRAY(str->dataObj), ele1) >= 0&&
          visu_node_array_getElementId(VISU_NODE_ARRAY(str->dataObj), ele2) >= 0;
      if (output)
        visu_config_file_exportEntry(str->data, FLAG_RESOURCES_LINK_STIPPLE, buf,
                                     "%d", *stipple);
    }
  shade = (ToolShade*)g_object_get_data(G_OBJECT(data), "shade");
  if (shade)
    {
      list = tool_shade_getList();
      str = ( struct _VisuConfigFileForeachFuncExport*)userData;
      output = TRUE;
      /* We export the resource only if the elements are
	 part of the given VisuData. */
      if (str->dataObj)
        output = visu_node_array_getElementId(VISU_NODE_ARRAY(str->dataObj), ele1) >= 0&&
          visu_node_array_getElementId(VISU_NODE_ARRAY(str->dataObj), ele2) >= 0;
      if (output)
        visu_config_file_exportEntry(str->data, FLAG_RESOURCES_LINK_STIPPLE, buf,
                                     "%d", g_list_index(list, shade));
    }

  g_free(buf);
}
static void exportResourcesWire(GString *data, VisuData *dataObj, VisuGlView *view _U_)
{
  struct _VisuConfigFileForeachFuncExport str;

  visu_config_file_exportComment(data, DESC_RESOURCES_WIRE_WIDTH);
  visu_config_file_exportEntry(data, FLAG_RESOURCES_WIRE_WIDTH, NULL,
                               "%d", wireWidth);
  str.data           = data;
  str.dataObj        = dataObj;
  visu_config_file_exportComment(data, DESC_RESOURCES_LINK_WIDTH);
  visu_config_file_exportComment(data, DESC_RESOURCES_LINK_STIPPLE);
  visu_config_file_exportComment(data, DESC_RESOURCES_SHADE);
  visu_pair_foreach(exportPairsWidth, &str);
  visu_config_file_exportComment(data, "");
}



/* OBSOLETE function, kept for backward compatibility. */
static gboolean readWireWidth(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position _U_,
			      VisuData *dataObj _U_, VisuGlView *view _U_,
                              GError **error)
{
  int token;
  VisuElement* ele[2];
  float radius;
  int val;
  VisuPairLink *data;
  gchar **tokens;

  g_return_val_if_fail(nbLines == 1, FALSE);

  /* Tokenize the line of values. */
  tokens = g_strsplit_set(g_strchug(lines[0]), " \n", TOOL_MAX_LINE_LENGTH);
  token = 0;

  /* Get the two elements. */
  if (!tool_config_file_readElementFromTokens(tokens, &token, ele, 2, nbLines, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }

  data = visu_pair_link_getFromId(ele[0], ele[1], 0);
  g_return_val_if_fail(data, FALSE);

  /* Read 1 int. */
  if (!tool_config_file_readFloatFromTokens(tokens, &token, &radius, 1, nbLines, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }
  val = CLAMP((int)radius, 0, RESOURCES_WIRE_WIDTH_MAX);
  visu_gl_pairs_wire_setWidth(data, val);
  
  g_strfreev(tokens);
  return TRUE;
}
static gboolean readWireNonLinear(VisuConfigFileEntry *entry _U_,
                                  gchar **lines _U_, int nbLines _U_, int position _U_,
				  VisuData *dataObj _U_, VisuGlView *view _U_,
                                  GError **error _U_)
{
  g_warning("No support anymore of this parameter.\n"
            "  Use pairWire_linkShade instead.");

  return TRUE;
}
static gboolean readWireToolShade(VisuConfigFileEntry *entry _U_,
                                  gchar **lines _U_, int nbLines _U_, int position _U_,
                                  VisuData *dataObj _U_, VisuGlView *view _U_,
                                  GError **error _U_)
{
  g_warning("No support anymore of this parameter.\n"
            "  Use pairWire_linkShade instead.");

  return TRUE;
}
