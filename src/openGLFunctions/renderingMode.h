/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef RENDERINGMODE_H
#define RENDERINGMODE_H

#include <glib.h>

/**
 * VisuGlRenderingMode:
 * @VISU_GL_RENDERING_WIREFRAME: objects are rendered with lines only ;
 * @VISU_GL_RENDERING_FLAT: objects are rendered with polygons whose colours are uniform
 *        on each polygon ;
 * @VISU_GL_RENDERING_SMOOTH: objects are rendered with polygons whose colours
 *          are shaded to be smooth all along the object.
 * @VISU_GL_RENDERING_SMOOTH_AND_EDGE: objects are rendered with lines hightlighting the
 * contours of polygons.
 * @VISU_GL_RENDERING_N_MODES: number of rendering mode.
 * @VISU_GL_RENDERING_FOLLOW: use this value not to choose any rendering mode.
 *
 * Implemented mode to draw objects.
 */
typedef enum
  {
    VISU_GL_RENDERING_WIREFRAME,
    VISU_GL_RENDERING_FLAT,
    VISU_GL_RENDERING_SMOOTH,
    VISU_GL_RENDERING_SMOOTH_AND_EDGE,
    VISU_GL_RENDERING_N_MODES,
    VISU_GL_RENDERING_FOLLOW
  } VisuGlRenderingMode;

void visu_gl_rendering_init(void);

gboolean visu_gl_rendering_setGlobalMode(VisuGlRenderingMode value);
VisuGlRenderingMode visu_gl_rendering_getGlobalMode(void);
gboolean visu_gl_rendering_getModeFromName(const char* name, VisuGlRenderingMode *id);
const char** visu_gl_rendering_getAllModes(void);
const char** visu_gl_rendering_getAllModeLabels(void);
void visu_gl_rendering_applyMode(VisuGlRenderingMode mode);

#endif
