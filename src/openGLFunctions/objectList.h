/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef OBJECTLIST_H
#define OBJECTLIST_H

#include <glib.h>
#include <GL/glu.h>

int visu_gl_objectlist_new(int size);
void visu_gl_objectlist_init(void);

/**
 * VisuGlArrowCentering:
 * @VISU_GL_ARROW_ORIGIN_CENTERED: the arrows are positioned with respect to the
 * point between tail and hat.
 * @VISU_GL_ARROW_BOTTOM_CENTERED: the arrows are positioned with respect to the bottom
 * of the arrow ;
 * @VISU_GL_ARROW_TAIL_CENTERED: the arrows are positioned with respect to the
 * center of the tail part ;
 * @VISU_GL_ARROW_CENTERED: the arrows are centered.
 *
 * The way to draw arrows (see visu_gl_drawSmoothArrow() for instance).
 */
typedef enum
  {
    VISU_GL_ARROW_ORIGIN_CENTERED,
    VISU_GL_ARROW_BOTTOM_CENTERED,
    VISU_GL_ARROW_TAIL_CENTERED,
    VISU_GL_ARROW_CENTERED
  } VisuGlArrowCentering;

void visu_gl_drawSmoothArrow(GLUquadricObj *obj, int material_id,
                             VisuGlArrowCentering centering,
                             float tailLength, float tailRadius, float tailN,
                             gboolean tailUseMat, float hatLength,
                             float hatRadius, float hatN, gboolean hatUseMat);
void visu_gl_drawEdgeArrow(int material_id, VisuGlArrowCentering centering,
                           float tailLength, float tailRadius,
                           gboolean tailUseMat, float hatLength,
                           float hatRadius, gboolean hatUseMat);
void visu_gl_drawEllipsoid(GLUquadricObj *obj, int material_id,
                           float aAxis, float bAxis,
                           float n, gboolean useMat);
void visu_gl_drawTorus(GLUquadricObj *obj, int material_id, float radius,
                       float ratio, int nA, int nB, gboolean useMat);
void visu_gl_drawDistance(float xyzRef[3], float xyz[3], gboolean drawLength);
void visu_gl_drawAngle(float xyzRef[3], float xyzRef2[3],
                       float xyz[3], guint id, gboolean drawLength);

#endif
