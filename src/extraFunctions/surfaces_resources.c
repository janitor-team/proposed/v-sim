/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "surfaces_resources.h"

#include <visu_tools.h>
#include <visu_data.h>
#include <visu_configFile.h>
#include <coreTools/toolConfigFile.h>

/**
 * SECTION:surfaces_resources
 * @short_description: Define the rendering parameters of a surface.
 *
 * <para>This structure stores all the rendering elements of a set of #VisuSurfaces.</para>
 */

/**
 * VisuSurfacesResources:
 * @surfnom: the name of the surface (in UTF-8) ;
 * @color: a #ToolColor for the surface ;
 * @material: the lighting effects of the surface ;
 * @rendered: boolean for the visibility of the surface ;
 * @sensitiveToPlanes: boolean for the sensitivity of a surface
 * to the masking effect of planes.
 *
 * This structure defines some drawing properties of a set of #VisuSurfaces.
 */

#define DESC_RESOURCE_COLOR      "Define the colour of one surface ;"	\
  " 4 floats (RGBA) 5 floats (material)"
#define FLAG_RESOURCE_COLOR      "isosurface_color"
#define DESC_RESOURCE_PROPERTIES "Define some surface properties ;"	\
  " rendered (0 or 1) sensitive to planes (0 or 1)"
#define FLAG_RESOURCE_PROPERTIES "isosurface_properties"

/* Local variables. */
static GHashTable *isosurfaces_resources = NULL;

/* Local methods. */
static void hash_free_resource(gpointer res);
static VisuSurfacesResources* resources_copy(VisuSurfacesResources *res);
static gboolean isosurfaces_read_resources(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
					   VisuData *dataObj, VisuGlView *view,
                                           GError **error);
static gboolean isosurfaces_read_colour(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
					VisuData *dataObj, VisuGlView *view,
                                        GError **error);
static gboolean isosurfaces_read_properties(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
					    VisuData *dataObj, VisuGlView *view,
                                            GError **error);
static void isosurfaces_export_resources(GString *data,
                                         VisuData *dataObj, VisuGlView *view);

/**
 * visu_surfaces_resources_get_type:
 *
 * Create and retrieve a #GType for a #VisuSurfacesResources object.
 *
 * Since: 3.7
 *
 * Returns: a new type for #VisuSurfacesResources structures.
 */
GType visu_surfaces_resources_get_type(void)
{
  static GType g_define_type_id = 0;

  if (g_define_type_id == 0)
    g_define_type_id =
      g_boxed_type_register_static("VisuSurfacesResources", 
                                   (GBoxedCopyFunc)resources_copy,
                                   (GBoxedFreeFunc)visu_surfaces_resources_free);
  return g_define_type_id;
}

/**
 * visu_surfaces_resources_init: (skip)
 *
 * Internal routine called automatically on #VisuSurfacesClass creation. Do not use
 * it.
 */
void visu_surfaces_resources_init(void)
{
  VisuConfigFileEntry *entry, *oldEntry;

  /* This entry is now obsolete but is kept for backward compatibility. */
  oldEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
				  "isosurface_property",
				  "Properties of a given isosurface",
				  1, isosurfaces_read_resources);
  visu_config_file_entry_setVersion(oldEntry, 3.3f);
  entry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
				  FLAG_RESOURCE_COLOR,
				  "The color and material of a given surface",
				  1, isosurfaces_read_colour);
  visu_config_file_entry_setVersion(entry, 3.4f);
  visu_config_file_entry_setReplace(entry, oldEntry);
  entry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
				  FLAG_RESOURCE_PROPERTIES,
				  "Properties of a given isosurface",
				  1, isosurfaces_read_properties);
  visu_config_file_entry_setVersion(entry, 3.4f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
				   isosurfaces_export_resources);

  isosurfaces_resources = g_hash_table_new_full(g_str_hash, g_str_equal,
						NULL, hash_free_resource);
}
/**
 * visu_surfaces_resources_free:
 * @res: an allocated #VisuSurfacesResources object to be freed.
 * 
 * This method frees the memory used by the given resource.
 */
void visu_surfaces_resources_free(VisuSurfacesResources *res)
{
  g_return_if_fail(res);

  DBG_fprintf(stderr, "Isosurfaces Resources: free resource %p (%s).\n",
	      (gpointer)res, res->surfnom);
  if (res->surfnom)
    g_free(res->surfnom);
  g_free(res->color);
  g_free(res);
}
static VisuSurfacesResources* visu_surfaces_init_resource()
{
  VisuSurfacesResources *res;
  float rgba[4];
  int position;
  ToolColor *color;

  res = g_malloc(sizeof(VisuSurfacesResources));
  rgba[0] = 1.0f;
  rgba[1] = 0.5f;
  rgba[2] = 0.5f;
  rgba[3] = 0.75f;
  color = tool_color_addFloatRGBA(rgba, &position);
  res->color = g_malloc(sizeof(ToolColor));
  for (position = 0; position < 4; position++)
    res->color->rgba[position] = color->rgba[position];
  res->material[0] = 0.2f;
  res->material[1] = 1.0f;
  res->material[2] = 0.5f;
  res->material[3] = 0.5f;
  res->material[4] = 0.0f;
  res->rendered = TRUE;
  res->surfnom = (gchar*)0;
  res->sensitiveToPlanes = TRUE;

  return res;
}
/**
 * visu_surfaces_resources_copy:
 * @res: an allocated #VisuSurfacesResources object to receive values ;
 * @res_old: a #VisuSurfacesResources to read the values from.
 * 
 * This method copies all values from @res_old to @res.
 */
void visu_surfaces_resources_copy(VisuSurfacesResources *res, VisuSurfacesResources *res_old)
{
  int i;

  g_return_if_fail(res && res_old);

  DBG_fprintf(stderr, "Isosurfaces Resources: copy resource from %p to %p.\n",
	      (gpointer)res_old, (gpointer)res);
  tool_color_copy(res->color, res_old->color);
  for (i = 0; i < 5; i++)
    res->material[i] = res_old->material[i];
  res->rendered = res_old->rendered;
  res->sensitiveToPlanes = res_old->sensitiveToPlanes;
}
static VisuSurfacesResources* resources_copy(VisuSurfacesResources *res)
{
  VisuSurfacesResources *out;

  out = g_malloc(sizeof(VisuSurfacesResources));
  visu_surfaces_resources_copy(out, res);
  return out;
}
/**
 * visu_surfaces_resources_getFromName:
 * @surf_name: the name of the surface (can be NULL) ;
 * @new_surf: a location to store a boolean value (can be NULL).
 * 
 * This returns the resource information matching the given @surf_name. If
 * the resource doesn't exist, it is created and @new is set to TRUE. If the given
 * name (@surf_name) is NULL, then a new resource is created, but it is not stored
 * and will not be shared by surfaces.
 *
 * Returns: (transfer none): the resource (created or retrieved).
 */
VisuSurfacesResources* visu_surfaces_resources_getFromName(const gchar *surf_name,
                                                           gboolean *nw)
{
  VisuSurfacesResources *res;

  g_return_val_if_fail(isosurfaces_resources, (VisuSurfacesResources*)0);

  if (surf_name && surf_name[0])
    res = g_hash_table_lookup(isosurfaces_resources, surf_name);
  else
    res = (VisuSurfacesResources*)0;
  if(!res)
    {    
      res = visu_surfaces_init_resource();
      if (surf_name && surf_name[0])
	{
	  res->surfnom = g_strdup(surf_name);
	  DBG_fprintf(stderr, "Isosurfaces Resources: registering new resources"
		      " for surface '%s'.\n", surf_name);
	  g_hash_table_insert(isosurfaces_resources, res->surfnom, res);
	}
      if (nw)
	*nw = TRUE;
    }
  else
    {
      if (nw)
	*nw = FALSE;
    }
      
  return res;
}
/**
 * visu_surfaces_resources_getRendered:
 * @res: the resource storing rendering information of a surface.
 *
 * Retrieves the rendering status of a surface.
 *
 * Since: 3.7
 *
 * Returns: TRUE, if the resource indicate a renedered surface.
 **/
gboolean visu_surfaces_resources_getRendered(const VisuSurfacesResources *res)
{
  g_return_val_if_fail(res, FALSE);

  return res->rendered;
}

/******************/
/* Local methods. */
/******************/
static void hash_free_resource(gpointer res)
{
  visu_surfaces_resources_free((VisuSurfacesResources*)res);
}
static gboolean isosurfaces_read_resources(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
					   VisuData *dataObj _U_, VisuGlView *view _U_,
                                           GError **error)
{
  VisuSurfacesResources *res;
  gboolean rendered;
  float rgba[4];
  float material[5];
  int k;
  gchar **tokens;

  g_return_val_if_fail(nbLines == 1, FALSE);

  tokens = g_strsplit(g_strchomp(lines[0]), "\"", 3);
  
  if (!tokens[0] || !tokens[1])
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Can't parse resource '%s' of"
			     " iso-surfaces on line %d.\n"),
			   "isosurface_property", position);
      g_strfreev(tokens);
      return FALSE;
    }

  if(sscanf(tokens[2], "%d %f %f %f %f %f %f %f %f %f\n", 
	    &rendered, &rgba[0], &rgba[1],
	    &rgba[2], &rgba[3], &material[0], &material[1],
	    &material[2], &material[3], &material[4]) != 10)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Can't parse resource '%s' of"
			     " iso-surfaces on line %d.\n"),
			   "isosurface_property", position);
      g_strfreev(tokens);
      return FALSE;
    }

  /* Read resources are always made public and added in the hashtable. */
  res = visu_surfaces_resources_getFromName(tokens[1], (gboolean*)0);
  res->color = tool_color_addFloatRGBA(rgba, &k);
  res->rendered = rendered;
  for (k = 0; k < 5; k++)
    res->material[k] = material[k];
  DBG_fprintf(stderr, "Isosurfaces Resources: resources found for surface '%s'\n", tokens[1]);
  g_strfreev(tokens);

  return TRUE;
}
static gboolean isosurfaces_read_colour(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
					VisuData *dataObj _U_, VisuGlView *view _U_,
                                        GError **error)
{
  VisuSurfacesResources *res;
  float rgba[4];
  float material[5];
  int k;
  gchar **tokens;

  g_return_val_if_fail(nbLines == 1, FALSE);

  tokens = g_strsplit(g_strchomp(lines[0]), "\"", 3);
  
  if (!tokens[0] || !tokens[1])
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Can't parse resource '%s' of"
			     " iso-surfaces on line %d.\n"),
			   FLAG_RESOURCE_COLOR, position);
      g_strfreev(tokens);
      return FALSE;
    }

  if(sscanf(tokens[2], "%f %f %f %f %f %f %f %f %f", 
	    &rgba[0], &rgba[1], &rgba[2], &rgba[3], &material[0], &material[1],
	    &material[2], &material[3], &material[4]) != 9)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Can't parse resource '%s' of"
			     " iso-surfaces on line %d.\n"),
			   FLAG_RESOURCE_COLOR, position);
      g_strfreev(tokens);
      return FALSE;
    }

  /* Read resources are always made public and added in the hashtable. */
  res = visu_surfaces_resources_getFromName(tokens[1], (gboolean*)0);
  res->color = tool_color_addFloatRGBA(rgba, &k);
  for (k = 0; k < 5; k++)
    res->material[k] = material[k];
  DBG_fprintf(stderr, "Isosurfaces Resources: resources '%s' found for surface '%s'\n",
	      FLAG_RESOURCE_COLOR, tokens[1]);
  g_strfreev(tokens);

  return TRUE;
}
static gboolean isosurfaces_read_properties(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
					    VisuData *dataObj _U_, VisuGlView *view _U_,
                                            GError **error)
{
  VisuSurfacesResources *res;
  int rendered, sensitive;
  gchar **tokens;

  g_return_val_if_fail(nbLines == 1, FALSE);

  tokens = g_strsplit(g_strchomp(lines[0]), "\"", 3);
  
  if (!tokens[0] || !tokens[1])
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Can't parse resource '%s' of"
			     " iso-surfaces on line %d.\n"),
			   FLAG_RESOURCE_PROPERTIES, position);
      g_strfreev(tokens);
      return FALSE;
    }

  if(sscanf(tokens[2], "%d %d", &rendered, &sensitive) != 2)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Can't parse resource '%s' of"
			     " iso-surfaces on line %d.\n"),
			   FLAG_RESOURCE_PROPERTIES, position);
      g_strfreev(tokens);
      return FALSE;
    }

  /* Read resources are always made public and added in the hashtable. */
  res = visu_surfaces_resources_getFromName(tokens[1], (gboolean*)0);
  res->rendered = rendered;
  res->sensitiveToPlanes = sensitive;
  DBG_fprintf(stderr, "Isosurfaces Resources: resources '%s' found for surface '%s'\n",
	      FLAG_RESOURCE_PROPERTIES, tokens[1]);
  g_strfreev(tokens);

  return TRUE;
}

static void isosurfaces_export_one_surf_resources(gpointer key, gpointer value,
						  gpointer user_data)
{
  struct _VisuConfigFileForeachFuncExport *str;
  VisuSurfacesResources *res;

  res = (VisuSurfacesResources*)value;
  str = (struct _VisuConfigFileForeachFuncExport*)user_data;
  DBG_fprintf(stderr, "Isosurfaces Resources: exporting surface '%s' properties...",
	      (char *)key);
  visu_config_file_exportEntry(str->data, FLAG_RESOURCE_COLOR, (gchar *)key,
                               "%4.3f %4.3f %4.3f %4.3f   %4.2f %4.2f %4.2f %4.2f %4.2f",
                               res->color->rgba[0], res->color->rgba[1],
                               res->color->rgba[2], res->color->rgba[3], res->material[0],
                               res->material[1], res->material[2], res->material[3],
                               res->material[4]);
  visu_config_file_exportEntry(str->data, FLAG_RESOURCE_PROPERTIES, (gchar *)key,
                               "%d %d", res->rendered, res->sensitiveToPlanes);
  DBG_fprintf(stderr, "OK.\n");
}
			 

static void isosurfaces_export_resources(GString *data,
                                         VisuData *dataObj _U_, VisuGlView *view _U_)
{
  struct _VisuConfigFileForeachFuncExport str;
 
  if(isosurfaces_resources != NULL && g_hash_table_size(isosurfaces_resources) > 0)
    {
      visu_config_file_exportComment(data, DESC_RESOURCE_COLOR);
      visu_config_file_exportComment(data, DESC_RESOURCE_PROPERTIES);
      str.data = data;
      g_hash_table_foreach(isosurfaces_resources, isosurfaces_export_one_surf_resources, &str);
      visu_config_file_exportComment(data, "");
    }
  DBG_fprintf(stderr, "Isosurfaces Resources: exporting OK.\n");
}
