/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef VIBRATION_H
#define VIBRATION_H

#include <visu_data.h>

/**
 * VISU_VIBRATION_ID:
 *
 * The default name used for the #VisuGlExt representing
 * vibrations, see visu_gl_ext_getFromName().
 */
#define VISU_VIBRATION_ID "Vibration"

gboolean visu_vibration_init(VisuData *data, guint n, guint nNodes);
gboolean visu_vibration_getNPhonons(VisuData *data, guint *n);
gboolean visu_vibration_setCurrentMode(VisuData *data, guint iph, GError **error);
gboolean visu_vibration_setDisplacements(VisuData *data, guint iph,
                                         float *vibes, gboolean complex);
gboolean visu_vibration_setCharacteristic(VisuData *data, guint n,
                                          float q[3], float en, float omega);
gboolean visu_vibration_getCharacteristic(VisuData *data, guint n,
                                          float q[3], float *en, float *omega);
void visu_vibration_setUserFrequency(VisuData *data, float freq);
guint visu_vibration_getCurrentMode(VisuData *data);

guint visu_vibration_play(VisuData *data);
gboolean visu_vibration_stop(guint timeoutID);

void visu_vibration_resetPosition(VisuData *data);
void visu_vibration_setZeroTime(VisuData *data);
gboolean visu_vibration_setAmplitude(VisuData *data, float ampl);

gboolean visu_vibration_isSet(VisuData *data);

#endif
