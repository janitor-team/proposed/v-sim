/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef DATANODE_H
#define DATANODE_H

#include <visu_nodes.h>
#include <visu_data.h>

/* This module is a wrapper around node properties from
   VisuData. It adds some capabilities, such as a translatable
   name, a way to go from data to strings and reverse, callbacks when
   the properties is changed... */

G_BEGIN_DECLS

/**
 * VISU_DATA_NODE_TYPE:
 *
 * return the type of #VisuDataNode.
 */
#define VISU_DATA_NODE_TYPE	     (visu_data_node_get_type ())
/**
 * VISU_DATA_NODE:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuDataNode type.
 */
#define VISU_DATA_NODE(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_DATA_NODE_TYPE, VisuDataNode))
/**
 * VISU_DATA_NODE_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuDataNodeClass.
 */
#define VISU_DATA_NODE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_DATA_NODE_TYPE, VisuDataNodeClass))
/**
 * VISU_IS_DATA_NODE_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuDataNode object.
 */
#define VISU_IS_DATA_NODE_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_DATA_NODE_TYPE))
/**
 * VISU_IS_DATA_NODE_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuDataNodeClass class.
 */
#define VISU_IS_DATA_NODE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_DATA_NODE_TYPE))
/**
 * VISU_DATA_NODE_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_DATA_NODE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_DATA_NODE_TYPE, VisuDataNodeClass))

/**
 * VisuDataNodeClass:
 *
 * An opaque structure.
 */
typedef struct _VisuDataNodeClass VisuDataNodeClass;

/**
 * VisuDataNode:
 *
 * An opaque structure.
 */
typedef struct _VisuDataNode VisuDataNode;

/**
 * VisuDataNodeFromStringFunc:
 * @data: a #VisuDataNode ;
 * @dataObj: a #VisuData object ;
 * @node: a #VisuNode ;
 * @labelIn: a formatted string ;
 * @labelOut: a pointer to store a string ;
 * @modify: TRUE if the values have been modified.
 *
 * If the string is correctly formatted (that means it has the same format as
 * the string returned by GetValueAsStringFunc()) the stored values are modified.
 * The resulting string is created and put into @labelOut. If the input string @labelIn
 * is unparsable, @labelOut will contain current values.
 *
 * Returns: TRUE if the string was correctly parsed (modified or not).
 */
typedef gboolean (*VisuDataNodeFromStringFunc)(VisuDataNode *data, VisuData *dataObj,
					   VisuNode *node, gchar *labelIn,
					   gchar **labelOut, gboolean *modify);
/**
 * VisuDataNodeToStringFunc:
 * @data: a #VisuDataNode ;
 * @dataObj: a #VisuData object ;
 * @node: a #VisuNode.
 *
 * For the given node, the values stored are printed into a string.
 *
 * Returns: a newly created string (use g_free()).
 */
typedef gchar* (*VisuDataNodeToStringFunc)(VisuDataNode *data, VisuData *dataObj,
					 VisuNode *node);

/* Maybe removed if the stuff is converted to GObject. */
/**
 * VisuDataNodeCallbackMethod:
 * @dataObj: the #VisuData object on which the callback is done ;
 * @node: the #VisuNode on which the callback is done ;
 * @data: a user defined pointer.
 *
 * Interface for callbacks methods that are called whenever a data is changed
 * on a node.
 *
 * WARNING: it may be removed later.
 */
typedef void (*VisuDataNodeCallbackMethod)(VisuData *dataObj, VisuNode *node, gpointer data);

/**
 * visu_data_node_get_type:
 *
 * This method returns the type of #VisuDataNode, use VISU_DATA_NODE_TYPE instead.
 *
 * Returns: the type of #VisuDataNode.
 */
GType visu_data_node_get_type(void);



GObject* visu_data_node_new(const gchar *name, GType type);
GObject* visu_data_node_newWithCallbacks(const gchar *name,
                                         VisuDataNodeFromStringFunc setAsString,
                                         VisuDataNodeToStringFunc getAsString);

GList* visu_data_node_class_getAll(void);

void visu_data_node_setLabel(VisuDataNode *data, const gchar *label);
const gchar* visu_data_node_getLabel(VisuDataNode *data);

void visu_data_node_setUsed(VisuDataNode *data, VisuData *dataObj, gint nb);
gboolean visu_data_node_getUsed(VisuDataNode *data, VisuData *dataObj);

gboolean visu_data_node_setValueAsString(VisuDataNode *data, VisuData *dataObj,
				   VisuNode *node, gchar *labelIn,
				   gchar **labelOut);
gchar* visu_data_node_getValueAsString(VisuDataNode *data, VisuData *dataObj, VisuNode *node);

void visu_data_node_setCallback(VisuDataNode *data, VisuDataNodeCallbackMethod callback,
                                gpointer user_data);
void visu_data_node_setEditable(VisuDataNode *data, gboolean status);
gboolean visu_data_node_getEditable(VisuDataNode *data);
void visu_data_node_emitValueChanged(VisuDataNode *data, VisuData *dataObj);


G_END_DECLS

#endif
