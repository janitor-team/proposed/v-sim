/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef SURFACES_H
#define SURFACES_H

#include <glib.h>
#include <glib-object.h>

#include "surfaces_resources.h"
#include "surfaces_points.h"

#include <extraFunctions/plane.h>
#include <openGLFunctions/view.h>
#include <openGLFunctions/light.h>
#include <visu_data.h>


/**
 * VISU_TYPE_SURFACES:
 *
 * return the type of #VisuSurfaces.
 */
#define VISU_TYPE_SURFACES	     (visu_surfaces_get_type ())
/**
 * SURFACES:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuSurfaces type.
 */
#define VISU_SURFACES(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_SURFACES, VisuSurfaces))
/**
 * VISU_SURFACES_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuSurfacesClass.
 */
#define VISU_SURFACES_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_SURFACES, VisuSurfacesClass))
/**
 * VISU_IS_SURFACES_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuSurfaces object.
 */
#define VISU_IS_SURFACES_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_SURFACES))
/**
 * VISU_IS_SURFACES_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuSurfacesClass class.
 */
#define VISU_IS_SURFACES_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_SURFACES))
/**
 * VISU_SURFACES_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_SURFACES_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_SURFACES, VisuSurfacesClass))

/**
 * VisuSurfacesClass:
 *
 * An opaque structure.
 */
typedef struct _VisuSurfacesClass VisuSurfacesClass;

/**
 * VisuSurfaces:
 *
 * All fields are private, use the access routines.
 */
typedef struct _VisuSurfaces VisuSurfaces;

/**
 * visu_surfaces_get_type:
 *
 * This method returns the type of #VisuSurfaces, use VISU_TYPE_SURFACES instead.
 *
 * Returns: the type of #VisuSurfaces.
 */
GType visu_surfaces_get_type(void);

/**
 * VISU_ERROR_ISOSURFACES: (skip)
 *
 * Internal function for error handling.
 */
#define VISU_ERROR_ISOSURFACES visu_surfaces_getErrorQuark()
GQuark visu_surfaces_getErrorQuark(void);
enum
  {
    VISU_ERROR_FORMAT,
    VISU_ERROR_CHECKSUM
  };

/**
 * VISU_SURFACES_PROPERTY_POTENTIAL
 *
 * Flag used in an ASCII surf file to give informations on the value
 * the surface is built from.
 */
#define VISU_SURFACES_PROPERTY_POTENTIAL "potential_values"

/* Some constructors, destructors. */
VisuSurfaces* visu_surfaces_new(guint bufferSize);
void visu_surfaces_allocate(VisuSurfaces *surf, guint nsurf, guint npolys, guint npoints);
void visu_surfaces_add(VisuSurfaces *surf, guint nsurf, guint npolys, guint npoints);

gboolean visu_surfaces_loadFile(const char *file, VisuSurfaces **surf, GError **error);

guint visu_surfaces_getN(VisuSurfaces *surf);
VisuSurfacesResources* visu_surfaces_getResource(VisuSurfaces *surf, guint i);
int visu_surfaces_getId(VisuSurfaces *surf, int i);

const gchar* visu_surfaces_getName(VisuSurfaces *surf, int surf_index);
gboolean visu_surfaces_getRendered(VisuSurfaces *surf, int surf_index);
gboolean visu_surfaces_setRendered(VisuSurfaces *surf, int surf_index, gboolean status);
VisuSurfacesResources* visu_surfaces_getResourceById(VisuSurfaces *surf, int surf_index);
void visu_surfaces_setResource(VisuSurfaces *surf, int surf_index, VisuSurfacesResources *res);
int* visu_surfaces_getSortedById(VisuSurfaces *surf);
int visu_surfaces_getNewId(VisuSurfaces *surf);
int visu_surfaces_getPosition(VisuSurfaces *surf, int id);

gboolean visu_surfaces_setColorAndMaterial(VisuSurfaces *surf, int surf_index,
                                           ToolColor *color,
                                           float material[VISU_GL_LIGHT_MATERIAL_N_VALUES]);

float* visu_surfaces_getPropertyFloat(VisuSurfaces *surf, const gchar *name);
float* visu_surfaces_addPropertyFloat(VisuSurfaces *surf, const gchar* name);
gboolean visu_surfaces_getPropertyValueFloat(VisuSurfaces *surf, int idSurf,
					   const gchar *name, float *value);
gboolean visu_surfaces_hide(VisuSurfaces *surf, VisuPlane **planes);

gboolean visu_surfaces_remove(VisuSurfaces *surf, int idSurf);

void visu_surfaces_checkConsistency(VisuSurfaces* surf);

void visu_surfaces_setShowAll(VisuSurfaces *surf, gboolean show);

/* Private area. */
struct _VisuSurfaces
{
  GObject parent;
  gboolean dispose_has_run;

  /* Number of different surfaces. */
  guint nsurf;

  VisuSurfacesPoints basePoints;
  VisuSurfacesPoints volatilePlanes;

  /* The description of the box where surfaces are drawn. */
  VisuBox *box;

  /* Resources for each surfaces. */
  VisuSurfacesResources **resources;

  /* Id for each surfaces. */
  int *ids;

  /* Table to add properties to surfaces. */
  GHashTable *properties;
};

#endif
