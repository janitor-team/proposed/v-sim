/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)
  
	Adresse m�l :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "extraNode.h"

#include <glib.h>
#include "dataNode.h"

/**
 * SECTION:extraNode
 * @short_description: add further characteristics to the nodes.
 *
 * <para>blabla</para>
 */

#define LABEL_ID "extraNode_label"

/* Local variables. */
static VisuDataNode *dataNodeLabel = NULL;
static GValue    dataLabel     = {0, {{0}, {0}}};

static void freeLabel(gpointer obj, gpointer data _U_)
{
  g_free(obj);
}
static gpointer newOrCopyLabel(gconstpointer orig, gpointer user_data _U_)
{
  if (orig)
    return g_strdup(orig);
  else
    return (gpointer)0;
}

/**
 * visu_extra_node_addLabel:
 * @data: a #VisuData object.
 * 
 * Add the possibility to store labels for nodes.
 */
void visu_extra_node_addLabel(VisuData *data)
{
  VisuNodeProperty* nodeProp;

  /* Test if the property is already set or not. */
  nodeProp = visu_node_array_getProperty(VISU_NODE_ARRAY(data), LABEL_ID);
  DBG_fprintf(stderr, "Extra Node: add a label node property (existing %p).\n",
              (gpointer)nodeProp);
  if (!nodeProp)
    /* If not, create it. */
    visu_node_array_property_newPointer(VISU_NODE_ARRAY(data),
                                  LABEL_ID, freeLabel, newOrCopyLabel, (gpointer)0);

  /* Register a new NodeData, if not already. */
  if (!dataNodeLabel)
    {
      dataNodeLabel = VISU_DATA_NODE(visu_data_node_new(LABEL_ID, G_TYPE_STRING));
      visu_data_node_setLabel(dataNodeLabel, _("Label"));
      visu_data_node_setEditable(dataNodeLabel, TRUE);

      g_value_init(&dataLabel, G_TYPE_POINTER);
    }

  /* Make this property used. */
  visu_data_node_setUsed(dataNodeLabel, data, 1);
}
/**
 * visu_extra_node_setLabel:
 * @data: a #VisuData object.
 * @nodeId: the id of a node.
 * @label: (allow-none): the label to set (will be copied).
 *
 * Set a @label to the node @nodeId.
 */
void visu_extra_node_setLabel(VisuData *data, guint nodeId, const gchar *label)
{
  VisuNode *node;
  gchar *lbl;

  node = visu_node_array_getFromId(VISU_NODE_ARRAY(data), nodeId);
  g_return_if_fail(node);

  /* Associates the values to the node. */
  if (label && label[0])
    lbl = g_strdup(label);
  else
    lbl = (gchar*)0;
  g_value_set_pointer(&dataLabel, (gpointer)lbl);
  visu_node_setpropertyValue(VISU_NODE_ARRAY(data), node, LABEL_ID, &dataLabel);
}
/**
 * visu_extra_node_getLabel:
 * @data: a #VisuData object.
 * @node: a given node
 *
 * Retrieve the label of @node. If @node has no label, NULL is
 * returned.
 *
 * Returns: (transfer none): a label, private value, do not freed.
 */
const gchar* visu_extra_node_getLabel(VisuData *data, VisuNode *node)
{
  VisuNodeProperty* nodeProp;
  gchar *lbl;

  nodeProp = visu_node_array_getProperty(VISU_NODE_ARRAY(data), LABEL_ID);
  if (!nodeProp)
    return (const gchar*)0;

  /* Read the value of the node. */
  visu_node_property_getValue(nodeProp, node, &dataLabel);
  lbl = (gchar*)g_value_get_pointer(&dataLabel);

  return lbl;
}
