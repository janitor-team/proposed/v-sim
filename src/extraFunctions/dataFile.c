/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "dataFile.h"

#include <string.h>

#include "dataNode.h"
#include <visu_object.h>
#include <visu_tools.h>
#include <visu_configFile.h>
#include <openGLFunctions/objectList.h>
#include <coreTools/toolColor.h>
#include <coreTools/toolShade.h>

/**
 * SECTION:dataFile
 * @short_description: Adds a possibility to colorize nodes depending
 * on data read in an input file.
 *
 * <para>With this module, it is possible to colorize nodes depending
 * on data read in an input file. An input file can be associated to a
 * #VisuData object using visu_colorization_new_fromFile(). Doing this, the
 * rendering is changed and nodes are colorized following a scheme
 * describe later. To turn off colorization without removing the data
 * file (for temporary turn off for instance), use
 * visu_colorization_setUsed().</para>
 * <para>The input file must have the same numbers of uncommented
 * lines as there are nodes in the #VisuData associated with. If less
 * data is given, missing data are treaded as min values data. The
 * input data file can has as much column as desired. The colorization
 * is based on a linear color transformation. This transformation is
 * applied on color channel in RGB mode or in HSV mode. Resulting
 * color is given by : [resulting color vect] = [vectB] + [input
 * data][vectA], where [input data] are input data scaled to [0;1]. It
 * is possible to choose which column multiplies which color
 * channel.</para>
 * <para>Using visu_colorization_applyHide(), it is possible to hide
 * some nodes depending on given input data.</para>
 */

/**
 * VisuColorization:
 *
 * An opaque structure to store colorisation settings.
 *
 * Since: 3.7
 */
struct _VisuColorization
{
  /* Set to TRUE to colorize. */
  gboolean used;
  guint refCount;
  gulong range_sig;

  ToolShade *shade;

  /* Columns to be used. */
  int colUsed[3];
  int scaleUsed;

  /* Scheme to scale : manual or auto. */
  VisuColorizationInputScaleId scaleType;

  /* Number of read columns. */
  guint nbColumns;

  /* Values min and max read for each column. */
  float *readMinMax;
  /* Min and max values for manual scale, for each column. */
  float *manualMinMax;
  /* Colourisation is applied only on values in range when FALSE. */
  gboolean applyToAll;

  /* File where data has been read. */
  gchar *file;

  /* Hiding function and data. */
  VisuColorizationHidingFunc hidingFunc;
  gpointer hidingData;
  GDestroyNotify hidingDestroy;
};

/* Cache pointers for data from currentVisuData.
   This cache is usefull to avoid too much g_object_get_data().
   This cache is refresh as soon as currentVisuData is changed. */
VisuColorization *cacheVisuColorization;
const VisuData *cacheVisuData;

/* Default values */
#define DATAFILE_NB_COLUMN_DEFAULT  -4
#define DATAFILE_COLUMN_X  -3
#define DATAFILE_COLUMN_Y  -2
#define DATAFILE_COLUMN_Z  -1
#define DATAFILE_SCALE_TYPE_DEFAULT VISU_COLORIZATION_NORMALIZE
#define DATAFILE_MIN_NORM_DEFAULT    0.
#define DATAFILE_MAX_NORM_DEFAULT   +1.
#define DATAFILE_COLOR_TYPE_DEFAULT dataFile_hsv

#define DATAFILE_ID "dataColor_data"

#define RESOURCE_RANGE_NAME "colorization_restrictInRange"
#define RESOURCE_RANGE_DESC "Apply colourisation only if in range."

/* Internal variables. */
static VisuDataNode *dataNode;
static GValue dataValue = {0, {{0}, {0}}};
static GQuark quark;
static gboolean restrictInRange = FALSE;

/* local methods. */
static void dataFileFree(gpointer data);
static void dataFileAttach(VisuColorization *dt, VisuData *dataObj);
static gboolean colorFromUserData(VisuData *visuData, float rgba[4],
                                  VisuElement *ele, VisuNode* node);
static float radiusFromUserData(VisuData *visuData, VisuNode *node);
static float valuesFromData(VisuData *visuData,
                            int column, float fromVal, gboolean *out);
static void freeData(gpointer obj, gpointer data);
static gpointer newOrCopyData(gconstpointer orig, gpointer user_data);
static void exportResources(GString *data, VisuData *dataObj, VisuGlView *view);
static void onEntryRange(VisuObject *obj, const gchar *key, gpointer data);

/**
 * visu_colorization_init: (skip)
 *
 * Called by V_Sim on startup, should not be called again.
 *
 * Returns: 1 if eveything goes right.
 */
int visu_colorization_init()
{
  VisuConfigFileEntry *resourceEntry;

  cacheVisuColorization = (VisuColorization*)0;
  cacheVisuData = (VisuData*)0;
  quark         =  g_quark_from_static_string("data_file");

  /* Register a new NodeData. */
  dataNode = VISU_DATA_NODE(visu_data_node_new(DATAFILE_ID, G_TYPE_FLOAT));
  visu_data_node_setLabel(dataNode, _("Colorisation data"));

  g_value_init(&dataValue, G_TYPE_POINTER);

  resourceEntry = visu_config_file_addBooleanEntry(VISU_CONFIG_FILE_RESOURCE,
                                                   RESOURCE_RANGE_NAME,
                                                   RESOURCE_RANGE_DESC,
                                                   &restrictInRange);
  visu_config_file_entry_setVersion(resourceEntry, 3.7f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE, exportResources);

  return 1;
}

/**
 * visu_colorization_getErrorQuark:
 *
 * Internal routine for error handling.
 *
 * Returns: (transfer none): the #GQuark associated to errors related to colour data
 * files.
 */
GQuark visu_colorization_getErrorQuark()
{
  return quark;
}

GType visu_colorization_get_type(void)
{
  static GType g_define_type_id = 0;

  if (g_define_type_id == 0)
    g_define_type_id =
      g_boxed_type_register_static("VisuColorization",
                                   (GBoxedCopyFunc)visu_colorization_ref,
                                   (GBoxedFreeFunc)visu_colorization_unref);
  return g_define_type_id;
}

/**
 * visu_colorization_new:
 *
 * Create a new object to store colorisation data.
 *
 * Since: 3.7
 *
 * Returns: (transfer full): a newly created #VisuColorization.
 **/
VisuColorization* visu_colorization_new()
{
  VisuColorization *dataFile;
  GList *lst;

  DBG_fprintf(stderr, "Visu Colorization: creating new object.\n");

  lst = tool_shade_getList();

  dataFile = g_malloc(sizeof(VisuColorization));
  dataFile->used            = FALSE;
  dataFile->refCount        = 1;
  dataFile->file            = (gchar*)0;
  dataFile->readMinMax      = (float*)0;
  dataFile->manualMinMax    = g_malloc(sizeof(float) * 2 * 3);
  dataFile->manualMinMax[0] = DATAFILE_MIN_NORM_DEFAULT;
  dataFile->manualMinMax[1] = DATAFILE_MAX_NORM_DEFAULT;
  dataFile->manualMinMax[2] = DATAFILE_MIN_NORM_DEFAULT;
  dataFile->manualMinMax[3] = DATAFILE_MAX_NORM_DEFAULT;
  dataFile->manualMinMax[4] = DATAFILE_MIN_NORM_DEFAULT;
  dataFile->manualMinMax[5] = DATAFILE_MAX_NORM_DEFAULT;
  dataFile->nbColumns       = 0;
  dataFile->scaleType       = DATAFILE_SCALE_TYPE_DEFAULT;
  dataFile->shade           = (lst)?tool_shade_copy((ToolShade*)lst->data):(ToolShade*)0;
  dataFile->colUsed[0]      = DATAFILE_NB_COLUMN_DEFAULT;
  dataFile->colUsed[1]      = DATAFILE_NB_COLUMN_DEFAULT;
  dataFile->colUsed[2]      = DATAFILE_NB_COLUMN_DEFAULT;
  dataFile->scaleUsed       = DATAFILE_NB_COLUMN_DEFAULT;
  dataFile->applyToAll      = !restrictInRange;
  dataFile->hidingFunc      = (VisuColorizationHidingFunc)0;
  dataFile->hidingData      = 0;
  dataFile->hidingDestroy   = (GDestroyNotify)0;

  dataFile->range_sig = g_signal_connect(VISU_OBJECT_INSTANCE,
                                         "entryParsed::" RESOURCE_RANGE_NAME,
                                         G_CALLBACK(onEntryRange), (gpointer)dataFile);

  return dataFile;
}
static void dataFileAttach(VisuColorization *dt, VisuData *visuData)
{
  DBG_fprintf(stderr, "Visu Colorization: attach data informations for"
	      " given VisuData object %p.\n", (gpointer)visuData);
  g_return_if_fail(visuData);

  g_object_set_data_full(G_OBJECT(visuData), "dataColor_parameters",
			 (gpointer)dt, dataFileFree);
  DBG_fprintf(stderr, " | %p\n", (gpointer)dt);

  /* Set the cache. */
  cacheVisuColorization = dt;
  cacheVisuData     = visuData;
}
static void dataFileFree(gpointer data)
{
  VisuColorization *dataFile;

  DBG_fprintf(stderr, "Visu Colorization: freeing dataFile.\n");
  dataFile = (VisuColorization*)data;

  if (dataFile->readMinMax)
    g_free(dataFile->readMinMax);
  if (dataFile->file)
    g_free(dataFile->file);
  if (dataFile->manualMinMax)
    g_free(dataFile->manualMinMax);
  if (dataFile->shade)
    tool_shade_free(dataFile->shade);
  if (dataFile->hidingData && dataFile->hidingDestroy)
    dataFile->hidingDestroy(dataFile->hidingData);
  g_signal_handler_disconnect(VISU_OBJECT_INSTANCE, dataFile->range_sig);
  g_free(dataFile);

  /* Update the cache. */
  if (dataFile == cacheVisuColorization)
    {
      cacheVisuColorization = (VisuColorization*)0;
      cacheVisuData = (VisuData*)0;
    }

}

/**
 * visu_colorization_ref:
 * @dt: a #VisuColorization object.
 *
 * Increase the ref counter.
 *
 * Since: 3.7
 *
 * Returns: itself.
 **/
VisuColorization* visu_colorization_ref(VisuColorization *dt)
{
  dt->refCount += 1;
  return dt;
}
/**
 * visu_colorization_unref:
 * @dt: a #VisuColorization object.
 *
 * Decrease the ref counter, free all memory if counter reachs zero.
 *
 * Since: 3.7
 **/
void visu_colorization_unref(VisuColorization *dt)
{
  dt->refCount -= 1;
  if (!dt->refCount)
    dataFileFree(dt);
}

static void dataFileActivate(VisuData *visuData, gboolean status)
{
  g_return_if_fail(visuData);

  if (status)
    {
      visu_element_setUpdateNodesOnMaterialChange();
      visu_data_setColorFunc(visuData, colorFromUserData);
      visu_data_setNodeScalingFunc(visuData, radiusFromUserData);
    }
  else
    {
      visu_element_unsetUpdateNodesOnMaterialChange();
      visu_data_setColorFunc(visuData, (VisuDataColorFunc)0);
      visu_data_setNodeScalingFunc(visuData, (VisuDataScalingFunc)0);
    }
}

static void freeData(gpointer obj, gpointer data)
{
#if GLIB_MINOR_VERSION > 9
  g_slice_free1(sizeof(float) * GPOINTER_TO_INT(data), obj);
#else
  if (GPOINTER_TO_INT(data) > 0)
    g_free(obj);
#endif
}
static gpointer newOrCopyData(gconstpointer orig, gpointer user_data)
{
  float *data;
  int nb;

  nb = GPOINTER_TO_INT(user_data);
#if GLIB_MINOR_VERSION > 9
  data = g_slice_alloc(sizeof(float) * nb);
#else
  data = g_malloc(sizeof(float) * nb);
#endif
  if (orig)
    memcpy(data, orig, sizeof(float) * nb);
  else
    memset(data, 0, sizeof(float) * nb);

  return (gpointer)data;
}

static VisuNodeProperty* _initData(VisuColorization *dt, VisuData *dataObj,
                                      guint nbColumns)
{
  guint i;
  VisuNodeProperty *prop;

  /* Update dataObj. */
  visu_node_array_freeProperty(VISU_NODE_ARRAY(dataObj), DATAFILE_ID);
  if (nbColumns > 0)
    prop = visu_node_array_property_newPointer(VISU_NODE_ARRAY(dataObj), DATAFILE_ID,
                                               freeData, newOrCopyData,
                                               GINT_TO_POINTER(nbColumns));
  else
    prop = (VisuNodeProperty*)0;
  /* Set the dimensions for the node property. */
  visu_data_node_setUsed(dataNode, dataObj, nbColumns);

  if (dt->nbColumns == nbColumns)
    return prop;

  dt->nbColumns = nbColumns;
  if (nbColumns > 0)
    {
      dt->readMinMax = g_realloc(dt->readMinMax,
                                 sizeof(float) * 2 * (dt->nbColumns));
      dt->manualMinMax =
        g_realloc(dt->manualMinMax,
                  sizeof(float) * 2 * (dt->nbColumns + 3));
      for (i = 0; i < dt->nbColumns; i++)
        {
          dt->readMinMax[2 * i + 0] = G_MAXFLOAT;
          dt->readMinMax[2 * i + 1] = -G_MAXFLOAT;
          dt->manualMinMax[2 * (i + 3) + 0] = DATAFILE_MIN_NORM_DEFAULT;
          dt->manualMinMax[2 * (i + 3) + 1] = DATAFILE_MAX_NORM_DEFAULT;
        }
      dt->colUsed[0] = (dt->colUsed[0] >= (int)dt->nbColumns)?0:dt->colUsed[0];
      dt->colUsed[1] = (dt->colUsed[1] >= (int)dt->nbColumns)?0:dt->colUsed[1];
      dt->colUsed[2] = (dt->colUsed[2] >= (int)dt->nbColumns)?0:dt->colUsed[2];
    }
  else
    {
      if (dt->readMinMax)
        g_free(dt->readMinMax);
      dt->readMinMax = (float*)0;
      if (dt->file)
        g_free(dt->file);
      dt->file = (gchar*)0;
    }
  return prop;
}
static void _addDataForNode(VisuColorization *dataFile,
                            VisuNodeProperty *prop, VisuNode *node, float *data)
{
  guint i;

  g_value_set_pointer(&dataValue, data);
  visu_node_property_setValue(prop, node, &dataValue);
  /* Check for minMax values for each column. */
  for (i = 0; i < dataFile->nbColumns; i++)
    {
      if (data[i] < dataFile->readMinMax[i * 2 + 0])
        dataFile->readMinMax[i * 2 + 0] = data[i];
      if (data[i] > dataFile->readMinMax[i * 2 + 1])
        dataFile->readMinMax[i * 2 + 1] = data[i];
    }
}
static void _finalizeData(VisuColorization *dataFile, VisuData *dataObj,
                          const gchar *filename)
{
  guint i;

  if (dataFile->file)
    g_free(dataFile->file);
  if (filename)
    dataFile->file = g_strdup(filename);
  else
    dataFile->file = (gchar*)0;
  /* Copy the read minMax into the manual minMax. */
  for (i = 0; i < dataFile->nbColumns; i++)
    {
      dataFile->manualMinMax[2 * (i + 3) + 0] = dataFile->readMinMax[i * 2 + 0];
      dataFile->manualMinMax[2 * (i + 3) + 1] = dataFile->readMinMax[i * 2 + 1];
    }
  dataFileActivate(dataObj, dataFile->used);
}

/**
 * visu_colorization_new_fromData:
 * @dataObj: a #VisuData object.
 * @nbColumns: an integer.
 * @data: (element-type gfloat): the colorization data.
 * @new: (allow-none) (out caller-allocates): a location for a boolean.
 *
 * Add colourisation data to @dataObj.
 *
 * Since: 3.7
 *
 * Returns: (transfer full): a newly created #VisuColorization or NULL.
 **/
VisuColorization* visu_colorization_new_fromData(VisuData *dataObj,
                                                 guint nbColumns, GArray *data,
                                                 gboolean *new)
{
  VisuColorization *dt;
  VisuNodeProperty *prop;
  guint i;
  VisuNodeArrayIter iter;
  float *vals;

  g_return_val_if_fail(data && (data->len % nbColumns == 0), (VisuColorization*)0);

  dt = visu_colorization_get(dataObj, TRUE, new);

  prop = _initData(dt, dataObj, nbColumns);
  
  visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter);
  for (visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter), i = 0; iter.node;
       visu_node_array_iterNextNodeNumber(VISU_NODE_ARRAY(dataObj), &iter),
         i += nbColumns)
    {
      vals = newOrCopyData(&g_array_index(data, float, i), GINT_TO_POINTER(nbColumns));
      /* Associates the values to the node. */
      _addDataForNode(dt, prop, iter.node, vals);
    }

  _finalizeData(dt, dataObj, (gchar*)0);

  return dt;
}

/**
 * visu_colorization_new_fromFile:
 * @data: a #VisuData object to attach the data file to ;
 * @filename: the path to find the data file on the disk ;
 * @new: (allow-none) (out caller-allocates): a location for a boolean.
 * @error: a location to a NULL #GError.
 *
 * Call this method to parse a data file and associate its values to the given #VisuData object.
 *
 * Returns: (transfer full): a newly created #VisuColorization or NULL.
 */
VisuColorization* visu_colorization_new_fromFile(VisuData *dataObj, const char* filename,
                                                 gboolean *new, GError **error)
{
  VisuColorization *dt;
  guint i, nb, nbColumns;
  GIOChannel *readFrom;
  GIOStatus status;
  GString *line;
  gsize term;
  float *data, fval;
  gchar **dataRead;
  VisuNodeArrayIter iter;
  gboolean voidLine;
  VisuNodeProperty *prop;
#if DEBUG == 1
  GTimer *timer;
  gulong fractionTimer;
#endif

  DBG_fprintf(stderr, "Visu Colorization: set data file to '%s' for %p.\n",
	      filename, (gpointer)dataObj);
  g_return_val_if_fail(dataObj && filename, FALSE);
  g_return_val_if_fail(error && (*error) == (GError*)0, FALSE);

  readFrom = g_io_channel_new_file(filename, "r", error);
  if (!readFrom)
    return (VisuColorization*)0;

  DBG_fprintf(stderr, "Visu VisuColorization: begin reading file.\n");
#if DEBUG == 1
  timer = g_timer_new();
  g_timer_start(timer);
#endif

  dt = visu_colorization_get(dataObj, TRUE, new);
  line = g_string_new("");
  nbColumns = 0;
  prop = (VisuNodeProperty*)0;
  visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter);
  for (visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter); iter.node;
       visu_node_array_iterNextNodeNumber(VISU_NODE_ARRAY(dataObj), &iter))
    {
      /* If the file is not finished, we read until a non-void line
	 and return this line. */
      voidLine = TRUE;
      do
	{
	  status = g_io_channel_read_line_string(readFrom, line, &term, error);
	  if (status == G_IO_STATUS_NORMAL)
	    {
	      g_strchug(line->str);
	      voidLine = (line->str[0] == '#' ||
			  line->str[0] == '!' ||
			  line->str[0] == '\0');
	    }
          if (!voidLine)
            {
              voidLine = TRUE;
              dataRead = g_strsplit_set(line->str, " \t;:\n", TOOL_MAX_LINE_LENGTH);
              for (i = 0; dataRead[i]; i++)
                if (dataRead[i][0] && 1 == sscanf(dataRead[i], "%f", &fval))
                  voidLine = FALSE;
	      g_strfreev(dataRead);
            }          
	}
      while (status == G_IO_STATUS_NORMAL && voidLine);
      if (status == G_IO_STATUS_ERROR)
	{
	  g_string_free(line, TRUE);
	  dataFileActivate(dataObj, FALSE);
	  dt->used = FALSE;
          _initData(dt, dataObj, 0);
	  status = g_io_channel_shutdown(readFrom, FALSE, (GError**)0);
	  g_io_channel_unref(readFrom);
	  return (VisuColorization*)0;
	}

      /* If still not done, we test the number of columns. */
      if (nbColumns == 0)
	{
	  if (!voidLine)
	    {
	      dataRead = g_strsplit_set(line->str, " \t;:\n", TOOL_MAX_LINE_LENGTH);
	      DBG_fprintf(stderr, "Visu dt: looking for number of columns.\n");
	      for (i = 0; dataRead[i]; i++)
		if (dataRead[i][0] && 1 == sscanf(dataRead[i], "%f", &fval))
		  {
		    DBG_fprintf(stderr, "   | one token : '%s'\n", dataRead[i]);
		    nbColumns += 1;
		  }
	      g_strfreev(dataRead);
	      DBG_fprintf(stderr, "Visu Colorization: detected number of columns : %d\n",
			  dt->nbColumns);
	    }
	  if (nbColumns == 0 || voidLine)
	    {
	      *error = g_error_new(VISU_ERROR_COLORIZATION, VISU_COLORIZATION_ERROR_NO_COLUMN,
				   _("Can't find any column of"
				     " data in the given file.\n"));
	      g_string_free(line, TRUE);
	      dataFileActivate(dataObj, FALSE);
	      dt->used = FALSE;
              _initData(dt, dataObj, 0);
	      status = g_io_channel_shutdown(readFrom, FALSE, (GError**)0);
	      g_io_channel_unref(readFrom);
	      return (VisuColorization*)0;
	    }
          prop = _initData(dt, dataObj, nbColumns);
	}

      /* We read the data from the line, if not void. */
      data = newOrCopyData((gconstpointer)0, GINT_TO_POINTER(nbColumns));
      if (!voidLine)
	{
	  dataRead = g_strsplit_set(line->str, " \t;:\n", TOOL_MAX_LINE_LENGTH);
	  nb = 0;
	  for (i = 0; dataRead[i] && nb < nbColumns; i++)
	    {
	      if (1 == sscanf(dataRead[i], "%f", data + nb))
		{
		  DBG_fprintf(stderr, "   '%s' - '%f'",
			      dataRead[i], data[nb]);
		  nb += 1;
		}
	    }
	  DBG_fprintf(stderr, "\n");
	  g_strfreev(dataRead);
	}
      else
	{
	  if (!*error)
	    *error = g_error_new(VISU_ERROR_COLORIZATION, VISU_COLORIZATION_ERROR_MISSING_DATA,
				 _("There are more nodes than data.\n"));
	}
      /* Associates the values to the node. */
      _addDataForNode(dt, prop, iter.node, data);
    }
  status = g_io_channel_shutdown(readFrom, FALSE, (GError**)0);
  g_io_channel_unref(readFrom);
  g_string_free(line, TRUE);

  if (dt->nbColumns == 0)
    {
      *error = g_error_new(VISU_ERROR_COLORIZATION, VISU_COLORIZATION_ERROR_NO_COLUMN,
			   _("Can't find any columns with numbers.\n"
			     "Valid format are as much numbers as desired, separated"
			     " by any of the following characters : [ ;:\\t].\n"));
      dt->used = FALSE;
      _initData(dt, dataObj, 0);
      return (VisuColorization*)0;
    }
  if (DEBUG)
    {
      DBG_fprintf(stderr, "Visu dataFile : min/max values :\n");
      for (i = 0; i < dt->nbColumns; i++)
	DBG_fprintf(stderr, "Visu dataFile :  col %d -> %f (min) and %f (max).\n",
		    i, dt->readMinMax[i * 2 + 0], dt->readMinMax[i * 2 + 1]);
    }
  _finalizeData(dt, dataObj, filename);

  cacheVisuColorization = dt;
  cacheVisuData = dataObj;

#if DEBUG == 1
  g_timer_stop(timer);
  fprintf(stderr, "Visu Colorization: data associated in %g micro-s.\n", g_timer_elapsed(timer, &fractionTimer)/1e-6);
  g_timer_destroy(timer);
#endif

  return dt;
}

/**
 * visu_colorization_get:
 * @data: (allow-none): a #VisuData object.
 * @create: a boolean.
 * @new: (out) (allow-none): a boolean location
 *
 * Return an already associated #VisuColorization object to @data, or
 * create it if necessary.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuColorization object. This object will
 * be automatically freed with @data.
 **/
VisuColorization* visu_colorization_get(VisuData *data,
                                        gboolean create, gboolean *new)
{
  VisuColorization *dataFile;

  DBG_fprintf(stderr, "Visu VisuColorization: get data for %p (%p %p).\n",
	      (gpointer)data, (gpointer)cacheVisuData, (gpointer)cacheVisuColorization);
  if (new)
    *new = FALSE;
  if (!data)
    return (VisuColorization*)0;
  if (data == cacheVisuData)
    dataFile = cacheVisuColorization;
  else
    {
      dataFile = (VisuColorization*)g_object_get_data(G_OBJECT(data), "dataColor_parameters");
      cacheVisuColorization = dataFile;
      cacheVisuData = data;
    }
  if (!dataFile && create)
    {
      DBG_fprintf(stderr, "Visu VisuColorization: get data and create new.\n");
      dataFile = visu_colorization_new();
      dataFileAttach(dataFile, data);
      if (new)
	*new = TRUE;
    }
  return dataFile;
}

/**
 * visu_colorization_getFileSet:
 * @dt: (allow-none): a #VisuColorization object.
 *
 * A set of data per node can be associated to a #VisuData. This
 * routine retrieves if @visuData has it or not.
 *
 * Since: 3.6
 *
 * Returns: TRUE if @visuData has colour data associated.
 */
gboolean visu_colorization_getFileSet(const VisuColorization *dt)
{
  if (dt && dt->file)
    return TRUE;
  else
    return FALSE;
}
/**
 * visu_colorization_getFile:
 * @dt: (allow-none): a #VisuColorization object.
 *
 * If the given @dt has an input data file already loaded, it returns its name.
 *
 * Returns: (transfer full): the name of the input data file if
 * set.
 */
gchar* visu_colorization_getFile(const VisuColorization *dt)
{
  if (dt)
    return g_strdup(dt->file);
  else
    return (gchar*)0;
}
/**
 * visu_colorization_setRestrictInRange:
 * @dt: a #VisuColorization object with some data file information.
 * @status: a boolean.
 *
 * The colourisation can be applied on all nodes or on nodes within
 * range. See visu_colorization_getRestrictInRange() and
 * visu_colorization_setMin() and visu_colorization_setMax().
 *
 * Since: 3.7
 *
 * Returns: TRUE if the status is changed indeed.
 */
gboolean visu_colorization_setRestrictInRange(VisuColorization *dt, gboolean status)
{
  g_return_val_if_fail(dt, FALSE);

  if (dt->applyToAll != status)
    return FALSE;

  dt->applyToAll = !status;
  return TRUE;
}
/**
 * visu_colorization_getRestrictInRange:
 * @dt: a #VisuColorization object with some data file information.
 *
 * The colourisation can be applied on all nodes or on nodes within
 * range. See visu_colorization_setRestrictInRange().
 *
 * Since: 3.7
 *
 * Returns: TRUE if colourisation is done only if values are in range.
 */
gboolean visu_colorization_getRestrictInRange(const VisuColorization *dt)
{
  if (dt)
    return !dt->applyToAll;
  else
    return TRUE;
}
/**
 * visu_colorization_getColumnMinMax:
 * @dt: the #VisuColorization object which the colour data are associated to ;
 * @minMax: an allocated array of two floating point values ;
 * @column: an integer.
 *
 * This method is used to retrieve the minimum and the maximum
 * values of the column designed by the @column argument. Column
 * are numbered beginning at 0.
 *
 * Returns: FALSE if @column < 0 or if @column is greater than the number
 *          of read column or if no file has been set.
 */
gboolean visu_colorization_getColumnMinMax(const VisuColorization *dt,
                                        float minMax[2], guint column)
{
  g_return_val_if_fail(dt, FALSE);
  g_return_val_if_fail(column < dt->nbColumns, FALSE);

  minMax[0] = dt->readMinMax[column * 2 + 0];
  minMax[1] = dt->readMinMax[column * 2 + 1];
  return TRUE;
}
/**
 * visu_colorization_setUsed:
 * @data: a #VisuData object to set the colorisation tool or not ;
 * @val: a boolean.
 * 
 * When TRUE, rendering is modified by applying a colorization
 * method to normal nodes. The color used depend on input data. See 
 * visu_colorization_new_fromFile() to choose them.
 *
 * Returns: TRUE if val is true and if a valid input file is already
 *          in memory.
 */
gboolean visu_colorization_setUsed(VisuData *visuData, int val)
{
  VisuColorization *dataFile;
  gboolean new;

  DBG_fprintf(stderr, "Visu Colorization: set using flag for"
	      " dataFile module to %d .\n", val);

  dataFile = visu_colorization_get(visuData, TRUE, &new);
  g_return_val_if_fail(dataFile, FALSE);

  if (val == dataFile->used)
    return FALSE;

  dataFile->used = val;

  dataFileActivate(visuData, dataFile->used);
  return !new;
}
/**
 * visu_colorization_getUsed:
 * @dt: (allow-none): a #VisuColorization object to get if the
 * colorisation tool is set or not ;
 * 
 * This method retrieve the used flag, see visu_colorization_setUsed() to set it.
 *
 * Returns: 1 if the used flag is set.
 */
gboolean visu_colorization_getUsed(const VisuColorization *dt)
{
  if (!dt)
    return FALSE;
  else
    return dt->used;
}
/**
 * visu_colorization_setScaleType:
 * @dt: a #VisuColorization object ;
 * @scale: an integer.
 *
 * This method is used to change the scale method used on input data.
 * See #VisuColorizationInputScaleId for further informations. This method raises
 * a error if no input file has already been associated to the give @visuData.
 *
 * Returns: TRUE if VisuNodeArray::RenderingChanged should be emitted.
 */
gboolean visu_colorization_setScaleType(VisuColorization *dt, VisuColorizationInputScaleId scale)
{
  g_return_val_if_fail(dt, FALSE);

  DBG_fprintf(stderr, "Visu Colorization: set the scale type to %d (previuosly %d).\n",
	      scale, dt->scaleType);
  if (scale == dt->scaleType)
    return FALSE;
  dt->scaleType = scale;

  return dt->used;
}
/**
 * visu_colorization_getScaleType:
 * @dt: (allow-none): a #VisuColorization object.
 *
 * Retrieve the scaling method of input data associated to the given @dt.
 *
 * Returns: the scaling method if @dt is not NULL
 *          or the default value if not.
 */
VisuColorizationInputScaleId visu_colorization_getScaleType(const VisuColorization *dt)
{
  if (!dt)
    return DATAFILE_SCALE_TYPE_DEFAULT;
  else
    return dt->scaleType;
}
static gboolean _setManualMinMax(VisuColorization *dataFile,
                                 float val, int column, guint minmax)
{
  g_return_val_if_fail(dataFile, FALSE);
  g_return_val_if_fail(column >= DATAFILE_COLUMN_X &&
                       column < (int)dataFile->nbColumns, FALSE);

  DBG_fprintf(stderr, "Visu Colorization: set the min/max (%d) value"
              " of column %d to %f (previuosly %f).\n",
	      minmax, column, val, dataFile->manualMinMax[2 * (column + 3) + minmax]);
  if (dataFile->manualMinMax[2 * (column + 3) + minmax] == val)
    return FALSE;
  dataFile->manualMinMax[2 * (column + 3) + minmax] = val;

  return dataFile->used;
}
/**
 * visu_colorization_setMin:
 * @dt: a #VisuColorization object ;
 * @min: a floating point value.
 * @column: a column id.
 *
 * When the scaling method is #VISU_COLORIZATION_MINMAX (see #VisuColorizationInputScaleId)
 * min and max value for convert input data are user defined. Use this method
 * to choose the minimum bound. This method raises
 * a error if no input file has already been associated to the give @visuData.
 *
 * Returns: TRUE if VisuNodeArray::RenderingChanged should be emitted.
 */
gboolean visu_colorization_setMin(VisuColorization *dt, float min, int column)
{
  return _setManualMinMax(dt, min, column, 0);
}
/**
 * visu_colorization_setMax:
 * @dt: a #VisuColorization object ;
 * @max: a floating point value.
 * @column: a column id.
 *
 * When the scaling method is #VISU_COLORIZATION_MINMAX (see #VisuColorizationInputScaleId)
 * min and max value for convert input data are user defined. Use this method
 * to choose the maximum bound. This method raises
 * a error if no input file has already been associated to the give @visuData.
 *
 * Returns: TRUE if VisuNodeArray::RenderingChanged should be emitted.
 **/
gboolean visu_colorization_setMax(VisuColorization *dt, float max, int column)
{
  return _setManualMinMax(dt, max, column, 1);
}
static float _getManualMinMax(const VisuColorization *dt, int column, int minmax)
{
  if (!dt)
    return (minmax == 0)?DATAFILE_MIN_NORM_DEFAULT:DATAFILE_MAX_NORM_DEFAULT;
  else
    return dt->manualMinMax[2 * (column + 3) + minmax];
}
/**
 * visu_colorization_getMin:
 * @dt: (allow-none): a #VisuData object.
 * @column: a column id.
 *
 * Retrieve the minimum value used when scaling is user defined.
 *
 * Returns: the minimum bound if @dt is not NULL
 *          or the default value if not.
 */
float visu_colorization_getMin(const VisuColorization *dt, int column)
{
  return _getManualMinMax(dt, column, 0);
}
/**
 * visu_colorization_getMax:
 * @dt: (allow-none): a #VisuData object.
 * @column: a column id.
 *
 * Retrieve the maximum value used when scaling is user defined.
 *
 * Returns: the maximum bound if @dt is not NULL
 *          or the default value if not.
 */
float visu_colorization_getMax(const VisuColorization *dt, int column)
{
  return _getManualMinMax(dt, column, 1);
}
/**
 * visu_colorization_getNColumns:
 * @dt: a #VisuColorization object.
 *
 * This method is used to retrieve the number of columns of data read in
 * the loaded file.
 *
 * Returns: this number of columns.
 */
int visu_colorization_getNColumns(const VisuColorization *dt)
{
  if (!dt)
    return DATAFILE_NB_COLUMN_DEFAULT;
  else
    return dt->nbColumns;
}
/**
 * visu_colorization_getSingleColumnId:
 * @dt: a #VisuColorization object.
 * @id: (out): a location to store a column id.
 *
 * The colourisation can be applied from values coming from several
 * columns. But, if only one column is used, this routine will give it
 * in @id.
 *
 * Returns: FALSE if several columns are used, or TRUE if a single
 * column is used for the colourisation.
 */
gboolean visu_colorization_getSingleColumnId(const VisuColorization *dt, gint *id)
{
  float *vectA, *vectB;

  if (!dt)
    return FALSE;

  if (dt->shade && tool_shade_getMode(dt->shade) == TOOL_SHADE_MODE_LINEAR)
    tool_shade_getLinearCoeff(dt->shade, &vectA, &vectB);
  else
    vectA = (float*)0;

  if ((dt->colUsed[0] == DATAFILE_NB_COLUMN_DEFAULT ||
       dt->colUsed[1] == DATAFILE_NB_COLUMN_DEFAULT ||
       dt->colUsed[0] == dt->colUsed[1] ||
       (vectA && (vectA[0] == 0.f || vectA[1] == 0.f))) &&
      (dt->colUsed[1] == DATAFILE_NB_COLUMN_DEFAULT ||
       dt->colUsed[2] == DATAFILE_NB_COLUMN_DEFAULT ||
       dt->colUsed[1] == dt->colUsed[2] ||
       (vectA && (vectA[1] == 0.f || vectA[2] == 0.f))) &&
      (dt->colUsed[2] == DATAFILE_NB_COLUMN_DEFAULT ||
       dt->colUsed[0] == DATAFILE_NB_COLUMN_DEFAULT ||
       dt->colUsed[2] == dt->colUsed[0] ||
       (vectA && (vectA[0] == 0.f || vectA[2] == 0.f))) && id)
    {
      if (dt->colUsed[0] != DATAFILE_NB_COLUMN_DEFAULT && (!vectA || vectA[0] != 0.f))
        *id = dt->colUsed[0];
      else if (dt->colUsed[1] != DATAFILE_NB_COLUMN_DEFAULT && (!vectA || vectA[1] != 0.f))
        *id = dt->colUsed[1];
      else
        *id = dt->colUsed[2];
    }
  else
    return FALSE;
  return TRUE;
}
/**
 * visu_colorization_setColUsed:
 * @dt: a #VisuColorization object ;
 * @val: a column id a special value ;
 * @pos: an integer in [0;2].
 *
 * Choose if the loaded value should change the given channel of the colour.
 *
 * Returns: TRUE if VisuNodeArray::RenderingChanged should be emitted.
 */
gboolean visu_colorization_setColUsed(VisuColorization *dt, int val, int pos)
{
  g_return_val_if_fail(pos >= 0 && pos < 3, FALSE);
  g_return_val_if_fail(dt, FALSE);

  DBG_fprintf(stderr, "Visu Colorization: channel %d uses column %d (previuosly %d).\n",
	      pos, val, dt->colUsed[pos]);
  g_return_val_if_fail(val < (int)dt->nbColumns &&
		       val >= DATAFILE_NB_COLUMN_DEFAULT, FALSE);

  if (dt->colUsed[pos] == val)
    return FALSE;
  dt->colUsed[pos] = val;

  return dt->used;
}
/**
 * visu_colorization_getColUsed:
 * @dt: (allow-none): a #VisuData object.
 *
 * This method is used to retrieve the vector used to adapt or not the colour
 * to the value of the loaded data.
 *
 * Returns: (transfer none) (array fixed-size=3): a three value array,
 * own by V_Sim. It should not be freed.
 */
const int* visu_colorization_getColUsed(const VisuColorization *dt)
{
  if (!dt)
    return (const int*)0;
  else
    return dt->colUsed;
}
/**
 * visu_colorization_setScalingUsed:
 * @dt: a #VisuColorization object hosting the data values ;
 * @val: a column id.
 *
 * Give the column id to used to take the scaling values from. Set -1
 * if no scaling used. The scaling is used to change the size of each
 * node, using an homothetic factor.
 *
 * Returns: TRUE if the status changed.
 */
gboolean visu_colorization_setScalingUsed(VisuColorization *dt, int val)
{
  g_return_val_if_fail(dt, FALSE);

  DBG_fprintf(stderr, "Visu Colorization: scaling uses column %d (previuosly %d).\n",
	      val, dt->scaleUsed);
  g_return_val_if_fail((val < (int)dt->nbColumns &&
			val >= 0) || val == DATAFILE_NB_COLUMN_DEFAULT, FALSE);

  if (dt->scaleUsed == val)
    return FALSE;
  dt->scaleUsed = val;

  return dt->used;
}
/**
 * visu_colorization_getScalingUsed:
 * @dt: (allow-none): a #VisuColorization object hosting the data values.
 *
 * Retrieve if a column is used as entry to scale the nodes.
 *
 * Returns: -1 if no scaling is used.
 */
int visu_colorization_getScalingUsed(const VisuColorization *dt)
{
  if (!dt)
    return DATAFILE_NB_COLUMN_DEFAULT;
  else
    return dt->scaleUsed;
}
/**
 * visu_colorization_setShade:
 * @dt: the #VisuColorization object which the colour data are associated to ;
 * @shade: a valid #ToolShade object.
 *
 * Apply all caracteristic of the given shade to the colorization the
 * the given #VisuObject.
 *
 * Returns: TRUE if VisuNodeArray::RenderingChanged should be emitted.
 */
gboolean visu_colorization_setShade(VisuColorization *dt, ToolShade *shade)
{
  g_return_val_if_fail(dt, FALSE);

  DBG_fprintf(stderr, "Visu VisuColorization: set shade.\n");

  tool_shade_free(dt->shade);
  dt->shade = tool_shade_copy(shade);

  return dt->used;
}
/**
 * visu_colorization_getShade:
 * @dt: (allow-none): the #VisuColorization object which the colour data are associated to.
 *
 * Return the shade used to colourise the nodes.
 *
 * Returns: (transfer none): the ToolShade used (own by V_Sim).
 */
ToolShade* visu_colorization_getShade(const VisuColorization *dt)
{
  if (!dt)
    return (ToolShade*)0;
  else
    return dt->shade;
}
/**
 * visu_colorization_applyHide:
 * @dt: a #VisuColorization object.
 * @dataObj: a #VisuData object to apply hiding scheme on.
 *
 * From the hiding function of #dt (see
 * visu_colorization_setHidingFunc()), apply hiding property to nodes
 * of @dataObj.
 *
 * Since: 3.7
 *
 * Returns: TRUE if "VisibilityChanged" signal should be emitted.
 **/
gboolean visu_colorization_applyHide(VisuColorization *dt, VisuData *dataObj)
{
  gboolean redraw;
  VisuNodeArrayIter iter;
  VisuNodeProperty *prop;
  GValue data;
  VisuColorizationNodeData ct;

  g_return_val_if_fail(dt && dataObj, FALSE);

  if (!dt->used || dt->nbColumns == 0 || dt->hidingFunc == (VisuColorizationHidingFunc)0)
    return FALSE;

  memset(&data, '\0', sizeof(GValue));
  g_value_init(&data, G_TYPE_POINTER);
  prop = visu_node_array_getProperty(VISU_NODE_ARRAY(dataObj), DATAFILE_ID);
  ct.dataObj = dataObj;
  ct.data = g_array_new(FALSE, FALSE, sizeof(float));
  ct.data->len = dt->nbColumns;

  redraw = FALSE;
  visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter);
  for (visu_node_array_iterStartVisible(VISU_NODE_ARRAY(dataObj), &iter); iter.node;
       visu_node_array_iterNextVisible(VISU_NODE_ARRAY(dataObj), &iter))
    {
      visu_node_property_getValue(prop, iter.node, &data);
      ct.data->data = (gchar*)g_value_get_pointer(&data);
      ct.node = iter.node;
      g_return_val_if_fail(ct.data->data, FALSE);
      if (dt->hidingFunc(dt, &ct, dt->hidingData))
        redraw = visu_node_setVisibility(iter.node, FALSE) || redraw;
    }

  g_array_free(ct.data, FALSE);

  return redraw;
}
/**
 * visu_colorization_setHidingFunc:
 * @dt: a #VisuColorization object.
 * @func: (allow-none) (closure data): a #VisuColorizationHidingFunc function.
 * @data: (closure): some data.
 * @destroy: a destroy function for @data.
 *
 * Set the hiding function to be called when
 * visu_colorization_applyHide() is used.
 *
 * Since: 3.7
 **/
void visu_colorization_setHidingFunc(VisuColorization *dt, VisuColorizationHidingFunc func,
                                     gpointer data, GDestroyNotify destroy)
{
  g_return_if_fail(dt);

  /* Free previous association. */
  if (dt->hidingData && dt->hidingDestroy)
    dt->hidingDestroy(dt->hidingData);

  DBG_fprintf(stderr, "Visu Colorization: set hiding function with data %p.\n", data);
  dt->hidingFunc = func;
  dt->hidingData = data;
  dt->hidingDestroy = destroy;
}

/*******************/
/* Drawing methods */
/*******************/
/* In these methods, vectA and vectB (and others) are not retrieve from the visuData object for
   speed reason. We use instead global pointers dataFile_vectA/B that are
   pointing to the right place in the visuData object that needs to be rendered. */

/* Normalization method */
static float valuesFromData(VisuData *visuData _U_,
                            int column, float fromVal, gboolean *out)
{
  float res;

  g_return_val_if_fail(cacheVisuColorization, 0.);
  if (out)
    *out = FALSE;
  switch (cacheVisuColorization->scaleType)
    {
    case VISU_COLORIZATION_NORMALIZE:
      g_return_val_if_fail(column >= 0 &&
                           column < (int)cacheVisuColorization->nbColumns, 0.);
      res = ( (fromVal - cacheVisuColorization->readMinMax[column * 2 + 0]) /
	      (cacheVisuColorization->readMinMax[column * 2 + 1] -
	       cacheVisuColorization->readMinMax[column * 2 + 0]) );
      DBG_fprintf(stderr, "Visu VisuColorization: normalise %f -> %f.\n", fromVal, res);
      return res;
    case VISU_COLORIZATION_MINMAX:
      res = ( (fromVal - cacheVisuColorization->manualMinMax[2 * (column + 3) + 0]) /
	      (cacheVisuColorization->manualMinMax[2 * (column + 3) + 1] -
               cacheVisuColorization->manualMinMax[2 * (column + 3) + 0]) );
      DBG_fprintf(stderr, "Visu VisuColorization: normalise %f -> %f.\n",
		  fromVal, CLAMP(res, 0., 1.));
      if (out)
        *out = (res < 0. || res > 1.);
      return CLAMP(res, 0., 1.);
    }
  return 0.;
}

/*******************/
/* Color functions */
/*******************/
static gboolean colorFromUserData(VisuData *visuData, float rgba[4],
                                  VisuElement *ele _U_, VisuNode* node)
{
  float val[3], red[3], coord[3];
  int i;
  float *storedValues;
  gboolean useCoord, useData, status, out;

  g_return_val_if_fail(visuData && node && rgba, FALSE);

  useCoord = (cacheVisuColorization->colUsed[0] == DATAFILE_COLUMN_X ||
	      cacheVisuColorization->colUsed[0] == DATAFILE_COLUMN_Y ||
	      cacheVisuColorization->colUsed[0] == DATAFILE_COLUMN_Z ||
	      cacheVisuColorization->colUsed[1] == DATAFILE_COLUMN_X ||
	      cacheVisuColorization->colUsed[1] == DATAFILE_COLUMN_Y ||
	      cacheVisuColorization->colUsed[1] == DATAFILE_COLUMN_Z ||
	      cacheVisuColorization->colUsed[2] == DATAFILE_COLUMN_X ||
	      cacheVisuColorization->colUsed[2] == DATAFILE_COLUMN_Y ||
	      cacheVisuColorization->colUsed[2] == DATAFILE_COLUMN_Z);
  useData = (cacheVisuColorization->colUsed[0] >= 0 ||
	     cacheVisuColorization->colUsed[1] >= 0 ||
	     cacheVisuColorization->colUsed[2] >= 0);

  storedValues = (float*)0;
  if (useData)
    {
      visu_node_array_getPropertyValue(VISU_NODE_ARRAY(visuData), node,
                                       DATAFILE_ID, &dataValue);
      storedValues = (float*)g_value_get_pointer(&dataValue);
      g_return_val_if_fail(storedValues, FALSE);
    }
  if (useCoord)
    {
      visu_data_getNodePosition(visuData, node, coord);
      visu_box_convertXYZtoBoxCoordinates(visu_boxed_getBox(VISU_BOXED(visuData)), red, coord);
      if (cacheVisuColorization->scaleType == VISU_COLORIZATION_MINMAX)
	{
	  red[0] = CLAMP((red[0] - cacheVisuColorization->manualMinMax[0]) /
                         (cacheVisuColorization->manualMinMax[1] -
                          cacheVisuColorization->manualMinMax[0]), 0.f, 1.f);
	  red[1] = CLAMP((red[1] - cacheVisuColorization->manualMinMax[2]) /
                         (cacheVisuColorization->manualMinMax[3] -
                          cacheVisuColorization->manualMinMax[2]), 0.f, 1.f);
	  red[2] = CLAMP((red[2] - cacheVisuColorization->manualMinMax[4]) /
                         (cacheVisuColorization->manualMinMax[5] -
                          cacheVisuColorization->manualMinMax[4]), 0.f, 1.f);
	}
    }

  status = TRUE;
  for (i = 0; i < 3; i++)
    if (cacheVisuColorization->colUsed[i] == DATAFILE_NB_COLUMN_DEFAULT)
      val[i] = 1.;
    else if (cacheVisuColorization->colUsed[i] == DATAFILE_COLUMN_X)
      val[i] = tool_modulo_float(red[0], 1);
    else if (cacheVisuColorization->colUsed[i] == DATAFILE_COLUMN_Y)
      val[i] = tool_modulo_float(red[1], 1);
    else if (cacheVisuColorization->colUsed[i] == DATAFILE_COLUMN_Z)
      val[i] = tool_modulo_float(red[2], 1);
    else
      {
        val[i] = valuesFromData(visuData, cacheVisuColorization->colUsed[i],
                                storedValues[cacheVisuColorization->colUsed[i]], &out);
        status = status && !out;
      }

  if (cacheVisuColorization->applyToAll || status)
    tool_shade_channelToRGB(cacheVisuColorization->shade, rgba, val);
/*   fprintf(stderr, "%f %f %f  ->  %f %f %f %f\n", */
/* 	  val[0], val[1], val[2], rgba[0], rgba[1], rgba[2], rgba[3]); */

  return cacheVisuColorization->applyToAll || status;
}
static float radiusFromUserData(VisuData *visuData, VisuNode *node)
{
  float *storedValues;

  if (cacheVisuColorization->scaleUsed == DATAFILE_NB_COLUMN_DEFAULT)
    return 1.f;

  storedValues = (float*)0;
  visu_node_array_getPropertyValue(VISU_NODE_ARRAY(visuData), node,
                                   DATAFILE_ID, &dataValue);
  storedValues = (float*)g_value_get_pointer(&dataValue);
  return valuesFromData(visuData, cacheVisuColorization->scaleUsed,
                        storedValues[cacheVisuColorization->scaleUsed], (gboolean*)0);

}

static void exportResources(GString *data, VisuData *dataObj _U_, VisuGlView *view _U_)
{
  visu_config_file_exportComment(data, RESOURCE_RANGE_DESC);
  visu_config_file_exportEntry(data, RESOURCE_RANGE_NAME, NULL, "%d", restrictInRange);

  visu_config_file_exportComment(data, "");
}
static void onEntryRange(VisuObject *obj _U_, const gchar *key _U_, gpointer data)
{
  VisuColorization *dt = (VisuColorization*)data;

  dt->applyToAll = !restrictInRange;
}
