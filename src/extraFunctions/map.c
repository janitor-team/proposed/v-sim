/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresses mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "map.h"
#include "isoline.h"

#include <math.h>
#include <GL/gl.h>
#include <glib.h>
#include <string.h>

#include <coreTools/toolConfigFile.h>
#include <visu_configFile.h>

#ifdef HAVE_CAIRO
#include <cairo.h>
#include <cairo-svg.h>
#include <cairo-pdf.h>
#endif

/**
 * SECTION:map
 * @short_description: Describe how to handle and draw coloured map on
 * a plane from a density field.
 *
 * <para>A map is a coloured representation of a scalar field on a
 * plane. To define a new map, use visu_map_newFromPlane() and use
 * visu_map_setField() to associate a scalarfield to it.</para>
 * <para>The representation of the map is done by an adaptive mesh of
 * triangles. One can adjust the size of the smaller resolution by
 * using visu_map_setLevel(). Finally the level of adaptiveness is
 * chosen at rendering time by choosing a more or less crude precision
 * argument to visu_map_draw().</para>
 * <para>In adition to the colour representation, isolines can be
 * drawn at given iso-values, see visu_map_setLines().</para>
 * <para>An additionnal capability allows to export #VisuMap into SVG
 * or PDF vector files.</para>
 *
 * Since: 3.6
 */

/**
 * Triangle :
 * @triangle_ABC :a location to store coordinates of tree vertexs
 * @value_scalarfield: a location to store the value of scalarfideld
 * @min_node : the minimum value
 * @max_node : the maximum value
 * @rgba: a color,can be NULL;
 * @flag:return TRUE if the triangle has the least one vertex is out of bounds,return FALSE ifnot
 *
 * The structure is used to store informations about coordinates,color,position of each vertex and
 * the maximum and minimum value of scalarfield
 */
typedef struct _Triangle
{
  float vertices[3][3];
  float minmax[2];
  float value[3];

  guint level;
  struct _Triangle *children[4];
} Triangle;

struct _VisuMap
{
  guint refCount;

  /* Pointers to building objects. */
  VisuPlane *plane;
  glong moved_signal;
  VisuScalarField *field;

  /* Transformation values. */
  tool_matrix_getScaledValue get_val;
  tool_matrix_getScaledValue get_inv;
  double scaleminmax[2];
  float extension[3];

  /* Triangle structure. */
  guint level;
  GList *triangles;

  /* The min/max valuess in [0;1] after colour transformation. */
  float minmax[2];
  /* The min/max values in scalarfield unit. */
  float valMinMax[2];

  guint nLines;
  VisuLine **lines;
};

#define FLAG_RESOURCE_LEG_SCALE "map_legendScale"
#define DESC_RESOURCE_LEG_SCALE "Choose the scale to draw the legend for coloured maps ; float (positive)"
#define DEFT_RESOURCE_LEG_SCALE 1.f

#define FLAG_RESOURCE_LEG_POS "map_legendPosition"
#define DESC_RESOURCE_LEG_POS "Choose the legend position ; two floats (between 0 and 1)"
#define DEFT_RESOURCE_LEG_POS 0.f

static float legendScale, legendPosition[2];

static void exportResources(GString *data, VisuData *dataObj, VisuGlView *view);

static Triangle* triangle_new(float ABC[3][3], guint level);
static void triangle_free(Triangle *T);
static void triangle_getDataAtLevel(Triangle *T, guint level, float **data, guint *pos);
static void triangle_setValues(Triangle *T, VisuScalarField *field,
                               tool_matrix_getScaledValue get_val, double minmax[2],
                               float extension[3]);
static void triangle_draw(Triangle *T, float thresh, const ToolShade *shade);
static void triangle_drawWithAlpha(Triangle *T, float thresh, const ToolShade *shade);
#ifdef HAVE_CAIRO
static void triangle_drawToCairo(Triangle *T, cairo_t *cr, float thresh,
                                 const ToolShade *shade, float basis[2][3], float center[3]);
#endif

static void onPlaneMoved(VisuPlane *plane, gpointer data);

static Triangle* triangle_new(float ABC[3][3], guint level)
{
  Triangle *T;

  T = g_malloc(sizeof(Triangle));
  T->children[0] = (Triangle*)0;
  T->children[1] = (Triangle*)0;
  T->children[2] = (Triangle*)0;
  T->children[3] = (Triangle*)0;
  T->minmax[0] = G_MAXFLOAT;
  T->minmax[1] = -G_MAXFLOAT;
  T->level = level;
  memcpy(T->vertices, ABC, sizeof(float) * 9);
  /* DBG_fprintf(stderr, "VisuScalarField: create Triangle %p (%d).\n", (gpointer)T, level); */

  return T;
}
static void triangle_free(Triangle *T)
{
  if (T->children[0])
    triangle_free(T->children[0]);
  if (T->children[1])
    triangle_free(T->children[1]);
  if (T->children[2])
    triangle_free(T->children[2]);
  if (T->children[3])
    triangle_free(T->children[3]);
  g_free(T);
}                              

static void triangle_getDataAtLevel(Triangle *T, guint level, float **data, guint *pos)
{
  if (T->level == level)
    {
      (data + (*pos))[0] = (float*)T->vertices;
      (data + (*pos))[1] = (float*)T->value;
      *pos += 2;
      return;
    }
  if (T->children[0])
    triangle_getDataAtLevel(T->children[0], level, data, pos);
  if (T->children[1])
    triangle_getDataAtLevel(T->children[1], level, data, pos);
  if (T->children[2])
    triangle_getDataAtLevel(T->children[2], level, data, pos);
  if (T->children[3])
    triangle_getDataAtLevel(T->children[3], level, data, pos);
}

static void _3DToVisuPlane(float uv[2], float xyz[3],
                       float basis[2][3], float origin[3])
{
  uv[0] = basis[0][0] * (xyz[0] - origin[0]) + basis[0][1] * (xyz[1] - origin[1]) +  basis[0][2] * (xyz[2] - origin[2]);
  uv[1] = basis[1][0] * (xyz[0] - origin[0]) + basis[1][1] * (xyz[1] - origin[1]) +  basis[1][2] * (xyz[2] - origin[2]);
}
static void triangle_setValues(Triangle *T, VisuScalarField *field,
                               tool_matrix_getScaledValue get_val, double minmax[2],
                               float extension[3])
{
  double value;
  int i;

  g_return_if_fail(T);

  T->minmax[0] =  G_MAXFLOAT;
  T->minmax[1] = -G_MAXFLOAT;
  if (field)
    for (i = 0; i < 3; i++)
      {
        if (!visu_scalar_field_getValue(field, T->vertices[i], &value, extension))
          if (!visu_scalar_field_getValue(field, T->vertices[(i + 1) % 3], &value, extension))
            visu_scalar_field_getValue(field, T->vertices[(i + 2) % 3], &value, extension);
        T->value[i]  = (float)get_val(value, minmax);
        T->minmax[0] = MIN(T->minmax[0], T->value[i]);
        T->minmax[1] = MAX(T->minmax[1], T->value[i]);
      }
}

static void triangle_draw(Triangle *T, float thresh, const ToolShade *shade)
{
  /* gchar str[32]; */
  float rgba[4];

  if ((T->minmax[1] - T->minmax[0]) <= thresh ||
      (!T->children[0] && !T->children[1] && !T->children[2] && !T->children[3]))
    {
      glBegin(GL_TRIANGLE_STRIP);
      tool_shade_valueToRGB(shade, rgba, T->value[0]);
      glColor3fv(rgba);
      glVertex3fv(T->vertices[0]);
      tool_shade_valueToRGB(shade, rgba, T->value[1]);
      glColor3fv(rgba);
      glVertex3fv(T->vertices[1]);
      tool_shade_valueToRGB(shade, rgba, T->value[2]);
      glColor3fv(rgba);
      glVertex3fv(T->vertices[2]);
      glEnd();
      /* sprintf(str, "%6.5f", T->value[0]); */
      /* glColor3fv(T->rgba[0]); */
      /* glRasterPos3fv(T->vertices[0]); visu_gl_text_drawChars(str, VISU_GL_TEXT_SMALL); */
      /* sprintf(str, "%6.5f", T->value[1]); */
      /* glColor3fv(T->rgba[1]); */
      /* glRasterPos3fv(T->vertices[1]); visu_gl_text_drawChars(str, VISU_GL_TEXT_SMALL); */
      /* sprintf(str, "%6.5f", T->value[2]); */
      /* glColor3fv(T->rgba[2]); */
      /* glRasterPos3fv(T->vertices[2]); visu_gl_text_drawChars(str, VISU_GL_TEXT_SMALL); */
      return;
    }

  if (T->children[0])
    triangle_draw(T->children[0], thresh, shade);
  if (T->children[1])
    triangle_draw(T->children[1], thresh, shade);
  if (T->children[2])
    triangle_draw(T->children[2], thresh, shade);
  if (T->children[3])
    triangle_draw(T->children[3], thresh, shade);
}
static void triangle_drawWithAlpha(Triangle *T, float thresh, const ToolShade *shade)
{
  /* gchar str[32]; */
  float rgba[4];

  if ((T->minmax[1] - T->minmax[0]) <= thresh ||
      (!T->children[0] && !T->children[1] && !T->children[2] && !T->children[3]))
    {
      glBegin(GL_TRIANGLE_STRIP);
      tool_shade_valueToRGB(shade, rgba, T->value[0]);
      rgba[3] = T->value[0];
      glColor4fv(rgba);
      glVertex3fv(T->vertices[0]);
      tool_shade_valueToRGB(shade, rgba, T->value[1]);
      rgba[3] = T->value[1];
      glColor4fv(rgba);
      glVertex3fv(T->vertices[1]);
      tool_shade_valueToRGB(shade, rgba, T->value[2]);
      rgba[3] = T->value[2];
      glColor4fv(rgba);
      glVertex3fv(T->vertices[2]);
      glEnd();
      return;
    }

  if (T->children[0])
    triangle_drawWithAlpha(T->children[0], thresh, shade);
  if (T->children[1])
    triangle_drawWithAlpha(T->children[1], thresh, shade);
  if (T->children[2])
    triangle_drawWithAlpha(T->children[2], thresh, shade);
  if (T->children[3])
    triangle_drawWithAlpha(T->children[3], thresh, shade);
}
#ifdef HAVE_CAIRO
static void triangle_drawToCairo(Triangle *T, cairo_t *cr, float thresh,
                                 const ToolShade *shade, float basis[2][3], float center[3])
{
  float uv[2];
  float v, rgba[4];

  if ((T->minmax[1] - T->minmax[0]) <= thresh ||
      (!T->children[0] && !T->children[1] && !T->children[2] && !T->children[3]))
    {
      v = (T->value[0] + T->value[1] + T->value[2]) / 3.f;
      tool_shade_valueToRGB(shade, rgba, v);
      cairo_set_source_rgba(cr, rgba[0], rgba[1], rgba[2], 1.f);
      _3DToVisuPlane(uv, T->vertices[0], basis, center);
      cairo_move_to(cr, uv[0], uv[1]);
      _3DToVisuPlane(uv, T->vertices[1], basis, center);
      cairo_line_to(cr, uv[0], uv[1]);
      _3DToVisuPlane(uv, T->vertices[2], basis, center);
      cairo_line_to(cr, uv[0], uv[1]);
      _3DToVisuPlane(uv, T->vertices[0], basis, center);
      cairo_line_to(cr, uv[0], uv[1]);
      cairo_fill_preserve(cr);
      /* cairo_set_source_rgb(cr, 0., 0., 0.); */
      cairo_stroke(cr);
      return;
    }

  if (T->children[0])
    triangle_drawToCairo(T->children[0], cr, thresh, shade, basis, center);
  if (T->children[1])
    triangle_drawToCairo(T->children[1], cr, thresh, shade, basis, center);
  if (T->children[2])
    triangle_drawToCairo(T->children[2], cr, thresh, shade, basis, center);
  if (T->children[3])
    triangle_drawToCairo(T->children[3], cr, thresh, shade, basis, center);
}
#endif



/**************************/
/* The Map object itself. */
/**************************/
/**
 * visu_map_get_type:
 * @void: 
 *
 * Create and retrieve a #GType for a #VisuMap object.
 *
 * Since: 3.7
 *
 * Returns: a new type for #VisuMap structures.
 **/
GType visu_map_get_type(void)
{
  static GType g_define_type_id = 0;

  if (g_define_type_id == 0)
    g_define_type_id =
      g_boxed_type_register_static("VisuMap", 
                                   (GBoxedCopyFunc)visu_map_ref,
                                   (GBoxedFreeFunc)visu_map_unref);
  return g_define_type_id;
}
/**
 * visu_map_ref:
 * @map: a #VisuMap object.
 *
 * Increase the ref counter.
 *
 * Since: 3.7
 *
 * Returns: itself.
 **/
VisuMap* visu_map_ref(VisuMap *map)
{
  map->refCount += 1;
  return map;
}
/**
 * visu_map_unref:
 * @map: a #VisuMap object.
 *
 * Decrease the ref counter, free all memory if counter reachs zero.
 *
 * Since: 3.7
 **/
void visu_map_unref(VisuMap *map)
{
  map->refCount -= 1;
  if (!map->refCount)
    visu_map_free(map);
}
/**
 * visu_map_new:
 *
 * Creates a new #VisuMap object.
 *
 * Since: 3.6
 *
 * Returns: (transfer full): a newly created #VisuMap object.
 */
VisuMap* visu_map_new()
{
  VisuMap *map;

  map = g_malloc(sizeof(VisuMap));

  map->plane = (VisuPlane*)0;
  map->moved_signal = 0;
  map->field = (VisuScalarField*)0;

  map->level     = 0;
  map->minmax[0] =  G_MAXFLOAT;
  map->minmax[1] = -G_MAXFLOAT;
  map->triangles = (GList*)0;

  map->nLines    = 0;
  map->lines     = (VisuLine**)0;

  DBG_fprintf(stderr, "Map: create a new map object at %p.\n", (gpointer)map);
  return map;
}
/**
 * visu_map_newFromPlane:
 * @plane: a #VisuPlane object.
 *
 * Creates a new #VisuMap object, projected on @plane.
 *
 * Since: 3.6
 *
 * Returns: a newly created #VisuMap object.
 */
VisuMap* visu_map_newFromPlane(VisuPlane *plane)
{
  VisuMap *map;

  map = visu_map_new();
  visu_map_setPlane(map, plane);
  return map;
}
/**
 * visu_map_free:
 * @map: a #VisuMap object.
 *
 * Deallocate all memory related to @map.
 *
 * Since: 3.6
 */
void visu_map_free(VisuMap *map)
{
  DBG_fprintf(stderr, "Map: free object %p.\n", (gpointer)map);
  visu_map_setPlane(map, (VisuPlane*)0);
  visu_map_setField(map, (VisuScalarField*)0, 0, (float*)0);
  g_free(map);
}

static float** map_getDataAtLevel(VisuMap *map, guint level, guint *n)
{
  float **data;
  guint i, m;
  GList *lst;

  g_return_val_if_fail(map && map->triangles, (float**)0);
  g_return_val_if_fail(level < 13, (float**)0);

  m = pow(4, level) * g_list_length(map->triangles);
  data = g_malloc(sizeof(float*) * m * 2);
  if (n)
    *n = m;

  DBG_fprintf(stderr, "VisuScalarField: generate the triangle data for %d triangles.\n", m);
  i = 0;
  for (lst = map->triangles; lst; lst = g_list_next(lst))
    triangle_getDataAtLevel((Triangle*)lst->data, level, data, &i);

  return data;
}
/**
 * visu_map_setPlane:
 * @map: a #VisuMap object.
 * @plane: (transfer full) (allow-none): a #VisuPlane object.
 *
 * Set the internal #VisuPlane of @map to @plane. All changed to
 * @plane will be automatically propagated to @map. Use
 * visu_map_setField() to choose the field to take values from, set
 * the level precision with visu_map_setLevel() and finally call
 * visu_map_compute() to get the map.
 *
 * Since: 3.7
 *
 * Returns: TRUE if internal plane is changed.
 **/
gboolean visu_map_setPlane(VisuMap *map, VisuPlane *plane)
{
  GList *inter, *lst;
  float xyz[2][3], ABC[3][3];
  guint i;

  g_return_val_if_fail(map, FALSE);

  /* Free currently allocated triangles. */
  for (lst = map->triangles; lst; lst = g_list_next(lst))
    triangle_free((Triangle*)lst->data);
  g_list_free(map->triangles);
  map->triangles = (GList*)0;
  for (i = 0; i < map->nLines; i++)
    visu_line_free(map->lines[i]);
  if (map->lines)
    g_free(map->lines);
  map->lines = (VisuLine**)0;
  map->nLines = 0;
  /* Free previous handle on plane. */
  if (map->plane)
    {
      g_signal_handler_disconnect(G_OBJECT(map->plane), map->moved_signal);
      g_object_unref(map->plane);
    }
  
  map->plane = plane;
  if (!plane)
    return TRUE;

  /* Reallocate everyone. */
  g_object_ref(plane);
  map->moved_signal = g_signal_connect(G_OBJECT(plane), "moved",
                                       G_CALLBACK(onPlaneMoved), (gpointer)map);

  inter = visu_plane_getIntersection(plane);
  visu_plane_getBasis(plane, xyz, ABC[0]);
  if (!inter)
    return TRUE;
  
  i = 1;
  ABC[i][0] = ((float*)inter->data)[0];
  ABC[i][1] = ((float*)inter->data)[1];
  ABC[i][2] = ((float*)inter->data)[2];
  for (inter = g_list_next(inter); inter; inter = g_list_next(inter))
    {
      i = i % 2 + 1;
      ABC[i][0] = ((float*)inter->data)[0];
      ABC[i][1] = ((float*)inter->data)[1];
      ABC[i][2] = ((float*)inter->data)[2];
      map->triangles = g_list_append(map->triangles, triangle_new(ABC, 0));
    }
  inter = visu_plane_getIntersection(plane);
  i = i % 2 + 1;
  ABC[i][0] = ((float*)inter->data)[0];
  ABC[i][1] = ((float*)inter->data)[1];
  ABC[i][2] = ((float*)inter->data)[2];
  map->triangles = g_list_append(map->triangles, triangle_new(ABC, 0));

  visu_box_getExtension(visu_boxed_getBox(VISU_BOXED(map->plane)), map->extension);

  return TRUE;
}
/**
 * visu_map_setField:
 * @map: a #VisuMap object.
 * @field: a #VisuScalarField object.
 * @scale: a flag.
 * @inputMinMax: two floats or NULL.
 *
 * It associates the values of @field to @map. The calculation is not
 * directly done, and a call to visu_map_compute() is necessary. If
 * @inputMinMax is provided, these values are used to scale the values
 * of @field to [0;1], otherwise the minMax values of the field itself
 * are used. The scaling algorithm is defined by @scale.
 *
 * Since: 3.6
 *
 * Returns: TRUE if @field is changed.
 */
gboolean visu_map_setField(VisuMap *map, VisuScalarField *field,
                           ToolMatrixScalingFlag scale, float *inputMinMax)
{
  g_return_val_if_fail(map, FALSE);

  if (map->field)
    g_object_unref(G_OBJECT(map->field));

  map->field = field;
  map->minmax[0] =  G_MAXFLOAT;
  map->minmax[1] = -G_MAXFLOAT;

  if (!field)
    return TRUE;

  g_object_ref(G_OBJECT(field));

  if (!inputMinMax)
    visu_scalar_field_getMinMax(field, map->scaleminmax);
  else
    {
      map->scaleminmax[0] = inputMinMax[0];
      map->scaleminmax[1] = inputMinMax[1];
    }

  /* In log scale, we need the second minimum value. */
  switch (scale)
    {
    case TOOL_MATRIX_SCALING_LINEAR:
      map->get_val = tool_matrix_getScaledLinear;
      map->get_inv = tool_matrix_getScaledLinearInv;
      break;
    case TOOL_MATRIX_SCALING_LOG:
      map->get_val = tool_matrix_getScaledLog;
      map->get_inv = tool_matrix_getScaledLogInv;
      break;
    case TOOL_MATRIX_SCALING_ZERO_CENTRED_LOG:
      map->get_val = tool_matrix_getScaledZeroCentredLog;
      map->get_inv = tool_matrix_getScaledZeroCentredLogInv;
      break;
    default:
      map->get_val = tool_matrix_getScaledLinear;
      map->get_inv = tool_matrix_getScaledLinearInv;
      break;
    }

  return TRUE;
}
/**
 * visu_map_setLevel:
 * @map: a #VisuMap object.
 * @glPrec: the global OpenGL precision for drawing (default is 1.).
 * @gross: current zoom level.
 * @refLength: a reference length (see visu_gl_camera_getRefLength()).
 *
 * Setup the level of recursivity in triangle calculation, depending
 * on the current zoom level.
 *
 * Since: 3.6
 *
 * Returns: TRUE if the level is actually changed.
 */
gboolean visu_map_setLevel(VisuMap *map, float glPrec, float gross, float refLength)
{
  float length, mLength, *xyz, basis[2][3], center[3];
  GList *inter;
  guint level;

  g_return_val_if_fail(map && map->plane, FALSE);

  visu_plane_getBasis(map->plane, basis, center);
  mLength = 0.f;
  /* In case of no intersections, we build an empty list. */
  for (inter = visu_plane_getIntersection(map->plane); inter; inter = g_list_next(inter))
    {
      xyz = (float*)inter->data;
      mLength = MAX(mLength,
                    (xyz[0] - center[0]) * (xyz[0] - center[0]) +
                    (xyz[1] - center[1]) * (xyz[1] - center[1]) +
                    (xyz[2] - center[2]) * (xyz[2] - center[2]));
    }

  /* We put by default (precision and gross = 1) 300 triangles along
     the longest line. */
  length = refLength / (300. * glPrec * (gross * .5 + .5));
  level = MIN(12, MAX((guint)(log(sqrt(mLength) / length) / log(2)), 1) - 1);
  DBG_fprintf(stderr, "Map: number of division %d for map %p.\n", level, (gpointer)map);

  if (level == map->level)
    return FALSE;

  map->level = level;
  if (level > 20)
    g_warning("Important level for triangle refining in maps.");
  return TRUE;
}
/**
 * visu_map_setLines:
 * @map: a #VisuMap object.
 * @nIsoLines: number of required isolines.
 * @minmax: span for isoline values.
 *
 * Calculate @nIsoLines equally distributed in @minmax.
 *
 * Since: 3.6
 *
 * Returns: TRUE if lines are successfully calculated.
 */
gboolean visu_map_setLines(VisuMap *map, guint nIsoLines, float minmax[2])
{
  float v, **data;
  guint i, j, n;

  g_return_val_if_fail(map, FALSE);

  DBG_fprintf(stderr, "Map: free old lines (%d).\n", map->nLines);
  for (i = 0; i < map->nLines; i++)
    visu_line_free(map->lines[i]);
  if (map->lines)
    g_free(map->lines);
  map->nLines = 0;
  map->lines  = (VisuLine**)0;

  DBG_fprintf(stderr, "Map: create %d new lines for map %p.\n", nIsoLines, (gpointer)0);
  if (nIsoLines > 0)
    {
      n = 0;
      data = map_getDataAtLevel(map, map->level, &n);
      map->lines = g_malloc(sizeof(VisuLine*) * nIsoLines);
      j = 0;
      for (i = 1; i <= nIsoLines; i++)
        {
          v = minmax[0] + (minmax[1] - minmax[0]) *
            (float)i / (float)(nIsoLines + 1);
          DBG_fprintf(stderr, "Map: compute line %d at value %f.\n", j, v);
          map->lines[j] = visu_line_newFromTriangles(data, n, v);
          if (map->lines[j])
            j += 1;
          DBG_fprintf(stderr, " | Ok %p.\n", (gpointer)map->lines[j]);
        }
      map->nLines = j;
      g_free(data);
      if (!map->nLines)
        {
          g_free(map->lines);
          map->lines  = (VisuLine**)0;
        }
    }

  return TRUE;
}
static void map_refine(VisuMap *map, Triangle *T)
{
  float ABC[3][3];

  g_return_if_fail(T && map && T->level < 100);

  /* We update the values of this triangle. */
  triangle_setValues(T, map->field, map->get_val, 
                     map->scaleminmax, map->extension);

  if (T->level >= map->level)
    return;

  if (!T->children[0])
    {
      ABC[0][0] = T->vertices[0][0]; ABC[0][1] = T->vertices[0][1]; ABC[0][2] = T->vertices[0][2];
      ABC[1][0] = 0.5f * (ABC[0][0] + T->vertices[1][0]);
      ABC[1][1] = 0.5f * (ABC[0][1] + T->vertices[1][1]);
      ABC[1][2] = 0.5f * (ABC[0][2] + T->vertices[1][2]);
      ABC[2][0] = 0.5f * (ABC[0][0] + T->vertices[2][0]);
      ABC[2][1] = 0.5f * (ABC[0][1] + T->vertices[2][1]);
      ABC[2][2] = 0.5f * (ABC[0][2] + T->vertices[2][2]);
      T->children[0] = triangle_new(ABC, T->level + 1);
    }
  map_refine(map, T->children[0]);
  T->minmax[0] = MIN(T->minmax[0], T->children[0]->minmax[0]);
  T->minmax[1] = MAX(T->minmax[1], T->children[0]->minmax[1]);

  if (!T->children[1])
    {
      ABC[0][0] = 0.5f * (T->vertices[2][0] + T->vertices[1][0]);
      ABC[0][1] = 0.5f * (T->vertices[2][1] + T->vertices[1][1]);
      ABC[0][2] = 0.5f * (T->vertices[2][2] + T->vertices[1][2]);
      T->children[1] = triangle_new(ABC, T->level + 1);
    }
  map_refine(map, T->children[1]);
  T->minmax[0] = MIN(T->minmax[0], T->children[1]->minmax[0]);
  T->minmax[1] = MAX(T->minmax[1], T->children[1]->minmax[1]);

  if (!T->children[2])
    {
      ABC[2][0] = T->vertices[1][0];
      ABC[2][1] = T->vertices[1][1];
      ABC[2][2] = T->vertices[1][2];
      T->children[2] = triangle_new(ABC, T->level + 1);
    }
  map_refine(map, T->children[2]);
  T->minmax[0] = MIN(T->minmax[0], T->children[2]->minmax[0]);
  T->minmax[1] = MAX(T->minmax[1], T->children[2]->minmax[1]);

  if (!T->children[3])
    {
      ABC[1][0] = 0.5f * (T->vertices[0][0] + T->vertices[2][0]);
      ABC[1][1] = 0.5f * (T->vertices[0][1] + T->vertices[2][1]);
      ABC[1][2] = 0.5f * (T->vertices[0][2] + T->vertices[2][2]);
      ABC[2][0] = T->vertices[2][0];
      ABC[2][1] = T->vertices[2][1];
      ABC[2][2] = T->vertices[2][2];
      T->children[3] = triangle_new(ABC, T->level + 1);
    }
  map_refine(map, T->children[3]);
  T->minmax[0] = MIN(T->minmax[0], T->children[3]->minmax[0]);
  T->minmax[1] = MAX(T->minmax[1], T->children[3]->minmax[1]);
}
/**
 * visu_map_compute:
 * @map: a #VisuMap object.
 *
 * After a #VisuPlane and a #VisuScalarField has been defined (see
 * visu_map_newFromPlane() and visu_map_setField()), the map is
 * computed by refining a triangle approximation up to level defined
 * by visu_map_setLevel(). The evaluation is lazy and calculation is
 * indeed done, only if required level is lower than actual
 * level. Calculation is done only for missing levels anyway.
 *
 * Since: 3.6
 */
void visu_map_compute(VisuMap *map)
{
  GList *inter;

  g_return_if_fail(map && map->plane && map->field);

  DBG_fprintf(stderr, "Map: refine all triangles of map %p (%d).\n",
              (gpointer)map, g_list_length(map->triangles));
  for (inter = map->triangles; inter; inter = g_list_next(inter))
    {
      map_refine(map, (Triangle*)inter->data);
      DBG_fprintf(stderr, " | %p done.\n", inter->data);
      map->minmax[0] = MIN(map->minmax[0], ((Triangle*)inter->data)->minmax[0]);
      map->minmax[1] = MAX(map->minmax[1], ((Triangle*)inter->data)->minmax[1]);
    }
  DBG_fprintf(stderr, " | minmax is %g %g.\n", map->minmax[0], map->minmax[1]);
  map->valMinMax[0] = map->get_inv(map->minmax[0], map->scaleminmax);
  map->valMinMax[1] = map->get_inv(map->minmax[1], map->scaleminmax);
  DBG_fprintf(stderr, " | memory usage is %gko\n",
              g_list_length(map->triangles) * pow(4, map->level) *
              sizeof(Triangle) / 1024.f);
  DBG_fprintf(stderr, " | %d lines.\n", map->nLines);
}
static void onPlaneMoved(VisuPlane *plane, gpointer data)
{
  VisuMap *map = (VisuMap*)data;

  /* We reset the plane. */
  visu_map_setPlane(map, plane);
  /* We recompute. */
  if (map->field)
    visu_map_compute(map);
}
/**
 * visu_map_draw:
 * @map: a #VisuMap object.
 * @prec: a pourcentage value.
 * @shade: a #ToolShade object.
 * @rgb: a colour or NULL.
 * @alpha: a boolean.
 *
 * It draws the @map with the given @shade. @prec give the level of
 * refinement used to draw the map, 100 means normal and 200 means
 * twice smaller level. If @rgb is present, this colour is used for
 * possible isolines. If @alpha is TRUE, an alpha channel is added as
 * a linear variation of the value of each vertex.
 * 
 * Since: 3.6
 */
void visu_map_draw(VisuMap *map, float prec, ToolShade *shade, float *rgb,
                   gboolean alpha)
{
  GList *inter;
  float thresh;
  guint i;
  float rgba[4];

  if (!map->plane || !map->field)
    return;
  
  glDisable(GL_CULL_FACE);
  glDisable(GL_LIGHTING);

  prec = (.06f - prec * 0.0003f);
  DBG_fprintf(stderr, "Map: plot triangles.\n");
  thresh = (map->minmax[1] - map->minmax[0]) * prec;
  if (alpha)
    for (inter = map->triangles; inter; inter = g_list_next(inter))
      triangle_drawWithAlpha((Triangle*)inter->data, thresh, shade);
  else
    for (inter = map->triangles; inter; inter = g_list_next(inter))
      triangle_draw((Triangle*)inter->data, thresh, shade);

  DBG_fprintf(stderr, "Map: plot lines.\n");
  for (i = 0; i < map->nLines; i++)
    {
      if (!rgb)
        {
          tool_shade_valueToRGB(shade, rgba, visu_line_getValue(map->lines[i]));
          rgba[0] = 1.f - rgba[0];
          rgba[1] = 1.f - rgba[1];
          rgba[2] = 1.f - rgba[2];
          rgb = rgba;
        }
      visu_line_draw(map->lines[i], rgb);
    }

  if (!alpha)
    {
      DBG_fprintf(stderr, "Map: plot box.\n");
      glLineWidth(1.f);
      glColor3f(0.f, 0.f, 0.f);
      glBegin(GL_LINE_LOOP);
      for (inter = visu_plane_getIntersection(map->plane); inter; inter = g_list_next(inter))
        glVertex3fv((float*)inter->data);
      glEnd();
    }

  glEnable(GL_CULL_FACE);
  glEnable(GL_LIGHTING);
}
/**
 * visu_map_getPlane:
 * @map: a #VisuMap object.
 *
 * Retrieves the plane @map is based on.
 *
 * Since: 3.7
 *
 * Returns: (transfer none) (allow-none): a #VisuPlane or %NULL.
 **/
VisuPlane* visu_map_getPlane(VisuMap *map)
{
  g_return_val_if_fail(map, (VisuPlane*)0);

  return map->plane;
}
/**
 * visu_map_getScaledMinMax:
 * @map: a #VisuMap object.
 *
 * After @map has been computed by visu_map_compute(), one can access
 * the scaled min and max values represented in the map. For
 * field values, see visu_map_getFieldMinMax().
 *
 * Since: 3.6
 *
 * Returns: two floats being the min and the max in [0;1].
 */
float* visu_map_getScaledMinMax(VisuMap *map)
{
  g_return_val_if_fail(map, (float*)0);

  return map->minmax;
}
/**
 * visu_map_getFieldMinMax:
 * @map: a #VisuMap object.
 *
 * After @map has been computed by visu_map_compute(), one can access
 * the min and max values of the field as represented in the map. For
 * scaled values, see visu_map_getScaledMinMax().
 *
 * Since: 3.6
 *
 * Returns: two floats being the min and the max.
 */
float* visu_map_getFieldMinMax(VisuMap *map)
{
  g_return_val_if_fail(map, (float*)0);

  return map->valMinMax;
}

/**
 * visu_map_export:
 * @map: a #VisuMap object.
 * @shade: a #ToolShade object.
 * @rgb: a colour (can be NULL).
 * @precision: a pourcentage for rendering quality.
 * @filename: the location to export to.
 * @format: the kind of format.
 * @error: a location to store an error.
 *
 * Export the given map to the @format, using the @shade color. If
 * @rgb is provided and @map has some isolines, they will be drawn
 * with this colour, otherwise an inverse colour is used.
 *
 * Since: 3.6
 *
 * Returns: TRUE if no error.
 */
gboolean visu_map_export(VisuMap *map, ToolShade *shade, float *rgb, float precision,
                         gchar *filename, VisuMapExportFormat format, GError **error)
{
#ifdef HAVE_CAIRO
#define fact 25.f
  float viewport[4];
  guint i, j;
  float *uvs;
  guint nVals;
  cairo_surface_t *surface;
  cairo_t *cr;
  cairo_status_t status;
  cairo_matrix_t mat = {fact, 0., 0., fact, 0., 0.};
  float basis[2][3], center[3], uv[2], rgba[4];
  GList *lst;
  
  g_return_val_if_fail(error && *error == (GError*)0, FALSE);

  viewport[0] = viewport[2] =  G_MAXFLOAT;
  viewport[1] = viewport[3] = -G_MAXFLOAT;
  visu_plane_getBasis(map->plane, basis, center);
  for (lst = visu_plane_getIntersection(map->plane); lst; lst = g_list_next(lst))
    {
      _3DToVisuPlane(uv, (float*)lst->data, basis, center);
      viewport[0] = MIN(viewport[0], uv[0]);
      viewport[1] = MAX(viewport[1], uv[0]);
      viewport[2] = MIN(viewport[2], uv[1]);
      viewport[3] = MAX(viewport[3], uv[1]);
    }
  DBG_fprintf(stderr, "Map: begin export to PDF/SVG in (%f-%f x %f-%f).\n",
              viewport[0], viewport[1], viewport[2], viewport[3]);
  switch (format)
    {
    case VISU_MAP_EXPORT_SVG:
      surface = cairo_svg_surface_create(filename,
					 (double)((viewport[1] - viewport[0]) * fact),
					 (double)((viewport[3] - viewport[2]) * fact));
      break;
    case VISU_MAP_EXPORT_PDF:
      surface = cairo_pdf_surface_create(filename,
					 (double)((viewport[1] - viewport[0]) * fact),
					 (double)((viewport[3] - viewport[2]) * fact));
      break;
    default:
      surface = (cairo_surface_t*)0;
    }
  status = cairo_surface_status(surface);
  if (status != CAIRO_STATUS_SUCCESS)
    {
      *error = g_error_new(G_FILE_ERROR, G_FILE_ERROR_FAILED,
			   "%s", cairo_status_to_string(status));
      cairo_surface_destroy(surface);
      visu_map_free(map);
      return FALSE;
    }

  cr = cairo_create(surface);
  status = cairo_status(cr);
  if (status != CAIRO_STATUS_SUCCESS)
    {
      *error = g_error_new(G_FILE_ERROR, G_FILE_ERROR_FAILED,
			   "%s", cairo_status_to_string(status));
      cairo_destroy(cr);
      cairo_surface_destroy(surface);
      visu_map_free(map);
      return FALSE;
    }
  mat.x0 = -(double)viewport[0] * fact;
  mat.y0 = -(double)viewport[2] * fact;
  cairo_set_matrix(cr, &mat);

  cairo_set_line_cap(cr, CAIRO_LINE_CAP_BUTT);
  cairo_set_line_join(cr, CAIRO_LINE_JOIN_BEVEL);
  cairo_set_line_width(cr, 0.01);

  for (lst = map->triangles; lst; lst = g_list_next(lst))
    triangle_drawToCairo((Triangle*)lst->data, cr,
                         (map->minmax[1] - map->minmax[0]) * (.06f - precision * 0.0003f),
                         shade, basis, center);

  /* Add isolines. */
  DBG_fprintf(stderr, "Map: export isolines.\n");
  for (i = 0; i < map->nLines; i++)
    {
      if (!rgb)
        {
          tool_shade_valueToRGB(shade, rgba, visu_line_getValue(map->lines[i]));
          rgba[0] = 1.f - rgba[0];
          rgba[1] = 1.f - rgba[1];
          rgba[2] = 1.f - rgba[2];
          rgba[3] = 1.f;
          rgb = rgba;
        }
      cairo_set_source_rgb(cr, rgb[0], rgb[1], rgb[2]);

      DBG_fprintf(stderr, "VisuScalarFields: project line on plane %p.\n",
                  (gpointer)map->plane);
      uvs = visu_line_project(map->lines[i], map->plane, &nVals);
      for (j = 0; j < nVals; j++)
        {
          cairo_move_to(cr, uvs[j * 4 + 0], uvs[j * 4 + 1]);
          cairo_line_to(cr, uvs[j * 4 + 2], uvs[j * 4 + 3]);
          cairo_stroke(cr);
        }
      g_free(uvs);
    }

  /* Add frame. */
  DBG_fprintf(stderr, "Map: export the frame.\n");
  uvs = visu_plane_getReducedIntersection(map->plane, &nVals);
  if (uvs)
    {
      cairo_set_source_rgb(cr, 0., 0., 0.);

      cairo_move_to(cr, uvs[(nVals - 1) * 2 + 0], uvs[(nVals - 1) * 2 + 1]);
      for (j = 0; j < nVals; j++)
        cairo_line_to(cr, uvs[j * 2 + 0], uvs[j * 2 + 1]);
      cairo_stroke(cr);
      g_free(uvs);
    }

  /* Add legend. */
/*   toolShadeExport(shade, (double)(viewport[2] * fact), */
/* 		  (double)(viewport[3] * fact)); */

  /* Finalising */
  cairo_show_page(cr);
  cairo_destroy(cr);
  cairo_surface_destroy(surface);

  return TRUE;
#else
  g_error("Not compiled with CAIRO not able to export.");
  return FALSE;
#endif
}

/*****************/
/* Init routine. */
/*****************/
/**
 * visu_map_init: (skip)
 *
 * Initialise the map routines. Should not be called except at
 * initialisation time.
 *
 * Since: 3.7
 */
void visu_map_init()
{
  float rgWidth[2] = {0.01f, 10.f};
  float rgPos[2] = {0.f, 1.f};
  VisuConfigFileEntry *conf;

  DBG_fprintf(stderr, "Map: set the conf entries for this module.\n");
  conf = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                             FLAG_RESOURCE_LEG_SCALE,
                                             DESC_RESOURCE_LEG_SCALE,
                                             1, &legendScale, rgWidth);
  visu_config_file_entry_setVersion(conf, 3.7f);
  conf = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                             FLAG_RESOURCE_LEG_POS,
                                             DESC_RESOURCE_LEG_POS,
                                             2, legendPosition, rgPos);
  visu_config_file_entry_setVersion(conf, 3.7f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
				   exportResources);

  legendScale = DEFT_RESOURCE_LEG_SCALE;
  legendPosition[0] = DEFT_RESOURCE_LEG_POS;
  legendPosition[1] = DEFT_RESOURCE_LEG_POS;
}

/*************************/
/* Resources management. */
/*************************/
static void exportResources(GString *data, VisuData *dataObj _U_, VisuGlView *view _U_)
{
  visu_config_file_exportComment(data, DESC_RESOURCE_LEG_SCALE);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_LEG_SCALE, NULL,
                               "%f", legendScale);

  visu_config_file_exportComment(data, DESC_RESOURCE_LEG_POS);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_LEG_POS, NULL,
                               "%f %f", legendPosition[0], legendPosition[1]);

  visu_config_file_exportComment(data, "");
}
float visu_map_getLegendScale()
{
  return legendScale;
}
float visu_map_getLegendPosition(ToolXyzDir dir)
{
  return (dir == TOOL_XYZ_X || dir == TOOL_XYZ_Y)?legendPosition[dir]:0.f;
}
