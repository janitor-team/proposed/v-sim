/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : J�r�my BLANC et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BLANC, 
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : J�r�my BLANC et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "vibration.h"

#include <string.h>
#include <math.h> /*for sinus*/
#include <coreTools/toolMatrix.h> /* for spherical coordonates */

#include <visu_object.h>
#include <visu_rendering.h>
#include <opengl.h> /* for highlight methods*/
#include <openGLFunctions/objectList.h> /* permit to draw arrow */
#include <extensions/node_vectors.h>
#include <visu_basic.h>

/**
 * SECTION:vibration
 * @short_description: Add a support to animate the node positions
 * with a phonon frequency.
 *
 * <para>Define a way to store vibration or phonons to a #VisuData
 * object. One can store several phonons in one object, each phonon is
 * them represented by its id. The phonons can be animated on screen,
 * using a user defined frequency (visu_vibration_setUserFrequency)
 * and amplitue (visu_vibration_setAmplitude), by calling
 * visu_vibration_play().</para>
 * <para>Phonons are set with visu_vibration_setCharacteristic() and
 * #VisuNode displacements are set with visu_vibration_setDisplacements().</para>
 */

#define OMEGA_COEFF 0.05f
#define AMPL_COEFF 0.5f

/* Displacement vector per atom is (U+iV) = (Ux + iVx, ...).
   Current applied displacement on one atom is (dx, dy, dz).
   qr is the Q.R part of the exponential. */
enum
  {
    Ux,
    Uy,
    Uz,
    mod,
    theta,
    phi,
    Vx,
    Vy,
    Vz,
    dx,
    dy,
    dz,
    qr,
    nData
  };

/*Local Method */
static void freeVib(gpointer data);
static void freeVibe(gpointer obj, gpointer data _U_);
static gpointer newOrCopyVibe(gconstpointer obj, gpointer data _U_);
static void vibrationIncr(VisuData *data, gboolean time, gboolean phase);
static void onPopulationChanged(VisuData *dataObj, gint *ids, gpointer data);
static void onPositionChanged(VisuData *dataObj, VisuElement *ele, gpointer data);
static gboolean move(gpointer data);

typedef struct vibration_
{
  /* Parameters for the available phonons. */
  guint n;
  float *q;
  float *omega;
  float *en;
  /* Array of all the displacement vectors. */
  gboolean *loaded;
  guint nUs;
  float *u;
  float *norm;

  /* Current applied frequency and amplitude, phonon id, time and delta time. */
  float freq, ampl;
  gint iph;
  gulong t;

  /* Internal parameters. */
  gboolean haveTime, havePhase;
  gulong signal;
} Vibration;

/******************************************************************************************************/

/**
 * visu_vibration_play:
 * @data: a #VisuData object.
 *
 * Make the phonon displace the #VisuNode of @data according to their
 * definitions (see visu_vibration_setDisplacements() and
 * visu_vibration_setCurrentMode()).
 *
 * Since: 3.5
 *
 * Returns: the timer's ID
 */
guint visu_vibration_play(VisuData *data)
{
  Vibration *vib;
  guint timeoutID = 0;

  vib = (Vibration*)g_object_get_data(G_OBJECT(data), VISU_VIBRATION_ID);
  g_return_val_if_fail(vib, 0);

  timeoutID = g_timeout_add(50, (GSourceFunc)move, data);
 
  return timeoutID;
}
/**
 * visu_vibration_stop:
 * @timeoutID: an id.
 *
 * Stop the timer with the given @timeoutID.
 *
 * Since: 3.5
 *
 * Returns: if the timer is indeed removed.
 */
gboolean visu_vibration_stop(guint timeoutID)
{
  /* to stop the timer */
  return g_source_remove(timeoutID);
}

/******************************************************************************************************/

static void freeVibe(gpointer obj, gpointer data _U_)
{
#if GLIB_MINOR_VERSION > 9
  g_slice_free1(sizeof(float) * nData, obj);
#else
  g_free(obj);
#endif
}

static gpointer newOrCopyVibe(gconstpointer obj, gpointer data _U_)
{
  float *spinData;

#if GLIB_MINOR_VERSION > 9
  spinData = g_slice_alloc(sizeof(float) * nData);
#else
  spinData = g_malloc(sizeof(float) * nData);
#endif
  if (obj)
    {      
      spinData[Ux] = ((float*)obj)[0];
      spinData[Uy] = ((float*)obj)[1];
      spinData[Uz] = ((float*)obj)[2];
      tool_matrix_cartesianToSpherical(spinData + mod, (float*)obj);
      spinData[Vx] = ((float*)obj)[3];
      spinData[Vy] = ((float*)obj)[4];
      spinData[Vz] = ((float*)obj)[5];
      spinData[dx] = 0.f;
      spinData[dy] = 0.f;
      spinData[dz] = 0.f;
      spinData[qr] = 0.f;
    }
  else
    memset(spinData, 0, sizeof(float) * nData);
    
  return (gpointer)spinData;
}
/**
 * visu_vibration_init:
 * @data: a #VisuData object.
 * @n: number of phonons to set.
 * @nNodes: number of vibrating nodes.
 *
 * Allocate (or reallocate) storage of phonons for @data. The
 * allocated memory will be automatically freed when @data is
 * destroyed.
 *
 * Since: 3.5
 *
 * Returns: TRUE if @data has no phonons before.
 */
gboolean visu_vibration_init(VisuData *data, guint n, guint nNodes)
{
  Vibration *vib;
  gboolean new;
  guint i;

  new = FALSE;
  vib = (Vibration*)g_object_get_data(G_OBJECT(data), VISU_VIBRATION_ID);
  if (!vib)
    {
      new = TRUE;
      vib = g_malloc(sizeof(Vibration));
      g_object_set_data_full(G_OBJECT(data), VISU_VIBRATION_ID, (gpointer)vib, freeVib);
      vib->q     = g_malloc(sizeof(float) * n * 3);
      vib->omega = g_malloc(sizeof(float) * n);
      vib->en    = g_malloc(sizeof(float) * n);
      vib->loaded= g_malloc(sizeof(gboolean) * n);
      vib->nUs   = 6 * nNodes;
      vib->u     = g_malloc(sizeof(float) * n * vib->nUs);
      vib->norm  = g_malloc(sizeof(float) * n);
      g_signal_connect(G_OBJECT(data), "PopulationIncrease",
		       G_CALLBACK(onPopulationChanged), (gpointer)0);
      vib->signal = g_signal_connect(G_OBJECT(data), "PositionChanged",
				     G_CALLBACK(onPositionChanged), (gpointer)0);
    }
  else if (n != vib->n || vib->nUs != 6 * nNodes)
    {
      vib->q     = g_realloc(vib->q,     sizeof(float) * n * 3);
      vib->omega = g_realloc(vib->omega, sizeof(float) * n);
      vib->en    = g_realloc(vib->en,    sizeof(float) * n);
      vib->loaded= g_realloc(vib->loaded,sizeof(gboolean) * n);
      vib->nUs   = 6 * nNodes;
      vib->u     = g_realloc(vib->u,     sizeof(float) * n * vib->nUs);
      vib->norm  = g_realloc(vib->norm,  sizeof(float) * n);
    }
  for (i = 0; i < n; i++)
    vib->loaded[i] = FALSE;

  vib->n    = n;
  vib->iph  = -1;
  vib->t    = 0;
  vib->freq = 5.f;
  vib->ampl = 1.f;
  vib->haveTime  = FALSE;
  vib->havePhase = FALSE;

  DBG_fprintf(stderr, "Vibration: set %d vibration properties for data %p (%d).\n",
	      n, (gpointer)data, nNodes);

  return new;
}
static void freeVib(gpointer data)
{
  Vibration *vib;

  vib = (Vibration*)data;
  g_free(vib->q);
  g_free(vib->omega);
  g_free(vib->en);
  g_free(vib->u);
  g_free(vib->norm);
  g_free(vib->loaded);
}
/**
 * visu_vibration_setDisplacements:
 * @data: a #VisuData object.
 * @iph: a phonon id.
 * @vibes: a set of displacement vectors.
 * @complex: a flag.
 *
 * visu_vibration_init() must have been call before. This routine is
 * used to define a set of displacement vectors, corresponding to one
 * phonon. The displacement vectors can be @complex, in that case
 * @vibes contains 6 values for each #VisuNode.
 *
 * Since: 3.5
 *
 * Returns: TRUE on success.
 */
gboolean visu_vibration_setDisplacements(VisuData *data, guint iph,
                                         float *vibes, gboolean complex)
{
  float *tmpVibeValues, max;
  Vibration *vib;
  int i, nSet;

  g_return_val_if_fail(data && vibes, FALSE);

  vib = (Vibration*)g_object_get_data(G_OBJECT(data), VISU_VIBRATION_ID);
  g_return_val_if_fail(vib && iph < vib->n, FALSE);

  DBG_fprintf(stderr, "Vibration: set vibration data for phonon %d (%d).\n",
	      iph, vib->nUs / 6);

  nSet = (complex)?6:3;

  /* We copy the displacement information into the Vibration
     structure for the given phonon. */
  max = 0.f;
  for (i = 0; i < (int)vib->nUs / 6; i++)
    {
      tmpVibeValues = vib->u + iph * vib->nUs + i * 6;
      tmpVibeValues[0] = vibes[i * nSet + 0];
      tmpVibeValues[1] = vibes[i * nSet + 1];
      tmpVibeValues[2] = vibes[i * nSet + 2];
      tmpVibeValues[3] = (complex)?vibes[i * nSet + 3]:0.f;
      tmpVibeValues[4] = (complex)?vibes[i * nSet + 4]:0.f;
      tmpVibeValues[5] = (complex)?vibes[i * nSet + 5]:0.f;
      max = MAX(tmpVibeValues[0] * tmpVibeValues[0] +
		tmpVibeValues[1] * tmpVibeValues[1] +
		tmpVibeValues[2] * tmpVibeValues[2] +
		tmpVibeValues[3] * tmpVibeValues[3] +
		tmpVibeValues[4] * tmpVibeValues[4] +
		tmpVibeValues[5] * tmpVibeValues[6], max);
      DBG_fprintf(stderr, "| %d -> (%g,%g,%g) + i (%g,%g,%g)\n", i,
		  tmpVibeValues[0], tmpVibeValues[1], tmpVibeValues[2],
		  tmpVibeValues[3], tmpVibeValues[4], tmpVibeValues[5]);
    }
  vib->norm[iph] = sqrt(max);
  DBG_fprintf(stderr, "Vibration: set norm to %f.\n", vib->norm[iph]);

  vib->loaded[iph] = TRUE;

  return TRUE;
}
/**
 * visu_vibration_setCurrentMode:
 * @data: a #VisuData object.
 * @iph: a phonon id.
 * @error: a location for a possible error.
 *
 * Set all node displacements to zero and setup the displacement
 * vector (u+iv) for each #VisuNode. After this call,
 * visu_vibration_play() will move the nodes according to their vibration.
 *
 * Since: 3.5
 *
 * Returns: TRUE if its the first time #VisuNodes are displaced.
 */
gboolean visu_vibration_setCurrentMode(VisuData *data, guint iph, GError **error)
{
  float *tmpVibeValues, xyz[3], red[3], *vals;
  float  *mM;
  VisuNodeArrayIter iter;
  VisuNodeProperty *nodevibe;
  GValue vibeValue = {0.0, {{0.0}, {0.0}}};
  Vibration *vib;
  gboolean new, ok;
  int i, iNode;

  g_return_val_if_fail(data, FALSE);

  DBG_fprintf(stderr, "Vibration: set current mode to %d.\n", iph);

  vib = (Vibration*)g_object_get_data(G_OBJECT(data), VISU_VIBRATION_ID);
  g_return_val_if_fail(vib && iph < vib->n, FALSE);

  if (!vib->loaded[iph])
    {
      visu_data_freePopulation(data);
      ok = visu_object_load(VISU_OBJECT_INSTANCE, data,
                            iph, (GCancellable*)0, error);
      if (!ok)
	return FALSE;
    }
  g_return_val_if_fail(vib->loaded[iph], FALSE);

  /* We copy for all the nodes the displacements into the VISU_VIBRATION_ID
     node property. */
  nodevibe = visu_node_array_getProperty(VISU_NODE_ARRAY(data), VISU_VIBRATION_ID);
  new = (nodevibe == (VisuNodeProperty*)0);

  /*Associate the table to a VisuNodeArray as a new property*/
  if (new)
    nodevibe = visu_node_array_property_newPointer(VISU_NODE_ARRAY(data),
                                             VISU_VIBRATION_ID, freeVibe, newOrCopyVibe,
                                             (gpointer)0);
  /*for each node*/
  i   = 0;
  g_value_init(&vibeValue, G_TYPE_POINTER);
  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  for(visu_node_array_iterStart(VISU_NODE_ARRAY(data), &iter); iter.node;
      visu_node_array_iterNext(VISU_NODE_ARRAY(data), &iter))
    {
      iNode = visu_node_array_getOriginal(VISU_NODE_ARRAY(data), iter.node->number);
      iNode = (iNode >= 0)?iNode:(int)iter.node->number;
      vals = vib->u + iph * vib->nUs + iNode * 6;
      if (new)
        {
          tmpVibeValues = newOrCopyVibe(vals, (gpointer)0);
          g_value_set_pointer(&vibeValue, tmpVibeValues);
          visu_node_property_setValue(nodevibe, iter.node, &vibeValue);
        }
      else
        {
          visu_node_property_getValue(nodevibe, iter.node, &vibeValue);
          tmpVibeValues = (float*)g_value_get_pointer(&vibeValue);
          tmpVibeValues[Ux] = vals[0];
          tmpVibeValues[Uy] = vals[1];
          tmpVibeValues[Uz] = vals[2];
          tool_matrix_cartesianToSpherical(tmpVibeValues + mod, vals);
          tmpVibeValues[Vx] = vals[3];
          tmpVibeValues[Vy] = vals[4];
          tmpVibeValues[Vz] = vals[5];
          tmpVibeValues[dx] = 0.f;
          tmpVibeValues[dy] = 0.f;
          tmpVibeValues[dz] = 0.f;
        }
      /* Set the phase. */
      visu_data_getNodePosition(data, iter.node, xyz);
      visu_box_convertXYZtoBoxCoordinates(visu_boxed_getBox(VISU_BOXED(data)), red, xyz);
      tmpVibeValues[qr] = 2.f * G_PI *
        (red[0] * vib->q[3 * iph + 0] +
         red[1] * vib->q[3 * iph + 1] +
         red[2] * vib->q[3 * iph + 2]);
      DBG_fprintf(stderr, "| %d -> (%g,%g,%g) + i (%g,%g,%g) | (%g,%g,%g) %g\n", i,
        	  tmpVibeValues[Ux], tmpVibeValues[Uy], tmpVibeValues[Uz],
        	  tmpVibeValues[Vx], tmpVibeValues[Vy], tmpVibeValues[Vz],
        	  tmpVibeValues[dx], tmpVibeValues[dy], tmpVibeValues[dz],
        	  tmpVibeValues[qr]);

      i += 1;
    }

  /* Set the norm of the phonon. */
  mM = (float*)g_object_get_data(G_OBJECT(data), "max_" VISU_VIBRATION_ID);
  if (!mM)
    {
      mM = g_malloc(sizeof(float) * 2);
      g_object_set_data_full(G_OBJECT(data), "max_" VISU_VIBRATION_ID, mM, g_free);
    }
  mM[1] = vib->norm[iph];

  vib->iph = iph;

  return new;
}
/**
 * visu_vibration_setCharacteristic:
 * @data: a #VisuData object.
 * @n: a phonon id.
 * @q: a reciprocal vector.
 * @en: the phonon energy.
 * @omega: the phonon frequency.
 *
 * This routine is used to define the characteristics of a given
 * phonon.
 *
 * Since: 3.5
 *
 * Returns: TRUE on success.
 */
gboolean visu_vibration_setCharacteristic(VisuData *data, guint n,
                                          float q[3], float en, float omega)
{
  Vibration *vib;

  g_return_val_if_fail(data, FALSE);

  vib = (Vibration*)g_object_get_data(G_OBJECT(data), VISU_VIBRATION_ID);
  g_return_val_if_fail(vib || vib->n <= n, FALSE);

  vib->q[n * 3 + 0] = q[0];
  vib->q[n * 3 + 1] = q[1];
  vib->q[n * 3 + 2] = q[2];
  vib->omega[n]     = omega;
  vib->en[n]        = en;

  return TRUE;
}
/**
 * visu_vibration_getCharacteristic:
 * @data: a #VisuData object.
 * @n: a phonon id.
 * @q: a location for the reciprocal vector.
 * @en: a locattion for the phonon energy.
 * @omega: a location for the phonon frequency.
 *
 * This routine is used to get the characteristics of a given
 * phonon, see visu_vibration_setCharacteristic() to set them.
 *
 * Since: 3.5
 *
 * Returns: TRUE on success.
 */
gboolean visu_vibration_getCharacteristic(VisuData *data, guint n,
                                          float q[3], float *en, float *omega)
{
  Vibration *vib;

  g_return_val_if_fail(data && en && omega, FALSE);

  vib = (Vibration*)g_object_get_data(G_OBJECT(data), VISU_VIBRATION_ID);
  g_return_val_if_fail(vib || vib->n <= n, FALSE);

  q[0]   = vib->q[n * 3 + 0];
  q[1]   = vib->q[n * 3 + 1];
  q[2]   = vib->q[n * 3 + 2];
  *omega = vib->omega[n];
  *en    = vib->en[n];

  return TRUE;
}
/**
 * visu_vibration_getNPhonons:
 * @data: a #VisuData object.
 * @n: a location.
 *
 * Retrieves if @data contains phonons or not. Number of stored
 * phonons is set in @n.
 *
 * Since: 3.5
 *
 * Returns: TRUE if @data has phonons.
 */
gboolean visu_vibration_getNPhonons(VisuData *data, guint *n)
{
  Vibration *vib;

  g_return_val_if_fail(data && n, FALSE);

  vib = (Vibration*)g_object_get_data(G_OBJECT(data), VISU_VIBRATION_ID);
  g_return_val_if_fail(vib, FALSE);

  *n = vib->n;

  return TRUE;
}
/**
 * visu_vibration_setUserFrequency:
 * @data: a #VisuData object.
 * @freq: a frequency.
 *
 * Change the frequency at which phonons are played with visu_vibration_play().
 *
 * Since: 3.5
 */
void visu_vibration_setUserFrequency(VisuData *data, float freq)
{
  Vibration *vib;
  float f;

  g_return_if_fail(data);

  vib = (Vibration*)g_object_get_data(G_OBJECT(data), VISU_VIBRATION_ID);
  g_return_if_fail(vib);

  f = vib->freq;
  if (freq == 0.f)
    vib->freq = vib->omega[vib->iph];
  else
    vib->freq = freq;
  /* We update t to keep the same position. */
  vib->t = (gulong)((float)vib->t * f / vib->freq);
}
/**
 * visu_vibration_setAmplitude:
 * @data: a #VisuData object.
 * @ampl: an amplitude.
 *
 * Change the amplitude at which phonon are displayed on screen when
 * using visu_vibration_play().
 * 
 * Since: 3.5
 *
 * Returns: TRUE if amplitude is actually changed.
 */
gboolean visu_vibration_setAmplitude(VisuData *data, float ampl)
{
  Vibration *vib;

  g_return_val_if_fail(data, FALSE);

  vib = (Vibration*)g_object_get_data(G_OBJECT(data), VISU_VIBRATION_ID);
  g_return_val_if_fail(vib, FALSE);

  if (vib->ampl == ampl)
    return FALSE;

  vib->ampl = ampl;
  return TRUE;
}
/**
 * visu_vibration_getCurrentMode:
 * @data: a #VisuData object.
 *
 * Retrieves the phonon that is currently applied to @data.
 *
 * Since: 3.5
 *
 * Returns: a phonon id.
 */
guint visu_vibration_getCurrentMode(VisuData *data)
{
  Vibration *vib;

  g_return_val_if_fail(data, 0);

  vib = (Vibration*)g_object_get_data(G_OBJECT(data), VISU_VIBRATION_ID);
  g_return_val_if_fail(vib, 0);

  return vib->iph;
}

static gboolean getVibration(VisuNodeProperty **nodevibe, Vibration **vib,
			     GValue *vibeValue, VisuData *data)
{
  *vib = (Vibration*)g_object_get_data(G_OBJECT(data), VISU_VIBRATION_ID);
  g_return_val_if_fail(*vib, FALSE);

  g_value_init(vibeValue, G_TYPE_POINTER);
  *nodevibe = visu_node_array_getProperty(VISU_NODE_ARRAY(data), VISU_VIBRATION_ID);

  return TRUE;
}

static void vibrationIncr(VisuData *data, gboolean time, gboolean phase)
{
  GValue vibeValue = {0.0, {{0.0}, {0.0}}};
  float *dxyz, t, c, wdtr, wdti, dU[3];
  VisuNodeArrayIter iter;
  VisuNodeProperty *nodevibe;
  Vibration *vib;
  gboolean set;

  set = getVibration(&nodevibe, &vib, &vibeValue, data);
  g_return_if_fail(set);

  if (vib->iph < 0)
    return;

  DBG_fprintf(stderr, "Vibration: incr %d %d\n", time, phase);
  DBG_fprintf(stderr, " | t = %ld, w = %g.\n", vib->t, vib->freq);

  /* update the timer every time the method is call */
  if (time)
    vib->t += 1;
  else
    vib->t = 0;

  c =  AMPL_COEFF  * vib->ampl / vib->norm[vib->iph];
  t = -OMEGA_COEFF * vib->freq * vib->t;

  DBG_fprintf(stderr, " | c = %g, t = %g.\n", c, t);

  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  for(visu_node_array_iterStart(VISU_NODE_ARRAY(data), &iter); iter.node;
      visu_node_array_iterNext(VISU_NODE_ARRAY(data), &iter))
    {
      /* Get the vector of vibration of the node (GValue) */
      visu_node_property_getValue(nodevibe, iter.node, &vibeValue );
      /* Convert GValue to Float[3]  */
      dxyz = (float*)g_value_get_pointer(&vibeValue);

      /* calculate xyz(t) */
      if (dxyz)
        {
          wdtr = c * sin(((time)?t:0.f) + ((phase)?dxyz[qr]:0.f));
          wdti = -c * cos(((time)?t:0.f) + ((phase)?dxyz[qr]:G_PI_2));
          dU[0] = dxyz[Ux] * wdtr + dxyz[Vx] * wdti;
          dU[1] = dxyz[Uy] * wdtr + dxyz[Vy] * wdti;
          dU[2] = dxyz[Uz] * wdtr + dxyz[Vz] * wdti;
          iter.node->translation[0] += dU[0] - dxyz[dx];
          iter.node->translation[1] += dU[1] - dxyz[dy];
          iter.node->translation[2] += dU[2] - dxyz[dz];
          dxyz[dx] = dU[0];
          dxyz[dy] = dU[1];
          dxyz[dz] = dU[2];

          DBG_fprintf(stderr, " | dx = %f, dy = %f, dz = %f\n",
                      dU[0], dU[1], dU[2]);
        }
    }
  vib->haveTime  = time;
  vib->havePhase = phase;

  /*emit signal for pairs and other things */   
  g_signal_handler_block(G_OBJECT(data), vib->signal);
  g_signal_emit_by_name(G_OBJECT(data), "PositionChanged", (VisuElement*)0, NULL);
  g_signal_handler_unblock(G_OBJECT(data), vib->signal);
  /* redraw with the new values */
  VISU_REDRAW_ADD;
}
static gboolean move(gpointer data)
{
  vibrationIncr(VISU_DATA(data), TRUE, TRUE);
  return TRUE;
}
/**
 * visu_vibration_resetPosition:
 * @data: a #VisuData object.
 *
 * Reset the node position of the given VisuData.
 *
 * Since: 3.5
 *
 * Returns: a boolean
 */
void visu_vibration_resetPosition(VisuData *data)
{
  vibrationIncr(data, FALSE, FALSE);
}
/**
 * visu_vibration_setZeroTime:
 * @data: a #VisuData object.
 *
 * Reset the position of phonons to use position at time equals zero
 * (so applying just the q vector displacement).
 *
 * Since: 3.5
 */
void visu_vibration_setZeroTime(VisuData *data)
{
  vibrationIncr(data, FALSE, TRUE);
}
/**
 * visu_vibration_isSet:
 * @data: a #VisuData object.
 *
 * Retrieve if @data has some phonon attached to.
 *
 * Since: 3.5
 *
 * Returns: TRUE if visu_vibration_init() has been called already.
 */
gboolean visu_vibration_isSet(VisuData *data)
{
  return (g_object_get_data(G_OBJECT(data), VISU_VIBRATION_ID) != (Vibration*)0);
}
static void onPopulationChanged(VisuData *dataObj, gint *ids, gpointer data _U_)
{
  int i;
  VisuNode *node;
  VisuNodeProperty *nodevibe;
  GValue vibeValue = {0.0, {{0.0}, {0.0}}};
  float *dxyz ,xyz[3], red[3];
  Vibration *vib;

  vib = (Vibration*)g_object_get_data(G_OBJECT(dataObj), VISU_VIBRATION_ID);
  g_return_if_fail(vib);

  if (vib->iph < 0)
    return;

  g_value_init(&vibeValue, G_TYPE_POINTER);
  nodevibe = visu_node_array_getProperty(VISU_NODE_ARRAY(dataObj), VISU_VIBRATION_ID);

  DBG_fprintf(stderr, "Vibration: on population increase recalculate pahse.\n");
  /* We need to recalculate the phase of the new ones. */
  for (i = 2; ids[i] >= 0; i++)
    {
      node = visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), ids[i]);
      /* Get the vector of vibration of the node (GValue) */
      visu_node_property_getValue(nodevibe, node, &vibeValue );
      /* Convert GValue to Float[3]  */
      dxyz = (float*)g_value_get_pointer(&vibeValue);
      
      visu_data_getNodePosition(dataObj, node, xyz);
      visu_box_convertXYZtoBoxCoordinates(visu_boxed_getBox(VISU_BOXED(dataObj)), red, xyz);
      DBG_fprintf(stderr, " | node %d, %f -> %f.\n", ids[i], dxyz[3],
		  red[0] * vib->q[3 * vib->iph + 0] +
		  red[1] * vib->q[3 * vib->iph + 1] +
		  red[2] * vib->q[3 * vib->iph + 2]);
      dxyz[qr] = 2.f * G_PI *
	(red[0] * vib->q[3 * vib->iph + 0] +
	 red[1] * vib->q[3 * vib->iph + 1] +
	 red[2] * vib->q[3 * vib->iph + 2]);
    }
}
static void onPositionChanged(VisuData *dataObj, VisuElement *ele _U_, gpointer data _U_)
{
  VisuNodeArrayIter iter;
  VisuNodeProperty *nodevibe;
  GValue vibeValue = {0.0, {{0.0}, {0.0}}};
  Vibration *vib;
  gboolean set;
  float *dxyz ,xyz[3], red[3];

  DBG_fprintf(stderr, "Vibration: on position changed recalculate phase.\n");

  set = getVibration(&nodevibe, &vib, &vibeValue, dataObj);
  g_return_if_fail(set);

  if (vib->iph < 0)
    return;

  /* We need to recalculate the phase of the new ones. */
  visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter);
  for(visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter); iter.node;
      visu_node_array_iterNext(VISU_NODE_ARRAY(dataObj), &iter))
    {
      /* Get the vector of vibration of the node (GValue) */
      visu_node_property_getValue(nodevibe, iter.node, &vibeValue );
      /* Convert GValue to Float[3]  */
      dxyz = (float*)g_value_get_pointer(&vibeValue);
      
      visu_data_getNodePosition(dataObj, iter.node, xyz);
      visu_box_convertXYZtoBoxCoordinates(visu_boxed_getBox(VISU_BOXED(dataObj)), red, xyz);
      DBG_fprintf(stderr, " | node %d, %f -> %f.\n", iter.node->number, dxyz[qr],
		  red[0] * vib->q[3 * vib->iph + 0] +
		  red[1] * vib->q[3 * vib->iph + 1] +
		  red[2] * vib->q[3 * vib->iph + 2]);
      dxyz[qr] = 2.f * G_PI *
	(red[0] * vib->q[3 * vib->iph + 0] +
	 red[1] * vib->q[3 * vib->iph + 1] +
	 red[2] * vib->q[3 * vib->iph + 2]);
    }
}
