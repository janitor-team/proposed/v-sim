/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "plane.h"

#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <visu_object.h>
#include <coreTools/toolMatrix.h>

/**
 * SECTION:plane
 * @short_description: Adds capabilities to draw and handle planes.
 *
 * <para>A #VisuPlane is a GObject. It is defined by its normal vector and
 * the distance of the plane with the origin (see
 * visu_plane_setNormalVector() and visu_plane_setDistanceFromOrigin()). When
 * these informations are given and an #VisuGlView is used to render
 * the plane, V_Sim computes the intersections of the plane with the
 * bounding box (see visu_plane_getIntersection()).</para>
 * <para>VisuPlanes can be used to hide nodes defining their
 * visu_plane_setHiddenState() and visu_plane_class_setHiddingMode(). A list of planes
 * can also be exported or imported from an XML file using
 * visu_plane_class_exportXMLFile() and visu_plane_class_parseXMLFile().</para>
 * <para>VisuPlanes can have transparency but the support of it is limited
 * to one plane. If several planes are drawn with transparency, they
 * may hide each other because of the implementation of transparency
 * in OpenGL (planes are treated as single polygons).</para>
 */

static VisuPlaneHidingMode plane_hidingMode = VISU_PLANE_HIDE_UNION;

struct _VisuPlane
{
  /* Internal object gestion. */
  GObject parent;
  gboolean dispose_has_run;

  /* Normal vector, unitary. */
  float nVect[3];
  /* Normal vector, user given. */
  float nVectUser[3];

  /* Distance between origin and intersection of the plane
     and the line made by origin and normal vector. */
  float dist;

  /* ToolColor of that plane. */
  ToolColor *color;

  /* Internal variables */
  VisuBox *box;
  gulong size_signal;

  /* Intersections with the bounding box.
     Consist of a GList of float[3], can be NULL if
     there is no interstection. */
  GList *inter;
  /* Isobarycenter G of all intersection points, required to order
     these points to form a convex polygon. */
  float pointG[3];
  /* The plane can hide the nodes on one of its side.
     This variable can be VISU_PLANE_SIDE_PLUS or VISU_PLANE_SIDE_MINUS or
     VISU_PLANE_SIDE_NONE. It codes the side of the plane which hides the nodes.
     If VISU_PLANE_SIDE_NONE is selected all nodes are rendered. */
  int hiddenSide;
  /* Store if the plane is rendered or not.
     Default is TRUE. */
  gboolean rendered;
};

enum
  {
    VISU_PLANE_MOVED_SIGNAL,
    VISU_PLANE_RENDERING_SIGNAL,
    VISU_PLANE_NB_SIGNAL
  };

struct _VisuPlaneClass
{
  GObjectClass parent;
};

/* Internal variables. */
static guint plane_signals[VISU_PLANE_NB_SIGNAL] = { 0 };

/* Object gestion methods. */
static void visu_plane_dispose   (GObject* obj);
static void visu_plane_finalize  (GObject* obj);
static void visu_boxed_interface_init(VisuBoxedInterface *iface);

/* Local methods. */
static VisuBox* visu_plane_getBox(VisuBoxed *self);
static gboolean visu_plane_setBox(VisuBoxed *self, VisuBox *box, gboolean update);
static void onBoxSizeChanged(VisuBox *box, float extens, gpointer data);
static int comparePolygonPoint(gconstpointer pointA, gconstpointer pointB, gpointer data);
static void computeInter(VisuPlane* plane);

G_DEFINE_TYPE_WITH_CODE(VisuPlane, visu_plane, G_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE(VISU_TYPE_BOXED,
                                              visu_boxed_interface_init))

static void visu_plane_class_init(VisuPlaneClass *klass)
{
  DBG_fprintf(stderr, "VisuPlane : creating the class of the object.\n");

  DBG_fprintf(stderr, "                - adding new signals ;\n");
  /**
   * VisuPlane::moved:
   * @plane: the object emitting the signal.
   *
   * This signal is emitted each time the plane position is changed
   * (either distance or normal).
   *
   * Since: 3.3
   */
  plane_signals[VISU_PLANE_MOVED_SIGNAL] =
    g_signal_newv("moved", G_TYPE_FROM_CLASS(klass),
		  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		  NULL, NULL, NULL, g_cclosure_marshal_VOID__VOID,
		  G_TYPE_NONE, 0, NULL);
  /**
   * VisuPlane::rendering:
   * @plane: the object emitting the signal.
   *
   * This signal is emitted each time the rendering properties (color,
   * visibility...) are affected.
   *
   * Since: 3.7
   */
  plane_signals[VISU_PLANE_RENDERING_SIGNAL] =
    g_signal_newv("rendering", G_TYPE_FROM_CLASS(klass),
		  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		  NULL, NULL, NULL, g_cclosure_marshal_VOID__VOID,
		  G_TYPE_NONE, 0, NULL);

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_plane_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_plane_finalize;

  
}
static void visu_boxed_interface_init(VisuBoxedInterface *iface)
{
  iface->get_box = visu_plane_getBox;
  iface->set_box = visu_plane_setBox;
}

static void visu_plane_init(VisuPlane *obj)
{
  obj->dispose_has_run = FALSE;

  DBG_fprintf(stderr, "VisuPlane : creating a new plane (%p).\n", (gpointer)obj);
  obj->inter = (GList*)0;
  obj->hiddenSide = VISU_PLANE_SIDE_NONE;
  obj->nVectUser[0] = 0.;
  obj->nVectUser[1] = 0.;
  obj->nVectUser[2] = 0.;
  obj->dist = 0.;
  obj->color = (ToolColor*)0;
  obj->box = (VisuBox*)0;
  obj->size_signal = 0;
  obj->rendered = TRUE;
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_plane_dispose(GObject* obj)
{
  DBG_fprintf(stderr, "VisuPlane : dispose object %p.\n", (gpointer)obj);

  if (VISU_PLANE(obj)->dispose_has_run)
    return;

  visu_plane_setBox(VISU_BOXED(obj), (VisuBox*)0, FALSE);

  VISU_PLANE(obj)->dispose_has_run = TRUE;
  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_plane_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_plane_finalize(GObject* obj)
{
  GList *tmpLst;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "VisuPlane : finalize object %p.\n", (gpointer)obj);

  /* Deleting the intersection nodes if any. */
  tmpLst = VISU_PLANE(obj)->inter;
  while (tmpLst)
    {
      g_free(tmpLst->data);
      tmpLst = g_list_next(tmpLst);
    }

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_plane_parent_class)->finalize(obj);
}
/**
 * visu_plane_newUndefined:
 *
 * Create a new plane with default values. This plane can't be
 * rendered directly and one needs to computes its intersection with
 * the bounding box before using planeComputeInter().
 *
 * Returns: (transfer full): a newly allocated #VisuPlane structure.
 */
VisuPlane* visu_plane_newUndefined(void)
{
  VisuPlane *plane;

  /* Create the object with default values. */
  plane = VISU_PLANE(g_object_new(VISU_TYPE_PLANE, NULL));
  g_return_val_if_fail(plane, (VisuPlane*)0);
  return plane;
}

/**
 * visu_plane_new:
 * @box: (transfer full): a box description ;
 * @vect: (array fixed-size=3): three values defining the normal
 * vector (unitary or not) ;
 * @dist: the distance between origin and intersection of the plane and the
 * line made by origin and normal vector ;
 * @color: a #ToolColor.
 *
 * Create a plane with the specified attributes.
 *
 * Returns: a newly allocated #VisuPlane structure.
 */
VisuPlane* visu_plane_new(VisuBox *box, float vect[3], float dist, ToolColor *color)
{
  VisuPlane *plane;

  g_return_val_if_fail(color, (VisuPlane*)0);

  /* Create the object with defaulty values. */
  plane = VISU_PLANE(g_object_new(VISU_TYPE_PLANE, NULL));
  g_return_val_if_fail(plane, (VisuPlane*)0);

  visu_plane_setNormalVector(plane, vect);
  visu_plane_setDistanceFromOrigin(plane, dist);
  visu_plane_setBox(VISU_BOXED(plane), box, TRUE);

  visu_plane_setColor(plane, color);
  
  return plane;
}

gboolean visu_plane_setNormalVector(VisuPlane *plane, float vect[3])
{
  int i;
  float norm;

  g_return_val_if_fail(VISU_IS_PLANE_TYPE(plane), FALSE);

  if (vect[0] == plane->nVectUser[0] &&
      vect[1] == plane->nVectUser[1] &&
      vect[2] == plane->nVectUser[2])
    return FALSE;
  g_return_val_if_fail(vect[0] * vect[0] +
		       vect[1] * vect[1] +
		       vect[2] * vect[2] != 0., FALSE);

  norm = 0.;
  for (i = 0; i < 3; i++)
    {
      norm += vect[i] * vect[i];
      plane->nVect[i] = vect[i];
      plane->nVectUser[i] = vect[i];
    }
  norm = sqrt(norm);
  for (i = 0; i < 3; i++)
    plane->nVect[i] /= norm;

  DBG_fprintf(stderr, "Visu VisuPlane : set normal vector (%f,%f,%f) for plane %p.\n",
	      plane->nVect[0], plane->nVect[1], plane->nVect[2], (gpointer)plane);
  DBG_fprintf(stderr, "Visu VisuPlane : set user vector (%f,%f,%f) for plane %p.\n",
	      plane->nVectUser[0], plane->nVectUser[1], plane->nVectUser[2],
	      (gpointer)plane);

  if (!plane->box)
    return TRUE;

  /* We recompute the intersection. */
  computeInter(plane);
  return TRUE;
}

gboolean visu_plane_setDistanceFromOrigin(VisuPlane *plane, float dist)
{
  g_return_val_if_fail(VISU_IS_PLANE_TYPE(plane), FALSE);

  if (plane->dist == dist)
    return FALSE;

  plane->dist = dist;
  DBG_fprintf(stderr, "Visu VisuPlane : set distance from origin %f for plane %p.\n",
	      dist, (gpointer)plane);

  if (!plane->box)
    return TRUE;

  /* We recompute the intersection. */
  computeInter(plane);
  return TRUE;
}

static gboolean visu_plane_setBox(VisuBoxed *self, VisuBox *box, gboolean update _U_)
{
  VisuPlane *plane;

  g_return_val_if_fail(VISU_IS_PLANE_TYPE(self), FALSE);
  plane = VISU_PLANE(self);

  if (plane->box == box)
    return FALSE;

  if (plane->box)
    {
      g_signal_handler_disconnect(G_OBJECT(plane->box), plane->size_signal);
      g_object_unref(plane->box);
    }
  plane->box = box;
  if (!box)
    return TRUE;

  g_object_ref(box);
  plane->size_signal = g_signal_connect(G_OBJECT(box), "SizeChanged",
                                        G_CALLBACK(onBoxSizeChanged), (gpointer)plane);
  computeInter(plane);

  return TRUE;
}
static VisuBox* visu_plane_getBox(VisuBoxed *self)
{
  g_return_val_if_fail(VISU_IS_PLANE_TYPE(self), (VisuBox*)0);

  return VISU_PLANE(self)->box;
}

gboolean visu_plane_setColor(VisuPlane *plane, ToolColor *color)
{
  g_return_val_if_fail(VISU_IS_PLANE_TYPE(plane), FALSE);

  if (color == plane->color)
    return FALSE;

  plane->color = color;

  g_signal_emit(G_OBJECT(plane), plane_signals[VISU_PLANE_RENDERING_SIGNAL], 0, NULL);
  return TRUE;
}
/**
 * visu_plane_setRendered:
 * @plane: a #VisuPlane ;
 * @rendered: TRUE to make the plane drawable.
 *
 * Change the visibility of the plane.
 *
 * Returns:  TRUE if visu_plane_class_showHideAll() should be called.
 */
gboolean visu_plane_setRendered(VisuPlane *plane, gboolean rendered)
{
  g_return_val_if_fail(VISU_IS_PLANE_TYPE(plane), FALSE);

  if (rendered == plane->rendered)
    return FALSE;

  plane->rendered = rendered;

  g_signal_emit(G_OBJECT(plane), plane_signals[VISU_PLANE_RENDERING_SIGNAL], 0, NULL);
  return TRUE;
}
/**
 * visu_plane_getRendered:
 * @plane: a #VisuPlane.
 *
 * Get the visibility of a plane.
 *
 * Returns: TRUE if the plane is visible.
 */
gboolean visu_plane_getRendered(VisuPlane *plane)
{
  g_return_val_if_fail(VISU_IS_PLANE_TYPE(plane), FALSE);

  return plane->rendered;
}
/**
 * visu_plane_getIntersection:
 * @plane: a #VisuPlane.
 *
 * The list of intersection between the plane and the box is made of
 * float[3]. The planeComputeInter() should have been called before.
 *
 * Returns: (transfer none) (element-type ToolVector): a list of float[3]
 * elements. This list is owned by V_Sim.
 */
GList* visu_plane_getIntersection(VisuPlane *plane)
{
  g_return_val_if_fail(VISU_IS_PLANE_TYPE(plane) && plane->box, (GList*)0);

  return plane->inter;
}
/**
 * visu_plane_getBasis:
 * @plane: a #VisuPlane ;
 * @xyz: two vectors.
 * @center: a point in cartesian coordinates.
 *
 * Stores the coordinates of barycentre of the plane in @center and
 * provide coordinates of two orthogonal vector in the plane. The planeComputeInter()
 * should have been called before.
 */
void visu_plane_getBasis(VisuPlane *plane, float xyz[2][3], float center[3])
{
  float spherical[3];

  g_return_if_fail(VISU_IS_PLANE_TYPE(plane));

  tool_matrix_cartesianToSpherical(spherical, plane->nVectUser);
  xyz[0][0] = cos(spherical[1] * TOOL_PI180) * cos(spherical[2] * TOOL_PI180);
  xyz[0][1] = cos(spherical[1] * TOOL_PI180) * sin(spherical[2] * TOOL_PI180);
  xyz[0][2] = -sin(spherical[1] * TOOL_PI180);
  xyz[1][0] = -sin(spherical[2] * TOOL_PI180);
  xyz[1][1] =  cos(spherical[2] * TOOL_PI180);
  xyz[1][2] = 0.;
  center[0] = plane->pointG[0];
  center[1] = plane->pointG[1];
  center[2] = plane->pointG[2];
}
/**
 * visu_plane_getReducedIntersection:
 * @plane: a #VisuPlane object.
 * @nVals: a location for an integer.
 *
 * This routine returns the coordinates in the @plane basis set of its
 * intersections with a box (see visu_plane_setBox()). The coordinates are
 * appended in the return array which length is stored in @nVals.
 *
 * Since: 3.6
 *
 * Returns: a newly allocated array of @nVals * 2 values. Free it with
 * g_free().
 */
float* visu_plane_getReducedIntersection(VisuPlane *plane, guint *nVals)
{
  float *out, basis[2][3], center[3], *xyz;
  GList *tmpLst;
  gint i;

  g_return_val_if_fail(VISU_IS_PLANE_TYPE(plane) && plane->box, (float*)0);
  g_return_val_if_fail(nVals, (float*)0);

  if (!plane->inter)
    return (float*)0;

  visu_plane_getBasis(plane, basis, center);
  
  i = 0;
  out = g_malloc(sizeof(float) * g_list_length(plane->inter) * 2);
  for (tmpLst = plane->inter; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      xyz = (float*)tmpLst->data;
      out[i * 2 + 0] =
	basis[0][0] * (xyz[0] - center[0]) +
	basis[0][1] * (xyz[1] - center[1]) +
	basis[0][2] * (xyz[2] - center[2]);
      out[i * 2 + 1] =
	basis[1][0] * (xyz[0] - center[0]) +
	basis[1][1] * (xyz[1] - center[1]) +
	basis[1][2] * (xyz[2] - center[2]);
      i += 1;
    }
  *nVals = i;

  return out;
}

/**
 * visu_plane_getLineIntersection:
 * @plane: a #VisuPlane object.
 * @A: (array fixed-size=3): coordinates of point A.
 * @B: (array fixed-size=3): coordinates of point B.
 * @lambda: (out caller-allocates) (allow-none): a location to store
 * the intersecting factor.
 *
 * If there is an intersection M between line (AB) and @plane, then this
 * function calculates M coordinates as M = A + lambda * AB.
 *
 * Since: 3.6
 *
 * Returns: TRUE if there is an intersection between line (AB) and the plane.
 **/
gboolean visu_plane_getLineIntersection(const VisuPlane *plane, const float A[3],
                                        const float B[3], float *lambda)
{
  float denom, lambda_;

  g_return_val_if_fail(VISU_IS_PLANE_TYPE(plane), FALSE);

  /*
    The plane is defined by n1x+n2y+n3z-d=0 with (n1,n2,n3) the normal vector (unitary)
    and d the algebric distance from the origin.
    A segment {P} of the box is defined by P=A+lambda.l with A a vertex, l a vector
    in the direction of the segment and lambda a real.
    If it exists a lambda that can solve P(lambda) in the plane equation, and this lambda
    is in [0;1] then this is the intersection.
   */
  lambda_  = plane->dist;
  lambda_ -= plane->nVect[0] * A[0];
  lambda_ -= plane->nVect[1] * A[1];
  lambda_ -= plane->nVect[2] * A[2];

  denom  = plane->nVect[0] * (B[0] - A[0]);
  denom += plane->nVect[1] * (B[1] - A[1]);
  denom += plane->nVect[2] * (B[2] - A[2]);
  if (denom == 0.)
    {
      /* if A is in the plane, we put lambda to 0. */
      if (lambda_ == 0.f)
        denom = 1.f;
      else
        return FALSE;
    }

  lambda_ /= denom;
  if (lambda)
    *lambda = lambda_;

  return TRUE;
}

static void computeInter(VisuPlane* plane)
{
  float lambda;
  int a[12] = {0, 1, 2, 3, 0, 1, 2, 3, 4, 5, 6, 7};
  int b[12] = {1, 2, 3, 0, 4, 5, 6, 7, 5, 6, 7, 4};
  int i, j, n;
  float *inter;
  GList *tmpLst;
  float vertices[8][3];

  g_return_if_fail(VISU_IS_PLANE_TYPE(plane) && plane->box);

  /* RAZ old intersection list. */
  if (plane->inter)
    {
      tmpLst = plane->inter;
      while(tmpLst)
	{
	  g_free((float*)tmpLst->data);
	  tmpLst = g_list_next(tmpLst);
	}
      g_list_free(plane->inter);
      plane->inter = (GList*)0;
    }
  /* Compute, vector and position for the box. */
  n = 0;
  plane->pointG[0] = 0.;
  plane->pointG[1] = 0.;
  plane->pointG[2] = 0.;
  visu_box_getVertices(plane->box, vertices, TRUE);
  for (i = 0; i < 12; i++)
    if (visu_plane_getLineIntersection(plane, vertices[a[i]], vertices[b[i]], &lambda) &&
        lambda >= 0. && lambda <= 1.)
      {
        inter = g_malloc(sizeof(float) * 3);
        for (j = 0; j < 3; j++)
          {
            inter[j] = vertices[a[i]][j] + lambda * (vertices[b[i]][j] -
                                                     vertices[a[i]][j]);
            plane->pointG[j] += inter[j];
          }
        n += 1;
        plane->inter = g_list_append(plane->inter, (gpointer)inter);
        DBG_fprintf(stderr, "Visu Plane: a new intersection (%f,%f,%f) for plane %p.\n",
                    inter[0], inter[1], inter[2], (gpointer)plane);
      }
  if (n > 0)
    {
      for (i = 0; i < 3; i++)
	plane->pointG[i] /= (float)n;
      plane->inter = g_list_sort_with_data(plane->inter, comparePolygonPoint, (gpointer)plane);
    }

  g_signal_emit(G_OBJECT(plane), plane_signals[VISU_PLANE_MOVED_SIGNAL], 0, NULL);
}

/**
 * visu_plane_getPlaneIntersection:
 * @plane1: a #VisuPlane object.
 * @plane2: another #VisuPlane object.
 * @A: (out) (array fixed-size=3): the coordinates of the first point
 * of intersection.
 * @B: (out) (array fixed-size=3): the coordinates of the second point
 * of intersection.
 *
 * Calculates the intersection between @plane1 and @plane2, if it
 * exists. The intersection is returned in @A and @B as the
 * coordinates of the two points on the border of @plane1 that
 * intersect @plane2. 
 *
 * Since: 3.7
 *
 * Returns: TRUE if there is an intersection between @plane1 and @plane2.
 **/
gboolean visu_plane_getPlaneIntersection(const VisuPlane *plane1, const VisuPlane *plane2,
                                         float A[3], float B[3])
{
  float lambda, *I, *J;
  GList *inter;
  guint n;
  float M[6][3];
  
  n = 0;
  for (inter = plane1->inter; inter; inter = g_list_next(inter))
    {
      I = (float*)inter->data;
      J = (inter->next)?(float*)inter->next->data:(float*)plane1->inter->data;
      if (visu_plane_getLineIntersection(plane2, I, J, &lambda) &&
          lambda >= 0.f && lambda <= 1.f)
        {
          M[n][0] = I[0] + lambda * (J[0] - I[0]);
          M[n][1] = I[1] + lambda * (J[1] - I[1]);
          M[n][2] = I[2] + lambda * (J[2] - I[2]);
          n += 1;
        }
    }
  if (n != 2)
    return FALSE;

  A[0] = M[0][0];
  A[1] = M[0][1];
  A[2] = M[0][2];

  B[0] = M[1][0];
  B[1] = M[1][1];
  B[2] = M[1][2];

  fprintf(stderr, "%g %g %g    |    %g %g %g\n", A[0], A[1], A[2], B[0], B[1], B[2]);

  return TRUE;
}

static int comparePolygonPoint(gconstpointer pointA, gconstpointer pointB, gpointer data)
{
  VisuPlane *plane;
  float vectGA[3], vectGB[3];
  int i;
  float det;

  plane = (VisuPlane*)data;
  for (i = 0; i < 3; i++)
    {
      vectGA[i] = ((float*)pointA)[i] - plane->pointG[i];
      vectGB[i] = ((float*)pointB)[i] - plane->pointG[i];
    }
  det = vectGA[0] * vectGB[1] * plane->nVect[2] + vectGB[0] * plane->nVect[1] * vectGA[2] +
    plane->nVect[0] * vectGA[1] * vectGB[2] - vectGA[2] * vectGB[1] * plane->nVect[0] -
    vectGA[1] * vectGB[0] * plane->nVect[2] - vectGA[0] * vectGB[2] * plane->nVect[1];
  if (det < 0.)
    return -1;
  else if (det > 0.)
    return 1;
  else
    return 0;
}

void visu_plane_getNVectUser(VisuPlane *plane, float *vect)
{
  int i;

  g_return_if_fail(VISU_IS_PLANE_TYPE(plane));

  for (i = 0; i < 3; i++)
    vect[i] = plane->nVectUser[i];
}

void visu_plane_getNVect(VisuPlane *plane, float *vect)
{
  int i;

  g_return_if_fail(VISU_IS_PLANE_TYPE(plane));

  for (i = 0; i < 3; i++)
    vect[i] = plane->nVect[i];
}
/**
 * visu_plane_getColor:
 * @plane: a #VisuPlane ;
 *
 * Stores the color of the plane.
 *
 * Returns: (transfer none): a #ToolColor.
 */
ToolColor* visu_plane_getColor(VisuPlane *plane)
{
  g_return_val_if_fail(VISU_IS_PLANE_TYPE(plane), (ToolColor*)0);

  return plane->color;
}
/**
 * visu_plane_getDistanceFromOrigin:
 * @plane: a #VisuPlane ;
 *
 * Stores the distance of the plane to the origin.
 *
 * Returns: a float value.
 */
gfloat visu_plane_getDistanceFromOrigin(VisuPlane *plane)
{
  g_return_val_if_fail(VISU_IS_PLANE_TYPE(plane), 0.f);

  return plane->dist;
}

int visu_plane_setHiddenState(VisuPlane *plane, int side)
{
  g_return_val_if_fail(VISU_IS_PLANE_TYPE(plane), 0);
  g_return_val_if_fail(side == VISU_PLANE_SIDE_NONE ||
		       side == VISU_PLANE_SIDE_PLUS ||
		       side == VISU_PLANE_SIDE_MINUS, 0);

  DBG_fprintf(stderr, "Visu VisuPlane : hide state (%d, old %d) for plane %p.\n",
	      side, plane->hiddenSide, (gpointer)plane);
  if (plane->hiddenSide == side)
    return 0;
  plane->hiddenSide = side;
  return 1;
}
int visu_plane_getHiddenState(VisuPlane *plane)
{
  g_return_val_if_fail(VISU_IS_PLANE_TYPE(plane), 0);
  return plane->hiddenSide;
}

static void onBoxSizeChanged(VisuBox *box _U_, float extens _U_, gpointer data)
{
  VisuPlane *plane = VISU_PLANE(data);

  computeInter(plane);
}

/**
 * visu_plane_class_getVisibility:
 * @listOfVisuPlanes: an array of #VisuPlane, NULL terminated ;
 * @point: three cartesian coordinates.
 *
 * Compute the visibility of the given @point, following the masking scheme
 * of the given plane list.
 *
 * Returns: TRUE if the point is not masked.
 */
gboolean visu_plane_class_getVisibility(VisuPlane **listOfVisuPlanes, float point[3])
{
  int i;
  float pScal;
  gboolean visibility;

  visibility = (plane_hidingMode == VISU_PLANE_HIDE_UNION) || !listOfVisuPlanes[0];

  for (i = 0; listOfVisuPlanes[i]; i++)
    {
      pScal = listOfVisuPlanes[i]->nVect[0] * point[0] +
	listOfVisuPlanes[i]->nVect[1] * point[1] +
	listOfVisuPlanes[i]->nVect[2] * point[2] -
	listOfVisuPlanes[i]->dist;
      switch (plane_hidingMode)
	{
	case VISU_PLANE_HIDE_UNION:
	  visibility =
	    visibility && (pScal * listOfVisuPlanes[i]->hiddenSide >= 0.);
	  break;
	case VISU_PLANE_HIDE_INTER:
	  visibility =
	    visibility || (pScal * listOfVisuPlanes[i]->hiddenSide >= 0.);
	  break;
	default: break;
	}
    }
  return visibility;
}
/**
 * visu_plane_class_getIntersection:
 * @listOfVisuPlanes: an array of #VisuPlane, NULL terminated ;
 * @pointA: three cartesian coordinates.
 * @pointB: three cartesian coordinates.
 * @inter: a location to store the intersection point.
 * @inside: a boolean.
 *
 * Compute the location of the intersection point of segment AB with list of
 * planes @listOfVisuPlanes. If there are several intersections, the closest to
 * point A is returned. If @inside is TRUE, then the intersection
 * point must be within the line [AB].
 *
 * Returns: TRUE if there is an intersection.
 */
gboolean visu_plane_class_getIntersection(VisuPlane **listOfVisuPlanes, float pointA[3],
				float pointB[3], float inter[3], gboolean inside)
{
  float lambda;
  int i;
  float lambdaMin;

/*   DBG_fprintf(stderr, "VisuPlane: intersection A(%g;%g;%g) B(%g;%g;%g).\n", */
/* 	      pointA[0], pointA[1], pointA[2], pointB[0], pointB[1], pointB[2]); */
  lambdaMin = 2.f;
  for (i = 0; listOfVisuPlanes[i]; i++)
    {
      if (!visu_plane_getLineIntersection(listOfVisuPlanes[i], pointA, pointB, &lambda))
        lambda = 2.f;
      if (( inside && lambda >= 0. && lambda <= 1. && lambda < lambdaMin) ||
          (!inside && ((lambda <= 0.f && lambda > lambdaMin) || (lambdaMin > 0.f && lambda < lambdaMin))))
        lambdaMin = lambda;
    }
  if (lambdaMin == 2.f)
    return FALSE;

  inter[0] = pointA[0] + lambdaMin * (pointB[0] - pointA[0]);
  inter[1] = pointA[1] + lambdaMin * (pointB[1] - pointA[1]);
  inter[2] = pointA[2] + lambdaMin * (pointB[2] - pointA[2]);
  return TRUE;
}

static gint comparisonForSortingFloats(gconstpointer a, gconstpointer b,
				       gpointer user_data _U_)
{
  float c;

  c = *((float*)b) - *((float*)a);
  if (c < 0.f)
    return  (gint)(1);
  else if (c > 0.f)
    return (gint)(-1);
  else
    return (gint)(0);
}

static gint comparisonForHavingIndices(gconstpointer a, gconstpointer b,
				       gpointer user_data)
{
  float c;
  float *array;

  array = (float*)user_data;
  /*DBG_fprintf(stderr, "INDICES : a %d    b %d\n", *((int*)a), *((int*)b));*/
  c = array[*(int*)b] - array[*(int*)a];
  /*DBG_fprintf(stderr, "LAMBDA b (%f) - LAMBDA a (%f) :  %f\n", vb, va,c);*/
  if (c < 0.f)
    return  (gint)(1);
  else if (c > 0.f)
    return (gint)(-1);
  else
    return (gint)(0);
}

gboolean visu_plane_class_getOrderedIntersections(int nVisuPlanes, VisuPlane **listOfVisuPlanes,
					float pointA[3], float pointB[3],
					float *inter, int *index)
{
  float *lambda;
  int *indices;
  int i;

  lambda = g_malloc(sizeof(float) * nVisuPlanes);
  indices = g_malloc(sizeof(int) * nVisuPlanes);
  for (i = 0; listOfVisuPlanes[i]; i++)
    {
      indices[i] = i;
      if (!visu_plane_getLineIntersection(listOfVisuPlanes[i], pointA, pointB, lambda + i) ||
          lambda[i] < 0. || lambda[i] > 1.)
        /* If lambda is not between 0 and 1, this means that the plane i is not
           intersected by the AB segment => return FALSE */
        return FALSE;
    }
  /* Sort lambda */
  DBG_fprintf(stderr, "Sorting planes by lambda :\n  ... lambda unsorted : ");
  for (i = 0; i < nVisuPlanes; i++)
    DBG_fprintf(stderr, "%f ", lambda[i]);
  DBG_fprintf(stderr, "\n");
  g_qsort_with_data(indices, nVisuPlanes, sizeof(int),
                    comparisonForHavingIndices, (float*)lambda);
  g_qsort_with_data(lambda, nVisuPlanes, sizeof(float),
                    comparisonForSortingFloats, NULL);
  DBG_fprintf(stderr, "  ... lambda sorted   : ");
  for (i = 0; i < nVisuPlanes; i++)
    DBG_fprintf(stderr, "%f ", lambda[i]);
  DBG_fprintf(stderr, "\n  ... indices : ");
  for (i = 0; i < nVisuPlanes; i++)
    DBG_fprintf(stderr, "%d ", indices[i]);
  DBG_fprintf(stderr, "\n");

  /* Put the intersections in inter in the right order. */
  DBG_fprintf(stderr, "  ... intersections sorted :\n");
  for (i = 0; i < nVisuPlanes; i++)
  {
    *(inter + 3*i) = pointA[0] + lambda[i] * (pointB[0] - pointA[0]);
    *(inter + 3*i + 1) = pointA[1] + lambda[i] * (pointB[1] - pointA[1]);
    *(inter + 3*i + 2) = pointA[2] + lambda[i] * (pointB[2] - pointA[2]);
    DBG_fprintf(stderr, "    %f %f %f\n", *(inter + 3*i), *(inter + 3*i + 1), *(inter + 3*i + 2));
    *(index + i) = indices[i];
  }

  g_free(lambda);
  g_free(indices);
  return TRUE;
}
/**
 * visu_plane_class_showHideAll:
 * @listOfVisuPlanes: (array zero-terminated=1) (element-type VisuPlane*) (transfer none):  an array of #VisuPlane, NULL terminated.
 * @visuData: a #VisuData which show or hide nodes to;
 *
 * This method test for each node if it is hidden or not by the
 * combination of all the given planes.
 *
 * Returns: TRUE if VisuNodeArray::VisibilityChanged signal should be called.
 */
gboolean visu_plane_class_showHideAll(VisuPlane **listOfVisuPlanes, VisuData *visuData)
{
  int i, n;
  int reDraw;
  float point[3];
  gboolean visibility;
  VisuPlane **tmpLst;
  VisuNodeArrayIter iter;

  g_return_val_if_fail(visuData && listOfVisuPlanes, FALSE);

  DBG_fprintf(stderr, "VisuPlanes : applying masking properties of planes.\n");

  /* Remove planes of the given list that doesn't have a masking action. */
  for (n = 0; listOfVisuPlanes[n]; n++);
  tmpLst = g_malloc(sizeof(VisuPlane*) * (n + 1));
  n = 0;
  for (i = 0; listOfVisuPlanes[i]; i++)
    {
      if (listOfVisuPlanes[i]->hiddenSide != VISU_PLANE_SIDE_NONE)
	{
	  DBG_fprintf(stderr, " | plane %p has a masking property.\n",
		      (gpointer)listOfVisuPlanes[i]);
	  tmpLst[n] = listOfVisuPlanes[i];
	  n += 1;
	}
      else
	DBG_fprintf(stderr, " | plane %p has no masking property.\n",
		    (gpointer)listOfVisuPlanes[i]);
    }
  tmpLst[n] = (VisuPlane*)0;

  reDraw = 0;
  /* We change the rendered attribute of all nodes, very expensive... */
  if (tmpLst[0])
    {
      visu_node_array_iterNew(VISU_NODE_ARRAY(visuData), &iter);
      for (visu_node_array_iterStart(VISU_NODE_ARRAY(visuData), &iter); iter.element;
	   visu_node_array_iterNextElement(VISU_NODE_ARRAY(visuData), &iter))
	if (iter.element->sensitiveToPlanes && iter.element->rendered)
	  {
	    DBG_fprintf(stderr, " | element '%s'\n", iter.element->name);
	    for (visu_node_array_iterRestartNode(VISU_NODE_ARRAY(visuData), &iter); iter.node;
		 visu_node_array_iterNextNode(VISU_NODE_ARRAY(visuData), &iter))
	      {
		/* If node is already hiden, well, we go to the next. */
		if (!iter.node->rendered)
		  continue;

		visu_data_getNodePosition(visuData, iter.node, point);
	    
		visibility = visu_plane_class_getVisibility(tmpLst, point);
		if (!visibility)
		  reDraw = visu_node_setVisibility(iter.node, FALSE) || reDraw;
		DBG_fprintf(stderr, " | node '%d' -> %d %d\n", iter.node->number, visibility, iter.node->rendered);
	      }
	  }
    }
  g_free(tmpLst);
  return reDraw;
}
int visu_plane_class_setHiddingMode(VisuPlaneHidingMode mode)
{
  g_return_val_if_fail(mode < VISU_PLANE_HIDE_N_VALUES, 0);

  if (mode == plane_hidingMode)
    return 0;

  plane_hidingMode = mode;
  return 1;
}

/***************************************************
 * Parsing of data files containing list of planes *
 ***************************************************/
/* Known elements. */
#define PLANES_PARSER_ELEMENT_PLANES    "planes"
#define PLANES_PARSER_ELEMENT_PLANE     "plane"
#define PLANES_PARSER_ELEMENT_GEOMETRY  "geometry"
#define PLANES_PARSER_ELEMENT_HIDE      "hide"
#define PLANES_PARSER_ELEMENT_COLOR     "color"
/* Known attributes. */
#define PLANES_PARSER_ATTRIBUTES_RENDERED "rendered"
#define PLANES_PARSER_ATTRIBUTES_VECTOR   "normal-vector"
#define PLANES_PARSER_ATTRIBUTES_DISTANCE "distance"
#define PLANES_PARSER_ATTRIBUTES_STATUS   "status"
#define PLANES_PARSER_ATTRIBUTES_INVERT   "invert"
#define PLANES_PARSER_ATTRIBUTES_RGBA     "rgba"

static gboolean planesStarted;

/* This method is called for every element that is parsed.
   The user_data must be a GList of planes. When a 'plane'
   element, a new plane is created and prepend in the list.
   When 'geometry' or other qualificative elements are
   found, the first plane of the list is modified accordingly. */
void listOfVisuPlanes_element(GMarkupParseContext *context _U_,
                          const gchar         *element_name,
                          const gchar        **attribute_names,
                          const gchar        **attribute_values,
                          gpointer             user_data,
                          GError             **error)
{
  GList **planesList;
  VisuPlane *plane;
  float normalVector[3];
  float distance;
  float colorRGBA[4];
  ToolColor *color;
  int i, res;
  int side, set;
  gboolean rendered;

  g_return_if_fail(user_data);
  planesList = (GList **)user_data;

  DBG_fprintf(stderr, "VisuPlanes parser: found '%s' element.\n", element_name);
  if (!strcmp(element_name, PLANES_PARSER_ELEMENT_PLANES))
    {
      /* Should have no attributes. */
      if (attribute_names[0])
	{
	  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ATTRIBUTE,
		      _("Unexpected attribute '%s' for element '%s'."),
		      attribute_names[0], PLANES_PARSER_ELEMENT_PLANES);
	  return;
	}
      /* Initialise planeList. */
      if (*planesList)
	g_warning("Unexpected non null pointer as user_data for the "
		  "plane parser.");
      planesStarted = TRUE;
      *planesList = (GList*)0;
    }
  else if (!strcmp(element_name, PLANES_PARSER_ELEMENT_PLANE))
    {
      rendered = TRUE;
      /* May have one attribute. */
      if (attribute_names[0])
	{
	  if (!strcmp(attribute_names[0], PLANES_PARSER_ATTRIBUTES_RENDERED) )
	    {
	      if (!strcmp(attribute_values[0], "yes"))
		rendered = TRUE;
	      else if (!strcmp(attribute_values[0], "no"))
		rendered = FALSE;
	      else
		g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
			    _("Invalid value '%s' for attribute '%s'."),
			    attribute_values[0], PLANES_PARSER_ATTRIBUTES_RENDERED);
	    }
	  else
	    {
	      g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ATTRIBUTE,
			  _("Unexpected attribute '%s' for element '%s'."),
			  attribute_names[0], PLANES_PARSER_ELEMENT_PLANE);
	      return;
	    }
	}
      plane = visu_plane_newUndefined();
      visu_plane_setRendered(plane, rendered);
      DBG_fprintf(stderr, "VisuPlanes parser: adding plane %p to list %p.\n",
		  (gpointer)plane, (gpointer)(*planesList));
      *planesList = g_list_prepend(*planesList, (gpointer)plane);
      DBG_fprintf(stderr, " | new plane list: %p.\n", (gpointer)(*planesList));
    }
  else if (planesStarted && !strcmp(element_name, PLANES_PARSER_ELEMENT_GEOMETRY))
    {
      if (!*planesList || !(*planesList)->data)
	{
	  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
		      _("DTD error : parent element '%s' of element '%s' is missing."),
		      PLANES_PARSER_ELEMENT_PLANE, element_name);
	  return;
	}

      DBG_fprintf(stderr, "VisuPlanes parser: associated plane : %p.\n", (*planesList)->data);
      for(i = 0; attribute_names[i]; i++)
	{
	  if (!strcmp(attribute_names[i], PLANES_PARSER_ATTRIBUTES_VECTOR))
	    {
	      res = sscanf(attribute_values[i], "%g %g %g",
			   normalVector, normalVector + 1, normalVector + 2);
	      if (res != 3)
		g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
			    _("Invalid value '%s' for attribute '%s'."),
			    attribute_values[i], PLANES_PARSER_ATTRIBUTES_VECTOR);
	      visu_plane_setNormalVector((VisuPlane*)(*planesList)->data, normalVector);
	    }
	  else if (!strcmp(attribute_names[i], PLANES_PARSER_ATTRIBUTES_DISTANCE))
	    {
	      res = sscanf(attribute_values[i], "%g", &distance);
	      if (res != 1)
		g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
			    _("Invalid value '%s' for attribute '%s'."),
			    attribute_values[i], PLANES_PARSER_ATTRIBUTES_DISTANCE);
	      visu_plane_setDistanceFromOrigin((VisuPlane*)(*planesList)->data, distance);
	    }
	  else
	    g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ATTRIBUTE,
			_("Unexpected attribute '%s' for element '%s'."),
			attribute_names[i], PLANES_PARSER_ELEMENT_GEOMETRY);
	}
    }
  else if (planesStarted && !strcmp(element_name, PLANES_PARSER_ELEMENT_HIDE))
    {
      if (!*planesList || !(*planesList)->data)
	{
	  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
		      _("DTD error: parent element '%s' of element '%s' is missing."),
		      PLANES_PARSER_ELEMENT_PLANE, element_name);
	  return;
	}

      set = 0;
      side = 1;
      for(i = 0; attribute_names[i]; i++)
	{
	  if (!strcmp(attribute_names[i], PLANES_PARSER_ATTRIBUTES_STATUS))
	    {
	      if (!strcmp(attribute_values[i], "yes"))
		set = 1;
	      else if (!strcmp(attribute_values[i], "no"))
		set = 0;
	      else
		g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
			    _("Invalid value '%s' for attribute '%s'."),
			    attribute_values[i], PLANES_PARSER_ATTRIBUTES_STATUS);
	    }
	  else if (!strcmp(attribute_names[i], PLANES_PARSER_ATTRIBUTES_INVERT))
	    {
	      if (!strcmp(attribute_values[i], "yes"))
		side = -1;
	      else if (!strcmp(attribute_values[i], "no"))
		side = 1;
	      else
		g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
			    _("Invalid value '%s' for attribute '%s'."),
			    attribute_values[i], PLANES_PARSER_ATTRIBUTES_INVERT);
	    }
	  else
	    g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ATTRIBUTE,
			_("Unexpected attribute '%s' for element '%s'."),
			attribute_names[i], PLANES_PARSER_ELEMENT_HIDE);
	}
      ((VisuPlane*)(*planesList)->data)->hiddenSide = side * set;
    }
  else if (planesStarted && !strcmp(element_name, PLANES_PARSER_ELEMENT_COLOR))
    {
      if (!*planesList || !(*planesList)->data)
	{
	  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
		      _("DTD error: parent element '%s' of element '%s' is missing."),
		      PLANES_PARSER_ELEMENT_PLANE, element_name);
	  return;
	}

      for(i = 0; attribute_names[i]; i++)
	{
	  if (!strcmp(attribute_names[i], PLANES_PARSER_ATTRIBUTES_RGBA))
	    {
	      res = sscanf(attribute_values[i], "%g %g %g %g",
			   colorRGBA, colorRGBA + 1, colorRGBA + 2, colorRGBA + 3);
	      if (res != 4)
		g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
			    _("Invalid value '%s' for attribute '%s'."),
			    attribute_values[i], PLANES_PARSER_ATTRIBUTES_RGBA);
	      color = tool_color_addFloatRGBA(colorRGBA, &res);
	      visu_plane_setColor(((VisuPlane*)(*planesList)->data), color);
	    }
	  else
	    g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ATTRIBUTE,
			_("Unexpected attribute '%s' for element '%s'."),
			attribute_names[i], PLANES_PARSER_ELEMENT_COLOR);
	}
    }
  else if (planesStarted)
    g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
		_("Unexpected element '%s'."), element_name);
}

/* Check when a element is closed that everything required has been set. */
void listOfVisuPlanes_end(GMarkupParseContext *context _U_,
		      const gchar         *element_name,
		      gpointer             user_data,
		      GError             **error)
{
  GList **planesList;
  float *vect;

  g_return_if_fail(user_data);
  planesList = (GList**)user_data;
  
  if (!strcmp(element_name, PLANES_PARSER_ELEMENT_PLANE))
    {
      g_return_if_fail(*planesList && (*planesList)->data);

      if (!((VisuPlane*)(*planesList)->data)->color)
	{
	  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ATTRIBUTE,
		      _("DTD error: missing or wrong child element '%s'."),
		      PLANES_PARSER_ELEMENT_COLOR);
	  return;
	}
      vect = ((VisuPlane*)(*planesList)->data)->nVectUser;
      if (vect[0] == 0. && vect[1] == 0. && vect[2] == 0.)
	{
	  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ATTRIBUTE,
		      _("DTD error: missing or wrong child element '%s'."),
		      PLANES_PARSER_ELEMENT_GEOMETRY);
	  return;
	}
    }
  else if (!strcmp(element_name, PLANES_PARSER_ELEMENT_PLANES))
    planesStarted = FALSE;
}

/* What to do when an error is raised. */
void listOfVisuPlanes_error(GMarkupParseContext *context _U_,
                          GError              *error,
                          gpointer             user_data _U_)
{
  DBG_fprintf(stderr, "VisuPlanes parser: error raised '%s'.\n", error->message);
}

/**
 * visu_plane_class_parseXMLFile:
 * @filename: (type filename): the file to parse ;
 * @planes: (out callee-allocates) (array zero-terminated=1):a pointer to store the
 * parsed list (will be allocated and %NULL terminated) ;
 * @error: a pointer to store the error (can be NULL).
 *
 * Read the given file (syntax in XML) and create a list of planes. This list is an
 * allocated array that should be deallocated with g_free(). This array has always
 * one argument, since it is NULL terminated.
 *
 * Returns: (skip): TRUE if everything goes right, if not and @error (if not NULL)
 *          is set and contains the message of the error.
 */
gboolean visu_plane_class_parseXMLFile(gchar* filename, VisuPlane ***planes, GError **error)
{
  GMarkupParseContext* xmlContext;
  GMarkupParser parser;
  gboolean res;
  gsize size;
  gchar *buffer;
  GList *list, *tmpLst;
  int i;

  g_return_val_if_fail(filename && planes && !*planes, FALSE);

  *planes = g_malloc(sizeof(VisuPlane*));
  *planes[0] = (VisuPlane*)0;

  buffer = (gchar*)0;
  if (!g_file_get_contents(filename, &buffer, &size, error))
    return FALSE;

  /* Create context. */
  list = (GList*)0;
  parser.start_element = listOfVisuPlanes_element;
  parser.end_element   = listOfVisuPlanes_end;
  parser.text          = NULL;
  parser.passthrough   = NULL;
  parser.error         = listOfVisuPlanes_error;
  xmlContext = g_markup_parse_context_new(&parser, 0, &list, NULL);

  /* Parse data. */
  planesStarted = FALSE;
  res = g_markup_parse_context_parse(xmlContext, buffer, size, error);

  /* Free buffers. */
  g_markup_parse_context_free(xmlContext);
  g_free(buffer);

  /* Need to reverse the list since elements have been prepended. */
  list = g_list_reverse(list);

  /* Convert the list to an array. */
  DBG_fprintf(stderr, "VisuPlanes: create array of planes (%d).\n", g_list_length(list));
  *planes = g_realloc(*planes, sizeof(VisuPlane*) * (g_list_length(list) + 1));
  i = 0;
  tmpLst = list;
  while (tmpLst)
    {
      DBG_fprintf(stderr, " | %d -> %p\n", i, tmpLst->data);
      (*planes)[i] = (VisuPlane*)tmpLst->data;
      i += 1;
      tmpLst = g_list_next(tmpLst);
    }
  (*planes)[i] = (VisuPlane*)0;
  g_list_free(list);

  if ((*planes)[0] == (VisuPlane*)0 && !*error)
    {
      *error = g_error_new(G_MARKUP_ERROR, G_MARKUP_ERROR_EMPTY,
			   _("The file contains no plane.\n"));
      res = FALSE;
    }

  return res;
}
gboolean visu_plane_class_exportXMLFile(const gchar* filename,
                                        VisuPlane **list, GError **error)
{
  GString *buffer;
  int i;
  gboolean valid;

  g_return_val_if_fail(filename && list, FALSE);

  buffer = g_string_new("  <planes>\n");
  for (i = 0; list[i]; i++)
    {
      g_string_append_printf(buffer, "    <plane rendered=\"%s\">\n",
			     (list[i]->rendered)?"yes":"no");
      g_string_append_printf(buffer, "      <geometry normal-vector=\"%g %g %g\""
			     " distance=\"%g\" />\n", list[i]->nVectUser[0],
			     list[i]->nVectUser[1], list[i]->nVectUser[2],
			     list[i]->dist);
      switch (list[i]->hiddenSide)
	{
	case VISU_PLANE_SIDE_NONE:
	  g_string_append(buffer, "      <hide status=\"no\" invert=\"no\" />\n");
	  break;
	case VISU_PLANE_SIDE_MINUS:
	  g_string_append(buffer, "      <hide status=\"yes\" invert=\"yes\" />\n");
	  break;
	case VISU_PLANE_SIDE_PLUS:
	  g_string_append(buffer, "      <hide status=\"yes\" invert=\"no\" />\n");
	  break;
	default:
	  g_warning("Unknown hiddenSide attribute ofr the given plane.");
	};
      g_string_append_printf(buffer, "      <color rgba=\"%g %g %g %g\" />\n",
			     list[i]->color->rgba[0], list[i]->color->rgba[1],
			     list[i]->color->rgba[2], list[i]->color->rgba[3]);
      g_string_append(buffer, "    </plane>\n");
    }
  g_string_append(buffer, "  </planes>");

  valid = tool_XML_substitute(buffer, filename, "planes", error);
  if (!valid)
    {
      g_string_free(buffer, TRUE);
      return FALSE;
    }
  
  valid = g_file_set_contents(filename, buffer->str, -1, error);
  g_string_free(buffer, TRUE);
  return valid;
}
