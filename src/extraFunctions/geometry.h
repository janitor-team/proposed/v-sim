/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <glib-object.h>

#include <visu_data.h>
#include <coreTools/toolShade.h>

typedef struct _VisuPaths VisuPaths;

#define VISU_TYPE_PATHS (visu_paths_get_type())
GType       visu_paths_get_type(void);

VisuPaths* visu_paths_new(float translation[3]);
VisuPaths* visu_paths_ref(VisuPaths *paths);
void        visu_paths_unref(VisuPaths *paths);
void        visu_paths_free(VisuPaths *paths);
void        visu_paths_empty(VisuPaths *paths);

gboolean    visu_paths_addFromDiff(VisuPaths *paths, VisuData *data);
gboolean    visu_paths_addNodeStep(VisuPaths *paths, guint time, guint nodeId,
                                    float xyz[3], float dxyz[3], float energy);
void        visu_paths_pinPositions(VisuPaths *paths, VisuData *data);

void        visu_paths_draw(VisuPaths *paths);
void        visu_paths_constrainInBox(VisuPaths *paths, VisuData *data);
gboolean    visu_paths_exportXMLFile(const VisuPaths *paths,
                                    const gchar *filename, GError **error);
gboolean    visu_paths_parseFromXML(const gchar* filename, VisuPaths *paths,
                                     GError **error);

void        visu_paths_setTranslation(VisuPaths *paths, float cartCoord[3]);
gboolean    visu_paths_setToolShade(VisuPaths *paths, ToolShade* shade);
ToolShade*      visu_paths_getToolShade(VisuPaths *paths);
guint       visu_paths_getLength(VisuPaths *paths);

/**
 * VISU_GEODIFF_ID:
 *
 * The default name used for the #VisuGlExt representing
 * displacements, see visu_gl_ext_getFromName().
 */
#define VISU_GEODIFF_ID        "geometry_diff"
gboolean visu_geodiff_new(VisuData *dataRef, VisuData *data, gboolean reorder);
gboolean visu_geodiff_hasData(VisuData *data);
gchar*   visu_geodiff_export(VisuData *data);
void     visu_geodiff_getPeriodicDistance(float diff[3], VisuData *data,
                                          VisuNode *node1, VisuNode *node2);

void visu_geometry_init();

#endif
