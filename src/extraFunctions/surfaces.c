/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "surfaces.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <visu_object.h>
#include <visu_tools.h>
#include <coreTools/toolMatrix.h>

/**
 * SECTION:surfaces
 * @short_description: Supports loading of .surf files and drawing of
 * surfaces through OpenGL.
 *
 * <para>Originally written by Luc Billard for his Visualize program. This
 * module allows loading of .surf files to draw scalar fields on top of
 * the current display scene. .surf files are text files which specs are
 * the following :
 * <itemizedlist>
 * <listitem><para>
 * 1st line is arbitrary
 * </para></listitem>
 * <listitem><para>
 * 2nd line must contain 3 real (float) values: dxx dyx dyy
 * </para></listitem>
 * <listitem><para>
 * 3rd line must contain 3 real (float) values: dzx dzy dzz
 * </para></listitem>
 * <listitem><para>
 * 4th line must contain 3 positive integers which
 * represents respectively the number of surfaces, the total number of
 * polys, and the total number of points
 * </para></listitem>
 * <listitem><para>
 * Then, for each of these surfaces :      
 *     
 * <itemizedlist> 
 * <listitem><para>
 * next line must contain the name of the surface : it is a
 * string which should match the pattern surface_*
 * </para></listitem>
 * <listitem><para>
 * next line must contain 2 positive integer values: the number of polys
 * (num_polys) and
 * the number of points (num_points) used by the surface
 * </para></listitem>
 * <listitem><para>
 * each of the following num_polys lines must match the pattern 
 * [n i_1 i_2 i_3 ... i_n] where n is the number of vertices in the poly (n >= 3)
 * and [i_1 i_2 i_3 ... i_n] are the numbering of these vertices (vertices numbered from 1 to num_points)
 * </para></listitem>
 * <listitem><para>
 * each of the following num_points lines must contain 6 real values for the 
 * successive (1 to num_points) points : [x y z nx ny nz], where x y z are the coordinates of the point
 * and nx ny nz are the coordinates of the unit normal at the point
 * </para></listitem>
 * </itemizedlist>
 * 
 * </para></listitem>
 * </itemizedlist>
 * 
 * It is the responsibility of the user to guarantee that
 * dxx, dyx, dyy, dzx, dzy, dzz match the one currently loaded in V_Sim's
 * current context. Though if you use <link
 * linkend="v-sim-panelSurfaces">panelSurfaces</link> you can ask
 * to resize the surfaces so that they fit in the current loaded box.
 * </para>
 */

#define ISOSURFACES_FLAG_POTENTIAL "# potentialValue"

static GQuark quark;

typedef struct VisuSurfacesProperties_struct
{
  gchar *name;
  /* The element (G_TYPE_INT...). */
  guint type;
  /* A pointer to the surf it belongs to. */
  VisuSurfaces *surf;
  /* The data. */
  gpointer data;
} VisuSurfacesProperties;
static void freeVisuSurfacesProperties(gpointer data);
static void propertiesRemoveSurf(gpointer key, gpointer value, gpointer data);
static void propertiesReallocateSurf(gpointer key, gpointer value, gpointer data);

/**
 * VisuSurfaces:
 * @refCount: ref counter.
 * @nsurf: number of surfaces encoded in this structure ;
 * @basePoints: the geometric description of surfaces.
 * @volatilePlanes: a storage for volatile polygons, .
 * @box: the description of the box where surfaces are drawn ;
 * @resources: for each surface points on a #VisuSurfacesResources ;
 * @ids: gives a list of ids used to index surfaces ;
 * @properties: an hashtable of properties.
 *
 * This structure stores surfaces. Several surfaces are stored in a single
 * structure for improved performences.
 */

enum
  {
    SURFACE_ADDED_SIGNAL,
    SURFACE_REMOVED_SIGNAL,
    SURFACE_MASKED_SIGNAL,
    SURFACE_RENDERING_SIGNAL,
    NB_SIGNAL
  };

struct _VisuSurfacesClass
{
  GObjectClass parent;
};

/* Internal variables. */
static guint surfaces_signals[NB_SIGNAL] = { 0 };

/* Object gestion methods. */
static void visu_surfaces_dispose   (GObject* obj);
static void visu_surfaces_finalize  (GObject* obj);
static void visu_boxed_interface_init(VisuBoxedInterface *iface);

/* Local functions. */
static VisuBox* visu_surfaces_getBox(VisuBoxed *self);
static gboolean visu_surfaces_setBox(VisuBoxed *self, VisuBox *box, gboolean fit);

G_DEFINE_TYPE_WITH_CODE(VisuSurfaces, visu_surfaces, G_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE(VISU_TYPE_BOXED,
                                              visu_boxed_interface_init))

static void visu_surfaces_class_init(VisuSurfacesClass *klass)
{
  DBG_fprintf(stderr, "VisuSurfaces: creating the class of the object.\n");

  DBG_fprintf(stderr, "                - adding new signals ;\n");
  /**
   * VisuSurfaces::added:
   * @surf: the object which received the signal.
   *
   * Gets emitted when a new surface is added to the object.
   *
   * Since: 3.7
   */
  surfaces_signals[SURFACE_ADDED_SIGNAL] =
    g_signal_newv("added", G_TYPE_FROM_CLASS(klass),
        	  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
        	  NULL, NULL, NULL, g_cclosure_marshal_VOID__VOID,
        	  G_TYPE_NONE, 0, NULL);
  /**
   * VisuSurfaces::removed:
   * @surf: the object which received the signal.
   * @idSurf: the id of the surface that has been removed.
   *
   * Gets emitted when a surface is removed from the object.
   *
   * Since: 3.7
   */
  surfaces_signals[SURFACE_REMOVED_SIGNAL] =
    g_signal_new("removed", G_TYPE_FROM_CLASS (klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL, NULL, g_cclosure_marshal_VOID__UINT,
                 G_TYPE_NONE, 1, G_TYPE_UINT);
  /**
   * VisuSurfaces::masked:
   * @surf: the object which received the signal.
   *
   * Gets emitted when a surface is shown or hidden by a plane.
   *
   * Since: 3.7
   */
  surfaces_signals[SURFACE_MASKED_SIGNAL] =
    g_signal_new("masked", G_TYPE_FROM_CLASS (klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);
  /**
   * VisuSurfaces::rendering:
   * @surf: the object which received the signal.
   * @idSurf: the id of the surface that has been modified.
   *
   * Gets emitted when a rendering property of a surface is modified.
   *
   * Since: 3.7
   */
  surfaces_signals[SURFACE_RENDERING_SIGNAL] =
    g_signal_new("rendering", G_TYPE_FROM_CLASS (klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL, NULL, g_cclosure_marshal_VOID__UINT,
                 G_TYPE_NONE, 1, G_TYPE_UINT);

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_surfaces_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_surfaces_finalize;

  quark = g_quark_from_static_string("visu_isosurfaces");
  
  visu_surfaces_resources_init();
}
static void visu_boxed_interface_init(VisuBoxedInterface *iface)
{
  iface->get_box = visu_surfaces_getBox;
  iface->set_box = visu_surfaces_setBox;
}

static void visu_surfaces_init(VisuSurfaces *obj)
{
  obj->dispose_has_run = FALSE;

  DBG_fprintf(stderr, "VisuSurfaces: creating a new surfaces (%p).\n", (gpointer)obj);

  obj->nsurf      = 0;
  obj->ids        = (int*)0;
  obj->box        = (VisuBox*)0;
  obj->resources  = (VisuSurfacesResources**)0;
  obj->properties = g_hash_table_new_full(g_str_hash, g_str_equal,
                                          NULL, freeVisuSurfacesProperties);
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_surfaces_dispose(GObject* obj)
{
  DBG_fprintf(stderr, "VisuSurfaces: dispose object %p.\n", (gpointer)obj);

  if (VISU_SURFACES(obj)->dispose_has_run)
    return;

  visu_surfaces_setBox(VISU_BOXED(obj), (VisuBox*)0, FALSE);

  VISU_SURFACES(obj)->dispose_has_run = TRUE;
  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_surfaces_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_surfaces_finalize(GObject* obj)
{
  VisuSurfaces *surf = VISU_SURFACES(obj);
  guint i;

  DBG_fprintf(stderr, "VisuSurfaces: finalize object %p.\n", (gpointer)obj);

  visu_surfaces_points_free(&surf->basePoints);
  visu_surfaces_points_free(&surf->volatilePlanes);
  if (surf->ids)
    g_free(surf->ids);
  if (surf->resources)
    {
      /* We free all resource that has no name (i.e. that are not stored in
	 the hashtable. */
      for (i = 0; i < surf->nsurf; i++)
	if (surf->resources[i] && !surf->resources[i]->surfnom)
	  visu_surfaces_resources_free(surf->resources[i]);
      g_free(surf->resources);
    }
  if (surf->properties)
    g_hash_table_destroy(surf->properties);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_surfaces_parent_class)->finalize(obj);
}

/**
 * visu_surfaces_getErrorQuark:
 *
 * Internal routine for error handling.
 *
 * Returns: the #GQuark associated to errors related to surface
 * files.
 */
GQuark visu_surfaces_getErrorQuark(void)
{
  return quark;
}

/**
 * visu_surfaces_checkConsistency:
 * @surf: a #VisuSurfaces object.
 *
 * Check if all arrays in the structures are consistent (without
 * overflow).
 */
void visu_surfaces_checkConsistency(VisuSurfaces* surf)
{
  DBG_fprintf(stderr, "Isosurfaces: Check consistency.\n");

  DBG_fprintf(stderr, " | Base points\n");
  visu_surfaces_points_check(&surf->basePoints);
  DBG_fprintf(stderr, " | Volatile planes\n");
  visu_surfaces_points_check(&surf->volatilePlanes);
}
/**
 * visu_surfaces_remove:
 * @surf: a #VisuSurfaces object ;
 * @idSurf: the id of the surf to remove.
 *
 * Remove from memory all polygons from the given surface.
 *
 * Returns: TRUE if the surface list is reduced to zero (and @surf
 *          to be freed).
 */
gboolean visu_surfaces_remove(VisuSurfaces *surf, int idSurf)
{
  guint i;
  int pos;
  
  g_return_val_if_fail(surf, FALSE);

  pos = visu_surfaces_getPosition(surf, idSurf);
  g_return_val_if_fail(pos >= 0 && (guint)pos < surf->nsurf, FALSE);

  if (surf->nsurf == 1)
    return TRUE;

  DBG_fprintf(stderr, "Isosurfaces: remove surface %d from %p.\n", pos, (gpointer)surf);

  DBG_fprintf(stderr, "Isosurfaces: remove base points.\n");
  visu_surfaces_points_remove(&surf->basePoints, pos);
  DBG_fprintf(stderr, "Isosurfaces: remove volatile planes.\n");
  visu_surfaces_points_remove(&surf->volatilePlanes, pos);
  surf->nsurf -= 1;
  
  /* If the resource has no name, then we free it since it is not shared. */
  DBG_fprintf(stderr, " | Additional removing.\n");
  if (!surf->resources[pos]->surfnom)
    visu_surfaces_resources_free(surf->resources[pos]);
  for (i = pos; i < surf->nsurf; i++)
    {
      surf->ids[i] = surf->ids[i + 1];
      /* Move resource pointers. */
      surf->resources[i] = surf->resources[i + 1];
    }
  surf->ids = g_realloc(surf->ids, sizeof(int) * surf->nsurf);
  surf->resources = g_realloc(surf->resources,
			      sizeof(VisuSurfacesResources*) * surf->nsurf);
  g_hash_table_foreach(surf->properties, propertiesRemoveSurf, GINT_TO_POINTER(pos));
  
#if DEBUG == 1
  visu_surfaces_checkConsistency(surf);
#endif

  g_signal_emit(G_OBJECT(surf), surfaces_signals[SURFACE_REMOVED_SIGNAL], 0, idSurf, NULL);

  return FALSE;
}

/**
 * visu_surfaces_new:
 * @bufferSize: an integer.
 *
 * Create a new (with unallocated internal arrays) structure to store
 * surfaces. The buffer size is used to store other values than the position
 * and the normal on each point.
 *
 * Returns: a newly allocated #VisuSurfaces structure.
 */
VisuSurfaces* visu_surfaces_new(guint bufferSize)
{
  VisuSurfaces *surf;

  surf = VISU_SURFACES(g_object_new(VISU_TYPE_SURFACES, NULL));
  DBG_fprintf(stderr, "VisuSurfaces: new surfaces %p.\n", (gpointer)surf);
  visu_surfaces_points_init(&surf->basePoints,     bufferSize);
  visu_surfaces_points_init(&surf->volatilePlanes, bufferSize);

  return surf;
}
/**
 * visu_surfaces_allocate:
 * @surf: a #VisuSurfaces structure ;
 * @nsurf: the number of surfaces to store ;
 * @npolys: the number of polygons (in total) ;
 * @npoints: the total number of vertices.
 *
 * Allocate internal arrays to store surfaces having the given description.
 */
void visu_surfaces_allocate(VisuSurfaces *surf, guint nsurf, guint npolys, guint npoints)
{
  guint i;
  
  DBG_fprintf(stderr, "VisuSurfaces: allocate %p to %d %d %d.\n",
	      (gpointer)surf, nsurf, npolys, npoints);
  surf->nsurf = nsurf;
  visu_surfaces_points_allocate(&surf->basePoints, nsurf, npolys, npoints);
  visu_surfaces_points_allocate(&surf->volatilePlanes, nsurf, 0, 0);

  DBG_fprintf(stderr, "VisuSurfaces: allocate complement.\n");
  /* Allocating pointers. */
  surf->ids       = g_malloc(nsurf * sizeof(int));
  surf->resources = g_malloc(nsurf * sizeof(VisuSurfacesResources*));

  /* Set initial values. */
  for(i = 0; i < surf->nsurf; i++)
    surf->resources[i] = (VisuSurfacesResources*)0;
  DBG_fprintf(stderr, " | OK.\n");
}
/**
 * visu_surfaces_add:
 * @surf: a #VisuSurfaces structure ;
 * @nsurf: the number of surfaces to add ;
 * @npolys: the number of polygons to add ;
 * @npoints: the number of vertices to add.
 *
 * Change the allocation of internal arrays to store the additional surfaces
 * with the given description.
 */
void visu_surfaces_add(VisuSurfaces *surf, guint nsurf, guint npolys, guint npoints)
{
  guint i, nsurf_old;
  gboolean alloc;

  g_return_if_fail(surf);
  
  nsurf_old = surf->nsurf;
  DBG_fprintf(stderr, "Isosurfaces: add surface(s) %d %d %d.\n",
	      nsurf, npolys, npoints);
  surf->nsurf                        += nsurf;
  surf->basePoints.nsurf             += nsurf;
  surf->volatilePlanes.nsurf         += nsurf;
  surf->basePoints.num_polys         += npolys;
  surf->basePoints.num_points        += npoints;
  surf->basePoints.num_polys_surf     = g_realloc(surf->basePoints.num_polys_surf,
						  surf->nsurf * sizeof(int));
  surf->volatilePlanes.num_polys_surf = g_realloc(surf->volatilePlanes.num_polys_surf,
						  surf->nsurf * sizeof(int));
  for (i = 0; i < nsurf; i++)
    {
      surf->basePoints.num_polys_surf[nsurf_old + i]     = 0;
      surf->volatilePlanes.num_polys_surf[nsurf_old + i] = 0;
    }

  surf->basePoints.poly_surf_index    = g_realloc(surf->basePoints.poly_surf_index,
						  surf->basePoints.num_polys *
						  sizeof(int));
  surf->basePoints.poly_num_vertices  = g_realloc(surf->basePoints.poly_num_vertices,
						  surf->basePoints.num_polys *
						  sizeof(int));
  surf->basePoints.poly_vertices      = g_realloc(surf->basePoints.poly_vertices,
						  surf->basePoints.num_polys *
						  sizeof(int*));

  alloc = (surf->basePoints.poly_points_data == (float**)0);
  surf->basePoints.poly_points_data   = g_realloc(surf->basePoints.poly_points_data,
                                                  surf->basePoints.num_points *
                                                  sizeof(float *));
  if (alloc)
    surf->basePoints.poly_points_data[0] = (float*)0;
  surf->basePoints.poly_points_data[0] = g_realloc(surf->basePoints.poly_points_data[0],
                                                   (VISU_SURFACES_POINTS_OFFSET_USER +
                                                    surf->basePoints.bufferSize) *
                                                   surf->basePoints.num_points *
                                                   sizeof(float));

  for(i = 0; i < surf->basePoints.num_points; i++)
    surf->basePoints.poly_points_data[i]   = surf->basePoints.poly_points_data[0] +
      i * (VISU_SURFACES_POINTS_OFFSET_USER + surf->basePoints.bufferSize);

  surf->ids                           = g_realloc(surf->ids,
						  surf->nsurf * sizeof(int));
  surf->resources                     = g_realloc(surf->resources,
						  surf->nsurf * sizeof(VisuSurfacesResources*));

  /* Dealing with properties reallocation. */
  g_hash_table_foreach(surf->properties, propertiesReallocateSurf,
		       GINT_TO_POINTER(surf->nsurf));
}
static gboolean visu_surfaces_setBox(VisuBoxed *self, VisuBox *box, gboolean fit)
{
  VisuSurfaces *surf;
  float trans[3][3], boxToXYZ[3][3], XYZToOldBox[3][3];
  double boxToXYZd[3][3], XYZToOldBoxd[3][3];

  g_return_val_if_fail(VISU_IS_SURFACES_TYPE(self), FALSE);
  surf = VISU_SURFACES(self);

  DBG_fprintf(stderr, "Isosurfaces: change the current box to fit to %p.\n",
	      (gpointer)box);
  if (surf->box == box)
    return FALSE;

  if (fit && surf->box)
    {
      visu_box_getInvMatrix(surf->box, XYZToOldBoxd);
      tool_matrix_dtof(XYZToOldBox, XYZToOldBoxd);
      visu_box_getCellMatrix(box, boxToXYZd);
      tool_matrix_dtof(boxToXYZ, boxToXYZd);

      tool_matrix_productMatrix(trans, boxToXYZ, XYZToOldBox);

      visu_surfaces_points_transform(&surf->basePoints, trans);
      visu_surfaces_points_transform(&surf->volatilePlanes, trans);
    }

  if (surf->box)
    g_object_unref(surf->box);
  surf->box = box;
  if (box)
    g_object_ref(box);

  return TRUE;
}
static VisuBox* visu_surfaces_getBox(VisuBoxed *self)
{
  g_return_val_if_fail(VISU_IS_SURFACES_TYPE(self), (VisuBox*)0);

  DBG_fprintf(stderr, "VisuSurfaces: get box.\n");
  return VISU_SURFACES(self)->box;
}
static void freeVisuSurfacesProperties(gpointer data)
{
  VisuSurfacesProperties *prop;

  prop = (VisuSurfacesProperties*)data;
  g_free(prop->name);
  g_free(prop->data);
  g_free(prop);
}
static void propertiesRemoveSurf(gpointer key _U_, gpointer value, gpointer data)
{
  guint idSurf, i;
  VisuSurfacesProperties *prop;

  idSurf = GPOINTER_TO_INT(data);
  prop = (VisuSurfacesProperties*)value;

  switch (prop->type)
    {
    case G_TYPE_FLOAT:
      for (i = idSurf; i < prop->surf->nsurf; i++)
	((float*)prop->data)[i] = ((float*)prop->data)[i + 1];
      prop->data = g_realloc(prop->data, prop->surf->nsurf * sizeof(float));
      break;
    default:
      g_warning("Unimplemented format.");
    }
}
static void propertiesReallocateSurf(gpointer key _U_, gpointer value, gpointer data)
{
  int nsurf;
  VisuSurfacesProperties *prop;

  nsurf = GPOINTER_TO_INT(data);
  prop = (VisuSurfacesProperties*)value;

  DBG_fprintf(stderr, "Visu Surfaces: reallocating for %d surfaces prop '%s'.\n", nsurf,
              (gchar*)key);
  switch (prop->type)
    {
    case G_TYPE_FLOAT:
      prop->data = g_realloc(prop->data, nsurf * sizeof(float));
      break;
    default:
      g_warning("Unimplemented format.");
    }
}
/* Dealing with properties as floats. */
/**
 * visu_surfaces_addPropertyFloat:
 * @surf: a #VisuSurfaces object ;
 * @name: the name of the property to add.
 *
 * Some properties can be associated to the surfaces stored in @surf.
 * This method is add a new property.
 *
 * Returns: a newly allocated array that can be populated.
 */
float* visu_surfaces_addPropertyFloat(VisuSurfaces *surf, const gchar* name)
{
  float *data;
  VisuSurfacesProperties *prop;

  g_return_val_if_fail(surf,            (float*)0);
  g_return_val_if_fail(name && name[0], (float*)0);

  DBG_fprintf(stderr, "Visu Surfaces: add a new float prop '%s'.\n", name);
  prop = g_malloc(sizeof(VisuSurfacesProperties));
  prop->name = g_strdup(name);
  prop->type = G_TYPE_FLOAT;
  prop->surf = surf;
  if (surf->nsurf > 0)
    data = g_malloc(sizeof(float) * surf->nsurf);
  else
    data = (gpointer)0;
  prop->data = (gpointer)data;
  g_hash_table_insert(surf->properties, (gpointer)prop->name, (gpointer)prop);
  return data;
}
gboolean visu_surfaces_addPropertyFloatValue(VisuSurfaces *surf, int idSurf,
					   const gchar* name, float value)
{
  float* data;
  int id;
  VisuSurfacesProperties *prop;

  g_return_val_if_fail(surf, FALSE);
  
  id = visu_surfaces_getPosition(surf, idSurf);
  g_return_val_if_fail(id >= 0 && (guint)id < surf->nsurf, FALSE);

  prop = (VisuSurfacesProperties*)g_hash_table_lookup(surf->properties, name);
  if (!prop)
    return FALSE;
  g_return_val_if_fail(prop->surf != surf, FALSE);

  data = (float*)prop->data;
  data[id] = value;
  return TRUE;
}
/**
 * visu_surfaces_getPropertyValueFloat:
 * @surf: a #VisuSurfaces object ;
 * @idSurf: a surface number ;
 * @name: the name of the property to get the value from ;
 * @value: a location to store the value.
 *
 * This method retrieves a float value stored as a property called @name for
 * the surface defined by its number @idSurf.
 *
 * Returns: TRUE if a value is indeed found.
*/
gboolean visu_surfaces_getPropertyValueFloat(VisuSurfaces *surf, int idSurf,
					   const gchar *name, float *value)
{
  float* data;
  int id;
  VisuSurfacesProperties *prop;

  g_return_val_if_fail(surf && value, FALSE);
  
  id = visu_surfaces_getPosition(surf, idSurf);
  g_return_val_if_fail(id >= 0 && (guint)id < surf->nsurf, FALSE);

  prop = (VisuSurfacesProperties*)g_hash_table_lookup(surf->properties, name);
  if (!prop)
    return FALSE;
  g_return_val_if_fail(prop->surf != surf, FALSE);

  data = (float*)prop->data;
  *value = data[id];
  return TRUE;
}
/**
 * visu_surfaces_getPropertyFloat:
 * @surf: a #VisuSurfaces object ;
 * @name: the name of the property to look for.
 *
 * Some properties can be associated to the surfaces stored in @surf.
 * This method is used to retrieve floating point values properties.
 *
 * Returns: a table with the values if the property is found, NULL
 *            otherwise.
 */
float* visu_surfaces_getPropertyFloat(VisuSurfaces *surf, const gchar *name)
{
  VisuSurfacesProperties *prop;

  g_return_val_if_fail(surf, (float*)0);

  prop = (VisuSurfacesProperties*)g_hash_table_lookup(surf->properties, name);
  if (prop)
    return (float*)prop->data;
  else
    return (float*)0;
}

/**
 * visu_surfaces_loadFile:
 * @file: target file to load ;
 * @surf: (out) (transfer full): a set of surfaces (location) ;
 * @error: a location to store errors.
 *
 * This loads a surface file and set default material properties for it.
 * See surf file specifications.
 *
 * Returns: (skip): TRUE in case of success, FALSE otherwise. Even in case of success
 *          @error may have been set.
 *
 */
gboolean visu_surfaces_loadFile(const char *file, VisuSurfaces **surf, GError **error)
{
  GIOChannel* surf_file;
  int line_number = 0;
  double dxx=0, dyx=0, dyy=0;
  double dzx=0, dzy=0, dzz=0;
  double geometry[VISU_BOX_N_VECTORS];
  int file_nsurfs=0, file_npolys=0, file_npoints=0;
  int npolys_loaded=0, npoints_loaded=0;
  guint i;
  int j,k, res;
  int sum_polys=0, sum_points=0;
  GIOStatus io_status;
  GString *current_line;
  float *densityData, densityValue, *fval;

  g_return_val_if_fail(surf, FALSE);
  g_return_val_if_fail(error, FALSE);

  DBG_fprintf(stderr, "Isosurfaces: trying to load %s file.\n", file);

  *surf = (VisuSurfaces*)0;

  current_line = g_string_new("");

  *error = (GError*)0;
  surf_file = g_io_channel_new_file(file, "r", error);
  if(!surf_file)
    return FALSE;

  /*   DBG_fprintf(stderr, "File opened\n"); */
  *error = (GError*)0;
  io_status = g_io_channel_read_line_string(surf_file, current_line, NULL, error);
  if(io_status != G_IO_STATUS_NORMAL)
    {
      g_string_free(current_line, TRUE);
      g_io_channel_unref(surf_file);
      return FALSE;
    }
  line_number++;  
  /*   DBG_fprintf(stderr, "Line %d read successfully\n", line_number); */

  *surf = visu_surfaces_new(0);

  *error = (GError*)0;
  io_status = g_io_channel_read_line_string(surf_file, current_line, NULL, error);
  if(io_status != G_IO_STATUS_NORMAL)
    {
      g_string_free(current_line, TRUE);
      g_io_channel_unref(surf_file);
      g_object_unref(*surf);
      *surf = (VisuSurfaces*)0;
      return FALSE;
    }
  line_number++;
  /*   DBG_fprintf(stderr, "Line %d read successfully\n", line_number); */
  if(sscanf(current_line->str, "%lf %lf %lf", &dxx, &dyx, &dyy) != 3)
    {
      *error = g_error_new(VISU_ERROR_ISOSURFACES,
			   VISU_ERROR_FORMAT,
			   _("Line %d doesn't match the [float, float, float]"
			     " pattern."), line_number);
      g_string_free(current_line, TRUE);
      g_io_channel_unref(surf_file);
      g_object_unref(*surf);
      *surf = (VisuSurfaces*)0;
      return FALSE;
    }

  *error = (GError*)0;
  io_status = g_io_channel_read_line_string(surf_file, current_line, NULL, error);
  if(io_status != G_IO_STATUS_NORMAL)
    {
      g_string_free(current_line, TRUE);
      g_io_channel_unref(surf_file);
      g_object_unref(*surf);
      *surf = (VisuSurfaces*)0;
      return FALSE;
    }
  line_number++;
  /*   DBG_fprintf(stderr, "Line %d read successfully\n", line_number); */
  if(sscanf(current_line->str, "%lf %lf %lf", &dzx, &dzy, &dzz) != 3)
    {
      *error = g_error_new(VISU_ERROR_ISOSURFACES,
			   VISU_ERROR_FORMAT,
			   _("Line %d doesn't match the [float, float, float]"
			     " pattern."), line_number);
      g_string_free(current_line, TRUE);
      g_io_channel_unref(surf_file);
      g_object_unref(*surf);
      *surf = (VisuSurfaces*)0;
      return FALSE;
    }
      
  *error = (GError*)0;
  io_status = g_io_channel_read_line_string(surf_file, current_line, NULL, error);
  if(io_status != G_IO_STATUS_NORMAL)
    {
      g_string_free(current_line, TRUE);
      g_io_channel_unref(surf_file);
      g_object_unref(*surf);
      *surf = (VisuSurfaces*)0;
      return FALSE;
    }
  line_number++;
  /*   DBG_fprintf(stderr, "Line %d read successfully\n", line_number); */
  res = sscanf(current_line->str, "%d %d %d", &file_nsurfs, &file_npolys, &file_npoints);
  if(res != 3 || file_nsurfs <= 0 || file_npolys <= 0 || file_npoints <= 0)
    {
      *error = g_error_new(VISU_ERROR_ISOSURFACES,
			   VISU_ERROR_FORMAT,
			   _("Line %d doesn't match the [int > 0, int > 0, int > 0]"
			     " pattern."), line_number);
      g_string_free(current_line, TRUE);
      g_io_channel_unref(surf_file);
      g_object_unref(*surf);
      *surf = (VisuSurfaces*)0;
      return FALSE;
    }
  /* From now on, the file is supposed to be a valid surface file. */
  (*surf)->nsurf = file_nsurfs;
  geometry[VISU_BOX_DXX] = dxx;
  geometry[VISU_BOX_DYX] = dyx;
  geometry[VISU_BOX_DYY] = dyy;
  geometry[VISU_BOX_DZX] = dzx;
  geometry[VISU_BOX_DZY] = dzy;
  geometry[VISU_BOX_DZZ] = dzz;
  (*surf)->box = visu_box_new(geometry, VISU_BOX_PERIODIC);
  visu_box_setMargin((*surf)->box, 0, FALSE);

  /* Allocate storage arrays. */
  visu_surfaces_allocate(*surf, file_nsurfs, file_npolys, file_npoints);

  /* Create a table to store the density values. */
  densityData = visu_surfaces_addPropertyFloat(*surf, VISU_SURFACES_PROPERTY_POTENTIAL);

  /* For each surf */
  for(i = 0; i < (*surf)->nsurf; i++) 
    {
      int surf_npolys=0, surf_npoints=0;
      
      densityData[i] = 0.;
      (*surf)->ids[i] = i;
      /* Allow some commentaries here begining with a '#' character. */
      do
	{
	  /* Get the first line that should contains current surf' name */
	  *error = (GError*)0;
	  io_status = g_io_channel_read_line_string(surf_file, current_line, NULL, error);
	  if(io_status != G_IO_STATUS_NORMAL)
	    {
	      g_string_free(current_line, TRUE);
	      g_io_channel_unref(surf_file);
	      g_object_unref(*surf);
	      *surf = (VisuSurfaces*)0;
	      return TRUE;
	    }
	  line_number++;

	  /* If the line begins with a '#', then we try to find a density value. */
	  if (current_line->str[0] == '#')
	    {
	      res = sscanf(current_line->str, ISOSURFACES_FLAG_POTENTIAL" %f", &densityValue);
	      if (res == 1)
		densityData[i] = densityValue;
	    }
	}
      while (current_line->str[0] == '#');
      g_strdelimit(current_line->str, "\n", ' ');
      g_strstrip(current_line->str);
      (*surf)->resources[i] = visu_surfaces_resources_getFromName(current_line->str, (gboolean*)0);
      DBG_fprintf(stderr, "Line %d ('%s' -> %p) read successfully %f\n",
		  line_number, current_line->str, (gpointer)(*surf)->resources[i],
		  densityData[i]);

      *error = (GError*)0;
      io_status = g_io_channel_read_line_string(surf_file, current_line, NULL, error);
      if(io_status != G_IO_STATUS_NORMAL)
	{
	  g_string_free(current_line, TRUE);
	  g_io_channel_unref(surf_file);
	  g_object_unref(*surf);
	  *surf = (VisuSurfaces*)0;
	  return TRUE;
	}
      line_number++;
      /*        DBG_fprintf(stderr, "Line %d read successfully\n", line_number); */
      res = sscanf(current_line->str, "%d %d", &surf_npolys, &surf_npoints);
      if(res != 2 || surf_npolys <= 0 || surf_npoints <= 0)
	{
	  *error = g_error_new(VISU_ERROR_ISOSURFACES,
			       VISU_ERROR_FORMAT,
			       _("Line %d doesn't match the [int > 0, int > 0]"
				 " pattern."), line_number);
	  g_string_free(current_line, TRUE);
	  g_io_channel_unref(surf_file);
	  g_object_unref(*surf);
	  *surf = (VisuSurfaces*)0;
	  return TRUE;
	}

      sum_polys += surf_npolys;
      if(sum_polys > file_npolys)
	{
	  *error = g_error_new(VISU_ERROR_ISOSURFACES,
			       VISU_ERROR_CHECKSUM,
			       _("Error on line %d. Declared number of polygons"
				 " reached."), line_number);
	  g_string_free(current_line, TRUE);
	  g_io_channel_unref(surf_file);
	  g_object_unref(*surf);
	  *surf = (VisuSurfaces*)0;
	  return TRUE;
	}

      sum_points += surf_npoints;
      if(sum_points > file_npoints)
	{
	  *error = g_error_new(VISU_ERROR_ISOSURFACES,
			       VISU_ERROR_CHECKSUM,
			       _("Error on line %d. Declared number of points"
				 " reached."), line_number);
	  g_string_free(current_line, TRUE);
	  g_io_channel_unref(surf_file);
	  g_object_unref(*surf);
	  *surf = (VisuSurfaces*)0;
	  return TRUE;
	}

      for(j = 0; j < surf_npolys; j++)
	{
	  int nvertex=0, nvertex_i=0;

	  gchar **split_line;
	   
	  *error = (GError*)0;
	  io_status = g_io_channel_read_line_string(surf_file, current_line, NULL, error);
	  if(io_status != G_IO_STATUS_NORMAL)
	    {
	      g_string_free(current_line, TRUE);
	      g_io_channel_unref(surf_file);
	      g_object_unref(*surf);
	      *surf = (VisuSurfaces*)0;
	      return TRUE;
	    }
	  line_number++;

	  split_line = g_strsplit_set(current_line->str, " ", -1);

	  for(k = 0; split_line[k] != NULL; k++)
	    {
	      if(g_ascii_strcasecmp(split_line[k], "") == 0) 
		continue;	       
	      if(nvertex == 0)
		{
		  if(sscanf(split_line[k], "%d", &nvertex) != 1 || nvertex < 3)
		    {
		      *error = g_error_new(VISU_ERROR_ISOSURFACES,
					   VISU_ERROR_FORMAT,
					   _("Line %dmust begin by an int."),
					   line_number);
		      g_string_free(current_line, TRUE);
		      g_io_channel_unref(surf_file);
		      g_object_unref(*surf);
                      *surf = (VisuSurfaces*)0;
		      return TRUE;
		    }
		  (*surf)->basePoints.poly_num_vertices[npolys_loaded] = nvertex;
		  (*surf)->basePoints.poly_vertices[npolys_loaded] = g_malloc(nvertex * sizeof(double));
		  (*surf)->basePoints.poly_surf_index[npolys_loaded] = i + 1;
		  (*surf)->basePoints.num_polys_surf[i] += 1;
		  npolys_loaded++;
		  continue;
		}
	      res = sscanf(split_line[k], "%u", &(*surf)->basePoints.poly_vertices[npolys_loaded-1][nvertex_i]);
	      if(res != 1)
		{
		  *error = g_error_new(VISU_ERROR_ISOSURFACES,
				       VISU_ERROR_FORMAT,
				       _("Line %d doesn't match the [int > 3, ...]"
					 " required pattern."), line_number);
		  g_string_free(current_line, TRUE);
		  g_io_channel_unref(surf_file);
		  g_object_unref(*surf);
                  *surf = (VisuSurfaces*)0;
                  return TRUE;
		}
	      (*surf)->basePoints.poly_vertices[npolys_loaded-1][nvertex_i] += -1 + npoints_loaded;
	      nvertex_i++;
	      if(nvertex_i >= nvertex)
		break;
	    }

	  g_strfreev(split_line);
	}
 
      for(j = 0; j < surf_npoints; j++) 
	{
	  *error = (GError*)0;
	  io_status = g_io_channel_read_line_string(surf_file, current_line, NULL, error);
	  if(io_status != G_IO_STATUS_NORMAL)
	    {
	      g_string_free(current_line, TRUE);
	      g_io_channel_unref(surf_file);
	      g_object_unref(*surf);
	      *surf = (VisuSurfaces*)0;
	      return TRUE;
	    }
	  line_number++;

	  fval = (*surf)->basePoints.poly_points_data[npoints_loaded];
	  if(sscanf(current_line->str, "%f %f %f %f %f %f", 
		    fval, fval + 1, fval + 2,
		    fval + VISU_SURFACES_POINTS_OFFSET_NORMAL,
		    fval + VISU_SURFACES_POINTS_OFFSET_NORMAL + 1,
		    fval + VISU_SURFACES_POINTS_OFFSET_NORMAL + 2) != 6) 
	    {
	      *error = g_error_new(VISU_ERROR_ISOSURFACES,
				   VISU_ERROR_FORMAT,
				   _("Line %d doesn't match the [float x 6]"
				     " required pattern."), line_number);
	      g_string_free(current_line, TRUE);
	      g_io_channel_unref(surf_file);
	      g_object_unref(*surf);
	      *surf = (VisuSurfaces*)0;
	      return TRUE;
	    }
	  npoints_loaded++;
	}
    }

  g_string_free(current_line, TRUE);
  g_io_channel_unref(surf_file);

  return TRUE;
}  
/**
 * visu_surfaces_setShowAll:
 * @surf: a #VisuSurfaces object ;
 * @show: TRUE to show all surfaces, FALSE to hide them.
 * 
 * Shows or hides all surfaces and check their "draw" status in the panel accordingly.
 */
void visu_surfaces_setShowAll(VisuSurfaces *surf, gboolean show)
{
  guint i;

  g_return_if_fail(surf);

  for (i = 0; i < surf->nsurf; i++)
    surf->resources[i]->rendered = show;
}
/**
 * visu_surfaces_hide:
 * @surf: a #VisuSurfaces object ;
 * @planes: an array of planes (NULL terminated).
 *
 * Change the visibility of polygons stored in @surf, following the masking
 * scheme defined by the given list of @planes (see #VisuPlane).
 *
 * Returns: TRUE if the surfaces visibility status have been changed.
 */
gboolean visu_surfaces_hide(VisuSurfaces *surf, VisuPlane **planes)
{
  guint i, j, iv;
  int k, iPoly, isurf, iVertice, n;
  gboolean *verticeStatus;
  gboolean visibility, redraw, boundary, valid;
  int *boundaryPolys;
  guint nBounadryPolys;
  int npolys, npoints;
  VisuSurfacesPoints *ptFr, *ptTo;

  g_return_val_if_fail(surf, FALSE);

  DBG_fprintf(stderr, "Isosurfaces: compute masked polygons"
	      " for surface %p.\n", (gpointer)surf);
  /* Free previous volatile points. */
  visu_surfaces_points_free(&surf->volatilePlanes);

  verticeStatus = g_malloc(sizeof(gboolean) * surf->basePoints.num_points);
  redraw = FALSE;
  /* Compute a visibility flag for each vertice. */
  for (i = 0; i < surf->basePoints.num_points; i++)
    verticeStatus[i] =
      visu_plane_class_getVisibility(planes, surf->basePoints.poly_points_data[i]);
  /* We store the id of boundary polygons. */
  boundaryPolys = g_malloc(sizeof(int) * surf->basePoints.num_polys);
  nBounadryPolys = 0;
  /* Hide polygons. */
  for (i = 0; i < surf->basePoints.num_polys; i++)
    {
      visibility = TRUE;
      boundary = FALSE;
      if (surf->resources[ABS(surf->basePoints.poly_surf_index[i]) - 1]->sensitiveToPlanes)
	{
	  for (j = 0; j < surf->basePoints.poly_num_vertices[i]; j++)
	    {
	      visibility = visibility && verticeStatus[surf->basePoints.poly_vertices[i][j]];
	      boundary = boundary || verticeStatus[surf->basePoints.poly_vertices[i][j]];
	    }
	  boundary = !visibility && boundary;
	}
      if (!visibility && surf->basePoints.poly_surf_index[i] > 0)
	{
	  /* Hide this polygon. */
	  surf->basePoints.num_polys_surf[surf->basePoints.poly_surf_index[i] - 1] -= 1;
	  surf->basePoints.poly_surf_index[i] = -surf->basePoints.poly_surf_index[i];
	  redraw = TRUE;
	}
      else if (visibility && surf->basePoints.poly_surf_index[i] < 0)
	{
	  /* Show this polygon. */
	  surf->basePoints.poly_surf_index[i] = -surf->basePoints.poly_surf_index[i];
	  surf->basePoints.num_polys_surf[surf->basePoints.poly_surf_index[i] - 1] += 1;
	  redraw = TRUE;
	}
      if (boundary)
	boundaryPolys[nBounadryPolys++] = i;
    }
  if (DEBUG)
    for (i = 0; i < surf->nsurf; i++)
      fprintf(stderr, " | surface %2d -> %7d polygons\n", i, surf->basePoints.num_polys_surf[i]);
  /* We count the number of boundaries per surface and allocate
     accordingly the volatile. */
  npolys  = nBounadryPolys;
  npoints = 0;
  for (i = 0; i < nBounadryPolys; i++)
    {
      /* The number of points to add is the number of visible points plus 2. */
      npoints += 2;
      for (j = 0; j < surf->basePoints.poly_num_vertices[boundaryPolys[i]]; j++)
	if (verticeStatus[surf->basePoints.poly_vertices[boundaryPolys[i]][j]])
	  npoints += 1;
    }
  DBG_fprintf(stderr, "Isosurfaces: volatile polygons.\n");
  DBG_fprintf(stderr, " | polys %d, points %d\n", npolys, npoints);
  visu_surfaces_points_allocate(&surf->volatilePlanes, surf->nsurf, npolys, npoints);
  surf->volatilePlanes.num_polys  = 0;
  surf->volatilePlanes.num_points = 0;
  /* We copy the polygons and the vertices in the volatile part. */
  for (i = 0; i < nBounadryPolys; i++)
    {
      ptFr = &surf->basePoints;
      isurf = -ptFr->poly_surf_index[boundaryPolys[i]] - 1;
      ptTo = &surf->volatilePlanes;
      iPoly = ptTo->num_polys;
      iVertice = ptTo->num_points;

      ptTo->num_polys_surf[isurf] += 1;
      ptTo->poly_surf_index[iPoly] = (int)isurf + 1;
      n = 2;
      for (j = 0; j < ptFr->poly_num_vertices[boundaryPolys[i]]; j++)
	if (verticeStatus[ptFr->poly_vertices[boundaryPolys[i]][j]])
	  n += 1;
      ptTo->poly_num_vertices[iPoly] = n;
      ptTo->poly_vertices[iPoly] = g_malloc(sizeof(int) * n);
      n = ptFr->poly_num_vertices[boundaryPolys[i]];
      /* Compute new vertex. */
      iv = 0;
      for (k = 0; k < n; k++)
	{
	  /* If vertex is visible, we put it. */
	  if (verticeStatus[ptFr->poly_vertices[boundaryPolys[i]][k]])
	    {
	      memcpy(ptTo->poly_points_data[iVertice],
		     ptFr->poly_points_data[ptFr->poly_vertices[boundaryPolys[i]][k]],
		     sizeof(float) * (VISU_SURFACES_POINTS_OFFSET_USER + ptTo->bufferSize));
	      g_return_val_if_fail(iv < ptTo->poly_num_vertices[iPoly], redraw);
	      ptTo->poly_vertices[iPoly][iv] = iVertice;
	      iVertice += 1;
	      iv += 1;
	    }
	  /* If next vertex brings a visibility change, we compute intersection. */
	  if ((!verticeStatus[ptFr->poly_vertices[boundaryPolys[i]][k]] &&
	       verticeStatus[ptFr->poly_vertices[boundaryPolys[i]][(k + 1)%n]]))
	    {
	      /* We take the previous and compute the intersection. */
	      valid = visu_plane_class_getIntersection
		(planes,
		 ptFr->poly_points_data[ptFr->poly_vertices[boundaryPolys[i]][(k + 1)%n]],
		 ptFr->poly_points_data[ptFr->poly_vertices[boundaryPolys[i]][k]],
		 ptTo->poly_points_data[iVertice], TRUE);
	      g_return_val_if_fail(valid, redraw);
	      /* Other values than coordinates are copied. */
	      memcpy(ptTo->poly_points_data[iVertice] + 3,
		     ptFr->poly_points_data[ptFr->poly_vertices[boundaryPolys[i]][(k + 1)%n]] + 3,
		     sizeof(float) * (VISU_SURFACES_POINTS_OFFSET_USER + ptTo->bufferSize - 3));
	      g_return_val_if_fail(iv < ptTo->poly_num_vertices[iPoly], redraw);
	      ptTo->poly_vertices[iPoly][iv] = iVertice;
	      iVertice += 1;
	      iv += 1;
	    }
	  if ((verticeStatus[ptFr->poly_vertices[boundaryPolys[i]][k]] &&
	       !verticeStatus[ptFr->poly_vertices[boundaryPolys[i]][(k + 1)%n]]))
	    {
	      /* We take the previous and compute the intersection. */
	      valid = visu_plane_class_getIntersection
		(planes, ptFr->poly_points_data[ptFr->poly_vertices[boundaryPolys[i]][k]],
		 ptFr->poly_points_data[ptFr->poly_vertices[boundaryPolys[i]][(k + 1)%n]],
		 ptTo->poly_points_data[iVertice], TRUE);
	      g_return_val_if_fail(valid, redraw);
	      memcpy(ptTo->poly_points_data[iVertice] + 3,
		     ptFr->poly_points_data[ptFr->poly_vertices[boundaryPolys[i]][k]] + 3,
		     sizeof(float) * (VISU_SURFACES_POINTS_OFFSET_USER + ptTo->bufferSize - 3));
	      g_return_val_if_fail(iv < ptTo->poly_num_vertices[iPoly], redraw);
	      ptTo->poly_vertices[iPoly][iv] = iVertice;
	      iVertice += 1;
	      iv += 1;
	    }
	}
      ptTo->num_polys += 1;
      ptTo->num_points = iVertice;
    }
  g_free(verticeStatus);
  g_free(boundaryPolys);

#if DEBUG == 1
  visu_surfaces_checkConsistency(surf);
#endif
  if (redraw)
    g_signal_emit(G_OBJECT(surf), surfaces_signals[SURFACE_MASKED_SIGNAL], 0, NULL);

  return redraw;
}

/**
 * visu_surfaces_getN:
 * @surf: the surface object.
 *
 * Retrieves th number of surfaces stired in a given @surf object.
 *
 * Returns: number of surfaces.
 */
guint visu_surfaces_getN(VisuSurfaces *surf)
{
  if (!surf)
    return 0;

  return surf->nsurf;
}
/**
 * visu_surfaces_getName:
 * @surf: the surface object ;
 * @surf_index: the number of the surface.
 * 
 * This returns for the given @surf_index its name 
 * (1 <= surf_index <= surfaces_number) 
 *
 * Returns: the name of the surface or empty name or NULL, if @surf_index is invalid.
 */
const gchar* visu_surfaces_getName(VisuSurfaces *surf, int surf_index)
{
  int id;

  g_return_val_if_fail(surf, (gchar*)0);

  id = visu_surfaces_getPosition(surf, surf_index);
  g_return_val_if_fail(id >= 0 && (guint)id < surf->nsurf, (gchar*)0);

/*   fprintf(stderr, "%d %d %p '%s'\n", surf_index, id, (gpointer)surf->resources[id], surf->resources[id]->surfnom); */
  return surf->resources[id]->surfnom;
}
/**
 * visu_surfaces_getRendered:
 * @surf: the surface object ;
 * @surf_index: the number of the surface.
 * 
 * This returns for the given @surf_index its visibility.
 *
 * Returns: the visibility of the surface or FALSE, if @surf_index is invalid.
 */
gboolean visu_surfaces_getRendered(VisuSurfaces *surf, int surf_index)
{
  int id;

  g_return_val_if_fail(surf, FALSE);

  id = visu_surfaces_getPosition(surf, surf_index);
  g_return_val_if_fail(id >= 0 && (guint)id < surf->nsurf, FALSE);

  return surf->resources[id]->rendered;
}
/**
 * visu_surfaces_setRendered:
 * @surf: the surface object ;
 * @surf_index: the number of the surface.
 * @status: a boolean.
 * 
 * Change the visibility status of surface @surf_index.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the visibility has been indeed changed.
 */
gboolean visu_surfaces_setRendered(VisuSurfaces *surf, int surf_index, gboolean status)
{
  guint id;

  id = visu_surfaces_getPosition(surf, surf_index);
  g_return_val_if_fail(VISU_IS_SURFACES_TYPE(surf) && id < surf->nsurf, FALSE);

  if (surf->resources[id]->rendered == status)
    return FALSE;

  surf->resources[id]->rendered = status;
  g_signal_emit(G_OBJECT(surf), surfaces_signals[SURFACE_RENDERING_SIGNAL], 0, surf_index, NULL);
  return TRUE;
}
/**
 * visu_surfaces_setColorAndMaterial:
 * @surf: the surface object ;
 * @surf_index: the number of the surface.
 * @color: (transfer none): a #ToolColor object.
 * @material: (array fixed-size=5): the material values.
 * 
 * Update the color and material property of given surface.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the values are differents.
 */
gboolean visu_surfaces_setColorAndMaterial(VisuSurfaces *surf, int surf_index,
                                           ToolColor *color,
                                           float material[VISU_GL_LIGHT_MATERIAL_N_VALUES])
{
  guint id, i;

  id = visu_surfaces_getPosition(surf, surf_index);
  g_return_val_if_fail(VISU_IS_SURFACES_TYPE(surf) && id < surf->nsurf, FALSE);

  if (tool_color_equal(surf->resources[id]->color, color) &&
      surf->resources[id]->material[0] == material[0] &&
      surf->resources[id]->material[1] == material[1] &&
      surf->resources[id]->material[2] == material[2] &&
      surf->resources[id]->material[3] == material[3] &&
      surf->resources[id]->material[4] == material[4])
    return FALSE;

  for (i = 0; i < 4; i++)
    surf->resources[id]->color->rgba[i] = color->rgba[i];
  for (i = 0; i < VISU_GL_LIGHT_MATERIAL_N_VALUES; i++)
    surf->resources[id]->material[i] = material[i];

  g_signal_emit(G_OBJECT(surf), surfaces_signals[SURFACE_RENDERING_SIGNAL], 0, surf_index, NULL);
  return TRUE;
}

/**
 * visu_surfaces_getResource:
 * @surf: the surface object ;
 * @i: the ith stored surface.
 * 
 * This returns the resource of the ith stored surface. If the surface
 * is known by its id, use visu_surfaces_getResourceById() instead.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): the resource of the surface or NULL, if
 * @i is invalid.
 */
VisuSurfacesResources* visu_surfaces_getResource(VisuSurfaces *surf, guint i)
{
  g_return_val_if_fail(VISU_IS_SURFACES_TYPE(surf), (VisuSurfacesResources*)0);
  g_return_val_if_fail(i < surf->nsurf, (VisuSurfacesResources*)0);

  return surf->resources[i];
}
/**
 * visu_surfaces_getResourceById:
 * @surf: the surface object ;
 * @surf_index: the id of the surface.
 * 
 * This returns for the given @surf_index its resource information.
 *
 * Returns: (transfer none): the resource of the surface or NULL, if
 * @surf_index is invalid.
 */
VisuSurfacesResources* visu_surfaces_getResourceById(VisuSurfaces *surf, int surf_index)
{
  int id;

  g_return_val_if_fail(surf, (VisuSurfacesResources*)0);

  id = visu_surfaces_getPosition(surf, surf_index);
  g_return_val_if_fail(id >= 0 && (guint)id < surf->nsurf, (VisuSurfacesResources*)0);

/*   fprintf(stderr, "%d %p\n", id, (gpointer)surf->resources); */
  return surf->resources[id];
}
/**
 * visu_surfaces_setResource:
 * @surf: the surface object ;
 * @surf_index: the number of the surface ;
 * @res: the new resource.
 * 
 * This method is used to change the resource of a surface.
 */
void visu_surfaces_setResource(VisuSurfaces *surf, int surf_index, VisuSurfacesResources *res)
{
  int id;

  g_return_if_fail(surf && res);

  id = visu_surfaces_getPosition(surf, surf_index);
  g_return_if_fail(id >= 0 && (guint)id < surf->nsurf);

  if (!surf->resources[id]->surfnom)
    visu_surfaces_resources_free(surf->resources[id]);
  DBG_fprintf(stderr, "VisuSurfaces: set resource %p (%s) to surface %d (%d).\n",
	      (gpointer)res, res->surfnom, surf_index, id);
  surf->resources[id] = res;

  g_signal_emit(G_OBJECT(surf), surfaces_signals[SURFACE_RENDERING_SIGNAL], 0, surf_index, NULL);
}
/**
 * visu_surfaces_getId:
 * @surf: the surface object ;
 * @i: the number of the surface.
 * 
 * This returns for the given @i its id information.
 *
 * Returns: the id of the surface or 0, if @i is invalid.
 */
int visu_surfaces_getId(VisuSurfaces *surf, int i)
{
  g_return_val_if_fail(i >= 0 && (guint)i < surf->nsurf, -1);
  
  return surf->ids[i];
}
/**
 * visu_surfaces_getSortedById:
 * @surf: the surface object.
 * 
 * This returns the surface numbers sorted using their ids.
 *
 * Returns: a newly allocated array with surface numbers.
 */
int* visu_surfaces_getSortedById(VisuSurfaces *surf)
{
  int *ids, tmp;
  guint i, j;

  g_return_val_if_fail(surf, (int*)0);

  ids = g_malloc(sizeof(int) * surf->nsurf);
  for (i = 0; i < surf->nsurf; i++)
    ids[i] = surf->ids[i];

  /* Idiotic algorithm to sort all surfaces. */
  for (i = 0; i < surf->nsurf; i++)
    for (j = 0; j < surf->nsurf; j++)
      if (ids[j] > ids[i])
        {
          tmp = ids[i];
          ids[i] = ids[j];
          ids[j] = tmp;
        }
  return ids;
}
/**
 * visu_surfaces_getNewId:
 * @surf: the surface object.
 * 
 * This returns a unique id to create a new surface.
 *
 * Returns: a value suitable to create a new surface in this set of surfaces.
 */
int visu_surfaces_getNewId(VisuSurfaces *surf)
{
  guint i;
  int id;
  
  if (!surf)
    return 0;
  
  id = -1;
  for (i = 0; i < surf->nsurf; i++)
    id = MAX(surf->ids[i], id);
  DBG_fprintf(stderr, "VisuSurfaces : get new id for surfaces %d.\n", id + 1);
  return id + 1;
}
/**
 * visu_surfaces_getPosition:
 * @surf: the surface object ;
 * @id: the id of the surface.
 * 
 * This returns for the given @id its number.
 *
 * Returns: the number of the surface or 0, if @id is invalid.
 */
int visu_surfaces_getPosition(VisuSurfaces *surf, int id)
{
  guint i;

  g_return_val_if_fail(surf, -1);

  for (i = 0; i < surf->nsurf; i++)
    if (surf->ids[i] == id)
      return i;

  g_warning("Unfound surface with id %d.", id);
  return -1;
}
