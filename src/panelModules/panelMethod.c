/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "panelMethod.h"

#include <support.h>
#include <visu_tools.h>
#include <visu_object.h>
#include <gtk_main.h>
#include <extraGtkFunctions/gtk_toolPanelWidget.h>

#include "gtkAtomic.h"
#include "gtkSpin.h"

/**
 * SECTION: panelMethod
 * @short_description: The tab where drawing method is chosen (atomic
 * or spin).
 *
 * <para>This tab has an area to display custom widgets according to
 * the selected rendering method, see visu_ui_panel_method_set().</para>
 */

/* Functions used to initialise the
 * specific area in the panel of rendering methods.
 */
ToolInitFunc listInitRendenringConfigGtkFunc[] = {
  visu_ui_panel_elements_atomic_initMethod,
  visu_ui_panel_elements_spin_initMethod,
  (ToolInitFunc)0};

/* Local variables. */
/* Store the toolpanel object, can be interesting to
   fetch the current visuData object for instance. */
static GtkWidget* panelMethod;
/* A flag to control if the GTK part of the panel
   has been initialated or not. */
static gboolean isPanelInitialised;
/* This is a hashtable to store the pointers to methods used
   to display some specific options of rendering methods.
   The key are pointer to valid RenderingMethod and the values
   are pointer to struct RenderingExpanderWidget_struct. */
static GHashTable *listOfRenderingSpecificConfigWidget;
/* Since C doesn't accept to (gpointer)-ize pointers to methods
   the hashtable can't store directly VisuUiNewWidgetFunc() so
   they are encapsulated in this struct. */
struct RenderingExpanderWidget_struct
{
  VisuUiNewWidgetFunc create;
  VisuUiPanelDestroyFunc destroy;
};
static VisuUiPanelDestroyFunc currentDestroyFunc;
/* Liststore to store the icon and the name of all rendering methods. */
static GtkListStore *listStoreRenderingMethods;
enum
  {
    COLUMN_ICON, /* A pixmap to represent the method */
    COLUMN_NAME,  /* The label shown */
    COLUMN_POINTER_TO_DATA,  /* Pointer to the rendering method. */
    COLUMN_POINTER_TO_WIDGETS, /* Pointer the above structure. */
    N_COLUMNS
  };

/* Widgets that can be accessed during the run. */
static GtkWidget *descrLabel;
static GtkWidget *comboMethod;
static GtkWidget *viewport;

/* Loacl methods. */
static GtkWidget* createTreeListRenderingMethod(int *iterSelected);
static void setRenderingSpecificWidget(struct RenderingExpanderWidget_struct *str);

/* Callbacks */
static void onMethodPanelEnter(VisuUiPanel *visu_ui_panel, gpointer data);
static void onRenderingChanged(GObject *obj, VisuRendering *meth, gpointer data);
static void comboInputMethodsChanged(GtkComboBox *combo, gpointer data);


VisuUiPanel* visu_ui_panel_method_init(VisuUiMain *ui _U_)
{
  char *cl = _("Rendering method");
  char *tl = _("Draw");
  int i;

  /* Now that everything has a value, we create the panel. */
  panelMethod = visu_ui_panel_newWithIconFromPath("Panel_method", cl, tl,
					      "stock-method_20.png");
  if (!panelMethod)
    return (VisuUiPanel*)0;
  visu_ui_panel_setDockable(VISU_UI_PANEL(panelMethod), TRUE);

  /* Initialisation of loacl variables. */
  isPanelInitialised = FALSE;
  currentDestroyFunc = (VisuUiPanelDestroyFunc)0;
  listOfRenderingSpecificConfigWidget = g_hash_table_new_full(g_direct_hash,
                                                              g_direct_equal,
                                                              NULL, g_free);

  /* Initialise the rendering specific widgets */
  for (i = 0; listInitRendenringConfigGtkFunc[i]; i++)
    listInitRendenringConfigGtkFunc[i]();

  /* Set global callbacks. */
  g_signal_connect(G_OBJECT(panelMethod), "page-entered",
		           G_CALLBACK(onMethodPanelEnter), (gpointer)0);

  return VISU_UI_PANEL(panelMethod);
}

static void onMethodPanelEnter(VisuUiPanel *visu_ui_panel _U_, gpointer data _U_)
{
  GtkWidget *label, *vbox, *align, *scrollView, *hbox;
  int iterSelected;
  
  /* On page enter, we just create the GTK objects if required. */
  if (isPanelInitialised)
    return;

  vbox = gtk_vbox_new(FALSE, 0);
  
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);
  
  label = gtk_label_new(_("<b>Input method:</b>"));
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_misc_set_padding(GTK_MISC(label), 5, 0);
  gtk_widget_set_name(label, "label_head");
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  
  comboMethod = createTreeListRenderingMethod(&iterSelected);
  gtk_box_pack_start(GTK_BOX(hbox), comboMethod, TRUE, TRUE, 0);
  
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);
  
  label = gtk_label_new(_("Description:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.);
  gtk_misc_set_padding(GTK_MISC(label), 15, 0);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

  descrLabel = gtk_label_new("");
  gtk_widget_set_name(descrLabel, "label_info");
  gtk_misc_set_alignment(GTK_MISC(descrLabel), 0.5, 0.5);
  gtk_misc_set_padding(GTK_MISC(descrLabel), 2, 0);
  gtk_box_pack_start(GTK_BOX(hbox), descrLabel, TRUE, TRUE, 0);
  gtk_widget_set_size_request(descrLabel, 250, -1);
  gtk_label_set_use_markup(GTK_LABEL(descrLabel), TRUE);
  gtk_label_set_line_wrap(GTK_LABEL(descrLabel), TRUE);
  gtk_label_set_justify(GTK_LABEL(descrLabel), GTK_JUSTIFY_FILL);
  
  align = gtk_alignment_new(0., 0.5, 1., 1.);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 10, 0, 0, 0);
  gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, FALSE, 0);
  
  label = gtk_label_new(_("<b>Options:</b>"));
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_misc_set_padding(GTK_MISC(label), 5, 0);
  gtk_widget_set_name(label, "label_head");
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_container_add(GTK_CONTAINER(align), label);

  scrollView = gtk_scrolled_window_new((GtkAdjustment*)0,
				       (GtkAdjustment*)0);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW (scrollView),
				                 GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW (scrollView), GTK_SHADOW_NONE);
  gtk_box_pack_start(GTK_BOX(vbox), scrollView, TRUE, TRUE, 2);

  viewport = gtk_viewport_new (NULL, NULL);
  gtk_container_add(GTK_CONTAINER(scrollView), viewport);
  
  gtk_widget_show_all(vbox);
  isPanelInitialised = TRUE;
  
  gtk_container_add(GTK_CONTAINER(panelMethod), vbox);
  
  /* Set callbacks. */
  g_signal_connect(G_OBJECT(comboMethod), "changed",
         		   G_CALLBACK(comboInputMethodsChanged), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "renderingChanged",
		   G_CALLBACK(onRenderingChanged), (gpointer)1);

  /* Set the default values. */
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboMethod), iterSelected);
}

static GtkWidget* createTreeListRenderingMethod(int *iterSelected)
{
  GtkTreeIter iter;
  GList *methodLst;
  GtkWidget *methodsWd;
  GtkCellRenderer *renderer;
  int i;
  VisuRendering *methodInUse, *meth;
  GdkPixbuf *pixbufIconBox;
  gpointer str;
  const gchar *iconPath;

  DBG_fprintf(stderr, "Panel Method: create the combobox of methods.\n");
  /* We create the structure that store the methods */
  listStoreRenderingMethods = gtk_list_store_new (N_COLUMNS,
						  GDK_TYPE_PIXBUF,
						  G_TYPE_STRING,
						  G_TYPE_POINTER,
						  G_TYPE_POINTER);
  *iterSelected = 0;
  i = 0;
  methodInUse = visu_object_getRendering(VISU_OBJECT_INSTANCE);
  for (methodLst = visu_rendering_getAllObjects();
       methodLst; methodLst = g_list_next(methodLst))
    {
      meth = (VisuRendering*)methodLst->data;
      DBG_fprintf(stderr, " | add method '%s'.\n", visu_rendering_getName(meth, TRUE));
      iconPath = visu_rendering_getIconPath(meth);
      if (iconPath)
	pixbufIconBox = gdk_pixbuf_new_from_file_at_size(iconPath,
							 16, 16, (GError**)0);
      else
	pixbufIconBox = (GdkPixbuf*)0;
      str = g_hash_table_lookup(listOfRenderingSpecificConfigWidget, methodLst->data);
      gtk_list_store_append(listStoreRenderingMethods, &iter);
      gtk_list_store_set(listStoreRenderingMethods, &iter,
			 COLUMN_ICON, pixbufIconBox,
			 COLUMN_NAME, visu_rendering_getName(meth, TRUE),
			 COLUMN_POINTER_TO_DATA, methodLst->data,
			 COLUMN_POINTER_TO_WIDGETS, str,
			 -1);
      if (meth == methodInUse)
	*iterSelected = i;
      i += 1;
    }
  /* We create the tree widget that show the methods. */
  methodsWd = gtk_combo_box_new_with_model(GTK_TREE_MODEL(listStoreRenderingMethods));
  renderer = gtk_cell_renderer_pixbuf_new();
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(methodsWd), renderer, FALSE);
  gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(methodsWd), renderer, "pixbuf", 0);
  renderer = gtk_cell_renderer_text_new();
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(methodsWd), renderer, FALSE);
  gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(methodsWd), renderer, "text", 1);

  return methodsWd;
}

static void comboInputMethodsChanged(GtkComboBox *combo, gpointer data _U_)
{
  GtkTreeModel *tree;
  gboolean selected;
  GtkTreeIter iter;
  VisuRendering *meth;
  char* markup;
  struct RenderingExpanderWidget_struct *str;

  DBG_fprintf(stderr, "Panel Method: Change occured on the method combo.\n");
  tree = gtk_combo_box_get_model(combo);
  selected = gtk_combo_box_get_active_iter(combo, &iter);
  if (selected)
    {
      gtk_tree_model_get(tree, &iter, COLUMN_POINTER_TO_DATA, &meth,
                         COLUMN_POINTER_TO_WIDGETS, &str, -1);
      g_return_if_fail(meth);

      DBG_fprintf(stderr, "Panel Method: set the rendering method to '%s'.\n",
                  visu_rendering_getName(meth, FALSE));
      markup = g_markup_printf_escaped("<span size=\"smaller\">%s</span>",
                                       visu_rendering_getDescription(meth));
      gtk_label_set_markup(GTK_LABEL(descrLabel), markup);
      g_free(markup);
      visu_object_setRendering(VISU_OBJECT_INSTANCE, meth);
      setRenderingSpecificWidget(str);
    }
  else
    {
      g_warning("Can't find the selected method in the"
		" combobox of the config panel.\n");
      gtk_label_set_text(GTK_LABEL(descrLabel), _("None"));
      setRenderingSpecificWidget((struct RenderingExpanderWidget_struct*)0);
    }
}

static void setRenderingSpecificWidget(struct RenderingExpanderWidget_struct *str)
{
  GtkWidget *wd;

  /* Remove the current widget. */
  wd = gtk_bin_get_child(GTK_BIN(viewport));
  if (wd)
    {
      if (currentDestroyFunc)
      	currentDestroyFunc();
      gtk_widget_destroy(wd);
    }

  if (str && str->create)
    {
      DBG_fprintf(stderr, "Panel Method: creating the rendering specific"
		  " widget for current method.\n");
      wd = str->create();
      currentDestroyFunc = str->destroy;
    }
  else
    {
      DBG_fprintf(stderr, "Panel Method: no rendering specific"
		  " widget for the current method.\n");
      wd = gtk_label_new("");
      gtk_label_set_markup(GTK_LABEL(wd), _("<i>None</i>"));
      gtk_misc_set_alignment(GTK_MISC(wd), 0., 0.);
      gtk_misc_set_padding(GTK_MISC(wd), 15, 0);
      currentDestroyFunc = (VisuUiPanelDestroyFunc)0;
    }
  gtk_container_add(GTK_CONTAINER(viewport), wd);
  gtk_widget_show(wd);
}
static void onRenderingChanged(GObject *obj _U_, VisuRendering *meth,
			       gpointer data _U_)
{
  GtkTreeIter iter;
  gboolean valid;
  VisuRendering* method;
  int i;

  if (meth)
    {
      DBG_fprintf(stderr, "Panel Method: changing rendering method to '%s'.\n",
		  visu_rendering_getName(meth, FALSE));
      i = 0;
      /* Get the first iter in the list */
      valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(listStoreRenderingMethods), &iter);
      while (valid)
	{
	  gtk_tree_model_get(GTK_TREE_MODEL(listStoreRenderingMethods), &iter, 
			     COLUMN_POINTER_TO_DATA, &method,
			     -1);
	  if (method == meth)
	    {
	      gtk_combo_box_set_active(GTK_COMBO_BOX(comboMethod), i);
	      DBG_fprintf(stderr, "Panel Method: ok, new method found at position %d.\n", i);
	      return;
	    }
	  valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(listStoreRenderingMethods), &iter);
	  i += 1;
	}
      g_warning("Can't find the current rendering method ('%s')"
	      " is the combobox of the config panel.",
		visu_rendering_getName(meth, FALSE));
    }
  else
    g_warning("Unsupported NULL rendering method.");
}

void visu_ui_panel_method_set(VisuRendering *method,
				       VisuUiNewWidgetFunc create,
				       VisuUiPanelDestroyFunc destroy)
{
  struct RenderingExpanderWidget_struct *str;

  g_return_if_fail(method && (!destroy || (destroy && create)));
  
  if (!create)
    return;

  str = g_malloc(sizeof(struct RenderingExpanderWidget_struct));
  str->create = create;
  str->destroy = destroy;
  g_hash_table_insert(listOfRenderingSpecificConfigWidget,
		      (gpointer)method, (gpointer)str);
}
