/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "panelSurfaces.h"
#include "panelSurfacesTools.h"
#include "panelPlanes.h"

#include <math.h>
#include <string.h>
#include <stdlib.h>

#include <visu_gtk.h>
#include <visu_object.h>
#include <support.h>
#include <gtk_main.h>
#include <opengl.h>
#include <gtk_renderingWindowWidget.h>
#include <openGLFunctions/objectList.h>
#include <openGLFunctions/interactive.h>
#include <extensions/surfs.h>

#include <extraFunctions/surfaces.h>
#include <extraFunctions/scalarFields.h>
#include <extraFunctions/pot2surf.h>
#include <extraGtkFunctions/gtk_toolPanelWidget.h>
#include <extraGtkFunctions/gtk_colorComboBoxWidget.h>
#include <extraGtkFunctions/gtk_shadeComboBoxWidget.h>
#include <extraGtkFunctions/gtk_valueIOWidget.h>
#include <extraGtkFunctions/gtk_fieldChooser.h>

/**
 * SECTION:panelSurfaces
 * @short_description: Gtk interface to load isosurfaces.
 * 
 * <para>This module contains the panel used to draw isosurfaces. From
 * it, you can draw isosurfaces on screen after they are loaded
 * through the <link linkend="v-sim-surfaces">surfaces</link>
 * module. You can also access tools to manage and create .surf files,
 * these tools are related to the <link
 * linkend="v-sim-panelSurfacesTools">panelSurfacesTools</link>
 * module.</para>
 */

/* Global variables. */
static GtkWidget *panelSurfaces;
static GtkWidget *isosurfaces_gtk_vbox;
static GtkTreeStore *isosurfaces_data_list;
static GtkListStore *fields_data_list;
static GtkWidget *isosurfaces_tree_model;
static gulong box_signal, hide_signal;
static gboolean disableCallbacks;
static gboolean fitToBox;
static GtkTreeViewColumn *colorColumn;
enum
  {
    SURFACE_TYPE_FILE_DENPOT, /* The entry is a potential or a density file. */
    SURFACE_TYPE_FILE_SURF,   /* The entry is a surfaces file. */
    SURFACE_TYPE_SURF         /* The entry is a surface. */
  };

enum
  {
    NUMBER_COLUMN,
    SHOW_COLUMN,
    USE_SHOW_COLUMN,
    NAME_COLUMN,
    DISPLAY_NAME_COLUMN,
    EDIT_NAME_COLUMN,
    COLOR_NAME_COLUMN,
    TYPE_COLUMN,
    POTENTIAL_COLUMN,
    USE_POTENTIAL_COLUMN,
    EDIT_POTENTIAL_COLUMN,
    COLOR_POTENTIAL_COLUMN,
    PIXBUF_COLUMN,
    SHADE_COLUMN,
    MASKING_COLUMN,
    DATA_SURF_COLUMN,
    DATA_FIELD_COLUMN,
    FILENAME_COLUMN,
    N_COLUMNS
  };

static gboolean getSelectedRow(GtkTreeIter *iter);
static void isosurfaces_update_data_list(GtkTreeIter *iter);
static void showHideVisuSurfaces(GtkTreeIter *iter, gboolean recurse, gpointer value);
static void changeSurfaceColor(GtkTreeIter *iter, gboolean recurse, gpointer color);
static gboolean panel_isosurfaces_add(GtkTreeIter *iter, gboolean setValue,
				      float value, const gchar *name,
				      VisuSurfaces **surf);
static gboolean panel_isosurfaces_remove(GtkTreeIter *iter);
static void isosurfaces_remove_field(GtkListStore *list, VisuScalarField *field);
static void changeTreeViewElement(GtkTreeIter *iter, const gchar *nameRef,
				  int columnId, gpointer value);
static gboolean isosurfaces_show_next();
static gboolean loadXMLFile(const gchar *filename, GError **error);
static void _update_surf_at_iter(GtkTreeIter *iter, VisuSurfaces *surf);

/* Callbacks. */
static void onDataReady(VisuObject *obj, VisuData *dataObj,
                        VisuGlView *view, gpointer data);
static void onDataNotReady(VisuObject *obj, VisuData *dataObj,
                           VisuGlView *view, gpointer data);
static void onDataFocused(GObject *visu, VisuData *dataObj, gpointer data);
static void onOpenClicked(GtkButton *button, gpointer data);
static void onTreeSelectionChanged(GtkTreeSelection *tree, gpointer data);
static void onAddButtonClicked(GtkButton *button, gpointer data);
static void onAddSpecialButtonClicked(GtkButton *button, gpointer data);
static void onGenerateChanged(GtkSpinButton *spin, gpointer data);
static void onRemoveButtonClicked(GtkButton *button, gpointer data);
static void onReorderToggled(GtkToggleButton *toggle, gpointer data);
static void onNameEdited(GtkCellRendererText *cellrenderertext,
			 gchar *path, gchar *text, gpointer user_data);
static void onPotentialValueEdited(GtkCellRendererText *cellrenderertext,
				   gchar *path, gchar *text, gpointer user_data);
static void onShowHideAllButton(GtkButton *button, gpointer visibility);
static void onRangesChanged(VisuUiColorCombobox *colorComboBox, guint valId, gpointer data);
static void onEditPropertiesClicked(GtkButton *button, gpointer data);
static gboolean onTreeViewClicked(GtkWidget *widget, GdkEventButton *event,
				  gpointer user_data);
static void onAskForHideNodes(VisuData *visuData, gboolean *redraw, gpointer data);
static gboolean onPropertiesClosed(GtkWidget *widget, GdkEvent *event, gpointer data);
static void onToolShadeChange(VisuUiShadeCombobox *combo, ToolShade *shade, gpointer data);
static void onMaskingToggled(GtkCellRendererToggle *cell_renderer,
			     gchar *path, gpointer user_data);
static void onSizeChanged(VisuBox *box, gfloat extens, gpointer user_data);
static void onResourcesChanged(GObject *object, VisuData *dataObj, gpointer data);
static void onDrawIntraChanged(GtkToggleButton *button, gpointer data);
static void onTreeViewActivated(GtkTreeView *tree_view, GtkTreePath *path,
				GtkTreeViewColumn *column, gpointer user_data);
static void panelIsosurfacesUpdate_surfaceProperties();

/* Global widgets. */
static GtkWidget *useButton;
static GtkWidget *edit_window;
static GtkWidget *edit_combo_color;
static GtkWidget *vboxColorur;
static GtkWidget *vboxToolShade;
static GtkWidget *shadeCombo;
static GtkWidget *auto_reorder;
static GtkWidget *buttonAddSurface;
static GtkWidget *buttonAddSpecial;
static GtkWidget *buttonRemoveSurface;
static GtkWidget *buttonEdit;
static GtkWidget *buttonOpen;
static GtkWidget *buttonConvert;
static GtkWidget *checkAutoLoad;
static GtkWidget *checkDrawIntra;
static GtkWidget *valueIO;

/* Global flags. */
static gboolean reverse_order = FALSE;
static gboolean autoload_file = FALSE;

/* New types. */
typedef void (*ChangesMethod)(GtkTreeIter *iter, gboolean recurse, gpointer data);



/* Get the iter of the selected row. */
static gboolean getSelectedRow(GtkTreeIter *iter)
{
  GtkTreeModel *model;
  GtkTreeSelection* tree_selection = 
    gtk_tree_view_get_selection(GTK_TREE_VIEW(isosurfaces_tree_model));

  if (!gtk_tree_selection_get_selected(tree_selection, &model, iter))
    return FALSE;

  return TRUE;
}

/* Call @method on iter depending on selection.
   - If there is no selection, changes are applied on all surfaces.
   - If selection is on a file, changes are applied on all surfaces on that file.
   - If selection is on a surface, changes are applied on all surfaces
     of the same file.
   */
static void applyChanges(ChangesMethod method, gpointer value)
{
  gboolean valid;
  int type;
  gboolean validParent, validChild;
  GtkTreeIter iter, parent, child;
  
  DBG_fprintf(stderr, "Panel VisuSurfaces: run in the treeview to apply changes.\n");
  valid = getSelectedRow(&iter);
  if (!valid)
    {
      DBG_fprintf(stderr, " | run on all row without recurse.\n");
      /* Nothing is selected, we call the changing method on all surfaces. */
      validParent = gtk_tree_model_get_iter_first
        (GTK_TREE_MODEL(isosurfaces_data_list), &parent);
      while (validParent)
        {
          DBG_fprintf(stderr, "Panel VisuSurfaces: find one parent.\n");
          validChild = gtk_tree_model_iter_children(GTK_TREE_MODEL(isosurfaces_data_list),
                                                    &child, &parent);
          while (validChild)
            {
              DBG_fprintf(stderr, "Panel VisuSurfaces: find one child.\n");
              method(&child, FALSE, value);
              validChild = gtk_tree_model_iter_next
                (GTK_TREE_MODEL(isosurfaces_data_list), &child);
            }
          validParent = gtk_tree_model_iter_next
            (GTK_TREE_MODEL(isosurfaces_data_list), &parent);
        }
    }
  else
    {
      DBG_fprintf(stderr, " | run on all row of the current file.\n");
      /* Something is selected, we try to find the parent file. */
      gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), &iter,
                         TYPE_COLUMN, &type, -1);
      if (type == SURFACE_TYPE_SURF)
        {
          valid = gtk_tree_model_iter_parent(GTK_TREE_MODEL(isosurfaces_data_list),
                                             &parent, &iter);
          g_return_if_fail(valid);
        }
      else
        parent = iter;
      /* We change the visibility of all child of parent. */
      valid = gtk_tree_model_iter_children(GTK_TREE_MODEL(isosurfaces_data_list),
                                           &iter, &parent);
      while (valid)
        {
          DBG_fprintf(stderr, "Panel VisuSurfaces: find one child.\n");
          method(&iter, TRUE, value);
          valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(isosurfaces_data_list), &iter);
        }
    }

}

/* Change one element in the tree (visibility, pixbuf color...) for
   the given iter and if @nameRef is not NULL for also all surfaces
   whose resource's name is @nameRef. */
static void changeTreeViewElement(GtkTreeIter *iter, const gchar *nameRef,
				  int columnId, gpointer value)
{
  gchar *name;
  gboolean validParent, validChild;
  GtkTreeIter parent, child;

  g_return_if_fail(iter);

  DBG_fprintf(stderr, "Panel VisuSurfaces: change value of '%s' from column %d.\n",
	      nameRef, columnId);
  gtk_tree_store_set(isosurfaces_data_list, iter,
		     columnId, value, -1);
  if (!nameRef)
    return;

  /* Update the treestore, for all surfaces with the same name. */
  validParent = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(isosurfaces_data_list), &parent);
  while (validParent)
    {
      validChild = gtk_tree_model_iter_children(GTK_TREE_MODEL(isosurfaces_data_list),
                                                &child, &parent);
      while (validChild)
        {
          gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list),
        		     &child, NAME_COLUMN, &name, -1);
	  DBG_fprintf(stderr, " | child '%s'\n", name);
          if (name && !strcmp(name, nameRef))
            gtk_tree_store_set(isosurfaces_data_list, &child,
                               columnId, value, -1);
          validChild = gtk_tree_model_iter_next(GTK_TREE_MODEL(isosurfaces_data_list), &child);
        }
      validParent = gtk_tree_model_iter_next(GTK_TREE_MODEL(isosurfaces_data_list), &parent);
    }
  DBG_fprintf(stderr, "Panel VisuSurfaces: OK.\n");
}

/***********************/
/* Visibility Handling */
/***********************/
/* Change the visibility of one surface (given by its iter).
   The visibility is in value (using GPOINTER_TO_INT) and
   recurse is a flag to tell if changes must be propagated to
   all elements with the same name or not. */
static void showHideVisuSurfaces(GtkTreeIter *iter, gboolean recurse, gpointer value)
{
  int surf_number;
  VisuSurfaces *surfaces;
  int type;
  gchar *name;

  g_return_if_fail(iter);
  
  /* Grep surface informations. */
  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list),
		     iter, NUMBER_COLUMN, &surf_number,
		     DATA_SURF_COLUMN, &surfaces,
		     NAME_COLUMN, &name,
		     TYPE_COLUMN, &type, -1);
  if (type != SURFACE_TYPE_SURF)
    return;

  /* Update the treestore, for all surfaces with the same name. */
  if (recurse)
    changeTreeViewElement(iter, name, SHOW_COLUMN, value);
  else
    gtk_tree_store_set(isosurfaces_data_list, iter,
                       SHOW_COLUMN, value, -1);

  /* Update the surface properties. */
  DBG_fprintf(stderr, "Panel VisuSurfaces: set visible property of"
	      " surface %d to %d.\n", surf_number, GPOINTER_TO_INT(value));
  visu_surfaces_setRendered(surfaces, surf_number, (gboolean)GPOINTER_TO_INT(value));
}

/* Callback for the "Draw" check box. 
   If the selected surface were drawn, hides it and uncheck the button.
   If it was hidden, shows it and check the button. */
void isosurfaces_tree_show_hide(GtkCellRendererToggle *cell_renderer, 
				gchar *string_path, gpointer user_data _U_)
{
  GtkTreePath* path = gtk_tree_path_new_from_string(string_path);
  GtkTreeIter iter;
  gboolean valid;

  valid = gtk_tree_model_get_iter(GTK_TREE_MODEL(isosurfaces_data_list), &iter, path);
  gtk_tree_path_free(path);
  if(!valid)
    return;

  DBG_fprintf(stderr, "Panel VisuSurfaces: toggle show box on '%s'.\n", string_path);
  /* We change the visibility of the surface (both resource and check box). */
  showHideVisuSurfaces(&iter, TRUE,
                       GINT_TO_POINTER(!gtk_cell_renderer_toggle_get_active(cell_renderer)));
  visu_gl_ext_surfaces_draw(visu_gl_ext_surfaces_getDefault());
  VISU_REDRAW_ADD;
}
static void onMaskingToggled(GtkCellRendererToggle *cell_renderer _U_,
			     gchar *string_path, gpointer user_data _U_)
{
  GtkTreePath* path = gtk_tree_path_new_from_string(string_path);
  GtkTreeIter iter;
  gboolean valid, mask, rebuild;
  VisuSurfaces *surf;
  VisuSurfacesResources *res;
  int idSurf;
  VisuPlane **planes;

  valid = gtk_tree_model_get_iter(GTK_TREE_MODEL(isosurfaces_data_list), &iter, path);
  gtk_tree_path_free(path);
  g_return_if_fail(valid);

  DBG_fprintf(stderr, "Panel VisuSurfaces: toggle masking box on '%s'.\n",
	      string_path);
  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), &iter,
		     NUMBER_COLUMN, &idSurf,
		     MASKING_COLUMN, &mask,
		     DATA_SURF_COLUMN, &surf, -1);

  gtk_tree_store_set(isosurfaces_data_list, &iter,
		     MASKING_COLUMN, !mask, -1);

  res = visu_surfaces_getResourceById(surf, idSurf);
  if (res->sensitiveToPlanes == !mask)
    return;
  res->sensitiveToPlanes = !mask;

  rebuild = FALSE;
  /* Hide the polygons in the surface. */
  planes = visu_ui_panel_planes_getAll(TRUE);
  if (planes[0])
    rebuild = visu_surfaces_hide(surf, planes);
  g_free(planes);

  if (rebuild)
    VISU_REDRAW_ADD;
}
/**
 * visu_ui_panel_surfaces_showAll:
 * @show: TRUE to show all surfaces, FALSE to hide them.
 * 
 * Shows or hides all surfaces and check their "draw" status in the panel accordingly.
 *
 * Returns: TRUE if surface list should be rebuild and redraw.
 **/
gboolean visu_ui_panel_surfaces_showAll(gboolean show)
{
  applyChanges(showHideVisuSurfaces, GINT_TO_POINTER(show));
  visu_gl_ext_surfaces_draw(visu_gl_ext_surfaces_getDefault());
  VISU_REDRAW_ADD;
  return TRUE;
}

void onShowHideAllButton(GtkButton *button _U_, gpointer visibility)
{
  gboolean reorder;

  reorder = visu_ui_panel_surfaces_showAll(GPOINTER_TO_INT(visibility));
  if (reorder)
    VISU_REDRAW_ADD;
}


/******************************/
/* Color & material Handling */
/******************************/
/* Changes the colour for a specific surface identified by its iter.
   If recurse is TRUE, changes are also applied on surfaces with
   the same name. */
void changeSurfaceColor(GtkTreeIter *iter, gboolean recurse, gpointer colorComboBox)
{
  GdkPixbuf *pixbuf;
  int id;
  VisuSurfaces *surfaces;
  gchar *name;
  ToolColor *color;
  float *rgba;
  float *material;

  g_return_if_fail(iter);
  
  /* Grep surface informations. */
  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), iter,
		     DATA_SURF_COLUMN, &surfaces,
		     NAME_COLUMN, &name,
		     NUMBER_COLUMN, &id, -1);

  /* We get a color from the values of the ranges. */
  material = visu_ui_color_combobox_getRangeMaterial(VISU_UI_COLOR_COMBOBOX(colorComboBox));
  rgba = visu_ui_color_combobox_getRangeColor(VISU_UI_COLOR_COMBOBOX(colorComboBox));
  color = tool_color_new(rgba);
  
  /* Change the pixbuf for all surfaces with the name @name. */
  pixbuf = tool_color_get_stamp(color, TRUE);
  if (recurse)
    changeTreeViewElement(iter, name, PIXBUF_COLUMN, (gpointer)pixbuf);
  else
    gtk_tree_store_set(isosurfaces_data_list, iter,
                       PIXBUF_COLUMN, (gpointer)pixbuf, -1);
  g_object_unref(pixbuf);

  /* Change the color & material. */
  visu_surfaces_setColorAndMaterial(surfaces, id, color, material);

  g_free(material);
  g_free(rgba);
  g_free(color);
}

/* Callback for when the combo color is changed in the edit panel */
void isosurfaces_combo_selection_changed(VisuUiColorCombobox *combo,
					 ToolColor *color _U_, gpointer data)
{
  GtkTreeIter iter;
  gboolean valid;

  DBG_fprintf(stderr, "Panel VisuSurfaces: caught the 'color-selected' signal.\n");

  if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(data)))
    {
      valid = getSelectedRow(&iter);
      g_return_if_fail(valid);
      /* We only change the selected surface. */
      changeSurfaceColor(&iter, TRUE, (gpointer)combo);
    }
  else
    /* We change all surfaces depending on the selected one. */
    applyChanges(changeSurfaceColor, (gpointer)combo);

  visu_gl_ext_surfaces_draw(visu_gl_ext_surfaces_getDefault());
  VISU_REDRAW_ADD;
}

static void onRangesChanged(VisuUiColorCombobox *colorComboBox,
			    guint valId _U_, gpointer data)
{
  gboolean valid;
  GtkTreeIter iter;

  DBG_fprintf(stderr, "Panel VisuSurfaces: caught the "
                      "'[material:color]-value-changed' signal.\n");

  if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(data)))
    {
      valid = getSelectedRow(&iter);
      g_return_if_fail(valid);
      /* We only change the selected surface. */
      changeSurfaceColor(&iter, TRUE, (gpointer)colorComboBox);
    }
  else
    /* We change all surfaces depending on the selected one. */
    applyChanges(changeSurfaceColor, (gpointer)colorComboBox);

  visu_gl_ext_surfaces_draw(visu_gl_ext_surfaces_getDefault());
  VISU_REDRAW_ADD;
}
static void onToolShadeChange(VisuUiShadeCombobox *combo _U_, ToolShade *shade, gpointer data _U_)
{
  gboolean valid;
  GtkTreeIter iter, child;
  int type, surfId;
  VisuScalarField *field;
  VisuSurfaces *surf;
  float value, alpha;
  double minmax[2];
  VisuSurfacesResources *res, *resNew;

  DBG_fprintf(stderr, "Panel VisuSurfaces: caught the "
                      "'shade-selected' signal.\n");

  valid = getSelectedRow(&iter);
  g_return_if_fail(valid);

  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), &iter,
		     TYPE_COLUMN, &type,
		     DATA_FIELD_COLUMN, &field, -1);
  g_return_if_fail(type == SURFACE_TYPE_FILE_DENPOT && field);

  visu_scalar_field_getMinMax(field, minmax);
  alpha = sqrt((float)gtk_tree_model_iter_n_children
	       (GTK_TREE_MODEL(isosurfaces_data_list), &iter));

  /* For each children, we apply the shade. */
  valid = gtk_tree_model_iter_children(GTK_TREE_MODEL(isosurfaces_data_list),
				       &child, &iter);
  while (valid)
    {
      gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), &child,
			 TYPE_COLUMN, &type,
			 POTENTIAL_COLUMN, &value,
			 NUMBER_COLUMN, &surfId,
			 DATA_SURF_COLUMN, &surf, -1);
      g_return_if_fail(type == SURFACE_TYPE_SURF);

      res = visu_surfaces_getResourceById(surf, surfId);
      if (res->surfnom)
	{
	  resNew = visu_surfaces_resources_getFromName((const gchar*)0, (gboolean*)0);
	  visu_surfaces_resources_copy(resNew, res);
	  visu_surfaces_setResource(surf, surfId, resNew);
	  res = resNew;
	}
      /* We change the colour. */
      tool_shade_valueToRGB(shade, res->color->rgba,
				     (value - minmax[0]) / (minmax[1] - minmax[0]));
      res->color->rgba[3] = exp(-alpha * (1. - (value - minmax[0]) / (minmax[1] - minmax[0])));

      valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(isosurfaces_data_list), &child);
    }
  
  isosurfaces_update_data_list(&iter);
  VISU_REDRAW_ADD;
}

/**
 * visu_ui_panel_surfaces_editProperties:
 * @iter: the currently selected row iter (or NULL).
 *
 * Opens a new window allowing to edit surface properties.
 */
void visu_ui_panel_surfaces_editProperties(GtkTreeIter *iter)
{
  GtkWidget *hbox, *label, *expand, *notebook, *radio;
  GSList *radiobuttonCycle_group;
  static GtkWidget *radioOne, *radioAll;
  gint type;

  if (!GTK_IS_WINDOW(edit_window))
    {
      edit_window = gtk_dialog_new_with_buttons
	(_("Edit surface properties"),
	 GTK_WINDOW(visu_ui_panel_getContainerWindow(VISU_UI_PANEL(panelSurfaces))), 0,
	 GTK_STOCK_CLOSE, GTK_RESPONSE_ACCEPT, NULL);

      gtk_window_set_default_size(GTK_WINDOW(edit_window), 320, -1);
      gtk_window_set_type_hint(GTK_WINDOW(edit_window), GDK_WINDOW_TYPE_HINT_UTILITY);
      gtk_window_set_skip_pager_hint(GTK_WINDOW(edit_window), TRUE);
      gtk_container_set_border_width(GTK_CONTAINER(edit_window), 3);

      notebook = gtk_notebook_new();
      gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG(edit_window))),
			 notebook, TRUE, TRUE, 0);

      /* Create the colour tab. */
      vboxColorur = gtk_vbox_new(FALSE, 0);
      gtk_notebook_append_page(GTK_NOTEBOOK(notebook), vboxColorur,
			       gtk_label_new(_("Color")));

      hbox = gtk_hbox_new(FALSE, 0);
      gtk_box_pack_start(GTK_BOX(vboxColorur), hbox, FALSE, FALSE, 5);

      label = gtk_label_new(_("Apply on: "));
      gtk_widget_set_name(label, "label_head_2");
      gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

      radio = gtk_radio_button_new(NULL);
      gtk_radio_button_set_group(GTK_RADIO_BUTTON(radio), (GSList*)0);
      radiobuttonCycle_group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radio));
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio), TRUE);
      gtk_box_pack_start(GTK_BOX(hbox), radio, TRUE, TRUE, 0);
      label = gtk_label_new(_("selected surface"));
      gtk_container_add(GTK_CONTAINER(radio), label);
      radioOne = radio;

      radio = gtk_radio_button_new(NULL);
      gtk_radio_button_set_group(GTK_RADIO_BUTTON(radio), radiobuttonCycle_group);
      radiobuttonCycle_group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radio));
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio), FALSE);
      gtk_box_pack_start(GTK_BOX(hbox), radio, TRUE, TRUE, 0);
      label = gtk_label_new(_("all surfaces"));
      gtk_container_add(GTK_CONTAINER(radio), label);
      radioAll = radio;

      hbox = gtk_hbox_new(FALSE, 0);
      gtk_box_pack_start(GTK_BOX(vboxColorur), hbox, FALSE, FALSE, 0);

      label = gtk_label_new(_("Color: "));
      gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 2);

      edit_combo_color = visu_ui_color_combobox_newWithRanges(TRUE);
      visu_ui_color_combobox_setExpanded(VISU_UI_COLOR_COMBOBOX(edit_combo_color), TRUE);
      gtk_box_pack_start(GTK_BOX(hbox), edit_combo_color, TRUE, TRUE, 2);

      expand = visu_ui_color_combobox_getRangeWidgets(VISU_UI_COLOR_COMBOBOX(edit_combo_color));
      gtk_box_pack_start(GTK_BOX(vboxColorur), expand, FALSE, FALSE, 0);

      /* Set the callbacks. */
      g_signal_connect(G_OBJECT(edit_window), "response",
		       G_CALLBACK(gtk_widget_hide), (gpointer)0);
      g_signal_connect(G_OBJECT(edit_window), "delete-event",
		       G_CALLBACK(onPropertiesClosed), (gpointer)0);
      g_signal_connect(G_OBJECT(edit_window), "destroy-event",
		       G_CALLBACK(onPropertiesClosed), (gpointer)0);
      g_signal_connect(G_OBJECT(edit_combo_color), "color-selected",
		       G_CALLBACK(isosurfaces_combo_selection_changed),
		       (gpointer)radio);
      g_signal_connect(G_OBJECT(edit_combo_color), "material-value-changed",
		       G_CALLBACK(onRangesChanged), (gpointer)radio);
      g_signal_connect(G_OBJECT(edit_combo_color), "color-value-changed",
		       G_CALLBACK(onRangesChanged), (gpointer)radio);

     /* Create the shade tab. */
      vboxToolShade = gtk_vbox_new(FALSE, 0);
      gtk_notebook_append_page(GTK_NOTEBOOK(notebook), vboxToolShade,
			       gtk_label_new(_("Shade")));
      
      label = gtk_label_new(_("Apply a shade to the current surfaces of"
			      " the selected scalar field."));
      gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_FILL);
      gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
      gtk_box_pack_start(GTK_BOX(vboxToolShade), label, FALSE, FALSE, 5);

      hbox = gtk_hbox_new(FALSE, 0);
      gtk_box_pack_start(GTK_BOX(vboxToolShade), hbox, FALSE, FALSE, 5);

      label = gtk_label_new(_("ToolShade: "));
      gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

      shadeCombo = visu_ui_shade_combobox_new(TRUE, TRUE);
      gtk_box_pack_start(GTK_BOX(hbox), shadeCombo, TRUE, TRUE, 0);

      /* Set the callbacks. */
      g_signal_connect(G_OBJECT(shadeCombo), "shade-selected",
		       G_CALLBACK(onToolShadeChange), (gpointer)0);

      gtk_widget_show_all(edit_window);
    }
  else
    gtk_window_present(GTK_WINDOW(edit_window));
  
  /* Set the radio combo. */
  if (iter)
    gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), iter,
		       TYPE_COLUMN, &type, -1);
  else
    type = SURFACE_TYPE_FILE_DENPOT;
  gtk_widget_set_sensitive(radioOne, (type == SURFACE_TYPE_SURF));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioAll),
			       (type != SURFACE_TYPE_SURF));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioOne),
			       (type == SURFACE_TYPE_SURF));

  panelIsosurfacesUpdate_surfaceProperties();
}
static void panelIsosurfacesUpdate_surfaceProperties()
{
  VisuSurfacesResources *res;
  gboolean valid;
  int current_surface_selected, type;
  GtkTreeIter iter;
  VisuSurfaces *surfaces;
  gint shadeId;
  float material[5];

  res = (VisuSurfacesResources*)0;
  type = -1;
  valid = getSelectedRow(&iter);
  if(valid)
    {
      gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), &iter,
			 DATA_SURF_COLUMN, &surfaces,
			 TYPE_COLUMN, &type,
			 NUMBER_COLUMN, &current_surface_selected,
			 SHADE_COLUMN, &shadeId, -1);

      if (type == SURFACE_TYPE_SURF)
	res = visu_surfaces_getResourceById(surfaces, current_surface_selected);
      if (type != SURFACE_TYPE_FILE_DENPOT)
	shadeId = -1;
    }
  else
    {
      shadeId = -1;
      type = SURFACE_TYPE_SURF;
    }
  
  /* Set the default values but don't raise any change signal. */
  if (res)
    {
      visu_ui_color_combobox_setRangeColor(VISU_UI_COLOR_COMBOBOX(edit_combo_color), res->color->rgba, FALSE);
      visu_ui_color_combobox_setRangeMaterial(VISU_UI_COLOR_COMBOBOX(edit_combo_color), res->material, FALSE);
    }
  else
    {
      material[0] = 0.2f;
      material[1] = 1.0f;
      material[2] = 0.5f;
      material[3] = 0.5f;
      material[4] = 0.0f;
      visu_ui_color_combobox_setRangeMaterial(VISU_UI_COLOR_COMBOBOX(edit_combo_color), material, FALSE);
    }
  /* Set widgets sensitive or not. */
  if (vboxToolShade)
    {
      gtk_widget_set_sensitive(vboxToolShade, (type == SURFACE_TYPE_FILE_DENPOT));
      gtk_combo_box_set_active(GTK_COMBO_BOX(shadeCombo), shadeId);
    }
}

static gboolean onPropertiesClosed(GtkWidget *widget,
				   GdkEvent *event _U_, gpointer data _U_)
{
  gtk_widget_hide(widget);
  return TRUE;
}


static void onEditPropertiesClicked(GtkButton *button _U_, gpointer data _U_)
{
  GtkTreeIter iter;
  gboolean valid;

  DBG_fprintf(stderr, "Panel VisuSurfaces: clicked on properties button.\n");
  valid = getSelectedRow(&iter);
  if (valid)
    visu_ui_panel_surfaces_editProperties(&iter);
  else
    visu_ui_panel_surfaces_editProperties((GtkTreeIter*)0);
}



/* See header for more info */
static void onOpenClicked(GtkButton *button _U_, gpointer data _U_)
{
  char *file_choosed;
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips = gtk_tooltips_new ();
#endif
  GtkWidget *dialog;
  gboolean invalid, redraw;
  VisuData *dataObj;
  float trans[3] = {0.f, 0.f, 0.f};
  VisuScalarFieldMethod *fmt;
  VisuBox *boxObj;
  GtkTreeIter iter;
  VisuSurfaces *surf;
  VisuScalarField *field;

  dialog = visu_ui_field_chooser_new((GtkWindow*)0);

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelSurfaces));
  redraw = FALSE;
  invalid = TRUE;
  while(invalid)
    {
      switch(gtk_dialog_run(GTK_DIALOG(dialog)))
	{
	case GTK_RESPONSE_ACCEPT:
	  file_choosed = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
          fmt = visu_ui_field_chooser_getFileFormat(VISU_UI_FIELD_CHOOSER(dialog));
          if (visu_ui_field_chooser_getFit(VISU_UI_FIELD_CHOOSER(dialog)) ==
              VISU_UI_FIT_TO_BOX)
            boxObj = visu_boxed_getBox(VISU_BOXED(dataObj));
          else
            boxObj = (VisuBox*)0;
	  if(visu_ui_panel_surfaces_loadFile(file_choosed, boxObj, (GHashTable*)0, fmt))
	    {
	      redraw = TRUE;
	      invalid = FALSE;
	    }
	  break;
	default:
	  invalid = FALSE;
	}
    }

  /* We update the VisuData bounding box. */
  if (redraw && visu_ui_field_chooser_getFit(VISU_UI_FIELD_CHOOSER(dialog)) ==
      VISU_UI_FIT_TO_SURFACE)
    {
      getSelectedRow(&iter);
      gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), &iter,
                         DATA_SURF_COLUMN, &surf,
                         DATA_FIELD_COLUMN, &field,
                         -1);
      DBG_fprintf(stderr, "Panel Surfaces: update data bounding box.\n");
      if (field)
        {
          visu_boxed_setBox(VISU_BOXED(dataObj), VISU_BOXED(field), FALSE);
          visu_box_setMargin(visu_boxed_getBox(VISU_BOXED(dataObj)),
                             visu_node_array_getMaxElementSize(VISU_NODE_ARRAY(dataObj)) +
                             visu_data_getAllNodeExtens(dataObj, (VisuBox*)0), TRUE);
        }
      else if (surf)
        visu_boxed_setBox(VISU_BOXED(dataObj), VISU_BOXED(surf), FALSE);
      visu_data_setXYZtranslation(dataObj, trans);
      g_signal_emit_by_name(G_OBJECT(dataObj), "PositionChanged", (VisuElement*)0, NULL);
    }

  /* Free everything. */
  DBG_fprintf(stderr, "Panel Surfaces: free load dialog.\n");
  gtk_widget_destroy(dialog);
}
static gboolean visu_ui_panel_surfaces_loadScalarField(const gchar *filename, GList **list,
                                                       VisuScalarFieldMethod *meth,
                                                       GHashTable *table)
{
  gboolean valid;
  GError *error;
  GList *tmplst;

  g_return_val_if_fail(list, FALSE);

  error = (GError*)0;
  if (meth)
    valid = visu_scalar_field_method_load(meth, filename, list, &error);
  else
    valid = visu_scalar_field_new_fromFile(filename, list, table, &error);
  if (!valid)
    {
      if (error)
	g_error_free(error);
      return FALSE;
    }

  if (error)
    {
      visu_ui_raiseWarning(_("Loading a file"),
                           error->message, (GtkWindow*)0);
      g_error_free(error);
      for (tmplst = *list; tmplst; tmplst = g_list_next(tmplst))
        g_object_unref(G_OBJECT(tmplst->data));
      g_list_free(*list);
      *list = (GList*)0;
      return TRUE;
    }

  return TRUE;
}
static gboolean visu_ui_panel_surfaces_loadSurface(const gchar *filename,
                                                   VisuSurfaces **surf)
{
  gboolean valid;
  GError *error;

  g_return_val_if_fail(surf, FALSE);

  error = (GError*)0;
  valid = visu_surfaces_loadFile(filename, surf, &error);
  if (!valid)
    {
      if (error)
	g_error_free(error);
      return FALSE;
    }

  if (error)
    {
      visu_ui_raiseWarning(_("Loading a file"),
                           error->message, (GtkWindow*)0);
      g_error_free(error);
      g_object_unref(*surf);
      *surf = (VisuSurfaces*)0;
      return TRUE;
    }

  return TRUE;
}
/**
 * visu_ui_panel_surfaces_addField:
 * @field: (transfer full): a #VisuScalarField object.
 * @iter: (out caller-allocates): a location to store the iter.
 * 
 * This routine can be used to add a #VisuScalarField to the tree
 * view. @iter is then populated with the row it has been inserted to.
 *
 * Since: 3.7
 **/
void visu_ui_panel_surfaces_addField(VisuScalarField *field, GtkTreeIter *iter)
{
  gchar *name, *label;
  double minmax[2];
  
  name = g_path_get_basename(visu_scalar_field_getFilename(field));
  visu_scalar_field_getMinMax(field, minmax);
  label = g_strdup_printf(_("<b>%s</b>\n  <span size=\"smaller\"><i>"
                            "Den./pot. data (min|max)\n  %g | %g</i></span>"),
                          name, minmax[0], minmax[1]);
  gtk_list_store_append(fields_data_list, iter);
  gtk_list_store_set(fields_data_list, iter,
                     VISU_UI_SURFACES_FIELD_LABEL, label,
                     VISU_UI_SURFACES_FIELD_POINTER, field,
                     -1);

  gtk_tree_store_append(isosurfaces_data_list, iter, (GtkTreeIter*)0);
  gtk_tree_store_set(isosurfaces_data_list, iter,
                     NUMBER_COLUMN, -1,
                     USE_SHOW_COLUMN, FALSE,
                     DISPLAY_NAME_COLUMN, label,
                     TYPE_COLUMN, SURFACE_TYPE_FILE_DENPOT,
                     USE_POTENTIAL_COLUMN, FALSE,
                     FILENAME_COLUMN, visu_scalar_field_getFilename(field),
                     SHADE_COLUMN, -1,
                     DATA_SURF_COLUMN, (VisuSurfaces*)0,
                     DATA_FIELD_COLUMN, field,
                     -1);
  DBG_fprintf(stderr, " | field %p.\n", (gpointer)field);
  g_free(label);
  g_free(name);
}
/**
 * visu_ui_panel_surfaces_addSurfaces:
 * @surf: (transfer full): a #VisuSurfaces object.
 * @name: a name @surf comes from.
 * @iter: (out caller-allocates): a location to store the iter.
 * 
 * This routine can be used to add a #VisuSurfaces to the tree
 * view. @iter is then populated with the row it has been inserted to.
 *
 * Since: 3.7
 **/
void visu_ui_panel_surfaces_addSurfaces(VisuSurfaces *surf, const gchar *name,
                                        GtkTreeIter *iter)
{
  gchar *label;
  
  label = g_strdup_printf(_("<b>%s</b>\n  <span size=\"smaller\"><i>"
                            "Surfaces data</i></span>"), name);

  gtk_tree_store_append(isosurfaces_data_list, iter, (GtkTreeIter*)0);
  gtk_tree_store_set(isosurfaces_data_list, iter,
                     NUMBER_COLUMN, -1,
                     USE_SHOW_COLUMN, FALSE,
                     DISPLAY_NAME_COLUMN, label,
                     TYPE_COLUMN, SURFACE_TYPE_FILE_SURF,
                     USE_POTENTIAL_COLUMN, FALSE,
                     FILENAME_COLUMN, name,
                     SHADE_COLUMN, -1,
                     DATA_SURF_COLUMN, surf,
                     DATA_FIELD_COLUMN, (VisuScalarField*)0,
                     -1);
  DBG_fprintf(stderr, " | surf %p.\n", (gpointer)surf);

  _update_surf_at_iter(iter, surf);

  if (visu_gl_ext_surfaces_add(visu_gl_ext_surfaces_getDefault(), surf))
    visu_gl_ext_surfaces_draw(visu_gl_ext_surfaces_getDefault());
}
/**
 * visu_ui_panel_surfaces_loadFile:
 * @file_name: (type filename): the file you want to try to load
 * @boxToFit: (allow-none): a #VisuBox object to fit to ;
 * @table: (allow-none): a set of different #Option (can be NULL).
 * @meth: a #VisuScalarFieldMethod object.
 * 
 * Tries to load the given @file_name  and if it succeeds, adds loaded surfaces 
 * to the isosurfaces panel. If @file_name is a #VisuScalarField then,
 * @meth is used to load it. If @fitToBox is not %NULL, the load
 * surfaces or scalar fields are fit to it.
 * 
 * Return value: TRUE in case of success.
 */
gboolean visu_ui_panel_surfaces_loadFile(const char* file_name, VisuBox *boxToFit,
                                         GHashTable *table, VisuScalarFieldMethod *meth)
{
  VisuSurfaces *surf;
  GtkTreeIter iter/* , iter2 */;
  gchar *name;
  gboolean valid;
  /* gchar *filename; */
  int type;
  GList *list, *tmplst;

  g_return_val_if_fail(file_name, FALSE);
  g_return_val_if_fail(isosurfaces_data_list, FALSE);

  list = (GList*)0;
  
  surf = (VisuSurfaces*)0;
  valid = visu_ui_panel_surfaces_loadSurface(file_name, &surf);
  if (valid)
    {
      if (!surf)
        return FALSE;
      type = SURFACE_TYPE_FILE_SURF;
      DBG_fprintf(stderr, "Panel VisuSurfaces: append surfaces %p.\n",
		  (gpointer)surf);
      list = g_list_append(list, (gpointer)surf);
    }
  else
    {
      valid = visu_ui_panel_surfaces_loadScalarField(file_name, &list, meth, table);
      if (valid)
        {
          if (!list)
            return FALSE;
          type = SURFACE_TYPE_FILE_DENPOT;
        }
    }
  if (!valid)
    /* Not a density/potential file. */
    return FALSE;

  if (boxToFit)
    {
      fitToBox = TRUE;
      if (type == SURFACE_TYPE_FILE_SURF)
        for (tmplst = list; tmplst; tmplst = g_list_next(tmplst))
          visu_boxed_setBox(VISU_BOXED(tmplst->data), VISU_BOXED(boxToFit), TRUE);
      else
        for (tmplst = list; tmplst; tmplst = g_list_next(tmplst))
          visu_boxed_setBox(VISU_BOXED(tmplst->data), VISU_BOXED(boxToFit), FALSE);
      }
  else
    fitToBox = FALSE;

  /* Before adding it to the tree, we may remove all identic
     already existing file. */
  /* DBG_fprintf(stderr, "Panel VisuSurfaces: may remove an existing entry.\n"); */
  /* valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(isosurfaces_data_list), */
  /*       				&iter); */
  /* while (valid) */
  /*   { */
  /*     gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), */
  /*       		 &iter, FILENAME_COLUMN, &filename, -1); */
  /*     iter2 = iter; */
  /*     valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(isosurfaces_data_list), */
  /*       			       &iter); */
  /*     if (!strcmp(filename, file_name)) */
  /*       panel_isosurfaces_remove(&iter2); */
  /*   } */

  /* We create a root element for the file. */
  DBG_fprintf(stderr, "Panel VisuSurfaces: add a new root entry '%s'.\n", file_name);
  if (type == SURFACE_TYPE_FILE_SURF)
    {
      name = g_path_get_basename(file_name);
      for (tmplst = list; tmplst; tmplst = g_list_next(tmplst))
        visu_ui_panel_surfaces_addSurfaces(VISU_SURFACES(tmplst->data), name, &iter); 
      g_free(name);
    }
    else
      for (tmplst = list; tmplst; tmplst = g_list_next(tmplst))
        visu_ui_panel_surfaces_addField((VisuScalarField*)tmplst->data, &iter);
  g_list_free(list);

  gtk_tree_selection_select_iter
    (gtk_tree_view_get_selection(GTK_TREE_VIEW(isosurfaces_tree_model)), &iter);

  return TRUE;
}

/* refresh tree view info according to loaded surfaces by surfaces.c 
   refresh gl display */
static void isosurfaces_update_data_list(GtkTreeIter *iter)
{
  int i, n, type, *ids, pos;
  VisuSurfacesResources *res;
  const gchar *current_surface;
  GdkPixbuf *pixbuf;
  float *potentialData;
  GtkTreeIter childIter, selectedIter;
  gboolean valid;
  GtkTreePath *path;
  GtkTreeRowReference* selectedRow;
  const gchar *display_name;
  VisuSurfaces *surf;

  g_return_if_fail(isosurfaces_data_list);

  /* Save the selected item. */
  valid = getSelectedRow(&selectedIter);
  if (valid)
    {
      DBG_fprintf(stderr, "Panel VisuSurfaces: store selected iter.\n");
      path = gtk_tree_model_get_path(GTK_TREE_MODEL(isosurfaces_data_list),
				     &selectedIter);
      selectedRow = gtk_tree_row_reference_new(GTK_TREE_MODEL(isosurfaces_data_list),
					       path);
      gtk_tree_path_free(path);
    }
  else
    selectedRow = (GtkTreeRowReference*)0;
  
  DBG_fprintf(stderr, "Panel VisuSurfaces: get surf & type for given iter.\n");
  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), iter,
		     TYPE_COLUMN, &type,
		     DATA_SURF_COLUMN, &surf, -1);

  /* We remove all children from the given iter. */
  DBG_fprintf(stderr, "Panel VisuSurfaces: remove children from the iter.\n");
  valid = gtk_tree_model_iter_children(GTK_TREE_MODEL(isosurfaces_data_list),
				       &childIter, iter);
  while (valid)
    {
      gtk_tree_store_remove(isosurfaces_data_list, &childIter);
      valid = gtk_tree_model_iter_children(GTK_TREE_MODEL(isosurfaces_data_list),
					   &childIter, iter);
    }

  /* No surfaces are currently associated to the iter. */
  if (!surf)
    {
      if (selectedRow)
	gtk_tree_row_reference_free(selectedRow);
      return;
    }

  ids = visu_surfaces_getSortedById(surf);
  n = visu_surfaces_getN(surf);
  DBG_fprintf(stderr, "Panel VisuSurfaces: append %d children to the iter.\n", n);
  potentialData = visu_surfaces_getPropertyFloat(surf, VISU_SURFACES_PROPERTY_POTENTIAL);
  for(i = 0; i < n; i++)
    {
      current_surface = visu_surfaces_getName(surf, ids[i]);
      if (!current_surface)
	display_name = VISU_UI_SURFACE_NAME_CHOOSE;
      else
	display_name = current_surface;
      DBG_fprintf(stderr, " | surface '%s' %d.\n", current_surface, ids[i]);
      res = visu_surfaces_getResourceById(surf, ids[i]);
      g_return_if_fail(res);
      pixbuf = tool_color_get_stamp(res->color, TRUE);
      pos = visu_surfaces_getPosition(surf, ids[i]);
      gtk_tree_store_append(isosurfaces_data_list, &childIter, iter);
      gtk_tree_store_set(isosurfaces_data_list, &childIter,
			 NUMBER_COLUMN, ids[i],
			 SHOW_COLUMN, res->rendered,
			 USE_SHOW_COLUMN, TRUE,
			 NAME_COLUMN, current_surface,
			 DISPLAY_NAME_COLUMN, display_name,
			 EDIT_NAME_COLUMN, TRUE,
			 COLOR_NAME_COLUMN, "blue",
			 TYPE_COLUMN, SURFACE_TYPE_SURF,
			 POTENTIAL_COLUMN, potentialData[pos],
			 EDIT_POTENTIAL_COLUMN, (type == SURFACE_TYPE_FILE_DENPOT),
			 USE_POTENTIAL_COLUMN, TRUE,
			 DATA_SURF_COLUMN, surf,
			 MASKING_COLUMN, res->sensitiveToPlanes,
			 PIXBUF_COLUMN, pixbuf, -1);
			/* We count down the ref on the pixbuf, since one was added when
			   created and one was added when insering into the tree_model.
			   But we want only one ref count. */
      g_object_unref(pixbuf);
      if (type == SURFACE_TYPE_FILE_DENPOT)
	gtk_tree_store_set(isosurfaces_data_list, &childIter,
			   COLOR_POTENTIAL_COLUMN, "blue", -1);
    }
  g_free(ids);

  /* Expand children. */
  path = gtk_tree_model_get_path(GTK_TREE_MODEL(isosurfaces_data_list), iter);
  gtk_tree_view_expand_row(GTK_TREE_VIEW(isosurfaces_tree_model), path, TRUE);
  gtk_tree_path_free(path);

  DBG_fprintf(stderr, "Panel VisuSurfaces: restore selected item.\n");
  if (selectedRow)
    {
      path = gtk_tree_row_reference_get_path(selectedRow);
      gtk_tree_row_reference_free(selectedRow);
      if (path)
	{
	  valid = gtk_tree_model_get_iter(GTK_TREE_MODEL(isosurfaces_data_list),
					  &childIter, path);
	  gtk_tree_path_free(path);
	  if (valid)
	    gtk_tree_selection_select_iter
	      (gtk_tree_view_get_selection(GTK_TREE_VIEW(isosurfaces_tree_model)),
	       &childIter);
	}
    }
}

/* See header file for more info */
static gboolean isosurfaces_show_next()
{
  GtkTreeIter iter, selected_iter, child_iter;
  GtkTreeSelection* selected_surf;

  selected_surf = gtk_tree_view_get_selection(GTK_TREE_VIEW(isosurfaces_tree_model));

  if(!gtk_tree_selection_get_selected(selected_surf, NULL, &iter))
    return FALSE;

  if (gtk_tree_model_iter_has_child(GTK_TREE_MODEL(isosurfaces_data_list), &iter))
    {
      gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(isosurfaces_data_list),
                                    &child_iter, &iter, 0);
      iter = child_iter;
    }

  selected_iter = iter;
  if(reverse_order == FALSE)
    {
      if (!gtk_tree_model_iter_next(GTK_TREE_MODEL(isosurfaces_data_list), &iter))
	{
	  reverse_order = TRUE;
	  if (!gtk_tree_model_iter_previous(GTK_TREE_MODEL(isosurfaces_data_list), &selected_iter))
	    return FALSE;
          iter = selected_iter;
	}
    }
  else
    {
      if (!gtk_tree_model_iter_previous(GTK_TREE_MODEL(isosurfaces_data_list), &iter))
	{
	  reverse_order = FALSE;
	  if (!gtk_tree_model_iter_next(GTK_TREE_MODEL(isosurfaces_data_list), &selected_iter))
	    return FALSE;
          iter = selected_iter;
	}
    }

  /* Set the selection to the new row. */
  gtk_tree_selection_select_iter(selected_surf, &iter);

  /* Set all surfaces, set show to FALSE. */
  visu_ui_panel_surfaces_showAll(FALSE);
  /* For the selected surface, set show to TRUE. */
  showHideVisuSurfaces(&iter, FALSE, GINT_TO_POINTER(TRUE));
  visu_gl_ext_surfaces_draw(visu_gl_ext_surfaces_getDefault());
  VISU_REDRAW_ADD;

  return TRUE;
}

static void onPlayStop(gpointer data)
{
  gtk_button_set_label(GTK_BUTTON(data), _("Play"));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(data), FALSE);
}

/* Callback for the "play" button. Registers the above function to be called at a specified time
   interval. */
static void isosurfaces_play(GtkToggleButton *play_stop, GtkWidget *time_interval)
{
  static int event_id = 0;

  if(gtk_toggle_button_get_active(play_stop) == TRUE)
    {
      gtk_button_set_label(GTK_BUTTON(play_stop), _("Stop"));
      event_id = g_timeout_add_full(G_PRIORITY_DEFAULT,
                                    gtk_spin_button_get_value(GTK_SPIN_BUTTON(time_interval)),
                                    isosurfaces_show_next, play_stop, onPlayStop);
    }
  else
    g_source_remove(event_id);
}


/* Sets up the model/view architecture used to store the surfaces info. */
void isosurfaces_make_tree_view()
{
  GtkWidget *image;
  GtkCellRenderer *cell_show = gtk_cell_renderer_toggle_new();
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column = NULL;

  isosurfaces_data_list =
    gtk_tree_store_new (N_COLUMNS, G_TYPE_INT, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN,
			G_TYPE_STRING, G_TYPE_STRING, G_TYPE_BOOLEAN, G_TYPE_STRING,
			G_TYPE_INT, G_TYPE_FLOAT, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN,
			G_TYPE_STRING, GDK_TYPE_PIXBUF, G_TYPE_INT, G_TYPE_BOOLEAN,
			G_TYPE_POINTER,	G_TYPE_POINTER, G_TYPE_STRING);
  isosurfaces_tree_model = 
    gtk_tree_view_new_with_model (GTK_TREE_MODEL (isosurfaces_data_list));

  g_signal_connect(cell_show, "toggled", G_CALLBACK(isosurfaces_tree_show_hide), NULL);

  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(_("File / label"), renderer,
						    "markup", DISPLAY_NAME_COLUMN,
						    "foreground", COLOR_NAME_COLUMN,
						    "editable", EDIT_NAME_COLUMN, NULL);
  g_signal_connect(G_OBJECT(renderer), "edited",
		   G_CALLBACK(onNameEdited), (gpointer)0);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_alignment(column, 0);
  gtk_tree_view_column_set_resizable(column, TRUE);
  gtk_tree_view_append_column (GTK_TREE_VIEW (isosurfaces_tree_model), column);

  column = gtk_tree_view_column_new_with_attributes("", cell_show,
						    "active", SHOW_COLUMN,
						    "visible", USE_SHOW_COLUMN, NULL);
  gtk_tree_view_column_set_expand(column, FALSE);
  gtk_tree_view_column_set_alignment(column, 0.5);
  gtk_tree_view_append_column (GTK_TREE_VIEW (isosurfaces_tree_model), column);

  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes(_("Value"), renderer,
						    "text", POTENTIAL_COLUMN,
						    "editable", EDIT_POTENTIAL_COLUMN,
						    "foreground", COLOR_POTENTIAL_COLUMN,
						    "visible", USE_POTENTIAL_COLUMN, NULL);
  gtk_tree_view_column_set_sort_column_id(GTK_TREE_VIEW_COLUMN(column),
					  POTENTIAL_COLUMN);
  g_signal_connect(G_OBJECT(renderer), "edited",
		   G_CALLBACK(onPotentialValueEdited), (gpointer)0);
  gtk_tree_view_append_column(GTK_TREE_VIEW(isosurfaces_tree_model), column);

  renderer = gtk_cell_renderer_pixbuf_new ();
  column = gtk_tree_view_column_new_with_attributes(_("Color"), renderer,
						    "pixbuf", PIXBUF_COLUMN,
						    NULL);
  image = gtk_image_new_from_stock(GTK_STOCK_SELECT_COLOR,
				   GTK_ICON_SIZE_SMALL_TOOLBAR);
  gtk_widget_show(image);
  gtk_tree_view_column_set_widget(column, image);
  gtk_tree_view_append_column(GTK_TREE_VIEW(isosurfaces_tree_model), column);
  colorColumn = column;

  renderer = gtk_cell_renderer_toggle_new();
  column = gtk_tree_view_column_new_with_attributes(_("Plane masking"), renderer,
						    "active", MASKING_COLUMN,
						    "visible", USE_POTENTIAL_COLUMN,
						    NULL);
  image = create_pixmap((GtkWidget*)0, "stock-masking.png");
  gtk_widget_show(image);
  gtk_tree_view_column_set_widget(column, image);
  g_signal_connect(G_OBJECT(renderer), "toggled",
		   G_CALLBACK(onMaskingToggled), NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(isosurfaces_tree_model), column);

  gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (isosurfaces_tree_model), TRUE);  

  g_signal_connect(gtk_tree_view_get_selection(GTK_TREE_VIEW(isosurfaces_tree_model)),
		   "changed", G_CALLBACK(onTreeSelectionChanged), (gpointer)0);
  g_signal_connect(G_OBJECT(isosurfaces_tree_model), "button-release-event",
		   G_CALLBACK(onTreeViewClicked), (gpointer)0);
  g_signal_connect(G_OBJECT(isosurfaces_tree_model), "row-activated",
		   G_CALLBACK(onTreeViewActivated), (gpointer)0);
}

/* Callback for time interval value spin button.
   Changes the frequency used to play the surfaces. */
void isosurfaces_change_time_interval(GtkWidget *spin_button _U_,
				      GtkWidget *play_stop)
{
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(play_stop), FALSE);
}
/**
 * visu_ui_panel_surfaces_setUsed:
 * @used: a boolean.
 *
 * Change the status of the isosurface extension, drawn or not.
 *
 * Returns: TRUE if the OpenGLAskForReDraw signal should be emitted.
 */
gboolean visu_ui_panel_surfaces_setUsed(gboolean used)
{
  if (!visu_gl_ext_setActive(VISU_GL_EXT(visu_gl_ext_surfaces_getDefault()), used))
    return FALSE;
  visu_gl_ext_surfaces_draw(visu_gl_ext_surfaces_getDefault());

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(useButton), used);
  return TRUE;
}

/* Callback for the check button "use isosurfaces".
   Enables/disables the drawing of isosurfaces by V_Sim's core. */
void isosurfaces_switch_use(GtkToggleButton *togglebutton, gpointer user_data _U_)
{
  gboolean redraw;

  redraw = visu_ui_panel_surfaces_setUsed(gtk_toggle_button_get_active(togglebutton));  
  if(redraw)
    VISU_REDRAW_ADD;
}
/**
 * visu_ui_panel_surfaces_updateAtIter:
 * @iter: an iterator.
 *
 * Reset the shown surfaces at @iter.
 *
 * Since: 3.7
 **/
void visu_ui_panel_surfaces_updateAtIter(GtkTreeIter *iter)
{
  VisuSurfaces *surf;

  DBG_fprintf(stderr, "Panel Surfaces: update view at iter.\n");
  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list),
                     iter, DATA_SURF_COLUMN, &surf, -1);
  _update_surf_at_iter(iter, surf);
}
static void _update_surf_at_iter(GtkTreeIter *iter, VisuSurfaces *surf)
{
  VisuPlane **planes;

  /* Update the tree view. */
  isosurfaces_update_data_list(iter);
  /* Hide the polygons in the surface. */
  if (surf)
    {
      planes = visu_ui_panel_planes_getAll(TRUE);
      if (planes[0])
        visu_surfaces_hide(surf, planes);
      g_free(planes);
    }
}
/**
 * visu_ui_panel_surfaces_add:
 * @filename: the name of the scalar field from which to add a surface ;
 * @value: the iso value ;
 * @name: the name used to identify the new surface (can be NULL).
 *
 * Create and add a surface created from the given scalar field. This
 * field must already be loaded. If @name is not given, the surface will
 * be called "Isosurface id" where id is an increasing counter.
 */
void visu_ui_panel_surfaces_add(gchar *filename, float value, gchar *name)
{
  GtkTreeIter iter;
  gboolean valid, found;
  gchar *file;
  VisuSurfaces *surf;

  g_return_if_fail(filename);

  DBG_fprintf(stderr, "Panel VisuSurfaces: add a new surface (%g) for '%s'.\n",
	      value, filename);
  valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(isosurfaces_data_list), &iter);
  /* We look for the iter with the given file. */
  found = FALSE;
  while (valid && !found)
    {
      gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list),
			 &iter, FILENAME_COLUMN, &file, -1);
      found = (!strcmp(file, filename));
      if (!found)
	valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(isosurfaces_data_list), &iter);
    }
  if (valid && found)
    {
      valid = panel_isosurfaces_add(&iter, TRUE, value, name, &surf);
      if (valid)
        _update_surf_at_iter(&iter, surf);
    }
  else
    g_warning("Cannot find the given scalar field '%s'.", filename);
}
/**
 * visu_ui_panel_surfaces_computeAuto:
 * @iter: a #GtkTreeIter.
 *
 * Add new surfaces to the entry pointed by @iter. This entry must be
 * a #VisuScalarField object. This routine will redraw if necessary.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuSurfaces object.
 **/
const VisuSurfaces* visu_ui_panel_surfaces_computeAuto(GtkTreeIter *iter)
{
  VisuSurfaces *surf;
  gboolean rebuild;

  rebuild = panel_isosurfaces_add(iter, FALSE, 0.f, (const gchar*)0, &surf);
  if (rebuild)
    {
      _update_surf_at_iter(iter, surf);
      VISU_REDRAW_ADD;
    }

  return surf;
}
/**
 * visu_ui_panel_surfaces_compute:
 * @iter: a #GtkTreeIter.
 * @values: (array length=nValues): values to create surfaces at.
 * @names: (array length=nValues): names for the new surfaces.
 * @nValues: number of surfaces to create.
 *
 * This routine will generate @nValues surfaces for the
 * #VisuScalarField located at @iter. It will redraw if necessary.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuSurfaces object.
 **/
const VisuSurfaces* visu_ui_panel_surfaces_compute(GtkTreeIter *iter, const float *values,
                                             const gchar **names, guint nValues)
{
  VisuSurfaces *surf;
  gboolean rebuild;
  VisuScalarField *field;
  guint i;
  int nb;

  g_return_val_if_fail(nValues, (const VisuSurfaces*)0);

  rebuild = panel_isosurfaces_add(iter, TRUE, values[0], names[0], &surf);
  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list),
		     iter, DATA_FIELD_COLUMN, &field, -1);
  for (i = 1; i < nValues; i++)
    {
      nb = visu_surfaces_getNewId(surf);
      rebuild = visu_surfaces_createFromScalarField(&surf, field, values[i], nb, names[i]) || rebuild;
    }
  if (rebuild)
    {
      _update_surf_at_iter(iter, surf);
      VISU_REDRAW_ADD;
    }

  return surf;
}
static gboolean panel_isosurfaces_add(GtkTreeIter *iter, gboolean setValue,
				      float value, const gchar *name, VisuSurfaces **surf)
{
  double mimax[2];
  int type, nb;
  gboolean valid, new;
  VisuScalarField *field;
  VisuSurfacesResources *res;
  int blue[4] = {0, 24, 185, 196}, red[4] = {185, 24, 0, 196};
  gchar *resName;
  GtkTreePath *path;
  gint* ind;

  g_return_val_if_fail(iter && surf, FALSE);

  *surf = (VisuSurfaces*)0;
  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list),
		     iter, TYPE_COLUMN, &type,
		     DATA_SURF_COLUMN, surf,
		     DATA_FIELD_COLUMN, &field, -1);

  g_return_val_if_fail(type == SURFACE_TYPE_FILE_DENPOT && field, FALSE);

  nb = visu_surfaces_getNewId(*surf);
  if (setValue)
    valid = visu_surfaces_createFromScalarField(surf, field, value, nb, name);
  else
    {
      visu_scalar_field_getMinMax(field, mimax);
      /* For densities, this create only one surface. For
	 wavefunctions, it create two and set their style
	 accordingly. */
      if (mimax[0] * mimax[1] < 0.f &&
	  MIN(ABS(mimax[0]), ABS(mimax[1])) / MAX(ABS(mimax[0]), ABS(mimax[1])) > 0.2)
	{
	  path = gtk_tree_model_get_path(GTK_TREE_MODEL(isosurfaces_data_list), iter);
	  ind = gtk_tree_path_get_indices(path);

	  resName = g_strdup_printf(_("Negative (%d)"), ind[0]);
	  res = visu_surfaces_resources_getFromName(resName, &new);
	  if (new)
	    {
	      res->color = tool_color_addIntRGBA(blue);
	      res->rendered = TRUE;
	    }
	  valid = visu_surfaces_createFromScalarField(surf, field, mimax[0] / 2., nb, resName);
	  g_free(resName);

	  resName = g_strdup_printf(_("Positive (%d)"), ind[0]);
	  res = visu_surfaces_resources_getFromName(resName, &new);
	  if (new)
	    {
	      res->color = tool_color_addIntRGBA(red);
	      res->rendered = TRUE;
	    }
	  nb = visu_surfaces_getNewId(*surf);
	  valid = valid && visu_surfaces_createFromScalarField(surf, field, mimax[1] / 2.,
					  nb, resName);
	  g_free(resName);
	  
	  gtk_tree_path_free(path);
	}
      else
	valid = visu_surfaces_createFromScalarField(surf, field, (mimax[0] + mimax[1]) / 2., nb, (gchar*)0);
    }

  gtk_tree_store_set(isosurfaces_data_list, iter,
		     DATA_SURF_COLUMN, *surf, -1);
  if (*surf && visu_gl_ext_surfaces_add(visu_gl_ext_surfaces_getDefault(), *surf))
      visu_gl_ext_surfaces_draw(visu_gl_ext_surfaces_getDefault());

  return valid;
}
static void isosurfaces_remove_field(GtkListStore *list, VisuScalarField *field)
{
  gboolean valid;
  GtkTreeIter iter;
  VisuScalarField *tmpField;

  valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(list), &iter);
  while (valid)
    {
      gtk_tree_model_get(GTK_TREE_MODEL(list), &iter,
			 VISU_UI_SURFACES_FIELD_POINTER, &tmpField, -1);
      if (tmpField == field)
	{
	  gtk_list_store_remove(list, &iter);
	  return;
	}
      valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(list), &iter);
    }
}
static gboolean panel_isosurfaces_remove(GtkTreeIter *iter)
{
  int type, idSurf;
  VisuSurfaces *surf;
  VisuScalarField *field;
  gboolean empty, valid;
  GtkTreeIter iterFather, iterChild;

  g_return_val_if_fail(iter, FALSE);

  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list),
		     iter, TYPE_COLUMN, &type,
		     DATA_SURF_COLUMN, &surf,
		     DATA_FIELD_COLUMN, &field,
		     NUMBER_COLUMN, &idSurf, -1);
  
  switch (type)
    {
    case SURFACE_TYPE_FILE_DENPOT:
      /* If the denpot has children, we remove them.
	 And don't delete the data. */
      if (gtk_tree_model_iter_has_child(GTK_TREE_MODEL(isosurfaces_data_list), iter))
	{
	  valid = gtk_tree_model_iter_children(GTK_TREE_MODEL(isosurfaces_data_list),
					       &iterChild, iter);
	  while (valid)
	    {
	      gtk_tree_store_remove(isosurfaces_data_list, &iterChild);
	      valid = gtk_tree_model_iter_children
		(GTK_TREE_MODEL(isosurfaces_data_list), &iterChild, iter);
	    }
	  if (surf)
            {
              if (visu_gl_ext_surfaces_remove(visu_gl_ext_surfaces_getDefault(), surf))
                visu_gl_ext_surfaces_draw(visu_gl_ext_surfaces_getDefault());
              g_object_unref(surf);
            }
	  gtk_tree_store_set(isosurfaces_data_list, iter,
			     DATA_SURF_COLUMN, (VisuSurfaces*)0, -1);
	  visu_ui_value_io_setSensitiveSave(VISU_UI_VALUE_IO(valueIO), FALSE);
          return TRUE;
	}
    case SURFACE_TYPE_FILE_SURF:
      /* We remove the entry from the tree. */
      gtk_tree_store_remove(isosurfaces_data_list, iter);
      if (field)
	isosurfaces_remove_field(fields_data_list, field);
      /* We free the memory used by these surfaces. */
      if (surf)
        {
          if (visu_gl_ext_surfaces_remove(visu_gl_ext_surfaces_getDefault(), surf))
            visu_gl_ext_surfaces_draw(visu_gl_ext_surfaces_getDefault());
          g_object_unref(surf);
        }
      if (field)
	g_object_unref(field);
      /* We need to rebuild the order list. */
      return TRUE;
    case SURFACE_TYPE_SURF:
      /* We change the surface list. */
      empty = visu_surfaces_remove(surf, idSurf);
      if (empty)
	{
	  gtk_tree_model_iter_parent(GTK_TREE_MODEL(isosurfaces_data_list),
				     &iterFather, iter);
	  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list),
			     &iterFather, TYPE_COLUMN, &type, -1);
	  if (type == SURFACE_TYPE_FILE_SURF)
	    gtk_tree_store_remove(isosurfaces_data_list, &iterFather);
	  else
	    {
	      gtk_tree_store_set(isosurfaces_data_list, &iterFather,
				 DATA_SURF_COLUMN, (VisuSurfaces*)0, -1);
	      /* We remove the entry from the tree. */
	      gtk_tree_store_remove(isosurfaces_data_list, iter);
	    }
          if (visu_gl_ext_surfaces_remove(visu_gl_ext_surfaces_getDefault(), surf))
            visu_gl_ext_surfaces_draw(visu_gl_ext_surfaces_getDefault());
	  g_object_unref(surf);
	}
      else
	{
	  /* We remove the entry from the tree. */
	  gtk_tree_store_remove(isosurfaces_data_list, iter);
	}
      /* We need to rebuild the order list. */
      return TRUE;
    default:
      g_warning("Wrong type for the selected entry.");
    }
  return FALSE;
}


/* Creates the main panel. */
void isosurfaces_create_gtk_interface(VisuUiPanel *visu_ui_panel)
{
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips = gtk_tooltips_new ();
#endif
  GtkWidget *top_hbox = gtk_hbox_new (FALSE, 0);
  GtkWidget *h_box3 = gtk_hbox_new (FALSE, 0);
  GtkWidget *show_all = gtk_button_new();
  GtkWidget *show_none = gtk_button_new();
  GtkWidget *play_stop =
    gtk_toggle_button_new_with_label(_("Play"));
  GtkWidget *time_interval = 
    gtk_spin_button_new_with_range  (50, 2000, 10);
  GtkWidget *scrolled_window = gtk_scrolled_window_new (NULL, NULL);
/*   GtkWidget *reorder_button = gtk_button_new_with_label("Re-order"); */
  GtkWidget *image_color = create_pixmap((GtkWidget*)0, "stock_effects-object-colorize_20.png");
  GtkWidget *image_show = create_pixmap((GtkWidget*)0, "stock-select-all_20.png");
  GtkWidget *image_hide = create_pixmap((GtkWidget*)0, "stock-unselect-all_20.png");
  GtkWidget *tree_vbox = gtk_vbox_new(FALSE, 0);
  GtkWidget *tree_hbox = gtk_hbox_new(FALSE, 0);
  GtkWidget *image, *align, *vbox;

  vbox = gtk_vbox_new(FALSE, 0);

  useButton = gtk_check_button_new_with_mnemonic(_("_Use isosurfaces"));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(useButton),
			       visu_gl_ext_getActive(VISU_GL_EXT(visu_gl_ext_surfaces_getDefault())));
  gtk_box_pack_start(GTK_BOX(vbox), useButton, FALSE, FALSE, 0);

  buttonEdit = gtk_button_new();
  gtk_widget_set_sensitive(buttonEdit, FALSE);

  gtk_container_add(GTK_CONTAINER(buttonEdit), image_color);
  gtk_container_add(GTK_CONTAINER(show_all), image_show);
  gtk_container_add(GTK_CONTAINER(show_none), image_hide);

  /* The line about loading files. */
  gtk_box_pack_start(GTK_BOX(vbox), top_hbox, FALSE, FALSE, 0);
  align = gtk_alignment_new(0., 0.5, 1., 0.);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 0, 0, 10, 0);
  gtk_box_pack_start(GTK_BOX(top_hbox), align, TRUE, TRUE, 2);
  checkAutoLoad = gtk_check_button_new_with_mnemonic(_("Auto _load data file"));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkAutoLoad), autoload_file);
  gtk_widget_set_tooltip_text(checkAutoLoad,
			_("Try to load a data file whenever a new V_Sim file is loaded."
			  " If the new file contains a scalar field, it is loaded, otherwise"
			  " a surface file is tested using a .surf extension on the file name."));
  gtk_container_add(GTK_CONTAINER(align), checkAutoLoad);
  buttonOpen = gtk_button_new();
  buttonConvert = gtk_button_new_with_mnemonic(_("_Convert"));
  gtk_widget_set_tooltip_text(buttonOpen, _("Load a surface file or a potential/density file."));
  gtk_box_pack_end(GTK_BOX(top_hbox), buttonOpen, FALSE, FALSE, 2);
  image = gtk_image_new_from_stock(GTK_STOCK_OPEN, GTK_ICON_SIZE_BUTTON);
  gtk_container_add(GTK_CONTAINER( buttonOpen), image);
  gtk_widget_set_tooltip_text(buttonConvert, _("Several built-in tools to create .surf files."));
  gtk_box_pack_end(GTK_BOX(top_hbox), buttonConvert, FALSE, FALSE, 2);
  

  isosurfaces_make_tree_view ();

  isosurfaces_gtk_vbox = gtk_vbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), isosurfaces_gtk_vbox, TRUE, TRUE, 0);

  auto_reorder = gtk_check_button_new_with_mnemonic(_("_Reorder on the fly"));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(auto_reorder), FALSE);

/*   gtk_widget_set_sensitive(reorder_button, FALSE); */

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(play_stop), FALSE);

  g_signal_connect(G_OBJECT(auto_reorder), "toggled",
		   G_CALLBACK(onReorderToggled), (gpointer)0);
/*   g_signal_connect(G_OBJECT(reorder_button), "clicked", */
/* 		   G_CALLBACK(isosurfaces_rebuild_gl_list_in_order_and_redraw), NULL); */
  g_signal_connect(G_OBJECT(buttonOpen), "clicked",
		   G_CALLBACK(onOpenClicked), NULL);
  g_signal_connect(G_OBJECT(buttonConvert), "clicked",
		   G_CALLBACK(visu_ui_panel_surfaces_tools_init), NULL);
  g_signal_connect(G_OBJECT(buttonEdit), "clicked",
		   G_CALLBACK(onEditPropertiesClicked), (gpointer)0);
  g_signal_connect(G_OBJECT(show_all), "clicked",
		   G_CALLBACK(onShowHideAllButton), GINT_TO_POINTER(1));
  g_signal_connect(G_OBJECT(show_none), "clicked",
		   G_CALLBACK(onShowHideAllButton), GINT_TO_POINTER(0));
  g_signal_connect(G_OBJECT(play_stop), "toggled",
		   G_CALLBACK(isosurfaces_play), time_interval);
  g_signal_connect(G_OBJECT(time_interval), "value_changed",
		   G_CALLBACK(isosurfaces_change_time_interval), play_stop);
  g_signal_connect(G_OBJECT(useButton), "toggled",
		   G_CALLBACK(isosurfaces_switch_use), NULL);

  /* The main viewport. */
  gtk_box_pack_start(GTK_BOX(isosurfaces_gtk_vbox), tree_hbox, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(tree_hbox), scrolled_window, TRUE, TRUE, 0);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window), 
				 GTK_POLICY_AUTOMATIC, 
				 GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolled_window), GTK_SHADOW_IN);
  gtk_container_add (GTK_CONTAINER (scrolled_window), isosurfaces_tree_model);
  gtk_box_pack_start(GTK_BOX(tree_hbox), tree_vbox, FALSE, FALSE, 2);

  gtk_widget_set_tooltip_text(show_none, _("Hides all surfaces"));
  gtk_box_pack_start(GTK_BOX(tree_vbox), show_none, FALSE, FALSE, 2);
  gtk_widget_set_tooltip_text(show_all, _("Shows all surfaces"));
  gtk_box_pack_start(GTK_BOX(tree_vbox), show_all, FALSE, FALSE, 2);
 
  gtk_widget_set_tooltip_text(buttonEdit,
		       _("Change color and material properties."));
  gtk_box_pack_end(GTK_BOX(tree_vbox), buttonEdit, FALSE, FALSE, 2);

  align = gtk_alignment_new(0.5, 1., 0., 0.);
  gtk_box_pack_start(GTK_BOX(tree_vbox), align, TRUE, TRUE, 2);
  buttonAddSurface = gtk_button_new();
  gtk_widget_set_sensitive(buttonAddSurface, FALSE);
  gtk_widget_set_tooltip_text(buttonAddSurface,
		       _("Add a new surface."));
  g_signal_connect(G_OBJECT(buttonAddSurface), "clicked",
		   G_CALLBACK(onAddButtonClicked), NULL);
  gtk_container_add(GTK_CONTAINER(align), buttonAddSurface);
  image = gtk_image_new_from_stock ("gtk-add", GTK_ICON_SIZE_BUTTON);
  gtk_container_add(GTK_CONTAINER(buttonAddSurface), image);

  buttonAddSpecial = gtk_button_new();
  gtk_widget_set_sensitive(buttonAddSpecial, FALSE);
  gtk_widget_set_tooltip_text(buttonAddSpecial,
		       _("Add many surfaces to the list of surfaces."));
  g_signal_connect(G_OBJECT(buttonAddSpecial), "clicked",
		   G_CALLBACK(onAddSpecialButtonClicked), NULL);
  gtk_box_pack_start(GTK_BOX(tree_vbox), buttonAddSpecial, FALSE, FALSE, 2);
  image = gtk_image_new_from_stock ("gtk-execute", GTK_ICON_SIZE_BUTTON);
  gtk_container_add(GTK_CONTAINER(buttonAddSpecial), image);

  align = gtk_alignment_new(0.5, 0., 0., 0.);
  gtk_box_pack_start(GTK_BOX(tree_vbox), align, TRUE, TRUE, 2);
  buttonRemoveSurface = gtk_button_new ();
  gtk_widget_set_sensitive(buttonRemoveSurface, FALSE);
  gtk_widget_set_tooltip_text(buttonRemoveSurface,
		       _("Remove selected surface or file."));
  g_signal_connect(G_OBJECT(buttonRemoveSurface), "clicked",
		   G_CALLBACK(onRemoveButtonClicked), NULL);
  gtk_container_add(GTK_CONTAINER(align), buttonRemoveSurface);
  image = gtk_image_new_from_stock ("gtk-remove", GTK_ICON_SIZE_BUTTON);
  gtk_container_add (GTK_CONTAINER(buttonRemoveSurface), image);

  /* The surfaces tool line. */
  gtk_box_pack_start(GTK_BOX(isosurfaces_gtk_vbox), h_box3, FALSE, FALSE, 0);
  gtk_widget_set_tooltip_text(play_stop, _("Starts/stops showing isosurfaces at specified rate"));
  gtk_box_pack_start(GTK_BOX(h_box3), play_stop, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(h_box3), gtk_label_new(_("@")), FALSE, FALSE, 0);
  gtk_widget_set_tooltip_text(time_interval, _("Selects rate to show isosurfaces"));
  gtk_entry_set_width_chars(GTK_ENTRY(time_interval), 3);
  gtk_box_pack_start(GTK_BOX(h_box3), time_interval, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(h_box3), gtk_label_new(_("ms")), FALSE, FALSE, 0);

  valueIO = visu_ui_value_io_new(visu_ui_panel_getContainerWindow(VISU_UI_PANEL(panelSurfaces)),
		       _("Import iso-values from an existing XML file."),
		       _("Export iso-values to the current XML file."),
		       _("Export iso-values to a new XML file."));
  visu_ui_value_io_connectOnOpen(VISU_UI_VALUE_IO(valueIO), loadXMLFile);
  visu_ui_value_io_connectOnSave(VISU_UI_VALUE_IO(valueIO),
                                 visu_ui_panel_surfaces_exportXMLFile);
  align = gtk_alignment_new(1., 0.5, 1., 0.);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 0, 0, 20, 0);
  gtk_container_add(GTK_CONTAINER(align), valueIO);
  gtk_box_pack_end(GTK_BOX(h_box3), align, TRUE, TRUE, 0);

  /* The option line. */
  h_box3 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(isosurfaces_gtk_vbox), h_box3, FALSE, FALSE, 0);

  checkDrawIntra = gtk_check_button_new_with_mnemonic(_("Draw _intra surfaces"));
  gtk_widget_set_tooltip_text(checkDrawIntra,
		       _("Draw the interior of iso-surfaces with the complementary colour."));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkDrawIntra),
			       visu_gl_ext_surfaces_getDrawIntra(visu_gl_ext_surfaces_getDefault()));
  g_signal_connect(G_OBJECT(checkDrawIntra), "toggled",
		   G_CALLBACK(onDrawIntraChanged), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(h_box3), checkDrawIntra, FALSE, FALSE, 0);

  gtk_box_pack_end(GTK_BOX(h_box3), auto_reorder, FALSE, FALSE, 0);
  gtk_widget_set_tooltip_text(auto_reorder,
			_("Automatically re-orders surfaces in back to front order"
			  " whenever camera is modified (can be slow but get rid of"
			  " transparency problems). This has no effect if the"
			  " transparency option of the OpenGL panel is set"));

  gtk_widget_show_all(vbox);

  gtk_container_add(GTK_CONTAINER(visu_ui_panel), vbox);
}
/**
 * visu_ui_panel_surfaces_parseXMLFile:
 * @filename: a location to read the XML data.
 * @error: a location to store possible error.
 *
 * This routine reads an XML file and setup the resources and the
 * isovalues to the selected row accordingly.
 *
 * Returns: TRUE on success.
 */
gboolean visu_ui_panel_surfaces_parseXMLFile(const gchar *filename, GError **error)
{
  gboolean valid;
  gchar *path;
  GtkTreeIter iter;
  VisuSurfaces *surfaces;
  VisuScalarField *field;
  int type;
  /* VisuData *dataObj; */
  
  g_return_val_if_fail(getSelectedRow(&iter), FALSE);

  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list),
		     &iter, TYPE_COLUMN, &type,
		     DATA_SURF_COLUMN, &surfaces,
		     DATA_FIELD_COLUMN, &field, -1);
  g_return_val_if_fail(type == SURFACE_TYPE_FILE_DENPOT, FALSE);
  
  path = tool_path_normalize(filename);
  valid = visu_surfaces_parseXMLFile(filename, &surfaces, field, error);
  if (valid)
    {
      gtk_tree_store_set(isosurfaces_data_list, &iter,
			 DATA_SURF_COLUMN, surfaces, -1);
      _update_surf_at_iter(&iter, surfaces);

      /* dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelSurfaces)); */
      /* g_return_val_if_fail(dataObj, FALSE); */

      /* isosurfaces_rebuild_gl_list(visu_data_getBox(dataObj)); */
    }
  g_free(path);

  return valid;
}
static gboolean loadXMLFile(const gchar *filename, GError **error)
{
  if (visu_ui_panel_surfaces_parseXMLFile(filename, error))
    {
      /* Update the sensitivity. */
      visu_ui_value_io_setSensitiveSave(VISU_UI_VALUE_IO(valueIO), TRUE);
      /* We ask for redraw. */
      VISU_REDRAW_ADD;
      return TRUE;
    }
  else
    return FALSE;
}
/**
 * visu_ui_panel_surfaces_exportXMLFile:
 * @filename: a filename to export to.
 * @error: (allow-none): a location to store an error.
 *
 * Export to @filename the list of isosurfaces values of the selected
 * scalar-field file.
 *
 * Since: 3.7
 *
 * Returns: TRUE if everything goes right.
 **/
gboolean visu_ui_panel_surfaces_exportXMLFile(const gchar *filename, GError **error)
{
  GtkTreeIter iter, child;
  int type, n, id;
  float *values;
  gboolean valid;
  VisuSurfacesResources **res;
  VisuSurfaces *surf;

  if (!getSelectedRow(&iter))
    return TRUE;

  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list),
		     &iter, TYPE_COLUMN, &type,
		     DATA_SURF_COLUMN, &surf, -1);
  g_return_val_if_fail(type == SURFACE_TYPE_FILE_DENPOT && surf, FALSE);
  n = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(isosurfaces_data_list), &iter);
  values = g_malloc(sizeof(float) * n);
  res = g_malloc(sizeof(VisuSurfacesResources*) * n);
  valid = gtk_tree_model_iter_children(GTK_TREE_MODEL(isosurfaces_data_list),
				       &child, &iter);
  n = 0;
  while (valid)
    {
      gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list),
			 &child, POTENTIAL_COLUMN, values + n,
			 NUMBER_COLUMN, &id, -1);
      res[n] = visu_surfaces_getResourceById(surf, id);
      valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(isosurfaces_data_list), &child);
      n += 1;
    }
  
  valid = visu_surfaces_exportXMLFile(filename, values, res, n, error);

  g_free(res);
  g_free(values);

  return valid;
}


/* static VisuSurfaces** getAllVisuSurfacesList() */
/* { */
/*   int n; */
/*   gboolean valid; */
/*   GtkTreeIter iter; */
/*   VisuSurfaces **surfaces; */

/*   n = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(isosurfaces_data_list), */
/* 				     (GtkTreeIter*)0); */
/*   surfaces = g_malloc(sizeof(VisuSurfaces*) * (n + 1)); */
/*   n = 0; */
/*   valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(isosurfaces_data_list), &iter); */
/*   while (valid) */
/*     { */
/*       gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), */
/* 			 &iter, DATA_SURF_COLUMN, surfaces + n, -1); */
/*       /\* In case the surface n is NULL, we don't count it. *\/ */
/*       if (surfaces[n]) */
/* 	n += 1; */
/*       valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(isosurfaces_data_list), &iter); */
/*     } */
/*   surfaces[n] = (VisuSurfaces*)0; */
/*   DBG_fprintf(stderr, "Panel VisuSurfaces: get list of all surfaces (%d).\n", n); */
/*   return surfaces; */
/* } */

static void onSizeChanged(VisuBox *box, gfloat extens _U_, gpointer user_data _U_)
{
  gboolean valid;
  GtkTreeIter iter;
  VisuScalarField *field;
  VisuSurfaces *surf;

  DBG_fprintf(stderr, "Panel VisuSurfaces: caught a 'SizeChanged' signal.\n");

  /* We recompute the scalar fields and isosurfaces if
     the fitToBox flag is TRUE. */
  if (fitToBox)
    for (valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(isosurfaces_data_list), &iter);
         valid;
         valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(isosurfaces_data_list), &iter))
      {
        gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), &iter,
                           DATA_FIELD_COLUMN, &field,
                           DATA_SURF_COLUMN, &surf, -1);
        if (surf)
          visu_boxed_setBox(VISU_BOXED(surf), VISU_BOXED(box), TRUE);
        if (field)
          visu_boxed_setBox(VISU_BOXED(field), VISU_BOXED(box), FALSE);
      }
}

static void onDrawIntraChanged(GtkToggleButton *button, gpointer data _U_)
{
  if (disableCallbacks)
    return;

  if (visu_gl_ext_surfaces_setDrawIntra(visu_gl_ext_surfaces_getDefault(),
                                        gtk_toggle_button_get_active(button)))
    {
      visu_gl_ext_surfaces_draw(visu_gl_ext_surfaces_getDefault());
      VISU_REDRAW_ADD;
    }
}

static void onResourcesChanged(GObject *object _U_, VisuData *dataObj _U_,
			       gpointer data _U_)
{
  /* We change the check boxes. */
  disableCallbacks = TRUE;
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkDrawIntra),
			       visu_gl_ext_surfaces_getDrawIntra(visu_gl_ext_surfaces_getDefault()));
  disableCallbacks = FALSE;
}

/* Callback to set the interface insensitive whenever a file hasn't been loaded. */
static void onDataReady(VisuObject *visu _U_, VisuData *dataObj,
                        VisuGlView *view _U_, gpointer data _U_)
{
  if (dataObj)
    {
      DBG_fprintf(stderr, "Panel VisuSurfaces: caught 'dataRendered' signal,"
                  " attaching signals.\n");
      hide_signal =
        g_signal_connect(G_OBJECT(dataObj), "AskForShowHide",
                         G_CALLBACK(onAskForHideNodes), (gpointer)0);
      box_signal =
        g_signal_connect(G_OBJECT(visu_boxed_getBox(VISU_BOXED(dataObj))), "SizeChanged",
                         G_CALLBACK(onSizeChanged), (gpointer)0);
    }
}
static void onDataNotReady(VisuObject *visu _U_, VisuData *dataObj,
                           VisuGlView *view _U_, gpointer data _U_)
{
  g_signal_handler_disconnect(G_OBJECT(dataObj), hide_signal);
  g_signal_handler_disconnect(G_OBJECT(visu_boxed_getBox(VISU_BOXED(dataObj))), box_signal);
}

static void onTreeSelectionChanged(GtkTreeSelection *tree, gpointer data _U_)
{
  gboolean res;
  GtkTreeModel *model;
  GtkTreeIter iter;
  int type;
  gboolean empty, hasChildren;

  DBG_fprintf(stderr, "Panel VisuSurfaces: catch 'changed' signal from "
	      "the gtkTreeSelection.\n");

  res = gtk_tree_selection_get_selected(tree, &model, &iter);
  if (res)
    {
      gtk_tree_model_get(model, &iter, TYPE_COLUMN, &type, -1);
      hasChildren = (type == SURFACE_TYPE_FILE_DENPOT) &&
	gtk_tree_model_iter_has_child(GTK_TREE_MODEL(isosurfaces_data_list), &iter);
    }
  else
    {
      type = -1;
      hasChildren = FALSE;
    }
    
  empty = !gtk_tree_model_get_iter_first(GTK_TREE_MODEL(isosurfaces_data_list), &iter);

  gtk_widget_set_sensitive(buttonRemoveSurface, (type >= 0));
  gtk_widget_set_sensitive(buttonAddSurface, (type == SURFACE_TYPE_FILE_DENPOT));
  gtk_widget_set_sensitive(buttonAddSpecial, (type == SURFACE_TYPE_FILE_DENPOT));
  gtk_widget_set_sensitive(buttonEdit, !empty);
  visu_ui_value_io_setSensitiveOpen(VISU_UI_VALUE_IO(valueIO), (type == SURFACE_TYPE_FILE_DENPOT));
  visu_ui_value_io_setSensitiveSave(VISU_UI_VALUE_IO(valueIO), (type == SURFACE_TYPE_FILE_DENPOT &&
					       hasChildren));

  if (GTK_IS_WINDOW(edit_window))
    panelIsosurfacesUpdate_surfaceProperties();
}

static void onAddButtonClicked(GtkButton *button _U_, gpointer data _U_)
{
  gboolean valid;
  GtkTreeIter iter;

  valid = getSelectedRow(&iter);
  g_return_if_fail(valid);

  visu_ui_panel_surfaces_computeAuto(&iter);
}
static void onAddSpecialButtonClicked(GtkButton *button _U_, gpointer data _U_)
{
  gboolean rebuild, valid;
  GtkTreeIter iter;
  float *values;
  int nbValues;
  VisuSurfaces *surf;
  VisuScalarField *field;
  int type, i;
  double mimax[2];
  gchar *name;
  GtkWidget *dialog, *progress;
  char mess[50];
  GList *lst, *children;

  valid = getSelectedRow(&iter);
  g_return_if_fail(valid);

  surf = (VisuSurfaces*)0;
  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list),
		     &iter, TYPE_COLUMN, &type,
		     DATA_SURF_COLUMN, &surf,
		     DATA_FIELD_COLUMN, &field, -1);

  g_return_if_fail(type == SURFACE_TYPE_FILE_DENPOT && field);

  visu_scalar_field_getMinMax(field, mimax);

  values = (float*)0;
  name = (gchar*)0;
  dialog = visu_ui_panel_surfaces_generateValues(&nbValues, &values, &name,
					      (float)mimax[0], (float)mimax[1]);
  rebuild = FALSE;

  if (!dialog)
    return;

  progress = (GtkWidget*)0;
  children = gtk_container_get_children
    (GTK_CONTAINER(gtk_dialog_get_content_area(GTK_DIALOG(dialog))));
  for (lst = children; lst; lst = lst->next)
       
    {
      gtk_widget_set_sensitive(GTK_WIDGET(lst->data), FALSE);
      if (GTK_IS_PROGRESS_BAR(lst->data))
	progress = GTK_WIDGET(lst->data);
    }
  g_list_free(children);
  for (i = 0; i < nbValues; i++)
    {
      sprintf(mess, "%d/%d", i + 1, nbValues);
      gtk_progress_bar_set_text(GTK_PROGRESS_BAR(progress), mess);
      gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(progress),
				    (float)i/(float)nbValues);
      visu_ui_wait();
      rebuild = panel_isosurfaces_add(&iter, TRUE, values[i], name, &surf) || rebuild;
    }
  gtk_widget_destroy(dialog);

  if (values)
    g_free(values);
  if (name)
    g_free(name);

  visu_ui_value_io_setSensitiveSave(VISU_UI_VALUE_IO(valueIO), TRUE);

  if (rebuild)
    {
      _update_surf_at_iter(&iter, surf);
      VISU_REDRAW_ADD;
    }
}
static void onRemoveButtonClicked(GtkButton *button _U_, gpointer data _U_)
{
  gboolean rebuild, valid;
  GtkTreeIter iter;

  valid = getSelectedRow(&iter);
  g_return_if_fail(valid);

  rebuild = panel_isosurfaces_remove(&iter);
  if (rebuild)
    VISU_REDRAW_ADD;
}
static void onNameEdited(GtkCellRendererText *cellrenderertext _U_,
			 gchar *path, gchar *text, gpointer user_data _U_)
{
  GtkTreeIter iter;
  gboolean status, new;
  int type, surfId;
  VisuSurfaces *surf;
  VisuSurfacesResources *res, *res_old;
  GdkPixbuf *pixbuf;
  gchar *display_name;
  VisuPlane **planes;

  if (!strcmp(text, VISU_UI_SURFACE_NAME_STR))
    return;

  status = gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(isosurfaces_data_list),
					       &iter, path);
  g_return_if_fail(status);

  /* We get the VisuSurfaces of the edited object. */
  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), &iter,
		     DATA_SURF_COLUMN, &surf,
		     NUMBER_COLUMN, &surfId,
		     TYPE_COLUMN, &type, -1);
  g_return_if_fail(type == SURFACE_TYPE_SURF);
  g_return_if_fail(surf);

  /* Save previous resource. */
  res_old = visu_surfaces_getResourceById(surf, surfId);
  DBG_fprintf(stderr, "Panel VisuSurfaces: old resources %p (%s).\n",
	      (gpointer)res_old, res_old->surfnom);

  /* We change the resource of the edited surface. */
  res = visu_surfaces_resources_getFromName(text, &new);
  DBG_fprintf(stderr, "Panel VisuSurfaces: new resources %p (%s).\n",
	      (gpointer)res, res->surfnom);
  /* If the resource is new, we copy the value of previous
     resource into the new. */
  if (new)
    visu_surfaces_resources_copy(res, res_old);

  /* We set the new resource (may free the old one). */
  visu_surfaces_setResource(surf, surfId, res);
  
  if (!new)
    {
      /* We need to change the visibility and color of the
	 surface since the new resource is different from old one. */
      pixbuf = tool_color_get_stamp(res->color, TRUE);
      gtk_tree_store_set(isosurfaces_data_list, &iter,
			 PIXBUF_COLUMN, (gpointer)pixbuf,
			 MASKING_COLUMN, res->sensitiveToPlanes,
			 SHOW_COLUMN, res->rendered, -1);
      g_object_unref(pixbuf);
      /* Hide the polygons in the surface. */
      planes = visu_ui_panel_planes_getAll(TRUE);
      if (planes[0])
	visu_surfaces_hide(surf, planes);
      g_free(planes);
    }
  if (!res->surfnom)
    display_name = VISU_UI_SURFACE_NAME_CHOOSE;
  else
    display_name = res->surfnom;
  gtk_tree_store_set(isosurfaces_data_list, &iter,
		     DISPLAY_NAME_COLUMN, display_name,
		     NAME_COLUMN, text, -1);

  VISU_REDRAW_ADD;
}
static void onPotentialValueEdited(GtkCellRendererText *cellrenderertext _U_,
				   gchar *path, gchar *text, gpointer user_data _U_)
{
  GtkTreeIter iterFather;
  GtkTreeIter iter;
  GtkTreePath *treePath;
  float new_value = atof(text);
  VisuSurfaces *surf;
  VisuScalarField *field;
  int type, idSurf;
  double minmax[2];
  gboolean status, empty, new;
  gchar *name;
  VisuSurfacesResources *res, *res_old;

  status = gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(isosurfaces_data_list),
					       &iter, path);
  g_return_if_fail(status);

  gtk_tree_model_iter_parent(GTK_TREE_MODEL(isosurfaces_data_list),
			     &iterFather, &iter);
  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), &iterFather,
		     DATA_FIELD_COLUMN, &field,
		     DATA_SURF_COLUMN, &surf,
		     TYPE_COLUMN, &type, -1);
  g_return_if_fail(type == SURFACE_TYPE_FILE_DENPOT);
  g_return_if_fail(surf && field);

  visu_scalar_field_getMinMax(field, minmax);
  g_return_if_fail(new_value >= minmax[0] && new_value <= minmax[1]);

  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), &iter,
		     NAME_COLUMN, &name,
		     NUMBER_COLUMN, &idSurf, -1);
  /* We save the ressource of the surface if it is a private resource. */
  res = visu_surfaces_getResourceById(surf, idSurf);
  res_old = visu_surfaces_resources_getFromName(res->surfnom, &new);
  DBG_fprintf(stderr, "Panel VisuSurfaces: remove surface with id %d ('%s').\n",
	      idSurf, res->surfnom);

  /* If the resource is new, we copy the value of previous
     resource into the new. */
  if (new)
    visu_surfaces_resources_copy(res_old, res);
  else
    res_old = (VisuSurfacesResources*)0;

  /* We remove the surface. */
  empty = visu_surfaces_remove(surf, idSurf);
  if (empty)
    {
      visu_gl_ext_surfaces_remove(visu_gl_ext_surfaces_getDefault(), surf);
      g_object_unref(surf);
      surf = (VisuSurfaces*)0;
    }

  /* We add the new surface. */
  DBG_fprintf(stderr, "Panel VisuSurfaces: create surface with id %d.\n", idSurf);
  status = visu_surfaces_createFromScalarField(&surf, field, new_value, idSurf, name);

  /* We get and copy the surface resource if the old one was private. */
  if (res_old)
    {
      res = visu_surfaces_getResourceById(surf, idSurf);
      visu_surfaces_resources_copy(res, res_old);
      visu_surfaces_resources_free(res_old);
    }

  /* We read the surf pointer if this one has changed. */
  gtk_tree_store_set(isosurfaces_data_list, &iterFather,
		     DATA_SURF_COLUMN, surf, -1);

  _update_surf_at_iter(&iterFather, surf);

  /* We reselect the right iter. */
  treePath = gtk_tree_path_new_from_string(path);
  gtk_tree_selection_select_path
    (gtk_tree_view_get_selection(GTK_TREE_VIEW(isosurfaces_tree_model)), treePath);
  gtk_tree_path_free(treePath);

  if (empty)
    visu_gl_ext_surfaces_add(visu_gl_ext_surfaces_getDefault(), surf);
  visu_gl_ext_surfaces_draw(visu_gl_ext_surfaces_getDefault());

  VISU_REDRAW_ADD;
}

static void onReorderToggled(GtkToggleButton *toggle, gpointer data _U_)
{
  visu_gl_ext_surfaces_setOnTheFlyOrdering(visu_gl_ext_surfaces_getDefault(),
                                           (gtk_toggle_button_get_active(toggle))?visu_ui_panel_getView(VISU_UI_PANEL(panelSurfaces)):(VisuGlView*)0);
}

static void onDataFocused(GObject *visu _U_, VisuData *dataObj, gpointer data _U_)
{
  GtkTreeIter iter;
  gboolean valid, rebuild, *flag;
  int type, i, *ids, n;
  VisuSurfaces *surf;
  VisuScalarField *field;
  gchar *fileCpy, *fileExtPosition;
  GString *dataFile;
  float isoValues[256], *potentialData;
  int nIsoValues;
  const gchar *isoNames[256];
  double minmax[2];
  
  DBG_fprintf(stderr, "Panel Surfaces: caught the 'DataFocused'"
	      " signal.\n | setting sensititvity.\n");
  if (dataObj)
    {
      gtk_widget_set_sensitive(isosurfaces_gtk_vbox, TRUE);
      gtk_widget_set_sensitive(buttonOpen, TRUE);
      gtk_widget_set_sensitive(buttonConvert, TRUE);
    }
  else
    {
      gtk_widget_set_sensitive(isosurfaces_gtk_vbox, FALSE);
      gtk_widget_set_sensitive(buttonOpen, FALSE);
      gtk_widget_set_sensitive(buttonConvert, FALSE);
    }

  DBG_fprintf(stderr, " | update list of surfaces and fields.\n");
  /* Store all isovalues into isoValues array. */
  nIsoValues = 0;
  /* Remove all scalar-fields. */
  rebuild = FALSE;
  valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(isosurfaces_data_list), &iter);
  while (valid)
    {
      gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), &iter,
                         DATA_FIELD_COLUMN, &field,
                         DATA_SURF_COLUMN, &surf,
                         TYPE_COLUMN, &type, -1);
      g_return_if_fail(type == SURFACE_TYPE_FILE_DENPOT ||
		       type == SURFACE_TYPE_FILE_SURF);
      
      /* If the data is a field, we store all its values and names. */
      if (surf && type == SURFACE_TYPE_FILE_DENPOT)
	{
	  ids = visu_surfaces_getSortedById(surf);
	  n = visu_surfaces_getN(surf);
	  potentialData = visu_surfaces_getPropertyFloat
	    (surf, VISU_SURFACES_PROPERTY_POTENTIAL);
	  for (i = 0; i < n; i++)
	    {
	      isoValues[nIsoValues] = potentialData[ids[i]];
	      /* If a name is given, we can keep it since it won't be
		 freed. */
	      isoNames[nIsoValues] = visu_surfaces_getName(surf, ids[i]);
	      nIsoValues = MIN(255, nIsoValues + 1);
	    }
	  g_free(ids);
	}
      DBG_fprintf(stderr, " | remove a non persistent scalar field.\n");
      if (surf)
        {
          visu_gl_ext_surfaces_remove(visu_gl_ext_surfaces_getDefault(), surf);
          g_object_unref(surf);
        }
      if (field)
	g_object_unref(field);
      rebuild = TRUE;
      valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(isosurfaces_data_list), &iter);
    }
  gtk_tree_store_clear(GTK_TREE_STORE(isosurfaces_data_list));
  gtk_list_store_clear(GTK_LIST_STORE(fields_data_list));
  
  /* If the flag of autoload is not checked or no data is available, we return. */
  if (!dataObj || !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkAutoLoad)))
    {
      /* If rebuild is TRUE, we rebuild the list to empty. */
      if (rebuild)
        {
          DBG_fprintf(stderr, "Panel VisuSurfaces: reorder and build the OpenGL list.\n");
          visu_gl_ext_surfaces_draw(visu_gl_ext_surfaces_getDefault());
        }
      return;
    }

  /* Read if the new file has the VISU_SCALAR_FIELD_DEFINED_IN_STRUCT_FILE flag. */
  flag = (gboolean*)g_object_get_data(G_OBJECT(dataObj),
				      VISU_SCALAR_FIELD_DEFINED_IN_STRUCT_FILE);
  fileCpy = g_strdup(visu_data_getFile(dataObj, 0, (ToolFileFormat**)0));
  if (!flag)
    {
      /* We try to find a surf file with the right name. */
      fileExtPosition = g_strrstr(fileCpy, ".");
      if (fileExtPosition)
	*fileExtPosition = '\0';
      dataFile = g_string_new("");
      g_string_printf(dataFile, "%s.%s", fileCpy, "surf");
      g_free(fileCpy);
      DBG_fprintf(stderr, "Panel VisuSurfaces: try to load a new surface file"
		  " ('%s') from the name of the rendered file.\n", dataFile->str);
      fileCpy = dataFile->str;
      g_string_free(dataFile, FALSE);
      if (!g_file_test(fileCpy, G_FILE_TEST_EXISTS))
	{
          g_free(fileCpy);
	  fileCpy = (gchar*)0;
          DBG_fprintf(stderr, " | file doesn't exist.\n");
        }
    }
  if (fileCpy)
    {
      rebuild = visu_ui_panel_surfaces_loadFile(fileCpy, visu_boxed_getBox(VISU_BOXED(dataObj)),
                                                (GHashTable*)0, (VisuScalarFieldMethod*)0);
      g_free(fileCpy);
      /* We create the surfaces for the values readed from
	 isoValues. */
      valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(isosurfaces_data_list), &iter);
      if (valid)
	{
	  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), &iter,
			     DATA_FIELD_COLUMN, &field,
			     TYPE_COLUMN, &type, -1);
	  if (type == SURFACE_TYPE_FILE_DENPOT)
	    {
	      visu_scalar_field_getMinMax(field, minmax);
	      for (i = 0; i < nIsoValues; i++)
		if (isoValues[i] > minmax[0] && isoValues[i] < minmax[1])
		  panel_isosurfaces_add(&iter, TRUE, isoValues[i], isoNames[i],
					&surf);
	    }
          _update_surf_at_iter(&iter, surf);
	}
    }
}
/**
 * visu_ui_panel_surfaces_hide:
 * @planes: an array of planes to be applied (NULL terminated).
 *
 * Must be called after the initialisation of the plane subpanel. It applies the
 * masking scheme of planes on current surfaces.
 *
 * Returns: TRUE if the surface should be rebuilt.
 */
gboolean visu_ui_panel_surfaces_hide(VisuPlane ** planes)
{
  gboolean valid;
  GtkTreeIter iter;
  VisuSurfaces *surf;
  gboolean rebuild;

  DBG_fprintf(stderr, "Panel VisuSurfaces: apply hiding scheme.\n");
  g_return_val_if_fail(planes, FALSE);

  /* For each polygons, we compute if it is masked by a plane or not. */
  rebuild = FALSE;
  valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(isosurfaces_data_list), &iter);
  while (valid)
    {
      gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), &iter,
			 DATA_SURF_COLUMN, &surf, -1);
      if (surf)
	rebuild = visu_surfaces_hide(surf, planes) || rebuild;
      valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(isosurfaces_data_list), &iter);
    }
  
  return rebuild;
}

static void onAskForHideNodes(VisuData *visuData _U_, gboolean *redraw,
			      gpointer data _U_)
{
  VisuPlane **planes;

  DBG_fprintf(stderr, "Panel VisuSurfaces: caught the 'AskForShowHide' signal.\n");
  planes = visu_ui_panel_planes_getAll(TRUE);
  *redraw = visu_ui_panel_surfaces_hide(planes) || *redraw;
  g_free(planes);
}

static gboolean onPopupClicked(GtkWidget *widget, GdkEventButton *event _U_,
			       gpointer user_data _U_)
{
  gtk_widget_destroy(widget);
  return FALSE;
}
static gboolean onTreeViewClicked(GtkWidget *widget, GdkEventButton *event,
				  gpointer user_data _U_)
{
  gboolean valid;
  GtkTreePath *path;
  GtkTreeIter iter;
  int type, row;
  VisuScalarField *field;
  GtkWidget *win, *vbox, *wd, *table, *align;
  gchar *name, *markup;
  const gchar *comment;
  gint x, y;
  GList *options, *tmplst;

  if (event->button != 3)
    return FALSE;

  DBG_fprintf(stderr, "Panel VisuSurfaces: right clic detected.\n");

  valid = gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(widget), (gint)event->x,
					(gint)event->y, &path, (GtkTreeViewColumn**)0,
                                        (gint*)0, (gint*)0);
  if (!valid)
    return FALSE;

  valid = gtk_tree_model_get_iter(GTK_TREE_MODEL(isosurfaces_data_list), &iter, path);
  gtk_tree_path_free(path);
  if (!valid)
    return FALSE;

  gtk_tree_model_get(GTK_TREE_MODEL(isosurfaces_data_list), &iter,
		     DATA_FIELD_COLUMN, &field,
		     TYPE_COLUMN, &type, -1);
  if (type != SURFACE_TYPE_FILE_DENPOT)
    return FALSE;

  g_return_val_if_fail(field, FALSE);

  /* Ok, now, we have the scalar field.
     We create a popup window with info. */
  win = gtk_window_new(GTK_WINDOW_POPUP);
  gtk_widget_set_events(win, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_KEY_PRESS_MASK);
  gtk_grab_add(win);
#if GTK_MAJOR_VERSION > 2
  gdk_device_get_position(event->device, NULL, &x, &y);
#else
  gdk_display_get_pointer(gdk_screen_get_display(gtk_widget_get_screen(widget)),
			  (GdkScreen**)0, &x, &y, NULL);
#endif

  gtk_window_move(GTK_WINDOW(win), x, y);
  g_signal_connect(G_OBJECT(win), "button-press-event",
		   G_CALLBACK(onPopupClicked), (gpointer)0);
  gtk_container_set_border_width(GTK_CONTAINER(win), 20);
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(win), vbox);

  /* Print the File name. */
  name = g_path_get_basename(visu_scalar_field_getFilename(field));
  markup = g_markup_printf_escaped("<span size=\"larger\"><b>%s</b></span>", name);
  g_free(name);
  wd = gtk_label_new("");
  gtk_misc_set_alignment(GTK_MISC(wd), 0., 0.5);
  gtk_label_set_markup(GTK_LABEL(wd), markup);
  g_free(markup);
  gtk_box_pack_start(GTK_BOX(vbox), wd, FALSE, FALSE, 0);

  /* Print a list of all interesting characteristics of the file. */
  align = gtk_alignment_new(0., 0.5, 1., 1.);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 15, 0, 15, 0);
  gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, FALSE, 0);
  options = visu_scalar_field_getAllOptions(field);
  table = gtk_table_new(1 + g_list_length(options), 2, FALSE);
  gtk_container_add(GTK_CONTAINER(align), table);

  wd = gtk_label_new("");
  gtk_misc_set_alignment(GTK_MISC(wd), 1., 0.5);
  gtk_label_set_markup(GTK_LABEL(wd), _("<span style=\"italic\">Comment:</span>"));
  gtk_table_attach(GTK_TABLE(table), wd, 0, 1, 0, 1, GTK_FILL, GTK_SHRINK, 5, 0);

  comment = visu_scalar_field_getCommentary(field);
  if (comment)
    wd = gtk_label_new(comment);
  else
    wd = gtk_label_new(_("None"));
  gtk_misc_set_alignment(GTK_MISC(wd), 0., 0.5);
  gtk_table_attach(GTK_TABLE(table), wd, 1, 2, 0, 1, GTK_EXPAND | GTK_FILL, GTK_SHRINK, 5, 0);
  
  row = 1;
  tmplst = options;
  while(tmplst)
    {
      wd = gtk_label_new("");
      gtk_misc_set_alignment(GTK_MISC(wd), 1., 0.5);
      markup = g_markup_printf_escaped("<span style=\"italic\">%s:</span>",
                                       tool_option_getName((ToolOption*)tmplst->data));
      gtk_label_set_markup(GTK_LABEL(wd), markup);
      g_free(markup);
      gtk_table_attach(GTK_TABLE(table), wd, 0, 1, row, row + 1, GTK_FILL, GTK_SHRINK, 5, 0);

      wd = gtk_label_new("");
      gtk_misc_set_alignment(GTK_MISC(wd), 0., 0.5);
      markup = tool_option_getValueAndLabel((ToolOption*)tmplst->data);
      gtk_label_set_markup(GTK_LABEL(wd), markup);
      g_free(markup);
      gtk_table_attach(GTK_TABLE(table), wd, 1, 2, row, row + 1, GTK_EXPAND | GTK_FILL, GTK_SHRINK, 5, 0);
      
      row += 1;
      tmplst = g_list_next(tmplst);
    }
  g_list_free(options);

  gtk_widget_show_all(win);

  return FALSE;
}
static void onTreeViewActivated(GtkTreeView *tree_view, GtkTreePath *path,
				GtkTreeViewColumn *column, gpointer user_data _U_)
{
  GtkTreeIter iter;

  /* We do something only if the color column is activated for a
     surface. */
  if (column != colorColumn)
    return;
  if (gtk_tree_path_get_depth(path) < 2)
    return;

  if (gtk_tree_model_get_iter(gtk_tree_view_get_model(tree_view), &iter, path))
    visu_ui_panel_surfaces_editProperties(&iter);
}
/**
 * visu_ui_panel_surfaces_getFields:
 * 
 * This method gives read access to the #GtkListStore used to store
 * the scalar field files.
 *
 * Returns: (transfer none): the #GtkListStore used by this panel to
 * store its scalar fields. It should be considered read-only.
 */
GtkListStore* visu_ui_panel_surfaces_getFields()
{
  return fields_data_list;
}


struct GenerateWidgets_struct
{
  GtkWidget *spin_start;
  GtkWidget *spin_end;
  GtkWidget *spin_step;
  GtkWidget *spin_delta;
};

static void onSpecialModeToggled(GtkToggleButton *button, gpointer data)
{
  gtk_widget_set_sensitive(GTK_WIDGET(data), gtk_toggle_button_get_active(button));
}
static void onGenerateChanged(GtkSpinButton *spin, gpointer data)
{
  struct GenerateWidgets_struct *wds;
  float delta;

  wds = (struct GenerateWidgets_struct*)data;
  g_return_if_fail(wds);

  delta = gtk_spin_button_get_value(GTK_SPIN_BUTTON(wds->spin_end)) -
    gtk_spin_button_get_value(GTK_SPIN_BUTTON(wds->spin_start));
  if (spin == GTK_SPIN_BUTTON(wds->spin_step) ||
      spin != GTK_SPIN_BUTTON(wds->spin_delta))
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(wds->spin_delta), delta /
			      gtk_spin_button_get_value(GTK_SPIN_BUTTON
							(wds->spin_step)));
  if (spin == GTK_SPIN_BUTTON(wds->spin_delta) ||
      spin != GTK_SPIN_BUTTON(wds->spin_step))
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(wds->spin_step), delta /
			      gtk_spin_button_get_value(GTK_SPIN_BUTTON
							(wds->spin_delta)));
}
/**
 * visu_ui_panel_surfaces_generateValues:
 * @nbValues: a location of an integer to store the number of generated values ;
 * @values: a pointer on a float array. The target of this pointer must be
 *          NULL and it will be allocated after a call to this method. Use
 *          g_free() after use to free it.
 * @name: a pointer to store a name. The target of this pointer must be
 *        NULL on enter. It is associated only if a name is given.
 * @minVal: the minimum value for the range ;
 * @maxVal: the maximum value for the range.
 * 
 * This method opens a little dialog window that is made to help the
 * user enter a list of values for creation of iso-surfaces. These values
 * are generated between @minVal and @maxVal.
 *
 * Returns: (transfer full): the dialog widget.
 */
GtkWidget* visu_ui_panel_surfaces_generateValues(int *nbValues, float **values,
					      gchar **name, float minVal,
					      float maxVal)
{
  GtkWidget *dialog, *table;
  float value, incr;
#define MAX_NB_VALUES 99

  struct GenerateWidgets_struct wds;
  GtkWidget *spin_start;
  GtkWidget *spin_end;
  GtkWidget *spin_step;
  GtkWidget *spin_delta;
  GtkWidget *radioStep, *radioDelta;
  GtkWidget *entry, *progress;

  GSList *radiobuttonCycle_group;

  g_return_val_if_fail(nbValues && values &&
		       !*values && name && !*name, (GtkWidget*)0);

  dialog = gtk_dialog_new_with_buttons (_("Generate iso-values"),
					NULL, GTK_DIALOG_MODAL,
					GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					GTK_STOCK_OK, GTK_RESPONSE_ACCEPT, NULL);

  table = gtk_table_new(6, 2, FALSE);

  spin_start = gtk_spin_button_new_with_range(minVal, maxVal, 0.0000001);
  g_signal_connect(G_OBJECT(spin_start), "value_changed",
		   G_CALLBACK(onGenerateChanged), (gpointer)&wds);
  spin_end = gtk_spin_button_new_with_range(minVal, maxVal, 0.0000001);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spin_end), maxVal);
  g_signal_connect(G_OBJECT(spin_end), "value_changed",
		   G_CALLBACK(onGenerateChanged), (gpointer)&wds);
  spin_step = gtk_spin_button_new_with_range(2, MAX_NB_VALUES, 1);
  g_signal_connect(G_OBJECT(spin_step), "value_changed",
		   G_CALLBACK(onGenerateChanged), (gpointer)&wds);
  spin_delta = gtk_spin_button_new_with_range(0.000001, maxVal - minVal,
					      (maxVal - minVal) / 200);
  g_signal_connect(G_OBJECT(spin_delta), "value_changed",
		   G_CALLBACK(onGenerateChanged), (gpointer)&wds);
  wds.spin_start = spin_start;
  wds.spin_end   = spin_end;
  wds.spin_step  = spin_step;
  wds.spin_delta = spin_delta;

  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spin_step), 10);

  radioStep = gtk_radio_button_new_with_label(NULL, _("Number of steps:"));
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(radioStep), (GSList*)0);
  radiobuttonCycle_group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radioStep));
  gtk_table_attach(GTK_TABLE(table), radioStep, 0, 1, 2, 3,
                   (GtkAttachOptions)0, (GtkAttachOptions)0, 0, 0);
  g_signal_connect(G_OBJECT(radioStep), "toggled",
		   G_CALLBACK(onSpecialModeToggled), (gpointer)spin_step);

  radioDelta = gtk_radio_button_new_with_label(NULL, _("Delta of steps:"));
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(radioDelta), radiobuttonCycle_group);
  radiobuttonCycle_group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radioDelta));
  gtk_table_attach(GTK_TABLE(table), radioDelta, 1, 2, 2, 3,
                   (GtkAttachOptions)0, (GtkAttachOptions)0, 0, 0);
  g_signal_connect(G_OBJECT(radioDelta), "toggled",
		   G_CALLBACK(onSpecialModeToggled), (gpointer)spin_delta);

  entry = gtk_entry_new();

  progress = gtk_progress_bar_new();

  gtk_table_attach(GTK_TABLE(table), spin_step, 0, 1, 3, 4,
                   GTK_FILL | GTK_EXPAND, (GtkAttachOptions)0, 0, 0);
  gtk_table_attach(GTK_TABLE(table), spin_delta, 1, 2, 3, 4,
                   GTK_FILL | GTK_EXPAND, (GtkAttachOptions)0, 0, 0);

  gtk_table_attach(GTK_TABLE(table), gtk_label_new(_("Start:")), 0, 1, 0, 1,
                   GTK_FILL | GTK_EXPAND, (GtkAttachOptions)0, 0, 0);
  gtk_table_attach(GTK_TABLE(table), gtk_label_new(_("End:")), 1, 2, 0, 1,
                   GTK_FILL | GTK_EXPAND, (GtkAttachOptions)0, 0, 0);
  gtk_table_attach(GTK_TABLE(table), spin_start, 0, 1, 1, 2,
                   GTK_FILL | GTK_EXPAND, (GtkAttachOptions)0, 0, 0);
  gtk_table_attach(GTK_TABLE(table), spin_end, 1, 2, 1, 2,
                   GTK_FILL | GTK_EXPAND, (GtkAttachOptions)0, 0, 0);
  gtk_table_attach(GTK_TABLE(table), gtk_label_new(_("Name (optional):")), 0, 2, 4, 5,
                   GTK_FILL | GTK_EXPAND, (GtkAttachOptions)0, 0, 0);
  gtk_table_attach(GTK_TABLE(table), entry, 0, 2, 5, 6,
                   GTK_FILL | GTK_EXPAND, (GtkAttachOptions)0, 0, 0);

  gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG(dialog))),
		     table, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG(dialog))),
		     progress, FALSE, FALSE, 5);

  gtk_widget_set_sensitive(spin_delta, FALSE);

  gtk_widget_show_all(dialog);

  *values = (float*)0;
  *nbValues = 0;
  switch(gtk_dialog_run(GTK_DIALOG(dialog)))
    {
    case GTK_RESPONSE_ACCEPT:
      {
	float start = gtk_spin_button_get_value(GTK_SPIN_BUTTON(spin_start));
	float end = gtk_spin_button_get_value(GTK_SPIN_BUTTON(spin_end));
	int step = floor(gtk_spin_button_get_value(GTK_SPIN_BUTTON(spin_step)));

	*values = g_malloc(sizeof(float) * MAX_NB_VALUES);

	value = start;
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioStep)))
	  {
	    if (abs(start - minVal) < 1e-6)
	      step += 1;
	    incr = (end - start) / step;
	  }
	else
	  incr = gtk_spin_button_get_value(GTK_SPIN_BUTTON(spin_delta));
	if (abs(value - minVal) < 1e-6)
	  value += incr;
	while ((incr > 0 && (value - end) < -1e-6) ||
	       (incr < 0 && (value - end) > 1e-6))
	  {
	    if (value > minVal && value < maxVal)
	      {
		DBG_fprintf(stderr, "Panel VisuSurfaces: create a new value, %d %f\n",
			    *nbValues, value);
		(*values)[*nbValues] = value;
		*nbValues += 1;
	      }
	    value += incr;
	  }
	*name = g_strdup(gtk_entry_get_text(GTK_ENTRY(entry)));
      }
    default:
      return dialog;
    }
}

/**
 * visu_ui_panel_surfaces_init: (skip)
 *
 * Should be used in the list declared in externalModules.h to be loaded by
 * V_Sim on start-up. This routine will create the #VisuUiPanel where the iso-surfaces
 * stuff can be done, such as creating a surface, loading a scalar field,
 * changing the properties...
 *
 * Returns: a newly created #VisuUiPanel object.
 */
VisuUiPanel* visu_ui_panel_surfaces_init(VisuUiMain *ui)
{
  /* Long description */
  char *cl = _("Drawing iso-surfaces");
  /* Short description */
  char *tl = "Isosurfaces";

  panelSurfaces = visu_ui_panel_newWithIconFromPath("Panel_surfaces", cl, _(tl),
                                                    "stock-isosurfaces_20.png");
  visu_ui_panel_setDockable(VISU_UI_PANEL(panelSurfaces), TRUE);

  fields_data_list =  gtk_list_store_new(VISU_UI_SURFACES_FIELD_N_COLUMNS,
					 G_TYPE_STRING,
					 VISU_TYPE_SCALAR_FIELD);

  /* Create the widgets. */
  isosurfaces_create_gtk_interface(VISU_UI_PANEL(panelSurfaces));
  gtk_widget_set_sensitive(isosurfaces_gtk_vbox, FALSE);
  gtk_widget_set_sensitive(buttonOpen, FALSE);
  gtk_widget_set_sensitive(buttonConvert, FALSE);
  vboxColorur = (GtkWidget*)0;
  vboxToolShade  = (GtkWidget*)0;
  fitToBox   = TRUE;
  /* Add the signal for the vBoxVisuPlanes. */
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataRendered",
                   G_CALLBACK(onDataReady), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataUnRendered",
                   G_CALLBACK(onDataNotReady), (gpointer)0);
  g_signal_connect(G_OBJECT(ui), "DataFocused",
		   G_CALLBACK(onDataFocused), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "resourcesLoaded",
		   G_CALLBACK(onResourcesChanged), (gpointer)0);
  visu_gl_ext_surfaces_setOnObserveOrdering(visu_gl_ext_surfaces_getDefault(),
                                            visu_ui_rendering_window_class_getInteractive
                                            ());

  if(!panelSurfaces)
    return NULL;

  return VISU_UI_PANEL(panelSurfaces);
}
