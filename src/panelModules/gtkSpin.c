/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresses m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "gtkSpin.h"

#include <visu_gtk.h>
#include <visu_elements.h>
#include <renderingMethods/renderingSpin.h>
#include <openGLFunctions/view.h>
#include <coreTools/toolMatrix.h>
#include "gtkAtomic.h"
#include "panelElements.h"
#include "panelMethod.h"
#include <support.h>

#include <assert.h>
#include <string.h>
#include <math.h>

/**
 * SECTION:gtkSpin
 * @short_description: The gtk interface elements that can interfere
 * with renderingSpin parameters.
 *
 * <para>This is the gtk interface for all #VisuRenderingSpin module
 * parameters. They are split in two parts. The first part is placed
 * under the config tab and allows the user to modify the "global
 * properties" of the renderingSpin module such as cone's axe
 * orientation, and color repartition. The second part is under the
 * element panel and allows to modify each element resource. Size of
 * the arrows, and their shape can be modified.</para>
 */

#define LABEL_ARROW_SIZE      _("<i>Shape size and color properties:</i>")
#define LABEL_SPIN_POLICY     _("Drawing policy for spins with null modulus:")

void set_view(GtkWidget* button, gpointer data);

/* A widget to select a shape for the arrows */
static GtkWidget *gtkw_element_shape_number = NULL;
static GtkWidget *heightResourceSpin = NULL;
static GtkWidget *rheightResourceSpin = NULL;
static GtkWidget *lengthResourceSpin = NULL;
static GtkWidget *rlengthResourceSpin = NULL;
static GtkWidget *ratioElipsoidSpin = NULL;
static GtkWidget *lengthElipsoidSpin = NULL;
static GtkWidget *gtkw_cone_theta_angle = NULL;
static GtkWidget *gtkw_cone_phi_angle = NULL;
static GtkWidget *gtkw_color_wheel_angle = NULL;
static GtkWidget *gtkw_use_element_color = NULL;
static GtkWidget *gtkw_use_element_color_hat = NULL;
static GtkWidget *useElementForElipsoid;
static GtkWidget *gtkw_x = NULL, *gtkw_y = NULL, *gtkw_z = NULL; 
static GtkWidget *gtkw_set_view;
static GtkWidget *labelSizeProp;
static GtkWidget *vboxArrowShape, *vboxElipsoidShape;
static GtkWidget *radioPerTypeMod, *radioGlobalMod;
static GtkWidget *expandAtomic;
static GtkWidget *radioAlwaysSpin, *radioEmptySpin, *radioAtomicSpin;
static GtkWidget *labelAtomic;

static guint element_callback_ids[10];

static int need_redraw = 1;

static gulong configResourcesLoadedId;

struct FileChooserSpin_struct
{
  GtkLabel* label[2];
  GtkRadioButton* radio[2];
  gchar* files[2];
  GtkFileChooserDialog *fileChooserDialog;
  GtkWidget* apply[2];
  int fileKind;

  VisuUiPreview preview;
};
/* A special widget to select two kind of files at a time. */
static gboolean gtkSpinCreate_fileChooser(VisuData *data, GtkWindow *parent);
/* Its callbacks. */
static void onFileSelected(GtkButton *button, gpointer data);
static void fileKindChangedForPosition(GtkToggleButton *button, gpointer data);
static void fileKindChangedForSpin(GtkToggleButton *button, gpointer data);
static void fileUpdatePreview(GtkFileChooser *chooser,
			      struct FileChooserSpin_struct *fileChooserSpin);

/* Callbacks */
void global_resource_callback(GtkWidget* widget, gpointer data);
void element_resource_callback(GtkWidget* widget, gpointer data);
static void changeSomethingSpin();
GtkWidget* createGtkInterfaceForSpinMethod();
static void onPolicyChanged(GtkToggleButton *toggle, gpointer data);
static void onPolicyAtomicChanged(GtkToggleButton *toggle, gpointer data);
static void onPolicyModulusChanged(GtkToggleButton *toggle, gpointer data);

static GtkWidget* createGtkConfigForSpinMethod(void);
static void destroyGtkConfigForSpinMethod();

/* Local routines. */
static gboolean fileSelected(struct FileChooserSpin_struct *values);
static void sync_global_resources(GObject *object, VisuData *dataObj, gpointer data);
static void sync_local_resources(GObject *trash, gpointer data);


/***************/
/* Public part */
/***************/

/* See h file for more info */
void visu_ui_panel_elements_spin_init()
{
  visu_ui_panel_elements_setMethod(visu_rendering_getByName
				    (VISU_RENDERING_SPIN_NAME),
				    changeSomethingSpin,
				    createGtkInterfaceForSpinMethod);
}

/* See h file for more info */
void visu_ui_panel_elements_spin_initMethod()
{
  visu_ui_panel_method_set(visu_rendering_getByName
				    (VISU_RENDERING_SPIN_NAME),
				    createGtkConfigForSpinMethod,
				    destroyGtkConfigForSpinMethod);
}

/* Creates the hbox containing x,y,z spin buttons */
static GtkWidget *gtk_spin_create_direction(float *cartesian_values,
					    float *spherical_values)
{
  GtkWidget *table;
  GtkWidget* label_x = gtk_label_new(_("x:"));
  GtkWidget* label_y = gtk_label_new(_("y:"));
  GtkWidget* label_z = gtk_label_new(_("z:"));
  GtkWidget* label_theta = gtk_label_new("\316\270:");
  GtkWidget* label_phi = gtk_label_new("\317\206:");
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;

  tooltips = gtk_tooltips_new();
#endif

  table = gtk_table_new(2, 6, FALSE);

  gtk_misc_set_alignment(GTK_MISC(label_x), 1., 0.5);
  gtk_misc_set_alignment(GTK_MISC(label_y), 1., 0.5);
  gtk_misc_set_alignment(GTK_MISC(label_z), 1., 0.5);

  gtkw_x = gtk_spin_button_new_with_range(-99.99, 99.99, 0.05);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_x), cartesian_values[0]);
  g_signal_connect(gtkw_x, "value-changed", 
		   G_CALLBACK(global_resource_callback), GINT_TO_POINTER(-1));   

  gtkw_y = gtk_spin_button_new_with_range(-99.99, 99.99, 0.05);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_y), cartesian_values[1]);
  g_signal_connect(gtkw_y, "value-changed", 
		   G_CALLBACK(global_resource_callback), GINT_TO_POINTER(-2));   

  gtkw_z = gtk_spin_button_new_with_range(-99.99, 99.99, 0.05);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_z), cartesian_values[2]);
  g_signal_connect(gtkw_z, "value-changed", 
		   G_CALLBACK(global_resource_callback), GINT_TO_POINTER(-3));   

  gtk_table_attach(GTK_TABLE(table), label_x, 0, 1, 0, 1,
		   GTK_EXPAND | GTK_FILL, GTK_SHRINK, 0, 2);
  gtk_table_attach(GTK_TABLE(table), label_y, 2, 3, 0, 1,
		   GTK_EXPAND | GTK_FILL, GTK_SHRINK, 0, 2);
  gtk_table_attach(GTK_TABLE(table), label_z, 4, 5, 0, 1,
		   GTK_EXPAND | GTK_FILL, GTK_SHRINK, 0, 2);

  gtk_table_attach(GTK_TABLE(table), gtkw_x, 1, 2, 0, 1,
		   GTK_EXPAND | GTK_FILL, GTK_SHRINK, 1, 2);
  gtk_table_attach(GTK_TABLE(table), gtkw_y, 3, 4, 0, 1,
		   GTK_EXPAND | GTK_FILL, GTK_SHRINK, 1, 2);
  gtk_table_attach(GTK_TABLE(table), gtkw_z, 5, 6, 0, 1,
		   GTK_EXPAND | GTK_FILL, GTK_SHRINK, 1, 2);

  gtk_misc_set_alignment(GTK_MISC(label_theta), 1., 0.5);
  gtk_misc_set_alignment(GTK_MISC(label_phi), 1., 0.5);

  gtkw_cone_theta_angle = gtk_spin_button_new_with_range(0, 180., 3);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_cone_theta_angle), spherical_values[1]);
  g_signal_connect(gtkw_cone_theta_angle, "value-changed",
		   G_CALLBACK(global_resource_callback), GINT_TO_POINTER(0));

  gtkw_cone_phi_angle = gtk_spin_button_new_with_range(0, 360., 3);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_cone_phi_angle), spherical_values[2]);
  g_signal_connect(gtkw_cone_phi_angle, "value-changed",
		   G_CALLBACK(global_resource_callback), GINT_TO_POINTER(1));
  gtk_spin_button_set_wrap(GTK_SPIN_BUTTON(gtkw_cone_phi_angle), TRUE);

  gtkw_set_view = gtk_button_new_with_label (_("Set ortho."));
  gtk_widget_set_tooltip_text(gtkw_set_view,
			      _("Set the cone orientation to be orthogonal"
				" to the screen."));
  g_signal_connect(gtkw_set_view, "clicked",
		   G_CALLBACK(set_view), NULL);

  gtk_table_attach(GTK_TABLE(table), label_theta, 0, 1, 1, 2,
		   GTK_EXPAND | GTK_FILL, GTK_SHRINK, 0, 2);
  gtk_table_attach(GTK_TABLE(table), label_phi,   2, 3, 1, 2,
		   GTK_EXPAND | GTK_FILL, GTK_SHRINK, 0, 2);

  gtk_table_attach(GTK_TABLE(table), gtkw_cone_theta_angle, 1, 2, 1, 2,
		   GTK_EXPAND | GTK_FILL, GTK_SHRINK, 1, 2);
  gtk_table_attach(GTK_TABLE(table), gtkw_cone_phi_angle, 3, 4, 1, 2,
		   GTK_EXPAND | GTK_FILL, GTK_SHRINK, 1, 2);

  gtk_table_attach(GTK_TABLE(table), gtkw_set_view, 4, 6, 1, 2,
		   GTK_EXPAND, GTK_SHRINK, 1, 2);

  return table;
}

/* Creates the hbox containing the color_wheel_angle button */
static GtkWidget *gtk_spin_create_color_wheel(float color_wheel_angle)
{
  GtkWidget* hbox = gtk_hbox_new(FALSE, 0);
  GtkWidget* label = gtk_label_new(_("Rotate color wheel:"));

  gtk_misc_set_alignment(GTK_MISC(label), 0.,0.5);

  gtkw_color_wheel_angle = gtk_spin_button_new_with_range(0, 360., 3);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_color_wheel_angle), color_wheel_angle);
  g_signal_connect(gtkw_color_wheel_angle, "value-changed",
		   G_CALLBACK(global_resource_callback), GINT_TO_POINTER(2));
  gtk_spin_button_set_wrap(GTK_SPIN_BUTTON(gtkw_color_wheel_angle), TRUE);

  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 1);
  gtk_box_pack_start(GTK_BOX(hbox), gtkw_color_wheel_angle, FALSE, FALSE, 1);

  /* Degrees. */
  label = gtk_label_new(_("deg."));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 1);

  return hbox;
}

GtkWidget* createHidingModeRadioWidgets()
{
  GtkWidget *hbox;
  GSList *radiobutton_group;
  VisuRenderingSpinDrawingPolicy policy;

  hbox = gtk_hbox_new(FALSE, 0);
  radioAlwaysSpin = gtk_radio_button_new_with_label(NULL, _("always"));
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(radioAlwaysSpin), (GSList*)0);
  radiobutton_group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radioAlwaysSpin));
  gtk_box_pack_start(GTK_BOX(hbox), radioAlwaysSpin, TRUE, TRUE, 1);
  g_signal_connect(G_OBJECT(radioAlwaysSpin), "toggled",
		   G_CALLBACK(onPolicyChanged), GINT_TO_POINTER(VISU_RENDERING_SPIN_ALWAYS));
  radioEmptySpin = gtk_radio_button_new_with_label(NULL, _("never"));
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(radioEmptySpin), radiobutton_group);
  radiobutton_group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radioEmptySpin));
  gtk_box_pack_start(GTK_BOX(hbox), radioEmptySpin, TRUE, TRUE, 1);
  g_signal_connect(G_OBJECT(radioEmptySpin), "toggled",
		   G_CALLBACK(onPolicyChanged), GINT_TO_POINTER(VISU_RENDERING_SPIN_HIDE_NULL));
  radioAtomicSpin = gtk_radio_button_new_with_label(NULL, _("atomic"));
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(radioAtomicSpin), radiobutton_group);
  radiobutton_group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radioAtomicSpin));
  gtk_box_pack_start(GTK_BOX(hbox), radioAtomicSpin, TRUE, TRUE, 1);
  g_signal_connect(G_OBJECT(radioAtomicSpin), "toggled",
		   G_CALLBACK(onPolicyChanged), GINT_TO_POINTER(VISU_RENDERING_SPIN_ATOMIC_NULL));
  g_object_get(G_OBJECT(visu_rendering_getByName(VISU_RENDERING_SPIN_NAME)),
               "hiding-mode", &policy, NULL);
  switch (policy)
    {
    case VISU_RENDERING_SPIN_ALWAYS:
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioAlwaysSpin), TRUE);
      break;
    case VISU_RENDERING_SPIN_HIDE_NULL:
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioEmptySpin), TRUE);
      break;
    case VISU_RENDERING_SPIN_ATOMIC_NULL:
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioAtomicSpin), TRUE);
      break;
    default:
      g_warning("Wrong 'hiding-mode'.");
    }

  gtk_widget_show_all(hbox);
  return hbox;
}
  
/* Creates the vbox displayed in the config panel */
static GtkWidget* createGtkConfigForSpinMethod()
{
  GtkWidget *vbox = gtk_vbox_new(FALSE, 0);
  GtkWidget *label2 = gtk_label_new(_("Color cone orientation:"));
  float cartesian[3], spherical[3], wheel;
  GtkWidget *labelPolicy;
  GtkWidget *hbox, *check, *label, *align;
  GSList *radiobutton_group;
  VisuRenderingSpinModulusPolicy modPol;
  VisuRendering *spin;
  gboolean atomic;

  DBG_fprintf(stderr, "Gtk Spin : building specific spin rendering method config widget.\n");
  spin = visu_rendering_getByName(VISU_RENDERING_SPIN_NAME);

  spherical[0] = 1;
  g_object_get(G_OBJECT(spin), "cone-theta", spherical + 1, "cone-phi", spherical + 2, NULL);

  tool_matrix_sphericalToCartesian(cartesian, spherical);

  /* Use modulus. */
  g_object_get(G_OBJECT(spin), "modulus-scaling", &modPol, NULL);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("Spin lenght is proportional to modulus: "));
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_widget_set_name(label, "label_head_2");
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 3);
  check = gtk_check_button_new();
  gtk_box_pack_start(GTK_BOX(hbox), check, FALSE, FALSE, 10);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check),
			       (modPol != VISU_RENDERING_SPIN_CONSTANT));
  g_signal_connect(G_OBJECT(check), "toggled",
		   G_CALLBACK(onPolicyModulusChanged),
		   GINT_TO_POINTER(VISU_RENDERING_SPIN_CONSTANT));

  hbox = gtk_hbox_new(FALSE, 0);
  radioPerTypeMod = gtk_radio_button_new_with_label(NULL, _("per node type"));
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(radioPerTypeMod), (GSList*)0);
  radiobutton_group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radioPerTypeMod));
  gtk_box_pack_start(GTK_BOX(hbox), radioPerTypeMod, TRUE, TRUE, 1);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioPerTypeMod),
			       modPol == VISU_RENDERING_SPIN_PER_TYPE);
  gtk_widget_set_sensitive(radioPerTypeMod, (modPol != VISU_RENDERING_SPIN_CONSTANT));
  g_signal_connect(G_OBJECT(radioPerTypeMod), "toggled",
		   G_CALLBACK(onPolicyModulusChanged),
		   GINT_TO_POINTER(VISU_RENDERING_SPIN_PER_TYPE));
  radioGlobalMod = gtk_radio_button_new_with_label(NULL, _("globaly"));
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(radioGlobalMod), radiobutton_group);
  radiobutton_group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radioGlobalMod));
  gtk_box_pack_start(GTK_BOX(hbox), radioGlobalMod, TRUE, TRUE, 1);
  g_signal_connect(G_OBJECT(radioGlobalMod), "toggled",
		   G_CALLBACK(onPolicyModulusChanged),
		   GINT_TO_POINTER(VISU_RENDERING_SPIN_GLOBAL));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioGlobalMod),
			       modPol == VISU_RENDERING_SPIN_GLOBAL);
  gtk_widget_set_sensitive(radioGlobalMod, (modPol != VISU_RENDERING_SPIN_CONSTANT));
  gtk_widget_show_all(hbox);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  /* Drawing atomic. */
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("Use atomic rendering in addition to spin: "));
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_widget_set_name(label, "label_head_2");
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 3);
  check = gtk_check_button_new();
  gtk_box_pack_start(GTK_BOX(hbox), check, FALSE, FALSE, 10);
  g_object_get(G_OBJECT(spin), "use-atomic", &atomic, NULL);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), atomic);
  g_signal_connect(G_OBJECT(check), "toggled",
		   G_CALLBACK(onPolicyAtomicChanged), (gpointer)0);

  /* Drawing policy. */
  labelPolicy = gtk_label_new(LABEL_SPIN_POLICY);
  gtk_misc_set_alignment(GTK_MISC(labelPolicy), 0., 0.5);
  gtk_misc_set_padding(GTK_MISC(labelPolicy), 3, 0);
  gtk_widget_set_name(labelPolicy, "label_head_2");
  gtk_box_pack_start(GTK_BOX(vbox), labelPolicy, FALSE, FALSE, 3);

  hbox = createHidingModeRadioWidgets();
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  label = gtk_label_new("");
  gtk_label_set_markup(GTK_LABEL(label), _("Color distribution options:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_misc_set_padding(GTK_MISC(label), 3, 0);
  gtk_widget_set_name(label, "label_head_2");
  align = gtk_alignment_new(0.5, 0.5, 1., 1.);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 5, 0, 0, 0);
  gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, FALSE, 3);
  gtk_container_add(GTK_CONTAINER(align), label);

  g_object_get(G_OBJECT(spin), "cone-omega", &wheel, NULL);
  gtk_box_pack_start(GTK_BOX(vbox), 
		     gtk_spin_create_color_wheel(wheel), FALSE, FALSE, 3);

  gtk_box_pack_start(GTK_BOX(vbox), label2, FALSE, FALSE, 3);
  gtk_misc_set_alignment(GTK_MISC(label2), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(vbox),
                     gtk_spin_create_direction(cartesian, spherical), FALSE, FALSE, 3);

  gtk_widget_show_all(vbox);

  configResourcesLoadedId = g_signal_connect(VISU_OBJECT_INSTANCE, "resourcesLoaded",
					     G_CALLBACK(sync_global_resources), NULL);

  return vbox;
}
static void destroyGtkConfigForSpinMethod()
{
  DBG_fprintf(stderr, "Gtk Spin: remove signal from config rendering widget.\n");
  g_signal_handler_disconnect(VISU_OBJECT_INSTANCE, configResourcesLoadedId);
}
static void onPolicyChanged(GtkToggleButton *toggle, gpointer data)
{
  gboolean atomic;
  VisuData *dataObj;

  if (!gtk_toggle_button_get_active(toggle))
    return;

  g_object_set(G_OBJECT(visu_rendering_getByName(VISU_RENDERING_SPIN_NAME)),
               "hiding-mode", (guint)GPOINTER_TO_INT(data), NULL);

  atomic = (gtk_toggle_button_get_active(toggle) ||
	    gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioAtomicSpin)));
  gtk_widget_set_sensitive(expandAtomic, atomic);	    
  gtk_expander_set_expanded(GTK_EXPANDER(expandAtomic), atomic);
  if (atomic)
    gtk_widget_hide(labelAtomic);
  else
    gtk_widget_show(labelAtomic);

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(VISU_UI_PANEL_ELEMENTS));
  if (!dataObj)
    return;
  g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
  VISU_REDRAW_ADD;
}
static void onPolicyAtomicChanged(GtkToggleButton *toggle, gpointer data _U_)
{
  VisuData *dataObj;
  gboolean atomic;

  g_object_set(G_OBJECT(visu_rendering_getByName(VISU_RENDERING_SPIN_NAME)),
               "use-atomic", gtk_toggle_button_get_active(toggle), NULL);

  atomic = (gtk_toggle_button_get_active(toggle) ||
	    gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioAtomicSpin)));
  gtk_widget_set_sensitive(expandAtomic, atomic);
  gtk_expander_set_expanded(GTK_EXPANDER(expandAtomic), atomic);
  if (atomic)
    gtk_widget_hide(labelAtomic);
  else
    gtk_widget_show(labelAtomic);

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(VISU_UI_PANEL_ELEMENTS));
  if (!dataObj)
    return;
  g_signal_emit_by_name(G_OBJECT(dataObj), "ElementRenderingChanged", (VisuElement*)0, NULL);
  g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
  VISU_REDRAW_ADD;
}
static void onPolicyModulusChanged(GtkToggleButton *toggle, gpointer data)
{
  VisuData *dataObj;
  VisuRenderingSpinModulusPolicy modPol;

  if ((VisuRenderingSpinModulusPolicy)data == VISU_RENDERING_SPIN_CONSTANT)
    {
      if (!gtk_toggle_button_get_active(toggle))
	modPol = VISU_RENDERING_SPIN_CONSTANT;
      else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioPerTypeMod)))
	modPol = VISU_RENDERING_SPIN_PER_TYPE;
      else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioGlobalMod)))
	modPol = VISU_RENDERING_SPIN_GLOBAL;
      else
	modPol = VISU_RENDERING_SPIN_CONSTANT;
      gtk_widget_set_sensitive(radioGlobalMod, gtk_toggle_button_get_active(toggle));
      gtk_widget_set_sensitive(radioPerTypeMod, gtk_toggle_button_get_active(toggle));
    }
  else
    {
      if (!gtk_toggle_button_get_active(toggle))
        return;
      modPol = (VisuRenderingSpinModulusPolicy)data;
    }

  g_object_set(G_OBJECT(visu_rendering_getByName(VISU_RENDERING_SPIN_NAME)),
               "modulus-scaling", modPol, NULL);

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(VISU_UI_PANEL_ELEMENTS));
  if (!dataObj)
    return;
  g_signal_emit_by_name(G_OBJECT(dataObj), "ElementRenderingChanged", (VisuElement*)0, NULL);
  g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
  VISU_REDRAW_ADD;
}

/* Initialise the gtk methods associated with
   the spin rendering method. */
void visu_ui_panel_elements_spin_initOpen()
{
  visu_ui_setRenderingSpecificMethod(visu_rendering_getByName
				      (VISU_RENDERING_SPIN_NAME),
				      gtkSpinCreate_fileChooser);
}


/* Creates a custom file chooser which allows the user to select a spin and a position file */
static gboolean gtkSpinCreate_fileChooser(VisuData *data, GtkWindow *parent)
{
  GtkWidget *fileSelection;
  GList *lst, *tmpLst;
  const GList *tmpLst2;
  GtkWidget *vbox, *wd;
  gchar* directory;
  GtkFileFilter *filterPos, *filterSpin, *filterAll;
  GtkWidget *table, *hbox, *align;
  GtkWidget *label, *button;
  GtkWidget *labelPosition, *labelSpin;
  GtkWidget *radioPosition, *radioSpin;
  GtkWidget *hiding_mode;
  struct FileChooserSpin_struct fileChooserSpin;
  gint status;
  VisuRendering *spin;

#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;

  tooltips = gtk_tooltips_new ();
#endif

  if (!data)
    return 0;

  fileSelection = gtk_file_chooser_dialog_new(_("Load session"),
					      GTK_WINDOW(parent),
					      GTK_FILE_CHOOSER_ACTION_OPEN,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OPEN, GTK_RESPONSE_OK,
					      NULL);
  directory = visu_ui_getLastOpenDirectory();
  if (directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(fileSelection), directory);

  gtk_widget_set_name(fileSelection, "filesel");
  gtk_window_set_position(GTK_WINDOW(fileSelection), GTK_WIN_POS_CENTER_ON_PARENT);
  gtk_window_set_modal(GTK_WINDOW(fileSelection), TRUE);

  spin = visu_rendering_getByName(VISU_RENDERING_SPIN_NAME);

  DBG_fprintf(stderr, "Gtk Spin: filter for position.\n");
  filterPos = gtk_file_filter_new ();
  lst = visu_rendering_getFileFormat(spin, FILE_KIND_POSITION); 
  for (tmpLst = lst; tmpLst; tmpLst = g_list_next(tmpLst))
    for (tmpLst2 = tool_file_format_getFilePatterns((ToolFileFormat*)tmpLst->data);
         tmpLst2; tmpLst2 = g_list_next(tmpLst2))
      {
        DBG_fprintf(stderr, " | add pattern '%s'.\n", (char*)tmpLst2->data);
        gtk_file_filter_add_pattern (filterPos, (char*)tmpLst2->data);
      }
  g_list_free(lst);
  gtk_file_filter_set_name(filterPos, visu_rendering_getFileTypeName
			   (spin, FILE_KIND_POSITION));

  DBG_fprintf(stderr, "Gtk Spin: filter for spin.\n");
  filterSpin = gtk_file_filter_new ();
  lst = visu_rendering_getFileFormat(spin, FILE_KIND_SPIN); 
  for (tmpLst = lst; tmpLst; tmpLst = g_list_next(tmpLst))
    for (tmpLst2 = tool_file_format_getFilePatterns((ToolFileFormat*)tmpLst->data);
         tmpLst2; tmpLst2 = g_list_next(tmpLst2))
      {
        DBG_fprintf(stderr, " | add pattern '%s'.\n", (char*)tmpLst2->data);
        gtk_file_filter_add_pattern (filterSpin, (char*)tmpLst2->data);
      }
  g_list_free(lst);
  gtk_file_filter_set_name(filterSpin,
			   visu_rendering_getFileTypeName(spin, FILE_KIND_SPIN));
  filterAll = gtk_file_filter_new ();
  gtk_file_filter_add_pattern (filterAll, "*");
  gtk_file_filter_set_name(filterAll, _("All files"));
  
/*   g_signal_connect(G_OBJECT(fileSelection), "file-activated", */
/* 		   G_CALLBACK(fileSelectedForSpinMethod), (gpointer)&fileChooserSpin); */

  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(fileSelection), filterPos);
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(fileSelection), filterSpin);
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(fileSelection), filterAll);
  gtk_file_chooser_set_filter(GTK_FILE_CHOOSER(fileSelection), filterPos);

  /* The preview widget. */
  visu_ui_preview_add(&fileChooserSpin.preview, GTK_FILE_CHOOSER(fileSelection));
  g_signal_connect(GTK_FILE_CHOOSER(fileSelection), "update-preview",
		   G_CALLBACK(fileUpdatePreview), &fileChooserSpin);

  /* The additional widgets. */
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_file_chooser_set_extra_widget(GTK_FILE_CHOOSER(fileSelection), vbox);
  
  label = gtk_label_new(_("<b>Selected files are used for</b>:"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_widget_set_name(label, "label_head_2");
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 2);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);

  /* The table for the two file types selections (spin & position). */
  table = gtk_table_new(2, 4, FALSE);
  gtk_box_pack_start(GTK_BOX(hbox), table, TRUE, TRUE, 2);
  
  /* Position button. */
  align = gtk_alignment_new(0., 1.0, 1., 0.);
  radioPosition = gtk_radio_button_new_with_label(NULL, _("position"));
  gtk_container_add(GTK_CONTAINER(align), radioPosition);
  gtk_table_attach(GTK_TABLE(table), align, 0, 1, 0, 1,
		   GTK_FILL, GTK_SHRINK, 2, 0);
  g_signal_connect(G_OBJECT(radioPosition), "toggled",
		   G_CALLBACK(fileKindChangedForPosition),
		   (gpointer)&fileChooserSpin);

  /* Spin button */
  align = gtk_alignment_new(0., 1.0, 1., 0.);
  radioSpin = gtk_radio_button_new_with_label_from_widget
    (GTK_RADIO_BUTTON(radioPosition), _("spin"));
  gtk_container_add(GTK_CONTAINER(align), radioSpin);
  gtk_table_attach(GTK_TABLE(table), align, 0, 1, 1, 2,
		   GTK_FILL, GTK_SHRINK, 2, 0);
  g_signal_connect(G_OBJECT(radioSpin), "toggled",
		   G_CALLBACK(fileKindChangedForSpin),
		   (gpointer)&fileChooserSpin);

  /* Layout labels */
  label = gtk_label_new(":");
  gtk_table_attach(GTK_TABLE(table), label, 2, 3, 0, 1, GTK_SHRINK, GTK_SHRINK, 5, 0);

  label = gtk_label_new(":");
  gtk_table_attach(GTK_TABLE(table), label, 2, 3, 1, 2, GTK_SHRINK, GTK_SHRINK, 5, 0);

  labelPosition = gtk_label_new(_("None"));
  gtk_misc_set_alignment(GTK_MISC(labelPosition), 0., 0.5);
  gtk_table_attach(GTK_TABLE(table), labelPosition, 3, 4, 0, 1,
		   GTK_FILL | GTK_EXPAND, GTK_SHRINK, 2, 0);

  labelSpin = gtk_label_new(_("None"));
  gtk_misc_set_alignment(GTK_MISC(labelSpin), 0., 0.5);
  gtk_table_attach(GTK_TABLE(table), labelSpin, 3, 4, 1, 2,
		   GTK_FILL | GTK_EXPAND, GTK_SHRINK, 2, 0);

  button = gtk_button_new();
  gtk_button_set_relief(GTK_BUTTON(button), GTK_RELIEF_NONE);
  wd = gtk_image_new_from_stock(GTK_STOCK_APPLY, GTK_ICON_SIZE_MENU);
  gtk_container_add(GTK_CONTAINER(button), wd);
  gtk_table_attach(GTK_TABLE(table), button, 1, 2, 0, 1,
		   GTK_SHRINK, GTK_SHRINK, 10, 0);
  g_signal_connect(G_OBJECT(button), "clicked",
		   G_CALLBACK(onFileSelected), (gpointer)&fileChooserSpin);
  gtk_widget_set_tooltip_text(button,
			      _("Set the selected file as position to load."));
  fileChooserSpin.apply[0] = button;

  button = gtk_button_new();
  gtk_button_set_relief(GTK_BUTTON(button), GTK_RELIEF_NONE);
  wd = gtk_image_new_from_stock(GTK_STOCK_APPLY, GTK_ICON_SIZE_MENU);
  gtk_container_add(GTK_CONTAINER(button), wd);
  gtk_table_attach(GTK_TABLE(table), button, 1, 2, 1, 2,
		   GTK_SHRINK, GTK_SHRINK, 10, 0);
  g_signal_connect(G_OBJECT(button), "clicked",
		   G_CALLBACK(onFileSelected), (gpointer)&fileChooserSpin);
  gtk_widget_set_tooltip_text(button,
			       _("Set the selected file as spin to load."));
  gtk_widget_set_sensitive(button, FALSE);
  fileChooserSpin.apply[1] = button;

  /* Hiding mode button */
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);
  label = gtk_label_new(LABEL_SPIN_POLICY);
  gtk_widget_set_name(label, "label_head_2");
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 2);
  hiding_mode = createHidingModeRadioWidgets();
  gtk_box_pack_start(GTK_BOX(hbox), hiding_mode, FALSE, FALSE, 2);

  gtk_widget_show_all(vbox);
  
/*   frame = gtk_frame_new(NULL); */
/*   gtk_widget_show (frame); */
/*   gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), frame, TRUE, TRUE, 0); */

/*   label = gtk_label_new(_("The spin method uses 2 files: one stores the position and the other is used for the spin orientation. These two files are required to render a spin configurations. Either position or spin file can be kept between two views if they have the same number of nodes.")); */
/*   gtk_widget_show(label); */
/*   gtk_container_add(GTK_CONTAINER(frame), label); */
/*   gtk_label_set_line_wrap(GTK_LABEL(label), TRUE); */
/*   gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5); */
/*   gtk_misc_set_padding(GTK_MISC(label), 5, 5); */

/*   hbox = gtk_hbox_new(FALSE, 0); */
/*   gtk_widget_show(hbox); */
/*   gtk_frame_set_label_widget(GTK_FRAME(frame), hbox); */
/*   image = gtk_image_new_from_stock(GTK_STOCK_HELP, GTK_ICON_SIZE_SMALL_TOOLBAR); */
/*   gtk_widget_show(image); */
/*   gtk_box_pack_start(GTK_BOX(hbox), image, FALSE, FALSE, 0); */
/*   label = gtk_label_new (_("Help")); */
/*   gtk_widget_show(label); */
/*   gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0); */
  
  fileChooserSpin.label[FILE_KIND_POSITION] = GTK_LABEL(labelPosition);
  fileChooserSpin.label[FILE_KIND_SPIN] = GTK_LABEL(labelSpin);
  fileChooserSpin.radio[FILE_KIND_POSITION] = GTK_RADIO_BUTTON(radioPosition);
  fileChooserSpin.radio[FILE_KIND_SPIN] = GTK_RADIO_BUTTON(radioSpin);
  fileChooserSpin.fileChooserDialog = GTK_FILE_CHOOSER_DIALOG(fileSelection);
  fileChooserSpin.fileKind = FILE_KIND_POSITION;
  fileChooserSpin.files[FILE_KIND_POSITION] = (gchar*)0;
  fileChooserSpin.files[FILE_KIND_SPIN] = (gchar*)0;

  do
    {
      status = gtk_dialog_run(GTK_DIALOG(fileSelection));
      if (status != GTK_RESPONSE_OK)
	{
	  if (fileChooserSpin.files[FILE_KIND_POSITION])
	    g_free(fileChooserSpin.files[FILE_KIND_POSITION]);
	  if (fileChooserSpin.files[FILE_KIND_SPIN])
	    g_free(fileChooserSpin.files[FILE_KIND_SPIN]);

	  gtk_widget_destroy(fileSelection);
	  
	  return FALSE;
	}
      else
	{
	  DBG_fprintf(stderr, "Gtk Spin: get response OK.\n");
	  fileSelected(&fileChooserSpin);
	  if (fileChooserSpin.files[FILE_KIND_POSITION] &&
	      fileChooserSpin.files[FILE_KIND_SPIN])
	    {
	      visu_data_addFile(data, fileChooserSpin.files[FILE_KIND_POSITION],
			       FILE_KIND_POSITION, (ToolFileFormat*)0);
	      visu_data_addFile(data, fileChooserSpin.files[FILE_KIND_SPIN],
			       FILE_KIND_SPIN, (ToolFileFormat*)0);
	      g_free(fileChooserSpin.files[FILE_KIND_POSITION]);
	      g_free(fileChooserSpin.files[FILE_KIND_SPIN]);

	      directory = gtk_file_chooser_get_current_folder
		(GTK_FILE_CHOOSER(fileSelection));
	      visu_ui_setLastOpenDirectory((char*)directory, VISU_UI_DIR_FILE);
	      g_free(directory);
	      
	      gtk_widget_destroy(fileSelection);
	  
	      return TRUE;
	    }
	}
    } while (TRUE);
  return FALSE;
}

static gboolean fileSelected(struct FileChooserSpin_struct *values)
{
  gchar *filename = NULL, *filenameUTF8;
  gchar *message;

  filename =
    gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(values->fileChooserDialog));
  if (!filename)
    return FALSE;

  if (!g_file_test(filename, G_FILE_TEST_IS_DIR))
    {
      DBG_fprintf(stderr, "Gtk Spin: select '%s' as file %d\n", filename, values->fileKind);
      if (values->files[values->fileKind])
	g_free(values->files[values->fileKind]);
      /* filename is not freed here, it will be later. */
      values->files[values->fileKind] = filename;
      filenameUTF8 = g_filename_to_utf8(filename, -1, NULL, NULL, NULL);
      g_return_val_if_fail(filenameUTF8, TRUE);

      if (g_utf8_strlen(filenameUTF8, 51) > 50)
	message =
	  g_strdup_printf("(...)%s", g_utf8_offset_to_pointer
			  (filenameUTF8, g_utf8_strlen(filenameUTF8, -1) - 50));
      else
	message = g_strdup(filenameUTF8);
      gtk_label_set_text(values->label[values->fileKind], message);
      g_free(message);
      g_free(filenameUTF8);
      if (values->fileKind == FILE_KIND_POSITION)
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(values->radio[FILE_KIND_SPIN]), TRUE);
      else if (values->fileKind == FILE_KIND_SPIN)
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(values->radio[FILE_KIND_POSITION]), TRUE);
      
      DBG_fprintf(stderr, "Gtk Spin: interface updated after selection.\n");

      return TRUE;
/*       if (values->files[FILE_KIND_POSITION] && values->files[FILE_KIND_SPIN]) */
/* 	{ */
/* 	  DBG_fprintf(stderr, "Gtk Spin: emit response OK.\n"); */
/* 	  gtk_dialog_response(GTK_DIALOG(values->fileChooserDialog), GTK_RESPONSE_OK); */
/* 	} */
    }

  g_free(filename);
  return FALSE;
}

static void onFileSelected(GtkButton *button _U_, gpointer data)
{
  fileSelected((struct FileChooserSpin_struct*)data);
}

static void fileUpdatePreview(GtkFileChooser *chooser,
			      struct FileChooserSpin_struct *fileChooserSpin)
{
  const char *filenames[3];

  if (fileChooserSpin->files[FILE_KIND_POSITION])
    filenames[0] = g_strdup(fileChooserSpin->files[FILE_KIND_POSITION]);
  else
    filenames[0] = gtk_file_chooser_get_preview_filename(chooser);
  if (fileChooserSpin->files[FILE_KIND_POSITION] &&
      !fileChooserSpin->files[FILE_KIND_SPIN])
    filenames[1] = gtk_file_chooser_get_preview_filename(chooser);
  else if (fileChooserSpin->files[FILE_KIND_SPIN])
    filenames[1] = g_strdup(fileChooserSpin->files[FILE_KIND_SPIN]);
  else
    filenames[1] = (char*)0;
  filenames[2] = (char*)0;

  DBG_fprintf(stderr, "Gtk Spin: update preview with %s %s.\n",
	      filenames[0], filenames[1]);

  /* We test if the selected filename is a directory or not. */
  if (filenames[0] && !g_file_test(filenames[0], G_FILE_TEST_IS_DIR) &&
      filenames[1] && !g_file_test(filenames[1], G_FILE_TEST_IS_DIR))
    visu_ui_preview_update(&fileChooserSpin->preview, filenames);
  if (filenames[0])
    g_free((gchar*)filenames[0]);
  if (filenames[1])
    g_free((gchar*)filenames[1]);
}

static void fileKindChangedForPosition(GtkToggleButton *button, gpointer data)
{
  struct FileChooserSpin_struct* param = data;
  GSList *tmpLst;

  if (!gtk_toggle_button_get_active(button))
    return;

  tmpLst = gtk_file_chooser_list_filters(GTK_FILE_CHOOSER(param->fileChooserDialog));
  gtk_file_chooser_set_filter(GTK_FILE_CHOOSER(param->fileChooserDialog),
			      (GtkFileFilter*)(tmpLst->data));
  param->fileKind = FILE_KIND_POSITION;

  gtk_widget_set_sensitive(param->apply[0], TRUE);
  gtk_widget_set_sensitive(param->apply[1], FALSE);

  if (param->files[0])
    gtk_file_chooser_select_filename(GTK_FILE_CHOOSER(param->fileChooserDialog),
				     param->files[0]);
}

static void fileKindChangedForSpin(GtkToggleButton *button, gpointer data)
{
  struct FileChooserSpin_struct* param = data;
  GSList *tmpLst;

  if (!gtk_toggle_button_get_active(button))
    return;

  tmpLst = gtk_file_chooser_list_filters(GTK_FILE_CHOOSER(param->fileChooserDialog));
  gtk_file_chooser_set_filter(GTK_FILE_CHOOSER(param->fileChooserDialog),
			      (GtkFileFilter*)(tmpLst->next->data));
  param->fileKind = FILE_KIND_SPIN;

  gtk_widget_set_sensitive(param->apply[1], TRUE);
  gtk_widget_set_sensitive(param->apply[0], FALSE);

  if (param->files[1])
    gtk_file_chooser_select_filename(GTK_FILE_CHOOSER(param->fileChooserDialog),
				     param->files[1]);
}

/* Create the gtk widgets (a hbox with a spin with
   positive values) and return it. */
GtkWidget* createGtkInterfaceForSpinMethod()
{
  GtkWidget* hbox[7], *vbox, *label, *vboxAtomic;
  GtkWidget* shapeLabel, *hLabel, *rhLabel, *lLabel, *rlLabel, *ratioLabel, *lengthLabel;
  int i, j = 0;
  VisuRenderingSpinDrawingPolicy hide;
  gboolean atomic, useAt;
  VisuRendering *spin;

  vbox = gtk_vbox_new(FALSE, 0);

  for(i=0; i<7; i++)
    hbox[i] = gtk_hbox_new(FALSE, 0);
  vboxArrowShape = gtk_vbox_new(FALSE, 0);
  gtk_widget_show(vboxArrowShape);
  for(i=2; i<5; i++)
    {
      gtk_box_pack_start(GTK_BOX(vboxArrowShape), hbox[i], FALSE, FALSE, 2);
      gtk_widget_show(hbox[i]);
    }
  vboxElipsoidShape = gtk_vbox_new(FALSE, 0);
  for(i=5; i<7; i++)
    {
      gtk_box_pack_start(GTK_BOX(vboxElipsoidShape), hbox[i], FALSE, FALSE, 2);
      gtk_widget_show(hbox[i]);
    }
      
  /* Labels creation */
  shapeLabel = gtk_label_new("");
  gtk_label_set_markup(GTK_LABEL(shapeLabel), _("<i>Spin shape:</i>"));
  gtk_misc_set_alignment(GTK_MISC(shapeLabel), 0., 0.5);
  gtk_misc_set_padding(GTK_MISC(shapeLabel), 10, 0);
  gtk_widget_show(shapeLabel);

  labelSizeProp = gtk_label_new("");
  gtk_label_set_markup(GTK_LABEL(labelSizeProp), LABEL_ARROW_SIZE);
  gtk_misc_set_alignment(GTK_MISC(labelSizeProp), 0., 0.5);
  gtk_misc_set_padding(GTK_MISC(labelSizeProp), 10, 0);
  gtk_widget_show(labelSizeProp);

  /* Size labels. */
  hLabel = gtk_label_new(_("Hat length:"));
  gtk_misc_set_alignment(GTK_MISC(hLabel), 1.0, 0.5);
  gtk_widget_show(hLabel);

  rhLabel = gtk_label_new(_("Tail length:"));
  gtk_misc_set_alignment(GTK_MISC(rhLabel), 1.0, 0.5);
  gtk_widget_show(rhLabel);

  lLabel = gtk_label_new(_("Hat radius:"));
  gtk_misc_set_alignment(GTK_MISC(lLabel), 1.0, 0.5);
  gtk_widget_show(lLabel);

  rlLabel = gtk_label_new(_("Tail radius:"));
  gtk_misc_set_alignment(GTK_MISC(rlLabel), 1.0, 0.5);
  gtk_widget_show(rlLabel);

  /* Spin buttons creation */
  heightResourceSpin = gtk_spin_button_new_with_range(0, 9, 0.05);
  element_callback_ids[0] = g_signal_connect((gpointer)heightResourceSpin, "value-changed",
					     G_CALLBACK(element_resource_callback),
					     GINT_TO_POINTER(VISU_RENDERING_SPIN_HAT_RADIUS));
  gtk_widget_show(heightResourceSpin);

  rheightResourceSpin = gtk_spin_button_new_with_range(0, 9, 0.05);
  element_callback_ids[1] = g_signal_connect((gpointer)rheightResourceSpin, "value-changed",
					     G_CALLBACK(element_resource_callback),
					     GINT_TO_POINTER(VISU_RENDERING_SPIN_TAIL_RADIUS));
  gtk_widget_show(rheightResourceSpin);

  lengthResourceSpin = gtk_spin_button_new_with_range(0, 9, 0.05);
  element_callback_ids[2] = g_signal_connect((gpointer)lengthResourceSpin, "value-changed",
					     G_CALLBACK(element_resource_callback),
					     GINT_TO_POINTER(VISU_RENDERING_SPIN_HAT_LENGTH));
  gtk_widget_show(lengthResourceSpin);

  rlengthResourceSpin = gtk_spin_button_new_with_range(0, 9, 0.05);
  element_callback_ids[3] = g_signal_connect((gpointer)rlengthResourceSpin, "value-changed",
					     G_CALLBACK(element_resource_callback),
					     GINT_TO_POINTER(spin_VISU_RENDERING_SPIN_TAIL_LENGTH));
  gtk_widget_show(rlengthResourceSpin);

  /* Check box creation */
  label = gtk_label_new(_("Use element color on:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_widget_show(label);

  gtkw_use_element_color = gtk_check_button_new_with_label(_(" tail"));
  element_callback_ids[4] = g_signal_connect(gtkw_use_element_color, "toggled", 
					     G_CALLBACK(element_resource_callback),
					     GINT_TO_POINTER(VISU_RENDERING_SPIN_TAIL_COLOR));
  gtk_widget_show(gtkw_use_element_color);

  gtkw_use_element_color_hat = gtk_check_button_new_with_label(_(" hat"));
  element_callback_ids[5] = g_signal_connect(gtkw_use_element_color_hat, "toggled", 
					     G_CALLBACK(element_resource_callback),
					     GINT_TO_POINTER(VISU_RENDERING_SPIN_HAT_COLOR));
  gtk_widget_show(gtkw_use_element_color_hat);

  /* Combo boxes creation */
  gtkw_element_shape_number = gtk_combo_box_text_new();
  for (i=0; i<VISU_RENDERING_SPIN_N_SHAPES; i++)
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(gtkw_element_shape_number),
                              (const gchar*)0,
			      visu_rendering_spin_getShapeNameI18n(i));
  /* set callback for the combo button. */
  element_callback_ids[6] = g_signal_connect(gtkw_element_shape_number, "changed",
					     G_CALLBACK(element_resource_callback),
					     GINT_TO_POINTER(VISU_RENDERING_SPIN_SHAPE));
  gtk_widget_show(gtkw_element_shape_number);

  /* Set the spins for the elipsoid shape. */
  ratioElipsoidSpin = gtk_spin_button_new_with_range(0, 9, 0.05);
  element_callback_ids[7] = g_signal_connect(G_OBJECT(ratioElipsoidSpin), "value-changed",
					     G_CALLBACK(element_resource_callback),
					     GINT_TO_POINTER(VISU_RENDERING_SPIN_B_AXIS));
  gtk_widget_show(ratioElipsoidSpin);

  lengthElipsoidSpin = gtk_spin_button_new_with_range(0, 9, 0.05);
  element_callback_ids[8] = g_signal_connect(G_OBJECT(lengthElipsoidSpin), "value-changed",
					     G_CALLBACK(element_resource_callback),
					     GINT_TO_POINTER(VISU_RENDERING_SPIN_A_AXIS));
  gtk_widget_show(lengthElipsoidSpin);
  ratioLabel = gtk_label_new(_("B axis: "));
  gtk_misc_set_alignment(GTK_MISC(ratioLabel), 1.0, 0.5);
  gtk_widget_show(ratioLabel);
  lengthLabel = gtk_label_new(_("A axis: "));
  gtk_misc_set_alignment(GTK_MISC(lengthLabel), 1.0, 0.5);
  gtk_widget_show(lengthLabel);
  useElementForElipsoid = gtk_check_button_new_with_label(_("Use element color"));
  element_callback_ids[9] = g_signal_connect(G_OBJECT(useElementForElipsoid), "toggled", 
					     G_CALLBACK(element_resource_callback),
					     GINT_TO_POINTER(VISU_RENDERING_SPIN_ELIPSOID_COLOR));
  gtk_widget_show(useElementForElipsoid);

  /* Atomic options. */
  expandAtomic = gtk_expander_new(_("Atomic rendering options"));
  gtk_widget_set_sensitive(expandAtomic, FALSE);
  gtk_expander_set_expanded(GTK_EXPANDER(expandAtomic), FALSE);
  gtk_widget_show(expandAtomic);
  vboxAtomic = visu_ui_panel_elements_atomic_initBuild();
  gtk_container_add(GTK_CONTAINER(expandAtomic), vboxAtomic);
  labelAtomic = gtk_label_new(_("<i>Enable the atomic rendering in the method tab.</i>"));
  gtk_widget_show(labelAtomic);
  gtk_label_set_use_markup(GTK_LABEL(labelAtomic), TRUE);

  spin = visu_rendering_getByName(VISU_RENDERING_SPIN_NAME);
  g_object_get(G_OBJECT(spin), "hiding-mode", &hide,
               "use-atomic", &useAt, NULL);
  atomic = (useAt || hide == VISU_RENDERING_SPIN_ATOMIC_NULL);
  gtk_widget_set_sensitive(expandAtomic, atomic);
  gtk_expander_set_expanded(GTK_EXPANDER(expandAtomic), atomic);
  if (atomic)
    gtk_widget_hide(labelAtomic);
  else
    gtk_widget_show(labelAtomic);

  /* Displaying every widget created so far */
  gtk_box_pack_start(GTK_BOX(hbox[j]), shapeLabel, TRUE, TRUE, 1);
  gtk_box_pack_start(GTK_BOX(hbox[j]), gtkw_element_shape_number, FALSE, FALSE, 3);
  gtk_box_pack_start(GTK_BOX(vbox), hbox[j], FALSE, FALSE, 2);
  gtk_widget_show(hbox[j++]);

  gtk_box_pack_start(GTK_BOX(hbox[j]), labelSizeProp, TRUE, TRUE, 1);
  gtk_box_pack_start(GTK_BOX(vbox), hbox[j], FALSE, FALSE, 2);
  gtk_widget_show(hbox[j++]);

  gtk_box_pack_start(GTK_BOX(vbox), vboxArrowShape, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(hbox[j]), hLabel, FALSE, FALSE, 1);
  gtk_box_pack_start(GTK_BOX(hbox[j]), heightResourceSpin, FALSE,FALSE, 3);
  gtk_box_pack_start(GTK_BOX(hbox[j]), rhLabel, TRUE, TRUE, 1);
  gtk_box_pack_start(GTK_BOX(hbox[j++]), rheightResourceSpin, FALSE,FALSE, 3);

  gtk_box_pack_start(GTK_BOX(hbox[j]), lLabel, FALSE, FALSE, 1);
  gtk_box_pack_start(GTK_BOX(hbox[j]), lengthResourceSpin, FALSE,FALSE, 3);
  gtk_box_pack_start(GTK_BOX(hbox[j]), rlLabel, TRUE, TRUE, 1);
  gtk_box_pack_start(GTK_BOX(hbox[j++]), rlengthResourceSpin, FALSE,FALSE, 3);

  gtk_box_pack_start(GTK_BOX(hbox[j]), label, TRUE, TRUE, 1);
  gtk_box_pack_start(GTK_BOX(hbox[j]), gtkw_use_element_color_hat, FALSE, FALSE, 1);
  gtk_box_pack_start(GTK_BOX(hbox[j++]), gtkw_use_element_color, FALSE, FALSE, 1);

  gtk_box_pack_start(GTK_BOX(vbox), vboxElipsoidShape, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(hbox[j]), lengthLabel, FALSE, FALSE, 1);
  gtk_box_pack_start(GTK_BOX(hbox[j]), lengthElipsoidSpin, FALSE,FALSE, 3);
  gtk_box_pack_start(GTK_BOX(hbox[j]), ratioLabel, TRUE, TRUE, 1);
  gtk_box_pack_start(GTK_BOX(hbox[j++]), ratioElipsoidSpin, FALSE,FALSE, 3);

  gtk_box_pack_start(GTK_BOX(hbox[j++]), useElementForElipsoid, FALSE, FALSE, 1);

  gtk_box_pack_start(GTK_BOX(vbox), expandAtomic, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), labelAtomic, FALSE, FALSE, 0);

  g_signal_connect(VISU_OBJECT_INSTANCE, "resourcesLoaded",
		   G_CALLBACK(sync_local_resources), NULL);

  return vbox;
}

/* This function is called whenever the current element is changed in the gtk panel. */
static void changeSomethingSpin(GList *eleList)
{
  VisuElement *ele;

  DBG_fprintf(stderr, "Gtk Spin: changing values on element change for %p.\n",
              (gpointer)eleList);
  if (!eleList)
    return;

  DBG_fprintf(stderr, "Gtk Spin: give the element change to the atomic part.\n");
  visu_ui_panel_elements_atomic_onChange(eleList);

  ele = (VisuElement*)eleList->data;

  need_redraw = 0;

  DBG_fprintf(stderr, "Gtk Spin: change all values on element change.\n");
  g_signal_handler_block (heightResourceSpin, element_callback_ids[0] );  
  g_signal_handler_block (rheightResourceSpin, element_callback_ids[1] );  
  g_signal_handler_block (lengthResourceSpin, element_callback_ids[2] );  
  g_signal_handler_block (rlengthResourceSpin, element_callback_ids[3] );  
  g_signal_handler_block (gtkw_use_element_color, element_callback_ids[4] );  
  g_signal_handler_block (gtkw_use_element_color_hat, element_callback_ids[5] );  
/*   g_signal_handler_block (gtkw_element_shape_number, element_callback_ids[6] );   */
  g_signal_handler_block (ratioElipsoidSpin, element_callback_ids[7] );  
  g_signal_handler_block (lengthElipsoidSpin, element_callback_ids[8] );  
  g_signal_handler_block (useElementForElipsoid, element_callback_ids[9] );  

  gtk_spin_button_set_value(GTK_SPIN_BUTTON(heightResourceSpin),
			    visu_rendering_spin_getResourceFloat(ele, VISU_RENDERING_SPIN_HAT_RADIUS));
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(rheightResourceSpin),
			    visu_rendering_spin_getResourceFloat(ele, VISU_RENDERING_SPIN_TAIL_RADIUS));
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(lengthResourceSpin),
			    visu_rendering_spin_getResourceFloat(ele, VISU_RENDERING_SPIN_HAT_LENGTH));
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(rlengthResourceSpin),
			    visu_rendering_spin_getResourceFloat(ele, spin_VISU_RENDERING_SPIN_TAIL_LENGTH));
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(lengthElipsoidSpin),
			    visu_rendering_spin_getResourceFloat(ele, VISU_RENDERING_SPIN_A_AXIS));
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(ratioElipsoidSpin),
			    visu_rendering_spin_getResourceFloat(ele, VISU_RENDERING_SPIN_B_AXIS));
  
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(gtkw_use_element_color),
			       visu_rendering_spin_getResourceBoolean(ele, VISU_RENDERING_SPIN_TAIL_COLOR));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(gtkw_use_element_color_hat),
			       visu_rendering_spin_getResourceBoolean(ele, VISU_RENDERING_SPIN_HAT_COLOR));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(useElementForElipsoid),
			       visu_rendering_spin_getResourceBoolean(ele, VISU_RENDERING_SPIN_ELIPSOID_COLOR));

  gtk_combo_box_set_active(GTK_COMBO_BOX(gtkw_element_shape_number),
			   visu_rendering_spin_getResourceUint(ele, VISU_RENDERING_SPIN_SHAPE));

  g_signal_handler_unblock (heightResourceSpin, element_callback_ids[0] );  
  g_signal_handler_unblock (rheightResourceSpin, element_callback_ids[1] );  
  g_signal_handler_unblock (lengthResourceSpin, element_callback_ids[2] );  
  g_signal_handler_unblock (rlengthResourceSpin, element_callback_ids[3] );  
  g_signal_handler_unblock (gtkw_use_element_color, element_callback_ids[4] );  
  g_signal_handler_unblock (gtkw_use_element_color_hat, element_callback_ids[5] );  
/*   g_signal_handler_unblock (gtkw_element_shape_number, element_callback_ids[6] );   */
  g_signal_handler_unblock (ratioElipsoidSpin, element_callback_ids[7] );  
  g_signal_handler_unblock (lengthElipsoidSpin, element_callback_ids[8] );  
  g_signal_handler_unblock (useElementForElipsoid, element_callback_ids[9] );  

  need_redraw = 1;
}

/* This is the callback func for each element resource. */
void element_resource_callback(GtkWidget* widget, gpointer data)
{
  gboolean refresh;
  GList *tmpLst, *eleList;
  VisuElement *ele;
  VisuRenderingSpinResources property;
  gboolean new_bool;
  guint new_int;
  float new_float;
  VisuData *dataObj;

  eleList = visu_ui_panel_elements_getSelected();
  if (!eleList)
    return;

  property = GPOINTER_TO_INT(data);
  refresh = FALSE;
  for (tmpLst = eleList; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      ele = (VisuElement*)tmpLst->data;
      switch (property)
	{
	case VISU_RENDERING_SPIN_HAT_COLOR:
	case VISU_RENDERING_SPIN_TAIL_COLOR:
	case VISU_RENDERING_SPIN_ELIPSOID_COLOR:
	  new_bool = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget));

	  DBG_fprintf(stderr, "GtkSpin : Set element '%d' property to %d\n",
		      property, new_bool);
	  refresh = visu_rendering_spin_setResourceBoolean(ele, property, new_bool) || refresh;
	  break;
	case VISU_RENDERING_SPIN_SHAPE:
	  new_int = (guint)gtk_combo_box_get_active(GTK_COMBO_BOX(widget));

	  DBG_fprintf(stderr, "GtkSpin : Set element '%d' property to %d\n",
		      property, new_int);
	  refresh = visu_rendering_spin_setResourceUint(ele, property, new_int) || refresh;
	  if (new_int == VISU_RENDERING_SPIN_ELLIPSOID || new_int == VISU_RENDERING_SPIN_TORUS)
	    {
	      gtk_widget_show(vboxElipsoidShape);
	      gtk_widget_hide(vboxArrowShape);
	    }
	  else
	    {
	      gtk_widget_hide(vboxElipsoidShape);
	      gtk_widget_show(vboxArrowShape);
	    }
	  break;
	case VISU_RENDERING_SPIN_HAT_LENGTH:
	case VISU_RENDERING_SPIN_HAT_RADIUS:
	case spin_VISU_RENDERING_SPIN_TAIL_LENGTH:
	case VISU_RENDERING_SPIN_TAIL_RADIUS:
	case VISU_RENDERING_SPIN_A_AXIS:
	case VISU_RENDERING_SPIN_B_AXIS:
	  new_float = gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));

	  DBG_fprintf(stderr, "GtkSpin : Set element '%d' property to %f\n",
		      property, new_float);
	  refresh = visu_rendering_spin_setResourceFloat(ele, property, new_float) || refresh;

	  break;
	default:
	  g_warning("Unknown property '%d' in callback for spin element values.", property);
	  return;
	}
    }

  if(need_redraw && refresh)
    {
      dataObj = visu_ui_panel_getData(VISU_UI_PANEL(VISU_UI_PANEL_ELEMENTS));
      if (!dataObj)
	return;

      for(tmpLst = eleList; tmpLst; tmpLst = g_list_next(tmpLst))
        {
          g_signal_emit_by_name(G_OBJECT(dataObj), "ElementRenderingChanged",
                                (VisuElement*)tmpLst->data, NULL);
          g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged",
                                (VisuElement*)tmpLst->data, NULL);
        }
      VISU_REDRAW_ADD;
    }

  g_list_free(eleList);
}

/* This is the callback func for each global resource. */
void global_resource_callback(GtkWidget* button, gpointer data)
{
  int value;
  VisuData *dataObj;
  VisuRendering *spin;

  value = GPOINTER_TO_INT(data);
  if (value >= 0)
    {
      spin = visu_rendering_getByName(VISU_RENDERING_SPIN_NAME);
      switch (value)
	{
	case 0:
          g_object_set(G_OBJECT(spin), "cone-theta",
                       gtk_spin_button_get_value(GTK_SPIN_BUTTON(button)), NULL);
          break;
	case 1:
          g_object_set(G_OBJECT(spin), "cone-phi",
                       gtk_spin_button_get_value(GTK_SPIN_BUTTON(button)), NULL);
          break;
	case 2:
          g_object_set(G_OBJECT(spin), "cone-omega",
                       gtk_spin_button_get_value(GTK_SPIN_BUTTON(button)), NULL);
          break;
	default:
	  g_warning("Unknown property '%d' in callback for spin global values.", value);
	  return;
	}
      if(need_redraw)
        {
          float spherical[3];
          float cartesian[3];
          int i;

          need_redraw = 0;
	  
          spherical[0] = 1;
          g_object_get(G_OBJECT(spin),
                       "cone-theta", spherical + 1, "cone-phi", spherical + 2, NULL);

          tool_matrix_sphericalToCartesian(cartesian, spherical);

          for(i=0; i<3; i++)
            if(cartesian[i] < 0.01 && cartesian [i] > -0.01)
              cartesian[i] = 0;
	  
          gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_x), cartesian[0]);
          gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_y), cartesian[1]);
          gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_z), cartesian[2]);

          need_redraw = 1;
        }
    }
  else
    {
      float cartesian[3];
      float spherical[3];
      float x = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(gtkw_x));
      float y = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(gtkw_y));
      float z = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(gtkw_z));
      
      if(need_redraw)
	{
	  need_redraw = 0;

	  cartesian[0] = x;
	  cartesian[1] = y;
	  cartesian[2] = z;
	  
	  tool_matrix_cartesianToSpherical(spherical, cartesian);

	  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_cone_theta_angle), spherical[1]);
	  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_cone_phi_angle), spherical[2]);

	  need_redraw = 1;
	}
    }

  if(need_redraw == 1)
    {
      dataObj = visu_ui_panel_getData(VISU_UI_PANEL(VISU_UI_PANEL_ELEMENTS));
      if (!dataObj)
	return;

      g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
      VISU_REDRAW_ADD;
    }
}

void set_view(GtkWidget* button _U_, gpointer data _U_)
{
  float theta;
  float phi;
  VisuData *dataObj;
  VisuGlView *view;

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(VISU_UI_PANEL_ELEMENTS));
  view = visu_ui_panel_getView(VISU_UI_PANEL(VISU_UI_PANEL_ELEMENTS));
  if (!dataObj || !view)
    return;

  theta = tool_modulo_float(view->camera->theta, 360);
  if(theta > 180)
      theta = 360 - theta;
  phi = tool_modulo_float(view->camera->phi, 360);

  need_redraw = 0;

  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_cone_theta_angle), theta);

  need_redraw = 1;

  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_cone_phi_angle), phi);
}

static void sync_global_resources(GObject *object _U_, VisuData *dataObj _U_,
				  gpointer data _U_)
{
  float spherical[3], cartesian[3], wheel;
  int i;

  DBG_fprintf(stderr, "Gtk Spin: caught the 'resourcesLoaded' signal.\n");

  if(gtkw_cone_theta_angle == NULL)
    return;

  spherical[0] = 1;
  g_object_get(G_OBJECT(visu_rendering_getByName(VISU_RENDERING_SPIN_NAME)),
               "cone-theta", spherical + 1, "cone-phi", spherical + 2,
               "cone-omega", &wheel, NULL);

  tool_matrix_sphericalToCartesian(cartesian, spherical);

  for(i=0; i<3; i++)
    if(cartesian[i] < 0.01 && cartesian [i] > -0.01)
      cartesian[i] = 0;

  need_redraw = 0;

  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_x), cartesian[0]);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_y), cartesian[1]);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_z), cartesian[2]);

  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_cone_theta_angle), spherical[1]);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_cone_phi_angle), spherical[2]); 
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtkw_color_wheel_angle), wheel);

  need_redraw = 1;  
}      

static void sync_local_resources(GObject *trash _U_, gpointer data _U_)
{
  GList *eleList;
  VisuElement *ele;

  if(heightResourceSpin == NULL)
    return;
  
  eleList = visu_ui_panel_elements_getSelected();
  if (!eleList)
    return;

  ele = (VisuElement*)eleList->data;
  g_list_free(eleList);

  need_redraw = 0;

  gtk_spin_button_set_value(GTK_SPIN_BUTTON(heightResourceSpin),
			    visu_rendering_spin_getResourceFloat(ele, VISU_RENDERING_SPIN_HAT_RADIUS));
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(rheightResourceSpin),
			    visu_rendering_spin_getResourceFloat(ele, VISU_RENDERING_SPIN_TAIL_RADIUS));
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(lengthResourceSpin),
			    visu_rendering_spin_getResourceFloat(ele, VISU_RENDERING_SPIN_HAT_LENGTH));
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(rlengthResourceSpin),
			    visu_rendering_spin_getResourceFloat(ele, spin_VISU_RENDERING_SPIN_TAIL_LENGTH));
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(lengthElipsoidSpin),
			    visu_rendering_spin_getResourceFloat(ele, VISU_RENDERING_SPIN_A_AXIS));
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(ratioElipsoidSpin),
			    visu_rendering_spin_getResourceFloat(ele, VISU_RENDERING_SPIN_B_AXIS));
  
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(gtkw_use_element_color),
			       visu_rendering_spin_getResourceBoolean(ele, VISU_RENDERING_SPIN_TAIL_COLOR));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(gtkw_use_element_color_hat),
			       visu_rendering_spin_getResourceBoolean(ele, VISU_RENDERING_SPIN_HAT_COLOR));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(useElementForElipsoid),
			       visu_rendering_spin_getResourceBoolean(ele, VISU_RENDERING_SPIN_ELIPSOID_COLOR));

  gtk_combo_box_set_active(GTK_COMBO_BOX(gtkw_element_shape_number),
			   visu_rendering_spin_getResourceUint(ele, VISU_RENDERING_SPIN_SHAPE));

  need_redraw = 1;
}
