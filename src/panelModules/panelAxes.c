/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "panelAxes.h"

#include <string.h>

#include <support.h>
#include <visu_object.h>
#include <gtk_main.h>
#include <openGLFunctions/view.h>
#include <coreTools/toolColor.h>
#include <coreTools/toolMatrix.h>
#include <extraGtkFunctions/gtk_toolPanelWidget.h>
#include <extraGtkFunctions/gtk_lineObjectWidget.h>
#include <extensions/box.h>
#include <extensions/scale.h>
#include <extensions/legend.h>

/**
 * SECTION: panelAxes
 * @short_description: The tab where axes, box, scale and legend are
 * setup.
 *
 * <para>Nothing tunable here.</para>
 */

/* Sensitive widget in this subpanel. */
static GtkWidget *panelAxes;
static GtkWidget *lineBox;
static GtkWidget *lineAxes;
static GtkWidget *lineScales;
static GtkWidget *spinScaleLength;
static GtkWidget *entryScale;
static GtkWidget *originespin[3];
static GtkWidget *orientationspin[3];
static GtkWidget *checkLegend;
static GtkWidget *checkLengths, *checkAxes; /* , *checkBasis */
static GtkWidget *spinXPos, *spinYPos, *spinXLegPos, *spinYLegPos;
static GtkWidget *lblScales;

static gboolean disableCallbacks;
static VisuGlExtAxes *currentAxes = NULL;

/* Private functions. */
static GtkWidget *createInteriorAxes();

/* Local callbacks. */
static void onExtensionUsed(VisuUiLine *line, gboolean used, gpointer data);
static void onBoxRGBValueChanged(VisuUiLine *line, float *rgb, gpointer data);
static void onBoxLineWidthChanged(VisuUiLine *line, gint width, gpointer data);
static void onBoxStippleChanged(VisuUiLine *line, gint stipple, gpointer data);
static void onBoxLengthsToggled(GtkToggleButton *toggle, gpointer data);
static void onBoxLegPosChanged(GtkSpinButton *spin, gpointer data);
static void onAxesUsed(VisuUiLine *line, gboolean used, gpointer data);
static void onAxesToggled(GtkToggleButton *toggle, gpointer data);
static void onAxesWidthChanged(VisuUiLine *line, gint width, gpointer user_data);
static void onAxesRGBChanged(VisuUiLine *line, float *rgb, gpointer data);
static void onAxesStippleChanged(VisuUiLine *line, gint stipple, gpointer data);
static void onAxesPosChanged(GtkSpinButton *spin, gpointer data);
static void onEnter(VisuUiPanel *visu_ui_panel, gpointer data);
static void onResources(GObject *object, VisuData *dataObj, gpointer data);
static void onScalesWidthChanged(VisuUiLine *line, gint width, gpointer user_data);
static void onScalesRGBChanged(VisuUiLine *line, float *rgb, gpointer data);
static void onScalesStippleChanged(VisuUiLine *line, gint stipple, gpointer data);
static void scaleDistanceChanged(GtkSpinButton *spin,gpointer data);
static void scaleOriginChanged(GtkSpinButton *spin,gpointer data);
static void scaleOrientationChanged(GtkSpinButton *spin,gpointer data);
static void onLegendChanged(GtkEntry *entry, gpointer data);
static void onUseLegendChanged(GtkToggleButton *toggle, gpointer data);
static void onViewAvail(VisuObject *obj, VisuData *dataObj, VisuGlView *view, gpointer data);

/**
 * visu_ui_panel_axes_init: (skip)
 *
 * Should be used in the list declared in externalModules.h to be loaded by
 * V_Sim on start-up. This routine will create the #VisuUiPanel where the axes and the
 * label stuffs can be tuned, such as their colour, create scales...
 *
 * Returns: a newly created #VisuUiPanel object.
 */
VisuUiPanel* visu_ui_panel_axes_init(VisuUiMain *ui _U_)
{
  panelAxes = visu_ui_panel_newWithIconFromPath("Panel_axes", _("Box, axes and labels"),
                                                _("Frames/labels"), "stock-axes_20.png");
  if (!panelAxes)
    return (VisuUiPanel*)0;

  gtk_container_add(GTK_CONTAINER(panelAxes), createInteriorAxes());
  visu_ui_panel_setDockable(VISU_UI_PANEL(panelAxes), TRUE);

  /* Create the callbacks of all the sensitive widgets. */
  g_signal_connect(G_OBJECT(panelAxes), "page-entered",
		   G_CALLBACK(onEnter), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataRendered",
		   G_CALLBACK(onViewAvail), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "resourcesLoaded",
		   G_CALLBACK(onResources), (gpointer)0);

  /* Private parameters. */
  disableCallbacks = FALSE;

  return VISU_UI_PANEL(panelAxes);
}
/**
 * visu_ui_panel_axes_setAxesExtension:
 * @axes: (transfer full) (allow-none): a #VisuGlExtAxes object.
 *
 * Set the current axes extension handled by this #VisuUiPanel.
 *
 * Since: 3.7
 **/
void visu_ui_panel_axes_setAxesExtension(VisuGlExtAxes *axes)
{
  if (currentAxes)
    g_object_unref(currentAxes);
  currentAxes = axes;
  if (axes)
    g_object_ref(axes);
}

static GtkWidget *createInteriorAxes()
{
  GtkWidget *vbox, *hbox, *vbox2;
  GtkWidget *label;
  GtkWidget *table;
  GtkWidget *align;
  float *xyz, *orientation, len;
  VisuGlExtScale *scale;
  gchar *lblXYZ[3] = {"X", "Y", "Z"};
  float xyzDefault[3] = {0.f, 0.f, 0.f};
  float orientationDefault[3] = {1.f, 0.f, 0.f};
  int i;
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;

  tooltips = gtk_tooltips_new ();
#endif

  vbox = gtk_vbox_new(FALSE, 0);

  /*********************/
  /* The Bounding box. */
  /*********************/
  align = gtk_alignment_new(0.5, 0.5, 1, 1);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 0, 15, 0, 0);
  gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, FALSE, 0);
  lineBox = visu_ui_line_new(_("Bounding box"));
  gtk_container_add(GTK_CONTAINER(align), lineBox);
  vbox2 = visu_ui_line_getOptionBox(VISU_UI_LINE(lineBox));
  checkLengths = gtk_check_button_new_with_mnemonic(_("Show box _lengths"));
  gtk_box_pack_start(GTK_BOX(vbox2), checkLengths, FALSE, FALSE, 0);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox2), hbox, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("x pos.")), TRUE, TRUE, 0);
  spinXLegPos = gtk_spin_button_new_with_range(0., 1., 0.1);
  gtk_box_pack_start(GTK_BOX(hbox), spinXLegPos, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("y pos.")), TRUE, TRUE, 0);
  spinYLegPos = gtk_spin_button_new_with_range(0., 1., 0.1);
  gtk_box_pack_start(GTK_BOX(hbox), spinYLegPos, FALSE, FALSE, 0);
/*   label = gtk_label_new(_("Draw basis set:")); */
/*   gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5); */
/*   gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0); */
  /* checkBasis = gtk_check_button_new(); */
/*   gtk_box_pack_start(GTK_BOX(hbox), checkBasis, FALSE, FALSE, 0); */
  g_signal_connect(G_OBJECT(lineBox), "use-changed",
		   G_CALLBACK(onExtensionUsed), (gpointer)visu_gl_ext_box_getDefault());
  g_signal_connect(G_OBJECT(lineBox), "width-changed",
		   G_CALLBACK(onBoxLineWidthChanged), (gpointer)0);
  g_signal_connect(G_OBJECT(lineBox), "color-changed",
		   G_CALLBACK(onBoxRGBValueChanged), (gpointer)0);
  g_signal_connect(G_OBJECT(lineBox), "stipple-changed",
		   G_CALLBACK(onBoxStippleChanged), (gpointer)0);
  g_signal_connect(G_OBJECT(checkLengths), "toggled",
		   G_CALLBACK(onBoxLengthsToggled), (gpointer)0);
  g_signal_connect(G_OBJECT(spinXLegPos), "value-changed",
		   G_CALLBACK(onBoxLegPosChanged), (gpointer)0);
  g_signal_connect(G_OBJECT(spinYLegPos), "value-changed",
		   G_CALLBACK(onBoxLegPosChanged), (gpointer)0);

  /*************/
  /* The Axes. */
  /*************/
  lineAxes = visu_ui_line_new(_("Basis set"));
  align = gtk_alignment_new(0.5, 0., 1, 1);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 0, 15, 0, 0);
  gtk_container_add(GTK_CONTAINER(align), lineAxes);
  gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, FALSE, 0);
  vbox2 = visu_ui_line_getOptionBox(VISU_UI_LINE(lineAxes));
  checkAxes = gtk_check_button_new_with_mnemonic(_("Use _box basis-set"));
  gtk_box_pack_start(GTK_BOX(vbox2), checkAxes, TRUE, TRUE, 0);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox2), hbox, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("x pos.")), TRUE, TRUE, 0);
  spinXPos = gtk_spin_button_new_with_range(0., 1., 0.1);
  gtk_box_pack_start(GTK_BOX(hbox), spinXPos, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("y pos.")), TRUE, TRUE, 0);
  spinYPos = gtk_spin_button_new_with_range(0., 1., 0.1);
  gtk_box_pack_start(GTK_BOX(hbox), spinYPos, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(lineAxes), "use-changed",
		   G_CALLBACK(onAxesUsed), (gpointer)0);
  g_signal_connect(G_OBJECT(lineAxes), "width-changed",
		   G_CALLBACK(onAxesWidthChanged), (gpointer)0);
  g_signal_connect(G_OBJECT(lineAxes), "color-changed",
		   G_CALLBACK(onAxesRGBChanged), (gpointer)0);
  g_signal_connect(G_OBJECT(lineAxes), "stipple-changed",
		   G_CALLBACK(onAxesStippleChanged), (gpointer)0);
  g_signal_connect(G_OBJECT(checkAxes), "toggled",
		   G_CALLBACK(onAxesToggled), (gpointer)0);
  g_signal_connect(G_OBJECT(spinXPos), "value-changed",
		   G_CALLBACK(onAxesPosChanged), (gpointer)0);
  g_signal_connect(G_OBJECT(spinYPos), "value-changed",
		   G_CALLBACK(onAxesPosChanged), (gpointer)0);

  /**************/
  /* The legend */
  /**************/
  hbox = gtk_hbox_new(FALSE, 0);
  align = gtk_alignment_new(0.5, 0., 1, 1);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 0, 15, 0, 0);
  gtk_container_add(GTK_CONTAINER(align), hbox);
  gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, FALSE, 0);

  /* The drawn checkbox. */
  checkLegend = gtk_check_button_new();
  gtk_box_pack_start(GTK_BOX(hbox), checkLegend, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(checkLegend), "toggled",
                   G_CALLBACK(onUseLegendChanged), (gpointer)0);

  /* The label. */
  label = gtk_label_new(_("<b>Legend</b>"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_widget_set_name(label, "label_head");
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 2);

  /*************************/
  /* Adding scale widgets. */
  /*************************/
  lineScales = visu_ui_line_new(_("Label"));
  align = gtk_alignment_new(0.5, 0., 1, 1);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 0, 15, 0, 0);
  gtk_container_add(GTK_CONTAINER(align), lineScales);
  gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, FALSE, 0);
  vbox2 = visu_ui_line_getOptionBox(VISU_UI_LINE(lineScales));
  g_signal_connect(G_OBJECT(lineScales), "use-changed",
		   G_CALLBACK(onExtensionUsed), (gpointer)visu_gl_ext_scale_getDefault());
  g_signal_connect(G_OBJECT(lineScales), "width-changed",
		   G_CALLBACK(onScalesWidthChanged), (gpointer)0);
  g_signal_connect(G_OBJECT(lineScales), "color-changed",
		   G_CALLBACK(onScalesRGBChanged), (gpointer)0);
  g_signal_connect(G_OBJECT(lineScales), "stipple-changed",
		   G_CALLBACK(onScalesStippleChanged), (gpointer)0);

  /* To be removed. */
  lblScales = gtk_label_new(_("<i><b>Several scales are defined from resource files,\n"
                              "but only one is editable.</b></i>"));
  gtk_label_set_use_markup(GTK_LABEL(lblScales), TRUE);
  gtk_box_pack_end(GTK_BOX(vbox2), lblScales, FALSE, FALSE, 10);
  gtk_widget_set_no_show_all(lblScales, TRUE);
  scale = visu_gl_ext_scale_getDefault();
  if (visu_gl_ext_scale_getNArrows(scale) > 0)
    {
      len = visu_gl_ext_scale_getLength(scale, 0);
      xyz = visu_gl_ext_scale_getOrigin(scale, 0);
      orientation = visu_gl_ext_scale_getOrientation(scale, 0);
      if (visu_gl_ext_scale_getNArrows(scale) > 1)
        gtk_widget_show(lblScales);
    }
  else
    {
      len = 5.;
      xyz = xyzDefault;
      orientation = orientationDefault;
    }

  /* code yoann*/
  hbox = gtk_hbox_new(FALSE,0);
  label = gtk_label_new(_("Legend:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox),label, FALSE, FALSE, 0); 
  entryScale = gtk_entry_new();
  gtk_widget_set_tooltip_text(entryScale,
		       _("Use blank legend to print the default"
			 " value with the distance."));
  gtk_box_pack_start(GTK_BOX(hbox), entryScale, TRUE, TRUE, 3);
  g_signal_connect(G_OBJECT(entryScale), "activate",
		   G_CALLBACK(onLegendChanged), (gpointer)0);
  
  label = gtk_label_new(_("Length:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);
 
  spinScaleLength = gtk_spin_button_new_with_range(1., 1000., 1.);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinScaleLength), len);
  gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spinScaleLength), 2);
  g_signal_connect((gpointer)spinScaleLength, "value-changed",
                   G_CALLBACK(scaleDistanceChanged), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hbox),spinScaleLength, FALSE, FALSE, 3); 
  gtk_box_pack_start(GTK_BOX(vbox2),hbox,FALSE,FALSE,5);

  table = gtk_table_new(3, 4, FALSE); /* je declare une nouvelle table*/
  gtk_box_pack_start(GTK_BOX(vbox2), table, FALSE, FALSE, 0); 

  label = gtk_label_new(_("Origin")); /* je cree le premier labelle*/
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);/*fonction pour placer le labelle a droite*/
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2, /*je positionne le label */
                   GTK_FILL | GTK_EXPAND, (GtkAttachOptions)0, 5, 0);/*j'indique qu il peut prendre toute la place qu'il a besoins*/
  label = gtk_label_new(_("Orientation")); /* je crée le second label*/
  gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3,/* je le positionne*/
                   GTK_FILL | GTK_EXPAND, (GtkAttachOptions)0, 5, 0);/* j'indique qui peut s etendre */

  for (i = 0; i < 3; i++)
    {
      gtk_table_attach(GTK_TABLE(table), gtk_label_new(lblXYZ[i]), 1 + i, 2 + i, 0, 1,
		       (GtkAttachOptions)0, (GtkAttachOptions)0, 0, 0);

      originespin[i] = gtk_spin_button_new_with_range(-10., 999., 1.);
      gtk_spin_button_set_value(GTK_SPIN_BUTTON(originespin[i]), xyz[i]);
      gtk_spin_button_set_digits(GTK_SPIN_BUTTON(originespin[i]), 2);
      g_signal_connect(G_OBJECT(originespin[i]), "value-changed",
		       G_CALLBACK(scaleOriginChanged), GINT_TO_POINTER(i));
      gtk_table_attach(GTK_TABLE(table), originespin[i], 1 + i, 2 + i, 1, 2,
		       (GtkAttachOptions)0, (GtkAttachOptions)0, 3, 0);
      orientationspin[i] = gtk_spin_button_new_with_range(-10., 999., 1.);
      gtk_spin_button_set_value(GTK_SPIN_BUTTON(orientationspin[i]), orientation[i]);
      gtk_spin_button_set_digits(GTK_SPIN_BUTTON(orientationspin[i]), 2);
      g_signal_connect(G_OBJECT(orientationspin[i]), "value-changed",
		       G_CALLBACK(scaleOrientationChanged), GINT_TO_POINTER(i));
      gtk_table_attach(GTK_TABLE(table), orientationspin[i], 1 + i, 2 + i, 2, 3,
		       (GtkAttachOptions)0, (GtkAttachOptions)0, 3, 0);
    }

  gtk_widget_show_all(vbox); /* fonctions qui permet de tous dessiner */

  return vbox;
}


/*************/
/* Callbacks */
/*************/
static void onResources(GObject *object _U_, VisuData *dataObj _U_, gpointer data)
{
  DBG_fprintf(stderr, "Panel Axes: catch the 'resourcesChanged'"
	      " signal, updating.\n");

  onEnter(VISU_UI_PANEL(panelAxes), data);
}
static void onViewAvail(VisuObject *obj _U_, VisuData *dataObj,
                        VisuGlView *view, gpointer data _U_)
{
  VisuBox *box;
  VisuNodeArray *nodes;

  DBG_fprintf(stderr, "Panel Axes: caught 'dataRendered' signal, attaching default axes.\n");
  box = (dataObj)?visu_boxed_getBox(VISU_BOXED(dataObj)):(VisuBox*)0;
  nodes = (dataObj)?VISU_NODE_ARRAY(dataObj):(VisuNodeArray*)0;

  visu_ui_panel_axes_setAxesExtension((dataObj)?visu_gl_ext_axes_getDefault():(VisuGlExtAxes*)0);

  if (visu_gl_ext_box_setBox(visu_gl_ext_box_getDefault(), box))
    visu_gl_ext_box_draw(visu_gl_ext_box_getDefault());

  visu_gl_ext_frame_setGlView(VISU_GL_EXT_FRAME(visu_gl_ext_box_legend_getDefault()), view);
  if (visu_gl_ext_box_legend_setBox(visu_gl_ext_box_legend_getDefault(), box))
    visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(visu_gl_ext_box_legend_getDefault()));

  visu_gl_ext_frame_setGlView(VISU_GL_EXT_FRAME(visu_gl_ext_legend_getDefault()), view);
  if (visu_gl_ext_legend_setNodes(visu_gl_ext_legend_getDefault(), nodes))
    visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(visu_gl_ext_legend_getDefault()));

  if (visu_gl_ext_scale_setGlView(visu_gl_ext_scale_getDefault(), view))
    visu_gl_ext_scale_draw(visu_gl_ext_scale_getDefault());
}
static void onEnter(VisuUiPanel *visu_ui_panel _U_, gpointer data _U_)
{
  float *xyz, *orientation, xpos, ypos;
  VisuGlExtScale *scale;
  float xyzDefault[3] = {0.f, 0.f, 0.f};
  float orientationDefault[3] = {1.f, 0.f, 0.f};
  gchar *legend;
  VisuGlExtBox *box;

  disableCallbacks = TRUE;

  /* Set the box values. */
  box = visu_gl_ext_box_getDefault();
  visu_ui_line_setUsed(VISU_UI_LINE(lineBox), visu_gl_ext_getActive(VISU_GL_EXT(box)));
  visu_ui_line_setWidth(VISU_UI_LINE(lineBox), visu_gl_ext_box_getLineWidth(box));
  visu_ui_line_setColor(VISU_UI_LINE(lineBox), visu_gl_ext_box_getRGB(box));
  visu_ui_line_setStipple(VISU_UI_LINE(lineBox), visu_gl_ext_box_getLineStipple(box));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkLengths),
			       visu_gl_ext_getActive(VISU_GL_EXT(visu_gl_ext_box_legend_getDefault())));
  visu_gl_ext_frame_getPosition(VISU_GL_EXT_FRAME(visu_gl_ext_box_legend_getDefault()),
                                &xpos, &ypos);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinXLegPos), xpos);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinYLegPos), ypos);

  /* Set the axes values. */
  if (currentAxes)
    {
      gtk_widget_set_sensitive(lineAxes, TRUE);
      visu_ui_line_setUsed(VISU_UI_LINE(lineAxes),
                           visu_gl_ext_getActive(VISU_GL_EXT(currentAxes)));
      visu_ui_line_setWidth(VISU_UI_LINE(lineAxes),
                            visu_gl_ext_axes_getLineWidth(currentAxes));
      visu_ui_line_setColor(VISU_UI_LINE(lineAxes),
                            visu_gl_ext_axes_getRGB(currentAxes));
      visu_ui_line_setStipple(VISU_UI_LINE(lineAxes),
                              visu_gl_ext_axes_getLineStipple(currentAxes));
      visu_gl_ext_axes_getPosition(currentAxes, &xpos, &ypos);
      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinXPos), xpos);
      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinYPos), ypos);
    }
  else
    gtk_widget_set_sensitive(lineAxes, FALSE);

  /* Set the scale class values. */
  scale = visu_gl_ext_scale_getDefault();
  visu_ui_line_setUsed(VISU_UI_LINE(lineScales), visu_gl_ext_getActive(VISU_GL_EXT(scale)));
  visu_ui_line_setWidth(VISU_UI_LINE(lineScales), visu_gl_ext_scale_getDefaultLineWidth());
  visu_ui_line_setColor(VISU_UI_LINE(lineScales), visu_gl_ext_scale_getDefaultRGB());
  visu_ui_line_setStipple(VISU_UI_LINE(lineScales), visu_gl_ext_scale_getDefaultStipple());

  /* To be removed. */
  gtk_widget_hide(lblScales);
  if (visu_gl_ext_scale_getNArrows(scale) > 0)
    {
      DBG_fprintf(stderr, "Panel Axes: set scales values from %p.\n",
		  (gpointer)scale);
      xyz = visu_gl_ext_scale_getOrigin(scale, 0);
      orientation = visu_gl_ext_scale_getOrientation(scale, 0);
      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinScaleLength),
				visu_gl_ext_scale_getLength(scale, 0));
      legend = (gchar*)visu_gl_ext_scale_getLegend(scale, 0);
      if (!legend)
	legend = "";
      if (visu_gl_ext_scale_getNArrows(scale) > 1)
        gtk_widget_show(lblScales);
    }
  else
    {
      xyz = xyzDefault;
      orientation = orientationDefault;
      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinScaleLength), 5.);
      legend = "";
    }
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(originespin[0]), xyz[0]);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(originespin[1]), xyz[1]);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(originespin[2]), xyz[2]);

  gtk_spin_button_set_value(GTK_SPIN_BUTTON(orientationspin[0]), orientation[0]);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(orientationspin[1]), orientation[1]);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(orientationspin[2]), orientation[2]);

  gtk_entry_set_text(GTK_ENTRY(entryScale), legend);
      
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkLegend),
			       visu_gl_ext_getActive(VISU_GL_EXT(visu_gl_ext_legend_getDefault())));

  disableCallbacks = FALSE;
}
static void onExtensionUsed(VisuUiLine *line _U_, gboolean used, gpointer data)
{
  if (disableCallbacks || !data)
    return;

  DBG_fprintf(stderr, "Panel Axes: toggle extension use.\n");
  visu_gl_ext_setActive(VISU_GL_EXT(data), used);
  if (VISU_IS_GL_EXT_SCALE(data))
    visu_gl_ext_scale_draw(VISU_GL_EXT_SCALE(data));
  else if (VISU_IS_GL_EXT_BOX(data))
    visu_gl_ext_box_draw(VISU_GL_EXT_BOX(data));
  VISU_REDRAW_ADD;
}

static void onAxesUsed(VisuUiLine *line _U_, gboolean used, gpointer data _U_)
{
  if (disableCallbacks || !currentAxes)
    return;

  visu_gl_ext_setActive(VISU_GL_EXT(currentAxes), used);
  visu_gl_ext_axes_draw(VISU_GL_EXT_AXES(currentAxes));
  VISU_REDRAW_ADD;
}
static void onAxesWidthChanged(VisuUiLine *line _U_, gint width, gpointer data _U_)
{
  if (disableCallbacks || !currentAxes)
    return;

  if (visu_gl_ext_axes_setLineWidth(currentAxes, (float)width))
    {
      visu_gl_ext_axes_draw(currentAxes);
      VISU_REDRAW_ADD;
    }
}
static void onAxesRGBChanged(VisuUiLine *line _U_, float *rgb, gpointer data _U_)
{
  if (disableCallbacks || !currentAxes)
    return;

  if (visu_gl_ext_axes_setRGB(currentAxes, rgb, TOOL_COLOR_MASK_RGBA))
    {
      visu_gl_ext_axes_draw(currentAxes);
      VISU_REDRAW_ADD;
    }
}
static void onAxesStippleChanged(VisuUiLine *line _U_, gint stipple, gpointer data _U_)
{
  if (disableCallbacks || !currentAxes)
    return;

  if (visu_gl_ext_axes_setLineStipple(currentAxes, stipple))
    {
      visu_gl_ext_axes_draw(currentAxes);
      VISU_REDRAW_ADD;
    }
}
static void onAxesToggled(GtkToggleButton *toggle, gpointer data _U_)
{
  VisuData *dataObj;

  if (disableCallbacks || !currentAxes)
    return;

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelAxes));
  if (visu_gl_ext_axes_setBasisFromBox
      (currentAxes,
       (gtk_toggle_button_get_active(toggle))?visu_boxed_getBox(VISU_BOXED(dataObj)):(VisuBox*)0))
    {
      visu_gl_ext_axes_draw(currentAxes);
      VISU_REDRAW_ADD;
    }
}
static void onAxesPosChanged(GtkSpinButton *spin _U_, gpointer data _U_)
{
  if (disableCallbacks || !currentAxes)
    return;
  
  if (visu_gl_ext_axes_setPosition(currentAxes,
                                   (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinXPos)),
                                   (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinYPos))))
    {
      visu_gl_ext_axes_draw(currentAxes);
      VISU_REDRAW_ADD;
    }
}

static void scaleDistanceChanged(GtkSpinButton *spin, gpointer data _U_)
{
  gboolean res;
  VisuGlExtScale *scale;
  float xyz[3], orientation[3], len;

  if (disableCallbacks)
    return;

  scale = visu_gl_ext_scale_getDefault();
  if (visu_gl_ext_scale_getNArrows(scale) == 0)
    {
      xyz[0] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(originespin[0]));
      xyz[1] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(originespin[1]));
      xyz[2] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(originespin[2]));
      orientation[0] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(orientationspin[0]));
      orientation[1] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(orientationspin[1]));
      orientation[2] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(orientationspin[2]));
      len = gtk_spin_button_get_value(GTK_SPIN_BUTTON(spin)) / 2.f;
      visu_gl_ext_scale_add(scale, xyz, orientation, len, (const gchar*)0);
    }

  DBG_fprintf(stderr, "Panel Box&axes: scale distance changed.\n");
  res = visu_gl_ext_scale_setLength(scale, 0,
                                    (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spin)));
  if (*gtk_entry_get_text(GTK_ENTRY(entryScale)) == '\0')
    visu_gl_ext_scale_setLegend(scale, 0, (const gchar*)0);
  else
    visu_gl_ext_scale_setLegend(scale, 0, gtk_entry_get_text(GTK_ENTRY(entryScale)));

  if (res)
    {
      visu_gl_ext_scale_draw(scale);
      VISU_REDRAW_ADD;
    }
}

static void scaleOriginChanged(GtkSpinButton *spin, gpointer data)
{
  gboolean res;
  int xyzMask[3] = {TOOL_XYZ_MASK_X, TOOL_XYZ_MASK_Y, TOOL_XYZ_MASK_Z};
  int val;
  VisuGlExtScale *scale;
  float xyz[3], orientation[3];

  val = GPOINTER_TO_INT(data);
  g_return_if_fail(val >= 0 && val < 3);

  if (disableCallbacks)
    return;

  scale = visu_gl_ext_scale_getDefault();
  if (visu_gl_ext_scale_getNArrows(scale) == 0)
    {
      xyz[0] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(originespin[0]));
      xyz[1] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(originespin[1]));
      xyz[2] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(originespin[2]));
      orientation[0] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(orientationspin[0]));
      orientation[1] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(orientationspin[1]));
      orientation[2] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(orientationspin[2]));
      xyz[val] += 1.f;
      visu_gl_ext_scale_add(scale, xyz, orientation,
                            gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinScaleLength)),
                            (const gchar*)0);
    }

  DBG_fprintf(stderr, "Panel Box&axes: scale origine %d change.\n", val);
  xyz[val] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(spin));
  res = visu_gl_ext_scale_setOrigin(scale, 0, xyz, xyzMask[val]);

  if (res)
    {
      visu_gl_ext_scale_draw(scale);
      VISU_REDRAW_ADD;
    }
}

static void scaleOrientationChanged(GtkSpinButton *spin, gpointer data)
{
  gboolean res;
  int xyzMask[3] = {TOOL_XYZ_MASK_X, TOOL_XYZ_MASK_Y, TOOL_XYZ_MASK_Z};
  int val;
  VisuGlExtScale *scale;
  float xyz[3], orientation[3];

  val = GPOINTER_TO_INT(data);
  g_return_if_fail(val >= 0 && val < 3);

  if (disableCallbacks)
    return;

  scale = visu_gl_ext_scale_getDefault();
  if (visu_gl_ext_scale_getNArrows(scale) == 0)
    {
      xyz[0] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(originespin[0]));
      xyz[1] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(originespin[1]));
      xyz[2] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(originespin[2]));
      orientation[0] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(orientationspin[0]));
      orientation[1] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(orientationspin[1]));
      orientation[2] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(orientationspin[2]));
      orientation[val] += 999.f;
      visu_gl_ext_scale_add(scale, xyz, orientation,
                            gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinScaleLength)),
                            (const gchar*)0);
    }

  DBG_fprintf(stderr, "Panel Box&axes: scale orientation %d changed.\n", val);
  xyz[val] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(spin));
  res = visu_gl_ext_scale_setOrientation(scale, 0, xyz, xyzMask[val]);

  if (res)
    {
      visu_gl_ext_scale_draw(scale);
      VISU_REDRAW_ADD;
    }
}
static void onLegendChanged(GtkEntry *entry, gpointer data _U_)
{
  gboolean res;
  VisuGlExtScale *scale;
  float xyz[3], orientation[3];

  if (disableCallbacks)
    return;

  scale = visu_gl_ext_scale_getDefault();
  if (visu_gl_ext_scale_getNArrows(scale) == 0)
    {
      xyz[0] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(originespin[0]));
      xyz[1] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(originespin[1]));
      xyz[2] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(originespin[2]));
      orientation[0] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(orientationspin[0]));
      orientation[1] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(orientationspin[1]));
      orientation[2] = gtk_spin_button_get_value(GTK_SPIN_BUTTON(orientationspin[2]));
      visu_gl_ext_scale_add(scale, xyz, orientation,
                            gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinScaleLength)),
                            (const gchar*)0);
    }

  DBG_fprintf(stderr, "Panel Box&axes: scale legend changed.\n");
  res = visu_gl_ext_scale_setLegend(scale, 0, gtk_entry_get_text(entry));

  if (res)
    {
      visu_gl_ext_scale_draw(scale);
      VISU_REDRAW_ADD;
    }
}
static void onScalesWidthChanged(VisuUiLine *line _U_, gint width, gpointer data _U_)
{
  if (visu_gl_ext_scale_setDefaultLineWidth(width))
    {
      visu_gl_ext_scale_draw(visu_gl_ext_scale_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void onScalesRGBChanged(VisuUiLine *line _U_, float *rgb, gpointer data _U_)
{
  float rgba[4];

  memcpy(rgba, rgb, sizeof(float) * 3);
  rgba[3] = 1.f;
  if (visu_gl_ext_scale_setDefaultRGB(rgba, TOOL_COLOR_MASK_RGBA))
    {
      visu_gl_ext_scale_draw(visu_gl_ext_scale_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void onScalesStippleChanged(VisuUiLine *line _U_, gint stipple, gpointer data _U_)
{
  if (visu_gl_ext_scale_setDefaultStipple(stipple))
    {
      visu_gl_ext_scale_draw(visu_gl_ext_scale_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void onUseLegendChanged(GtkToggleButton *toggle, gpointer data _U_)
{
  if (disableCallbacks)
    return;

  visu_gl_ext_setActive(VISU_GL_EXT(visu_gl_ext_legend_getDefault()),
                           gtk_toggle_button_get_active(toggle));
  visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(visu_gl_ext_legend_getDefault()));
  VISU_REDRAW_ADD;
}

static void onBoxLengthsToggled(GtkToggleButton *toggle, gpointer data _U_)
{
  if (disableCallbacks)
    return;

  visu_gl_ext_setActive(VISU_GL_EXT(visu_gl_ext_box_legend_getDefault()),
                           gtk_toggle_button_get_active(toggle));
  visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(visu_gl_ext_box_legend_getDefault()));
  VISU_REDRAW_ADD;
}
static void onBoxLineWidthChanged(VisuUiLine *line _U_, gint width, gpointer data _U_)
{
  if (disableCallbacks)
    return;

  if (visu_gl_ext_box_setLineWidth(visu_gl_ext_box_getDefault(), (float)width))
    {
      visu_gl_ext_box_draw(visu_gl_ext_box_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void onBoxStippleChanged(VisuUiLine *line _U_, gint stipple, gpointer data _U_)
{
  if (disableCallbacks)
    return;

  if (visu_gl_ext_box_setLineStipple(visu_gl_ext_box_getDefault(), stipple))
    {
      visu_gl_ext_box_draw(visu_gl_ext_box_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void onBoxRGBValueChanged(VisuUiLine *line _U_, float *rgb, gpointer data _U_)
{
  if (disableCallbacks)
    return;

  if (visu_gl_ext_box_setRGB(visu_gl_ext_box_getDefault(), rgb, TOOL_COLOR_MASK_RGBA))
    {
      visu_gl_ext_box_draw(visu_gl_ext_box_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void onBoxLegPosChanged(GtkSpinButton *spin _U_, gpointer data _U_)
{
  if (disableCallbacks)
    return;
  
  if (visu_gl_ext_frame_setPosition
      (VISU_GL_EXT_FRAME(visu_gl_ext_box_legend_getDefault()),
       (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinXLegPos)),
       (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinYLegPos))))
    {
      visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(visu_gl_ext_box_legend_getDefault()));
      VISU_REDRAW_ADD;
    }
}
