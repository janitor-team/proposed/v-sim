/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include <math.h>

#include "gtkAtomic.h"

#include <visu_elements.h>
#include <renderingMethods/renderingAtomic.h>
#include <extraGtkFunctions/gtk_numericalEntryWidget.h>
#include "panelElements.h"
#include "panelMethod.h"
#include <visu_gtk.h>
#include <support.h>
#include <extensions/forces.h>

/**
 * SECTION:gtkAtomic
 * @short_description: The gtk interface elements that are specific to
 * the atomic rendering method.
 *
 * <para>This part gathers all the routine specific to the widgets
 * related to the atomic rendering method. For the method itself,
 * there is no specific widgets. For the #VisuElement part, one can
 * tune the shape, the radius and the elipsoid orientation (when
 * selected). There is no specific #GtkFileChooser widget.</para>
 */

#define SPIN_ATOMIC_RADIUS_UPPER (gdouble)999.
#define SPIN_ATOMIC_RADIUS_LOWER (gdouble)0.001
#define SPIN_ATOMIC_RADIUS_STEP  (gdouble)0.05
#define LABEL_RADIUS      _("Radius:")

/* the spin button to control the radius. */
static GtkWidget *spinRadius;
static GtkWidget *entryShape;
static GtkWidget *labelRadius;
static GtkWidget *spinRatio;
static GtkWidget *spinPhi;
static GtkWidget *spinTheta;
static GtkWidget *checkForces;
static GtkWidget *radioScaleAuto;
static GtkWidget *entryScale;
static GtkWidget *labelMaxForces;
static gulong configResourcesLoadedId;

enum
  {
    paramRadius,
    paramShape,
    paramRatio,
    paramPhi,
    paramTheta
  };

/* Callbacks */
static gboolean disableSignals = FALSE;
static void paramChanged(GtkSpinButton* button, gpointer data);
static void shapeChanged(GtkComboBox *box, gpointer data);
static void updateOnResources(GObject *obj, VisuData *dataObj, gpointer data);
static void onDrawForces(GtkToggleButton *toggle, gpointer data);
static void onDataReady(GObject *obj, VisuData *dataObj, VisuGlView *view, gpointer data);
static void onScaleForces(GtkToggleButton *toggle, gpointer data);
static void onEntryForces(VisuUiNumericalEntry *entry, gdouble old_value, gpointer data);
static gboolean onForcesHook(GSignalInvocationHint *ihint, guint nvalues,
                             const GValue *param_values, gpointer data);

/* Local routines. */
static GtkWidget* createGtkConfigForAtomicMethod(void);
static void destroyGtkConfigForAtomicMethod();
static void updateLabelMaxForces();


/***************/
/* Public part */
/***************/

/**
 * visu_ui_panel_elements_atomic_init: (skip)
 *
 * Initialise the specific area in the element panel
 * for the atomic rendering method.
 */
void visu_ui_panel_elements_atomic_init()
{
  visu_ui_panel_elements_setMethod(visu_rendering_getByName
				    (VISU_RENDERING_ATOMIC_NAME),
				    visu_ui_panel_elements_atomic_onChange,
				    visu_ui_panel_elements_atomic_initBuild);
}

/**
 * visu_ui_panel_elements_atomic_initOpen: (skip)
 *
 * Initialise the gtk methods associated with
 * the atomic rendering method.
 */
void visu_ui_panel_elements_atomic_initOpen()
{
  DBG_fprintf(stderr, "Gtk Atomic: set no specific file method.\n");
  visu_ui_setRenderingSpecificMethod(visu_rendering_getByName
				      (VISU_RENDERING_ATOMIC_NAME),
				      (VisuUiSetFilesFunc)0);
}

/**
 * visu_ui_panel_elements_atomic_initMethod: (skip)
 *
 * Initialise widgets related to the atomic rendering methods.
 *
 * Since: 3.7
 */
void visu_ui_panel_elements_atomic_initMethod()
{
  visu_ui_panel_method_set(visu_rendering_getByName
                           (VISU_RENDERING_ATOMIC_NAME),
                           createGtkConfigForAtomicMethod,
                           destroyGtkConfigForAtomicMethod);

  /* May draw Forces on new data. */
  labelMaxForces = (GtkWidget*)0;
  radioScaleAuto = (GtkWidget*)0;
  entryScale     = (GtkWidget*)0;
  checkForces    = (GtkWidget*)0;
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataRendered",
                   G_CALLBACK(onDataReady), NULL);
  g_signal_add_emission_hook(g_signal_lookup("ForcesChanged",
                                             VISU_TYPE_RENDERING_ATOMIC),
                             0, onForcesHook, (gpointer)0, (GDestroyNotify)0);
}

/**
 * visu_ui_panel_elements_atomic_initBuild: (skip)
 * 
 * Create the gtk widgets (a hbox with a spin with
 *  positive values) and return it.
 *
 * Returns: newly created widgets to handle atomic rendering
 * characteristics of elements.
 */
GtkWidget* visu_ui_panel_elements_atomic_initBuild()
{
  GtkWidget* hbox, *vbox;
  GtkWidget* label;
  GtkWidget *comboShape;
  const char **names, **ids;
  int i;

  DBG_fprintf(stderr, "GTK Atomic: create the gtk interface.\n");

  vbox = gtk_vbox_new(FALSE, 0);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);

  labelRadius = gtk_label_new("");
  gtk_label_set_text(GTK_LABEL(labelRadius), LABEL_RADIUS);
  gtk_box_pack_start(GTK_BOX(hbox), labelRadius, FALSE, FALSE, 1);

  spinRadius = gtk_spin_button_new_with_range(SPIN_ATOMIC_RADIUS_LOWER,
                                              SPIN_ATOMIC_RADIUS_UPPER,
                                              SPIN_ATOMIC_RADIUS_STEP);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinRadius), 1.0);
  gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spinRadius), 3);
  gtk_box_pack_start(GTK_BOX(hbox), spinRadius, FALSE,FALSE, 3);

  label = gtk_label_new(_("Shape: "));
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 1);
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);

  comboShape = gtk_combo_box_text_new();
  names = visu_rendering_atomic_getAllShapesI18n();
  ids   = visu_rendering_atomic_getAllShapes();
  if (names && ids)
    for (i = 0; names[i] && ids[i]; i++)
      gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(comboShape), ids[i], names[i]);
  else
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(comboShape), (const gchar*)0,
			      (char*)visu_rendering_atomic_getShapeNameDefault());
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboShape), 0);
  entryShape = comboShape;
  /* set callback for the combo button. */
  g_signal_connect(G_OBJECT(entryShape), "changed",
		   G_CALLBACK(shapeChanged), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hbox), comboShape, FALSE, FALSE, 3);

  /* set callback for the spin button. */
  g_signal_connect((gpointer)spinRadius, "value-changed",
		   G_CALLBACK(paramChanged), GINT_TO_POINTER(paramRadius));

  /* Set widgets for the elipsoid parameters. */
  label = gtk_label_new("");
  gtk_label_set_markup(GTK_LABEL(label), _("Parameters for elipsoid shape"));
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 5);
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);
  label = gtk_label_new(_("Ratio: "));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_misc_set_padding(GTK_MISC(label), 10, 0);
  spinRatio = gtk_spin_button_new_with_range(1., 10., 0.1);
  gtk_box_pack_end(GTK_BOX(hbox), spinRatio, FALSE, FALSE, 0);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);
  label = gtk_label_new(_("Phi: "));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_misc_set_padding(GTK_MISC(label), 10, 0);
  spinPhi = gtk_spin_button_new_with_range(-180., 180., 1.);
  gtk_box_pack_end(GTK_BOX(hbox), spinPhi, FALSE, FALSE, 0);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);
  label = gtk_label_new(_("Theta: "));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_misc_set_padding(GTK_MISC(label), 10, 0);
  spinTheta = gtk_spin_button_new_with_range(-180., 180., 1.);
  gtk_box_pack_end(GTK_BOX(hbox), spinTheta, FALSE, FALSE, 0);

  g_signal_connect((gpointer)spinRatio, "value-changed",
		   G_CALLBACK(paramChanged), GINT_TO_POINTER(paramRatio));
  g_signal_connect((gpointer)spinPhi, "value-changed",
		   G_CALLBACK(paramChanged), GINT_TO_POINTER(paramPhi));
  g_signal_connect((gpointer)spinTheta, "value-changed",
		   G_CALLBACK(paramChanged), GINT_TO_POINTER(paramTheta));
  

  gtk_widget_show_all(vbox);
  return vbox;
}

/**
 * visu_ui_panel_elements_atomic_onChange:
 * @eleList: (element-type VisuElement*): a list of #VisuElement.
 *
 * Call this routine with a list of #VisuElement to change the values
 * shown in the widgets controling the atomic rendering. The values to
 * be put are read from the first #VisuElement of the list. The list
 * is then stored for future changes to the widgets will be applied to
 * all these elements.
 */
void visu_ui_panel_elements_atomic_onChange(GList *eleList)
{
  float radius, ratio, phi, theta;
  int shape;
  VisuElement *ele;

  DBG_fprintf(stderr, "Gtk Atomic: changing values on element change.\n");
  if (!eleList)
    return;

  ele = (VisuElement*)eleList->data;

  disableSignals = TRUE;

  /* Change the radius for the new element. */
  DBG_fprintf(stderr, " | radius for %s.\n", ele->name);
  radius = visu_rendering_atomic_getRadius(ele);
  if (radius >= 0.)
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinRadius), radius);
  else
    {
      g_warning("Can't find a value for radius of element '%s'.\n", ele->name);
      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinRadius),
				visu_rendering_atomic_getRadiusDefault());
    }
  /* Change the shape for the new element. */
  DBG_fprintf(stderr, " | shape.\n");
  shape = visu_rendering_atomic_getShape(ele);
  if (shape >= 0)
    gtk_combo_box_set_active(GTK_COMBO_BOX(entryShape), shape);
  else
    {
      g_warning("Can't find the shape of element '%s'.\n", ele->name);
      gtk_combo_box_set_active(GTK_COMBO_BOX(entryShape), visu_rendering_atomic_getShapeDefault());
    }
  /* Change the elipsoid parameters. */
  DBG_fprintf(stderr, " | ellipsoid.\n");
  ratio = visu_rendering_atomic_getElipsoidRatio(ele);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinRatio), ratio);
  phi = visu_rendering_atomic_getElipsoidPhi(ele);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinPhi), phi);
  theta = visu_rendering_atomic_getElipsoidTheta(ele);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinTheta), theta);
  DBG_fprintf(stderr, " | Done.\n");

  disableSignals = FALSE;
}

/* Creates the vbox displayed in the config panel */
static GtkWidget* createGtkConfigForAtomicMethod()
{
  GtkWidget *vbox, *wd, *hbox, *radio, *vbox2;
  GSList *lst;
  gfloat scale;

  DBG_fprintf(stderr, "Gtk Atomic: building specific widgets for method.\n");

  vbox = gtk_vbox_new(FALSE, 0);

  checkForces = gtk_check_button_new_with_mnemonic
    (_("Display _forces (if available)"));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkForces),
                               visu_gl_ext_getActive(VISU_GL_EXT(visu_gl_ext_forces_getDefault())));
  g_signal_connect(G_OBJECT(checkForces), "toggled",
                   G_CALLBACK(onDrawForces), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(vbox), checkForces, FALSE, FALSE, 0);
  vbox2 = gtk_vbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), vbox2, FALSE, FALSE, 0);
  wd = gtk_label_new(_("Policy to scale arrows:"));
  gtk_misc_set_alignment(GTK_MISC(wd), 0., 0.5);
  gtk_misc_set_padding(GTK_MISC(wd), 10, 0);
  gtk_box_pack_start(GTK_BOX(vbox2), wd, FALSE, FALSE, 0);
  /* hbox = gtk_hbox_new(FALSE, 0); */
  /* gtk_box_pack_start(GTK_BOX(vbox2), hbox, FALSE, FALSE, 0); */
  /* radio = gtk_radio_button_new_with_label(NULL, _("per node type")); */
  /* gtk_radio_button_set_group(GTK_RADIO_BUTTON(radio), (GSList*)0); */
  /* lst = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radio)); */
  /* gtk_box_pack_start(GTK_BOX(hbox), radio, TRUE, TRUE, 15); */
  scale = visu_gl_ext_node_vectors_getNormalisation(VISU_GL_EXT_NODE_VECTORS(visu_gl_ext_forces_getDefault()));
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox2), hbox, FALSE, FALSE, 0);
  radioScaleAuto = gtk_radio_button_new_with_label(NULL, _("automatic"));
  if (scale > 0.f)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioScaleAuto), FALSE);
  g_signal_connect(G_OBJECT(radioScaleAuto), "toggled",
                   G_CALLBACK(onScaleForces), (gpointer)0);
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(radioScaleAuto), (GSList*)0);
  lst = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radioScaleAuto));
  gtk_box_pack_start(GTK_BOX(hbox), radioScaleAuto, TRUE, TRUE, 15);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox2), hbox, FALSE, FALSE, 0);
  radio = gtk_radio_button_new_with_label(NULL, _("manual"));
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(radio), lst);
  if (scale > 0.f)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio), TRUE);
  g_signal_connect(G_OBJECT(radio), "toggled",
                   G_CALLBACK(onScaleForces), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hbox), radio, FALSE, FALSE, 15);
  entryScale = visu_ui_numerical_entry_new(1e-2);
  gtk_entry_set_width_chars(GTK_ENTRY(entryScale), 6);
  if (scale > 0.f)
    visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryScale), scale);
  g_signal_connect(G_OBJECT(entryScale), "value-changed",
                   G_CALLBACK(onEntryForces), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hbox), entryScale, FALSE, FALSE, 0);
  labelMaxForces = gtk_label_new("");
  gtk_misc_set_alignment(GTK_MISC(labelMaxForces), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox), labelMaxForces, TRUE, TRUE, 5);
  updateLabelMaxForces();
  
  gtk_widget_show_all(vbox);

  configResourcesLoadedId =
    g_signal_connect(VISU_OBJECT_INSTANCE, "resourcesLoaded",
                     G_CALLBACK(updateOnResources), NULL);

  return vbox;
}
static void destroyGtkConfigForAtomicMethod()
{
  DBG_fprintf(stderr, "Gtk Atomic: remove signal from config rendering widget.\n");
  g_signal_handler_disconnect(VISU_OBJECT_INSTANCE, configResourcesLoadedId);
}



/****************/
/* Private part */
/****************/
static void shapeChanged(GtkComboBox *box, gpointer data _U_)
{
  int shape;
  GList *tmpLst, *eleList;
  gboolean refresh;
  VisuElement *ele;
  VisuGlView *view;
  VisuRendering *method;

  if (disableSignals)
    return;

  eleList = visu_ui_panel_elements_getSelected();
  if (!eleList)
    return;

  shape = (int)gtk_combo_box_get_active(box);
  method = visu_object_getRendering(VISU_OBJECT_INSTANCE);
  view = visu_ui_panel_getView(VISU_UI_PANEL(VISU_UI_PANEL_ELEMENTS));
  refresh = FALSE;
  for (tmpLst = eleList; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      ele = (VisuElement*)tmpLst->data;
      if (visu_rendering_atomic_setShape(ele, shape))
	{
          visu_rendering_createElement(method, ele, view);
	  refresh = TRUE;
	}
    }
  g_list_free(eleList);
  if (refresh)
    VISU_REDRAW_ADD;
}

static void paramChanged(GtkSpinButton* button, gpointer data)
{
  int param;
  float value;
  VisuElement *ele;
  VisuGlView *view;
  GList *tmpLst, *eleList;
  gboolean refresh, res;
  VisuRendering *method;

  if (disableSignals)
    return;

  eleList = visu_ui_panel_elements_getSelected();
  if (!eleList)
    return;

  view = visu_ui_panel_getView(VISU_UI_PANEL(VISU_UI_PANEL_ELEMENTS));
  param = GPOINTER_TO_INT(data);
  value = gtk_spin_button_get_value(button);
  method = visu_object_getRendering(VISU_OBJECT_INSTANCE);
  refresh = FALSE;
  for (tmpLst = eleList; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      ele = (VisuElement*)tmpLst->data;
      switch (param)
	{
	case paramRadius:
	  res = visu_rendering_atomic_setRadius(ele, value);
	  break;
	case paramRatio:
	  res = visu_rendering_atomic_setElipsoidRatio(ele, value);
	  break;
	case paramPhi:
	  res = visu_rendering_atomic_setElipsoidPhi(ele, value);
	  break;
	case paramTheta:
	  res = visu_rendering_atomic_setElipsoidTheta(ele, value);
	  break;
	default:
	  res = FALSE;
	}
      if (res)
	{
          visu_rendering_createElement(method, ele, view);
	  refresh = TRUE;
	}
    }
  g_list_free(eleList);
  if (refresh)
    VISU_REDRAW_ADD;
}

static void updateOnResources(GObject *obj _U_, VisuData *dataObj _U_, gpointer data _U_)
{
}

static void updateLabelMaxForces()
{
  float max;
  VisuData *dataObj;
  gchar *str;

  if (!labelMaxForces)
    return;

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(VISU_UI_PANEL_ELEMENTS));
  if (!dataObj)
    max = 0.f;
  else
    max = visu_rendering_atomic_getMaxForces(dataObj);
      
  if (max > 0.f)
    {
      str = g_strdup_printf(_("(max. force is %.4g)"), max);
      gtk_label_set_text(GTK_LABEL(labelMaxForces), str);
      g_free(str);
    }
  else
    gtk_label_set_text(GTK_LABEL(labelMaxForces), _("(No force data)"));
}

static void onDrawForces(GtkToggleButton *toggle, gpointer data _U_)
{
  if (visu_gl_ext_setActive(VISU_GL_EXT(visu_gl_ext_forces_getDefault()),
                            gtk_toggle_button_get_active(toggle)))
    {
      visu_gl_ext_node_vectors_draw(VISU_GL_EXT_NODE_VECTORS(visu_gl_ext_forces_getDefault()));
      VISU_REDRAW_ADD;
    }
}

static void onDataReady(GObject *obj _U_, VisuData *dataObj _U_,
                        VisuGlView *view _U_, gpointer data _U_)
{
  updateLabelMaxForces();
}

static void onScaleForces(GtkToggleButton *toggle, gpointer data _U_)
{
  VisuGlExtNodeVectors *forces;
  gfloat scale;

  if (!gtk_toggle_button_get_active(toggle))
    return;

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioScaleAuto)))
    scale = -1.f;
  else
    scale = (entryScale)?(float)visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryScale)):-1.f;

  forces = VISU_GL_EXT_NODE_VECTORS(visu_gl_ext_forces_getDefault());
  if (visu_gl_ext_node_vectors_setNormalisation(forces, scale))
    visu_gl_ext_node_vectors_draw(forces);

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkForces)))
    VISU_REDRAW_ADD;
}

static void onEntryForces(VisuUiNumericalEntry *entry _U_,
                          gdouble old_value _U_, gpointer data _U_)
{
  VisuGlExtNodeVectors *forces;
  gfloat scale;

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioScaleAuto)))
    scale = -1.f;
  else
    scale = (entryScale)?(float)visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryScale)):-1.f;

  forces = VISU_GL_EXT_NODE_VECTORS(visu_gl_ext_forces_getDefault());
  if (visu_gl_ext_node_vectors_setNormalisation(forces, scale))
    visu_gl_ext_node_vectors_draw(forces);

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkForces)) &&
      !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioScaleAuto)))
    VISU_REDRAW_ADD;
}

static gboolean onForcesHook(GSignalInvocationHint *ihint _U_, guint nvalues _U_,
                             const GValue *param_values _U_, gpointer data _U_)
{
  DBG_fprintf(stderr, "Panel Method: new forces available.\n");
  updateLabelMaxForces();

  return TRUE;
}
