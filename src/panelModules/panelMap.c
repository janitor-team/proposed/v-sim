/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresses m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "panelMap.h"
#include "panelPlanes.h"
#include "panelSurfaces.h"

#include <GL/gl.h>

#include <support.h>
#include <visu_object.h>
#include <visu_extension.h>
#include <visu_gtk.h>
#include <gtk_main.h>
#include <coreTools/toolMatrix.h>
#include <extraFunctions/plane.h>
#include <extraFunctions/scalarFields.h>
#include <extraGtkFunctions/gtk_toolPanelWidget.h>
#include <extraGtkFunctions/gtk_shadeComboBoxWidget.h>
#include <extraGtkFunctions/gtk_colorComboBoxWidget.h>
#include <extraGtkFunctions/gtk_numericalEntryWidget.h>
#include <openGLFunctions/objectList.h>
#include <openGLFunctions/view.h>
#include <openGLFunctions/text.h>
#include <extensions/shade.h>
#include <extensions/maps.h>

/**
 * SECTION:panelMap
 * @short_description: The widget to create coloured map.
 *
 * <para>This is the user interface for the coloured maps. For a
 * plane, a scalar field and a shade, it is possible to draw one
 * coloured plane. The available planes are taken from the #panelVisuPlane
 * subpanel and the scalar field for the #panelSurfaces.</para>
 */

/* Local variables. */
static GtkWidget *panelMap;
static GtkWidget *comboVisuPlane;
static GtkWidget *comboField;
static GtkWidget *comboToolShade, *ckAlpha;
static GtkWidget *comboMap;
static GtkWidget *spinPrecision;
static GtkWidget *rdLinear, *rdLog, *rdZero;
static GtkWidget *ckColour, *cbColour;
static GtkWidget *spinIsoLines;
static GtkWidget *removeButton, *buildButton, *exportButton;
static VisuGlExtMaps *extMaps;
static VisuGlExtShade *extLegend;
static float drawnMinMax[2] = {G_MAXFLOAT, -G_MAXFLOAT};
static GtkWidget *radioNormalized, *radioMinMax;
static GtkWidget *hboxEntries, *entryDataMax, *entryDataMin;
static gboolean isMapInitialised;
static GtkWidget *warnVisuPlane, *warnField, *warnLabel;
static gulong comboMap_signal;

/* String used to labelled planes, dist. means 'distance' and
   norm. means 'normal' (50 chars max). */
#define LABEL_PLANE _("<span size=\"small\">plane (%2d;%2d;%2d - %4.1f)</span>")
enum
  {
    MAP_PLANE,
    MAP_MOVE_SIGNAL,
    MAP_LABEL,
    MAP_OBJ,
    MAP_N_COLUMNS
  };
static GtkListStore *maps;

/* Local methods. */
static void createGtkInterface(VisuUiPanel *panel);
static gboolean getElements(VisuPlane **plane, VisuScalarField **field, ToolShade **shade);
static void getParameters(ToolMatrixScalingFlag *scale, float **rgb,
			  float manualMinMax[2], gboolean *useMinMax,
			  float *precision, guint *nLines);
static gboolean setupMap( gpointer data);
static gboolean rebuildFromField(gpointer data);
static VisuPlane* removeMap(GtkTreeIter *iter);
static void updateInterface(gboolean selectFirst);
static void setField(VisuMap *map);
static void setCompute(VisuMap *map);
static gboolean setGlobalMinMax();
static gboolean setIsoLine(VisuMap *map);
static VisuMap* addMap();
static gboolean checkAvailability();

/* Local callbacks. */
static void onBuildClicked(GtkButton *button, gpointer data);
static void onRemoveClicked(GtkButton *button, gpointer data);
static void onExportClicked(GtkButton *button, gpointer data);
static void onPrecisionChanged(GtkSpinButton *spin, gpointer data);
static void onFieldChanged(GtkComboBox *combo, gpointer data);
static void onToolShadeChanged(VisuUiShadeCombobox *combo, ToolShade *shade, gpointer data);
static void onComboVisuPlaneChanged(GtkComboBox *combo, gpointer data);
static void onComboMapChanged(GtkComboBox *combo, gpointer data);
static void onPlaneMoved(VisuPlane *plane, gpointer data);
static void onScaleChanged(GtkToggleButton *button, gpointer data);
static void onAlphaChanged(GtkToggleButton *button, gpointer data);
static void onUseColourChanged(GtkToggleButton *button, gpointer data);
static void onIsoChanged(GtkSpinButton *button, gpointer data);
static void onColorChange(VisuUiColorCombobox *combo, ToolColor *selectedColor, gpointer data);
static void onScaleTypeChange(GtkToggleButton *toggle, gpointer data);
static void onEntryMinMaxChangeValue(VisuUiNumericalEntry *entry,
				     double oldValue, gpointer data);
static void onMapEnter(VisuUiPanel *map, gpointer data);
static void onViewAvail(VisuObject *obj, VisuData *dataObj, VisuGlView *view, gpointer data);
static void onPlaneDeleted(GtkTreeModel *tree_model, GtkTreePath  *path,
			   gpointer user_data);

static void createGtkInterface(VisuUiPanel *panel)
{
  GtkWidget *vbox, *hbox, *label, *combo, *align, *wd;
  GtkListStore *list;
  GtkCellRenderer *renderer;
  ToolColor *color;
  float black[4] = {0.f, 0.f, 0.f, 1.f};
  int pos;

  /* We create the list of maps. */
  maps = gtk_list_store_new(MAP_N_COLUMNS,
			    G_TYPE_OBJECT,
			    G_TYPE_ULONG,
			    G_TYPE_STRING,
                            G_TYPE_POINTER);

  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(panel), vbox);
  
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 3);
  label = gtk_label_new(_("<b>Map sources</b>"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_widget_set_name(label, "label_head");
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  warnLabel = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_end(GTK_BOX(hbox), warnLabel, FALSE, FALSE, 0);
  wd = gtk_image_new_from_stock(GTK_STOCK_DIALOG_WARNING,
				GTK_ICON_SIZE_BUTTON);
  gtk_box_pack_start(GTK_BOX(warnLabel), wd, FALSE, FALSE, 0);
  label = gtk_label_new(_("<span size=\"smaller\">missing elements</span>"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_box_pack_start(GTK_BOX(warnLabel), label, FALSE, FALSE, 0);
  
  
  /* The plane selector. */
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("Cutting plane:"));
  gtk_misc_set_padding(GTK_MISC(label), 5, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  label = gtk_label_new(_("<span size='small'>"
			  "<i>Create elements in the 'planes' tab</i></span>"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_misc_set_padding(GTK_MISC(label), 5, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);
  gtk_box_pack_end(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  
  align = gtk_alignment_new(0.5, 0.5, 1.0, 1.0);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 0, 0, 15, 15);
  gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, FALSE, 2);

  hbox = gtk_hbox_new(FALSE, 5);
  gtk_container_add(GTK_CONTAINER(align), hbox);
  list = visu_ui_panel_planes_getList();
  g_signal_connect(G_OBJECT(list), "row-deleted",
		   G_CALLBACK(onPlaneDeleted), (gpointer)0);
  combo = gtk_combo_box_new_with_model(GTK_TREE_MODEL(list));
  renderer = gtk_cell_renderer_pixbuf_new();
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(combo), renderer, FALSE);
  g_object_set(G_OBJECT(renderer), "xpad", 10, NULL);
  gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(combo), renderer,
				"pixbuf", VISU_UI_PANEL_PLANES_COLOR_PIXBUF);
  renderer = gtk_cell_renderer_text_new();
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(combo), renderer, FALSE);
  g_object_set(G_OBJECT(renderer), "xalign", 1.0, NULL);
  gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(combo), renderer,
				"markup", VISU_UI_PANEL_PLANES_LABEL);
  gtk_box_pack_start(GTK_BOX(hbox), combo, TRUE, TRUE, 0);
  comboVisuPlane = combo;
  g_signal_connect(G_OBJECT(combo), "changed",
		   G_CALLBACK(onComboVisuPlaneChanged), (gpointer)0);
  warnVisuPlane = gtk_image_new_from_stock(GTK_STOCK_DIALOG_WARNING,
				       GTK_ICON_SIZE_BUTTON);
  gtk_box_pack_end(GTK_BOX(hbox), warnVisuPlane, FALSE, FALSE, 0);

  /* The scalar field selector. */
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("Scalar field:"));
  gtk_misc_set_padding(GTK_MISC(label), 5, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  label = gtk_label_new(_("<span size='small'>"
			  "<i>Import fields in the 'isosurfaces' tab</i></span>"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_misc_set_padding(GTK_MISC(label), 5, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);
  gtk_box_pack_end(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  
  align = gtk_alignment_new(0.5, 0.5, 1.0, 1.0);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 0, 0, 15, 15);
  gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, FALSE, 2);

  hbox = gtk_hbox_new(FALSE, 5);
  gtk_container_add(GTK_CONTAINER(align), hbox);
  list = visu_ui_panel_surfaces_getFields();
  combo = gtk_combo_box_new_with_model(GTK_TREE_MODEL(list));
  renderer = gtk_cell_renderer_text_new();
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(combo), renderer, FALSE);
  g_object_set(G_OBJECT(renderer), "xalign", 1.0, NULL);
  gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(combo), renderer,
				"markup", VISU_UI_SURFACES_FIELD_LABEL);
  gtk_box_pack_start(GTK_BOX(hbox), combo, TRUE, TRUE, 0);
  comboField = combo;
  g_signal_connect(G_OBJECT(combo), "changed",
		   G_CALLBACK(onFieldChanged), (gpointer)0);
  warnField = gtk_image_new_from_stock(GTK_STOCK_DIALOG_WARNING,
				       GTK_ICON_SIZE_BUTTON);
  gtk_box_pack_end(GTK_BOX(hbox), warnField, FALSE, FALSE, 0);

  /* The shade selector. */
  wd = gtk_hbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(vbox), wd, FALSE, FALSE, 0);
  label = gtk_label_new(_("Shade:"));
  gtk_misc_set_padding(GTK_MISC(label), 5, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(wd), label, FALSE, FALSE, 0);
  ckAlpha = gtk_check_button_new_with_mnemonic(_("with _transparency"));
  gtk_box_pack_end(GTK_BOX(wd), ckAlpha, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(ckAlpha), "toggled",
		   G_CALLBACK(onAlphaChanged), (gpointer)0);
  
  align = gtk_alignment_new(0.5, 0.5, 1.0, 1.0);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 0, 0, 15, 15);
  gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, FALSE, 2);

  combo = visu_ui_shade_combobox_new(TRUE, TRUE);
  gtk_combo_box_set_active(GTK_COMBO_BOX(combo), -1);
  gtk_container_add(GTK_CONTAINER(align), combo);
  g_signal_connect(G_OBJECT(combo), "shade-selected",
		   G_CALLBACK(onToolShadeChanged), (gpointer)0);
  comboToolShade = combo;

  /* ToolOptions. */
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 3);

  label = gtk_label_new(_("<b>Options</b>"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_widget_set_name(label, "label_head");
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  
  label = gtk_label_new("%");
  gtk_misc_set_padding(GTK_MISC(label), 5, 0);
  gtk_box_pack_end(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  spinPrecision = gtk_spin_button_new_with_range(0, 200, 5);
  gtk_entry_set_width_chars(GTK_ENTRY(spinPrecision), 3);
  gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spinPrecision), 0);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinPrecision), 100);
  gtk_box_pack_end(GTK_BOX(hbox), spinPrecision, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(spinPrecision), "value-changed",
		   G_CALLBACK(onPrecisionChanged), (gpointer)0);
  label = gtk_label_new(_("Precision:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_box_pack_end(GTK_BOX(hbox), label, FALSE, FALSE, 5);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  label = gtk_label_new(_("Scale:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 5);

  rdLinear = gtk_radio_button_new_with_mnemonic((GSList*)0, _("_linear"));
  g_signal_connect(G_OBJECT(rdLinear), "toggled",
		   G_CALLBACK(onScaleChanged), GINT_TO_POINTER(TOOL_MATRIX_SCALING_LINEAR));
  gtk_box_pack_start(GTK_BOX(hbox), rdLinear, FALSE, FALSE, 0);
  rdLog = gtk_radio_button_new_with_mnemonic_from_widget(GTK_RADIO_BUTTON(rdLinear),
							 _("lo_g."));
  g_signal_connect(G_OBJECT(rdLog), "toggled",
		   G_CALLBACK(onScaleChanged), GINT_TO_POINTER(TOOL_MATRIX_SCALING_LOG));
  gtk_box_pack_start(GTK_BOX(hbox), rdLog, FALSE, FALSE, 0);
  rdZero = gtk_radio_button_new_with_mnemonic_from_widget(GTK_RADIO_BUTTON(rdLinear),
							  _("_zero centred log."));
  g_signal_connect(G_OBJECT(rdZero), "toggled",
		   G_CALLBACK(onScaleChanged), GINT_TO_POINTER(TOOL_MATRIX_SCALING_ZERO_CENTRED_LOG));
  gtk_box_pack_start(GTK_BOX(hbox), rdZero, FALSE, FALSE, 0);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  label = gtk_label_new(_("Number of isolines:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);
  spinIsoLines = gtk_spin_button_new_with_range(0, 20, 1);
  gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spinIsoLines), 0);
  gtk_entry_set_width_chars(GTK_ENTRY(spinIsoLines), 2);
  gtk_box_pack_start(GTK_BOX(hbox), spinIsoLines, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(spinIsoLines), "value-changed",
		   G_CALLBACK(onIsoChanged), (gpointer)0);

  align = gtk_alignment_new(1.0, 0.5, 0., 1.0);
  gtk_box_pack_start(GTK_BOX(hbox), align, TRUE, TRUE, 0);
  ckColour = gtk_check_button_new_with_mnemonic(_("_colour:"));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ckColour), TRUE);
  g_signal_connect(G_OBJECT(ckColour), "toggled",
		   G_CALLBACK(onUseColourChanged), (gpointer)0);
  gtk_container_add(GTK_CONTAINER(align), ckColour);

  cbColour = visu_ui_color_combobox_new(FALSE);
  visu_ui_color_combobox_setPrintValues(VISU_UI_COLOR_COMBOBOX(cbColour), FALSE);
  color = tool_color_addFloatRGBA(black, &pos);
  visu_ui_color_combobox_setSelection(VISU_UI_COLOR_COMBOBOX(cbColour), color);
  g_signal_connect(G_OBJECT(cbColour), "color-selected",
		   G_CALLBACK(onColorChange), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hbox), cbColour, FALSE, FALSE, 5);

  /* The normalisation. */
  hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  label = gtk_label_new(_("Normalise:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);

  radioNormalized = gtk_radio_button_new_with_mnemonic(NULL, _("auto"));
  gtk_box_pack_start(GTK_BOX(hbox), radioNormalized, FALSE, FALSE, 0);

  radioMinMax = gtk_radio_button_new_with_mnemonic_from_widget
    (GTK_RADIO_BUTTON(radioNormalized), _("manual"));
  gtk_box_pack_start(GTK_BOX(hbox), radioMinMax, FALSE, FALSE, 0);

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioNormalized), TRUE);
  g_signal_connect(G_OBJECT(radioMinMax), "toggled",
		   G_CALLBACK(onScaleTypeChange), GINT_TO_POINTER(FALSE));
  g_signal_connect(G_OBJECT(radioNormalized), "toggled",
		   G_CALLBACK(onScaleTypeChange), GINT_TO_POINTER(TRUE));

  hboxEntries = gtk_hbox_new (FALSE, 0);
  gtk_widget_set_sensitive(hboxEntries, FALSE);
  gtk_box_pack_end(GTK_BOX(hbox), hboxEntries, FALSE, FALSE, 0);

  label = gtk_label_new("[");
  gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);
  gtk_box_pack_start(GTK_BOX(hboxEntries), label, TRUE, TRUE, 0);
  entryDataMin = visu_ui_numerical_entry_new(-1.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryDataMin), 5);
  gtk_box_pack_start(GTK_BOX(hboxEntries), entryDataMin, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(entryDataMin), "value-changed",
		   G_CALLBACK(onEntryMinMaxChangeValue), GINT_TO_POINTER(0));

  label = gtk_label_new(";");
  gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);
  gtk_box_pack_start(GTK_BOX(hboxEntries), label, FALSE, FALSE, 0);
  entryDataMax = visu_ui_numerical_entry_new(1.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryDataMax), 5);
  gtk_box_pack_start(GTK_BOX(hboxEntries), entryDataMax, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(entryDataMax), "value-changed",
		   G_CALLBACK(onEntryMinMaxChangeValue), GINT_TO_POINTER(1));

  label = gtk_label_new("]");
  gtk_box_pack_start(GTK_BOX(hboxEntries), label, FALSE, FALSE, 0);

  /* The action buttons. */
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_end(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);

  wd = gtk_label_new(_("List of maps:"));
  gtk_box_pack_start(GTK_BOX(hbox), wd, FALSE, FALSE, 5);

  comboMap = gtk_combo_box_new_with_model(GTK_TREE_MODEL(maps));
  gtk_widget_set_sensitive(comboMap, FALSE);
  renderer = gtk_cell_renderer_text_new();
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(comboMap), renderer, FALSE);
  gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(comboMap), renderer,
				"markup", MAP_LABEL);
  gtk_box_pack_start(GTK_BOX(hbox), comboMap, TRUE, FALSE, 0);
  comboMap_signal = g_signal_connect(G_OBJECT(comboMap), "changed",
				     G_CALLBACK(onComboMapChanged), (gpointer)0);

  buildButton = gtk_button_new();
  wd = gtk_image_new_from_stock(GTK_STOCK_ADD, GTK_ICON_SIZE_MENU);
  gtk_container_add(GTK_CONTAINER(buildButton), wd);
  gtk_box_pack_end(GTK_BOX(hbox), buildButton, FALSE, FALSE, 2);
  g_signal_connect(G_OBJECT(buildButton), "clicked",
		   G_CALLBACK(onBuildClicked), (gpointer)0);

  removeButton = gtk_button_new();
  wd = gtk_image_new_from_stock(GTK_STOCK_REMOVE, GTK_ICON_SIZE_MENU);
  gtk_container_add(GTK_CONTAINER(removeButton), wd);
  gtk_widget_set_sensitive(removeButton, FALSE);
  gtk_box_pack_end(GTK_BOX(hbox), removeButton, FALSE, FALSE, 2);
  g_signal_connect(G_OBJECT(removeButton), "clicked",
		   G_CALLBACK(onRemoveClicked), (gpointer)0);

  exportButton = gtk_button_new();
  wd = gtk_image_new_from_stock(GTK_STOCK_SAVE_AS, GTK_ICON_SIZE_MENU);
  gtk_container_add(GTK_CONTAINER(exportButton), wd);
  gtk_widget_set_sensitive(exportButton, FALSE);
  gtk_box_pack_end(GTK_BOX(hbox), exportButton, FALSE, FALSE, 2);
  g_signal_connect(G_OBJECT(exportButton), "clicked",
		   G_CALLBACK(onExportClicked), (gpointer)0);

  gtk_widget_show_all(vbox);
  gtk_widget_hide(warnLabel);
  gtk_widget_hide(warnVisuPlane);
  gtk_widget_hide(warnField);

  isMapInitialised = TRUE;
}
/**
 * visu_ui_panel_map_update:
 * @map: (allow-none): a #VisuMap to update (parameters may have
 * changed).
 *
 * Rebuild @map depending on parameters selected in the interface. If
 * @map is NULL, rebuild all maps.
 *
 * Since: 3.7
 **/
void visu_ui_panel_map_update(VisuMap *map)
{
  setField(map);
  setCompute(map);
  setGlobalMinMax();
  setIsoLine(map);
  visu_gl_ext_maps_draw(extMaps);
  visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(extLegend));
}
static gboolean rebuildFromField(gpointer data _U_)
{
  visu_ui_panel_map_update((VisuMap*)0);
  return FALSE;
}
static VisuMap* addMap()
{
  GtkTreeIter iter;
  gboolean valid, add;
  VisuPlane *plane, *tmpVisuPlane;
  gulong moveId;
  gchar str[256];
  float vect[3], dist;
  ToolShade *shade;
  VisuScalarField *field;
  VisuMap *map;

  DBG_fprintf(stderr, "Panel Map: add a new map.\n");

  valid = getElements(&plane, &field, &shade);
  if (!valid)
    {
      gtk_widget_show(warnLabel);
      if (!plane)
	gtk_widget_show(warnVisuPlane);
      if (!field)
	gtk_widget_show(warnField);
      return (VisuMap*)0;
    }

  /* Check that the plane has not already been added. */
  add = TRUE;
  for (valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(maps), &iter);
       valid && add; valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(maps), &iter))
    {
      gtk_tree_model_get(GTK_TREE_MODEL(maps), &iter,
                         MAP_PLANE, &tmpVisuPlane, MAP_OBJ, &map, -1);
      g_object_unref(G_OBJECT(tmpVisuPlane));
      add = add && (tmpVisuPlane != plane);
    }
  if (!add)
    return map;

  /* We listen to the moved signal. */
  moveId = g_signal_connect_after(G_OBJECT(plane), "moved",
                                  G_CALLBACK(onPlaneMoved), (gpointer)0);

  map = visu_map_newFromPlane(plane);
  visu_plane_getNVectUser(plane, vect);
  dist = visu_plane_getDistanceFromOrigin(plane);
  sprintf(str, LABEL_PLANE, (int)vect[0], (int)vect[1], (int)vect[2], dist);
  /* We store this new entry in the map list. */
  gtk_list_store_append(maps, &iter);
  gtk_list_store_set(maps, &iter,
		     MAP_PLANE, plane,
		     MAP_LABEL, str,
		     MAP_MOVE_SIGNAL, moveId,
                     MAP_OBJ, map,
		     -1);
  /* Change the visibility status of the plane to FALSE. */
  visu_ui_panel_planes_setRendered(plane, FALSE);

  /* We update the interface. */
  DBG_fprintf(stderr, "Panel Map: update interface.\n");
  updateInterface(FALSE);
  gtk_combo_box_set_active_iter(GTK_COMBO_BOX(comboMap), &iter);

  DBG_fprintf(stderr, "Panel Map: setup new map.\n");
  g_idle_add(setupMap, (gpointer)map);

  return map;
}
static gboolean setupMap(gpointer data)
{
  VisuMap *map = (VisuMap*)data;
  ToolShade *shade;
  ToolColor *color;
  gboolean redraw;

  g_return_val_if_fail(map, FALSE);

  shade = visu_ui_shade_combobox_getSelection(VISU_UI_SHADE_COMBOBOX(comboToolShade));
  g_return_val_if_fail(shade, FALSE);
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ckColour)))
    color = visu_ui_color_combobox_getSelection(VISU_UI_COLOR_COMBOBOX(cbColour));
  else
    color = (ToolColor*)0;
  visu_gl_ext_maps_add(extMaps, map,
                       gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinPrecision)),
                       shade, color,
                       gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ckAlpha)));

  /* We set the map characteristics. */
  setField(map);
  setCompute(map);

  /* Map is computed, we add the lines. */
  redraw = setGlobalMinMax();
  setIsoLine((redraw)?(VisuMap*)0:map);

  visu_gl_ext_maps_draw(extMaps);
  visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(extLegend));

  return FALSE;
}
static void setField(VisuMap *map_)
{
  gboolean valid;
  GtkTreeIter iter;
  float manualMinMax[2], *minmax, *rgb, precision;
  double minMax[2];
  VisuScalarField *field;
  ToolMatrixScalingFlag scale;
  gboolean useMinMax;
  guint nLines;
  VisuMap *map;

  DBG_fprintf(stderr, "Panel Map: set field for map %p.\n", (gpointer)map_);

  g_return_if_fail(getElements((VisuPlane**)0, &field, (ToolShade**)0));
  getParameters(&scale, &rgb, manualMinMax, &useMinMax, &precision, &nLines);
  if (useMinMax)
    minmax = manualMinMax;
  else
    minmax = (float*)0;

  if (map_)
    visu_map_setField(map_, field, scale, minmax);
  else
    for (valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(maps), &iter);
         valid; valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(maps), &iter))
      {
        gtk_tree_model_get(GTK_TREE_MODEL(maps), &iter, MAP_OBJ, &map, -1);

        if (map)
          visu_map_setField(map, field, scale, minmax);
      }
  visu_gl_ext_maps_setDirty(extMaps, map_);

  /* We update the legend min and max. */
  if (useMinMax)
    visu_gl_ext_shade_setMinMax(extLegend, manualMinMax[0], manualMinMax[1]);
  else
    {
      visu_scalar_field_getMinMax(field, minMax);
      visu_gl_ext_shade_setMinMax(extLegend, minMax[0], minMax[1]);
    }
}
static void setCompute(VisuMap *map_)
{
  gboolean valid;
  GtkTreeIter iter;
  VisuMap *map;

  DBG_fprintf(stderr, "Panel Map: compute map %p.\n", (gpointer)map_);

  /* We compute the map, or all maps. */
  if (map_)
    visu_map_compute(map_);
  else
    for (valid = (map_)?TRUE:gtk_tree_model_get_iter_first(GTK_TREE_MODEL(maps), &iter);
         valid; valid = (map_)?FALSE:gtk_tree_model_iter_next(GTK_TREE_MODEL(maps), &iter))
    {
      gtk_tree_model_get(GTK_TREE_MODEL(maps), &iter, MAP_OBJ, &map, -1);
      if (map)
        visu_map_compute(map);
    }
}
static gboolean setGlobalMinMax()
{
  gboolean valid;
  GtkTreeIter iter;
  VisuMap *map;
  float *drawnMinMax_, oldMinMax[2];

  oldMinMax[0] = drawnMinMax[0];
  oldMinMax[1] = drawnMinMax[1];

  /* We update the drawnMinMax array. */
  drawnMinMax[0] = G_MAXFLOAT;
  drawnMinMax[1] = -G_MAXFLOAT;
  for (valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(maps), &iter);
       valid; valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(maps), &iter))
    {
      gtk_tree_model_get(GTK_TREE_MODEL(maps), &iter, MAP_OBJ, &map, -1);
      if (map)
        {
          drawnMinMax_ = visu_map_getScaledMinMax(map);

          drawnMinMax[0] = MIN(drawnMinMax[0], drawnMinMax_[0]);
          drawnMinMax[1] = MAX(drawnMinMax[1], drawnMinMax_[1]);
        }
    }
  DBG_fprintf(stderr, "Panel Map: global scaled min/max: %g/%g.\n",
              drawnMinMax[0], drawnMinMax[1]);

  return (oldMinMax[0] != drawnMinMax[0] || oldMinMax[1] != drawnMinMax[1]);
}
static gboolean setIsoLine(VisuMap *map_)
{
  gboolean valid;
  GtkTreeIter iter;
  VisuMap *map;
  guint i, nLines;
  float minmax[2];
  float *marks;

  nLines = (guint)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinIsoLines));
  DBG_fprintf(stderr, "Panel Map: set %d lines for map %p.\n", nLines, (gpointer)map_);

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioMinMax)))
    {
      minmax[0] = 0.;
      minmax[1] = 1.;
    }
  else
    {
      minmax[0] = drawnMinMax[0];
      minmax[1] = drawnMinMax[1];
    }

  /* Compute the isolines for one or each maps. */
  if (map_)
    visu_map_setLines(map_, nLines, minmax);
  else
    for (valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(maps), &iter);
         valid; valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(maps), &iter))
      {
        gtk_tree_model_get(GTK_TREE_MODEL(maps), &iter, MAP_OBJ, &map, -1);

        if (map)
          visu_map_setLines(map, nLines, minmax);
      }
  visu_gl_ext_maps_setDirty(extMaps, map_);

  /* Update the shade legend. */
  marks = g_malloc(sizeof(float) * (2 + nLines));
  for (i = 0; i < nLines; i++)
    marks[1 + i] = (minmax[1] - minmax[0]) * (float)(i + 1) /
      (float)(nLines + 1) + minmax[0];
  marks[0]          = drawnMinMax[0];
  marks[1 + nLines] = drawnMinMax[1];
  visu_gl_ext_shade_setMarks(extLegend, marks, 2 + nLines);
  g_free(marks);

  return TRUE;
}
/**
 * visu_ui_panel_map_setData:
 * @planeId: a entry for the plane combobox ;
 * @scalarFieldId: a entry for the scalar field combobox ;
 * @shadeId: a entry for the shade combobox.
 *
 * Change the values of the three combobox of this panel.
 *
 * Since: 3.4
 *
 * Returns: (transfer none): the associated #VisuMap.
 */
VisuMap* visu_ui_panel_map_setData(guint planeId, guint scalarFieldId, guint shadeId)
{
  if (!isMapInitialised)
    createGtkInterface(VISU_UI_PANEL(panelMap));

  visu_gl_ext_setActive(VISU_GL_EXT(extMaps), FALSE);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboVisuPlane), planeId);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboField), scalarFieldId);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboToolShade), shadeId);
  visu_gl_ext_setActive(VISU_GL_EXT(extMaps), TRUE);

  return addMap();
}

static void onBuildClicked(GtkButton *button _U_, gpointer data _U_)
{
  addMap();
  VISU_REDRAW_ADD;
}

static void onPrecisionChanged(GtkSpinButton *spin, gpointer data _U_)
{
  if (visu_gl_ext_maps_setPrecision(extMaps, (VisuMap*)0, gtk_spin_button_get_value(spin)))
    {
      visu_gl_ext_maps_draw(extMaps);
      VISU_REDRAW_ADD;
    }
}

static void onAlphaChanged(GtkToggleButton *button, gpointer data _U_)
{
  if (visu_gl_ext_maps_setTransparent(extMaps, (VisuMap*)0,
                                      gtk_toggle_button_get_active(button)))
    {
      visu_gl_ext_maps_draw(extMaps);
      VISU_REDRAW_ADD;
    }
}
static void onToolShadeChanged(VisuUiShadeCombobox *combo _U_, ToolShade *shade, gpointer data _U_)
{
  gtk_widget_hide(warnLabel);

  if (visu_gl_ext_maps_setShade(extMaps, (VisuMap*)0, shade))
    visu_gl_ext_maps_draw(extMaps);

  if (visu_gl_ext_shade_setShade(extLegend, shade) ||
      visu_gl_ext_frame_setGlView(VISU_GL_EXT_FRAME(extLegend),
                                  visu_ui_panel_getView(VISU_UI_PANEL(panelMap))))
    visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(extLegend));

  if (visu_gl_ext_getActive(VISU_GL_EXT(extMaps)) ||
      visu_gl_ext_getActive(VISU_GL_EXT(extLegend)))
    VISU_REDRAW_ADD;
}
static void onFieldChanged(GtkComboBox *combo _U_, gpointer data _U_)
{
  gtk_widget_hide(warnLabel);
  gtk_widget_hide(warnField);

  if (visu_gl_ext_getActive(VISU_GL_EXT(extMaps)) && checkAvailability())
    {
      g_idle_add(rebuildFromField, (gpointer)0);
      VISU_REDRAW_ADD;
    }
}
static void onComboVisuPlaneChanged(GtkComboBox *combo _U_, gpointer data _U_)
{
  gtk_widget_hide(warnLabel);
  gtk_widget_hide(warnVisuPlane);
}
static void onComboMapChanged(GtkComboBox *combo, gpointer data _U_)
{
  GtkTreeIter iter;
  gboolean valid;
  VisuPlane *plane, *tmpVisuPlane;
  GtkTreeModel *model;

  DBG_fprintf(stderr, "Panel Map: set map list to %d.\n",
	      gtk_combo_box_get_active(combo));
  valid = gtk_combo_box_get_active_iter(combo, &iter);
  g_return_if_fail(valid);

  gtk_tree_model_get(GTK_TREE_MODEL(maps), &iter,
		     MAP_PLANE, &plane,
		     -1);
  g_object_unref(G_OBJECT(plane));
  visu_gl_ext_setActive(VISU_GL_EXT(extMaps), FALSE);
  model = gtk_combo_box_get_model(GTK_COMBO_BOX(comboVisuPlane));
  for (valid = gtk_tree_model_get_iter_first(model, &iter);
       valid; valid = gtk_tree_model_iter_next(model, &iter))
    {
      gtk_tree_model_get(model, &iter, VISU_UI_PANEL_PLANES_POINTER, &tmpVisuPlane, -1);
      g_object_unref(G_OBJECT(tmpVisuPlane));
      if (plane == tmpVisuPlane)
	gtk_combo_box_set_active_iter(GTK_COMBO_BOX(comboVisuPlane), &iter);
    }
  visu_gl_ext_setActive(VISU_GL_EXT(extMaps), TRUE);
}

static void onPlaneMoved(VisuPlane *plane, gpointer data _U_)
{
  gboolean valid;
  GtkTreeIter iter;
  VisuPlane *tmpVisuPlane;
  gchar str[256];
  float vect[3], dist;
  
  if (visu_gl_ext_getActive(VISU_GL_EXT(extMaps)))
    {
      for (valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(maps), &iter);
	   valid; valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(maps), &iter))
	{
	  gtk_tree_model_get(GTK_TREE_MODEL(maps), &iter,
                             MAP_PLANE, &tmpVisuPlane, -1);
	  g_object_unref(G_OBJECT(tmpVisuPlane));
	  if (tmpVisuPlane == plane)
	    {
	      visu_plane_getNVectUser(plane, vect);
	      dist = visu_plane_getDistanceFromOrigin(plane);
              /* We update the label in the map list. */
	      sprintf(str, LABEL_PLANE, (int)vect[0],
		      (int)vect[1], (int)vect[2], dist);
	      gtk_list_store_set(maps, &iter, MAP_LABEL, str, -1);
	    }
	}
      setGlobalMinMax();
      setIsoLine((VisuMap*)0);
      visu_gl_ext_maps_draw(extMaps);
      visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(extLegend));
    }
}
static void onPlaneDeleted(GtkTreeModel *tree_model, GtkTreePath  *path _U_,
			   gpointer user_data _U_)
{
  gboolean valid, valid2, found;
  GtkTreeIter iterMap, iterVisuPlane;
  VisuPlane *plane, *tmpVisuPlane;

  DBG_fprintf(stderr, "Panel Map: plane deleted.\n");

  /* We try to find the plane in maps that is not existing anymore
     in planes. */
  for (valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(maps), &iterMap);
       valid; valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(maps), &iterMap))
    {
      gtk_tree_model_get(GTK_TREE_MODEL(maps), &iterMap, MAP_PLANE, &tmpVisuPlane, -1);
      g_object_unref(G_OBJECT(tmpVisuPlane));
      found = FALSE;
      for (valid2 = gtk_tree_model_get_iter_first(tree_model, &iterVisuPlane);
	   valid2 && !found;
	   valid2 = gtk_tree_model_iter_next(tree_model, &iterVisuPlane))
	{
	  gtk_tree_model_get(tree_model, &iterVisuPlane,
			     VISU_UI_PANEL_PLANES_POINTER, &plane, -1);
	  g_object_unref(G_OBJECT(plane));
	  
	  found = found || (plane == tmpVisuPlane);
	}
      if (!found)
	{
	  plane = removeMap(&iterMap);
	  updateInterface(TRUE);
          visu_gl_ext_maps_draw(extMaps);
	  break;
	}
    }
}

static void onRemoveClicked(GtkButton *button _U_, gpointer data _U_)
{
  gboolean valid;
  GtkTreeIter iter;
  VisuPlane *plane;

  /* We get the current map, if no, we do nothing. */
  valid = gtk_combo_box_get_active_iter(GTK_COMBO_BOX(comboMap), &iter);
  if (!valid)
    return;

  plane = removeMap(&iter);

  /* Return the visibility status of the plane to TRUE. */
  visu_ui_panel_planes_setRendered(plane, TRUE);

  updateInterface(TRUE);

  visu_gl_ext_maps_draw(extMaps);
  VISU_REDRAW_ADD;
}

static VisuPlane* removeMap(GtkTreeIter *iter)
{
  VisuPlane *plane;
  gulong moveId;
  VisuMap *map;

  /* We stop the listener on the move signal. */
  gtk_tree_model_get(GTK_TREE_MODEL(maps), iter,
		     MAP_MOVE_SIGNAL, &moveId, MAP_PLANE, &plane, MAP_OBJ, &map, -1);
  g_signal_handler_disconnect(G_OBJECT(plane), moveId);
  g_object_unref(G_OBJECT(plane));
  if (map)
    {
      visu_map_free(map);
      visu_gl_ext_maps_remove(extMaps, map);
    }

  /* We remove the entry from the liststore. */
  g_signal_handler_block(G_OBJECT(GTK_COMBO_BOX(comboMap)), comboMap_signal);
  gtk_list_store_remove(maps, iter);
  g_signal_handler_unblock(G_OBJECT(GTK_COMBO_BOX(comboMap)), comboMap_signal);

  if (map && setGlobalMinMax())
    setIsoLine((VisuMap*)0);

  return plane;
}

static void updateInterface(gboolean selectFirst)
{
  gboolean valid;

  /* Update the interface. */
  valid = (gtk_tree_model_iter_n_children(GTK_TREE_MODEL(maps), (GtkTreeIter*)0) > 0);
  DBG_fprintf(stderr, "Panel Map: status of maps %d.\n", valid);

  gtk_widget_set_sensitive(removeButton, valid);
  gtk_widget_set_sensitive(exportButton, valid);
  gtk_widget_set_sensitive(comboMap, valid);
  if (valid && selectFirst)
    gtk_combo_box_set_active(GTK_COMBO_BOX(comboMap), 0);

  visu_gl_ext_setActive(VISU_GL_EXT(extMaps), valid);
  visu_gl_ext_setActive(VISU_GL_EXT(extLegend), valid);
}

static void onScaleChanged(GtkToggleButton *button, gpointer data)
{
  
  if (!gtk_toggle_button_get_active(button))
    return;

  visu_gl_ext_shade_setScaling(extLegend, GPOINTER_TO_INT(data));
  if (visu_gl_ext_getActive(VISU_GL_EXT(extMaps)))
    {
      g_idle_add(rebuildFromField, (gpointer)0);
      VISU_REDRAW_ADD;
    }
}
static void onScaleTypeChange(GtkToggleButton *toggle, gpointer data)
{
  if (!gtk_toggle_button_get_active(toggle))
    return;

  gtk_widget_set_sensitive(hboxEntries, !GPOINTER_TO_INT(data));

  if (visu_gl_ext_getActive(VISU_GL_EXT(extMaps)))
    {
      g_idle_add(rebuildFromField, (gpointer)0);
      VISU_REDRAW_ADD;
    }
}

static void onUseColourChanged(GtkToggleButton *button, gpointer data _U_)
{
  ToolColor *color;

  if (gtk_toggle_button_get_active(button))
    color = visu_ui_color_combobox_getSelection(VISU_UI_COLOR_COMBOBOX(cbColour));
  else
    color = (ToolColor*)0;
  if (visu_gl_ext_maps_setLineColor(extMaps, (VisuMap*)0, color))
    {
      visu_gl_ext_maps_draw(extMaps);
      VISU_REDRAW_ADD;
    }
}
static void onColorChange(VisuUiColorCombobox *combo _U_, ToolColor *selectedColor,
			  gpointer data _U_)
{
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ckColour)) &&
      visu_gl_ext_maps_setLineColor(extMaps, (VisuMap*)0, selectedColor))
    {
      visu_gl_ext_maps_draw(extMaps);
      VISU_REDRAW_ADD;
    }
}

static void onIsoChanged(GtkSpinButton *button _U_, gpointer data _U_)
{
  if (visu_gl_ext_getActive(VISU_GL_EXT(extMaps)))
    {
      setIsoLine((VisuMap*)0);
      visu_gl_ext_maps_draw(extMaps);
      visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(extLegend));
    }
  if (visu_gl_ext_getActive(VISU_GL_EXT(extLegend)) ||
      visu_gl_ext_getActive(VISU_GL_EXT(extMaps)))
    VISU_REDRAW_ADD;
}
static void onEntryMinMaxChangeValue(VisuUiNumericalEntry *entry _U_,
				     double oldValue _U_, gpointer data _U_)
{
  if (visu_gl_ext_getActive(VISU_GL_EXT(extMaps)))
    {
      g_idle_add(rebuildFromField, (gpointer)0);
      VISU_REDRAW_ADD;
    }
}

static gboolean getElements(VisuPlane **plane, VisuScalarField **field, ToolShade **shade)
{
  GtkTreeModel *model;
  GtkTreeIter iter;
  gboolean valid, out;

  if (plane)
    *plane = (VisuPlane*)0;
  if (field)
    *field = (VisuScalarField*)0;
  if (shade)
    *shade = (ToolShade*)0;
  out = TRUE;

  if (field)
    {
      model = gtk_combo_box_get_model(GTK_COMBO_BOX(comboField));
      valid = gtk_combo_box_get_active_iter(GTK_COMBO_BOX(comboField), &iter);
      if (!model || !valid)
        out = FALSE;
      else
        gtk_tree_model_get(model, &iter, VISU_UI_SURFACES_FIELD_POINTER, field, -1);
    }

  if (shade)
    {
      *shade = visu_ui_shade_combobox_getSelection(VISU_UI_SHADE_COMBOBOX(comboToolShade));
      if (!*shade)
        out = FALSE;
    }

  if (plane)
    {
      /* We get the plane to listen to its signals. */
      model = gtk_combo_box_get_model(GTK_COMBO_BOX(comboVisuPlane));
      valid = gtk_combo_box_get_active_iter(GTK_COMBO_BOX(comboVisuPlane), &iter);
      if (!model || !valid)
	out = FALSE;
      else
	{
	  gtk_tree_model_get(model, &iter, VISU_UI_PANEL_PLANES_POINTER, plane, -1);
	  g_object_unref(G_OBJECT(*plane));
	}
    }

  return out;
}
static void getParameters(ToolMatrixScalingFlag *scale, float **rgb,
			  float manualMinMax[2], gboolean *useMinMax,
			  float *precision, guint *nLines)
{
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rdLinear)))
    *scale = TOOL_MATRIX_SCALING_LINEAR;
  else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(rdLog)))
    *scale = TOOL_MATRIX_SCALING_LOG;
  else
    *scale = TOOL_MATRIX_SCALING_ZERO_CENTRED_LOG;

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ckColour)))
    *rgb = visu_ui_color_combobox_getSelection(VISU_UI_COLOR_COMBOBOX(cbColour))->rgba;
  else
    *rgb = (float*)0;

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioNormalized)))
    *useMinMax = FALSE;
  else
    {
      manualMinMax[0] = visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryDataMin));
      manualMinMax[1] = visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryDataMax));
      DBG_fprintf(stderr, "Panel Map: get min/max: %g/%g.\n",
		  manualMinMax[0], manualMinMax[1]);
      *useMinMax = TRUE;
    }

  *precision = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinPrecision));
  *nLines = (guint)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinIsoLines));
}

static void onExportClicked(GtkButton *button _U_, gpointer data _U_)
{
  GtkWidget *dialog;
  gchar *name, *filename, *directory;
  GtkFileFilter *filterPDF, *filterSVG, *filter;
  VisuData *dataObj;
  VisuMapExportFormat format;
  gboolean valid;
  ToolShade *shade;
  float *rgb, precision;
  GError *error;
  GtkTreeIter iter;
  VisuMap *map;

  dialog = gtk_file_chooser_dialog_new
    (_("Export to SVG or PDF."),
     visu_ui_panel_getContainerWindow(VISU_UI_PANEL(panelMap)),
     GTK_FILE_CHOOSER_ACTION_SAVE,
     GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
     GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
     NULL);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 7
  gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER (dialog), TRUE);
#endif

  filterPDF = filter = gtk_file_filter_new();
  gtk_file_filter_set_name(filter, _("PDF document (*.pdf)"));
  gtk_file_filter_add_pattern(filter, "*.pdf");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);
  filterSVG = filter = gtk_file_filter_new();
  gtk_file_filter_set_name(filter, _("SVG document (*.svg)"));
  gtk_file_filter_add_pattern(filter, "*.svg");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);

  /* We set the suggested filename, either a previous one
     or the one given. */
  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelMap));
  directory = visu_ui_getLastOpenDirectory();
  if (directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog), directory);
  filename = (gchar*)0;
  if (dataObj)
    filename = g_object_get_data(G_OBJECT(dataObj), "exportMap_filename");
  if (!filename)
    filename = _("map.pdf");
  gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), filename);

  if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT)
    {
      filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
      name = g_path_get_basename(filename);
      g_object_set_data_full(G_OBJECT(dataObj), "exportMap_filename",
			     (gpointer)name, g_free);
      
      /* We detect the format. */
      filter = gtk_file_chooser_get_filter(GTK_FILE_CHOOSER(dialog));
      if (filter == filterPDF)
	format = VISU_MAP_EXPORT_PDF;
      else if (filter == filterSVG)
	format = VISU_MAP_EXPORT_SVG;
      else
	format = VISU_MAP_EXPORT_PDF;
      /* We get the data. */
      valid = gtk_combo_box_get_active_iter(GTK_COMBO_BOX(comboMap), &iter);
      g_return_if_fail(valid);
      gtk_tree_model_get(GTK_TREE_MODEL(maps), &iter, MAP_OBJ, &map, -1);
      shade = visu_ui_shade_combobox_getSelection(VISU_UI_SHADE_COMBOBOX(comboToolShade));
      rgb = (float*)0;
      if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ckColour)))
        rgb = visu_ui_color_combobox_getSelection(VISU_UI_COLOR_COMBOBOX(cbColour))->rgba;
      precision = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinPrecision));
      g_return_if_fail(map && shade);
      /* We export. */
      error = (GError*)0;
      if (!visu_map_export(map, shade, rgb, precision, filename, format, &error) && error)
	{
	  visu_ui_raiseWarning(_("Export a coloured map"),
			       error->message,
			       GTK_WINDOW(dialog));
	  g_error_free(error);
	}
      g_free(filename);
    }
  gtk_widget_destroy(dialog);
}
/**
 * visu_ui_panel_map_setScale:
 * @scale: the status.
 *
 * Set the scaling method.
 *
 * Since: 3.4
 */
void visu_ui_panel_map_setScale(ToolMatrixScalingFlag scale)
{
  if (!isMapInitialised)
    createGtkInterface(VISU_UI_PANEL(panelMap));

  if (scale == TOOL_MATRIX_SCALING_LINEAR)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rdLinear), TRUE);
  else if (scale == TOOL_MATRIX_SCALING_LOG)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rdLog), TRUE);
  else if (scale == TOOL_MATRIX_SCALING_ZERO_CENTRED_LOG)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rdZero), TRUE);
  else
    g_warning("Wrong scale argument");
}
/**
 * visu_ui_panel_map_setNIsolines:
 * @nIsoLines: a value.
 *
 * Change the number of drawn isolines.
 *
 * Since: 3.4
 */
void visu_ui_panel_map_setNIsolines(guint nIsoLines)
{
  if (!isMapInitialised)
    createGtkInterface(VISU_UI_PANEL(panelMap));

  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinIsoLines), nIsoLines);
}
/**
 * visu_ui_panel_map_setIsolinesColor:
 * @color: an RGB value (can be NULL).
 *
 * Change the colour of drawn isolines. If @color is NULL, then the
 * specific colour is unchecked and inversed colours are used.
 *
 * Since: 3.5
 */
void visu_ui_panel_map_setIsolinesColor(float *color)
{
  ToolColor *cl;
  float rgba[4];
  int position;

  if (!isMapInitialised)
    createGtkInterface(VISU_UI_PANEL(panelMap));

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ckColour), (color != (float*)0));
  if (color)
    {
      rgba[0] = color[0];
      rgba[1] = color[1];
      rgba[2] = color[2];
      rgba[3] = 1.f;
      cl = tool_color_addFloatRGBA(rgba, &position);
      visu_ui_color_combobox_setSelection(VISU_UI_COLOR_COMBOBOX(cbColour), cl);
    }
}
/**
 * visu_ui_panel_map_setPrecision:
 * @prec: a pourcentage.
 *
 * Set the precision used to render the maps. At 200, there is no
 * adaptive mesh.
 *
 * Since: 3.6
 */
void visu_ui_panel_map_setPrecision(guint prec)
{
  if (!isMapInitialised)
    createGtkInterface(VISU_UI_PANEL(panelMap));

  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinPrecision), prec);
}
/**
 * visu_ui_panel_map_setMinMax:
 * @minMax: two floats containing min and max values. Can be NULL.
 *
 * Set-up the  minimum and maximum scaling values. In case @minMax is
 * NULL, then the automatic normalisation is chosen.
 *
 * Since: 3.6
 */
void visu_ui_panel_map_setMinMax(float *minMax)
{
  if (!isMapInitialised)
    createGtkInterface(VISU_UI_PANEL(panelMap));

  if (minMax)
    {
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryDataMin), minMax[0]);
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryDataMax), minMax[1]);
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioMinMax), TRUE);
    }
  else
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioNormalized), TRUE);
}

static gboolean checkAvailability()
{
  gboolean valid;
  GtkTreeIter iterMap;

  /* Delete maps if necessary. */
  if (gtk_combo_box_get_active(GTK_COMBO_BOX(comboVisuPlane)) < 0 ||
      gtk_combo_box_get_active(GTK_COMBO_BOX(comboField)) < 0)
    {
      for (valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(maps), &iterMap);
	   valid;
	   valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(maps), &iterMap))
	removeMap(&iterMap);
      visu_gl_ext_setActive(VISU_GL_EXT(extMaps), FALSE);
      visu_gl_ext_setActive(VISU_GL_EXT(extLegend), FALSE);
      return FALSE;
    }
  return TRUE;
}

static void onMapEnter(VisuUiPanel *map, gpointer data _U_)
{
  DBG_fprintf(stderr, "Panel Map: caught the 'page-entered' signal %d.\n",
	      isMapInitialised);
  if (!isMapInitialised)
    createGtkInterface(map);

  if (gtk_tree_model_iter_n_children
      (gtk_combo_box_get_model(GTK_COMBO_BOX(comboVisuPlane)), (GtkTreeIter*)0) > 0 &&
      gtk_combo_box_get_active(GTK_COMBO_BOX(comboVisuPlane)) < 0)
    gtk_combo_box_set_active(GTK_COMBO_BOX(comboVisuPlane), 0);
  if (gtk_tree_model_iter_n_children
      (gtk_combo_box_get_model(GTK_COMBO_BOX(comboField)), (GtkTreeIter*)0) > 0 &&
      gtk_combo_box_get_active(GTK_COMBO_BOX(comboField)) < 0)
    gtk_combo_box_set_active(GTK_COMBO_BOX(comboField), 0);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboToolShade), 0);

  /* Delete maps if necessary. */
  if (!checkAvailability())
    updateInterface(FALSE);
}
static void onViewAvail(VisuObject *obj _U_, VisuData *dataObj _U_,
                        VisuGlView *view, gpointer data _U_)
{
  DBG_fprintf(stderr, "Panel Maps: caught 'dataRendered' signal, attaching default axes.\n");

  visu_gl_ext_maps_setGlView(extMaps, view);
  
  visu_gl_ext_setActive(VISU_GL_EXT(extMaps), FALSE);
  visu_gl_ext_setActive(VISU_GL_EXT(extLegend), FALSE);
}
/**
 * visu_ui_panel_map_init: (skip)
 *
 * Should be used in the list declared in externalModules.h to be loaded by
 * V_Sim on start-up. This routine will create the #VisuUiPanel where
 * the coloured map stuff can be done, such as choosing a plane,
 * associating a scalar field...
 *
 * Returns: a newly created #VisuUiPanel object.
 */
VisuUiPanel* visu_ui_panel_map_init(VisuUiMain *ui _U_)
{
  /* Long description */
  char *cl = _("Map projections");
  /* Short description */
  char *tl = _("Maps");

  panelMap = visu_ui_panel_newWithIconFromPath("Panel_map", cl, tl, "stock-map_20.png");
  g_return_val_if_fail(panelMap, (VisuUiPanel*)0);

  isMapInitialised = FALSE;
  g_signal_connect(G_OBJECT(panelMap), "page-entered",
		   G_CALLBACK(onMapEnter), (gpointer)0);

  visu_ui_panel_setDockable(VISU_UI_PANEL(panelMap), TRUE);

  /* Extension to draw values. */
  extMaps = visu_gl_ext_maps_new(NULL);
  visu_gl_ext_setActive(VISU_GL_EXT(extMaps), FALSE);

  /* Extension to draw a legend. */
  extLegend = visu_gl_ext_shade_new("Map legend");

  g_signal_connect(VISU_OBJECT_INSTANCE, "dataRendered",
		   G_CALLBACK(onViewAvail), (gpointer)0);

  return VISU_UI_PANEL(panelMap);
}
