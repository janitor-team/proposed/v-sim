/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "panelFogBgColor.h"

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <support.h>
#include <gtk_main.h>
#include <visu_tools.h>
#include <visu_object.h>
#include <visu_gtk.h>
#include <extensions/fogAndBGColor.h>
#include <coreTools/toolColor.h>

/**
 * SECTION:panelFogBgColor
 * @short_description: The widget to tune the background and the fog.
 *
 * <para>This is the user interface for fog and the background. The
 * background can be either uniform or use a bitmap picture. The fog
 * can be of different colours, custom or background.</para>
 */

/* Sensitive widget in this subpanel. */
static GtkWidget *checkFogIsOn;
static GtkWidget *rangeFogStart, *rangeFogEnd;
static GtkWidget *radioBgFog, *radioOtherFog;
static GtkWidget *rgbBgColor[4];
static GtkWidget *rgbFogColor[3];
static GtkWidget *bgImageWd, *checkFollowZoom;
static gulong trans_signal, gross_signal;

/* Private functions. */
GtkWidget *createInteriorFogBgColor();
void createCallBacksFogBgColor();
static gboolean loadBgFile(gpointer data);

/* Local callbacks. */
static void fogCheckChanged(GtkToggleButton *button, gpointer data);
static void changedRGBBgColor(GtkRange *rg, gpointer data);
static void radioBgChanged(GtkToggleButton *button, gpointer data);
static void changedRGBFogColor(GtkRange *rg, gpointer data);
static void changedFogStart(GtkRange *rg, gpointer data);
static void changedFogEnd(GtkRange *rg, gpointer data);
static void onFogBgEnter(VisuUiPanel *visu_ui_panel, gpointer data);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
static void onBgImageSet(GtkFileChooserButton *filechooserbutton, gpointer data);
#else
static void onBgImageSet(GtkDialog *dialog, gint response, gpointer data);
#endif
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 6
static void onBgImageChoose(GtkButton *bt, gpointer data);
#endif
static void onBgImageUnset(GtkButton *bt, gpointer data);
static void onBgImageFollow(GtkToggleButton *bt, gpointer data);
static void onDataReady(VisuObject *obj, VisuData *dataObj,
                        VisuGlView *view, gpointer data);
static void onDataNotReady(VisuObject *obj, VisuData *dataObj,
                           VisuGlView *view, gpointer data);
static void onCameraChange(VisuGlView *view, gpointer data);

static GtkWidget *panelFogBgColor;
static int disableCallbacksFogBgColor;

VisuUiPanel* visu_ui_panel_bg_init(VisuUiMain *ui _U_)
{
  char *cl = _("Fog and background color");
  char *tl = _("Fog & bg");

  panelFogBgColor = visu_ui_panel_newWithIconFromPath("Panel_fog_and_bg_color", cl, tl,
						  "stock-fog_20.png");
  if (!panelFogBgColor)
    return (VisuUiPanel*)0;
  gtk_container_add(GTK_CONTAINER(panelFogBgColor), createInteriorFogBgColor());
  visu_ui_panel_setDockable(VISU_UI_PANEL(panelFogBgColor), TRUE);

  /* Create the callbacks of all the sensitive widgets. */
  createCallBacksFogBgColor();
  g_signal_connect(G_OBJECT(panelFogBgColor), "page-entered",
		   G_CALLBACK(onFogBgEnter), (gpointer)0);

  g_signal_connect(VISU_OBJECT_INSTANCE, "dataRendered",
                   G_CALLBACK(onDataReady), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataUnRendered",
                   G_CALLBACK(onDataNotReady), (gpointer)0);

  /* Private parameters. */
  disableCallbacksFogBgColor = 0;

  return VISU_UI_PANEL(panelFogBgColor);
}
static void onDataReady(VisuObject *visu _U_, VisuData *dataObj,
                        VisuGlView *view, gpointer data _U_)
{
  if (view && dataObj)
    {
      visu_gl_ext_bg_setGlView(visu_gl_ext_bg_getDefault(), view);
      trans_signal =
        g_signal_connect(G_OBJECT(view), "XsYsChanged",
                         G_CALLBACK(onCameraChange), (gpointer)0);
      gross_signal =
        g_signal_connect(G_OBJECT(view), "GrossChanged",
                         G_CALLBACK(onCameraChange), (gpointer)0);
    }
  else
    visu_gl_ext_bg_setGlView(visu_gl_ext_bg_getDefault(), (VisuGlView*)0);
}
static void onDataNotReady(VisuObject *visu _U_, VisuData *dataObj _U_,
                           VisuGlView *view, gpointer data _U_)
{
  g_signal_handler_disconnect(G_OBJECT(view), trans_signal);
  g_signal_handler_disconnect(G_OBJECT(view), gross_signal);
}

static void update_preview_cb(GtkFileChooser *file_chooser, gpointer data)
{
  GtkWidget *preview;
  char *filename;
  GdkPixbuf *pixbuf;
  gboolean have_preview;

  preview = GTK_WIDGET (data);
  filename = gtk_file_chooser_get_preview_filename (file_chooser);

  if (filename)
    pixbuf = gdk_pixbuf_new_from_file_at_size (filename, 128, 128, NULL);
  else
    pixbuf = (GdkPixbuf*)0;
  have_preview = (pixbuf != NULL);
  g_free (filename);

  gtk_image_set_from_pixbuf (GTK_IMAGE (preview), pixbuf);
  if (pixbuf)
    g_object_unref (pixbuf);

  gtk_file_chooser_set_preview_widget_active(file_chooser, have_preview);
}

GtkWidget *createInteriorFogBgColor()
{
  GtkWidget *vbox, *hbox;
  GtkWidget *label;
  GtkWidget *table;
#define RED_LABEL   _("R:")
#define GREEN_LABEL _("G:")
#define BLUE_LABEL  _("B:")
#define ALPHA_LABEL  _("A:")
  char *rgb[4];
  char *rgbName[4] = {"scroll_r", "scroll_g", "scroll_b", "scroll_a"};
  int i;
  GtkWidget *dialog, *bt, *preview;
  GtkFileFilter *filters;
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;

  tooltips = gtk_tooltips_new ();
#endif

  rgb[0] = RED_LABEL;
  rgb[1] = GREEN_LABEL;
  rgb[2] = BLUE_LABEL;
  rgb[3] = ALPHA_LABEL;

  vbox = gtk_vbox_new(FALSE, 0);

  table = gtk_table_new(5, 2, FALSE);
  gtk_box_pack_start(GTK_BOX(vbox), table, FALSE, FALSE, 5);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_table_attach(GTK_TABLE(table), hbox, 0, 2, 0, 1,
		   GTK_EXPAND | GTK_FILL, GTK_SHRINK, 0, 2);
  label = gtk_label_new(_("Background:"));
  gtk_widget_set_name(label, "label_head");
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 2);
  for (i = 0; i < 4; i++)
    {
      label = gtk_label_new(rgb[i]);
      gtk_table_attach(GTK_TABLE(table), label, 0, 1, i + 1, i + 2,
		       GTK_SHRINK, GTK_SHRINK, 5, 0);
      rgbBgColor[i] = gtk_hscale_new_with_range(0., 1., 0.001);
      gtk_scale_set_value_pos(GTK_SCALE(rgbBgColor[i]),
			      GTK_POS_RIGHT);
      gtk_widget_set_name(rgbBgColor[i], rgbName[i]);
      gtk_table_attach(GTK_TABLE(table), rgbBgColor[i],
		       1, 2, i + 1, i + 2,
		       GTK_EXPAND | GTK_FILL, GTK_SHRINK, 5, 0);
    }
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("Insert an image:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);
  dialog = gtk_file_chooser_dialog_new(_("Choose a background image"),
				       (GtkWindow*)0,
				       GTK_FILE_CHOOSER_ACTION_OPEN,
				       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				       GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
				       NULL);
  filters = gtk_file_filter_new();
  gtk_file_filter_add_pixbuf_formats(filters);
  gtk_file_chooser_set_filter(GTK_FILE_CHOOSER(dialog), filters);
  preview = gtk_image_new();
  gtk_file_chooser_set_preview_widget(GTK_FILE_CHOOSER(dialog), preview);
  gtk_file_chooser_set_preview_widget_active(GTK_FILE_CHOOSER(dialog), FALSE);
  g_signal_connect(GTK_FILE_CHOOSER(dialog), "update-preview",
		   G_CALLBACK(update_preview_cb), preview);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  bgImageWd = gtk_file_chooser_button_new_with_dialog(dialog);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
  g_signal_connect(G_OBJECT(bgImageWd), "file-set",
		   G_CALLBACK(onBgImageSet), (gpointer)0);
#else
  g_signal_connect(G_OBJECT(dialog), "response",
		   G_CALLBACK(onBgImageSet), (gpointer)bgImageWd);
#endif
#else
  bgImageWd = gtk_button_new_from_stock(GTK_STOCK_OPEN);
  g_signal_connect(G_OBJECT(dialog), "clicked",
		   G_CALLBACK(onBgImageChoose), (gpointer)dialog);
#endif
  gtk_box_pack_start(GTK_BOX(hbox), bgImageWd, TRUE, TRUE, 0);
  bt = gtk_button_new();
  gtk_widget_set_tooltip_text(bt, _("Remove the background image."));
  gtk_container_add(GTK_CONTAINER(bt),
		    gtk_image_new_from_stock(GTK_STOCK_CLEAR, GTK_ICON_SIZE_MENU));
  gtk_box_pack_start(GTK_BOX(hbox), bt, FALSE, FALSE, 5);
  g_signal_connect(G_OBJECT(bt), "clicked",
		   G_CALLBACK(onBgImageUnset), (gpointer)bgImageWd);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  checkFollowZoom = gtk_check_button_new_with_mnemonic(_("Image size & position _follow the camera"));
  g_signal_connect(G_OBJECT(checkFollowZoom), "toggled",
		   G_CALLBACK(onBgImageFollow), (gpointer)0);
  gtk_box_pack_end(GTK_BOX(hbox), checkFollowZoom, FALSE, FALSE, 0);

  hbox = gtk_hbox_new(FALSE, 0);
  label = gtk_label_new(_("Use fog:"));
  gtk_widget_set_name(label, "label_head");
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 2);
  checkFogIsOn = gtk_check_button_new();
  gtk_box_pack_start(GTK_BOX(hbox), checkFogIsOn, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);
  
  table = gtk_table_new(2, 2, FALSE);
  gtk_box_pack_start(GTK_BOX(vbox), table, FALSE, FALSE, 5);
  label = gtk_label_new(_("Start:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1,
		   GTK_SHRINK, GTK_SHRINK, 5, 0);
  rangeFogStart = gtk_hscale_new_with_range(0., 100., 0.1);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
  gtk_range_set_restrict_to_fill_level(GTK_RANGE(rangeFogStart), TRUE);
  gtk_range_set_fill_level(GTK_RANGE(rangeFogStart), 50.);
  gtk_range_set_show_fill_level(GTK_RANGE(rangeFogStart), TRUE);
#endif
  gtk_scale_set_value_pos(GTK_SCALE(rangeFogStart),
			  GTK_POS_RIGHT);
  gtk_table_attach(GTK_TABLE(table), rangeFogStart,
		   1, 2, 0, 1,
		   GTK_EXPAND | GTK_FILL, GTK_SHRINK, 5, 0);
  label = gtk_label_new(_("End:"));
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2,
		   GTK_SHRINK, GTK_SHRINK, 5, 0);
  rangeFogEnd = gtk_hscale_new_with_range(0., 100., 0.1);
  gtk_scale_set_value_pos(GTK_SCALE(rangeFogEnd),
			  GTK_POS_RIGHT);
  gtk_table_attach(GTK_TABLE(table), rangeFogEnd,
		   1, 2, 1, 2,
		   GTK_EXPAND | GTK_FILL, GTK_SHRINK, 5, 0);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);
  label = gtk_label_new(_("Color:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 2);
  radioBgFog = gtk_radio_button_new_with_label(NULL, _("background color"));
  gtk_box_pack_start(GTK_BOX(hbox), radioBgFog, FALSE, FALSE, 2);
  radioOtherFog =
    gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(radioBgFog),
						_("specific color"));
  gtk_box_pack_start(GTK_BOX(hbox), radioOtherFog, FALSE, FALSE, 2);

  table = gtk_table_new(5, 2, FALSE);
  gtk_box_pack_start(GTK_BOX(vbox), table, FALSE, FALSE, 5);
  for (i = 0; i < 3; i++)
    {
      label = gtk_label_new(rgb[i]);
      gtk_table_attach(GTK_TABLE(table), label, 0, 1, i + 1, i + 2,
		       GTK_SHRINK, GTK_SHRINK, 5, 0);
      rgbFogColor[i] = gtk_hscale_new_with_range(0., 1., 0.001);
      gtk_scale_set_value_pos(GTK_SCALE(rgbFogColor[i]),
			      GTK_POS_RIGHT);
      gtk_widget_set_sensitive(rgbFogColor[i], FALSE);
      gtk_widget_set_name(rgbFogColor[i], rgbName[i]);
      gtk_table_attach(GTK_TABLE(table), rgbFogColor[i],
		       1, 2, i + 1, i + 2,
		       GTK_EXPAND | GTK_FILL, GTK_SHRINK, 5, 0);
    }

  gtk_widget_show_all(vbox);

  return vbox;
}

void createCallBacksFogBgColor()
{
  int i;

  g_signal_connect((gpointer)checkFogIsOn, "toggled",
		   G_CALLBACK(fogCheckChanged), (gpointer)0);
  for (i = 0; i < 4; i++)
    g_signal_connect((gpointer)rgbBgColor[i], "value-changed",
		     G_CALLBACK(changedRGBBgColor), GINT_TO_POINTER(i));
  g_signal_connect((gpointer)radioBgFog, "toggled",
		   G_CALLBACK(radioBgChanged), (gpointer)0);
  for (i = 0; i < 3; i++)
    g_signal_connect((gpointer)rgbFogColor[i], "value-changed",
		     G_CALLBACK(changedRGBFogColor), GINT_TO_POINTER(i));
  g_signal_connect((gpointer)rangeFogStart, "value-changed",
		   G_CALLBACK(changedFogStart), (gpointer)rangeFogEnd);
  g_signal_connect((gpointer)rangeFogEnd, "value-changed",
		   G_CALLBACK(changedFogEnd), (gpointer)rangeFogStart);
}

/*************/
/* Callbacks */
/*************/
static void fogRedraw()
{
  VisuData *dataObj;

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelFogBgColor));
  if (!dataObj) 
    return;
  visu_gl_ext_fog_create(visu_ui_panel_getView(VISU_UI_PANEL(panelFogBgColor)),
                         visu_boxed_getBox(VISU_BOXED(dataObj)));
  VISU_REDRAW_ADD;
}
static void fogCheckChanged(GtkToggleButton *button, gpointer data _U_)
{
  if (disableCallbacksFogBgColor)
    return;

  visu_gl_ext_fog_setOn(gtk_toggle_button_get_active(button));
  fogRedraw();
}
static void changedFogStart(GtkRange *rg, gpointer data _U_)
{
  float val[2];

  if (disableCallbacksFogBgColor)
    return;

  val[0] = (float)gtk_range_get_value(rg);
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 11
  if (val[0] > gtk_range_get_value(GTK_RANGE(data)))
    {
      gtk_range_set_value(rg, gtk_range_get_value(GTK_RANGE(data)));
      val[0] = (float)gtk_range_get_value(rg);
    }
#endif
  val[0]= val[0] / 100.;
  
  if (visu_gl_ext_fog_setStartEndValues(val, VISU_GL_EXT_FOG_MASK_START))
    fogRedraw();
}
static void changedFogEnd(GtkRange *rg, gpointer data)
{
  float val[2];

  val[1] = (float)gtk_range_get_value(rg);
  if (val[1] < gtk_range_get_value(GTK_RANGE(data)))
    {
      gtk_range_set_value(rg, gtk_range_get_value(GTK_RANGE(data)));
      val[1] = (float)gtk_range_get_value(rg);
    }
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
  gtk_range_set_fill_level(GTK_RANGE(rangeFogStart), val[1]);
#endif
  val[1]= val[1] / 100.;
  
  if (disableCallbacksFogBgColor)
    return;

  if (visu_gl_ext_fog_setStartEndValues(val, VISU_GL_EXT_FOG_MASK_END))
    fogRedraw();
}
static void radioBgChanged(GtkToggleButton *button, gpointer data _U_)
{
  int i;

  if (disableCallbacksFogBgColor)
    return;

  for (i = 0; i < 3; i++)
    gtk_widget_set_sensitive(rgbFogColor[i], !gtk_toggle_button_get_active(button));
  if (visu_gl_ext_fog_setUseSpecificColor(!gtk_toggle_button_get_active(button)))
    VISU_REDRAW_ADD;
}
static void changedRGBFogColor(GtkRange *rg, gpointer data)
{
  GtkAdjustment *adj;
  int rgbMask[4] = {TOOL_COLOR_MASK_R, TOOL_COLOR_MASK_G, TOOL_COLOR_MASK_B, TOOL_COLOR_MASK_A};
  float rgb[4];

  if (disableCallbacksFogBgColor)
    return;

  g_return_if_fail(GPOINTER_TO_INT(data) >= 0 && GPOINTER_TO_INT(data) < 4);

  adj = gtk_range_get_adjustment(rg);
  rgb[GPOINTER_TO_INT(data)] = (float)gtk_adjustment_get_value(adj);
  
  if (visu_gl_ext_fog_setValues(rgb, rgbMask[GPOINTER_TO_INT(data)]))
    VISU_REDRAW_ADD;
}
static void onFogBgEnter(VisuUiPanel *visu_ui_panel _U_, gpointer data _U_)
{
  int i;
  float rgb[4];

  disableCallbacksFogBgColor = 1;
  DBG_fprintf(stderr, "Panel Fog & bg : refresh data on enter.\n");
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkFogIsOn),
			       visu_gl_ext_fog_getOn());
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioOtherFog),
			       visu_gl_ext_fog_getUseSpecificColor());
  visu_gl_ext_bg_getRGBA(visu_gl_ext_bg_getDefault(), rgb);
  for (i = 0; i < 4; i++)
    gtk_range_set_value(GTK_RANGE(rgbBgColor[i]), (gdouble)rgb[i]);
  visu_gl_ext_fog_getValues(rgb);
  for (i = 0; i < 3; i++)
    gtk_range_set_value(GTK_RANGE(rgbFogColor[i]), (gdouble)rgb[i]);
  gtk_range_set_value(GTK_RANGE(rangeFogStart), (gdouble)visu_gl_ext_fog_getStart() * 100.);
  gtk_range_set_value(GTK_RANGE(rangeFogEnd), (gdouble)visu_gl_ext_fog_getEnd() * 100.);
  disableCallbacksFogBgColor = 0;
}



static void changedRGBBgColor(GtkRange *rg, gpointer data)
{
  GtkAdjustment *adj;
  int rgbMask[4] = {TOOL_COLOR_MASK_R, TOOL_COLOR_MASK_G, TOOL_COLOR_MASK_B, TOOL_COLOR_MASK_A};
  float rgb[4];

  if (disableCallbacksFogBgColor)
    return;

  g_return_if_fail(GPOINTER_TO_INT(data) >= 0 && GPOINTER_TO_INT(data) < 4);

  adj = gtk_range_get_adjustment(rg);
  rgb[GPOINTER_TO_INT(data)] = (float)gtk_adjustment_get_value(adj);
  
  if (visu_gl_ext_bg_setRGBA(visu_gl_ext_bg_getDefault(),
                             rgb, rgbMask[GPOINTER_TO_INT(data)]))
    {
      visu_gl_ext_bg_draw(visu_gl_ext_bg_getDefault());
      VISU_REDRAW_ADD;
    }
}
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 11
static void onBgImageSet(GtkFileChooserButton *filechooserbutton, gpointer data _U_)
{
  gchar *filename;

  DBG_fprintf(stderr, "Panel Fog & Bg: bg image changed.\n");
  filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filechooserbutton));
  if (!filename)
    return;
  g_idle_add(loadBgFile, (gpointer)filename);
  VISU_REDRAW_ADD;
}
#else
static void onBgImageSet(GtkDialog *dialog, gint response, gpointer data _U_)
{
  gchar *filename;

  if (response != GTK_RESPONSE_ACCEPT)
    return;

  filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
  g_idle_add(loadBgFile, (gpointer)filename);
  VISU_REDRAW_ADD;
}
#endif
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 6
static void onBgImageChoose(GtkButton *bt, gpointer data)
{
  gchar *filename;

  if (gtk_dialog_run(GTK_DIALOG(data)) != GTK_RESPONSE_ACCEPT)
    return;

  filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(data));
  g_idle_add(loadBgFile, (gpointer)filename);
  VISU_REDRAW_ADD;
}
#endif
static gboolean loadBgFile(gpointer data)
{
  GError *error;
  GdkPixbuf *pixbuf;
  gchar *filename, *title;
  gboolean fit;

  filename = (gchar*)data;

  DBG_fprintf(stderr, "Panel Fog & Bg: set the background image to '%s'.\n",
	      filename);
  error = (GError*)0;
  pixbuf = gdk_pixbuf_new_from_file(filename, &error);
  if (!pixbuf)
    {
      visu_ui_raiseWarning(_("Load image file"), error->message, (GtkWindow*)0);
      g_error_free(error);
      g_free(filename);
      return FALSE;
    }

  fit = TRUE;
  title = g_path_get_basename(filename);
  g_free(filename);
  if (!strcmp(title, "logo_grey.png"))
    {
      fit = FALSE;
      g_free(title);
      title = (gchar*)0;
    }
  visu_gl_ext_bg_setImage(visu_gl_ext_bg_getDefault(),
                          gdk_pixbuf_get_pixels(pixbuf),
                          gdk_pixbuf_get_width(pixbuf),
                          gdk_pixbuf_get_height(pixbuf),
                          gdk_pixbuf_get_has_alpha(pixbuf),
                          title, fit);
  g_object_unref(pixbuf);
  g_free(title);
  visu_gl_ext_bg_draw(visu_gl_ext_bg_getDefault());

  return FALSE;
}
static void onBgImageUnset(GtkButton *bt _U_, gpointer data)
{
  visu_gl_ext_bg_setImage(visu_gl_ext_bg_getDefault(),
                          (guchar*)0, 0, 0, FALSE, (const gchar*)0, TRUE);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  gtk_file_chooser_unselect_all(GTK_FILE_CHOOSER(data));
#endif
  visu_gl_ext_bg_draw(visu_gl_ext_bg_getDefault());
  VISU_REDRAW_ADD;
}
void visu_ui_panel_bg_setImage(const gchar *filename)
{
  gchar *path, *cur;

  DBG_fprintf(stderr, "Panel Fog & Bg: set bg image to '%s'.\n", filename);
  if (!g_path_is_absolute(filename))
    {
      cur = g_get_current_dir();
      path = g_build_filename(cur, filename, NULL);
      g_free(cur);
    }
  else
    path = g_strdup(filename);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(bgImageWd), path);
#endif

  loadBgFile(path);
}
static void onBgImageFollow(GtkToggleButton *bt, gpointer data _U_)
{
  VisuGlView *view;

  view = visu_ui_panel_getView(VISU_UI_PANEL(panelFogBgColor));
  g_return_if_fail(view);

  visu_gl_ext_bg_setFollowCamera(visu_gl_ext_bg_getDefault(),
                                 gtk_toggle_button_get_active(bt),
                                 view->camera->gross, view->camera->xs, view->camera->ys);
}
static void onCameraChange(VisuGlView *view, gpointer data _U_)
{
  if (visu_gl_ext_bg_setCamera(visu_gl_ext_bg_getDefault(),
                               view->camera->gross, view->camera->xs, view->camera->ys))
    {
      visu_gl_ext_bg_draw(visu_gl_ext_bg_getDefault());
      VISU_REDRAW_ADD;
    }
}
