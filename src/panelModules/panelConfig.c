/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "panelConfig.h"

#include <string.h>
#include <sys/stat.h>

#include <support.h>
#include <visu_object.h>
#include <visu_basic.h>
#include <visu_configFile.h>
#include <coreTools/toolConfigFile.h>

/**
 * SECTION: panelConfig
 * @short_description: The tab where miscellaneous options are setup.
 *
 * <para>Nothing special here.</para>
 */

#define FLAG_PARAMETER_TABVIEW_CONFIG   "config_subPanelTabView"
#define FLAG_PARAMETER_SKIN_CONFIG      "config_skin"
#define FLAG_PARAMETER_REFRESH_CONFIG   "config_refreshIsOn"
#define FLAG_PARAMETER_PERIOD_CONFIG    "config_refreshPeriod"
#define DESC_PARAMETER_TABVIEW_CONFIG   "See or not the labels on tabs ; boolean 0 or 1"
#define DESC_PARAMETER_SKIN_CONFIG      "Path to a gtkrc file ; chain"
#define DESC_PARAMETER_REFRESH_CONFIG   "When on V_Sim reloads the file at periodic time ; boolean 0 or 1"
#define DESC_PARAMETER_PERIOD_CONFIG    "The period of reloading in ms ; integer (10 < v < 10000)"
#define PARAMETER_CONFIG_TABVIEW_DEFAULT   0
#define PARAMETER_CONFIG_SKIN_DEFAULT      "None"
#define PARAMETER_CONFIG_REFRESH_DEFAULT   0
#define PARAMETER_CONFIG_PERIOD_DEFAULT    250
static gboolean config_subPanelTabView;
static gchar *config_skin;
static int config_refreshIsOn;
static float config_refreshPeriod;
static GtkWidget *comboUnit, *panelVBox;


/* Private functions. */
static GtkWidget *createInteriorConfig();
static void setSkin(char* label);
static gboolean automaticReload(gpointer data);

/* This function details how to read the preferedNumberOfViews
   parameter. */
static gboolean readConfigTabView(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				  VisuData *dataObj, VisuGlView *view, GError **error);
static gboolean readConfigSkin(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			       VisuData *dataObj, VisuGlView *view, GError **error);
static gboolean readConfigRefresh(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				  VisuData *dataObj, VisuGlView *view, GError **error);
static gboolean readConfigPeriod(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				 VisuData *dataObj, VisuGlView *view, GError **error);
/* These functions write all the element list to export there associated resources. */
static void exportParametersPanelConfig(GString *data,
                                        VisuData* dataObj, VisuGlView *view);
static void onUnitChanged(GtkComboBox *combo, gpointer data);

static void initPanelConfigGtkPart();

static char *defaultSkinPath, *userSkinPath;

gboolean isPanelConfigInitialized;

/* Specific widgets used in this panel. */
static VisuUiPanel *panelConfig;
static GtkWidget *checkShowTab;
static GtkWidget *checkRefreshAuto;
static GtkWidget *checkStorePositions;
static GtkWidget *checkRedCoord;

/* Callbacks */
static void checkShowTabToggled(GtkToggleButton *button, gpointer data);
static void checkRememberToggled(GtkToggleButton *button, gpointer data);
static void checkRefreshToggled(GtkToggleButton *button, gpointer data);
static void checkRedCoordToggled(GtkToggleButton *button, gpointer data);
void entryRcFilesChanged(GtkEntry *entry, gpointer data);
void directorySelectedForResources(GtkButton *button, gpointer user_data);
void openRcFileSelector(GtkButton *button, gpointer data);
static void onRefreshPeriodChanged(GtkSpinButton* button, gpointer data);
static void onDataFocused(GObject *obj, VisuData *dataObj, gpointer data);
gboolean treePathClicked(GtkWidget *widget, GdkEventButton *event, gpointer user_data);
void directoryRemoveFromResources(GtkButton *button, gpointer user_data);
static void onConfigEnter(VisuUiPanel *visu_ui_panel, gpointer data);

/**
 * visu_ui_panel_config_init: (skip)
 * @ui: a #VisuUiMain object.
 *
 * Should be used in the list declared in externalModules.h to be loaded by
 * V_Sim on start-up. This routine will create the #VisuUiPanel where the configuration
 * stuff can be done, such as the auto-reloading.
 *
 * Returns: a newly created #VisuUiPanel object.
 */
VisuUiPanel* visu_ui_panel_config_init(VisuUiMain *ui)
{
  char *cl = _("Configure the interface");
  char *tl = _("Configuration");

  visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
                           FLAG_PARAMETER_TABVIEW_CONFIG,
                           DESC_PARAMETER_TABVIEW_CONFIG,
                           1, readConfigTabView);
  visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
                           FLAG_PARAMETER_SKIN_CONFIG,
                           DESC_PARAMETER_SKIN_CONFIG,
                           1, readConfigSkin);
  visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
                           FLAG_PARAMETER_REFRESH_CONFIG,
                           DESC_PARAMETER_REFRESH_CONFIG,
                           1, readConfigRefresh);
  visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
                           FLAG_PARAMETER_PERIOD_CONFIG,
                           DESC_PARAMETER_PERIOD_CONFIG,
                           1, readConfigPeriod);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_PARAMETER,
                                    exportParametersPanelConfig);

#if GTK_MAJOR_VERSION < 3
  defaultSkinPath = g_build_filename(visu_basic_getDataDir(), "v_sim.rc", NULL);
  userSkinPath    = g_build_filename(visu_basic_getLocalDir(), "v_sim.rc", NULL);
#else
  defaultSkinPath = g_build_filename(visu_basic_getDataDir(), "v_sim.css", NULL);
  userSkinPath    = g_build_filename(visu_basic_getLocalDir(), "v_sim.css", NULL);
#endif
  config_subPanelTabView = PARAMETER_CONFIG_TABVIEW_DEFAULT;
  config_refreshPeriod = -1;
  config_refreshIsOn = -1;

  /* Now that everything has a value, we create the panel. */
  panelConfig = VISU_UI_PANEL(visu_ui_panel_newWithIconFromStock("Panel_configuration",
                                                                 cl, tl,
                                                                 GTK_STOCK_PREFERENCES));
  if (!panelConfig)
    return (VisuUiPanel*)0;

  isPanelConfigInitialized = FALSE;
  visu_ui_panel_config_setRefreshPeriod((VisuUiRenderingWindow*)0, PARAMETER_CONFIG_PERIOD_DEFAULT);
  visu_ui_panel_config_setAutomaticRefresh((VisuUiRenderingWindow*)0, PARAMETER_CONFIG_REFRESH_DEFAULT);

  /* Set global callbacks. */
  g_signal_connect(G_OBJECT(ui), "DataFocused",
		   G_CALLBACK(onDataFocused), (gpointer)0);
  g_signal_connect(G_OBJECT(panelConfig), "page-entered",
		   G_CALLBACK(onConfigEnter), (gpointer)0);

  return panelConfig;
}

static void initPanelConfigGtkPart()
{
  if (isPanelConfigInitialized)
    return;

  DBG_fprintf(stderr, "Panel Config : creating the config panel on demand.\n");
  isPanelConfigInitialized = TRUE;

  panelVBox = createInteriorConfig();
  gtk_container_add(GTK_CONTAINER(panelConfig), panelVBox);

  /* Force the callbacks to initialise the values. */
  checkShowTabToggled((GtkToggleButton*)0, (gpointer)0);
}


static GtkWidget *createInteriorConfig()
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *spinRefresh;
  const gchar **units;
  int i;
 
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12 
  GtkTooltips *tooltips;

  tooltips = gtk_tooltips_new ();
#endif

  vbox = gtk_vbox_new(FALSE, 0);

  checkShowTab = gtk_check_button_new_with_mnemonic(_("Always show _labels in tabs"));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkShowTab),
			       (gboolean)config_subPanelTabView);
  gtk_box_pack_start(GTK_BOX(vbox), checkShowTab, FALSE, FALSE, 3);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 3);
  checkRefreshAuto = gtk_check_button_new_with_mnemonic(_("Automatic _refresh"));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkRefreshAuto),
			       (gboolean)config_refreshIsOn);
  gtk_box_pack_start(GTK_BOX(hbox), checkRefreshAuto, FALSE, FALSE, 0);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 3);
  label = gtk_label_new(_(" ms"));
  gtk_box_pack_end(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  spinRefresh = gtk_spin_button_new_with_range(10., 10000., 10.);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinRefresh),
			    config_refreshPeriod);
  gtk_box_pack_end(GTK_BOX(hbox), spinRefresh, FALSE, FALSE, 0);
  label = gtk_label_new(_("period:"));
  gtk_box_pack_end(GTK_BOX(hbox), label, FALSE, FALSE, 0);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 3);
  checkStorePositions = gtk_check_button_new_with_mnemonic(_("Remember _windows positions"));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkStorePositions),
			       (gboolean)TRUE);
  gtk_box_pack_start(GTK_BOX(hbox), checkStorePositions, FALSE, FALSE, 0);

  checkRedCoord = gtk_check_button_new_with_mnemonic(_("Display _coordinates in reduce"));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkRedCoord),
			       (gboolean)visu_ui_rendering_window_class_getDisplayCoordinatesInReduce());
  gtk_box_pack_start(GTK_BOX(vbox), checkRedCoord, FALSE, FALSE, 3);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 3);
  label = gtk_label_new(_("Set the prefered unit:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);

  comboUnit = gtk_combo_box_text_new();
  units = tool_physic_getUnitNames();
  for (i = 0; units[i]; i++)
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(comboUnit), (const gchar*)0, units[i]);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboUnit), visu_basic_getPreferedUnit());
  g_signal_connect(G_OBJECT(comboUnit), "changed",
		   G_CALLBACK(onUnitChanged), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hbox), comboUnit, FALSE, FALSE, 0);

  /* Set the callbacks. */
  g_signal_connect(G_OBJECT(checkShowTab), "toggled",
		   G_CALLBACK(checkShowTabToggled), (gpointer)0);
  g_signal_connect(G_OBJECT(checkStorePositions), "toggled",
		   G_CALLBACK(checkRememberToggled), (gpointer)0);
  g_signal_connect(G_OBJECT(checkRefreshAuto), "toggled",
		   G_CALLBACK(checkRefreshToggled), (gpointer)0);
  g_signal_connect(G_OBJECT(checkRedCoord), "toggled",
		   G_CALLBACK(checkRedCoordToggled), (gpointer)0);
  g_signal_connect(G_OBJECT(spinRefresh), "value-changed",
		   G_CALLBACK(onRefreshPeriodChanged), (gpointer)0);
    
  gtk_widget_show_all(vbox);
  return vbox;
}

/*************/
/* Callbacks */
/*************/
static void checkShowTabToggled(GtkToggleButton *button _U_, gpointer data _U_)
{
  visu_ui_panel_config_setTabView(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkShowTab)));
}
static void checkRememberToggled(GtkToggleButton *button, gpointer data _U_)
{
  visu_ui_main_class_setRememberPosition(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(button)));
}
static void onConfigEnter(VisuUiPanel *visu_ui_panel _U_, gpointer data _U_)
{
  if (!isPanelConfigInitialized)
    initPanelConfigGtkPart();

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkStorePositions),
			       visu_ui_main_class_getRememberPosition());
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkRedCoord),
			       visu_ui_rendering_window_class_getDisplayCoordinatesInReduce());
}
static void checkRefreshToggled(GtkToggleButton *button, gpointer data _U_)
{
  visu_ui_panel_config_setAutomaticRefresh(visu_ui_main_class_getDefaultRendering(),
				  gtk_toggle_button_get_active(button));
}
static void checkRedCoordToggled(GtkToggleButton *button, gpointer data _U_)
{
  visu_ui_rendering_window_class_setDisplayCoordinatesInReduce(gtk_toggle_button_get_active(button));
}
static void onRefreshPeriodChanged(GtkSpinButton* button, gpointer data _U_)
{
  visu_ui_panel_config_setRefreshPeriod(visu_ui_main_class_getDefaultRendering(),
			       (float)gtk_spin_button_get_value(button));
}
void openRcFileSelector(GtkButton *button _U_, gpointer data _U_)
{
}
static void setSkin(char* label)
{
  char *tmp;
#if GTK_MAJOR_VERSION < 3
  GtkSettings *settings;
#else
  GError *error;
  GtkCssProvider *css;
#endif

  if (!label || label[0] == '\0' || !strcmp(label, "None"))
    return;

  if (!strcmp(label, "V_Sim"))
    {
      /* We try first a skin in the user config path. */
      if (g_file_test(userSkinPath, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR))
	tmp = userSkinPath;
      else
	tmp = defaultSkinPath;
    }
  else
    tmp = label;

  /* test if the file exists or fall back on default installation path */
  DBG_fprintf(stderr,"Panel Config: Reading rc file '%s' ... %d\n",
	      tmp, g_file_test(tmp, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR));
  if (!g_file_test(tmp, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR))
    return;

#if GTK_MAJOR_VERSION < 3
  gtk_rc_parse(tmp);
  DBG_fprintf(stderr, "Panel Config: get the settings.\n");
  settings = gtk_settings_get_default ();
  gtk_rc_reparse_all_for_settings(settings, TRUE);
  DBG_fprintf(stderr,"Panel Config: applying RC file OK.\n");
#else
  css = gtk_css_provider_new();
  error = (GError*)0;
  gtk_css_provider_load_from_path(css, tmp, &error);
  if (error)
    {
      g_warning("%s", error->message);
      g_error_free(error);
    }
  DBG_fprintf(stderr, "Panel Config: CSS file loaded.\n");
  gtk_style_context_add_provider_for_screen(gdk_screen_get_default(),
                                            GTK_STYLE_PROVIDER(css),
                                            GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
  g_object_unref(G_OBJECT(css));
  gtk_style_context_reset_widgets(gdk_screen_get_default());
#endif
}
void autoReloadAddTimeout(VisuData *data, guint time, gpointer user_data)
{
  guint *timeoutId;

  g_return_if_fail(data);
  
  timeoutId = (guint*)g_object_get_data(G_OBJECT(data), "autoLoad_timeoutId");
  if (!timeoutId)
    {
      timeoutId = g_malloc(sizeof(guint));
      g_object_set_data_full(G_OBJECT(data), "autoLoad_timeoutId",
			     (gpointer)timeoutId, g_free);
      *timeoutId = 0;
    }
  if (!*timeoutId)
    *timeoutId = visu_data_addTimeout(data, time, automaticReload, user_data);
}
void autoReloadRemoveTimeout(VisuData *data)
{
  guint *timeoutId;

  g_return_if_fail(data);
  
  timeoutId = (guint*)g_object_get_data(G_OBJECT(data), "autoLoad_timeoutId");
  if (timeoutId)
    {
      visu_data_removeTimeout(data, *timeoutId);
      *timeoutId = 0;
    }
}
static gboolean automaticReload(gpointer data)
{
  struct stat statBuf;
  int res, iSet;
  const gchar *file;
  time_t modTime;
  int kind, nbKind;
  gboolean reloadNeeded;
  VisuData *dataObj;
  time_t *lastReadTime;

  DBG_fprintf(stderr, "Panel Config: automatic reload called.\n");
  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelConfig));
  if (!dataObj)
    {
      DBG_fprintf(stderr, "Panel Config: automatic reload aborted, no file available.\n");
      return TRUE;
    }
  
  lastReadTime = (time_t*)g_object_get_data(G_OBJECT(dataObj), "autoLoad_accessTime");
  g_return_val_if_fail(lastReadTime, TRUE);

  reloadNeeded = FALSE;
  nbKind = visu_rendering_getNFileTypes(visu_object_getRendering(VISU_OBJECT_INSTANCE));
  for (kind = 0; kind < nbKind; kind++)
    {
      file = visu_data_getFile(dataObj, kind, (ToolFileFormat**)0);
      if (!file)
	return TRUE;

      res = stat(file, &statBuf);
      if (!res)
	{
	  modTime = statBuf.st_ctime;
	  if (modTime > lastReadTime[kind])
	    {
	      reloadNeeded = TRUE;
	      lastReadTime[kind] = modTime;
	    }
	}
    }
  if (reloadNeeded)
    {
      DBG_fprintf(stderr, "Panel Config: automatic reloading in progress...\n");
      iSet = visu_data_getISubset(dataObj);
      visu_data_freePopulation(dataObj);
      visu_ui_rendering_window_loadFile(VISU_UI_RENDERING_WINDOW(data), dataObj, iSet);
    }
  return TRUE;
}
static void onDataFocused(GObject *obj _U_, VisuData *dataObj, gpointer data _U_)
{
  struct stat statBuf;
  int res;
  const gchar *file;
  int kind, nbKind;
  time_t *lastReadTime;

  if (!dataObj)
    return;

  DBG_fprintf(stderr, "Panel Config: caught the 'DataFocused' signal,"
	      " set acces time for VisuData %p.\n", (gpointer)dataObj);
  nbKind = visu_rendering_getNFileTypes(visu_object_getRendering(VISU_OBJECT_INSTANCE));
  lastReadTime = g_malloc(sizeof(time_t) * nbKind);
  for (kind = 0; kind < nbKind; kind++)
    {
      file = visu_data_getFile(dataObj, kind, (ToolFileFormat**)0);
      if (!file)
	lastReadTime[kind] = 0;
      else
	{
	  res = stat(file, &statBuf);
	  if (!res)
	    lastReadTime[kind] = statBuf.st_ctime;
	  else
	    lastReadTime[kind] = 0;
	}
    }
  g_object_set_data_full(G_OBJECT(dataObj), "autoLoad_accessTime",
			 lastReadTime, g_free);
  if (config_refreshIsOn)
    autoReloadAddTimeout(dataObj, config_refreshPeriod,
			 (gpointer)visu_ui_main_class_getDefaultRendering());
}
static void onUnitChanged(GtkComboBox *combo, gpointer data _U_)
{
  visu_basic_setPreferedUnit(gtk_combo_box_get_active(combo));
}



/**********************************/
/* Public method to change values */
/**********************************/
/**
 * visu_ui_panel_config_setTabView:
 * @viewed: a boolean value.
 *
 * The note can show its tabs or not. Change this with that method.
 */
void visu_ui_panel_config_setTabView(gboolean viewed)
{
  DBG_fprintf(stderr,"Panel Config : toggle tab view -> %d (previously %d)\n",
	      (int)viewed, config_subPanelTabView);
  if (viewed == config_subPanelTabView)
    return;

  visu_ui_panel_class_setHeaderVisibility(viewed);
  
  /* set the associated parameter. */
  config_subPanelTabView = viewed;
}
/**
 * visu_ui_panel_config_setAutomaticRefresh:
 * @window: the #VisuUiRenderingWindow to associated the auto refresh ;
 * @bool: a boolean value.
 *
 * V_Sim can poll the rendered file at periodic intervals to check if the file
 * has been modified. If true, the file is reloaded. Turn this functionality on or off
 * with this method. See visu_ui_panel_config_setRefreshPeriod() to tune the period of the polling.
 */
void visu_ui_panel_config_setAutomaticRefresh(VisuUiRenderingWindow *window, gboolean bool)
{
  VisuData *dataObj;

  DBG_fprintf(stderr, "Panel Config : set refresh to %d (previously %d).\n",
	      bool, config_refreshIsOn);

  if (config_refreshIsOn == bool)
    return;

  config_refreshIsOn = bool;

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelConfig));
  if (dataObj)
    {
      if (bool)
	autoReloadAddTimeout(dataObj, config_refreshPeriod, (gpointer)window);
      else
	autoReloadRemoveTimeout(dataObj);
    }
}
/**
 * visu_ui_panel_config_setRefreshPeriod:
 * @window: the #VisuUiRenderingWindow to associated the auto refresh ;
 * @val: a floating point value in milliseconds.
 *
 * V_Sim can poll the rendered file at periodic intervals. Use this method to tune
 * the period. See visu_ui_panel_config_setAutomaticRefresh() to enable this functionality.
 */
void visu_ui_panel_config_setRefreshPeriod(VisuUiRenderingWindow *window, float val)
{
  VisuData *dataObj;

  DBG_fprintf(stderr, "Panel Config : set the refresh period to %f (previously %f).\n",
	      val, config_refreshPeriod);

  if (config_refreshPeriod == val)
    return;

  config_refreshPeriod = val;
  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelConfig));
  if (dataObj && config_refreshIsOn)
    {
      DBG_fprintf(stderr, "Panel Config : stopping the callback of refresh "
		  "to change the period.\n");
      autoReloadRemoveTimeout(dataObj);
      autoReloadAddTimeout(dataObj, config_refreshPeriod, (gpointer)window);
    }
}
/**
 * visu_ui_panel_config_getArea:
 *
 * This routine can be used to extend the configure panel from plug-ins.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #GtkBox containing elements of the
 * configure panel.
 **/
GtkWidget* visu_ui_panel_config_getArea()
{
  if (!isPanelConfigInitialized)
    initPanelConfigGtkPart();
  
  return panelVBox;
}

/********************/
/* In/Out functions */
/********************/

/* This function details how to read the preferedNumberOfViews
   parameter. */
static gboolean readConfigTabView(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				  VisuData *dataObj _U_, VisuGlView *view _U_,
                                  GError **error)
{
  gboolean viewed;

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readBoolean(lines[0], position, &viewed, 1, error))
    return FALSE;
  visu_ui_panel_config_setTabView(viewed);
  
  return TRUE;
}
static gboolean readConfigSkin(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			       VisuData *dataObj _U_, VisuGlView *view _U_,
                               GError **error)
{
  g_return_val_if_fail(nbLines == 1, FALSE);

  lines[0] = g_strstrip(lines[0]);

  if (!lines[0][0])
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: 1 string value must appear"
			     " after the %s markup.\n"), position,
			   FLAG_PARAMETER_SKIN_CONFIG);
      config_skin = g_strdup(PARAMETER_CONFIG_SKIN_DEFAULT);
      setSkin(config_skin);
      return FALSE;
    }
  config_skin = g_strdup(lines[0]);
  
  setSkin(config_skin);

  return TRUE;
}
static gboolean readConfigRefresh(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				  VisuData *dataObj _U_, VisuGlView *view _U_,
                                  GError **error)
{
  gboolean bool;

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readBoolean(lines[0], position, &bool, 1, error))
    return FALSE;
  visu_ui_panel_config_setAutomaticRefresh(visu_ui_main_class_getDefaultRendering(), bool);

  return TRUE;
}
static gboolean readConfigPeriod(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				 VisuData *dataObj _U_, VisuGlView *view _U_,
                                 GError **error)
{
  float val;

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readFloat(lines[0], position, &val, 1, error))
    return FALSE;
  config_refreshPeriod = val;
  visu_ui_panel_config_setRefreshPeriod(visu_ui_main_class_getDefaultRendering(),
			       config_refreshPeriod);

  return TRUE;
}

/* These functions write all the element list to export there associated resources. */
static void exportParametersPanelConfig(GString *data,
                                        VisuData *dataObj _U_, VisuGlView *view _U_)
{
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_TABVIEW_CONFIG);
  g_string_append_printf(data, "%s[gtk]: %d\n\n", FLAG_PARAMETER_TABVIEW_CONFIG,
			 config_subPanelTabView);
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_SKIN_CONFIG);
  g_string_append_printf(data, "%s[gtk]: %s\n\n", FLAG_PARAMETER_SKIN_CONFIG,
			 config_skin);
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_REFRESH_CONFIG);
  g_string_append_printf(data, "%s[gtk]: %d\n\n", FLAG_PARAMETER_REFRESH_CONFIG,
			 config_refreshIsOn);
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_PERIOD_CONFIG);
  g_string_append_printf(data, "%s[gtk]: %i\n\n", FLAG_PARAMETER_PERIOD_CONFIG,
			 (int)config_refreshPeriod);
}

