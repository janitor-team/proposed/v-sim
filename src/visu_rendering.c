/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "visu_rendering.h"

#include "visu_data.h"
#include "visu_basic.h"
#include "visu_tools.h"
#include "visu_configFile.h"
#include "coreTools/toolConfigFile.h"
#include "renderingMethods/renderingAtomic.h"
#include "renderingMethods/renderingSpin.h"

#include <glib.h>
#include <glib/gstdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * SECTION:visu_rendering
 * @short_description: Methods to create and add new rendering
 * methods.
 *
 * <para> The way visu renders its data is done by modules. They are
 * called rendering methods and they describes how data are drawn on
 * the screen. Many can be defined but only one is used at a time to
 * render the data.</para>
 *
 * <para>One or more file type are associated with a rendering
 * method. And a rendering method must specify the way to load the
 * data it needs. Taking the example of a spin system representation,
 * there are two kinds of file. The first kind describes the position
 * the spin and the second contains their orientations.</para>
 *
 * <para>To create a new rendering method, subclass
 * #VisuRendering. The name is mandatory and must be
 * unique. The description is not compulsory. The number of file kinds
 * is also required. Use renderingMethodSet_fileType() to associated a
 * #GList of #ToolFileFormat. In our example of spin system, the first
 * kind of file is about positions, and the associated file formats
 * are *.ascii, *.d3 and *.xyz.</para>
 *
 * <para>The #VisuRendering_struct has to two pointers on methods
 * that are important. The first, createOpenGLElementFunc() is called
 * when V_Sim needs to create an OpenGL list corresponding to the
 * #VisuElement given as argument. This list then can be used to
 * render each node and thus accelerating the rendering
 * operations. The second method is createOpenGLNodeFunc() and is
 * called by V_Sim for each node of the system when the main OpenGL
 * list (the one for the nodes) is created. Thus, in the contrary of
 * the first method, thios one should not create an OpenGL list but
 * directly call OpenGL routines to draw the node. This method is also
 * responsible to put the node at the right position. Use
 * visu_data_getNodePosition() to retrieve the position and translate
 * the node accordingly.</para>
 */

/**
 * VisuRendering:
 *
 * This structure is used to describe a rendering method. Besides
 * names, representing icon... this structure stores pointers to
 * method technically used to draw elements with this method.
 */

/**
 * VisuRenderingFormatLoad_struct:
 * @name: a descriptive name ;
 * @fmt: file formats associated to this load method ;
 * @load: the load method ;
 * @priority: an int.
 *
 * The priority argument is used when a file is tested to be opened.
 * The smaller, the earlier the load method is tested.
 */
typedef struct _LoadFormat
{
  ToolFileFormat *fmt;
  VisuRenderingLoadFormatFunc load;
  int priority;
} LoadFormat;

struct _VisuRenderingPrivate
{
  /* Some variable to describe this rendering method.
     The attribute name is mandatory since it is
     used to identify the method. */
  gchar* name;
  gchar* label;
  gchar* desc;
  /* A path to a small 16x16 image to represent the method.
     If null, no image is used. */
  gchar* icon;

  /* Dealing with file type. */
  /* Number of files required to render one set of data. */
  guint nFiles;
  GList **formats;
  gchar **fileTypeLabel;

  /* Pointers to useful openGL methods. */
  /* VisuRenderingCreateElementFunc createElement; */
  /* VisuRenderingCreateNodeFunc    createNode; */
  /* VisuRenderingGetNodeExtendFunc getExtend; */

  gboolean dispose_has_run;
};


enum {
  FILE_TYPE_CHANGED,
  ELEMENT_SIZE_CHANGED,
  LAST_SIGNAL
};

enum {
    PROP_0,
    NAME_PROP,
    LABEL_PROP,
    DESC_PROP,
    NFILES_PROP
};

/* Id of possible signals. */
static guint signals[LAST_SIGNAL];
static GQuark quark = (GQuark)0;
/* A GHashTable to store all the available rendering methods
   in the system. The keys are the name of each method. */
static GHashTable *tableOfMethods = (GHashTable*)0;
/* The list of the available rendering method. */
static GList      *listOfMethods  = (GList*)0;

#define FLAG_FAV_RENDER    "rendering_favoriteMethod"
#define DESC_FAV_RENDER    "Favorite method used to render files ; chain"
#define DEFAULT_RENDER_FAV ""
static gboolean readFavVisuRendering(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                     VisuData *dataObj, VisuGlView *view, GError **error);
static void exportFavVisuRendering(GString *data, VisuData *dataObj, VisuGlView *view);

/* Local callbacks. */
static void visu_rendering_dispose(GObject* obj);
static void visu_rendering_finalize(GObject* obj);
static void visu_rendering_get_property(GObject* obj, guint property_id,
                                        GValue *value, GParamSpec *pspec);
static void visu_rendering_set_property(GObject* obj, guint property_id,
                                        const GValue *value, GParamSpec *pspec);

/* Local methods. */
static void setupAllMethods();
static gint comparePriority(gconstpointer a, gconstpointer b);

G_DEFINE_TYPE(VisuRendering, visu_rendering, G_TYPE_OBJECT)

static void visu_rendering_class_init(VisuRenderingClass *klass)
{
  GType paramFloat[1] = {G_TYPE_FLOAT};

  DBG_fprintf(stderr, "Visu Rendering: creating the class of the object.\n");

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose      = visu_rendering_dispose;
  G_OBJECT_CLASS(klass)->finalize     = visu_rendering_finalize;
  G_OBJECT_CLASS(klass)->set_property = visu_rendering_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_rendering_get_property;
  klass->createElement                = NULL;
  klass->createNode                   = NULL;
  klass->getNodeExtend                = NULL;
  klass->getElementGlId               = NULL;

  /* Initialise statics. */
  quark = g_quark_from_static_string("visu_rendering");
  /**
   * VisuRendering::fileTypeChanged:
   * @obj: the object emitting the signal.
   *
   * TODO
   *
   * Since: 3.6
   */
  signals[FILE_TYPE_CHANGED] = 
    g_signal_newv ("fileTypeChanged",
		   G_TYPE_FROM_CLASS (klass),
		   G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		   NULL /* class closure */,
		   NULL /* accumulator */,
		   NULL /* accu_data */,
		   g_cclosure_marshal_VOID__VOID,
		   G_TYPE_NONE /* return_type */,
		   0     /* n_params */,
		   NULL  /* param_types */);
  /**
   * VisuRendering::elementSizeChanged:
   * @obj: the object emitting the signal.
   * @size: the new size.
   *
   * Emitted when the size of a element is changed.
   *
   * Since: 3.6
   */
  signals[ELEMENT_SIZE_CHANGED] = 
    g_signal_newv ("elementSizeChanged",
		   G_TYPE_FROM_CLASS (klass),
		   G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE,
		   NULL /* class closure */,
		   NULL /* accumulator */,
		   NULL /* accu_data */,
		   g_cclosure_marshal_VOID__FLOAT,
		   G_TYPE_NONE /* return_type */,
		   1    /* n_params */,
		   paramFloat  /* param_types */);

  /**
   * VisuRendering::name:
   *
   * The name identifying the method. Can be used as an id.
   *
   * Since: 3.6
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), NAME_PROP,
     g_param_spec_string("name", _("Name"), _("Name of the method."), "",
                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE | G_PARAM_READABLE));
  /**
   * VisuRendering::label:
   *
   * The label describing the method, can be translate and used in a UI.
   *
   * Since: 3.6
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), LABEL_PROP,
     g_param_spec_string("label", _("Label"), _("Label of the method."), "",
                         G_PARAM_WRITABLE | G_PARAM_READABLE));
  /**
   * VisuRendering::description:
   *
   * A longer text describing the method, can be translate and used in a UI.
   *
   * Since: 3.6
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), DESC_PROP,
     g_param_spec_string("description", _("Description"), _("Description of the method."), "",
                         G_PARAM_WRITABLE | G_PARAM_READABLE));
  /**
   * VisuRendering::nFiles:
   *
   * Number of files to be read to get information for the rendering method.
   *
   * Since: 3.6
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), NFILES_PROP,
     g_param_spec_uint("nFiles", _("Number of input files"),
                       _("Required number of input files to read to load a data."), 1, 100, 1,
                       G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE | G_PARAM_READABLE));

  /* Initialise internal variables. */
  visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
                           FLAG_FAV_RENDER, DESC_FAV_RENDER,
                           1, readFavVisuRendering);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_PARAMETER,
				   exportFavVisuRendering);

  tableOfMethods = g_hash_table_new(g_str_hash, g_str_equal);
  listOfMethods  = (GList*)0;

  g_type_class_add_private(klass, sizeof(VisuRenderingPrivate));
}

static void visu_rendering_init(VisuRendering *obj)
{
  DBG_fprintf(stderr, "Visu rendering: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = G_TYPE_INSTANCE_GET_PRIVATE(obj, VISU_TYPE_RENDERING, VisuRenderingPrivate);
  obj->priv->name          = (gchar*)0;
  obj->priv->label         = (gchar*)0;
  obj->priv->desc          = (gchar*)0;
  obj->priv->icon          = (gchar*)0;
  obj->priv->nFiles        = 0;
  obj->priv->formats       = (GList**)0;
  obj->priv->fileTypeLabel = (gchar**)0;
  obj->priv->dispose_has_run = FALSE;
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_rendering_dispose(GObject* obj)
{
  DBG_fprintf(stderr, "Visu Rendering: dispose object %p.\n", (gpointer)obj);

  if (VISU_RENDERING(obj)->priv->dispose_has_run)
    return;

  VISU_RENDERING(obj)->priv->dispose_has_run = TRUE;

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_rendering_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_rendering_finalize(GObject* obj)
{
  VisuRenderingPrivate *meth;
  GList *tmpLst;
  guint i;
  LoadFormat *format;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Visu Rendering: finalize object %p.\n", (gpointer)obj);

  meth = VISU_RENDERING(obj)->priv;

  /* Remove object from class lists list. */
  listOfMethods = g_list_remove(listOfMethods, (gpointer)obj);
  g_hash_table_remove(tableOfMethods, (gpointer)(meth->name));

  if (meth->name)
    g_free(meth->name);
  if (meth->label)
    g_free(meth->label);
  if (meth->desc)
    g_free(meth->desc);
  if (meth->formats)
    {
      for (i = 0; i < meth->nFiles; i++)
        for (tmpLst = meth->formats[i]; tmpLst; tmpLst = g_list_next(tmpLst))
          {
            format = (LoadFormat*)tmpLst->data;
            g_object_unref(format->fmt);
            g_free(format);
          }
      g_free(meth->formats);
    }
  if (meth->fileTypeLabel)
    g_strfreev(meth->fileTypeLabel);

  G_OBJECT_CLASS(visu_rendering_parent_class)->finalize(obj);
}
static void visu_rendering_get_property(GObject* obj, guint property_id,
                                        GValue *value, GParamSpec *pspec)
{
  VisuRenderingPrivate *self = VISU_RENDERING(obj)->priv;

  DBG_fprintf(stderr, "Visu Rendering: get property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case NAME_PROP:
      g_value_set_string(value, self->name); DBG_fprintf(stderr, "%s.\n", self->name); break;
    case LABEL_PROP:
      g_value_set_string(value, self->label); DBG_fprintf(stderr, "%s.\n", self->label); break;
    case DESC_PROP:
      g_value_set_string(value, self->desc); DBG_fprintf(stderr, "%s.\n", self->desc); break;
    case NFILES_PROP:
      g_value_set_uint(value, self->nFiles); DBG_fprintf(stderr, "%d.\n", self->nFiles); break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_rendering_set_property(GObject* obj, guint property_id,
                                        const GValue *value, GParamSpec *pspec)
{
  VisuRenderingPrivate *self = VISU_RENDERING(obj)->priv;

  DBG_fprintf(stderr, "Visu Rendering: set property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case NAME_PROP:
      self->name = g_value_dup_string(value); DBG_fprintf(stderr, "%s.\n", self->name);
      DBG_fprintf(stderr, "Visu Rendering: registering this method.\n");
      g_hash_table_insert(tableOfMethods, (gpointer)self->name,
                          (gpointer)VISU_RENDERING(obj));
      listOfMethods = g_list_append(listOfMethods, (gpointer)VISU_RENDERING(obj));  
      break;
    case LABEL_PROP:
      self->label = g_value_dup_string(value); DBG_fprintf(stderr, "%s.\n", self->label); break;
    case DESC_PROP:
      self->desc = g_value_dup_string(value); DBG_fprintf(stderr, "%s.\n", self->desc); break;
    case NFILES_PROP:
      self->nFiles = g_value_get_uint(value); DBG_fprintf(stderr, "%d.\n", self->nFiles);
      self->formats       = g_malloc0(sizeof(GList*) * self->nFiles);
      self->fileTypeLabel = g_malloc0(sizeof(gchar*) * (self->nFiles + 1));
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}

static gint comparePriority(gconstpointer a, gconstpointer b)
{
  if (((LoadFormat*)a)->priority < ((LoadFormat*)b)->priority)
    return (gint)-1;
  else if (((LoadFormat*)a)->priority > ((LoadFormat*)b)->priority)
    return (gint)+1;
  else
    return (gint)0;
}

static void setupAllMethods()
{
  DBG_fprintf(stderr,"Visu Rendering: instantiate the rendering methods.\n");
  /* Setup all methods. */
  visu_rendering_atomic_new();
  visu_rendering_spin_new();
}

/**
 * visu_rendering_getNFileTypes:
 * @method: a method.
 *
 * This method is used to get the number of kind of files needed to
 * render a set of data.
 *
 * Returns: how many kind of files are handled by the given VisuRendering.
 */
guint visu_rendering_getNFileTypes(VisuRendering *method)
{
  g_return_val_if_fail(VISU_IS_RENDERING_TYPE(method), -1);

  return method->priv->nFiles;
}

/**
 * visu_rendering_setFileTypeLabel:
 * @method: a method ;
 * @fileType: an integer used as a key, must >= 0 and < method->nFiles ;
 * @name: a string to shortly describe the kind of file type (not NULL).
 *
 * Store a list of #ToolFileFormat for the kind of file @fileType. The @name
 * argument is copied. but warning, the @fileTypeList GList* is not copied.
 */
void visu_rendering_setFileTypeLabel(VisuRendering *method,
                                     guint fileType, const gchar* name)
{
  g_return_if_fail(VISU_IS_RENDERING_TYPE(method) && name && name[0]);
  g_return_if_fail(fileType < method->priv->nFiles);

  if (method->priv->fileTypeLabel[fileType])
    g_free(method->priv->fileTypeLabel[fileType]);
  method->priv->fileTypeLabel[fileType] = g_strdup(name);
}
/**
 * visu_rendering_addFileFormat:
 * @method: a method ;
 * @fileType: an integer used as a key, must >= 0 and < method->nFiles.
 * @fmt: a #ToolFileFormat ;
 * @priority: the priority (lower sooner) ;
 * @loadFunc: (scope call): the loading routine.
 *
 * Add a file format descriptor to the list of already known file formats
 * of the key @fileType.
 */
void visu_rendering_addFileFormat(VisuRendering *method, guint fileType,
                                  ToolFileFormat *fmt, guint priority,
                                  VisuRenderingLoadFormatFunc loadFunc)
{
  LoadFormat *format;

  g_return_if_fail(VISU_IS_RENDERING_TYPE(method) && fmt && loadFunc);
  g_return_if_fail(fileType < method->priv->nFiles);

  format = g_malloc(sizeof(LoadFormat));
  format->fmt = fmt;
  format->load = loadFunc;
  format->priority = priority;
  method->priv->formats[fileType] = g_list_prepend(method->priv->formats[fileType], (gpointer)format);
  method->priv->formats[fileType] = g_list_sort(method->priv->formats[fileType],
                                          comparePriority);
  g_signal_emit(G_OBJECT(method), signals[FILE_TYPE_CHANGED], 0, NULL);
}
/**
 * visu_rendering_setFileFormat:
 * @meth: a #VisuRendering object.
 * @fileType: a file kind id.
 * @from: a #VisuRendering object.
 *
 * It copies the #ToolFileFormat of method @from to @meth for the
 * given @fileType.
 *
 * Since: 3.6
 */
void visu_rendering_setFileFormat(VisuRendering *meth, guint fileType,
                                  VisuRendering *from)
{
  GList *tmpLst;
  LoadFormat *format, *f;

  g_return_if_fail(VISU_IS_RENDERING_TYPE(meth) && VISU_IS_RENDERING_TYPE(from));
  g_return_if_fail(fileType < meth->priv->nFiles);
  g_return_if_fail(fileType < from->priv->nFiles);

  for (tmpLst = meth->priv->formats[fileType]; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      format = (LoadFormat*)tmpLst->data;
      g_object_unref(G_OBJECT(format->fmt));
      g_free(format);
    }
  g_list_free(meth->priv->formats[fileType]);
  meth->priv->formats[fileType] = (GList*)0;
  for (tmpLst = from->priv->formats[fileType]; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      f = (LoadFormat*)tmpLst->data;
      format = g_malloc(sizeof(LoadFormat));
      format->fmt      = tool_file_format_copy(f->fmt);
      format->load     = f->load;
      format->priority = f->priority;
      meth->priv->formats[fileType] = g_list_prepend(meth->priv->formats[fileType],
                                               (gpointer)format);
    }
  meth->priv->formats[fileType] = g_list_sort(meth->priv->formats[fileType],
                                        comparePriority);
  g_signal_emit(G_OBJECT(meth), signals[FILE_TYPE_CHANGED], 0, NULL);
}
/**
 * visu_rendering_getFileFormat:
 * @method: this #VisuRendering object.
 * @fileType: the file kind of filee format to get from.
 *
 * This method is used to get the file formats associated to a kind of input file
 * handled by the rendering method.
 *
 * Returns: (transfer container) (element-type ToolFileFormat*): a
 * GList* with the #ToolFileFormat. This GList should been considered
 * read-only.
 *
 * Since: 3.6
 */
GList* visu_rendering_getFileFormat(VisuRendering *method, guint fileType)
{
  GList* lst, *tmpLst;

  g_return_val_if_fail(VISU_IS_RENDERING_TYPE(method), (GList*)0);
  g_return_val_if_fail(fileType < method->priv->nFiles, (GList*)0);

  lst = (GList*)0;
  for (tmpLst = method->priv->formats[fileType]; tmpLst; tmpLst = g_list_next(tmpLst))
    lst = g_list_append(lst, (gpointer)((LoadFormat*)tmpLst->data)->fmt);

  return lst;
}
/**
 * visu_rendering_getFileTypeName:
 * @method: a method ;
 * @fileType: an integer used as a key, must >= 0 and < method->priv->nFiles.
 *
 * This method is used to get the name associated to a kind of input file
 * handled by the rendering method.
 *
 * Returns: a string own by V_Sim. This string should been considered read-only.
 */
const gchar* visu_rendering_getFileTypeName(VisuRendering *method, guint fileType)
{
  g_return_val_if_fail(VISU_IS_RENDERING_TYPE(method), (gchar*)0);
  g_return_val_if_fail(fileType < method->priv->nFiles, (gchar*)0);

  return method->priv->fileTypeLabel[fileType];
}
/**
 * visu_rendering_getSizeOfElement:
 * @method: a method ;
 * @ele: a #VisuElement to get the size of.
 *
 * This method is used to retrieve the radius of the sphere that contains
 * the @ele.
 *
 * Returns: the radius of the given element.
 */
float visu_rendering_getSizeOfElement(VisuRendering *method, VisuElement *ele)
{
  g_return_val_if_fail(VISU_IS_RENDERING_TYPE(method) && VISU_IS_ELEMENT_TYPE(ele), 0.);
  g_return_val_if_fail(VISU_RENDERING_GET_CLASS(method)->getNodeExtend, 0.);

  DBG_fprintf(stderr, "Visu Rendering: get size for element '%s'.\n", ele->name);
  return VISU_RENDERING_GET_CLASS(method)->getNodeExtend(ele);
}
/**
 * visu_rendering_getElementGlId:
 * @method: a method ;
 * @ele: a #VisuElement object.
 *
 * This method is used to get the OpenGL identifier of a list for @ele.
 *
 * Since: 3.7
 *
 * Returns: a OpenGL list identifier.
 **/
int visu_rendering_getElementGlId(VisuRendering *method, VisuElement *ele)
{
  g_return_val_if_fail(VISU_IS_RENDERING_TYPE(method) && VISU_IS_ELEMENT_TYPE(ele), 0);
  g_return_val_if_fail(VISU_RENDERING_GET_CLASS(method)->getElementGlId, 0);

  DBG_fprintf(stderr, "Visu Rendering: get gl id for element '%s'.\n", ele->name);
  return VISU_RENDERING_GET_CLASS(method)->getElementGlId(ele);
}
/**
 * visu_rendering_createElement:
 * @method: a #VisuRendering method.
 * @element: a #VisuElement object.
 * @view: a #VisuGlView object.
 *
 * Use the create element function of @method to render @element for
 * the given zoom level.
 *
 * Since: 3.6
 *
 * Returns: the OpenGL list id of this element.
 */
int visu_rendering_createElement(VisuRendering *method, VisuElement *element,
                                 VisuGlView *view)
{
  g_return_val_if_fail(VISU_IS_RENDERING_TYPE(method), 0);

  return VISU_RENDERING_GET_CLASS(method)->createElement(element, view);
}
/**
 * visu_rendering_createNode:
 * @method: a #VisuRendering method.
 * @data: the #VisuData object the @node is taken from.
 * @node: a #VisuNode of @data.
 * @ele: (allow-none): the #VisuElement of @node or NULL.
 * @eleGlId: the OpenGL list id repsresenting @ele or -1 to get the
 * default one.
 *
 * Create @node at the right position calling OpenGL routines.
 *
 * Since: 3.6
 */
void visu_rendering_createNode(VisuRendering *method, VisuData *data,
                               VisuNode *node, VisuElement *ele, int eleGlId)
{
  g_return_if_fail(VISU_IS_RENDERING_TYPE(method));

  /* Default values. */
  if (!ele)
    ele = visu_node_array_getElement(VISU_NODE_ARRAY(data), node);
  if (eleGlId == -1)
    eleGlId = visu_rendering_getElementGlId(method, ele);

  /* Active call. */
  VISU_RENDERING_GET_CLASS(method)->createNode(data, node, ele, eleGlId);
}
/**
 * visu_rendering_getName:
 * @method: a #VisuRendering method.
 * @UTF8: a boolean.
 *
 * Get its name (in @UTF8 or not).
 *
 * Returns: a string, owned by V_Sim.
 */
const gchar* visu_rendering_getName(VisuRendering *method, gboolean UTF8)
{
  g_return_val_if_fail(VISU_IS_RENDERING_TYPE(method), (const gchar*)0);

  if (UTF8)
    return method->priv->label;
  else
    return method->priv->name;
}
/**
 * visu_rendering_getDescription:
 * @method: a #VisuRendering method.
 *
 * Get its description (in UTF8).
 *
 * Returns: a string, owned by V_Sim.
 */
const gchar* visu_rendering_getDescription(VisuRendering *method)
{
  g_return_val_if_fail(VISU_IS_RENDERING_TYPE(method), (const gchar*)0);

  return method->priv->desc;
}
/**
 * visu_rendering_getIconPath:
 * @method: a #VisuRendering method.
 *
 * Get the location where to find the icon of the method.
 *
 * Returns: a string, owned by V_Sim.
 */
const gchar* visu_rendering_getIconPath(VisuRendering *method)
{
  g_return_val_if_fail(VISU_IS_RENDERING_TYPE(method), (const gchar*)0);

  return method->priv->icon;
}
/**
 * visu_rendering_setIcon:
 * @method: a method ;
 * @path: a path to an image file.
 *
 * This method is used to set the path to an icon for the specified method.
 * The path is copied, and the given name can be freed freely after a call to
 * this method.
 */
void visu_rendering_setIcon(VisuRendering *method, const char *path)
{
  g_return_if_fail(VISU_IS_RENDERING_TYPE(method));

  if (method->priv->icon)
    {
      g_free(method->priv->icon);
      method->priv->icon = (gchar*)0;
    }
  if (!path)
    return;

  method->priv->icon = g_strdup(path);
}

static gboolean loadFileType(VisuRendering *method,
                             VisuData *data, guint fileType,
                             int nSet, GCancellable *cancel, GError **error)
{
  gchar *file;
  gboolean loadOk;
  GList *tmpLst;
  ToolFileFormat *fmt;
  LoadFormat *load;
#if GLIB_MINOR_VERSION > 5
  struct stat buf;
#endif

  g_return_val_if_fail(error && *error == (GError*)0, FALSE);
  file = g_strdup(visu_data_getFile(data, fileType, &fmt));
  if (!file)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FILE,
			   _("No file name available."));
      return FALSE;
    }
  
  if (!g_file_test(file, G_FILE_TEST_IS_REGULAR))
    {
      g_free(file);
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FILE,
			   _("The specified file is"
			     " not a regular file or may not exist."));
      return FALSE;
    }
#if GLIB_MINOR_VERSION > 5
  if (!g_stat(file, &buf) && buf.st_size == 0)
    {
      g_free(file);
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FILE,
			   _("The specified file is"
			     " an empty file."));
      return FALSE;
    }
#endif
  
  loadOk = FALSE;
  tmpLst = method->priv->formats[fileType];
  while (tmpLst && !loadOk)
    {
      load = (LoadFormat*)(tmpLst->data);

      /* Each load may set error even if the format is not recognise
	 and loadOK is FALSE, then we need to free the error. */
      if (*error)
	g_error_free(*error);
      *error = (GError*)0;

      if (!fmt || load->fmt == fmt)
	{
	  DBG_fprintf(stderr,"Visu Rendering: testing '%s' with format: %s.\n",
		      file, tool_file_format_getName(load->fmt));
          DBG_fprintf(stderr, " | %p has %d ref counts.\n",
                      (gpointer)data, G_OBJECT(data)->ref_count);
	  loadOk = load->load(data, file, load->fmt, nSet, cancel, error);
	  DBG_fprintf(stderr,"Visu Rendering: with format '%s' returns %d.\n",
		      tool_file_format_getName(load->fmt), loadOk);
          DBG_fprintf(stderr, " | %p has %d ref counts.\n",
                      (gpointer)data, G_OBJECT(data)->ref_count);
	}
      if (*error && (*error)->domain == G_FILE_ERROR)
        {
          g_free(file);
          return FALSE;
        }
      
      if (fmt && load->fmt == fmt)
	tmpLst = (GList*)0;
      else
	tmpLst = g_list_next(tmpLst);
    }
  g_free(file);

  if (!loadOk)
    {
      if (*error)
	g_error_free(*error);
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FILE,
			   _("Impossible to load this file, unrecognised format.\n"));
      return FALSE;
    }
  else if (*error)
    return FALSE;

  return TRUE;
}
/**
 * visu_rendering_load:
 * @method: a #VisuRendering method.
 * @data: a new #VisuData object to load into.
 * @nSet: the set id to read from.
 * @cancel: a cancellable.
 * @error: a location for possible error.
 *
 * Call the load routine of @method. Filenames to read from should
 * have been set to @data using visu_data_addFile(). If @format is
 * NULL, an automatic detection is used, trying all available file
 * formats.
 *
 * Returns: TRUE on success.
 */
gboolean visu_rendering_load(VisuRendering *method, VisuData *data,
                             int nSet, GCancellable *cancel, GError **error)
{
  guint i;
  VisuNodeArrayIter iter;
  ToolUnits unit, preferedUnit;

  g_return_val_if_fail(error && *error == (GError*)0, FALSE);
  g_return_val_if_fail(data, FALSE);

  DBG_fprintf(stderr, "Visu Rendering: starting load procedure...\n");

  for (i = 0; i < method->priv->nFiles; i++)
    {
      if(!loadFileType(method, data, i, nSet, cancel, error))
        return FALSE;
      /* If we have errors, we return. */
      if (*error)
        return TRUE;
    }

  /* We do a last check on population... in case. */
  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  g_return_val_if_fail(iter.nAllStoredNodes, FALSE);

  DBG_fprintf(stderr, "loading OK (%d nodes).\n", iter.nAllStoredNodes);

  /* Setup additional infos. */
  visu_data_setISubset(data, nSet);
  visu_data_setChangeElementFlag(data, FALSE);

  unit = visu_box_getUnit(visu_boxed_getBox(VISU_BOXED(data)));
  preferedUnit = visu_basic_getPreferedUnit();
  if (preferedUnit != TOOL_UNITS_UNDEFINED &&
      unit != TOOL_UNITS_UNDEFINED &&
      unit != preferedUnit)
    visu_box_setUnit(visu_boxed_getBox(VISU_BOXED(data)), preferedUnit);

  return TRUE;
}

/**
 * visu_rendering_getAllObjects:
 *
 * This method gives a GList with pointers to each rendering method.
 * Warning : the returned GList is not a copy, it must not be modified,
 * just read.
 *
 * Returns: (element-type VisuRendering*) (transfer none): A GList
 * containing all the registered rendering methods.
 */
GList* visu_rendering_getAllObjects(void)
{
  if (!tableOfMethods)
    g_type_class_ref(VISU_TYPE_RENDERING);
  if (g_hash_table_size(tableOfMethods) == 0)
    setupAllMethods();
  
  return listOfMethods;
}
/**
 * visu_rendering_getByName:
 * @name: (type filename): a string.
 *
 * Get the corresponding #VisuRendering method to @name.
 *
 * Returns: (transfer none):
 *
 * Since: 3.6
 */
VisuRendering* visu_rendering_getByName(const gchar *name)
{
  if (!tableOfMethods)
    g_type_class_ref(VISU_TYPE_RENDERING);
  if (g_hash_table_size(tableOfMethods) == 0)
    setupAllMethods();
  
  return (VisuRendering*)g_hash_table_lookup(tableOfMethods, (gpointer)name);
}
/**
 * visu_rendering_class_getErrorQuark:
 * 
 * Internal function to handle error.
 *
 * Returns: a #GQuark for #VisuRendering method errors.
 */
GQuark visu_rendering_class_getErrorQuark(void)
{
  if (!quark)
    g_type_class_ref(VISU_TYPE_RENDERING);

  return quark;
}

/**************/
/* Parameters */
/**************/
static gboolean readFavVisuRendering(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                     VisuData *dataObj _U_, VisuGlView *view _U_,
                                     GError **error)
{
  VisuRendering *meth;

  g_return_val_if_fail(nbLines == 1, FALSE);

  lines[0] = g_strstrip(lines[0]);

  if (!lines[0][0])
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
                           _("Parse error at line %d:"
                             " there is no specified method.\n"), position);
      return FALSE;
    }
  else
    {
      meth = visu_rendering_getByName(lines[0]);
      if (!meth)
	{
	  *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			       _("Parse error at line %d:"
				 " the specified method (%s) is unknown.\n"),
			       position, lines[0]);
	  return FALSE;
	}
      visu_object_setRendering(VISU_OBJECT_INSTANCE, meth);
    }

  return TRUE;
}
static void exportFavVisuRendering(GString *data,
                                   VisuData *dataObj _U_, VisuGlView *view _U_)
{
  VisuRendering *meth;

  meth = visu_object_getRendering(VISU_OBJECT_INSTANCE);
  g_return_if_fail(meth);
  g_string_append_printf(data, "# %s\n", DESC_FAV_RENDER);
  g_string_append_printf(data, "%s: %s\n\n", FLAG_FAV_RENDER,
                         meth->priv->name);
}
