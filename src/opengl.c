/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/gl.h>
#include <GL/glu.h> 

#include "opengl.h"

#include "visu_extension.h"
#include "visu_tools.h"
#include "visu_object.h"
#include "visu_configFile.h"
#include "openGLFunctions/renderingMode.h"
#include "openGLFunctions/interactive.h"
#include "openGLFunctions/view.h"
#include "openGLFunctions/objectList.h"
#include "coreTools/toolConfigFile.h"
#include "coreTools/toolColor.h"

/**
 * SECTION:opengl
 * @short_description: This part is responsible for the pseudo3D
 * rendering through OpenGl and gives methods to adapt the view.
 *
 * <para>There is a last parameter which controls when render is
 * done. There are two states : whenever a changing occurs (in fact
 * when the OpenGLAskForReDraw signal is received) or only the
 * OpenGLForceReDraw is received.</para>
 *
 * <para>Except when high performances are required, this module never
 * makes a redraw by itself, even when parameters such as the camera
 * position are changed. The redraw must be asked by the client by
 * emitting the OpenGLAskForReDraw signal or the OpenGLForceReDraw
 * signal. This is to avoid situations when the client makes different
 * changes but want just only one redraw at the end. All set methods
 * return a boolean which informs the client of the opportunity to
 * redraw after the set occurs. For example, if the client call a set
 * change on theta or phi but the server returns FALSE, it means that
 * not emission of OpenGLAskForReDraw is needed (for example because
 * theta or phi had already these values).</para>
 */

/******************************************************************************/

/* static int stereo; */
static VisuGlLights *currentLights = NULL;

#define FLAG_PARAMETER_OPENGL_IMMEDIATE   "opengl_immediateDrawing"
#define DESC_PARAMETER_OPENGL_IMMEDIATE   "If true, changes of parameters means immediate redrawing ; boolean 0 or 1"
#define PARAMETER_OPENGL_IMMEDIATE_DEFAULT 1
static gboolean opengl_immediate;
static gboolean readOpenGLImmediate(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                    VisuData *dataObj, VisuGlView *view, GError **error);

#define FLAG_PARAMETER_OPENGL_ANTIALIAS "opengl_antialias"
#define DESC_PARAMETER_OPENGL_ANTIALIAS "If true, lines are drawn smoother ; boolean 0 or 1"
#define PARAMETER_OPENGL_ANTIALIAS_DEFAULT 0
static gboolean antialias;
static gboolean readOpenGLAntialias(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                    VisuData *dataObj, VisuGlView *view, GError **error);

#define FLAG_PARAMETER_OPENGL_FAKEBS "opengl_fakeBackingStore"
#define DESC_PARAMETER_OPENGL_FAKEBS "If true, V_Sim catches the Expose event from the X server and calls a redraw ; boolean 0 or 1"
#define PARAMETER_OPENGL_FAKEBS_DEFAULT 0
static int fakeBackingStore;

#define FLAG_PARAMETER_OPENGL_TRANS "opengl_trueTransparency"
#define DESC_PARAMETER_OPENGL_TRANSS "If true, the transparency rendering is enhanced ; boolean 0 or 1"
static gboolean trueTransparency = FALSE;
static gboolean readOpenGLTransparency(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				       VisuData *dataObj, VisuGlView *view,
                                       GError **error);

#define FLAG_PARAMETER_OPENGL_ANGLE "opengl_stereoAngle"
#define DESC_PARAMETER_OPENGL_ANGLE "Give the angle of the two receivers in stereo output ; float positive"
static float stereoAngles[2];
#define PARAMETER_OPENGL_ANGLE_DEFAULT 5.f
static gboolean readOpenGLStereoAngle(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                      VisuData *dataObj, VisuGlView *view,
                                      GError **error);

#define FLAG_PARAMETER_OPENGL_STEREO "opengl_stereo"
#define DESC_PARAMETER_OPENGL_STEREO "If true, try to draw in stereo ; boolean 0 or 1"
static gboolean stereoStatus = FALSE;
static gboolean readOpenGLStereo(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                 VisuData *dataObj, VisuGlView *view, GError **error);

/* Local methods. */
static void exportParametersOpenGL(GString *data, VisuData *dataObj, VisuGlView *view);
static void glSetAntiAlias();

/******************************************************************************/

/**
 * visu_gl_init:
 *
 * This initialises the drawing window and all the variables used
 * by this module. It must be called once by the main program and should
 * not be used otherwise.
 */
void visu_gl_init()
{
  VisuConfigFileEntry *resourceEntry;

  DBG_fprintf(stderr, "OpenGl: initialization.\n");

  /* Parameters */
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
					  FLAG_PARAMETER_OPENGL_TRANS,
					  DESC_PARAMETER_OPENGL_TRANSS,
					  1, readOpenGLTransparency);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
					  FLAG_PARAMETER_OPENGL_ANTIALIAS,
					  DESC_PARAMETER_OPENGL_ANTIALIAS,
					  1, readOpenGLAntialias);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
					  FLAG_PARAMETER_OPENGL_IMMEDIATE,
					  DESC_PARAMETER_OPENGL_IMMEDIATE,
					  1, readOpenGLImmediate);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
					  FLAG_PARAMETER_OPENGL_ANGLE,
					  DESC_PARAMETER_OPENGL_ANGLE,
					  1, readOpenGLStereoAngle);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
					  FLAG_PARAMETER_OPENGL_STEREO,
					  DESC_PARAMETER_OPENGL_STEREO,
					  1, readOpenGLStereo);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_PARAMETER,
				   exportParametersOpenGL);
  
  g_type_class_ref(visu_interactive_get_type());

  antialias = PARAMETER_OPENGL_ANTIALIAS_DEFAULT;
  fakeBackingStore = PARAMETER_OPENGL_FAKEBS_DEFAULT;
  opengl_immediate = PARAMETER_OPENGL_IMMEDIATE_DEFAULT;
  stereoAngles[0] = +PARAMETER_OPENGL_ANGLE_DEFAULT;
  stereoAngles[1] = -PARAMETER_OPENGL_ANGLE_DEFAULT;

  visu_gl_objectlist_init();
}
/**
 * visu_gl_initGraphics:
 *
 * Internal use only. Call at startup to call OpenGL initialisation routines.
 *
 * Since: 3.6
 */
void visu_gl_initGraphics()
{
  /* Deals with lights. */
  currentLights = visu_gl_lights_new();
  visu_gl_lights_add(currentLights, visu_gl_light_newDefault());

  /* Initialise others sub-modules dealing with OpenGL. */
  visu_gl_rendering_init();
  g_type_class_ref(VISU_TYPE_GL_VIEW);
}

/**
 * visu_gl_getLights:
 *
 * V_Sim proposes a wrapper around the OpenGL light definitions.
 *
 * Returns: the set of current lights.
 */
VisuGlLights* visu_gl_getLights()
{
  if (!currentLights)
    visu_gl_initGraphics();

  return currentLights;
}

/******************************************************************************/

/**
 * visu_gl_setColor:
 * @material: a 5 elements array with the material properties ;
 * @rgba: a 4 elements array with the color values.
 *
 * This method call glMaterial to create the right shiningness, emission, diffuse...
 */
void visu_gl_setColor(float* material, float* rgba)
{
  float mm[4] = {0.0f, 0.0f, 0.0f, 0.0f};

  DBG_fprintf(stderr, "OpenGL: set rgba colours %gx%gx%gx%g.\n",
              rgba[0], rgba[1], rgba[2], rgba[3]);
  glColor4fv(rgba);

  mm[3] = rgba[3];
  mm[0] = material[VISU_GL_LIGHT_MATERIAL_AMB] * rgba[0];
  mm[1] = material[VISU_GL_LIGHT_MATERIAL_AMB] * rgba[1];
  mm[2] = material[VISU_GL_LIGHT_MATERIAL_AMB] * rgba[2];
  glMaterialfv(GL_FRONT, GL_AMBIENT, mm);
  mm[0] = material[VISU_GL_LIGHT_MATERIAL_DIF] * rgba[0];
  mm[1] = material[VISU_GL_LIGHT_MATERIAL_DIF] * rgba[1];
  mm[2] = material[VISU_GL_LIGHT_MATERIAL_DIF] * rgba[2];
  glMaterialfv(GL_FRONT, GL_DIFFUSE, mm);
  glMaterialf(GL_FRONT, GL_SHININESS, material[VISU_GL_LIGHT_MATERIAL_SHI] * 128.0f);
  mm[0] = material[VISU_GL_LIGHT_MATERIAL_SPE];
  mm[1] = material[VISU_GL_LIGHT_MATERIAL_SPE];
  mm[2] = material[VISU_GL_LIGHT_MATERIAL_SPE];
  glMaterialfv(GL_FRONT, GL_SPECULAR, mm);
  mm[0] = material[VISU_GL_LIGHT_MATERIAL_EMI] * rgba[0];
  mm[1] = material[VISU_GL_LIGHT_MATERIAL_EMI] * rgba[1];
  mm[2] = material[VISU_GL_LIGHT_MATERIAL_EMI] * rgba[2];
  glMaterialfv(GL_FRONT, GL_EMISSION, mm);
}
/**
 * visu_gl_setHighlightColor:
 * @material: a 5 elements array with the material properties ;
 * @rgb: a 3 elements array with the color values.
 * @alpha: the alpha channel.
 *
 * This method try to set a colour that will contrast with @rgb.
 */
void visu_gl_setHighlightColor(float material[5], float rgb[3], float alpha)
{
  float rgba[4], hsl[3];
  
  tool_color_convertRGBtoHSL(hsl, rgb);
  hsl[0] = tool_modulo_float(0.5f + hsl[0], 1);
  hsl[1] = 1.f;
  hsl[2] = .5f;
  tool_color_convertHSLtoRGB(rgba, hsl);
  rgba[3] = alpha;
  visu_gl_setColor(material, rgba);
}

/**
 * visu_gl_initContext:
 *
 * This method is called when an OpenGL surface is created for the first time.
 * It sets basic OpenGL options and calls other OpenGLFunctions used in V_Sim.
 */
void visu_gl_initContext()
{
  if (!currentLights)
    visu_gl_initGraphics();

  /* Set the openGL flags. */
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
  glDepthRange(0.0, 1.0);
  glClearDepth(1.0);

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);

  glEnable(GL_NORMALIZE);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  visu_gl_lights_apply(currentLights);
  visu_gl_rendering_applyMode(visu_gl_rendering_getGlobalMode());

  glSetAntiAlias();
}

/******************************************************************************/

/**
 * visu_gl_redraw:
 * @view: (transfer none): a #VisuGlView object with the camera settings.
 * @lists: (element-type v_sim.GlExt) (transfer none) (allow-none): a
 * #VisuGlExt array or %NULL.
 *
 * Basic drawing method : it clears the OpenGL area and call all lists if @lists
 * is %NULL, or draw only the given lists.
 */
void visu_gl_redraw(VisuGlView *view, GList *lists)
{
  int i_stereo, stereo;
  static int stereo_buf[2] = {GL_BACK_LEFT, GL_BACK_RIGHT};
  GLboolean glStereo;
  float centre[3];
  GList *ext, *lst;
  VisuBox *box;
#if DEBUG == 1
  GTimer *timer;
  gulong fractionTimer;
#endif

#if DEBUG == 1
  timer = g_timer_new();
  g_timer_start(timer);
#endif
  
  box = visu_boxed_getBox(VISU_BOXED(view));
  if (box)
    visu_box_getCentre(box, centre);
  else
    {
      centre[0] = 0.f;
      centre[1] = 0.f;
      centre[2] = 0.f;
    }

  ext = lists;
  if (!lists)
    ext = visu_gl_ext_getAll();

  glGetBooleanv(GL_STEREO, &glStereo);
  stereo = (view && glStereo && stereoStatus)?1:0;
  for(i_stereo = 0; i_stereo <= stereo; i_stereo++)
    {
      if (stereo == 1)
        {
          glRotatef(stereoAngles[i_stereo],
        	    view->camera->up[0],
        	    view->camera->up[1],
        	    view->camera->up[2]);
          glDrawBuffer(stereo_buf[i_stereo]);
          DBG_fprintf(stderr, "OpenGL: draw on buffer %d.\n", i_stereo);
        }
      else
        glDrawBuffer(GL_BACK);

      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      glPushMatrix();
      glTranslated(-centre[0], -centre[1], -centre[2]);

      glEnable(GL_DEPTH_TEST);

      /* if (!lists && !trueTransparency) */
      /*   { */
      /*     visu_gl_ext_callAll(FALSE); */
      /*     visu_gl_ext_callAll(TRUE); */
      /*   } */
      /* else if (!lists && trueTransparency) */
      /*   { */
      /*     glDepthMask(1); */
      /*     glEnable(GL_ALPHA_TEST); */
      /*     glAlphaFunc(GL_EQUAL, 1.); */
      /*     visu_gl_ext_callAll(FALSE); */
      /*     DBG_fprintf(stderr, "OpenGL: second pass for transparency.\n"); */
      /*     glAlphaFunc(GL_LESS, 1.); */
      /*     glDepthMask(0); */
      /*     visu_gl_ext_callAll(FALSE); */
      /*     glDepthMask(1); */
      /*     glAlphaFunc(GL_ALWAYS, 1.); */
      /*     glDisable(GL_ALPHA_TEST); */
      /*     visu_gl_ext_callAll(TRUE); */
      /*   } */
      /* else */ if (!trueTransparency)
        for (lst = ext; lst; lst = g_list_next(lst))
          visu_gl_ext_call(VISU_GL_EXT(lst->data), FALSE);
      else
        {
          glDepthMask(1);
          glEnable(GL_ALPHA_TEST);
          glAlphaFunc(GL_EQUAL, 1.);
          for (lst = ext; lst; lst = g_list_next(lst))
            visu_gl_ext_call(VISU_GL_EXT(lst->data), FALSE);
          DBG_fprintf(stderr, "OpenGL: second pass for transparency.\n");
          glAlphaFunc(GL_LESS, 1.);
          glDepthMask(0);
          for (lst = ext; lst; lst = g_list_next(lst))
            visu_gl_ext_call(VISU_GL_EXT(lst->data), FALSE);
          glDepthMask(1);
          glAlphaFunc(GL_ALWAYS, 1.);
          glDisable(GL_ALPHA_TEST);
        }
      for (lst = ext; lst; lst = g_list_next(lst))
        visu_gl_ext_call(VISU_GL_EXT(lst->data), TRUE);

      glPopMatrix();
    }

#if DEBUG == 1
  glFlush();
  g_timer_stop(timer);
  fprintf(stderr, "OpenGL: lists drawn in %g micro-s.\n",
          g_timer_elapsed(timer, &fractionTimer)*1e6);
  g_timer_destroy(timer);
#endif
}

/******************************************************************************/

/* To set the antialiasing on lines. */
static void glSetAntiAlias()
{
  if (antialias)
    {
      /* Set blend if not present */
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
/*       glBlendFunc(GL_SRC_ALPHA_SATURATE,GL_ONE); */

      /* Antialias */
      glEnable(GL_LINE_SMOOTH);
      glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
    }
  else
    {
      glDisable(GL_LINE_SMOOTH);
/*       glDisable(GL_BLEND); */
    }
}
/**
 * visu_gl_setAntialias:
 * @value: a boolean to activate or not the lines antialias.
 *
 * To set the antialiasing on lines.
 *
 * Returns: TRUE if the signal OpenGLAskForReDraw should be emitted.
 */
gboolean visu_gl_setAntialias(gboolean value)
{
  if (value == antialias)
    return FALSE;

  antialias = value;
  glSetAntiAlias();
  
  return TRUE;
}
/**
 * visu_gl_getAntialias:
 *
 * Get the value of the antialiasing parameter.
 *
 * Returns: wether or not the antialising for lines is activated.
 */
gboolean visu_gl_getAntialias()
{
  return antialias;
}

/**
 * visu_gl_setTrueTransparency:
 * @status: a boolean.
 *
 * If true the rendering is done twice to respect the transparency.
 *
 * Returns: TRUE if redraw should be done.
 */
gboolean visu_gl_setTrueTransparency(gboolean status)
{
  if (status == trueTransparency)
    return FALSE;

  trueTransparency = status;  
  return TRUE;
}
/**
 * visu_gl_getTrueTransparency:
 *
 * The drawing can be done in one pass or two to respect transparency.
 *
 * Returns: TRUE if the drawing is done twice.
 */
gboolean visu_gl_getTrueTransparency()
{
  return trueTransparency;
}

/**
 * visu_gl_setImmediate:
 * @bool: a boolean to set or not the immediateDrawing option.
 *
 * If true all changes are applied only when the refresh button
 * is pushed.
 *
 * Returns: TRUE if the value is changed.
 */
gboolean visu_gl_setImmediate(gboolean bool)
{
  if (bool == opengl_immediate)
    return FALSE;

  opengl_immediate = bool;
  return TRUE;
}
/**
 * visu_gl_getImmediate:
 *
 * Get the value of the immediateDrawing option.
 *
 * Returns: the value of the immediateDrawing option.
 */
gboolean visu_gl_getImmediate()
{
  return opengl_immediate;
}
/**
 * visu_gl_setStereoAngle:
 * @angle: a positive floating point value.
 *
 * Change the angle of the eyes in the stereo output.
 *
 * Returns: TRUE if redraw should be done.
 */
gboolean visu_gl_setStereoAngle(float angle)
{
  g_return_val_if_fail(angle > 0.f, FALSE);

  if (stereoAngles[0] == angle)
    return FALSE;

  stereoAngles[0] = angle;
  stereoAngles[1] = -angle;

  return stereoStatus;
}
/**
 * visu_gl_getStereoAngle:
 *
 * Retrieve the angle of the eyes in the stereo output.
 *
 * Returns: the angle.
 */
float visu_gl_getStereoAngle()
{
  return stereoAngles[0];
}
/**
 * visu_gl_getStereoCapability:
 *
 * Retrieve if the OpenGL window can render in stereo or not.
 *
 * Returns: TRUE if the OpenGL surface can draw in stereo.
 */
gboolean visu_gl_getStereoCapability()
{
  GLboolean glStereo;

  glGetBooleanv(GL_STEREO, &glStereo);

  return (gboolean)glStereo;
}
/**
 * visu_gl_setStereo:
 * @status: a boolean.
 *
 * Change the type of rendering. The surface can be switch to stereo,
 * only if the OpenGL has stereo capabilities (see
 * visu_gl_getStereoCapability()).
 */
gboolean visu_gl_setStereo(gboolean status)
{
  if (stereoStatus == status)
    return FALSE;
  stereoStatus = status;
  return TRUE;
}
/**
 * visu_gl_getStereo:
 *
 * Retrieve the status of the OpenGL surface.
 *
 * Returns: TRUE if the surface try to draw in stereo (may be TRUE,
 *          even if visu_gl_getStereoCapability() returns FALSE, in
 *          that case the stereo capability is not used).
 */
gboolean visu_gl_getStereo()
{
  return stereoStatus;
}

/***************************/
/* Dealing with parameters */
/***************************/

static gboolean readOpenGLAntialias(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				    VisuData *dataObj _U_, VisuGlView *view _U_,
                                    GError **error)
{
  gboolean val;
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readBoolean(lines[0], position, &val, 1, error))
    return FALSE;
  visu_gl_setAntialias((int)val);

  return TRUE;
}

static gboolean readOpenGLImmediate(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				    VisuData *dataObj _U_, VisuGlView *view _U_,
                                    GError **error)
{
  gboolean val;
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readBoolean(lines[0], position, &val, 1, error))
    return FALSE;
  visu_gl_setImmediate((int)val);

  return TRUE;
}
static gboolean readOpenGLTransparency(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				       VisuData *dataObj _U_, VisuGlView *view _U_,
                                       GError **error)
{
  gboolean val;
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readBoolean(lines[0], position, &val, 1, error))
    return FALSE;
  visu_gl_setTrueTransparency(val);

  return TRUE;
}
static gboolean readOpenGLStereo(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				 VisuData *dataObj _U_, VisuGlView *view _U_,
                                 GError **error)
{
  gboolean val;
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readBoolean(lines[0], position, &val, 1, error))
    return FALSE;
  visu_gl_setStereo(val);

  return TRUE;
}
static gboolean readOpenGLStereoAngle(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				      VisuData *dataObj _U_, VisuGlView *view _U_,
                                      GError **error)
{
  float val;
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readFloat(lines[0], position, &val, 1, error))
    return FALSE;
  if (val <= 0.f)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: angle must be positive.\n"),
			   position);
      return FALSE;
    }
  visu_gl_setStereoAngle(val);

  return TRUE;
}
static void exportParametersOpenGL(GString *data,
                                   VisuData *dataObj _U_, VisuGlView *view _U_)
{
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_OPENGL_ANTIALIAS);
  g_string_append_printf(data, "%s: %d\n\n", FLAG_PARAMETER_OPENGL_ANTIALIAS,
	  antialias);
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_OPENGL_IMMEDIATE);
  g_string_append_printf(data, "%s: %d\n\n", FLAG_PARAMETER_OPENGL_IMMEDIATE,
	  opengl_immediate);
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_OPENGL_TRANSS);
  g_string_append_printf(data, "%s: %d\n\n", FLAG_PARAMETER_OPENGL_TRANS,
	  trueTransparency);
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_OPENGL_STEREO);
  g_string_append_printf(data, "%s: %d\n\n", FLAG_PARAMETER_OPENGL_STEREO,
	  stereoStatus);
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_OPENGL_ANGLE);
  g_string_append_printf(data, "%s: %f\n\n", FLAG_PARAMETER_OPENGL_ANGLE,
	  stereoAngles[0]);
}
