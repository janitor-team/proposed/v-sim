/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "atomic_xyz.h"
#include "renderingAtomic_ascii.h"
#include "renderingAtomic.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <visu_rendering.h>
#include <visu_data.h>
#include <coreTools/toolPhysic.h>
#include <extraFunctions/vibration.h>
#include <extraFunctions/extraNode.h>

/*#define TESTNEW*/

/**
 * SECTION:atomic_xyz
 * @short_description: Method to load xyz position file.
 *
 * <para>XYZ formats are plain text format to store atomic
 * positions. This format is quite simple, first line must contain the
 * number of element, then
 * the second usually store a commentary (but this is not required),
 * then all lines that are not beginning with a '#' are tried to match
 * "label x y z":. If succeed a node is added, if not, the next line
 * is read as much time as specified on first line. This scheme can be
 * repeated as much time as required to store animation for instance.</para>
 */

static gboolean loadXyz(VisuData *data, const gchar* filename,
                        ToolFileFormat *format, int nSet,
                        GCancellable *cancel, GError **error);
static int read_Xyz_File(VisuData *data, GIOChannel *flux, int nSet, GError **error);

#ifdef TESTNEW
static gboolean test_Routine(float* coords, float* dcoord, VisuElement **nodeTypes);
#endif

/******************************************************************************/
/**
 * initAtomicXyz: (skip)
 * @method: the #VisuRendering method to be associated to.
 *
 * Create the structure that gives access to a load method and
 * some description of it (file formats associated, name...). Internal
 * use only
 */
void initAtomicXyz(VisuRendering *method)
{
  const gchar *typeXYZ[] = {"*.xyz", (char*)0};

  visu_rendering_addFileFormat(method, 0,
                               tool_file_format_new(_("'Element x y z' format"),
                                                    typeXYZ),
                               100, loadXyz);
}

static gboolean loadXyz(VisuData *data, const gchar* filename,
			ToolFileFormat *format _U_, int nSet,
                        GCancellable *cancel _U_, GError **error)
{
  int res;
  GIOChannel *readFrom;
  GIOStatus status;

  g_return_val_if_fail(error && *error == (GError*)0, FALSE);
  g_return_val_if_fail(data && filename, FALSE);

  readFrom = g_io_channel_new_file(filename, "r", error);
  if (!readFrom)
    return FALSE;

  res = read_Xyz_File(data, readFrom, nSet, error);
  DBG_fprintf(stderr, "Atomic XYZ: parse return %d.\n", res);
  status = g_io_channel_shutdown(readFrom, TRUE, (GError**)0);
  g_io_channel_unref(readFrom);
  if (status != G_IO_STATUS_NORMAL)
    {
      DBG_fprintf(stderr, "Atomic XYZ: can't close file.\n");
      return FALSE;
    }

  if (res < 0)
    {
      if (*error)
	g_error_free(*error);
      *error = (GError*)0;
      /* The file is not a XYZ file. */
      return FALSE;
    }
  else if (res > 0)
    /* The file is a XYZ file but some errors occured. */
    return TRUE;
  /* Everything is OK. */
  *error = (GError*)0;
  return TRUE;
}

static gboolean readNextLine(GIOChannel *flux, gboolean mandatory,
                             GString *line, GIOStatus *status, GError **error)
{
  gsize terminator_pos;

  /*if there are commentaries */
  do
    {
      *status = g_io_channel_read_line_string(flux, line,
                                             &terminator_pos, error);
      if (*status != G_IO_STATUS_NORMAL)
        {
          if (*status == G_IO_STATUS_EOF)
            {
              if (*error)
                g_error_free(*error);
              *error = (GError*)0;
              return !mandatory;
            }
          return FALSE;
        };
      g_strstrip(line->str);
    }
  while (line->str[0] == '#' || line->str[0] == '!' || line->str[0] == '\0');

  return TRUE;
}

static ToolUnits readUnit(const gchar *line)
{
  ToolUnits unit;
  gchar *tmpStr;
  guint nNodes;

  unit = TOOL_UNITS_UNDEFINED;

  tmpStr = g_strdup(line);
  if (sscanf(line, "%u %s", &nNodes, tmpStr) == 2)
    unit = tool_physic_getUnitFromName(g_strstrip(tmpStr));
  DBG_fprintf(stderr, " | units for the set is '%s' -> %d.\n",
              tmpStr, unit);
  g_free(tmpStr);

  return unit;
}
static gboolean readReduced(const gchar *line)
{
  gboolean reduced;
  gchar *tmpStr;
  guint nNodes;

  reduced = FALSE;
  tmpStr = g_strdup(line);
  if (sscanf(line, "%u %s", &nNodes, tmpStr) == 2)
    reduced = (!g_ascii_strcasecmp(tmpStr, "reduced"));
  DBG_fprintf(stderr, " | coordinates are reduced %d.\n", reduced);
  g_free(tmpStr);

  return reduced;
}

static float readEnergy(const gchar *line)
{
  gchar *tmpStr;
  guint nNodes;
  float ene;

  tmpStr = g_strdup(line);
  if (sscanf(line, "%u %s %f", &nNodes, tmpStr, &ene) != 3)
    ene = G_MAXFLOAT;
  else
    ene *= 27.21138386;
  g_free(tmpStr);
  
  DBG_fprintf(stderr, " | total energy for the set is %geV.\n", ene);
  return ene;
}

static void readFree(GList *lst, GHashTable *elements, GString *line,
                     VisuElement **nodeTypes, float *coords,
                     float *dcoord, float *forces, GList *labels)
{
  GList *tmpLst;

  if (line) g_string_free(line, TRUE);
  g_hash_table_destroy(elements);
  for (tmpLst = lst; tmpLst; tmpLst = g_list_next(tmpLst))
    g_free(tmpLst->data);
  if (lst) g_list_free(lst);
  if (nodeTypes) g_free(nodeTypes);
  if (coords) g_free(coords);
  if (dcoord) g_free(dcoord);
  if (forces) g_free(forces);
  for (tmpLst = labels; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      tmpLst = g_list_next(tmpLst);
      g_free(tmpLst->data);
    }
  if (labels) g_list_free(labels);
}

static void readCoord(const gchar *line, guint iNodes, guint nNodes,
                      float *xyz, float **dxyz, GHashTable *elements,
                      VisuElement **nodeTypes, gchar **label, GError **error)
{
  guint nbcolumn;
  float dxyz_[3];
  gchar nomloc[TOOL_MAX_LINE_LENGTH];
  VisuElement *type;
  struct VisuMethAscii *infos;
  int pos;

  *label = (gchar*)0;
  nbcolumn = sscanf(line, "%s %f %f %f %f %f %f",
                    nomloc,  xyz + 3 * iNodes + 0, xyz + 3 * iNodes + 1,
                    xyz + 3 * iNodes + 2, dxyz_ + 0, dxyz_ + 1, dxyz_ + 2);
  if (nbcolumn < 4)
    {
      DBG_fprintf(stderr, "Atomic XYZ: can't read line values.\n");
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
                           _("Wrong XYZ format, 'Atom X Y Z' awaited."));
    }
  else if (((*dxyz) && nbcolumn != 7) ||
           (!(*dxyz) && iNodes > 0 && nbcolumn == 7))
    {
      DBG_fprintf(stderr, "Atomic XYZ: can't read vibration values.\n");
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
                           _("Wrong XYZ + vibration format,"
                             " 'Atom X Y Z vx vy vz' awaited."));
    }
  if (nbcolumn == 7)
    {
      if (!(*dxyz))
        *dxyz = g_malloc(sizeof(float) * 3 * nNodes);
      (*dxyz)[3 * iNodes + 0] = dxyz_[0];
      (*dxyz)[3 * iNodes + 1] = dxyz_[1];
      (*dxyz)[3 * iNodes + 2] = dxyz_[2];
    }
  if (*error)
    return;

  nomloc[8] = '\0';
  type = visu_element_retrieveFromName(nomloc, (gboolean*)0);
  if (!type)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
                           _("Internal error, cannot create type for '%s'."),
                           nomloc);
      return;
    }
  nodeTypes[iNodes] = type;
  infos = (struct VisuMethAscii*)
    g_hash_table_lookup(elements, (gconstpointer)type);
  if (!infos)
    {
      infos          = g_malloc(sizeof(struct VisuMethAscii));
      infos->ele     = type;
      infos->pos     = g_hash_table_size(elements);
      infos->nbNodes = 1;
      g_hash_table_insert(elements, (gpointer)type, (gpointer)infos);
    }
  else
    infos->nbNodes += 1;

  /* Store a possible comment. */
  pos = 0;
  if (nbcolumn == 4)
    sscanf(line, "%s %f %f %f %n", nomloc, xyz + 3 * iNodes + 0,
           xyz + 3 * iNodes + 1, xyz + 3 * iNodes + 2, &pos);
  else
    sscanf(line, "%s %f %f %f %f %f %f %n",
           nomloc, xyz + 3 * iNodes + 0, xyz + 3 * iNodes + 1,
           xyz + 3 * iNodes + 2, dxyz_ + 0, dxyz_ + 1, dxyz_ + 2, &pos);
  if (line[pos] != '\0')
    {
      *label = g_strdup(line + pos + ((line[pos] == '#')?1:0));
      g_strstrip(*label);
      if ((*label)[0] == '\0')
        {
          g_free(*label);
          *label = (gchar*)0;
        }
    }
}

static void readForces(GIOChannel *flux, GString *line, guint nNodes,
                       float **forces, GIOStatus *status, GError **error)
{
  guint iNodes;
  gchar nomloc[TOOL_MAX_LINE_LENGTH];

  if (!strcmp(line->str, "forces"))
    {
      if (forces)
        *forces = g_malloc(sizeof(float) * nNodes * 3);
      for (iNodes = 0; iNodes < nNodes && *status == G_IO_STATUS_NORMAL; iNodes++)
	{
          if (!readNextLine(flux, TRUE, line, status, error))
            {
              *error = g_error_new(VISU_ERROR_RENDERING,
                                   RENDERING_ERROR_FORMAT,
                                   _("Missing forces (%d read but"
                                     " %d declared).\n"), iNodes, nNodes);
              return;
            }
          if (forces && sscanf(line->str, "%s %f %f %f",
                               nomloc,  *forces + 3 * iNodes + 0,
                               *forces + 3 * iNodes + 1,
                               *forces + 3 * iNodes + 2) != 4)
            {
              *error = g_error_new(VISU_ERROR_RENDERING,
                                   RENDERING_ERROR_FORMAT,
                                   _("Cannot read forces in '%s'.\n"), line->str);
              return;
            }
	}
      /* Eat blank or commentary lines between Sets */
      readNextLine(flux, FALSE, line, status, error);
    }
}

/******************************************************************************/
static int read_Xyz_File(VisuData *data, GIOChannel *flux, int nSet, GError **error)
{
  GIOStatus status;
  GString *line;
  gsize terminator_pos;
  int i;
  int res, nNodes, iNodes, nNodesSet;
  int nSets;
  char *kwd;
  float *coords, *dcoord, *forces;
  gchar *infoUTF8;
  GList *lst, *tmpLst;
  GHashTable *elements;
  float qpt[3], omega, totalEnergy;
  double box[3], boxGeometry[6];
  int ntype;
  ToolUnits unit;
  GArray *types, *nattyp;
  VisuElement **nodeTypes;
  guint natom;
  gchar *pt, *label;
  VisuBoxBoundaries bc;
  gboolean reduced;
  GList *nodeComments;
  VisuBox *boxObj;

#if DEBUG == 1
  GTimer *timer, *readTimer, *internalTimer;
  gulong fractionTimer;
  float time1, time2, time3;
#endif

  DBG_fprintf(stderr, "Atomic xyz: reading file as an xyz file.\n");

  line = g_string_new("");
  unit = TOOL_UNITS_UNDEFINED;
  reduced = FALSE;

  /* Storage of number of elements per types. */
  ntype = 0;
  elements = g_hash_table_new_full(g_direct_hash, g_direct_equal, NULL, g_free);

  /* We read the file completely to find the number of sets of points
     and we store only the one corresponding to @nSet. */
#if DEBUG == 1
  timer = g_timer_new();
  readTimer = g_timer_new();
  internalTimer = g_timer_new();
  g_timer_start(timer);
  g_timer_start(internalTimer);
  g_timer_stop(internalTimer);
  time3 = 0.f;
#endif

  nSets        = 0;
  nNodesSet    = 0;
  nodeTypes    = (VisuElement**)0;
  coords       = (float*)0;
  dcoord       = (float*)0;
  forces       = (float*)0;
  lst          = (GList*)0;
  nodeComments = (GList*)0;
  totalEnergy  = G_MAXFLOAT;
  status = g_io_channel_read_line_string(flux, line, &terminator_pos, error);

  if ( status != G_IO_STATUS_NORMAL )
    {
      readFree(lst, elements, line, nodeTypes, coords, dcoord, forces, nodeComments);
      return -1;
    }
 
  while( status != ( G_IO_STATUS_EOF ) )
    { 
      DBG_fprintf(stderr, "Atomic xyz: read node set number %d.\n", nSets);

      /*The Number Of Nodes*/
      nNodes=0;
      DBG_fprintf(stderr, "Atomic xyz: get n atoms from '%s'.\n", line->str);
      res = sscanf(line->str, "%d", &nNodes);
      if ( res != 1 ) 
	{ 
	  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			       _("Wrong XYZ format, no number on first line.\n"));
          readFree(lst, elements, line, nodeTypes, coords, dcoord, forces, nodeComments);
	  return (nSets > 0)?1:-1;
	}
      DBG_fprintf(stderr, " | number of declared nodes is %d.\n", nNodes);
#if DEBUG == 1
      if (nSets == nSet)
        g_timer_start(readTimer);
      else
	g_timer_continue(internalTimer);
#endif
      if (nSets == nSet)
        {
          unit = readUnit(line->str);
          reduced = readReduced(line->str);
          totalEnergy = readEnergy(line->str);
        }

      /*The Commentary line */
      if ( g_io_channel_read_line_string(flux, line, &terminator_pos, error) !=
	   G_IO_STATUS_NORMAL )
	{
          readFree(lst, elements, line, nodeTypes, coords, dcoord, forces, nodeComments);
	  return -2;
	}
      g_strstrip(line->str);
      DBG_fprintf(stderr, " | set the commentary to '%s'.\n", line->str);
      if (line->str[0] == '#')
	infoUTF8 = g_locale_to_utf8(line->str + 1, -1, NULL, NULL, NULL);
      else
	infoUTF8 = g_locale_to_utf8(line->str,     -1, NULL, NULL, NULL);
      if (infoUTF8)
	lst = g_list_append(lst, infoUTF8);
      else
	g_warning("Can't convert '%s' to UTF8.\n", line->str);
	
      /* The Data Lines.*/
      if (nSets == nSet)
	{
	  nNodesSet = nNodes; 
	  nodeTypes = g_malloc(sizeof(VisuElement*) * nNodes);
	  coords    = g_malloc(sizeof(float) * 3 * nNodes);
	  dcoord    = (float*)0;
          forces    = (float*)0;
	  qpt[0] = 0.f;
	  qpt[1] = 0.f;
	  qpt[2] = 0.f;
	}
      status = G_IO_STATUS_NORMAL;
      DBG_fprintf(stderr, " | read node coordinates.\n");
      for (iNodes = 0; iNodes < nNodes && status == G_IO_STATUS_NORMAL; iNodes++)
	{
          if (!readNextLine(flux, TRUE, line, &status, error))
            {
              *error = g_error_new(VISU_ERROR_RENDERING,
                                   RENDERING_ERROR_FORMAT,
                                   _("Missing coordinates (%d read but"
                                     " %d declared).\n"), iNodes, nNodes);
              readFree(lst, elements, line, nodeTypes, coords, dcoord, forces, nodeComments);
              return 1;
            }
          
          /* if Reading the nSets, parse the data */
          if (nSets == nSet)
            {
              /* Read the coordinates. */
              readCoord(line->str, iNodes, nNodes, coords, &dcoord,
                        elements, nodeTypes, &label, error);
	      if (*error)
		{
                  readFree(lst, elements, line, nodeTypes, coords, dcoord, forces, nodeComments);
		  return 1;			
		}
              if (label)
                {
                  nodeComments = g_list_prepend(nodeComments, label);
                  nodeComments = g_list_prepend(nodeComments,
                                                GINT_TO_POINTER(iNodes));
                }
	    }
#if DEBUG == 1
	  if (nSets == nSet)
	    g_timer_stop(readTimer);
	  else
	    g_timer_stop(internalTimer);
#endif
	}
#if DEBUG == 1
      g_timer_stop(timer);
#endif      
      /* Eat blank or commentary lines after coordinates. */
      if (!readNextLine(flux, FALSE, line, &status, error))
        {
          readFree(lst, elements, line, nodeTypes, coords, dcoord, forces, nodeComments);
          return 1;			
        }
      /* Maybe read forces (BigDFT addition). */
      readForces(flux, line, nNodes, (nSets == nSet)?&forces:(float**)0, &status, error);
      if (*error)
        {
          readFree(lst, elements, line, nodeTypes, coords, dcoord, forces, nodeComments);
          return 1;			
        }
      /* OK, one set of nodes have been read. */
      nSets++;
      DBG_fprintf(stderr, " | read OK %d %d.\n", status, G_IO_STATUS_EOF);
    }
  g_string_free(line, TRUE);
  ntype = g_hash_table_size(elements);
  DBG_fprintf(stderr, " | finish to read the file (ntype = %d).\n", ntype);
#if DEBUG == 1
  g_timer_stop(timer);
  time1 = g_timer_elapsed(timer, &fractionTimer)/1e-6;
  time2 = g_timer_elapsed(readTimer, &fractionTimer)/1e-6;
  time3 = g_timer_elapsed(internalTimer, &fractionTimer)/1e-6;
  g_timer_destroy(readTimer);
#endif

#if DEBUG == 1
  g_timer_start(timer);
#endif
  /* Allocate the space for the nodes. */
  if (ntype <= 0)
    {
      readFree(lst, elements, (GString*)0, nodeTypes, coords, dcoord, forces, nodeComments);
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("The file contains no atom coordinates.\n"));
      return -1;
    }
  types  = g_array_sized_new(FALSE, FALSE, sizeof(VisuElement*), ntype);
  nattyp = g_array_sized_new(FALSE, FALSE, sizeof(guint), ntype);
  g_array_set_size(types, ntype);
  g_array_set_size(nattyp, ntype);
  DBG_fprintf(stderr, "Atomic xyz: transfer to VisuData (%p %p).\n",
              (gpointer)types, (gpointer)nattyp);
  g_hash_table_foreach(elements, (GHFunc)visu_meth_asciiValToType, (gpointer)types);
  DBG_fprintf(stderr, " | transfer to types OK.\n");
  g_hash_table_foreach(elements, (GHFunc)visu_meth_asciiValToNb, (gpointer)nattyp);
  DBG_fprintf(stderr, " | transfer to nNodesPerEle OK.\n");
  g_hash_table_destroy(elements);

  DBG_fprintf(stderr, " | populate nodes.\n");
  /* Begin the storage into VisuData. */
  visu_data_setNSubset(data, nSets);

  visu_node_array_allocate(VISU_NODE_ARRAY(data), types, nattyp);
  DBG_fprintf(stderr, "Atomic xyz: there are %d types in this file.\n", ntype);
  if (DEBUG)
    {
      for (i = 0; i < ntype; i++)
	DBG_fprintf(stderr, " | %d atom(s) for type %d.\n",
                    g_array_index(nattyp, guint, i), i);
    }
  natom = 0;
  for (i = 0; i < ntype; i++) natom += g_array_index(nattyp, guint, i);
  g_array_free(nattyp, TRUE);
  g_array_free(types, TRUE);

  /* Set the commentary. */
  bc = VISU_BOX_FREE;
  tmpLst = lst;
  for (i = 0; i < nSets; i++)
    {
      /* Try to see if the commentary contains some keywords. */
      if (i == nSet)
	{
	  kwd = strstr((char*)tmpLst->data, "periodic");
	  if (kwd && sscanf(kwd + 8, "%lf %lf %lf",
				    box, box + 1, box + 2) == 3)
	    bc = VISU_BOX_PERIODIC;
	  kwd = strstr((char*)tmpLst->data, "surface");
	  if (kwd && sscanf(kwd + 8, "%lf %lf %lf",
				    box, box + 1, box + 2) == 3)
	    bc = VISU_BOX_SURFACE_ZX;
	}

      visu_data_setFileCommentary(data, (gchar*)tmpLst->data, i);
      g_free(tmpLst->data);
      tmpLst = g_list_next(tmpLst);
    }
  g_list_free(lst);
  
  boxObj = (VisuBox*)0;
  if (bc != VISU_BOX_FREE)
    {
      DBG_fprintf(stderr, "Atomic xyz: the elements are in %fx%fx%f.\n",
		  box[0], box[1], box[2]);
      boxGeometry[0] = box[0];
      boxGeometry[1] = 0.;
      boxGeometry[2] = box[1];
      boxGeometry[3] = 0.;
      boxGeometry[4] = 0.;
      boxGeometry[5] = box[2];
      boxObj = visu_box_new(boxGeometry, bc);
      visu_boxed_setBox(VISU_BOXED(data), VISU_BOXED(boxObj), FALSE);
      g_object_unref(boxObj);
    }

  /* Store the coordinates */
  for(iNodes = 0; iNodes < nNodesSet; iNodes++)
    visu_data_addNodeFromElement(data, nodeTypes[iNodes],
                                 coords + 3 * iNodes, reduced, FALSE);

#ifdef TESTNEW
  if ( test_Routine(coords, dcoord, nodeTypes) == FALSE )
    return -1;
#endif

  g_free(nodeTypes);
  g_free(coords);

  if (bc != VISU_BOX_PERIODIC)
    boxObj = visu_data_setTightBox(data);

  /* create the structure for phonons */
  if (dcoord)
    {
      visu_vibration_init(data, nSets, natom);
      for (i = 0; i < nSets; i++)
	{
	  omega = 1.f;
	  pt = strstr(visu_data_getFileCommentary(data, i), "freq=");
	  if (pt)
	    sscanf(pt + 5, "%f", &omega);
	  if (omega < 0.)
	    g_warning("Negative phonon frequency (%f).", omega);
	  visu_vibration_setCharacteristic(data, i, qpt, 0.f, omega);
	}
      visu_vibration_setDisplacements(data, nSet, dcoord, FALSE);
      g_free(dcoord);
    }

  /* Store the forces, if any. */
  if (forces)
    {
      visu_rendering_atomic_setForces(data, forces);
      g_free(forces);
    }
  /* We apply the comments, if any. */
  visu_extra_node_addLabel(data);
  if (nodeComments)
    {
      for (tmpLst = nodeComments; tmpLst; tmpLst = g_list_next(tmpLst))
        {
          visu_extra_node_setLabel(data, GPOINTER_TO_INT(tmpLst->data),
                             tmpLst->next->data);
          g_free(tmpLst->next->data);
          tmpLst = g_list_next(tmpLst);
        }
      g_list_free(nodeComments);
    }
  /* Add some other meta data. */
  if (totalEnergy != G_MAXFLOAT)
    g_object_set(G_OBJECT(data), "totalEnergy", totalEnergy, NULL);

  DBG_fprintf(stderr, "Atomic XYZ: apply the box geometry and set the unit.\n");
  visu_box_setMargin(boxObj, visu_node_array_getMaxElementSize(VISU_NODE_ARRAY(data)) +
                     visu_data_getAllNodeExtens(data, boxObj), TRUE);
  visu_box_setUnit(boxObj, unit);

#if DEBUG == 1
  g_timer_stop(timer);
  fprintf(stderr, "Atomic XYZ: parse all file    in %g micro-s.\n", time1);
  fprintf(stderr, "Atomic XYZ: parse coordinates in %g micro-s.\n", time2);
  fprintf(stderr, "Atomic XYZ: header parse      in %g micro-s.\n", time3);
  fprintf(stderr, "Atomic XYZ: set all data      in %g micro-s.\n",
	  g_timer_elapsed(timer, &fractionTimer)/1e-6);
  g_timer_destroy(timer);
#endif

  return 0;
}

/******************************************************************************/
#ifdef TESTNEW
static gboolean test_Routine(float* coords, float* dcoord, VisuElement **nodeTypes) {

  float xyz[15] = {-0.440035, -0.000385, 2.123698,
    -1.765945, 0.000399, 2.377542,
    -2.249233, -0.001453, 3.679971,
    -1.338875, -0.004508, 4.739569, 
    0.024627, -0.005918, 4.466144};
  float dxyz[15] = {0.001000, -0.151000, -0.002000,
    0.001000, -0.175000, 0.000000,
    0.003000, -0.198000, 0.001000,
    0.005000, -0.183000, -0.000000,
    0.005000, -0.146000, -0.003000};
  char* waitedType[5]={ "N", "C", "C", "Co", "C"};
  int i=0, j=0;

/* Checking coordonates values and type values*/
  DBG_fprintf(stderr, "+---------------------------------------------------------------+\n");
 /* for each node : checking names and coordonates values using a difference.*/
  while( (i<15) && (strcmp(nodeTypes[j]->name,waitedType[j])==0) && (ABS(coords[i]-xyz[i])<1e-6) && (ABS(dcoord[i]-dxyz[i])<1e-6)  )
   {
    if (i%3==0) {
      DBG_fprintf(stderr, "xyz parser : expected element: %s, found: %s \n", waitedType[j], nodeTypes[j]->name);	
      DBG_fprintf(stderr, "xyz parser : expected x: %f, found: %f \t", xyz[i], coords[i]);
      DBG_fprintf(stderr, "xyz parser : expected dx: %f, found: %f \n", dxyz[i], dcoord[i]);
    }
    if (i%3==1) {
      DBG_fprintf(stderr, "xyz parser : expected y: %f, found: %f \t", xyz[i], coords[i]);
      DBG_fprintf(stderr, "xyz parser : expected dy: %f, found: %f \n", dxyz[i], dcoord[i]);
    }	
    if (i%3==2) {
      DBG_fprintf(stderr, "xyz parser : expected z: %f, found: %f \t", xyz[i], coords[i]);
      DBG_fprintf(stderr, "xyz parser : expected dz: %f, found: %f \n", dxyz[i], dcoord[i]);
      j++;
      DBG_fprintf(stderr, "+---------------------------------------------------------------+\n");
    }		
    i++;
   }

  if (i!=15)  
   {
    DBG_fprintf(stderr, "xyz parser : An error occured while reading the test file : node number %d encoutred an error \n", j+1);
    return FALSE;
   }
  else
   {
   DBG_fprintf(stderr, "xyz parser : parser ok ! \n");
    return TRUE;
   }
}
#endif
