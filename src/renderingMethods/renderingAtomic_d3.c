/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "renderingAtomic_d3.h"
#include "renderingAtomic.h"
#include <visu_rendering.h>
#include <visu_data.h>
#include <coreTools/toolFortran.h>

/**
 * SECTION:renderingAtomic_d3
 * @short_description: Method to load d3 position file.
 *
 * <para>D3 format is a binary format to store elements and their
 * position.</para>
 */

static gboolean read_nattyp_natnom(FILE *flux, GError **error,
				   ToolFortranEndianId endian, gboolean store);
static gboolean read_npas_temps(FILE *flux, GError **error, ToolFortranEndianId endian);
static int free_read_d3();
static int read_d3_file(VisuData *data, FILE *flux, int set, GError **error);

static char title[129];
static guint nat, ntypeD3, npas;
static gdouble energy;
static GArray *nattyp;
static char **natnom;
static double *x, *y, *z;
static float *xf, *yf, *zf;
static GList *lst;


static gboolean loadD3(VisuData *data, const gchar* filename,
		       ToolFileFormat *format, int nSet,
                       GCancellable *cancel, GError **error);

/******************************************************************************/

/**
 * initAtomicD3: (skip)
 * @method: the #VisuRendering method to be associated to.
 *
 * Create the structure that gives access to a load method and
 * some description of it (file formats associated, name...). Internal
 * use only
 */
void initAtomicD3(VisuRendering *method)
{
  const gchar *typeD3[] = {"*.d3", "*-posi.d3", "*.d3-posi", (char*)0};
  
  visu_rendering_addFileFormat(method, 0,
                               tool_file_format_new(_("Native binary format"),
                                                    typeD3),
                               10, loadD3);
}

/******************************************************************************/

static gboolean read_nattyp_natnom(FILE *flux, GError **error,
				   ToolFortranEndianId endian, gboolean store)
{
  guint k;
  gboolean valid;
  guint ncheck, mcheck, *buf;
  gchar *name;

  g_return_val_if_fail((store && nattyp && natnom) || !store, FALSE);

  DBG_fprintf(stderr, "Atomic D3: read nattyp and nattom (%d).\n", store);
  valid = tool_fortran_readFlag(&ncheck, flux, error, endian);
  if (!valid) return valid;
  buf = (store)?(guint*)nattyp->data:(guint*)0;
  valid = tool_fortran_readInteger(buf, ntypeD3, flux, error,
				  endian, FALSE, store);
  if (!valid) return valid;
  if (store)
    nattyp = g_array_set_size(nattyp, ntypeD3);
  for(k = 0; k < ntypeD3; k++)
    {
      name = (store)?natnom[k]:(gchar*)0;
      valid = tool_fortran_readCharacter(name, 8, flux, error,
					endian, FALSE, store);
      if (!valid) return valid;
      if (store)
	{
	  natnom[k][8] = '\0';
	  g_strchomp(natnom[k]);
	}
    }
  valid = tool_fortran_readFlag(&mcheck, flux, error, endian);
  if (!valid) return valid;
  if (ncheck != mcheck)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("WARNING : wrong fortran syntax, flag size unmatched.\n"));
      return FALSE;
    }
  return TRUE;
}
static gboolean read_npas_temps(FILE *flux, GError **error, ToolFortranEndianId endian)
{
  guint ncheck, mcheck;
  gboolean valid;
  double temps;
  
  valid = tool_fortran_readFlag(&ncheck, flux, error, endian);
  if (!valid) return valid;
  valid = tool_fortran_readInteger(&npas, 1, flux, error, endian, FALSE, TRUE);
  if (!valid) return valid;
  valid = tool_fortran_readDouble(&temps, 1, flux, error, endian, FALSE, TRUE);
  if (!valid) return valid;
  valid = tool_fortran_readFlag(&mcheck, flux, error, endian);
  if (!valid) return valid;
  if (ncheck != mcheck)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("WARNING : wrong fortran syntax, flag size unmatched.\n"));
      return FALSE;
    }
  return TRUE;
}
static gboolean read_npas_temps_energy(FILE *flux, GError **error, ToolFortranEndianId endian)
{
  guint ncheck, mcheck;
  gboolean valid;
  double temps;
  
  valid = tool_fortran_readFlag(&ncheck, flux, error, endian);
  if (!valid) return valid;
  valid = tool_fortran_readInteger(&npas, 1, flux, error, endian, FALSE, TRUE);
  if (!valid) return valid;
  valid = tool_fortran_readDouble(&temps, 1, flux, error, endian, FALSE, TRUE);
  if (!valid) return valid;
  valid = tool_fortran_readDouble(&energy, 1, flux, error, endian, FALSE, TRUE);
  if (!valid) return valid;
  valid = tool_fortran_readFlag(&mcheck, flux, error, endian);
  if (!valid) return valid;
  if (ncheck != mcheck)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("WARNING : wrong fortran syntax, flag size unmatched.\n"));
      return FALSE;
    }
  return TRUE;
}
static gboolean read_unit(FILE *flux, GError **error, ToolFortranEndianId endian,
                          ToolUnits *unit)
{
  gboolean valid;
  gchar units[17];

  DBG_fprintf(stderr, "Atomic D3 : read units.\n");
  valid = tool_fortran_readCharacter(units, 16, flux, error,
                                     endian, TRUE, TRUE);
  if (!valid) return valid;
  units[16] = '\0';
  g_strchomp(units);
  *unit = tool_physic_getUnitFromName(units);
  DBG_fprintf(stderr, " | '%s' (%d)\n", units, *unit);
  
  return TRUE;
}
static gboolean read_periodicity(FILE *flux, GError **error, ToolFortranEndianId endian,
                                 VisuBoxBoundaries *bc)
{
  gboolean valid;
  gchar periodicity[5];

  DBG_fprintf(stderr, "Atomic D3 : read periodicity.\n");
  valid = tool_fortran_readCharacter(periodicity, 4, flux, error,
                                     endian, TRUE, TRUE);
  if (!valid) return valid;
  periodicity[4] = '\0';
  g_strchomp(periodicity);
  DBG_fprintf(stderr, " | '%s'\n", periodicity);
  if (!g_ascii_strcasecmp(periodicity, "Oxyz"))
    *bc = VISU_BOX_PERIODIC;
  else if (!g_ascii_strcasecmp(periodicity, "Ozx"))
    *bc = VISU_BOX_SURFACE_ZX;
  else if (!g_ascii_strcasecmp(periodicity, "Oxy"))
    *bc = VISU_BOX_SURFACE_XY;
  else if (!g_ascii_strcasecmp(periodicity, "Oyz"))
    *bc = VISU_BOX_SURFACE_YZ;
  else if (!g_ascii_strcasecmp(periodicity, "Ox"))
    *bc = VISU_BOX_WIRE_X;
  else if (!g_ascii_strcasecmp(periodicity, "Oy"))
    *bc = VISU_BOX_WIRE_Y;
  else if (!g_ascii_strcasecmp(periodicity, "Oz"))
    *bc = VISU_BOX_WIRE_Z;
  else if (!g_ascii_strcasecmp(periodicity, "O"))
    *bc = VISU_BOX_FREE;
  else
    *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
                         _("unknwon peridicity flag '%s'"), periodicity);
  return (*error == (GError*)0);
}

/******************************************************************************/

static gboolean loadD3(VisuData *data, const gchar* filename,
		       ToolFileFormat *format _U_, int nSet,
                       GCancellable *cancel _U_, GError **error)
{
  int res;
  FILE *readFrom;

  g_return_val_if_fail(error && *error == (GError*)0, FALSE);
  g_return_val_if_fail(data && filename, FALSE);

  readFrom = fopen(filename, "rb");
  if (!readFrom)
    {
      *error = g_error_new(G_FILE_ERROR, g_file_error_from_errno(errno),
			   _("Impossible to open this file."));
      return FALSE;
    }

  res = read_d3_file(data, readFrom, nSet, error);
  fclose(readFrom);

  if (res < 0)
    /* The file is not a D3 file. */
    return FALSE;
  else if (res > 0)
    /* The file is a D3 file but some errors occured. */
    return TRUE;
  /* Everything is OK. */
  *error = (GError*)0;
  return TRUE;
}

static int read_d3_file(VisuData *data, FILE *flux, int set, GError **error)
{
  int iSet;
  guint snat;
  VisuElement *type;
  GArray *types;
  gchar *infoUTF8;
  gboolean valid, store, readDouble, storeDouble;
  ToolFortranEndianId endian;
  guint nn[2], i, j, ncheck;
  double boxGeometry[6];
  float xyz[3];
  long offset;
  GList *tmpLst;
  VisuBox *box;
  ToolUnits unit;
  VisuBoxBoundaries bc;

  g_return_val_if_fail(error && *error == (GError*)0, FALSE);

  natnom = (gchar**)0;
  nattyp = (GArray*)0;
  x = (double*)0;
  y = (double*)0;
  z = (double*)0;
  xf = (float*)0;
  yf = (float*)0;
  zf = (float*)0;
  iSet = 0;
  storeDouble = TRUE;
  unit = TOOL_UNITS_UNDEFINED;
  bc = VISU_BOX_PERIODIC;

  /* Check the first element to find the endianness.
     The first element is an int equal to 128. */
  DBG_fprintf(stderr, "Atomic D3 : test for endianness.\n");
  valid = tool_fortran_testEndianness(128, flux, error, &endian);
  if (!valid)
    return -1;

  /* We read the set number @set and store the number of sets in
     nSet. */
  lst = (GList*)0;
  do
    {
      store = (iSet == set);

      /* TITLE */
      DBG_fprintf(stderr, "Atomic D3 : read title.\n");
      valid = tool_fortran_readCharacter(title, 128, flux, error, endian, TRUE, TRUE);
      if (!valid)
	return free_read_d3();
      title[128] = '\0';
      g_strchomp(title);
      DBG_fprintf(stderr, " | '%s'\n", title);

      /* NPAS-TEMPS(-ENERGY) (optional) */
      offset = ftell(flux);
      energy = G_MAXDOUBLE;
      valid = read_npas_temps(flux, error, endian);
      if (!valid)
	{
	  g_error_free(*error);
          *error = (GError*)0;
	  fseek(flux, offset, SEEK_SET);
          valid = read_npas_temps_energy(flux, error, endian);
        }
      if (!valid)
	{
	  g_error_free(*error);
          *error = (GError*)0;
	  fseek(flux, offset, SEEK_SET);
	  readDouble = TRUE;
	  lst = g_list_prepend(lst, g_strdup(title));
	}
      else
	{
	  readDouble = FALSE;
          if (store && energy != G_MAXDOUBLE)
            g_object_set(G_OBJECT(data), "totalEnergy", energy, NULL);
	  lst = g_list_prepend(lst, g_strdup_printf("%04d - %s", npas, title));
	}

      /* NAT-NTYPE */  
      DBG_fprintf(stderr, "Atomic D3 : read nat and ntype.\n");
      valid = tool_fortran_readInteger(nn, 2, flux, error, endian, TRUE, TRUE);
      if (!valid)
	return free_read_d3();
      nat = nn[0];
      ntypeD3 = nn[1];
      DBG_fprintf(stderr, " | %d %d\n", nat, ntypeD3);
               
      /* NATI-NOM */
      if (store)
	{
          DBG_fprintf(stderr, " | store nattyp and natom.\n");
          nattyp = g_array_sized_new(FALSE, FALSE, sizeof(guint), ntypeD3);
	  natnom = g_malloc(ntypeD3 * sizeof(char *));
	  for(i=0; i<ntypeD3; i++)
	    natnom[i] = g_malloc(9 * sizeof(char));
	}
      valid = read_nattyp_natnom(flux, error, endian, store);
      if(!valid)
	{
	  DBG_fprintf(stderr, "! invalid nattyp or natnom.\n");
	  return free_read_d3();
	}
      else
        DBG_fprintf(stderr, " | read nattyp natom OK.\n");
      if (store)
	{
	  DBG_fprintf(stderr, " | check coherence in number of atoms.\n");
	  snat = 0;
	  for(i=0; i<ntypeD3; i++)
	    snat = snat + g_array_index(nattyp, guint, i);
	  if(snat != nat)
	    {
	      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
				   _("wrong d3 syntax,"
				     " 'sum of nattyp's not equal to nat'\n"));
	      return free_read_d3();
	    }
	}
         
      /* DXX-DYX-DYY-DZX-DZY-DZZ */
      valid = tool_fortran_readDouble(boxGeometry, 6, flux, error, endian, TRUE, store);
      if (!valid)
	{
	  DBG_fprintf(stderr, "! invalid box '%s'.\n", (*error)->message);
	  return free_read_d3();
	}

      if (store)
	{
	  if (readDouble)
	    {
	      x = g_malloc(sizeof(double) * nat);
	      y = g_malloc(sizeof(double) * nat);
	      z = g_malloc(sizeof(double) * nat);
	    }
	  else
	    {
	      xf = g_malloc(sizeof(float) * nat);
	      yf = g_malloc(sizeof(float) * nat);
	      zf = g_malloc(sizeof(float) * nat);
	    }
	  storeDouble = readDouble;
	}
   
      /* X */
      if (readDouble)
	valid = tool_fortran_readDouble(x, nat, flux, error, endian, TRUE, store);
      else
	valid = tool_fortran_readReal(xf, nat, flux, error, endian, TRUE, store);
      if (!valid)
	{
	  DBG_fprintf(stderr, "! invalid x coordinates '%s'.\n", (*error)->message);
	  return free_read_d3();
	}
   
      /* Y */      
      if (readDouble)
	valid = tool_fortran_readDouble(y, nat, flux, error, endian, TRUE, store);
      else
	valid = tool_fortran_readReal(yf, nat, flux, error, endian, TRUE, store);
      if (!valid)
	{
	  DBG_fprintf(stderr, "! invalid y coordinates '%s'.\n", (*error)->message);
	  return free_read_d3();
	}
   
      /* Z */      
      if (readDouble)
	valid = tool_fortran_readDouble(z, nat, flux, error, endian, TRUE, store);
      else
	valid = tool_fortran_readReal(zf, nat, flux, error, endian, TRUE, store);
      if (!valid)
	{
	  DBG_fprintf(stderr, "! invalid z coordinates '%s'.\n", (*error)->message);
	  return free_read_d3();
	}

      iSet += 1;

      /* Go to the next fortran flag holding 128. */
      do
        {
          /* There is a strange thing with Fortran file, they
             seems to finish with '0x0a' character... */
          offset = ftell(flux);
          valid = tool_fortran_readFlag(&ncheck, flux, error, endian);
          if (!valid)
            g_error_free(*error);
          else if (ncheck == 16)
            {
              fseek(flux, offset, SEEK_SET);
              /* Special case of units. */
              if (!read_unit(flux, error, endian, &unit))
                return free_read_d3();
            }
          else if (ncheck == 4)
            {
              fseek(flux, offset, SEEK_SET);
              /* Special case of periodicity. */
              if (!read_periodicity(flux, error, endian, &bc))
                return free_read_d3();
            }
          else if (ncheck != 128)
            fseek(flux, offset + 4 + ncheck + 4, SEEK_SET);
          else
            fseek(flux, offset, SEEK_SET);
          valid = valid && (ncheck == 128);
        }
      while (!feof(flux) && !valid);
    }
  while (!feof(flux) && valid);
  lst = g_list_reverse(lst);

  if(iSet < set)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("input file has no dataset number %d"
			     " (%d have been read).\n"), set + 1, iSet);
      return free_read_d3();
    }

  DBG_fprintf(stderr, "Atomic d3 : nat = %d | ntypeD3 = %d\n", nat, ntypeD3);
  for(i = 0; i < ntypeD3; i++)
    DBG_fprintf(stderr, " | natnom %d = '%s', nb %d\n", i, natnom[i],
                g_array_index(nattyp, guint, i));
  /* Now we have all the element and the number of atoms per element.
     So we allocate the memory. */
  types = g_array_sized_new(FALSE, FALSE, sizeof(VisuElement*), ntypeD3);
  for(i = 0; i < ntypeD3; i++)
    {
      /* adding nomloc to the hashtable */
      type = visu_element_retrieveFromName(natnom[i], (gboolean*)0);
      if (!type)
	{
          g_warning("Cannot create a new type for '%s'\n", natnom[i]);
          return 1;
	}
      g_array_insert_val(types, i, type);
    }

  DBG_fprintf(stderr, " | begin to transfer data to VisuData.\n");
  /* Begin the storage into VisuData. */
  visu_data_setNSubset(data, iSet);

  /* Set the commentaries. */
  iSet = 0;
  tmpLst = lst;
  while (tmpLst)
    {
      infoUTF8 = g_locale_to_utf8((gchar*)tmpLst->data, -1, NULL, NULL, NULL);
      if (infoUTF8)
	{
	  visu_data_setFileCommentary(data, infoUTF8, iSet);
	  g_free(infoUTF8);
	}
      else
	g_warning("Can't convert '%s' to UTF8.\n", (gchar*)tmpLst->data);
      iSet += 1;
      tmpLst = g_list_next(tmpLst);
    }

  visu_node_array_allocate(VISU_NODE_ARRAY(data), types, nattyp);
  g_array_free(types, TRUE);

  nat = 0;
  for (i = 0; i < ntypeD3; i++)
    {
      for (j = 0; j < g_array_index(nattyp, guint, i); j++)
	{
	  xyz[0] = (storeDouble)?(float)x[nat]:xf[nat];
	  xyz[1] = (storeDouble)?(float)y[nat]:yf[nat];
	  xyz[2] = (storeDouble)?(float)z[nat]:zf[nat];
	  visu_data_addNodeFromIndex(data, i, xyz, FALSE, FALSE);
	  nat++;
	}
    }

  /* We finish with the geometry. */
  box = visu_box_new(boxGeometry, bc);
  visu_box_setMargin(box, visu_node_array_getMaxElementSize(VISU_NODE_ARRAY(data)) +
                     visu_data_getAllNodeExtens(data, box), TRUE);
  visu_box_setUnit(box, unit);
  visu_boxed_setBox(VISU_BOXED(data), VISU_BOXED(box), FALSE);
  g_object_unref(box);
  
  free_read_d3();
   
  return 0;
}

static int free_read_d3()
{
  guint i;
  GList *tmpLst;
  
  DBG_fprintf(stderr, "Atomic d3: freeing tmp variables.\n");
  if (natnom)
    {
      for(i = 0; i < ntypeD3; i++)
	if (natnom[i])
	  g_free(natnom[i]);
      g_free(natnom);
    }
  if(nattyp)
    g_array_free(nattyp, TRUE);
  if (x)
    g_free(x);
  if (y)
    g_free(y);
  if (z)
    g_free(z);
  if (xf)
    g_free(xf);
  if (yf)
    g_free(yf);
  if (zf)
    g_free(zf);
  if (lst)
    {
      tmpLst = lst;
      while (tmpLst)
	{
	  g_free(tmpLst->data);
	  tmpLst = g_list_next(tmpLst);
	}
      g_list_free(lst);
    }
  return 1;
}

/******************************************************************************/ 
/******************************************************************************/
