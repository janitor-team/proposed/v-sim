/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresses m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef RENDERINGSPIN_H
#define RENDERINGSPIN_H

#include <visu_rendering.h>
#include <visu_data.h>
#include <visu_object.h>
#include <visu_tools.h>

/**
 * VisuRenderingSpinDrawingPolicy:
 * @VISU_RENDERING_SPIN_ALWAYS: Arrows are drawn whatever the modulus value.
 * @VISU_RENDERING_SPIN_HIDE_NULL: Spin with a null modulus are hidden.
 * @VISU_RENDERING_SPIN_ATOMIC_NULL: Follow atomic rendering for null modulus.
 * @VISU_RENDERING_SPIN_N_MODES: a flag to count the number of modes.
 *
 * Different policy to render the spin when the modulus is null. This
 * policy is applied for all #VisuElement.
 */
typedef enum
  {
    VISU_RENDERING_SPIN_ALWAYS,    
    VISU_RENDERING_SPIN_HIDE_NULL,  
    VISU_RENDERING_SPIN_ATOMIC_NULL,
    VISU_RENDERING_SPIN_N_MODES
  } VisuRenderingSpinDrawingPolicy;
/**
 * VisuRenderingSpinModulusPolicy:
 * @VISU_RENDERING_SPIN_CONSTANT: arrows have all the same size, whatever the
 * modulus value.
 * @VISU_RENDERING_SPIN_PER_TYPE: arrows are scaled per node type.
 * @VISU_RENDERING_SPIN_GLOBAL: arrows are scaled globaly.
 * @VISU_RENDERING_SPIN_N_MODULUS_MODES: a flag to count the number of modes.
 *
 * Different policy to render the spin depending on the modulus.
 */
typedef enum
  {
    VISU_RENDERING_SPIN_CONSTANT,    
    VISU_RENDERING_SPIN_PER_TYPE,  
    VISU_RENDERING_SPIN_GLOBAL,
    VISU_RENDERING_SPIN_N_MODULUS_MODES
  } VisuRenderingSpinModulusPolicy;

/**
 * VisuRenderingSpinShapeId:
 * @VISU_RENDERING_SPIN_ARROW_SMOOTH: the shape is smooth and rounded ;
 * @VISU_RENDERING_SPIN_ARROW_SHARP: the shape is built on squares ;
 * @VISU_RENDERING_SPIN_ELLIPSOID: the shape is an ellipsoid ;
 * @VISU_RENDERING_SPIN_TORUS: the shape is a torus (direction of the arrow is
 * normal to the torus plane).
 * @VISU_RENDERING_SPIN_N_SHAPES: private.
 *
 * An identifier for the different shapes to draw elements.
 */
typedef enum
  {
    VISU_RENDERING_SPIN_ARROW_SMOOTH,
    VISU_RENDERING_SPIN_ARROW_SHARP,
    VISU_RENDERING_SPIN_ELLIPSOID,
    VISU_RENDERING_SPIN_TORUS,
    /*< private >*/
    VISU_RENDERING_SPIN_N_SHAPES
  } VisuRenderingSpinShapeId;

/**
 * VisuRenderingSpinFileType:
 * @FILE_KIND_POSITION: define a position file ;
 * @FILE_KIND_SPIN: define a spin description file.
 *
 * These are keys for the storing of spin files in a VisuData object. 
 */
typedef enum
  {
    FILE_KIND_POSITION,
    FILE_KIND_SPIN
  } VisuRenderingSpinFileType;

/**
 * VISU_RENDERING_SPIN_VALUES_ID:
 *
 * This flag should be used when creating a new spin rendering method
 * as the flag for the spin values as node properties (see
 * visu_node_array_property_newPointer()).
 */
#define VISU_RENDERING_SPIN_VALUES_ID     "spinRendering_values"
/**
 * VISU_RENDERING_SPIN_MAX_MODULUS_ID:
 *
 * This flag should be used when creating a new spin rendering method
 * as the flag for the max modulus values as a #VisuNode property (see
 * visu_node_array_property_newPointer()).
 */
#define VISU_RENDERING_SPIN_MAX_MODULUS_ID "spinRendering_maxModulus"

/**
 * VISU_RENDERING_SPIN_NAME:
 *
 * Public name of the spin rendering mode.
 */
#define VISU_RENDERING_SPIN_NAME "Spin visualisation"

/**
 * VISU_TYPE_RENDERING_SPIN:
 *
 * return the type of #VisuRenderingSpin.
 */
#define VISU_TYPE_RENDERING_SPIN	     (visu_rendering_spin_get_type ())
/**
 * VISU_RENDERING_SPIN:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuRenderingSpin type.
 */
#define VISU_RENDERING_SPIN(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_RENDERING_SPIN, VisuRenderingSpin))
/**
 * VISU_RENDERING_SPIN_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuRenderingSpinClass.
 */
#define VISU_RENDERING_SPIN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_RENDERING_SPIN, VisuRenderingSpinClass))
/**
 * VISU_IS_RENDERING_SPIN:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuRenderingSpin object.
 */
#define VISU_IS_RENDERING_SPIN(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_RENDERING_SPIN))
/**
 * VISU_IS_RENDERING_SPIN_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuRenderingSpinClass class.
 */
#define VISU_IS_RENDERING_SPIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_RENDERING_SPIN))
/**
 * VISU_RENDERING_SPIN_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_RENDERING_SPIN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_RENDERING_SPIN, VisuRenderingSpinClass))

/**
 * visu_rendering_spin_get_type:
 *
 * This method returns the type of #VisuRenderingSpin, use
 * VISU_TYPE_RENDERING_SPIN instead.
 *
 * Returns: the type of #VisuRenderingSpin.
 */
GType visu_rendering_spin_get_type(void);

/**
 * VisuRenderingSpinPrivate:
 *
 * An opaque structure.
 */
typedef struct _VisuRenderingSpinPrivate VisuRenderingSpinPrivate;
typedef struct _VisuRenderingSpinClass VisuRenderingSpinClass;
/**
 * VisuRenderingSpin:
 *
 * An opaque structure.
 */
struct _VisuRenderingSpin
{
  VisuRendering method;

  VisuRenderingSpinPrivate *priv;
};
typedef struct _VisuRenderingSpin VisuRenderingSpin;

VisuRenderingSpin* visu_rendering_spin_new();

/***********************/
/* Different resources */
/***********************/
const char* visu_rendering_spin_getShapeNameI18n(VisuRenderingSpinShapeId n);
VisuRenderingSpinDrawingPolicy visu_rendering_spin_getHidingPolicyFromName(const char *name);
const char* visu_rendering_spin_getHidingPolicyName(VisuRenderingSpinDrawingPolicy n);
const char* visu_rendering_spin_getHidingPolicyNameI18n(VisuRenderingSpinDrawingPolicy n);

/**
 * VisuRenderingSpinResources:
 * @VISU_RENDERING_SPIN_HAT_LENGTH: the length of the pointing element ;
 * @spin_VISU_RENDERING_SPIN_TAIL_LENGTH: the length of the tail ;
 * @VISU_RENDERING_SPIN_HAT_RADIUS: the raidus of the pointing element ;
 * @VISU_RENDERING_SPIN_TAIL_RADIUS: the radius of the tail ;
 * @VISU_RENDERING_SPIN_HAT_COLOR: if TRUE, the pointing part use the color of the element ;
 * @VISU_RENDERING_SPIN_TAIL_COLOR: if TRUE, the tail uses the color of the element ;
 * @VISU_RENDERING_SPIN_A_AXIS: the size of the A axis (elipsoid shape) ;
 * @VISU_RENDERING_SPIN_B_AXIS: the size of the B axis (elipsoid shape) ;
 * @VISU_RENDERING_SPIN_ELIPSOID_COLOR: if TRUE, the elipsoid uses the color of the element ;
 * @VISU_RENDERING_SPIN_SHAPE: an id to defined the shape (rounded arrow, elipsoid...) ;
 * @VISU_RENDERING_SPIN_N_RESOURCES: number of resources per element.
 * 
 * These are resources defined for each element. They can be accessed with
 * visu_rendering_spin_getResource() or visu_rendering_spin_getResourceBoolean() and other
 * methods of the same kind.
 */
typedef enum
  {
    VISU_RENDERING_SPIN_HAT_LENGTH,
    spin_VISU_RENDERING_SPIN_TAIL_LENGTH,
    VISU_RENDERING_SPIN_HAT_RADIUS,
    VISU_RENDERING_SPIN_TAIL_RADIUS,
    VISU_RENDERING_SPIN_HAT_COLOR,
    VISU_RENDERING_SPIN_TAIL_COLOR,
    VISU_RENDERING_SPIN_A_AXIS,
    VISU_RENDERING_SPIN_B_AXIS,
    VISU_RENDERING_SPIN_ELIPSOID_COLOR,
    VISU_RENDERING_SPIN_SHAPE,
    VISU_RENDERING_SPIN_N_RESOURCES
  } VisuRenderingSpinResources;

void visu_rendering_spin_getResource(VisuElement *ele,
                                     VisuRenderingSpinResources property,
                                     GValue *val);
gboolean visu_rendering_spin_setResource(VisuElement *ele,
                                         VisuRenderingSpinResources property,
                                         GValue *val);
/**
 * visu_rendering_spin_setResourceBoolean:
 * @ele: a pointer to a #VisuElement object ;
 * @property: the id of the property to set ;
 * @value: its value.
 *
 * This method is used to change element resources that are boolean.
 *
 * Returns: TRUE if the value was changed.
 */
gboolean visu_rendering_spin_setResourceBoolean(VisuElement *ele, VisuRenderingSpinResources property,
					  gboolean value);
/**
 * visu_rendering_spin_setResourceUint:
 * @ele: a pointer to a #VisuElement object ;
 * @property: the id of the property to set ;
 * @value: its value.
 *
 * This method is used to change element resources that are guint.
 *
 * Returns: TRUE if the value was changed.
 */
gboolean visu_rendering_spin_setResourceUint(VisuElement *ele, VisuRenderingSpinResources property,
				       guint value);
/**
 * visu_rendering_spin_setResourceFloat:
 * @ele: a pointer to a #VisuElement object ;
 * @property: the id of the property to set ;
 * @value: its value.
 *
 * This method is used to change element resources that are floating point.
 *
 * Returns: TRUE if the value was changed.
 */
gboolean visu_rendering_spin_setResourceFloat(VisuElement *ele, VisuRenderingSpinResources property,
					gfloat value);
/**
 * visu_rendering_spin_getResourceBoolean:
 * @ele: a pointer to a #VisuElement object ;
 * @property: the id of the property to get.
 *
 * This is the specific method to retrieve value of boolean element resources.
 *
 * Returns: the boolean value.
 */
gboolean visu_rendering_spin_getResourceBoolean(VisuElement *ele, VisuRenderingSpinResources property);
/**
 * visu_rendering_spin_getResourceUint:
 * @ele: a pointer to a #VisuElement object ;
 * @property: the id of the property to get.
 *
 * This is the specific method to retrieve value of guint element resources.
 *
 * Returns: the guint value.
 */
guint visu_rendering_spin_getResourceUint(VisuElement *ele, VisuRenderingSpinResources property);
/**
 * visu_rendering_spin_getResourceFloat:
 * @ele: a pointer to a #VisuElement object ;
 * @property: the id of the property to get.
 *
 * This is the specific method to retrieve value of floating point element resources.
 *
 * Returns: the floating point value.
 */
gfloat visu_rendering_spin_getResourceFloat(VisuElement *ele, VisuRenderingSpinResources property);

#endif
