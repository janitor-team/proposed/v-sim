/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use,
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info".

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "visu_configFile.h"

#include "visu_tools.h"
#include "visu_basic.h"
#include "visu_object.h"
#include "coreTools/toolConfigFile.h"

#include <stdlib.h>
#include <string.h>
#include <unistd.h> /* For the access markers R_OK, W_OK ... */

/**
 * SECTION:visu_configFile
 * @short_description: Defines methods to access (read/write) to
 * config files and to create different entries.
 *
 * <para>V_Sim uses two different configuration files. The first
 * stores the configuration of the program itself and is called
 * parameters file. The second stores differents values that control
 * the way files are rendered. It is called resources file. For
 * example, their is an entry in the parameters file that controls
 * your favorite rendering method ; and there is an entry in the
 * resources file that codes that vacancy elements are rendered by
 * cube in the atomic rendering method. Most methods of this part uses
 * a first argument usually called 'kind', that control if the method
 * will apply on the parameters file or on the resources
 * file. #VISU_CONFIG_FILE_PARAMETER and #VISU_CONFIG_FILE_RESOURCE are
 * the two flags that should be used for the 'kind' argument.</para>
 *
 * <para>There are different paths where these files can be
 * stored. These paths are stored in V_Sim with an order : for example
 * parameters file are first looked for in the current working
 * directory, then in the $HOME/.v_sim directory and finally in the
 * installation directory. This is transparent for the user and
 * visu_config_file_getValidPath() is the right method to access to the
 * best readable configuration file.</para>
 *
 * <para>Different part of V_Sim can add entries in these files. The
 * method visu_config_file_addEntry() is designed to this purpose. The
 * entries are defined by their name and they appear in the
 * configuration file as 'name:' followed by the data associated to
 * this entry. In the parameters file, the data are on the same
 * line. In the resources file, the data begin the line after and can
 * be longer that one line. When a configuration file is read, the
 * method associated to each entry (VisuConfigFileReadFunc()) is
 * called with a copy of their data lines. The method
 * visu_config_file_addExportFunction() should be used to add a callback
 * when the configurations files are written, then each part of V_Sim
 * that have entries can put some lines in the configuration
 * files.</para>
 */

#define PARAMETER_HEADER     "#V_Sim parameters file"
#define RESOURCE_HEADER      "#V_Sim resources file"
#define VERSION_HEADER       "3.0"

static const gchar *RESOURCES_FILENAMES[] = {"v_sim.res.xml", "v_sim.res", (gchar*)0};
static const gchar *PARAMETERS_FILENAMES[] = {"v_sim.par", (gchar*)0};

#define FLAG_RESOURCES_PATH "main_resourcesPath"
#define DESC_RESOURCES_PATH "Favorite paths to find and save the resources file ; chain[:chain]"
#define DEFAULT_RESOURCES_PATH ""
static gboolean readResourcesPaths(VisuConfigFileEntry *entry, gchar **lines, int nbLines, int position,
				   VisuData *dataObj, VisuGlView *view, GError **error);
static void exportResourcesPaths(GString *data, VisuData *dataObj, VisuGlView *view);

/**
 * VisuConfigFileEntry:
 *
 * This is the common name of the structure.
 */
struct _VisuConfigFileEntry
{
  /* Name of the key. */
  gchar *key;
  gchar *description;
  GQuark kquark; /* Quark associated to the key. */
  /* Version, default is 3.0. */
  float version;
  /* If set, entry is obsolete and newKey should replaces it. */
  gchar *newKey;

  /* A parameter or a resource */
  int kind;

  /* Number of line used by this resources.
     This is not used if the entry is a parameter
     since, parameters are on the same line than
     the key and are one line. */
  guint nbLines;

  /* This method is called when a file is read
     and the entry is found. */
  VisuConfigFileReadFunc read;
  gpointer storage;
  guint nValues;
  float range[2];

  /* Tag, tags are used to ignore or not some
     entries when a file is read. */
  gchar *tag;
};

typedef enum
  {
    _format_raw,
    _format_xml
  } _format_export;
_format_export format = _format_raw;
struct writeFunc_struct
{
  VisuConfigFileExportFunc writeFunc;
};

/* This hashtable stores all the known entries.
   The keys are the name of the entry (its key), and
   the value is a pointer to a VisuConfigFileEntry. */
static GHashTable *visuConfigFile_entryList = NULL;
static GList *registeredResources = NULL, *registeredParameters = NULL;
static GList *exportResourcesList, *exportParametersList;
static GHashTable *knownTags = NULL;

/* Store the paths to where it is possible to store
   and/or read resource files. This list is ordered
   and first element is the most prefered path. */
static GList *resourcesPath;
static gchar *currentResPath;
/* Store the paths to where it is possible to store
   parameters files. This list is ordered and first element is
   the most prefered path.*/
static GList *parametersPath;

/* Local methods. */
static void visuConfigFileInit();
static VisuConfigFileEntry* entry_copy(VisuConfigFileEntry *entry);
static void entry_free(VisuConfigFileEntry *entry);

/* Generic reading routines. */
static gboolean _readBoolean(VisuConfigFileEntry *entry, gchar **lines, int nbLines,
                             int position, VisuData *dataObj,
                             VisuGlView *view, GError **error);
static gboolean _readFloatv(VisuConfigFileEntry *entry, gchar **lines, int nbLines,
                            int position, VisuData *dataObj,
                            VisuGlView *view, GError **error);
static gboolean _readString(VisuConfigFileEntry *entry, gchar **lines, int nbLines,
                            int position, VisuData *dataObj _U_,
                            VisuGlView *view _U_, GError **error);

static gint compareStringsInGList(gconstpointer a, gconstpointer b)
{
  return strcmp((char*)a, (char*)b);
}

/**
 * visu_config_file_entry_get_type:
 * 
 * Create and retrieve a #GType for a #VisuConfigFileEntry object.
 *
 * Since: 3.7
 *
 * Returns: a new type for #VisuConfigFileEntry structures.
 */
GType visu_config_file_entry_get_type(void)
{
  static GType g_define_type_id = 0;

  if (g_define_type_id == 0)
    g_define_type_id =
      g_boxed_type_register_static("VisuConfigFileEntry", 
                                   (GBoxedCopyFunc)entry_copy,
                                   (GBoxedFreeFunc)entry_free);
  return g_define_type_id;
}
static VisuConfigFileEntry* entry_init(const gchar *key, const gchar *description,
                                       int kind, guint nbLines)
{
  VisuConfigFileEntry *entry;

  g_return_val_if_fail(key && *key, (VisuConfigFileEntry*)0);
  g_return_val_if_fail(description, (VisuConfigFileEntry*)0);
  g_return_val_if_fail(nbLines > 0 && (kind == VISU_CONFIG_FILE_PARAMETER ||
				       kind == VISU_CONFIG_FILE_RESOURCE),
		       (VisuConfigFileEntry*)0);

  entry = g_malloc(sizeof(VisuConfigFileEntry));
  entry->key = g_strdup(key);
  entry->kquark = g_quark_from_static_string(entry->key);
  entry->description = g_strdup(description);
  entry->kind = kind;
  if (kind == VISU_CONFIG_FILE_PARAMETER)
    entry->nbLines = 1;
  else
    entry->nbLines = nbLines;
  entry->storage = (gpointer)0;
  entry->tag = (gchar*)0;
  entry->newKey = (gchar*)0;
  entry->version = 3.0f;
  
  return entry;
}
static gboolean entry_register(VisuConfigFileEntry *entry)
{
  if (!visuConfigFile_entryList)
    visuConfigFileInit();

  DBG_fprintf(stderr, "Visu ConfigFile: going to add key '%s'.\n", entry->key);
  if (g_hash_table_lookup(visuConfigFile_entryList, (gpointer)entry->key))
    return FALSE;

  /* Store it. */
  g_hash_table_insert(visuConfigFile_entryList, (gpointer)entry->key, (gpointer)entry);
  if (entry->kind == VISU_CONFIG_FILE_RESOURCE)
    registeredResources = g_list_append(registeredResources, (gpointer)entry);
  else if (entry->kind == VISU_CONFIG_FILE_PARAMETER)
    registeredParameters = g_list_append(registeredParameters, (gpointer)entry);

  return TRUE;
}
static VisuConfigFileEntry* entry_copy(VisuConfigFileEntry *entry)
{
  VisuConfigFileEntry *out;

  out = g_malloc(sizeof(VisuConfigFileEntry));
  *out = *entry;
  out->key = g_strdup(entry->key);
  if (entry->description)
    out->description = g_strdup(entry->description);
  if (entry->newKey)
    out->newKey = g_strdup(entry->newKey);
  if (entry->tag)
    out->tag = g_strdup(entry->tag);
  return out;
}
static void entry_free(VisuConfigFileEntry *entry)
{
  g_free(entry->key);
  if (entry->description)
    g_free(entry->description);
  if (entry->newKey)
    g_free(entry->newKey);
  if (entry->tag)
    g_free(entry->tag);
  g_free(entry);
}

/**
 * visu_config_file_addEntry:
 * @kind: an integer ;
 * @key: a string (should not be NULL) ;
 * @description: (allow-none): a string (can be NULL) ;
 * @nbLines: an integer ;
 * @readFunc: (scope call): a VisuConfigFileReadFunc.
 *
 * This creates a new #VisuConfigFileEntry object with the given
 * values. The key and description arguments are copied.
 *
 * Returns: the newly created #VisuConfigFileEntry object.
 */
VisuConfigFileEntry* visu_config_file_addEntry(int kind, const gchar *key,
                                               const gchar* description, int nbLines,
                                               VisuConfigFileReadFunc readFunc)
{
  VisuConfigFileEntry *entry;

  entry = entry_init(key, description, kind, nbLines);
  if (!entry)
    return (VisuConfigFileEntry*)0;
  entry->read = readFunc;

  if (!entry_register(entry))
    {
      g_free(entry);
      g_warning("entry '%s' already exists!", key);
    }

  return entry;
}
/**
 * visu_config_file_addBooleanEntry:
 * @kind: an integer ;
 * @key: a string (should not be NULL) ;
 * @description: (allow-none): a string (can be NULL) ;
 * @location: a pointer where to store a boolean when the entry is
 * parsed.
 *
 * Defines a #VisuConfigFileEntry that will be a single boolean to
 * read and to store in @location.
 *
 * Since: 3.7
 *
 * Returns: (transfer full): the newly created #VisuConfigFileEntry object.
 **/
VisuConfigFileEntry* visu_config_file_addBooleanEntry(int kind, const gchar *key,
                                                      const gchar* description,
                                                      gboolean *location)
{
  VisuConfigFileEntry *entry;

  g_return_val_if_fail(location, (VisuConfigFileEntry*)0);

  entry = entry_init(key, description, kind, 1);
  if (!entry)
    return (VisuConfigFileEntry*)0;
  entry->read = _readBoolean;
  entry->storage = (gpointer)location;

  if (!entry_register(entry))
    {
      g_free(entry);
      g_warning("entry '%s' already exists!", key);
    }

  return entry;
}
/**
 * visu_config_file_addFloatArrayEntry:
 * @kind: an integer ;
 * @key: a string (should not be NULL) ;
 * @description: (allow-none): a string (can be NULL) ;
 * @nValues: the number of floats to read.
 * @location: a pointer where to store floats when the entry is
 * parsed.
 * @clamp: the min and max values allowed.
 *
 * Defines a #VisuConfigFileEntry that will parse @nValues floats and
 * store them consecutively in @location. The parsed values are
 * checked to be in @clamp.
 *
 * Since: 3.7
 *
 * Returns: (transfer full): the newly created #VisuConfigFileEntry object.
 **/
VisuConfigFileEntry* visu_config_file_addFloatArrayEntry(int kind, const gchar *key,
                                                         const gchar* description,
                                                         guint nValues, float *location,
                                                         float clamp[2])
{
  VisuConfigFileEntry *entry;

  g_return_val_if_fail(location, (VisuConfigFileEntry*)0);

  entry = entry_init(key, description, kind, 1);
  if (!entry)
    return (VisuConfigFileEntry*)0;
  entry->read = _readFloatv;
  entry->storage = (gpointer)location;
  entry->nValues = nValues;
  entry->range[0] = clamp[0];
  entry->range[1] = clamp[1];

  if (!entry_register(entry))
    {
      g_free(entry);
      g_warning("entry '%s' already exists!", key);
    }

  return entry;
}
/**
 * visu_config_file_addStringEntry:
 * @kind: an integer ;
 * @key: a string (should not be NULL) ;
 * @description: (allow-none): a string (can be NULL) ;
 * @location: a pointer where to store a string when the entry is
 * parsed.
 *
 * Defines a #VisuConfigFileEntry that will be a string to
 * read and to store in @location. If @location already contains a
 * string, it is g_free().
 *
 * Since: 3.7
 *
 * Returns: (transfer full): the newly created #VisuConfigFileEntry object.
 **/
VisuConfigFileEntry* visu_config_file_addStringEntry(int kind, const gchar *key,
                                                     const gchar* description,
                                                     gchar **location)
{
  VisuConfigFileEntry *entry;

  g_return_val_if_fail(location, (VisuConfigFileEntry*)0);

  entry = entry_init(key, description, kind, 1);
  if (!entry)
    return (VisuConfigFileEntry*)0;
  entry->read = _readString;
  entry->storage = (gpointer)location;

  if (!entry_register(entry))
    {
      g_free(entry);
      g_warning("entry '%s' already exists!", key);
    }

  return entry;
}
/**
 * visu_config_file_getEntries:
 * @kind: either #VISU_CONFIG_FILE_PARAMETER or
 * #VISU_CONFIG_FILE_RESOURCE ;
 *
 * This routine should be used for introspections purpose, to know
 * what resources or parameters are available.
 *
 * Returns: (element-type utf8) (transfer none): a #GList own by V_Sim.
 */
GList* visu_config_file_getEntries(int kind)
{
  g_return_val_if_fail(kind == VISU_CONFIG_FILE_PARAMETER ||
		       kind == VISU_CONFIG_FILE_RESOURCE,
		       (GList*)0);
  if (kind == VISU_CONFIG_FILE_PARAMETER)
    return registeredParameters;
  else
    return registeredResources;
}

/**
 * visu_config_file_addKnownTag:
 * @tag: a string (not nul or empty).
 *
 * If parameter entries have a tag, they are ignored except if their tag
 * has been declared using this method.
 */
void visu_config_file_addKnownTag(gchar* tag)
{
  g_return_if_fail(tag && *tag);

  if (!knownTags)
    visuConfigFileInit();

  g_hash_table_insert(knownTags, (gpointer)tag, GINT_TO_POINTER(1));
}
/**
 * visu_config_file_addExportFunction:
 * @kind: an integer to identify resources or parameters ;
 * @writeFunc: (scope call): a VisuConfigFileExportFunc method.
 *
 * This stores the @writeFunc given. It will be called when resources or parameters
 * will be exported to disk.
 */
void visu_config_file_addExportFunction(int kind, VisuConfigFileExportFunc writeFunc)
{
  struct writeFunc_struct *str;

  if (!writeFunc)
    return;
  g_return_if_fail(kind == VISU_CONFIG_FILE_PARAMETER ||
		   kind == VISU_CONFIG_FILE_RESOURCE);

  str = g_malloc(sizeof(struct writeFunc_struct));
  str->writeFunc = writeFunc;
  if (kind == VISU_CONFIG_FILE_RESOURCE)
    exportResourcesList =
      g_list_append(exportResourcesList, (gpointer)str);
  else if (kind == VISU_CONFIG_FILE_PARAMETER)
    exportParametersList =
      g_list_append(exportParametersList, (gpointer)str);
}

static void freeConfigEntry(gpointer data)
{
  VisuConfigFileEntry *entry;

  if (!data)
    return;

  entry = (VisuConfigFileEntry*)data;
  g_free(entry->key);
  g_free(entry->description);
  if (entry->tag)
    g_free(entry->tag);
  if (entry->newKey)
    g_free(entry->newKey);
  g_free(entry);
}
/**
 * visu_config_file_entry_setTag:
 * @entry: a #VisuConfigFileEntry object ;
 * @tag: a string.
 *
 * This method is used to set a tag to the given entry. This tag is used
 * to ignore or not the entry when the file is read. The @tag argument
 * is copied.
 */
void visu_config_file_entry_setTag(VisuConfigFileEntry *entry, const gchar *tag)
{
  g_return_if_fail(entry);

  if (entry->tag)
    g_free(entry->tag);
  entry->tag = g_strdup(tag);
}
/**
 * visu_config_file_entry_setVersion:
 * @entry: a #VisuConfigFileEntry object ;
 * @version: the version the entry appear in.
 *
 * Set the version number the entry appear in.
 */
void visu_config_file_entry_setVersion(VisuConfigFileEntry *entry, float version)
{
  g_return_if_fail(entry && version > 3.0f);

  entry->version = version;
}
/**
 * visu_config_file_entry_setReplace:
 * @newEntry: a #VisuConfigFileEntry object ;
 * @oldEntry: idem.
 *
 * Use this method to declare that @oldEntry has become obsolete and
 * has been replaced by @newEntry.
 */
void visu_config_file_entry_setReplace(VisuConfigFileEntry *newEntry,
                                       VisuConfigFileEntry *oldEntry)
{
  g_return_if_fail(newEntry && oldEntry);

  if (oldEntry->newKey)
    g_free(oldEntry->newKey);
  oldEntry->newKey = g_strdup(newEntry->key);
}

static gchar* _getKey(const gchar *buf, gchar **key, gchar **tag, guint iLine, GError **error)
{
  gchar *key_, *tag_, *end, *ret;

  *key = (gchar*)0;
  *tag = (gchar*)0;

  ret = strchr(buf, ':');
  if (!ret)
    return (gchar*)0;

  key_ = g_strndup(buf, ret - buf);
  key_ = g_strstrip(key_);
  *key = key_;

  /* Look for the tag */
  tag_ = strchr(key_, '[');
  if (tag_)
    {
      *tag_ = '\0';
      tag_ += 1;
      end = strchr(tag_, ']');
      if (end)
        {
          *end = '\0';
          tag_ = g_strdup(tag_);
          *tag = tag_;
        }
      else
        *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_TAG,
                             _("Parse error at line %d,"
                               " the tag '%s' is not closed.\n"),
                             iLine, tag_);
    }

  DBG_fprintf(stderr,"Visu ConfigFile: read a flag (tag): '%s' (%s).\n", key_, tag_);
  return ret;
}
static VisuConfigFileEntry* _getEntry(const gchar *key, guint iLine, GError **error)
{
  VisuConfigFileEntry *entry;

  entry = (VisuConfigFileEntry*)g_hash_table_lookup(visuConfigFile_entryList,
                                                    (gpointer)key);
  if (!entry)
    *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_MARKUP,
                         _("Parse error at line %d,"
                           " '%s' is an unknown markup.\n"),
                         iLine, key);
  else if (entry->newKey)
    g_warning(_("Markup '%s' is obsolete, replaced by '%s'."), key, entry->newKey);
  return entry;
}
static void _appendMessage(GString *message, GError **error)
{
  if (*error)
    {
      g_string_append(message, (*error)->message);
      g_error_free(*error);
      *error = (GError*)0;
    }
}
static gboolean _parse(VisuConfigFileEntry *entry, gchar **tokens, guint iLine,
                       VisuData *dataObj, VisuGlView *view, GError **error)
{
  gboolean ret;
  static guint sig = 0;

  if (!sig)
    sig = g_signal_lookup("entryParsed", VISU_TYPE_OBJECT);

  ret = TRUE;
  if (tokens)
    {
      if (entry->read)
        ret = entry->read(entry, tokens, entry->nbLines, iLine, dataObj, view, error);
      g_strfreev(tokens);
      if (ret)
        g_signal_emit(VISU_OBJECT_INSTANCE, sig, entry->kquark, entry->key);
    }
  return ret;
}

static gboolean _loadRaw(int kind, const char* fileName,
                         VisuData *dataObj, VisuGlView *view, GError **error)
{
  GIOChannel *ioFile;
  GString *line= (GString*)0;
  GIOStatus status;
  guint nbLine, i;
  gchar *deuxPoints;
  gchar **tokens;
  gchar *key, *tag;
  VisuConfigFileEntry *entry;
  GString *message;

  ioFile = g_io_channel_new_file(fileName, "r", error);
  if (*error)
    return FALSE;

  message = g_string_new("");
  line = g_string_new("");
  nbLine = 0;

  status = G_IO_STATUS_NORMAL;
  while (status == G_IO_STATUS_NORMAL)
    {
      status = g_io_channel_read_line_string(ioFile, line, NULL, error);
      if (*error)
        {
          g_string_free(line, TRUE);
          g_string_free(message, TRUE);
          return FALSE;
        }
      nbLine += 1;

      if (status == G_IO_STATUS_EOF || line->str[0] == '#' || line->str[0] == '\n')
        continue;

      entry = (VisuConfigFileEntry*)0;

      deuxPoints = _getKey(line->str, &key, &tag, nbLine, error);
      _appendMessage(message, error);
      
      if (key)
        {
          if (tag && !g_hash_table_lookup(knownTags, (gpointer)tag))
            {
              entry = (VisuConfigFileEntry*)0;
              DBG_fprintf(stderr, "Visu ConfigFile: the entry '%s' has an unknown tag (%s),"
                          " it will be dismissed.\n", key, tag);
            }
          else
            {
              entry = _getEntry(key, nbLine, error);
              _appendMessage(message, error);
            }
          g_free(key);
          if (tag)
            g_free(tag);
        }

      if (entry)
        {
          tokens = g_malloc0(sizeof(gchar*) * (entry->nbLines + 1));
          if (kind == VISU_CONFIG_FILE_RESOURCE)
            for (i = 0; i < entry->nbLines; i++)
              {
                status = g_io_channel_read_line_string(ioFile, line,
                                                       NULL, error);
                if (*error)
                  {
                    g_string_free(line, TRUE);
                    g_string_free(message, TRUE);
                    return FALSE;
                  }
                nbLine += 1;
                if (status != G_IO_STATUS_NORMAL)
                  {
                    g_strfreev(tokens);
                    tokens = (gchar**)0;
                    *error = g_error_new(TOOL_CONFIG_FILE_ERROR,
                                         TOOL_CONFIG_FILE_ERROR_MISSING,
                                         _("Parse error at line %d,"
                                           " '%s' needs %d lines but only %d were read.\n"),
                                         nbLine, entry->key, entry->nbLines, i);
                    break;
                  }
                tokens[i] = g_strdup(line->str);
              }
          else
            tokens[0] = g_strdup(deuxPoints + 1);
          _parse(entry, tokens, nbLine, dataObj, view, error);
          _appendMessage(message, error);
        }
    }
  g_string_free(line, TRUE);

  status = g_io_channel_shutdown(ioFile, FALSE, error);
  g_io_channel_unref(ioFile);
  if (status != G_IO_STATUS_NORMAL)
    {
      g_string_free(message, TRUE);
      return FALSE;
    }
  DBG_fprintf(stderr, "Visu ConfigFile: read OK (error len = %d).\n", (int)message->len);

  if (message->len > 0)
    {
      DBG_fprintf(stderr, " | %s\n", message->str);
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_READ,
                           "%s", message->str);
    }
  g_string_free(message, TRUE);

  return (*error == (GError*)0);
}

struct _dt
{
  gboolean parse;
  GString *message;
  VisuData *dataObj;
  VisuGlView *view;

  VisuConfigFileEntry *entry;
  gchar *tag, *id, *text;
};

static void _element(GMarkupParseContext *context _U_,
                     const gchar         *element_name,
                     const gchar        **attribute_names,
                     const gchar        **attribute_values,
                     gpointer             user_data,
                     GError             **error)
{
  struct _dt *dt = (struct _dt*)user_data;
  guint i;

  if (!strcmp(element_name, "resources"))
    dt->parse = TRUE;
  else if (!strcmp(element_name, "entry"))
    {
      dt->tag  = (gchar*)0;
      dt->id   = (gchar*)0;
      dt->text = (gchar*)0;
      for (i = 0; attribute_names[i]; i++)
        {
          if (!strcmp(attribute_names[i], "name"))
            {
              dt->entry = _getEntry(attribute_values[i], 0, error);
              _appendMessage(dt->message, error);
            }
          else if (!strcmp(attribute_names[i], "id"))
            dt->id = g_strdup(attribute_values[i]);
        }
    }
}
static void _endElement(GMarkupParseContext *context _U_,
                        const gchar         *element_name,
                        gpointer             user_data,
                        GError             **error)
{
  struct _dt *dt = (struct _dt*)user_data;
  gchar **tokens;

  if (!strcmp(element_name, "resources"))
    dt->parse = FALSE;
  else if (!strcmp(element_name, "entry") && dt->entry)
    {
      tokens = g_malloc0(sizeof(gchar*) * (dt->entry->nbLines  + 1));
      /* Tricks... */
      if (!strcmp(dt->entry->key, "pair_link"))
        {
          tokens[0] = g_strdup(dt->id);
          tokens[1] = g_strdup(dt->text);
        }
      else if (!strcmp(dt->entry->key, "isosurface_color") ||
               !strcmp(dt->entry->key, "isosurface_properties"))
        tokens[0] = g_strdup_printf("\"%s\" %s", (dt->id)?dt->id:"", dt->text);
      else
        tokens[0] = g_strdup_printf("%s %s", (dt->id)?dt->id:"", dt->text);
      _parse(dt->entry, tokens, 0, dt->dataObj, dt->view, error);
      _appendMessage(dt->message, error);
      dt->entry = (VisuConfigFileEntry*)0;
      if (dt->tag)
        g_free(dt->tag);
      if (dt->id)
        g_free(dt->id);
      if (dt->text)
        g_free(dt->text);
    }
}
static void _text(GMarkupParseContext *context _U_,
                  const gchar         *text,
                  gsize                text_len _U_,
                  gpointer             user_data,
                  GError             **error _U_)
{
  struct _dt *dt = (struct _dt*)user_data;

  if (dt->entry)
    dt->text = g_strdup(text);
}

static gboolean _loadXML(const gchar *filename, VisuData *dataObj,
                         VisuGlView *view, GError **error)
{
  GMarkupParseContext* xmlContext;
  GMarkupParser parser;
  gsize size;
  gchar *buffer;
  struct _dt dt;

  /* Read file. */
  buffer = (gchar*)0;
  if (!g_file_get_contents(filename, &buffer, &size, error))
    return FALSE;

  /* Create context. */
  parser.start_element = _element;
  parser.end_element   = _endElement;
  parser.text          = _text;
  parser.passthrough   = NULL;
  parser.error         = NULL;
  dt.parse   = FALSE;
  dt.message = g_string_new("");
  dt.dataObj = dataObj;
  dt.view    = view;
  dt.entry   = (VisuConfigFileEntry*)0;
  dt.tag     = (gchar*)0;
  dt.id      = (gchar*)0;
  dt.text    = (gchar*)0;
  xmlContext = g_markup_parse_context_new(&parser, 0, &dt, NULL);

  /* Parse data. */
  g_markup_parse_context_parse(xmlContext, buffer, size, error);

  /* Free buffers. */
  g_markup_parse_context_free(xmlContext);
  g_free(buffer);
  if (!*error && dt.message->len > 0)
    *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_READ,
                         "%s", dt.message->str);
  g_string_free(dt.message, TRUE);

  return (*error == (GError*)0);
}

/**
 * visu_config_file_load:
 * @kind: an integer to identify the kind of file ;
 * @filename: the path to file to read ;
 * @dataObj: (allow-none): a #VisuData object, sometime needed to
 * update values (can be NULL) ;
 * @view: (allow-none): a #VisuGlView object, sometime needed to
 * update values (can be NULL).
 * @error: (allow-none): a pointer to a GError pointer.
 *
 * Try to load the resources/parameters from the file name given in
 * parameter.
 *
 * Returns: TRUE if everything goes right. If @error is not NULL it
 *          should be freed with g_error_free().
 */
gboolean visu_config_file_load(int kind, const char* filename,
                               VisuData *dataObj, VisuGlView *view,
                               GError **error)
{
  gboolean res;

  if (!visuConfigFile_entryList)
    visuConfigFileInit();

  DBG_fprintf(stderr, "Visu ConfigFile: parsing '%s' file for"
	      " resources/parameters...\n", filename);

  g_return_val_if_fail(kind == VISU_CONFIG_FILE_RESOURCE ||
		       kind == VISU_CONFIG_FILE_PARAMETER, FALSE);

  if (kind == VISU_CONFIG_FILE_RESOURCE && strstr(filename, ".xml"))
    res = _loadXML(filename, dataObj, view, error);
  else
    res = _loadRaw(kind, filename, dataObj, view, error);

  /* We save the current path. */
  if (kind == VISU_CONFIG_FILE_RESOURCE)
    {
      if (currentResPath)
	g_free(currentResPath);
      currentResPath = g_strdup(filename);

      DBG_fprintf(stderr, "Gtk Save: emitting 'resourcesLoaded' signal.\n");
      g_signal_emit_by_name(VISU_OBJECT_INSTANCE, "resourcesLoaded", (gpointer)dataObj, NULL);
      DBG_fprintf(stderr, "Gtk Save: emission done.\n");
    }

  return res;
}

/**
 * visu_config_file_exportComment:
 * @buffer: the buffer to add a comment to.
 * @comment: a comment.
 *
 * Append to @buffer the given @comment, using the current output
 * style (raw text or XML as instance).
 *
 * Since: 3.7
 **/
void visu_config_file_exportComment(GString *buffer, const gchar *comment)
{
  g_return_if_fail(buffer && comment);

  if (!comment[0])
    {
      g_string_append(buffer, "\n");
      return;
    }

  switch (format)
    {
    case (_format_raw):
      g_string_append_printf(buffer, "# %s\n", comment);
      break;
    case (_format_xml):
      g_string_append_printf(buffer, "    <!-- %s -->\n", comment);
      break;
    }
}
/**
 * visu_config_file_exportEntry:
 * @buffer: the buffer to write the entry to.
 * @name: the name of the entry.
 * @id_value: (allow-none): an id for the entry.
 * @format_: the formatting string for the message.
 * @...: the values to print.
 *
 * Append to @buffer the given @entry, using the current output
 * style (raw text or XML as instance). @id_value can be used to
 * specify the entry apply to, for instance, the name of the
 * #VisuElement the colour property entry apply to.
 *
 * Since: 3.7
 **/
void visu_config_file_exportEntry(GString *buffer, const gchar *name,
                                  const gchar *id_value, const gchar *format_, ...)
{
  va_list arglist;
  gchar *buf;

  g_return_if_fail(buffer && name && format_);

  va_start(arglist, format_);
  buf = g_strdup_vprintf(format_, arglist);
  va_end(arglist);
  switch (format)
    {
    case (_format_raw):
      /* Special case for backward compatibility. */
      if (!strcmp(name, "pair_link"))
        g_string_append_printf(buffer, "%s:\n  %s\n  %s\n", name, (id_value)?id_value:"", buf);
      else if (!strcmp(name, "isosurface_color") || !strcmp(name, "isosurface_properties"))
        g_string_append_printf(buffer, "%s:\n  \"%s\" %s\n", name, (id_value)?id_value:"", buf);
      else
        g_string_append_printf(buffer, "%s:\n  %s  %s\n", name, (id_value)?id_value:"", buf);
      break;
    case (_format_xml):
      g_string_append_printf(buffer, "    <entry name=\"%s\"", name);
      if (id_value && id_value[0])
        g_string_append_printf(buffer, " id=\"%s\"", id_value);
      g_string_append_printf(buffer, ">%s</entry>\n", buf);
      break;
    }
  g_free(buf);
}

/**
 * visu_config_file_saveResourcesToXML:
 * @filename: the path to file to read ;
 * @lines: a pointer to an integer (can be NULL) ;
 * @dataObj: (allow-none): a #VisuData object (can be NULL) ;
 * @view: (allow-none): a #VisuGlView object (can be NULL) ;
 * @error: a location to store a possible error.
 *
 * Same routine as visu_config_file_save() but use an XML format instead.
 *
 * Since: 3.7
 *
 * Returns: TRUE if everything goes right.
 */
gboolean visu_config_file_saveResourcesToXML(const char* filename, int *lines,
                                             VisuData *dataObj, VisuGlView *view,
                                             GError **error)
{
  gchar *ptCh;
  GString *buffer;
  int nbLine;
  GList *pos;
  gboolean success;

  g_return_val_if_fail(error && !*error, FALSE);

  DBG_fprintf(stderr, "Visu ConfigFile: exporting '%s' file for"
	      " XML resources...\n", filename);

  format = _format_xml;
  buffer = g_string_new("<resources");
  g_string_append_printf(buffer, " version=\"%s\">\n", VERSION_HEADER);
  for (pos = exportResourcesList; pos; pos = g_list_next(pos))
    {
      /* g_string_append_printf("  <entry name=\"%s\">\n", ((VisuConfigFileEntry*)pos->data)->key); */
      ((struct writeFunc_struct*)(pos->data))->writeFunc(buffer, dataObj, view);
    }
  g_string_append(buffer, "  </resources>");

  nbLine = 0;
  ptCh = buffer->str;
  while ((ptCh = strchr(ptCh + 1, '\n')))
    nbLine += 1;

  DBG_fprintf(stderr, "Visu ConfigFile: export OK to string,"
	      " preparing to write file.\n");
  success = tool_XML_substitute(buffer, filename, "resources", error);
  if (!success)
    {
      g_string_free(buffer, TRUE);
      return FALSE;
    }
  success = g_file_set_contents(filename, buffer->str, -1, error);
  DBG_fprintf(stderr, "Visu ConfigFile: write %d lines (%d).\n", nbLine, success);
  g_string_free(buffer, TRUE);

  /* We save the current path. */
  if (success)
    {
      DBG_fprintf(stderr, " | save path '%s' as current.\n", filename);
      if (currentResPath)
	g_free(currentResPath);
      currentResPath = g_strdup(filename);
    }

  if (lines)
    *lines = nbLine;

  return success;
}

/**
 * visu_config_file_save:
 * @kind: an integer to identify the kind of file ;
 * @fileName: the path to file to read ;
 * @lines: a pointer to an integer (can be NULL) ;
 * @dataObj: (allow-none): a #VisuData object (can be NULL) ;
 * @view: (allow-none): a #VisuGlView object (can be NULL) ;
 * @error: a location to store a possible error.
 *
 * Try to export the resources/parameters to the file name given in
 * parameter. If @lines argument
 * is not NULL, and everything went right, it stores the number of written lines.
 * If the argument @dataObj is not null, only resources related
 * to the #VisuData object should be exported (for parameters files, @dataObj is
 * always NULL). The same for @view.
 *
 * Returns: TRUE if everything goes right.
 */
gboolean visu_config_file_save(int kind, const char* fileName, int *lines,
                               VisuData *dataObj, VisuGlView *view, GError **error)
{
  gchar *ptCh;
  GString *exportString;
  int nbLine;
  GList *pos;
  gboolean success;

  g_return_val_if_fail(error && !*error, FALSE);

  DBG_fprintf(stderr, "Visu ConfigFile: exporting '%s' file for"
	      " resources/parameters...\n", fileName);

  g_return_val_if_fail(kind == VISU_CONFIG_FILE_RESOURCE ||
		       kind == VISU_CONFIG_FILE_PARAMETER, FALSE);

  format = _format_raw;
  exportString = g_string_new("");
  if (kind == VISU_CONFIG_FILE_RESOURCE)
    g_string_append_printf(exportString, RESOURCE_HEADER);
  else if (kind == VISU_CONFIG_FILE_PARAMETER)
    g_string_append_printf(exportString, PARAMETER_HEADER);
  g_string_append_printf(exportString,
			 " v"VERSION_HEADER
			 "\n"
			 "#====================\n"
			 "\n"
			 "#WARNING: this file format is DIFFERENT from that for\n"
			 "#standard v_sim version <= 2.x\n"
			 "\n"
			 "#Line beginning with a # are not parsed.\n"
			 "\n");
  if (kind == VISU_CONFIG_FILE_RESOURCE)
    g_string_append_printf(exportString,
			   "#The only \"useful\" lines must have the following contents\n"
			   "#several two or more lines patterns:\n"
			   "#resource_name:\n"
			   "#values separeted by blank characters\n"
			   "\n"
			   "#The following resource names are valid :\n");
  else
    g_string_append_printf(exportString,
			   "#The only \"useful\" lines must have the following pattern:\n"
			   "#parameter_name: value\n"
			   "\n"
			   "#The following parameter names are valid :\n");
  for (pos = (kind == VISU_CONFIG_FILE_RESOURCE)?
	 registeredResources:registeredParameters;
       pos; pos = g_list_next(pos))
    g_string_append_printf(exportString, "# %s\n",
			   ((VisuConfigFileEntry*)(pos->data))->key);
  g_string_append_printf(exportString, "\n");

  for (pos = (kind == VISU_CONFIG_FILE_RESOURCE)?
	 exportResourcesList:exportParametersList;
       pos; pos = g_list_next(pos))
    ((struct writeFunc_struct*)(pos->data))->writeFunc(exportString, dataObj, view);

  nbLine = 0;
  ptCh = exportString->str;
  while ((ptCh = strchr(ptCh + 1, '\n')))
    nbLine += 1;

  DBG_fprintf(stderr, "Visu ConfigFile: export OK to string,"
	      " preparing to write file.\n");

  success = g_file_set_contents(fileName, exportString->str, -1, error);
  g_string_free(exportString, TRUE);

  DBG_fprintf(stderr, "Visu ConfigFile: write %d lines (%d).\n", nbLine, success);

  /* We save the current path. */
  if (kind == VISU_CONFIG_FILE_RESOURCE && success)
    {
      DBG_fprintf(stderr, " | save path '%s' as current.\n", fileName);
      if (currentResPath)
	g_free(currentResPath);
      currentResPath = g_strdup(fileName);
    }

  if (lines)
    *lines = nbLine;

  return success;
}

static gboolean _validateHeader(const gchar *filename, int kind)
{
  FILE *file;
  float version;
  char *msg, *header;
  char line[TOOL_MAX_LINE_LENGTH];

  DBG_fprintf(stderr, "Visu ConfigFile: looking for header of \n  '%s' ... ", filename);
  if (strstr(filename, ".xml"))
    {
      DBG_fprintf(stderr, "accepted.\n");
      return TRUE;
    }

  file = fopen(filename, "r");
  if (!file)
    {
      g_warning("The file '%s' should be readable but something goes"
                " nasty when one wants to open it.\n", filename);
      return FALSE;
    }
  version = 0.;
  msg = fgets(line, TOOL_MAX_LINE_LENGTH, file);
  fclose(file);
  if (kind == VISU_CONFIG_FILE_RESOURCE)
    header = RESOURCE_HEADER;
  else
    header = PARAMETER_HEADER;
  if (msg && !strncmp(line, header, strlen(header))
      && sscanf(line + strlen(header) + 2, "%f", &version))
    if (version >= 3.)
      {
        DBG_fprintf(stderr, "ok.\n");
        return TRUE;
      }
  DBG_fprintf(stderr, "wrong.\n");
  return FALSE;
}

static gchar* getValidFileWithHeader(int mode, const gchar* filenames[],
                                     int kind, GList **list)
{
  gchar *res;

  /* Look for a valid file.
     If it is for writing, a valid file is just given by a valid path.
     If it is for reading, a valid file is a valid path AND has a valid header. */
  while (*list)
    {
      /* We get the next valid path. */
      res = tool_getValidPath(list, filenames, mode);
      if (!res)
	{
	  DBG_fprintf(stderr, "Visu ConfigFile: no file available.\n");
	  return (gchar*)0;
	}

      /* if we are in reading mode, we test the header. */
      if (mode & R_OK)
	{
          if (_validateHeader(res, kind))
            return res;
          else
            g_free(res);
	}
      /* We are in writing mode so the valid path is ok. */
      else
	return res;
      *list = g_list_next(*list);
    }
  DBG_fprintf(stderr, "Visu ConfigFile: no file available.\n");
  return (gchar*)0;
}

/**
 * visu_config_file_getPathToResources:
 *
 * The resource file can be read from different places.
 *
 * Since: 3.6
 *
 * Returns: the path used to read the last resource file.
 */
const gchar* visu_config_file_getPathToResources()
{
  return currentResPath;
}
/**
 * visu_config_file_getValidPath:
 * @kind: an integer identifier ;
 * @mode: a value from R_OK, W_OK and X_OK as described in unistd.h.
 * @utf8: if 1, the path is return in UTF-8 format, otherwise, the locale
 * of the file system is used.
 *
 * Test the entries of the hadoc list to find
 * a valid position to read or write a config file.
 * It tests access for the specified file.
 *
 * Returns: the first valid path find in the list of known paths.
 */
gchar* visu_config_file_getValidPath(int kind, int mode, int utf8)
{
  GList *list;
  gchar* file;
  gchar* fileUTF8;

  g_return_val_if_fail(kind == VISU_CONFIG_FILE_RESOURCE ||
		       kind == VISU_CONFIG_FILE_PARAMETER, (gchar*)0);

  if (kind == VISU_CONFIG_FILE_RESOURCE)
    {
      list = resourcesPath;
      file = getValidFileWithHeader(mode, RESOURCES_FILENAMES,
				    kind, &list);
    }
  else
    {
      list = parametersPath;
      file = getValidFileWithHeader(mode, PARAMETERS_FILENAMES,
				    kind, &list);
    }

  if (!file)
    return file;

  if (utf8)
    {
      fileUTF8 = g_filename_from_utf8(file, -1, NULL, NULL, NULL);
      g_free(file);
      return fileUTF8;
    }
  else
    return file;
}
/**
 * visu_config_file_getNextValidPath:
 * @kind: an integer identifier ;
 * @accessMode: a value from R_OK, W_OK and X_OK as described in unistd.h ;
 * @list: (element-type filename) (inout) (transfer none): a pointer to a valid *GList ;
 * @utf8: if 1, the path is return in UTF-8 format, otherwise, the locale
 * of the file system is used.
 *
 * Test the entries of the given list to find
 * a valid position to read or write a config file.
 * It tests access for the specified file. After a call to this
 * method the @list argument points to the next entry in the list, after
 * the one found.
 *
 * Returns: the first valid path find in the given list of paths.
 */
gchar* visu_config_file_getNextValidPath(int kind, int accessMode, GList **list, int utf8)
{
  gchar* file;
  gchar* fileUTF8;

  g_return_val_if_fail(kind == VISU_CONFIG_FILE_RESOURCE ||
		       kind == VISU_CONFIG_FILE_PARAMETER, (gchar*)0);
  g_return_val_if_fail(list, (gchar*)0);

  if (!*list)
    return (gchar*)0;

  if (kind == VISU_CONFIG_FILE_RESOURCE)
    file = getValidFileWithHeader(accessMode, RESOURCES_FILENAMES,
				  kind, list);
  else
    file = getValidFileWithHeader(accessMode, PARAMETERS_FILENAMES,
				  kind, list);

  if (*list)
    *list = g_list_next(*list);

  if (!file)
    return file;

  if (utf8)
    {
      fileUTF8 = g_filename_from_utf8(file, -1, NULL, NULL, NULL);
      g_free(file);
      return fileUTF8;
    }
  else
    return file;
}
/**
 * visu_config_file_getDefaultFilename:
 * @kind: an integer identifier.
 *
 * This methods is used to get the filename used for different
 * config files.
 *
 * Returns: the filename of config file. The returned *gchar is
 *          owned by V_Sim and should not be freed or modified.
 */
const gchar* visu_config_file_getDefaultFilename(int kind)
{
  g_return_val_if_fail(kind == VISU_CONFIG_FILE_RESOURCE ||
		       kind == VISU_CONFIG_FILE_PARAMETER, (const gchar*)0);

  if (kind == VISU_CONFIG_FILE_RESOURCE)
    return RESOURCES_FILENAMES[0];
  else
    return PARAMETERS_FILENAMES[0];
}
/**
 * visu_config_file_getPathList:
 * @kind: an integer identifier.
 *
 * V_Sim stores a list of paths where to look for resources or parameters
 * files, this method is used to get these lists.
 *
 * Returns: (transfer none) (element-type filename): the list of the
 * parameters or resources paths. This list is read-only.
 */
GList* visu_config_file_getPathList(int kind)
{
  g_return_val_if_fail(kind == VISU_CONFIG_FILE_RESOURCE ||
		       kind == VISU_CONFIG_FILE_PARAMETER, (GList*)0);

  if (kind == VISU_CONFIG_FILE_RESOURCE)
    return resourcesPath;
  else
    return parametersPath;
}
GList* visuConfigFileAdd_resourcesPath(char* dir)
{
  GList *element;

  if (!dir || !dir[0])
    return (GList*)0;

  element = g_list_find_custom(resourcesPath, (gconstpointer)dir,
			       compareStringsInGList);
  if (!element)
    {
      DBG_fprintf(stderr, "Visu ConfigFile: add a new resource directory"
		  " to the path :\n '%s'\n", dir);
      resourcesPath = g_list_insert(resourcesPath, (gpointer)dir, 1);
      return resourcesPath->next;
    }
  else
    return (GList*)element;
}

/**
 * visu_config_file_exportToXML:
 * @filename: a string in the encoding of the file system ;
 * @kind: either #VISU_CONFIG_FILE_PARAMETER or
 * #VISU_CONFIG_FILE_RESOURCE ;
 * @error: a location to store an error.
 *
 * Export all the registered entries for resources or parameters to an
 * XML file.
 *
 * Returns: TRUE if the file is written with success.
 */
gboolean visu_config_file_exportToXML(const gchar *filename, int kind, GError **error)
{
  GString *str;
  GList *tmpLst;
  VisuConfigFileEntry *entry;
  gboolean status;
  gchar *desc;

  g_return_val_if_fail(filename && *filename, FALSE);
  g_return_val_if_fail(kind == VISU_CONFIG_FILE_RESOURCE ||
		       kind == VISU_CONFIG_FILE_PARAMETER, FALSE);

  str = g_string_new("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
  if (kind == VISU_CONFIG_FILE_PARAMETER)
    {
      tmpLst = registeredParameters;
      g_string_append_printf(str, "<configFile kind=\"parameters\">\n");
    }
  else
    {
      tmpLst = registeredResources;
      g_string_append_printf(str, "<configFile kind=\"resources\">\n");
    }
  while (tmpLst)
    {
      entry = (VisuConfigFileEntry*)tmpLst->data;
      if (entry->tag)
	g_string_append_printf(str, "  <entry name=\"%s\" tag=\"%s\" "
			       "version=\"%f3.1\">\n",
			       entry->key, entry->tag, entry->version);
      else
	g_string_append_printf(str, "  <entry name=\"%s\" version=\"%3.1f\">\n",
			       entry->key, entry->version);
      desc = g_markup_escape_text(entry->description, -1);
      g_string_append_printf(str, "    <description>%s</description>\n", desc);
      g_free(desc);
      if (entry->newKey)
	g_string_append_printf(str, "    <obsolete replacedBy=\"%s\" />\n",
			       entry->newKey);
      g_string_append_printf(str, "  </entry>\n");
      tmpLst = g_list_next(tmpLst);
    }
  g_string_append_printf(str, "</configFile>\n");

  status = g_file_set_contents(filename, str->str, -1, error);
  g_string_free(str, TRUE);
  return status;
}


static void visuConfigFileInit()
{
  gchar *currentDir;

  DBG_fprintf(stderr, "Visu ConfigFile: initialization process ...\n");

  visuConfigFile_entryList = g_hash_table_new_full(g_str_hash, g_str_equal,
						   NULL, freeConfigEntry);
  g_return_if_fail(visuConfigFile_entryList);

  registeredResources = (GList*)0;
  registeredParameters = (GList*)0;
  exportResourcesList = (GList*)0;
  exportParametersList = (GList*)0;
  knownTags = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, NULL);
  g_return_if_fail(knownTags);

  currentResPath = (gchar*)0;
  resourcesPath = (GList*)0;
  parametersPath = (GList*)0;
  resourcesPath = g_list_prepend(resourcesPath, (gpointer)V_SIM_DATA_DIR);
  parametersPath = g_list_prepend(parametersPath, (gpointer)V_SIM_DATA_DIR);

  resourcesPath = g_list_prepend(resourcesPath, (gpointer)V_SIM_OLD_LOCAL_CONF_DIR);
  parametersPath = g_list_prepend(parametersPath, (gpointer)V_SIM_OLD_LOCAL_CONF_DIR);

  resourcesPath = g_list_prepend(resourcesPath, (gpointer)V_SIM_LOCAL_CONF_DIR);
  parametersPath = g_list_prepend(parametersPath, (gpointer)V_SIM_LOCAL_CONF_DIR);

  currentDir = g_get_current_dir();
  resourcesPath = g_list_prepend(resourcesPath, (gpointer)currentDir);
  parametersPath = g_list_prepend(parametersPath, (gpointer)currentDir);

  visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
                           FLAG_RESOURCES_PATH,
                           DESC_RESOURCES_PATH,
                           1, readResourcesPaths);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_PARAMETER,
                                    exportResourcesPaths);
}

static gboolean readResourcesPaths(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position _U_,
				   VisuData *dataObj _U_, VisuGlView *view _U_,
                                   GError **error _U_)
{
  int i;
  gchar **tokens;
  gchar *key;

  g_return_val_if_fail(nbLines == 1, FALSE);

  tokens = g_strsplit_set(lines[0], ":", -1);
  for (i = 0; tokens[i]; i++)
    {
      key = g_strdup(tokens[i]);
      key = g_strstrip(key);
      visuConfigFileAdd_resourcesPath(key);
    }
  g_strfreev(tokens);
  return TRUE;
}
static void exportResourcesPaths(GString *data,
                                 VisuData *dataObj _U_, VisuGlView *view _U_)
{
  GList *pnt;

  g_string_append_printf(data, "# %s\n", DESC_RESOURCES_PATH);
  g_string_append_printf(data, "%s: ", FLAG_RESOURCES_PATH);
  pnt = resourcesPath;
  while(pnt)
    {
      /* We cancel the first and the last because it's the current working dir
	 and the install dir. */
      if (pnt->prev && pnt->next && pnt->next->next)
	g_string_append_printf(data, "%s", (char*)pnt->data);
      if (pnt->prev && pnt->next && pnt->next->next && pnt->next->next->next)
	g_string_append_printf(data, ":");
      pnt = g_list_next(pnt);
    }
  g_string_append_printf(data, "\n\n");
}

/* Specific routines. */
static gboolean _readBoolean(VisuConfigFileEntry *entry, gchar **lines, int nbLines,
                             int position, VisuData *dataObj _U_,
                             VisuGlView *view _U_, GError **error)
{
  gboolean val;

  g_return_val_if_fail(nbLines == 1 && entry->storage, FALSE);

  if (!tool_config_file_readBoolean(lines[0], position, &val, 1, error))
    return FALSE;
  *((gboolean*)entry->storage) = val;
  return TRUE;
}
static gboolean _readString(VisuConfigFileEntry *entry, gchar **lines, int nbLines,
                            int position, VisuData *dataObj _U_,
                            VisuGlView *view _U_, GError **error)
{
  gchar *str;

  g_return_val_if_fail(nbLines == 1, FALSE);

  lines[0] = g_strstrip(lines[0]);

  if (!lines[0][0])
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: 1 string value must appear"
			     " after the %s markup.\n"), position, entry->key);
      return FALSE;
    }
  str = *((gchar**)entry->storage);
  if (str)
    g_free(str);
  str = g_strdup(lines[0]);

  return TRUE;
}
static gboolean _readFloatv(VisuConfigFileEntry *entry, gchar **lines, int nbLines,
                            int position, VisuData *dataObj _U_,
                            VisuGlView *view _U_, GError **error)
{
  guint i;
  float *vals;
  
  g_return_val_if_fail(nbLines == 1 && entry->storage && entry->nValues > 0, FALSE);
  vals = g_malloc(sizeof(float) * entry->nValues);

  if (!tool_config_file_readFloat(lines[0], position, vals, entry->nValues, error))
    {
      g_free(vals);
      return FALSE;
    }

  for (i = 0; i < entry->nValues; i++)
    if (tool_config_file_clampFloat(vals + i, vals[i], entry->range[0], entry->range[1]))
      {
        *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
                             _("Parse error at line %d: %d floating points"
                               "(%g <= v <= %g) must appear after the %s markup.\n"),
                             position, entry->nValues, entry->range[0], entry->range[1],
                             entry->key);
        g_free(vals);
        return FALSE;
      }
  memcpy(entry->storage, vals, sizeof(float) * entry->nValues);
  g_free(vals);

  return TRUE;
}
