/*   EXTRAITS DE LA LICENCE
     Copyright CEA, contributeurs : Luc BILLARD et Damien
     CALISTE, laboratoire L_Sim, (2001-2006)
  
     Adresse m�l :
     BILLARD, non joignable par m�l ;
     CALISTE, damien P caliste AT cea P fr.

     Ce logiciel est un programme informatique servant � visualiser des
     structures atomiques dans un rendu pseudo-3D. 

     Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
     respectant les principes de diffusion des logiciels libres. Vous pouvez
     utiliser, modifier et/ou redistribuer ce programme sous les conditions
     de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
     sur le site "http://www.cecill.info".

     Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
     pris connaissance de la licence CeCILL, et que vous en avez accept� les
     termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
     Copyright CEA, contributors : Luc BILLARD et Damien
     CALISTE, laboratoire L_Sim, (2001-2006)

     E-mail address:
     BILLARD, not reachable any more ;
     CALISTE, damien P caliste AT cea P fr.

     This software is a computer program whose purpose is to visualize atomic
     configurations in 3D.

     This software is governed by the CeCILL  license under French law and
     abiding by the rules of distribution of free software.  You can  use, 
     modify and/ or redistribute the software under the terms of the CeCILL
     license as circulated by CEA, CNRS and INRIA at the following URL
     "http://www.cecill.info". 

     The fact that you are presently reading this means that you have had
     knowledge of the CeCILL license and that you accept its terms. You can
     find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "gtk_renderingWindowWidget.h"

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <math.h> /* for sqrt function... */
#include <string.h>
#include <stdlib.h>

#include "support.h"
#include "visu_gtk.h"
#include "visu_tools.h"
#include "visu_object.h"
#include "visu_basic.h"
#include "visu_data.h"
#include "visu_configFile.h"
#include "visu_extension.h"
#include "OSOpenGL/visu_openGL.h"
#include "renderingBackend/visu_actionInterface.h"
#include "gtk_openGLWidget.h"
#include "extraFunctions/extraNode.h"
#include "coreTools/toolFileFormat.h"
#include "coreTools/toolConfigFile.h"
#include "extraGtkFunctions/gtk_dumpDialogWidget.h"
#include "extraGtkFunctions/gtk_orientationChooser.h"
#include "openGLFunctions/objectList.h"
#include "openGLFunctions/interactive.h"
#include "opengl.h"
#include "extensions/axes.h"
#include "extensions/nodes.h"
#include "extensions/pairs.h"
#include "dumpModules/dumpToSVG.h"

/**
 * SECTION:gtk_renderingWindowWidget
 * @short_description: Defines a complex widget used to render files
 * and print information.
 *
 * <para>This is a complex widget, inheriting from #GtkWindow, with a
 * rendering area and a status bar area. A #VisuData is always
 * attached to this widget, see visu_ui_rendering_window_setData(). If not
 * the V_Sim logo is displayed.</para>
 *
 * <para>The rendering area can receive keyboard or mouse events, see
 * visu_ui_rendering_window_class_getInteractive.</para>
 *
 * <para>The status bar area has different buttons to load or export a
 * file. It also display some usefull information like the number of
 * rendered nodes. It has also a real status bar location displaying
 * tips about current available actions. One can add news using
 * visu_ui_rendering_window_pushMessage().</para>
 */

typedef enum
  {
    event_button_press,
    event_button_release,
    event_motion_notify,
    event_key_press,
    event_key_release,
    event_scroll
  } InteractiveEventsId;

struct InteractiveEvents_struct
{
  gulong callbackId;
  InteractiveEventsId id;
};
typedef struct InteractiveEvents_struct InteractiveEvents;


struct GtkInfoArea_struct
{
  GtkWidget *area;

  GtkWidget *infoBar;
  GtkWidget *searchEntry;

  GtkWidget *hboxFileInfo;
  GtkWidget *labelSize;
  GtkWidget *labelNb;
  GtkWidget *labelFileInfo;
  gboolean fileInfoFreeze;

  GtkWidget *hboxTools;
  GtkWidget *dumpButton;
  GtkWidget *loadButton;
  GtkWidget *raiseButton;
  GtkWidget *reloadButton;

  GtkWidget *statusInfo, *progress;
  guint progressId;
  GtkWidget *cancelButton;
  GCancellable *cancel;

  GtkWidget *hboxInteractive;
  GtkWidget *clearMarksButton;
  GtkWidget *infoButton;
  gulong infoClicked;

  guint statusInfoId;
};
typedef struct GtkInfoArea_struct GtkInfoArea;

#define GTK_STATUSINFO_NOFILEINFO _("<span style=\"italic\">No description is available</span>")
#define GTK_STATUSINFO_NONB       _("<span style=\"italic\">Nothing is loaded</span>")
#define FLAG_PARAMETER_RED_COORD   "config_showReducedCoordinates"
#define DESC_PARAMETER_RED_COORD   "Display coordinates in reduced values when picking a node ; boolean 0 or 1"
#define FLAG_PARAMETER_AUTO_ADJUST "config_autoAdjustCamera"
#define DESC_PARAMETER_AUTO_ADJUST "Auto adjust zoom capability for the box to be full size at zoom level 1 ; boolean 0 or 1"

#define MENU_CAMERA_SAVE    "<VisuUiRenderingWindow>/Camera/Save"
#define MENU_CAMERA_RESTORE "<VisuUiRenderingWindow>/Camera/Restore"
#define MENU_CAMERA_ORIENT  "<VisuUiRenderingWindow>/Camera/Orientation"
#define MENU_CAMERA_1       "<VisuUiRenderingWindow>/Camera/select1"
#define MENU_CAMERA_2       "<VisuUiRenderingWindow>/Camera/select2"
#define MENU_CAMERA_3       "<VisuUiRenderingWindow>/Camera/select3"
#define MENU_CAMERA_4       "<VisuUiRenderingWindow>/Camera/select4"
#define MENU_CAMERA_5       "<VisuUiRenderingWindow>/Camera/select5"
#define MENU_CAMERA_6       "<VisuUiRenderingWindow>/Camera/select6"
#define MENU_CAMERA_7       "<VisuUiRenderingWindow>/Camera/select7"
#define MENU_CAMERA_8       "<VisuUiRenderingWindow>/Camera/select8"
#define MENU_CAMERA_9       "<VisuUiRenderingWindow>/Camera/select9"
static const gchar* cameraAccels[] = {MENU_CAMERA_1, MENU_CAMERA_2, MENU_CAMERA_3, MENU_CAMERA_4,
                                      MENU_CAMERA_5, MENU_CAMERA_6, MENU_CAMERA_7, MENU_CAMERA_8,
                                      MENU_CAMERA_9};
static guint cameraKeys[] = {GDK_KEY_1, GDK_KEY_2, GDK_KEY_3, GDK_KEY_4, GDK_KEY_5,
                             GDK_KEY_6, GDK_KEY_7, GDK_KEY_8, GDK_KEY_9};

enum {
  EXPORT_SIGNAL,
  OPEN_SIGNAL,
  RELOAD_SIGNAL,
  SHOW_ACTION_DIALOG_SIGNAL,
  SHOW_MAIN_PANEL_SIGNAL,
  SHOW_ORIENTATION_SIGNAL,
  LOAD_NEXT_FILE_SIGNAL,
  LOAD_PREV_FILE_SIGNAL,
  SEARCH_SIGNAL,
  LAST_SIGNAL
};
enum
{
  TEXT_PLAIN,
  TEXT_URI_LIST
};

/* Local variables. */
static VisuInteractive *inter;
static guint _signals[LAST_SIGNAL] = { 0 };

/* Local methods. */
static void exportParameters(GString *data, VisuData *dataObj, VisuGlView *view);
static GtkInfoArea *gtkStatusInfo_createBar(VisuUiRenderingWindow *window, gint width,
					    gint height, gboolean withToolBar);
static gulong addInteractiveEventListeners(VisuUiRenderingWindow *window,
					   InteractiveEventsId id);
static GtkWidget* buildCameraMenu(VisuUiRenderingWindow *window);
static void _setLabelSize(GtkInfoArea *info, gint width, gint height);
static void _setFileDescription(GtkInfoArea *info, gchar *message);
static void _setNNodes(GtkInfoArea *info, gint nb);
static void getOpenGLAreaSize(VisuUiRenderingWindow *window,
			      guint *width, guint *height);
static void _redraw(VisuUiRenderingWindow *window, gboolean forceRedraw);

/* Local callbacks */
static void visu_ui_rendering_window_dispose (GObject* obj);
static void visu_ui_rendering_window_finalize(GObject* obj);
static void onMarkClearClicked(GtkButton *button, gpointer data);
static void onNodeInfoClicked(GtkToggleButton *button, gpointer data);
static void onRaiseButtonClicked(VisuUiRenderingWindow *window, gpointer user_data);
static void displayFileInfoOnDataLoaded(VisuUiRenderingWindow *window);
static void onRenderingMethodChanged(VisuUiRenderingWindow *window, VisuRendering *method,
				     gpointer data);
static void setFileButtonsSensitive(VisuUiRenderingWindow *window);
static void onNodePopulationChanged(VisuData *data, int *nodes, gpointer user_data);
static gboolean onDragMotion(GtkWidget *widget, GdkDragContext *context,
			     gint x, gint y, guint t, gpointer user_data);
static void onDropData(VisuUiRenderingWindow *window, GdkDragContext *context,
		       gint x, gint y, GtkSelectionData *selection_data,
		       guint target_type, guint time, GtkWidget *glArea);
static gboolean onCameraMenu(VisuUiRenderingWindow *window, GdkEventButton *event,
			     GtkEventBox *ev);
static void onCameraMenuSelected(GtkMenuShell *menushell, gpointer user_data);
static void onCameraMenuClicked(GtkMenuItem *menuitem, gpointer user_data);
static void onCameraMenuCurrentClicked(GtkMenuItem *menuitem, gpointer user_data);
static void onCameraMenuOrientationClicked(GtkMenuItem *menuitem, gpointer user_data);
static void minimalPickInfo(VisuInteractive *inter, VisuInteractivePick pick,
			    VisuNode *node0, VisuNode *node1, VisuNode *node2,
                            gpointer data);
static void minimalPickError(VisuInteractive *inter,
			     VisuInteractivePickError error, gpointer data);
static void onCancelButtonClicked(GtkButton *button, gpointer data);
static gboolean onCameraAccel(GtkAccelGroup *accel, GObject *obj,
                              guint key, GdkModifierType mod, gpointer data);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 17
static void _onSearchClose(GtkInfoBar *bar, gint response, gpointer data);
static void _onSearchEdited(GtkEntry *entry, gpointer data);
#endif

struct _VisuUiRenderingWindow
{
  GtkVBox generalVBox;
  gboolean dispose_has_run;

  /*********************************/
  /* Dealing with the OpenGL area. */
  /*********************************/
  /* The OpenGL area and it's notification zone. */
  GtkWidget *openGLArea;
  /* Stored size of the OpenGL area. */
  gint socketWidth, socketHeight;
  /* This pointer give the handle to rule all interactive actions. */
  GList *inters;
  /* Printed marks. */
  VisuGlExtMarks *marks;
  /* This is a list of currently connected
     signal for the interactive mode. */
  GList *interactiveEvents;
  /* A pointer on the current used cursor. */
  GdkCursor *currentCursor;
  GdkCursor *refCursor;
  /* Rendered extensions. */
  VisuGlExtNodes *extNodes;

  /*************************************/
  /* Dealing with the information bar. */
  /*************************************/
  /* TO BE INTEGRATED. */
  GtkInfoArea *info;
  /* TO BE INTEGRATED. */
  int nbStatusMessage;

  GtkAccelGroup *accel;

  /* A pointer to the currently loaded VisuData. */
  VisuData *currentData;
  /* Id of signals currently connected, related to the current #VisuData object. */
  gulong populationIncrease_id, populationDecrease_id;

  /* A pointer on the VisuGlView used for the rendering area. */
  VisuGlView *view;
};

struct _VisuUiRenderingWindowClass
{
  GtkVBoxClass parent_class;

  void (*renderingWindow) (VisuUiRenderingWindow *window);

  /* Action signals for keybindings, do not connect to these */
  void (*export) (VisuUiRenderingWindow *window);
  void (*open)   (VisuUiRenderingWindow *window);
  void (*reload) (VisuUiRenderingWindow *window);
  void (*orient) (VisuUiRenderingWindow *window);
  void (*search) (VisuUiRenderingWindow *window);

  GdkCursor *cursorRotate;
  GdkCursor *cursorWatch;
  GdkCursor *cursorPointer;
  GdkCursor *cursorPirate;
  GdkCursor *cursorGrab;
  GdkCursor *currentCursor;

  gboolean useReducedCoordinates;
  gboolean autoAdjust;

  /* To be removed when redraw is a signal object. */
  VisuUiRenderingWindow *redrawWidget;
};

static VisuUiRenderingWindowClass *my_class = NULL;

G_DEFINE_TYPE(VisuUiRenderingWindow, visu_ui_rendering_window, GTK_TYPE_VBOX)

/* Local callbacks */
static gboolean timeOutPopMessage(gpointer data);
static void onSizeChangeEvent(GtkWidget *widget, GtkAllocation *allocation, gpointer user_data);
static void onRedraw(VisuUiRenderingWindow *window, gpointer data);
static void onForceRedraw(VisuUiRenderingWindow *window, gpointer data);
static void onRealiseEvent(GtkWidget *wd, gpointer data);
static void onExport(VisuUiRenderingWindow *window);
static void onOpen(VisuUiRenderingWindow *window);
static void _onSearch(VisuUiRenderingWindow *window);
static void _orientationChooser(VisuUiRenderingWindow *window);
static gboolean _onSearchEsc(GtkWidget *widget, GdkEventKey *event, gpointer data);

/* Interactive mode listeners. */
static gboolean onKeyPressed(GtkWidget *widget, GdkEventKey *event, gpointer data);
static gboolean onKeyRelease(GtkWidget *widget, GdkEventKey *event, gpointer data);
static gboolean onMouseMotion(GtkWidget *widget, GdkEventMotion *event, gpointer user_data);
static gboolean onScrollEvent(GtkWidget *widget, GdkEventScroll *event, gpointer user_data);
static gboolean onButtonAction(VisuUiRenderingWindow *window, GdkEventButton *event, gpointer user_data);

static void visu_ui_rendering_window_class_init(VisuUiRenderingWindowClass *klass)
{
  GtkBindingSet *binding_set;
  VisuConfigFileEntry *resourceEntry;

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: creating the class of the widget.\n");

  /* Initialisation des curseurs utiles. */
  klass->cursorPirate  = gdk_cursor_new(GDK_PIRATE);
  klass->cursorRotate  = gdk_cursor_new(GDK_EXCHANGE);
  klass->cursorWatch   = gdk_cursor_new(GDK_WATCH);
  klass->cursorPointer = gdk_cursor_new(GDK_DOTBOX);
  klass->cursorGrab    = gdk_cursor_new(GDK_FLEUR);
  
  klass->export = onExport;
  klass->reload = visu_ui_rendering_window_reload;
  klass->open   = onOpen;
  klass->orient = _orientationChooser;
  klass->search = _onSearch;

  klass->useReducedCoordinates = FALSE;
  klass->autoAdjust            = TRUE;

  inter = visu_interactive_new(interactive_measureAndObserve);

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: connect the signals.\n");
  G_OBJECT_CLASS(klass)->dispose  = visu_ui_rendering_window_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_ui_rendering_window_finalize;

  /**
   * VisuUiRenderingWindow::export:
   * @window: the object emitting the signal.
   *
   * Signal emitted when the user ask for data export.
   *
   * Since: 3.6
   */
  _signals[EXPORT_SIGNAL] =
    g_signal_new("export", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                 G_STRUCT_OFFSET(VisuUiRenderingWindowClass, export),
                 NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);
  /**
   * VisuUiRenderingWindow::open:
   * @window: the object emitting the signal.
   *
   * Signal emitted when the user ask to open new data.
   *
   * Since: 3.6
   */
  _signals[OPEN_SIGNAL] =
    g_signal_new("open", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                 G_STRUCT_OFFSET(VisuUiRenderingWindowClass, open),
                 NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);
  /**
   * VisuUiRenderingWindow::reload:
   * @window: the object emitting the signal.
   *
   * Signal emitted when the user ask to reload current data.
   *
   * Since: 3.6
   */
  _signals[RELOAD_SIGNAL] =
    g_signal_new("reload", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                 G_STRUCT_OFFSET(VisuUiRenderingWindowClass, reload),
                 NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);
  /**
   * VisuUiRenderingWindow::search:
   * @window: the object emitting the signal.
   *
   * Signal emitted when the user ask to search info in current data.
   *
   * Since: 3.7
   */
  _signals[SEARCH_SIGNAL] =
    g_signal_new("search", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                 G_STRUCT_OFFSET(VisuUiRenderingWindowClass, search),
                 NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);
  /**
   * VisuUiRenderingWindow::show-action-dialog:
   * @window: the object emitting the signal.
   *
   * Signal emitted when the user ask to show the action dialog.
   *
   * Since: 3.6
   */
  _signals[SHOW_ACTION_DIALOG_SIGNAL] =
    g_signal_new("show-action-dialog", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS | G_SIGNAL_ACTION,
                 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);
  /**
   * VisuUiRenderingWindow::show-main-panel:
   * @window: the object emitting the signal.
   *
   * Signal emitted when the user ask to raise the main panel.
   *
   * Since: 3.6
   */
  _signals[SHOW_MAIN_PANEL_SIGNAL] =
    g_signal_new("show-main-panel", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS | G_SIGNAL_ACTION,
                 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);
  /**
   * VisuUiRenderingWindow::show-orientation-chooser:
   * @window: the object emitting the signal.
   *
   * Signal emitted when the user ask to precisely select a camera angle.
   *
   * Since: 3.7
   */
  _signals[SHOW_ORIENTATION_SIGNAL] =
    g_signal_new("show-orientation-chooser", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                 G_STRUCT_OFFSET(VisuUiRenderingWindowClass, orient),
                 NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);
  /**
   * VisuUiRenderingWindow::load-next-file:
   * @window: the object emitting the signal.
   *
   * Signal emitted when the user ask to load next file of a given list.
   *
   * Since: 3.7
   */
  _signals[LOAD_NEXT_FILE_SIGNAL] =
    g_signal_new("load-next-file", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS | G_SIGNAL_ACTION,
                 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);
  /**
   * VisuUiRenderingWindow::load-prev-file:
   * @window: the object emitting the signal.
   *
   * Signal emitted when the user ask to load previous file of a given list.
   *
   * Since: 3.7
   */
  _signals[LOAD_PREV_FILE_SIGNAL] =
    g_signal_new("load-prev-file", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS | G_SIGNAL_ACTION,
                 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: connect the bindings.\n");
  binding_set = gtk_binding_set_by_class(klass);
  gtk_binding_entry_add_signal(binding_set, GDK_KEY_s, GDK_CONTROL_MASK,
                               "export", 0);
  gtk_binding_entry_add_signal(binding_set, GDK_KEY_o, GDK_CONTROL_MASK,
                               "open", 0);
  gtk_binding_entry_add_signal(binding_set, GDK_KEY_r, GDK_CONTROL_MASK,
                               "reload", 0);
  gtk_binding_entry_add_signal(binding_set, GDK_KEY_v, GDK_CONTROL_MASK,
                               "show-orientation-chooser", 0);
  gtk_binding_entry_add_signal(binding_set, GDK_KEY_i, GDK_CONTROL_MASK,
                               "show-action-dialog", 0);
  gtk_binding_entry_add_signal(binding_set, GDK_KEY_f, GDK_CONTROL_MASK,
                               "search", 0);
  gtk_binding_entry_add_signal(binding_set, GDK_KEY_Home, 0, "show-main-panel", 0);
  gtk_binding_entry_add_signal(binding_set, GDK_KEY_n, 0, "load-next-file", 0);
  gtk_binding_entry_add_signal(binding_set, GDK_KEY_p, 0, "load-prev-file", 0);

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: add the resources.\n");
  resourceEntry = visu_config_file_addBooleanEntry(VISU_CONFIG_FILE_PARAMETER,
                                                   FLAG_PARAMETER_RED_COORD,
                                                   DESC_PARAMETER_RED_COORD,
                                                   &klass->useReducedCoordinates);
  visu_config_file_entry_setVersion(resourceEntry, 3.6f);
  resourceEntry = visu_config_file_addBooleanEntry(VISU_CONFIG_FILE_PARAMETER,
                                                   FLAG_PARAMETER_AUTO_ADJUST,
                                                   DESC_PARAMETER_AUTO_ADJUST,
                                                   &klass->autoAdjust);
  visu_config_file_entry_setVersion(resourceEntry, 3.6f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_PARAMETER,
				   exportParameters);

  my_class = klass;
}

static void visu_ui_rendering_window_dispose(GObject* obj)
{
  GList *ptList;
  InteractiveEvents *event;
  VisuUiRenderingWindow *window;
  VisuData *dataObj;

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: dispose object %p.\n", (gpointer)obj);

  window = VISU_UI_RENDERING_WINDOW(obj);
  if (window->dispose_has_run)
    return;
  window->dispose_has_run = TRUE;

  dataObj = window->currentData;
  window->currentData = (VisuData*)0;
  if (dataObj)
    DBG_fprintf(stderr, " | current data has %d ref counts.\n",
                G_OBJECT(dataObj)->ref_count);

  for (ptList = window->inters; ptList; ptList = g_list_next(ptList))
    visu_ui_rendering_window_popInteractive(window, VISU_INTERACTIVE(ptList->data));

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: releasing current handles.\n");
  if (window->info->progressId)
    g_source_remove(window->info->progressId);
  visu_gl_ext_marks_setData(window->marks, (VisuData*)0);
  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: release current data handle.\n");
  if (dataObj)
    {
      DBG_fprintf(stderr, " | current data has %d ref counts.\n",
                  G_OBJECT(dataObj)->ref_count);
      g_object_unref(dataObj);
    }
  if (window->marks)
    g_object_unref(window->marks);
  g_object_unref(window->info->cancel);
  if (window->view)
    g_object_unref(window->view);

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: removing interactive listeners.\n");
  for (ptList = window->interactiveEvents; ptList;
       ptList = g_list_next(ptList))
    {
      event = (InteractiveEvents*)ptList->data;
      DBG_fprintf(stderr, "  | disconnecting %d signal.\n", event->id);
      g_signal_handler_disconnect(G_OBJECT(window->openGLArea),
                                  event->callbackId);
      g_free(ptList->data);
    }
  if (window->interactiveEvents)
    g_list_free(window->interactiveEvents);
  window->interactiveEvents = (GList*)0;

  g_object_unref(window->accel);

  DBG_fprintf(stderr, " | ext nodes has %d ref counts.\n",
              G_OBJECT(window->extNodes)->ref_count);
  g_object_unref(window->extNodes);
  if (DEBUG && dataObj)
    DBG_fprintf(stderr, " | current data has %d ref counts.\n",
                G_OBJECT(dataObj)->ref_count);

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: chain to parent.\n");
  G_OBJECT_CLASS(visu_ui_rendering_window_parent_class)->dispose(obj);
  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: dispose done.\n");
}
static void visu_ui_rendering_window_finalize(GObject* obj)
{
  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: finalize object %p.\n", (gpointer)obj);

  g_free(VISU_UI_RENDERING_WINDOW(obj)->info);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_rendering_window_parent_class)->finalize(obj);
}

static void visu_ui_rendering_window_init(VisuUiRenderingWindow *renderingWindow)
{
  guint n;
  GClosure *closure;

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: initializing new object (%p).\n",
	      (gpointer)renderingWindow);

  renderingWindow->currentData           = (VisuData*)0;
  renderingWindow->populationDecrease_id = 0;
  renderingWindow->populationIncrease_id = 0;
  renderingWindow->view                  = (VisuGlView*)0;

  /* Binding for the camera menu. */
  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: connect the camera bindings.\n");
  renderingWindow->accel = gtk_accel_group_new();
  gtk_accel_map_add_entry(g_intern_static_string(MENU_CAMERA_RESTORE), GDK_KEY_r, 0);
  gtk_accel_map_add_entry(g_intern_static_string(MENU_CAMERA_SAVE), GDK_KEY_s, 0);
  gtk_accel_map_add_entry(g_intern_static_string(MENU_CAMERA_ORIENT),
                          GDK_KEY_v, GDK_CONTROL_MASK);
  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: connect the camera numbered bindings.\n");
  for (n = 0; n < 9; n++)
    {
      gtk_accel_map_add_entry(g_intern_static_string(cameraAccels[n]),
                              cameraKeys[n], GDK_CONTROL_MASK);
      closure = g_cclosure_new(G_CALLBACK(onCameraAccel),
                               (gpointer)renderingWindow,
                               (GClosureNotify)0);
      gtk_accel_group_connect_by_path(renderingWindow->accel,
                                      g_intern_static_string(cameraAccels[n]),
                                      closure);
      g_closure_unref(closure);
    }

  g_signal_connect(G_OBJECT(inter), "node-selection",
		   G_CALLBACK(minimalPickInfo), (gpointer)renderingWindow);
  g_signal_connect(G_OBJECT(inter), "selection-error",
		   G_CALLBACK(minimalPickError), (gpointer)renderingWindow);

  /* Add VisuGlExt to the OpenGL area. */
  renderingWindow->extNodes = visu_gl_ext_nodes_new();
  visu_interactive_setNodeList(inter, renderingWindow->extNodes);
  DBG_fprintf(stderr, " | ext nodes has %d ref counts.\n",
              G_OBJECT(renderingWindow->extNodes)->ref_count);
}

/**
 * visu_ui_rendering_window_new:
 * @width: its desired width ;
 * @height: its desired height ;
 * @withFrame: a boolean ;
 * @withToolBar: a boolean.
 *
 * A #VisuUiRenderingWindow widget is a GtkWindow that have an area for
 * OpenGL drawing and a statusBar with many stuff like action buttons,
 * real status bar for notifications, ... The rendering area can be
 * drawn with a frame or not. With this routine, only the
 * #VisuUiRenderingWindow widget is created.
 *
 * Returns: a newly created #VisuUiRenderingWindow widget.
 */
GtkWidget* visu_ui_rendering_window_new(int width, int height, gboolean withFrame,
                                     gboolean withToolBar)
{
  VisuUiRenderingWindow *renderingWindow;
  GtkWidget *wd;
  GtkTargetList *target_list;

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: create a new VisuUiRenderingWindow object.\n");

  renderingWindow = VISU_UI_RENDERING_WINDOW(g_object_new(visu_ui_rendering_window_get_type(), NULL));

  /* We create the statusinfo area. */
  renderingWindow->info = gtkStatusInfo_createBar(renderingWindow, width, height,
						  withToolBar);
  wd = renderingWindow->info->area;
  gtk_box_pack_end(GTK_BOX(renderingWindow), wd, FALSE, FALSE, 0);

  renderingWindow->openGLArea = visu_ui_gl_widget_new(TRUE);
  gtk_widget_set_can_focus(renderingWindow->openGLArea, TRUE);
  g_signal_connect_swapped(G_OBJECT(renderingWindow->openGLArea), "realize",
			   G_CALLBACK(onRealiseEvent), (gpointer)renderingWindow);

  /* DnD */
  gtk_drag_dest_set(renderingWindow->openGLArea,
                    (GTK_DEST_DEFAULT_ALL), NULL, 0, GDK_ACTION_COPY);
  target_list = gtk_target_list_new(NULL, 0);
  gtk_target_list_add_uri_targets(target_list, TEXT_URI_LIST);
  gtk_target_list_add_text_targets(target_list, TEXT_PLAIN);
  gtk_drag_dest_set_target_list(renderingWindow->openGLArea, target_list);
  gtk_target_list_unref(target_list);
  g_signal_connect(renderingWindow->openGLArea, "drag-motion",
		   G_CALLBACK(onDragMotion), NULL);
  g_signal_connect_swapped(renderingWindow->openGLArea, "drag-data-received",
			   G_CALLBACK(onDropData), (gpointer)renderingWindow);

  gtk_widget_set_size_request(renderingWindow->openGLArea, width, height);
  /* Attach no redraw redraw method. */
  visu_ui_gl_widget_setRedraw(VISU_UI_GL_WIDGET(renderingWindow->openGLArea),
                              (VisuUiGlWidgetRedrawMethod)0, (VisuGlView*)0);
  if (withFrame)
    {
      wd = gtk_frame_new(NULL);
      gtk_frame_set_shadow_type(GTK_FRAME(wd), GTK_SHADOW_ETCHED_IN);
      gtk_box_pack_start(GTK_BOX(renderingWindow), wd, TRUE, TRUE, 0);
      gtk_container_add(GTK_CONTAINER(wd), renderingWindow->openGLArea);
    }
  else
    gtk_box_pack_start(GTK_BOX(renderingWindow),
		       renderingWindow->openGLArea, TRUE, TRUE, 0);
  
  /* We physically show the vbox. */
  gtk_widget_show_all(GTK_WIDGET(renderingWindow));

  /* Unset the size request. */
  renderingWindow->socketWidth = width;
  renderingWindow->socketHeight = height;
  /* Set a listener on the size to show the warning icon accordingly. */
  g_signal_connect(G_OBJECT(renderingWindow->openGLArea), "size-allocate",
		   G_CALLBACK(onSizeChangeEvent), (gpointer)renderingWindow);

  /* Set local variables. */
  DBG_fprintf(stderr, "                - setup the local variables.\n");
  renderingWindow->nbStatusMessage          = 0;
  renderingWindow->interactiveEvents        = (GList*)0;
  renderingWindow->inters                   = (GList*)0;
  renderingWindow->marks                    = visu_gl_ext_marks_new(NULL);
  renderingWindow->currentCursor            = 
    VISU_UI_RENDERING_WINDOW_CLASS(G_OBJECT_GET_CLASS(renderingWindow))->cursorPirate;
  renderingWindow->refCursor                = 
    VISU_UI_RENDERING_WINDOW_CLASS(G_OBJECT_GET_CLASS(renderingWindow))->cursorPirate;

  g_signal_connect_swapped(VISU_OBJECT_INSTANCE, "OpenGLAskForReDraw",
			   G_CALLBACK(onRedraw), (gpointer)renderingWindow);
  g_signal_connect_swapped(VISU_OBJECT_INSTANCE, "OpenGLForceReDraw",
			   G_CALLBACK(onForceRedraw), (gpointer)renderingWindow);
  g_signal_connect_swapped(VISU_OBJECT_INSTANCE, "renderingChanged",
			   G_CALLBACK(onRenderingMethodChanged),
			   (gpointer)renderingWindow);
  onRenderingMethodChanged(renderingWindow,
                           visu_object_getRendering(VISU_OBJECT_INSTANCE),
			   (gpointer)0);

  /* Additional initialisations. */
  renderingWindow->view = visu_gl_view_new_withSize(width, height);

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: Building OK.\n");

  return GTK_WIDGET(renderingWindow);
}

static GtkInfoArea *gtkStatusInfo_createBar(VisuUiRenderingWindow *window, gint width,
					    gint height, gboolean withToolBar)
{
  GtkWidget *hbox;
  GtkWidget *wd, *image, *ev;
  GtkInfoArea *info;
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;
  tooltips = gtk_tooltips_new ();
#endif

  info = g_malloc(sizeof(GtkInfoArea));

  info->area = gtk_vbox_new(FALSE, 0);

#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 17
  info->infoBar = gtk_info_bar_new();
  wd = gtk_info_bar_add_button(GTK_INFO_BAR(info->infoBar), GTK_STOCK_CLOSE,
                               GTK_RESPONSE_CLOSE);
  gtk_button_set_focus_on_click(GTK_BUTTON(wd), FALSE);
  gtk_widget_set_no_show_all(info->infoBar, TRUE);
  g_signal_connect(G_OBJECT(info->infoBar), "response",
                   G_CALLBACK(_onSearchClose), (gpointer)window);
  g_signal_connect(G_OBJECT(info->infoBar), "key-press-event",
                   G_CALLBACK(_onSearchEsc), (gpointer)window);
  gtk_box_pack_start(GTK_BOX(info->area), info->infoBar, FALSE, FALSE, 0);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(gtk_info_bar_get_content_area(GTK_INFO_BAR(info->infoBar))), hbox);
  wd = gtk_label_new(_("Toggle highlight for node: "));
  gtk_box_pack_start(GTK_BOX(hbox), wd, FALSE, FALSE, 0);
  info->searchEntry = gtk_entry_new();
  gtk_entry_set_width_chars(GTK_ENTRY(info->searchEntry), 20);
  g_signal_connect(G_OBJECT(info->searchEntry), "activate",
                   G_CALLBACK(_onSearchEdited), (gpointer)window);
  gtk_box_pack_start(GTK_BOX(hbox), info->searchEntry, FALSE, FALSE, 0);
  gtk_widget_show_all(hbox);
#endif

  info->fileInfoFreeze = FALSE;
  info->hboxFileInfo = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(info->area), info->hboxFileInfo, FALSE, FALSE, 1);

  /* Size info */
  wd = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(info->hboxFileInfo), wd, FALSE, FALSE, 5);
  info->labelSize = gtk_label_new("");
  gtk_label_set_use_markup(GTK_LABEL(info->labelSize), TRUE);
  _setLabelSize(info, width, height);
  gtk_box_pack_start(GTK_BOX(wd), info->labelSize, FALSE, FALSE, 0);

  wd = gtk_vseparator_new();
  gtk_box_pack_start(GTK_BOX(info->hboxFileInfo), wd, FALSE, FALSE, 0);

  /* Nb nodes */
  info->labelNb = gtk_label_new("");
  gtk_label_set_use_markup(GTK_LABEL(info->labelNb), TRUE);
  _setNNodes(info, -1);
  gtk_box_pack_start(GTK_BOX(info->hboxFileInfo), info->labelNb, FALSE, FALSE, 5);

  wd = gtk_vseparator_new();
  gtk_box_pack_start(GTK_BOX(info->hboxFileInfo), wd, FALSE, FALSE, 0);

  /* File info */
  wd = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(info->hboxFileInfo), wd, TRUE, TRUE, 5);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 7
  image = gtk_image_new_from_stock(GTK_STOCK_INFO,
				   GTK_ICON_SIZE_MENU);
#else
  image = gtk_image_new_from_stock(GTK_STOCK_SAVE,
				   GTK_ICON_SIZE_MENU);
#endif
  gtk_box_pack_start(GTK_BOX(wd), image, FALSE, FALSE, 1);
  info->labelFileInfo = gtk_label_new("");
  gtk_label_set_use_markup(GTK_LABEL(info->labelFileInfo), TRUE);
  gtk_misc_set_alignment(GTK_MISC(info->labelFileInfo), 0., 0.5);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  gtk_label_set_ellipsize(GTK_LABEL(info->labelFileInfo), PANGO_ELLIPSIZE_END);
#endif
  _setFileDescription(info, GTK_STATUSINFO_NOFILEINFO);
  gtk_box_pack_start(GTK_BOX(wd), info->labelFileInfo, TRUE, TRUE, 0);
  ev = gtk_event_box_new();
  gtk_widget_set_tooltip_text(ev, _("Click here to get the list of"
				    " saved camera positions.\n"
				    "Use 's' and 'r' keys to save and"
				    " restore camera settings. <Shift> + 's'"
                                    " remove the current camera from the list."));
  g_signal_connect_swapped(G_OBJECT(ev), "button-release-event",
                           G_CALLBACK(onCameraMenu), (gpointer)window);
  gtk_box_pack_end(GTK_BOX(wd), ev, FALSE, FALSE, 0);
  image = gtk_image_new_from_stock(GTK_STOCK_ZOOM_FIT,
				   GTK_ICON_SIZE_MENU);
  gtk_container_add(GTK_CONTAINER(ev), image);

  /* Status */
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(info->area), hbox, FALSE, FALSE, 0);

  /* Handle box for action buttons. */
  if (withToolBar)
    {
      /* The container */
      info->hboxTools = gtk_hbox_new(TRUE, 0);
      gtk_box_pack_start(GTK_BOX(hbox), info->hboxTools, FALSE, FALSE, 0);

      /* Load button */
      info->loadButton = gtk_button_new();
      g_object_set(G_OBJECT(info->loadButton), "can-default", FALSE, "can-focus", FALSE,
                   "has-default", FALSE, "has-focus", FALSE, NULL);
      gtk_widget_set_sensitive(info->loadButton, FALSE);
      gtk_button_set_focus_on_click(GTK_BUTTON(info->loadButton), FALSE);
      gtk_widget_set_tooltip_text(info->loadButton,
                                  _("Open Ctrl+o"));
      g_signal_connect_swapped(G_OBJECT(info->loadButton), "clicked",
			       G_CALLBACK(onOpen), (gpointer)window);
      image = gtk_image_new_from_stock(GTK_STOCK_OPEN,
				       GTK_ICON_SIZE_MENU);
      gtk_container_add(GTK_CONTAINER(info->loadButton), image);
      gtk_box_pack_start(GTK_BOX(info->hboxTools), info->loadButton, FALSE, FALSE, 0);
      /* Refresh button */
      wd = gtk_button_new();
      g_object_set(G_OBJECT(wd), "can-default", FALSE, "can-focus", FALSE,
                   "has-default", FALSE, "has-focus", FALSE, NULL);
      gtk_button_set_focus_on_click(GTK_BUTTON(wd), FALSE);
      gtk_widget_set_tooltip_text(wd,
                                  _("Reload the current file Ctrl+r"));
      g_signal_connect_swapped(G_OBJECT(wd), "clicked",
                               G_CALLBACK(visu_ui_rendering_window_reload), (gpointer)window);
      image = gtk_image_new_from_stock(GTK_STOCK_REFRESH,
				       GTK_ICON_SIZE_MENU);
      gtk_container_add(GTK_CONTAINER(wd), image);
      gtk_box_pack_start(GTK_BOX(info->hboxTools), wd, FALSE, FALSE, 0);
      info->reloadButton = wd;
      /* Save button */
      info->dumpButton = gtk_button_new();
      g_object_set(G_OBJECT(info->dumpButton), "can-default", FALSE, "can-focus", FALSE,
                   "has-default", FALSE, "has-focus", FALSE, NULL);
      gtk_button_set_focus_on_click(GTK_BUTTON(info->dumpButton), FALSE);
      gtk_widget_set_tooltip_text(info->dumpButton,
                                  _("Export Ctrl+s"));
      g_signal_connect_swapped(G_OBJECT(info->dumpButton), "clicked",
                               G_CALLBACK(onExport), (gpointer)window);
      gtk_widget_set_sensitive(info->dumpButton, FALSE);
      image = gtk_image_new_from_stock(GTK_STOCK_SAVE_AS,
				       GTK_ICON_SIZE_MENU);
      gtk_container_add(GTK_CONTAINER(info->dumpButton), image);
      gtk_box_pack_start(GTK_BOX(info->hboxTools), info->dumpButton, FALSE, FALSE, 0);
      /* Auto-raise command panel button */
      info->raiseButton = gtk_button_new();
      g_object_set(G_OBJECT(info->raiseButton), "can-default", FALSE, "can-focus", FALSE,
                   "has-default", FALSE, "has-focus", FALSE, NULL);
      gtk_button_set_focus_on_click(GTK_BUTTON(info->raiseButton), FALSE);
      gtk_widget_set_tooltip_text(info->raiseButton,
                                  _("Raise the command panel window.\n"
                                    "  Use <home> as key binding."));
      g_signal_connect_swapped(G_OBJECT(info->raiseButton), "clicked",
			       G_CALLBACK(onRaiseButtonClicked), (gpointer)window);
      image = gtk_image_new_from_stock(GTK_STOCK_GO_UP,
				       GTK_ICON_SIZE_MENU);
      gtk_widget_show(image);
      gtk_container_add(GTK_CONTAINER(info->raiseButton), image);
      /* gtk_widget_set_no_show_all(info->raiseButton, TRUE); */
      gtk_box_pack_start(GTK_BOX(info->hboxTools), info->raiseButton, FALSE, FALSE, 0);
    }
  else
    {
      info->loadButton = (GtkWidget*)0;
      info->dumpButton = (GtkWidget*)0;
      info->raiseButton = (GtkWidget*)0;
    }

  /* The status bar or progress */
  info->statusInfo = gtk_statusbar_new();
  gtk_box_pack_start(GTK_BOX(hbox), info->statusInfo, TRUE, TRUE, 0);
#if GTK_MAJOR_VERSION < 3
  gtk_statusbar_set_has_resize_grip(GTK_STATUSBAR(info->statusInfo), FALSE);
#endif
  info->statusInfoId = gtk_statusbar_get_context_id(GTK_STATUSBAR(info->statusInfo),
                                                    "OpenGL statusbar.");
  gtk_widget_set_no_show_all(info->statusInfo, TRUE);
  gtk_widget_show(info->statusInfo);
  info->progressId = 0;
  info->progress = gtk_progress_bar_new();
  gtk_box_pack_start(GTK_BOX(hbox), info->progress, TRUE, TRUE, 0);
  gtk_widget_set_no_show_all(info->progress, TRUE);
  info->cancelButton = gtk_button_new_from_stock(GTK_STOCK_CANCEL);
  gtk_box_pack_start(GTK_BOX(hbox), info->cancelButton, FALSE, FALSE, 0);
  gtk_widget_set_no_show_all(info->cancelButton, TRUE);
  g_signal_connect(G_OBJECT(info->cancelButton), "clicked",
                   G_CALLBACK(onCancelButtonClicked), (gpointer)window);
  info->cancel = g_cancellable_new();
  
  /* The interactive button zone. */
  info->hboxInteractive = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), info->hboxInteractive, FALSE, FALSE, 0);

  /* Action button */
  wd = gtk_toggle_button_new();
  gtk_widget_set_sensitive(wd, FALSE);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(wd), FALSE);
  gtk_button_set_focus_on_click(GTK_BUTTON(wd), FALSE);
  g_object_set(G_OBJECT(wd), "can-default", FALSE, "can-focus", FALSE,
	       "has-default", FALSE, "has-focus", FALSE, NULL);
  info->infoClicked = g_signal_connect(G_OBJECT(wd), "clicked",
				       G_CALLBACK(onNodeInfoClicked),
				       (gpointer)window);
  gtk_widget_set_tooltip_text(wd, _("Measure / remove information"
				    " for the selected node."));
  image = gtk_image_new_from_stock(GTK_STOCK_PROPERTIES,
				   GTK_ICON_SIZE_MENU);
  gtk_container_add(GTK_CONTAINER(wd), image);
  gtk_box_pack_end(GTK_BOX(info->hboxInteractive), wd, FALSE, FALSE, 0);
  info->infoButton = wd;
  g_object_set_data_full(G_OBJECT(wd), "selectedNodeId",
			 g_malloc(sizeof(gint)), g_free);

  /* Clean marks button */
  wd = gtk_button_new();
  gtk_widget_set_sensitive(wd, FALSE);
  gtk_button_set_focus_on_click(GTK_BUTTON(wd), FALSE);
  g_object_set(G_OBJECT(wd), "can-default", FALSE, "can-focus", FALSE,
	       "has-default", FALSE, "has-focus", FALSE, NULL);
  g_signal_connect(G_OBJECT(wd), "clicked",
		   G_CALLBACK(onMarkClearClicked), (gpointer)window);
  gtk_widget_set_tooltip_text(wd, _("Remove all measurement marks."));
  image = gtk_image_new_from_stock(GTK_STOCK_CLEAR,
				   GTK_ICON_SIZE_MENU);
  gtk_container_add(GTK_CONTAINER(wd), image);
  gtk_box_pack_end(GTK_BOX(info->hboxInteractive), wd, FALSE, FALSE, 0);
  info->clearMarksButton = wd;

  return info;
}

static void onSizeChangeEvent(GtkWidget *widget _U_,
			      GtkAllocation *allocation, gpointer user_data)
{
  VisuUiRenderingWindow *window;

  window = VISU_UI_RENDERING_WINDOW(user_data);
  g_return_if_fail(window);

  /* Return if no changes in size (this event is called even the size
     is not really changed but has been negociated. */
  if (window->socketWidth == allocation->width &&
      window->socketHeight == allocation->height)
    return;

  window->socketWidth = allocation->width;
  window->socketHeight = allocation->height;
  _setLabelSize(window->info,
                window->socketWidth,
                window->socketHeight);

  /* If data are currently rendered on this window,
     we ask these data to update to the new size. */
  if (window->view)
    visu_gl_view_setViewport(window->view, allocation->width, allocation->height);
}

static void onRealiseEvent(GtkWidget *wd, gpointer data _U_)
{
  guint w, h;
  VisuUiRenderingWindow *window;

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: initializing OpenGL variable for"
	      "the new OpenGL area.\n");
  /* Set V_Sim OpenGL options. */
  visu_gl_initContext();
  DBG_fprintf(stderr, " | openGL context OK\n");

  window = VISU_UI_RENDERING_WINDOW(wd);

  /* If we have a VisuData object attached, we set its size. */
  if (window->view)
    {
      w = h = 0;
      getOpenGLAreaSize(window, &w, &h);
      visu_gl_view_setViewport(window->view, w, h);
      if (window->currentData)
        {
          visu_boxed_setBox(VISU_BOXED(window->view), VISU_BOXED(window->currentData),
                            VISU_UI_RENDERING_WINDOW_GET_CLASS(window)->autoAdjust);
          visu_gl_ext_rebuildAll();
        }
    }
  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: changing the cursor.\n");
  /* We set the cursor. */
  gdk_window_set_cursor(gtk_widget_get_window(window->openGLArea),
			window->currentCursor);  
  DBG_fprintf(stderr, " | cursor OK.\n");

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: changing the minimum size.\n");
  gtk_widget_set_size_request(window->openGLArea, 100, 100);

  gtk_widget_grab_focus(wd);
}

static gboolean onDragMotion(GtkWidget *widget, GdkDragContext *context,
			     gint x _U_, gint y _U_, guint t, gpointer data _U_)
{
  /*   GList *tmpLst; */
  GdkAtom atom;

  /*   DBG_fprintf(stderr, "VisuUiRenderingWindow: Hey ! You dnd move something !\n"); */
  /*   for (tmpLst = context->targets; tmpLst; tmpLst = g_list_next(tmpLst)) */
  /*     { */
  /*       DBG_fprintf(stderr, " | dnd: '%s'\n", */
  /* 		  gdk_atom_name(GDK_POINTER_TO_ATOM(tmpLst->data))); */
  /*     } */
  atom = gtk_drag_dest_find_target(widget, context,
				   gtk_drag_dest_get_target_list(widget));
  if (atom != GDK_NONE)
    gdk_drag_status(context, GDK_ACTION_COPY, t);
  else
    gdk_drag_status(context, 0, t);
  return (atom != GDK_NONE);
}

static void onDropData(VisuUiRenderingWindow *window, GdkDragContext *context,
		       gint x _U_, gint y _U_, GtkSelectionData *data,
		       guint type, guint time _U_, GtkWidget *glArea _U_)
{
  gchar **filenames;
  int i, n, delta;
  VisuData *newData;

  if (window == NULL || context == NULL || data == NULL ||
      gtk_selection_data_get_length(data) < 0)
    return;
  
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 21
  if (gdk_drag_context_get_suggested_action(context) != GDK_ACTION_COPY)
    return;
#else
  if (context->action != GDK_ACTION_COPY)
    return;
#endif

  DBG_fprintf(stderr, " | data: '%d' -> '%s'\n", type,
  	      gtk_selection_data_get_data(data));

  switch (type)
    {
    case TEXT_URI_LIST:
      filenames = gtk_selection_data_get_uris(data);
      break;
    case TEXT_PLAIN:
      filenames = g_strsplit((gchar*)gtk_selection_data_get_data(data), "\n", -1);
      break;
    default:
      filenames = g_malloc(sizeof(gchar*));
      filenames[0] = (gchar*)0;
      break;
    }

  gtk_drag_finish(context, TRUE, TRUE, time);

  newData = visu_data_new();
  n = 0;
  for (i = 0; filenames[i]; i++)
    {
      g_strstrip(filenames[i]);
      if (filenames[i][0] != '\0')
  	{
  	  delta = (strncmp("file://", filenames[i], 7))?0:7;
  	  visu_data_addFile(newData, filenames[i] + delta, n, (ToolFileFormat*)0);
  	  n += 1;
  	}
    }
  g_strfreev(filenames);

  visu_ui_rendering_window_loadFile(window, newData, 0);

  VISU_REDRAW_ADD;
}

static void minimalPickInfo(VisuInteractive *inter _U_, VisuInteractivePick pick,
			    VisuNode *node0, VisuNode *node1, VisuNode *node2 _U_,
                            gpointer data)
{
  float posSelect[3], posRef[3], dist;
  GString *str;
  VisuUiRenderingWindow *window;
  int i;
  gint *id;
  const gchar *comment;
  VisuElement *ele;

  window = VISU_UI_RENDERING_WINDOW(data);
  g_return_if_fail(window);

  while (window->nbStatusMessage > 1)
    visu_ui_rendering_window_popMessage(window);
  gtk_widget_set_sensitive(window->info->infoButton, FALSE);

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: update the status bar after pick.\n");
  switch (pick)
    {
    case PICK_SELECTED:
      gtk_widget_set_sensitive(window->info->infoButton, TRUE);

      id = (gint*)g_object_get_data(G_OBJECT(window->info->infoButton),
				    "selectedNodeId");
      *id = (gint)node0->number;
      g_signal_handler_block(G_OBJECT(window->info->infoButton),
			     window->info->infoClicked);
      gtk_toggle_button_set_active
	(GTK_TOGGLE_BUTTON(window->info->infoButton),
	 visu_gl_ext_marks_getActive(window->marks, *id));
      g_signal_handler_unblock(G_OBJECT(window->info->infoButton),
			       window->info->infoClicked);
      if (VISU_UI_RENDERING_WINDOW_GET_CLASS(window)->useReducedCoordinates)
        {
          visu_data_getNodeUserPosition(window->currentData, node0, posRef);
          visu_box_convertXYZtoBoxCoordinates(visu_boxed_getBox(VISU_BOXED(window->currentData)),
                                              posSelect, posRef);
        }
      else
        visu_data_getNodeUserPosition(window->currentData, node0, posSelect);
      str = g_string_new(_("Selected node number "));
      ele = visu_node_array_getElement(VISU_NODE_ARRAY(window->currentData), node0);
      g_string_append_printf(str, "%d - %s (%7.3g;%7.3g;%7.3g)",
			     node0->number + 1, ele->name,
			     posSelect[0], posSelect[1], posSelect[2]);
      comment = visu_extra_node_getLabel(window->currentData, node0);
      if (comment)
	g_string_append_printf(str, " %s", comment);
      visu_ui_rendering_window_pushMessage(window, str->str);
      g_string_free(str, TRUE);
      return;
    case PICK_DISTANCE:
      /* Have a ref and a selected node, then distance informations is compued. */
      visu_data_getNodePosition(window->currentData, node0, posSelect);
      visu_data_getNodePosition(window->currentData, node1, posRef);
      str = g_string_new(_("Distance between nodes "));
      dist = 0.;
      for (i = 0; i < 3; i++)
	dist += (posRef[i] - posSelect[i]) * (posRef[i] - posSelect[i]);
      dist = sqrt(dist);
      g_string_append_printf(str, _("%d and %d : %7.3f"),
			     node1->number + 1, node0->number + 1, dist);
      visu_ui_rendering_window_pushMessage(window, str->str);
      g_string_free(str, TRUE);
      return;
    case PICK_REFERENCE_1:
      visu_ui_rendering_window_pushMessage(window, _("<shift> right-click on"
					    " background to unset reference."));
      return;
    case PICK_REFERENCE_2:
      visu_ui_rendering_window_pushMessage(window,
				  _("<ctrl> right-click on"
				    " background to unset second reference."));
      return;
    case PICK_UNREFERENCE_1:
    case PICK_UNREFERENCE_2:
      return;
    default:
      return;
    }
}
static void minimalPickError(VisuInteractive *inter _U_,
			     VisuInteractivePickError error, gpointer data)
{
  VisuUiRenderingWindow *window;

  window = VISU_UI_RENDERING_WINDOW(data);
  switch (error)
    {
    case PICK_ERROR_NO_SELECTION:
      visu_ui_rendering_window_pushMessage(window, _("No node has been selected."));
      return;
    case PICK_ERROR_SAME_REF:
      visu_ui_rendering_window_pushMessage(window, _("Picked node is already used"
					    " as a reference."));
      return;
    case PICK_ERROR_REF1:
      visu_ui_rendering_window_pushMessage(window, _("Can't pick a second reference"
					    " without any first one"
					    " (use <shift> right-click)."));
      return;
    case PICK_ERROR_REF2:
      visu_ui_rendering_window_pushMessage(window, _("Can't remove first reference"
					    " before removing the second one."));
      return;
    default:
      return;
    }
}

static gulong addInteractiveEventListeners(VisuUiRenderingWindow *window,
					   InteractiveEventsId id)
{
  GList* ptList;
  InteractiveEvents *event;
  gboolean found;

  g_return_val_if_fail(VISU_UI_IS_RENDERING_WINDOW(window), (gulong)0);

  found  = FALSE;
  for (ptList = window->interactiveEvents; ptList && !found;
       ptList = g_list_next(ptList))
    {
      event = (InteractiveEvents*)ptList->data;
      if (event->id == id)
	found = TRUE;
    }
  if (found)
    return (gulong)0;

  event = g_malloc(sizeof(InteractiveEvents));
  event->id = id;
  switch (id)
    {
    case event_button_press:
      event->callbackId = g_signal_connect_swapped(G_OBJECT(window->openGLArea),
						   "button-press-event",
						   G_CALLBACK(onButtonAction), (gpointer)window);
      break;
    case event_button_release:
      event->callbackId = g_signal_connect_swapped(G_OBJECT(window->openGLArea),
						   "button-release-event",
						   G_CALLBACK(onButtonAction), (gpointer)window);
      break;
    case event_motion_notify:
      event->callbackId = g_signal_connect(G_OBJECT(window->openGLArea),
					   "motion-notify-event",
					   G_CALLBACK(onMouseMotion), (gpointer)window);
      break;
    case event_key_press:
      event->callbackId = g_signal_connect(G_OBJECT(window->openGLArea),
					   "key-press-event",
					   G_CALLBACK(onKeyPressed), (gpointer)window);
      break;
    case event_key_release:
      event->callbackId = g_signal_connect(G_OBJECT(window->openGLArea),
					   "key-release-event",
					   G_CALLBACK(onKeyRelease), (gpointer)window);
      break;
    case event_scroll:
      event->callbackId = g_signal_connect(G_OBJECT(window->openGLArea),
					   "scroll-event",
					   G_CALLBACK(onScrollEvent), (gpointer)window);
      break;
    default:
      g_warning("Unknown event to add.");
      g_free(event);
      return (gulong)0;
    };
  window->interactiveEvents = g_list_prepend(window->interactiveEvents,
                                             (gpointer)event);
  return event->callbackId;
}
static void setInteractiveType(VisuUiRenderingWindow *window,
			       VisuInteractiveId type)
{
  VisuUiRenderingWindowClass *klass;
  InteractiveEvents *event;
  gulong id;
  GList *ptList;

  g_return_if_fail(VISU_UI_IS_RENDERING_WINDOW(window));  
  klass = VISU_UI_RENDERING_WINDOW_CLASS(G_OBJECT_GET_CLASS(window));
  g_return_if_fail(klass);

  /* We set the cursors. */
  switch (type)
    {
    case interactive_observe:
    case interactive_measureAndObserve:
      if (gtk_widget_get_visible(GTK_WIDGET(window)))
	gdk_window_set_cursor(gtk_widget_get_window(window->openGLArea),
			      klass->cursorRotate);
      window->currentCursor = klass->cursorRotate;
      window->refCursor = klass->cursorRotate;
      break;
    case interactive_measure:
    case interactive_pick:
    case interactive_move:
    case interactive_mark:
      if (gtk_widget_get_visible(GTK_WIDGET(window)))
	gdk_window_set_cursor(gtk_widget_get_window(window->openGLArea),
			      klass->cursorPointer);
      window->currentCursor = klass->cursorPointer;
      window->refCursor = klass->cursorPointer;
      break;
    case interactive_none:
      if (gtk_widget_get_visible(GTK_WIDGET(window)))
	gdk_window_set_cursor(gtk_widget_get_window(window->openGLArea),
			      klass->cursorPirate);
      window->currentCursor = klass->cursorPirate;
      window->refCursor = klass->cursorPirate;
      break;
    }
  /* We set the listeners. */
  if (type != interactive_none)
    {
      DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: setup signals.\n");
      id = addInteractiveEventListeners(window, event_button_release);
      if (id)
	DBG_fprintf(stderr, "  | connecting %ld signal.\n", id);
      id = addInteractiveEventListeners(window, event_button_press);
      if (id)
	DBG_fprintf(stderr, "  | connecting %ld signal.\n", id);
      id = addInteractiveEventListeners(window, event_motion_notify);
      if (id)
	DBG_fprintf(stderr, "  | connecting %ld signal.\n", id);
      id = addInteractiveEventListeners(window, event_key_press);
      if (id)
	DBG_fprintf(stderr, "  | connecting %ld signal.\n", id);
      id = addInteractiveEventListeners(window, event_key_release);
      if (id)
	DBG_fprintf(stderr, "  | connecting %ld signal.\n", id);
      id = addInteractiveEventListeners(window, event_scroll);
      if (id)
	DBG_fprintf(stderr, "  | connecting %ld signal.\n", id);
    }
  else
    {
      DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: removing interactive listeners.\n");
      for (ptList = window->interactiveEvents; ptList;
           ptList = g_list_next(ptList))
	{
          event = (InteractiveEvents*)ptList->data;
	  DBG_fprintf(stderr, "  | disconnecting %d signal.\n", event->id);
	  g_signal_handler_disconnect(G_OBJECT(window->openGLArea),
                                      event->callbackId);
	  g_free(ptList->data);
	}
      if (window->interactiveEvents)
	g_list_free(window->interactiveEvents);
      window->interactiveEvents = (GList*)0;
    }
}
/**
 * visu_ui_rendering_window_pushInteractive:
 * @window: a #VisuUiRenderingWindow object.
 * @inter: a #VisuInteractive object.
 *
 * It adds @inter to the stack of interactive sessions currently
 * attached to @window and launch it.
 *
 * Since: 3.6
 */
void visu_ui_rendering_window_pushInteractive(VisuUiRenderingWindow *window,
                                              VisuInteractive *inter)
{
  VisuInteractiveId type;

  g_return_if_fail(VISU_UI_IS_RENDERING_WINDOW(window) &&
		   VISU_IS_INTERACTIVE(inter));

  type = visu_interactive_getType(inter);
  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: push a new interactive"
	      " session (%d / %d).\n", type, g_list_length(window->inters));
  window->inters = g_list_prepend(window->inters, inter);
  g_object_ref(G_OBJECT(inter));
  visu_interactive_setNodeList(inter, window->extNodes);
  DBG_fprintf(stderr, " | ext nodes has %d ref counts.\n",
              G_OBJECT(window->extNodes)->ref_count);
  visu_gl_ext_marks_setInteractive(window->marks, inter);

  setInteractiveType(window, type);
}
/**
 * visu_ui_rendering_window_popInteractive:
 * @window: a #VisuUiRenderingWindow object.
 * @inter: a #VisuInteractive object.
 *
 * It removes @inter from the stack of interactive sessions currently
 * attached to @window. If @inter was first on the stack, the next
 * session is launched.
 *
 * Since: 3.6
 */
void visu_ui_rendering_window_popInteractive(VisuUiRenderingWindow *window,
                                             VisuInteractive *inter)
{
  g_return_if_fail(VISU_UI_IS_RENDERING_WINDOW(window));

  window->inters = g_list_remove(window->inters, inter);
  g_object_unref(G_OBJECT(inter));
  visu_interactive_setNodeList(inter, (VisuGlExtNodes*)0);
  DBG_fprintf(stderr, " | ext nodes has %d ref counts.\n",
              G_OBJECT(window->extNodes)->ref_count);

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: pop an old interactive"
	      " session (%d).\n", g_list_length(window->inters));
  
  if (window->inters)
    {
      visu_gl_ext_marks_setInteractive(window->marks, VISU_INTERACTIVE(window->inters->data));
      setInteractiveType(window, visu_interactive_getType(VISU_INTERACTIVE(window->inters->data)));
    }
  else
    {
      visu_gl_ext_marks_setInteractive(window->marks, (VisuInteractive*)0);
      setInteractiveType(window, interactive_none);
    }
}

static gboolean onButtonAction(VisuUiRenderingWindow *window, GdkEventButton *event,
			       gpointer user_data _U_)
{
  ToolSimplifiedEvents ev;

  if (!window->inters)
    return TRUE;

  ev.button = 0;
  ev.motion = 0;
  ev.letter = '\0';
  ev.specialKey = Key_None;

  ev.x = event->x;
  ev.y = event->y;
  ev.button = event->button;
  ev.shiftMod = event->state & GDK_SHIFT_MASK;
  ev.controlMod = event->state & GDK_CONTROL_MASK;
  ev.buttonType = 0;
  if (event->type == GDK_BUTTON_PRESS)
    ev.buttonType = TOOL_BUTTON_TYPE_PRESS;
  else if (event->type == GDK_BUTTON_RELEASE)
    ev.buttonType = TOOL_BUTTON_TYPE_RELEASE;

  gdk_window_set_cursor(gtk_widget_get_window(window->openGLArea),
			VISU_UI_RENDERING_WINDOW_CLASS(G_OBJECT_GET_CLASS(window))->cursorWatch);
  visu_interactive_handleEvent(VISU_INTERACTIVE(window->inters->data), window->view, &ev);

  gdk_window_set_cursor(gtk_widget_get_window(window->openGLArea),
			window->currentCursor);

  return TRUE;
}
static gboolean onScrollEvent(GtkWidget *widget _U_, GdkEventScroll *event,
			      gpointer user_data)
{
  ToolSimplifiedEvents ev;
  VisuUiRenderingWindow *window;

  window = VISU_UI_RENDERING_WINDOW(user_data);
  g_return_val_if_fail(window, TRUE);
  if (!window->inters)
    return TRUE;

  ev.button = 0;
  ev.motion = 0;
  ev.letter = '\0';
  ev.specialKey = Key_None;

  ev.x = event->x;
  ev.y = event->y;
  if (event->direction == GDK_SCROLL_UP)
    ev.button = 4;
  else if (event->direction == GDK_SCROLL_DOWN)
    ev.button = 5;
  ev.shiftMod = event->state & GDK_SHIFT_MASK;
  ev.controlMod = event->state & GDK_CONTROL_MASK;

  if (ev.button)
    {
      gdk_window_set_cursor(gtk_widget_get_window(window->openGLArea),
			    VISU_UI_RENDERING_WINDOW_CLASS(G_OBJECT_GET_CLASS(window))->cursorWatch);
      visu_interactive_handleEvent(VISU_INTERACTIVE(window->inters->data),
                                   window->view, &ev);
      gdk_window_set_cursor(gtk_widget_get_window(window->openGLArea),
			    window->currentCursor);
    }

  return TRUE;
}
static gboolean onMouseMotion(GtkWidget *widget _U_, GdkEventMotion *event,
			      gpointer user_data)
{
  ToolSimplifiedEvents ev;
  VisuUiRenderingWindow *window;
  /* #if DEBUG == 1 */
  /*   GTimer *timer; */
  /*   gulong fractionTimer; */
  /* #endif */

  window = VISU_UI_RENDERING_WINDOW(user_data);
  g_return_val_if_fail(window, TRUE);
  if (!window->inters)
    return TRUE;

  /* #if DEBUG == 1 */
  /*   timer = g_timer_new(); */
  /*   g_timer_start(timer); */
  /* #endif */

  ev.button = 0;
  ev.motion = 1;
  ev.letter = '\0';
  ev.specialKey = Key_None;

  /*   DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: motion at %gx%g.\n", event->x, event->y); */
  ev.x = event->x;
  ev.y = event->y;
  gdk_window_get_root_coords(event->window, ev.x, ev.y, &ev.root_x, &ev.root_y);
  if (event->state & GDK_BUTTON1_MASK)
    ev.button = 1;
  else if (event->state & GDK_BUTTON2_MASK)
    ev.button = 2;
  else if (event->state & GDK_BUTTON3_MASK)
    ev.button = 3;
  ev.buttonType = TOOL_BUTTON_TYPE_PRESS;
  ev.shiftMod = event->state & GDK_SHIFT_MASK;
  ev.controlMod = event->state & GDK_CONTROL_MASK;

  if (ev.button)
    {
      gdk_window_set_cursor(gtk_widget_get_window(window->openGLArea),
                            VISU_UI_RENDERING_WINDOW_CLASS(G_OBJECT_GET_CLASS(window))->cursorWatch);
      visu_interactive_handleEvent(VISU_INTERACTIVE(window->inters->data),
                                   window->view, &ev);
      gdk_window_set_cursor(gtk_widget_get_window(window->openGLArea),
                            window->currentCursor);
    }

  if (event->is_hint)
    {
#if GDK_MAJOR_VERSION > 2 || GDK_MINOR_VERSION > 11
      gdk_event_request_motions(event);
#else
      gdk_window_get_pointer(event->window, NULL, NULL, NULL);
#endif
    }

  /* #if DEBUG == 1 */
  /*   g_timer_stop(timer); */
  /*   fprintf(stderr, "Gtk VisuUiRenderingWindow: handling mouse motion in %g micro-s.\n", */
  /*           g_timer_elapsed(timer, &fractionTimer)*1e6); */
  /*   g_timer_destroy(timer); */
  /* #endif */
  return TRUE;
}
static gboolean onKeyPressed(GtkWidget *widget _U_, GdkEventKey *event,
			     gpointer data)
{
  ToolSimplifiedEvents ev;
  VisuUiRenderingWindow *window;
  GList *cameras, *head;

  window = VISU_UI_RENDERING_WINDOW(data);
  g_return_val_if_fail(window, TRUE);
  if (!window->inters)
    return FALSE;

  ev.button = 0;
  ev.buttonType = 0;
  ev.motion = 0;
  ev.letter = '\0';
  ev.specialKey = Key_None;

  if ((event->keyval == GDK_KEY_r || event->keyval == GDK_KEY_R) &&
      !(event->state & GDK_CONTROL_MASK))
    {
      ev.letter = 'r';
      /* If any camera, print a message. */
      visu_interactive_getSavedCameras(VISU_INTERACTIVE(window->inters->data),
				      &cameras, &head);
      if (cameras)
	visu_ui_rendering_window_pushMessage(window, _("Restore saved camera position."));
      else
	visu_ui_rendering_window_pushMessage(window, _("No saved camera. Use 's' to save one."));
#if GLIB_MINOR_VERSION > 13
      g_timeout_add_seconds(3, timeOutPopMessage, (gpointer)window);
#else
      g_timeout_add(3000, timeOutPopMessage, (gpointer)window);
#endif
    }
  else if ((event->keyval == GDK_KEY_s || event->keyval == GDK_KEY_S) &&
          !(event->state & GDK_CONTROL_MASK))
    {
      ev.letter = 's';
      visu_ui_rendering_window_pushMessage(window, _("Save current camera position."));
#if GLIB_MINOR_VERSION > 13
      g_timeout_add_seconds(3, timeOutPopMessage, (gpointer)window);
#else
      g_timeout_add(3000, timeOutPopMessage, (gpointer)window);
#endif
    }
  else if(event->keyval == GDK_KEY_space)
    ev.letter = ' ';
  else if(event->keyval == GDK_KEY_Page_Up)
    ev.specialKey = Key_Page_Up;
  else if(event->keyval == GDK_KEY_Page_Down)
    ev.specialKey = Key_Page_Down;
  else if(event->keyval == GDK_KEY_Down)
    ev.specialKey = Key_Arrow_Down;
  else if(event->keyval == GDK_KEY_Up)
    ev.specialKey = Key_Arrow_Up;
  else if(event->keyval == GDK_KEY_Left)
    ev.specialKey = Key_Arrow_Left;
  else if(event->keyval == GDK_KEY_Right)
    ev.specialKey = Key_Arrow_Right;
#if GTK_MAJOR_VERSION > 2
  else if(event->keyval == GDK_KEY_Menu)
    {
      gdk_window_get_device_position(event->window,
                                     gdk_device_manager_get_client_pointer
                                     (gdk_display_get_device_manager
                                      (gdk_window_get_display(event->window))),
                                     &ev.x, &ev.y, NULL);
      gdk_window_get_root_coords(event->window, ev.x, ev.y, &ev.root_x, &ev.root_y);
      ev.specialKey = Key_Menu;
    }
#endif
  ev.shiftMod = event->state & GDK_SHIFT_MASK;
  ev.controlMod = event->state & GDK_CONTROL_MASK;

  if (ev.letter != '\0' || ev.specialKey != Key_None)
    {
      gdk_window_set_cursor(gtk_widget_get_window(window->openGLArea),
			    VISU_UI_RENDERING_WINDOW_CLASS(G_OBJECT_GET_CLASS(window))->cursorWatch);
      visu_interactive_handleEvent(VISU_INTERACTIVE(window->inters->data),
                                   window->view, &ev);
      gdk_window_set_cursor(gtk_widget_get_window(window->openGLArea),
			    window->currentCursor);
      return TRUE;
    }
  else if (event->keyval == GDK_KEY_Shift_L || event->keyval == GDK_KEY_Shift_R)
    {
      gdk_window_set_cursor(gtk_widget_get_window(window->openGLArea),
			    VISU_UI_RENDERING_WINDOW_CLASS(G_OBJECT_GET_CLASS(window))->cursorGrab);
      window->currentCursor = VISU_UI_RENDERING_WINDOW_CLASS(G_OBJECT_GET_CLASS(window))->cursorGrab;
      return TRUE;
    }

  return FALSE;
}
static gboolean onKeyRelease(GtkWidget *widget _U_, GdkEventKey *event,
			     gpointer data)
{
  VisuUiRenderingWindow *window;

  window = VISU_UI_RENDERING_WINDOW(data);
  g_return_val_if_fail(window, TRUE);

  if (event->keyval == GDK_KEY_Shift_L || event->keyval == GDK_KEY_Shift_R)
    {
      gdk_window_set_cursor(gtk_widget_get_window(window->openGLArea),
			    window->refCursor);
      window->currentCursor = window->refCursor;
    }

  return TRUE;
}

static gboolean timeOutPopMessage(gpointer data)
{
  visu_ui_rendering_window_popMessage(VISU_UI_RENDERING_WINDOW(data));

  return FALSE;
}

/**
 * visu_ui_rendering_window_pushMessage:
 * @window: a valid #VisuUiRenderingWindow object ;
 * @message: an UTF8 string to print on the status bar.
 *
 * Use this method to add some informations on the status bar.
 */
void visu_ui_rendering_window_pushMessage(VisuUiRenderingWindow *window, const gchar *message)
{
  g_return_if_fail(VISU_UI_IS_RENDERING_WINDOW(window));

  gtk_statusbar_push(GTK_STATUSBAR(window->info->statusInfo),
		     window->info->statusInfoId, message);
  window->nbStatusMessage += 1;
}
/**
 * visu_ui_rendering_window_popMessage:
 * @window: a valid #VisuUiRenderingWindow object.
 *
 * Remove the last message.
 */
void visu_ui_rendering_window_popMessage(VisuUiRenderingWindow *window)
{
  g_return_if_fail(VISU_UI_IS_RENDERING_WINDOW(window));

  gtk_statusbar_pop(GTK_STATUSBAR(window->info->statusInfo),
		    window->info->statusInfoId);
  window->nbStatusMessage -= 1;
}
static void getOpenGLAreaSize(VisuUiRenderingWindow *window,
			      guint *width, guint *height)
{
  GtkAllocation alloc;

  g_return_if_fail(VISU_UI_IS_RENDERING_WINDOW(window) && width && height);

  gtk_widget_get_allocation(window->openGLArea, &alloc);
  *width = alloc.width;
  *height = alloc.height;
}
/**
 * visu_ui_rendering_window_setData:
 * @window: a valid #VisuUiRenderingWindow object ;
 * @data: (allow-none): a #VisuData to render in the given @window.
 *
 * This method is used to attach a #VisuData object to a given window.
 * When, @data is not NULL, this method emit the #VisuObject::dataRendered signal,
 * and all modules that needs to draw something should catch this signal and
 * draw in the rendering window.
 */
void visu_ui_rendering_window_setData(VisuUiRenderingWindow *window, VisuData* data)
{
  VisuData *oldData;

  g_return_if_fail(VISU_UI_IS_RENDERING_WINDOW(window));
  DBG_fprintf(stderr, "Gtk renderingWindow: attach %p (%p) VisuData to %p window.\n",
	      (gpointer)data, (gpointer)window->currentData, (gpointer)window);
  oldData = window->currentData;
  if (oldData != data)
    {
      if (window->currentData)
	{
	  g_signal_handler_disconnect(G_OBJECT(window->currentData),
				      window->populationIncrease_id);
	  g_signal_handler_disconnect(G_OBJECT(window->currentData),
				      window->populationDecrease_id);

          DBG_fprintf(stderr, "Gtk renderingWindow:"
                      " emitting the 'dataUnRendered' signal.\n");
          g_signal_emit_by_name(VISU_OBJECT_INSTANCE, "dataUnRendered",
                                window->currentData, window->view, NULL);
          DBG_fprintf(stderr, "Gtk renderingWindow: emition done.\n");
	}
    }

  /* Change the rendering window with the new data (may be the same
     but with a different content). */
  window->currentData = data;
  displayFileInfoOnDataLoaded(window);
  setFileButtonsSensitive(window);

  DBG_fprintf(stderr, "##### VisuData association to a window #####\n");
  if (data)
    {
      /* Ref the object. */
      g_object_ref(G_OBJECT(data));
      DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: ref new object %p.\n",
		  (gpointer)data);

      if (gtk_widget_get_visible(GTK_WIDGET(window)))
        /* Adapt the view to the box. */
        visu_boxed_setBox(VISU_BOXED(window->view), VISU_BOXED(data),
                          VISU_UI_RENDERING_WINDOW_GET_CLASS(window)->autoAdjust);

      /* Attach the default redraw method. */
      visu_ui_gl_widget_setRedraw(VISU_UI_GL_WIDGET(window->openGLArea),
                                  visu_gl_redraw, window->view);

      /* Attach signals to the new #VisuData object. */
      window->populationIncrease_id =
	g_signal_connect(G_OBJECT(data), "PopulationIncrease",
			 G_CALLBACK(onNodePopulationChanged), window);
      window->populationDecrease_id =
	g_signal_connect(G_OBJECT(data), "PopulationDecrease",
			 G_CALLBACK(onNodePopulationChanged), window);

      visu_gl_ext_nodes_setData(window->extNodes, window->view, data);
      visu_gl_ext_nodes_draw(window->extNodes);
      if (visu_gl_ext_pairs_setData(visu_gl_ext_pairs_getDefault(), window->view, data))
        visu_gl_ext_pairs_draw(visu_gl_ext_pairs_getDefault());
      DBG_fprintf(stderr, "Gtk renderingWindow: emitting the 'dataRendered' signal.\n");
      g_signal_emit_by_name(VISU_OBJECT_INSTANCE, "dataRendered",
                            data, window->view, NULL);
      DBG_fprintf(stderr, "Gtk renderingWindow: emition done.\n");
    }
  else
    {
      visu_gl_ext_nodes_setData(window->extNodes, window->view, NULL);
      visu_gl_ext_pairs_setData(visu_gl_ext_pairs_getDefault(), window->view, NULL);
      DBG_fprintf(stderr, "Gtk renderingWindow: emitting the 'dataRendered' signal.\n");
      g_signal_emit_by_name(VISU_OBJECT_INSTANCE, "dataRendered",
                            NULL, window->view, NULL);
      DBG_fprintf(stderr, "Gtk renderingWindow: emition done.\n");

      if (oldData)
	{
	  /* Attach the default redraw method. */
	  visu_ui_gl_widget_setRedraw(VISU_UI_GL_WIDGET(window->openGLArea),
                                      (VisuUiGlWidgetRedrawMethod)0, (VisuGlView*)0);
	  /* Ask for redraw. */
          
	  _redraw(window, TRUE);
	}
    }

  visu_gl_ext_marks_setData(window->marks, data);
  visu_gl_ext_marks_setGlView(window->marks, window->view);
  /* All extensions. */
  if (visu_gl_ext_axes_setGlView(visu_gl_ext_axes_getDefault(), window->view))
    visu_gl_ext_axes_draw(visu_gl_ext_axes_getDefault());

  /* Reset the statusbar informations and other GUI parameters. */
  while (window->nbStatusMessage > 0)
    visu_ui_rendering_window_popMessage(window);
  if (data)
    {
      /* Put a commentary in the statusbar. */
      visu_ui_rendering_window_pushMessage(window, _("Rotate with left b.,"
					    " pick with right b.,"
					    " setup ref. with"
					    " <shift> or <control> b."));

      visu_ui_rendering_window_pushInteractive(window, inter);
      gtk_widget_set_sensitive(window->info->clearMarksButton, TRUE);
    }
  else
    {
      if (oldData)
	visu_ui_rendering_window_popInteractive(window, inter);
      gtk_widget_set_sensitive(window->info->clearMarksButton, FALSE);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 17
      gtk_widget_hide(GTK_WIDGET(window->info->infoBar));
      gtk_widget_grab_focus(window->openGLArea);
#endif
    }

  if (oldData)
    {
      DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: unref old object %p.\n",
		  (gpointer)oldData);
      g_object_unref(oldData);
    }
}
/**
 * visu_ui_rendering_window_getData:
 * @window: a valid #VisuUiRenderingWindow object.
 *
 * This method is used to get the #VisuData attached to a window.
 *
 * Returns: (transfer none): the #VisuData attached to the @window or NULL if none.
 */
VisuData* visu_ui_rendering_window_getData(VisuUiRenderingWindow *window)
{
  g_return_val_if_fail(VISU_UI_IS_RENDERING_WINDOW(window), (VisuData*)0);
  return window->currentData;
}
/**
 * visu_ui_rendering_window_getGlView:
 * @window: a valid #VisuUiRenderingWindow object.
 *
 * This method is used to get the #VisuGlView attached to the
 * rendering window.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): the #VisuGlView attached to the @window
 * or NULL on error.
 */
VisuGlView* visu_ui_rendering_window_getGlView(VisuUiRenderingWindow *window)
{
  g_return_val_if_fail(VISU_UI_IS_RENDERING_WINDOW(window), (VisuGlView*)0);
  return window->view;
}
/**
 * visu_ui_rendering_window_getAccelGroup:
 * @window: a #VisuUiRenderingWindow object.
 *
 * Retrieve the accelerator group of @window.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): the #GtkAccelGroup object of @window.
 **/
GtkAccelGroup* visu_ui_rendering_window_getAccelGroup(VisuUiRenderingWindow *window)
{
  g_return_val_if_fail(VISU_UI_IS_RENDERING_WINDOW(window), (GtkAccelGroup*)0);
  return window->accel;
}

/***************************/
/* GtkStatusInfo functions */
/***************************/
static void _setLabelSize(GtkInfoArea *info, gint width, gint height)
{
  gchar *str;

  g_return_if_fail(info);

  if (info->fileInfoFreeze)
    return;

  str = g_strdup_printf("<span size=\"smaller\"><b>%s</b> %dx%d</span>", _("Size:"), width, height);
  gtk_label_set_markup(GTK_LABEL(info->labelSize), str);
  g_free(str);
}
static void _setFileDescription(GtkInfoArea *info, gchar* message)
{
  gchar *str;

  g_return_if_fail(info);

  str = g_strdup_printf("<span size=\"smaller\">%s</span>", message);
  gtk_label_set_markup(GTK_LABEL(info->labelFileInfo), str);
  g_free(str);
}
static void _setNNodes(GtkInfoArea *info, gint nb)
{
  GString *str;

  g_return_if_fail(info);

  str = g_string_new("<span size=\"smaller\">");
  if (nb > 0)
    g_string_append_printf(str, _("<b>Nb nodes:</b> %d"), nb);
  else
    g_string_append(str, GTK_STATUSINFO_NONB);
  g_string_append_printf(str, "</span>");
  gtk_label_set_markup(GTK_LABEL(info->labelNb), str->str);
  g_string_free(str, TRUE);
}
static void onNodeInfoClicked(GtkToggleButton *button, gpointer data)
{
  VisuUiRenderingWindow *window;
  gint *id;

  window = VISU_UI_RENDERING_WINDOW(data);
  g_return_if_fail(window);

  id = (gint*)g_object_get_data(G_OBJECT(button), "selectedNodeId");
  g_return_if_fail(id && *id >= 0);

  if (visu_gl_ext_marks_setInfos(window->marks, *id,
			 gtk_toggle_button_get_active(button)))
    VISU_REDRAW_FORCE;
}
static void onMarkClearClicked(GtkButton *button _U_, gpointer data)
{
  VisuUiRenderingWindow *window;

  window = VISU_UI_RENDERING_WINDOW(data);
  g_return_if_fail(window);

  if (visu_gl_ext_marks_removeMeasures(window->marks, -1))
    VISU_REDRAW_FORCE;
}
static gboolean onCameraMenu(VisuUiRenderingWindow *window, GdkEventButton *event,
			     GtkEventBox *ev _U_)
{
  GtkWidget *wd;

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: click on the camera menu.\n");
  wd = buildCameraMenu(window);
  if (!wd)
    return TRUE;

  g_signal_connect(G_OBJECT(wd), "selection-done",
		   G_CALLBACK(onCameraMenuSelected), (gpointer)window);

  gtk_widget_show_all(wd);
  gtk_menu_popup(GTK_MENU(wd), NULL, NULL, NULL, NULL, 
		 1, event->time);

  return TRUE;
}
static GtkWidget* buildCameraMenu(VisuUiRenderingWindow *window)
{
  GtkWidget *menu, *item;
  gchar *lbl;
  GList *cameras, *head, *tmpLst, *rCameras;
  VisuGlCamera *current;
  guint n;

  if (!window->currentData || !window->inters)
    return (GtkWidget*)0;

  /* All camera. */
  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: get the cameras.\n");
  visu_interactive_getSavedCameras(VISU_INTERACTIVE(window->inters->data),
				  &cameras, &head);
  /*   if (!cameras) */
  /*     return (GtkWidget*)0; */

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: build the menu.\n");
  menu = gtk_menu_new();
  gtk_menu_set_accel_group(GTK_MENU(menu), window->accel);
  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: create the camera menu %p.\n",
	      (gpointer)menu);

  /* Set a title. */
  item = gtk_menu_item_new_with_label(_("Camera menu (saved in 'v_sim.par'):"));
  gtk_widget_set_sensitive(item, FALSE);
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
  item = gtk_separator_menu_item_new();
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
  /* Put the current camera. */
  current = window->view->camera;
  lbl = g_strdup_printf(_("save current camera:\n"
			  "(\316\270 %6.1f\302\260 ; \317\206 %6.1f\302\260 ; \317\211 %6.1f\302\260) "
			  "dx %4.1f dy %4.1f"),
			current->theta, current->phi, current->omega,
			current->xs, current->ys);
  item = gtk_menu_item_new_with_label(lbl);
  g_free(lbl);
  g_signal_connect(G_OBJECT(item), "activate",
                   G_CALLBACK(onCameraMenuCurrentClicked), window);
  gtk_menu_item_set_accel_path(GTK_MENU_ITEM(item),
                               g_intern_static_string(MENU_CAMERA_SAVE));
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
  /* Put an option to open the view selector. */
  item = gtk_menu_item_new_with_label(_("select precisely a camera view"));
  g_signal_connect(G_OBJECT(item), "activate",
                   G_CALLBACK(onCameraMenuOrientationClicked), window);
  gtk_menu_item_set_accel_path(GTK_MENU_ITEM(item),
                               g_intern_static_string(MENU_CAMERA_ORIENT));
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
  /* Separator. */
  item = gtk_separator_menu_item_new();
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
  if (!cameras)
    {
      item = gtk_menu_item_new_with_label(_("No saved camera. Use 's' to save one."));
      gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
    }
  else
    {
      item = gtk_menu_item_new_with_label(_("List of saved cameras:"));
      gtk_widget_set_sensitive(item, FALSE);
      gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
    }

  rCameras = g_list_reverse(g_list_copy(cameras));
  for (tmpLst = rCameras, n = 0; tmpLst; tmpLst = g_list_next(tmpLst), n+= 1)
    {
      current = (VisuGlCamera*)tmpLst->data;
      lbl = g_strdup_printf(_("(\316\270 %6.1f\302\260 ; \317\206 %6.1f\302\260 ; \317\211 %6.1f\302\260) "
			      "dx %4.1f dy %4.1f"),
			    current->theta, current->phi, current->omega,
			    current->xs, current->ys);
      item = gtk_menu_item_new_with_label(lbl);
      DBG_fprintf(stderr, " | add menu item %p (%p)\n", (gpointer)item, (gpointer)window);
      g_free(lbl);
      if (current == (VisuGlCamera*)head->data)
        gtk_menu_item_set_accel_path(GTK_MENU_ITEM(item),
                                     g_intern_static_string(MENU_CAMERA_RESTORE));
      else if (n < 9)
        gtk_menu_item_set_accel_path(GTK_MENU_ITEM(item),
                                     g_intern_static_string(cameraAccels[n]));
      g_signal_connect(G_OBJECT(item), "activate",
		       G_CALLBACK(onCameraMenuClicked), window);
      g_object_set_data(G_OBJECT(item), "Camera", (gpointer)current);
      gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
    }
  g_list_free(rCameras);
  return menu;
}
static void _setCamera(VisuUiRenderingWindow *window, VisuGlCamera *camera)
{
  gboolean reDrawNeeded;

  g_return_if_fail(VISU_UI_IS_RENDERING_WINDOW(window));
  if (!window->currentData || !window->inters || !camera)
    return;
  
  visu_interactive_pushSavedCamera(VISU_INTERACTIVE(window->inters->data), camera);

  reDrawNeeded =
    visu_gl_view_setThetaPhiOmega(window->view, camera->theta, camera->phi, camera->omega,
                                  VISU_GL_CAMERA_THETA | VISU_GL_CAMERA_PHI |
                                  VISU_GL_CAMERA_OMEGA);
  reDrawNeeded =
    visu_gl_view_setXsYs(window->view, camera->xs, camera->ys,
                         VISU_GL_CAMERA_XS | VISU_GL_CAMERA_YS) ||
    reDrawNeeded;
  reDrawNeeded = visu_gl_view_setGross(window->view, camera->gross) ||
    reDrawNeeded;
  reDrawNeeded = visu_gl_view_setPersp(window->view, camera->d_red) ||
    reDrawNeeded;
  if (reDrawNeeded)
    VISU_REDRAW_FORCE;
}
static gboolean onCameraAccel(GtkAccelGroup *accel _U_, GObject *obj,
                              guint key, GdkModifierType mod _U_, gpointer data)
{
  VisuUiRenderingWindow *window;
  GList *cameras, *head, *rCameras;
  VisuGlCamera *camera;

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: get accelerator for object %p.\n",
              (gpointer)obj);
  window = VISU_UI_RENDERING_WINDOW(data);
  /* All camera. */
  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: get the cameras.\n");
  visu_interactive_getSavedCameras(VISU_INTERACTIVE(window->inters->data),
                                   &cameras, &head);
  if (!cameras)
    return TRUE;

  rCameras = g_list_reverse(g_list_copy(cameras));
  camera = g_list_nth_data(rCameras, key - GDK_KEY_1);
  g_list_free(rCameras);
  _setCamera(window, camera);

  return TRUE;
}
static void onCameraMenuSelected(GtkMenuShell *menushell, gpointer user_data _U_)
{
  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: destroy the camera menu %p.\n",
	      (gpointer)menushell);
  gtk_widget_destroy(GTK_WIDGET(menushell));
}
static void onCameraMenuClicked(GtkMenuItem *menuitem, gpointer user_data)
{
  _setCamera(VISU_UI_RENDERING_WINDOW(user_data),
             (VisuGlCamera*)g_object_get_data(G_OBJECT(menuitem), "Camera"));
}
static void onCameraMenuCurrentClicked(GtkMenuItem *menuitem _U_, gpointer user_data)
{
  visu_interactive_pushSavedCamera(VISU_INTERACTIVE(VISU_UI_RENDERING_WINDOW(user_data)->inters->data),
                                   VISU_UI_RENDERING_WINDOW(user_data)->view->camera);
}
static void onOrientationChanged(VisuUiOrientationChooser *orientationChooser,
				 gpointer data)
{
  float values[2];
  gboolean reDrawNeeded;

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: orientation changed.\n");
  visu_ui_orientation_chooser_getAnglesValues(orientationChooser, values);

  reDrawNeeded = visu_gl_view_setThetaPhiOmega(VISU_UI_RENDERING_WINDOW(data)->view,
                                               values[0], values[1], 0.,
                                               VISU_GL_CAMERA_THETA | VISU_GL_CAMERA_PHI);
  if (reDrawNeeded)
    VISU_REDRAW_ADD;
}
static void _orientationChooser(VisuUiRenderingWindow *window)
{
  GtkWidget *orientationChooser;
  VisuGlCamera *current;
  float values[2];
  gboolean reDrawNeeded;

  orientationChooser = visu_ui_orientation_chooser_new
    (VISU_UI_ORIENTATION_DIRECTION, TRUE, window->currentData, NULL);
  gtk_window_set_modal(GTK_WINDOW(orientationChooser), TRUE);
  current = window->view->camera;
  values[0] = current->theta;
  values[1] = current->phi;
  visu_ui_orientation_chooser_setAnglesValues(VISU_UI_ORIENTATION_CHOOSER(orientationChooser),
                                     values);
  g_signal_connect(G_OBJECT(orientationChooser), "values-changed",
                   G_CALLBACK(onOrientationChanged), window);
  gtk_widget_show(orientationChooser);
  
  switch (gtk_dialog_run(GTK_DIALOG(orientationChooser)))
    {
    case GTK_RESPONSE_ACCEPT:
      DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: accept changings on orientation.\n");
      break;
    default:
      DBG_fprintf(stderr, "Gtk Observe: reset values on orientation.\n");
      reDrawNeeded =
        visu_gl_view_setThetaPhiOmega(window->view,
                                      values[0], values[1], 0.,
                                      VISU_GL_CAMERA_THETA | VISU_GL_CAMERA_PHI);
      if (reDrawNeeded)
	VISU_REDRAW_ADD;
    }
  DBG_fprintf(stderr, "Gtk Observe: orientation object destroy.\n");
  gtk_widget_destroy(orientationChooser);
}
static void onCameraMenuOrientationClicked(GtkMenuItem *menuitem _U_, gpointer data)
{
  _orientationChooser(VISU_UI_RENDERING_WINDOW(data));
}
static void displayFileInfoOnDataLoaded(VisuUiRenderingWindow *window)
{
  gchar* message;
  VisuNodeArrayIter iter;

  g_return_if_fail(window);

  if (window->currentData)
    {
      message = visu_data_getFileCommentary(window->currentData,
                                            visu_data_getISubset(window->currentData));
      visu_node_array_iterNew(VISU_NODE_ARRAY(window->currentData), &iter);
      _setNNodes(window->info, iter.nAllStoredNodes);
    }
  else
    {
      message = (gchar*)0;
      _setNNodes(window->info, -1);
    }
  if (message && message[0])
    _setFileDescription(window->info, message);
  else
    _setFileDescription(window->info, GTK_STATUSINFO_NOFILEINFO);
}

void visu_ui_rendering_window_lockUI(VisuUiRenderingWindow *window, gboolean status)
{
  g_return_if_fail(VISU_UI_IS_RENDERING_WINDOW(window));

  gtk_widget_set_sensitive(window->info->hboxTools, !status);
  gtk_widget_set_sensitive(window->info->hboxInteractive, !status);
  gtk_widget_set_sensitive(window->info->hboxFileInfo, !status);
}

struct _load_struct
{
  VisuUiRenderingWindow *window;
  VisuData *data;
  guint iSet;
};
static void stopProgress(VisuUiRenderingWindow *window)
{
  if (window->info->progressId)
    g_source_remove(window->info->progressId);
  window->info->progressId = 0;

  visu_ui_rendering_window_lockUI(window, FALSE);
}
static gboolean popProgress(gpointer data)
{
  VisuUiRenderingWindow *window;

  window = VISU_UI_RENDERING_WINDOW(data);

  gtk_progress_bar_pulse(GTK_PROGRESS_BAR(window->info->progress));

  return TRUE;
}
static gboolean showProgress(gpointer data)
{
  VisuUiRenderingWindow *window;

  window = VISU_UI_RENDERING_WINDOW(data);

  visu_ui_rendering_window_lockUI(window, TRUE);
  gtk_widget_show(window->info->progress);
  gtk_widget_show(window->info->cancelButton);
  gtk_widget_hide(window->info->statusInfo);

  gtk_progress_bar_set_text(GTK_PROGRESS_BAR(window->info->progress),
                            _("Loading file..."));

  if (window->info->progressId)
    g_source_remove(window->info->progressId);
  window->info->progressId = g_timeout_add(100, popProgress, data);
  
  return FALSE;
}
static void messProgress(gpointer mess, gpointer progress)
{
  gtk_progress_bar_set_text(GTK_PROGRESS_BAR(progress), (const gchar*)mess);
}
static void onCancelButtonClicked(GtkButton *button _U_, gpointer data)
{
  gtk_progress_bar_set_text(GTK_PROGRESS_BAR(VISU_UI_RENDERING_WINDOW(data)->info->progress),
                            _("Cancellation request, waiting for reply..."));
  g_cancellable_cancel(VISU_UI_RENDERING_WINDOW(data)->info->cancel);
}

static void _storeRecent(const gchar *filename)
{
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 9
  GtkRecentManager *manager;
  gchar *uri;
  GError *error;

  manager = gtk_recent_manager_get_default();
  error = (GError*)0;
  uri = g_filename_to_uri(filename, NULL, &error);
  if (error)
    {
      g_warning("%s", error->message);
      g_error_free(error);
      return;
    }
  DBG_fprintf(stderr, "VisuUiRenderingWindow: add '%s' to recent files.\n", uri);
  gtk_recent_manager_add_item(manager, uri);
  g_free(uri);
#endif
}

static gboolean _visu_ui_rendering_window_loadFile(gpointer data)
{
  VisuObject *o;
  VisuData *obj;
  GError *error;
  gboolean changeElement, res;
  VisuUiRenderingWindow *window;
  struct _load_struct *pt;
  int iSet;
  guint waitId;

  /* obj is the new object and main the panel that handle the
     loading. */
  pt = (struct _load_struct*)data;
  window = pt->window;
  obj = pt->data;
  iSet = (int)pt->iSet;
  o = VISU_OBJECT_INSTANCE;

  DBG_fprintf(stderr, "VisuUiRenderingWindow: loading process ... %p points to"
	      " previous VisuData.\n", (gpointer)window->currentData);

  g_cancellable_reset(window->info->cancel);
  waitId = g_timeout_add(500, showProgress, window);
  visu_object_setLoadMessageFunc(o, messProgress, (gpointer)window->info->progress);
  error = (GError*)0;
  res = visu_object_load(o, obj, iSet, window->info->cancel, &error);
  DBG_fprintf(stderr, "VisuUiRenderingWindow: basic load OK, "
              "continue with Gtk loading parts.\n");
  g_source_remove(waitId);
  stopProgress(window);
  g_free(pt);

  gtk_widget_hide(window->info->progress);
  gtk_widget_hide(window->info->cancelButton);
  gtk_widget_show(window->info->statusInfo);
  
  if (!res)
    {
      g_object_unref(obj);
      obj = (VisuData*)0;
      if (error)
	{
	  visu_ui_raiseWarning(_("Loading a file"), error->message, NULL);
	  g_error_free(error);
	}
      else
	g_warning("No error message.");
    }
  else if (obj)
    {
      _storeRecent(visu_data_getFile(obj, 0, (ToolFileFormat**)0));
      if (window->currentData)
        {
          changeElement =
            visu_node_array_compareElements(VISU_NODE_ARRAY(window->currentData),
                                            VISU_NODE_ARRAY(obj));
          visu_data_setChangeElementFlag(obj, changeElement);
        }
    }

  DBG_fprintf(stderr, "VisuUiRenderingWindow: loading process ... try to load %p.\n",
	      (gpointer)obj);
  if (obj)
    DBG_fprintf(stderr, " | %p has %d ref counts.\n",
                (gpointer)obj, G_OBJECT(obj)->ref_count);
  visu_ui_rendering_window_setData(window, obj);
  if (obj)
    DBG_fprintf(stderr, " | %p has %d ref counts.\n",
                (gpointer)obj, G_OBJECT(obj)->ref_count);

  if (!obj)
    return FALSE;

  /* We release a ref on obj, since
     visu_ui_rendering_window_setData has increased it. */
  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: release current data.\n");
  g_object_unref(G_OBJECT(obj));
	 
  /* A redraw here is necessary since the load is finished.
     A redraw in the parent may not work since the redraw queue
     may be emptied before. */
  VISU_REDRAW_ADD;

  return FALSE;
}
/**
 * visu_ui_rendering_window_loadFile:
 * @window: a valid #VisuUiRenderingWindow object.
 * @data: the #VisuData to be loaded.
 * @iSet: the id of @data to load.
 *
 * This method calls the general function to load data from file
 * and deals with errors with gtkDialogs. The filenames must have
 * already been set into @data using visu_data_addFile().
 */
void visu_ui_rendering_window_loadFile(VisuUiRenderingWindow *window, VisuData *data, guint iSet)
{
  struct _load_struct *pt;

  pt = g_malloc(sizeof(struct _load_struct));
  pt->window = window;
  pt->data   = data;
  pt->iSet   = iSet;
  g_idle_add(_visu_ui_rendering_window_loadFile, pt);
}
/**
 * visu_ui_rendering_window_open:
 * @window: the window the file will by rendered on ;
 * @parent: (allow-none): the parent window for the filechooser dialog.
 *
 * Do as if the load button has been pushed, i.e. open a filechooser
 * dialog on the @parent window, and load the resulting file,
 * refreshing the view if necessary.
 */
void visu_ui_rendering_window_open(VisuUiRenderingWindow *window, GtkWindow *parent)
{
  gboolean res;
  VisuUiSetFilesFunc loadAction;
  VisuData *newData;

  loadAction = visu_ui_getRenderingSpecificOpen(visu_object_getRendering(VISU_OBJECT_INSTANCE));
  g_return_if_fail(loadAction);

  newData = visu_data_new();
  visu_ui_setRenderWidget(window);
  res = loadAction(newData, parent);
  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: 'loadAction' OK.\n");
  
  if (res)
    visu_ui_rendering_window_loadFile(window, newData, 0);
  else
    g_object_unref(newData);
}

static void onOpen(VisuUiRenderingWindow *window)
{
  visu_ui_rendering_window_open(window, (GtkWindow*)0);
}
/**
 * visu_ui_rendering_window_reload:
 * @window: a #VisuUiRenderingWindow object.
 *
 * This routines reloads the current #VisuData object by rereading it
 * on disk. If there is no current #VisuData object, it reports an
 * error.
 *
 * Since: 3.7
 */
void visu_ui_rendering_window_reload(VisuUiRenderingWindow *window)
{
  VisuData *dataObj;
  int id;

  g_return_if_fail(VISU_UI_IS_RENDERING_WINDOW(window));

  /*   if (VISU_UI_RENDERING_WINDOW(data)->currentData) */
  /*     rebuildAllExtensionsLists(VISU_UI_RENDERING_WINDOW(data)->currentData); */
  /*   _redraw(VISU_UI_RENDERING_WINDOW(data), TRUE); */
  dataObj = window->currentData;
  g_return_if_fail(dataObj);

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: reload current file.\n");
  id = visu_data_getISubset(dataObj);
  visu_data_freePopulation(dataObj);
  g_object_ref(dataObj);
  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: call load file.\n");
  visu_ui_rendering_window_loadFile(window, dataObj, id);
  VISU_REDRAW_FORCE;
}

static void onRenderingMethodChanged(VisuUiRenderingWindow *window, VisuRendering *method,
				     gpointer data _U_)
{
  if (window->currentData)
    /* First free attached visuData. */
    visu_ui_rendering_window_setData(window, (VisuData*)0);

  /* Customize interface according to new method. */
  if (method)
    {
      visu_ui_rendering_window_popMessage(window);
      if (window->info->loadButton)
	gtk_widget_set_sensitive(window->info->loadButton, TRUE);
      visu_ui_rendering_window_pushMessage(window,
				  _("Use the 'open' button to render a file."));
    }
  else
    {
      if (window->info->loadButton)
	gtk_widget_set_sensitive(window->info->loadButton, FALSE);
      visu_ui_rendering_window_popMessage(window);
    }
}
static void onNodePopulationChanged(VisuData *data, int *nodes _U_,
				    gpointer user_data)
{
  VisuNodeArrayIter iter;

  if (data != VISU_UI_RENDERING_WINDOW(user_data)->currentData)
    return;

  /* Change the count of nodes. */
  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  _setNNodes(VISU_UI_RENDERING_WINDOW(user_data)->info, iter.nAllStoredNodes);
}
static void setFileButtonsSensitive(VisuUiRenderingWindow *window)
{
  g_return_if_fail(window);

  if (!window->info->dumpButton || !window->info->reloadButton)
    return;

  if (window->currentData)
    {
      gtk_widget_set_sensitive(window->info->dumpButton, TRUE);
      gtk_widget_set_sensitive(window->info->reloadButton, TRUE);
    }
  else
    {
      gtk_widget_set_sensitive(window->info->dumpButton, FALSE);
      gtk_widget_set_sensitive(window->info->reloadButton, FALSE);
    }
}

void updateDumpProgressBar(gpointer data)
{
  gdouble val;

  g_return_if_fail(GTK_PROGRESS_BAR(data));

  gtk_progress_bar_set_text(GTK_PROGRESS_BAR(data), _("Saving image..."));
  val = gtk_progress_bar_get_fraction(GTK_PROGRESS_BAR(data));
  if (val + 0.01 <= 1.0 && val >= 0.)
    gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(data), val + 0.01);
  visu_ui_wait();
}

/**
 * visu_ui_rendering_window_dump:
 * @window:a valid #VisuUiRenderingWindow object ;
 * @format: a #VisuDump object, corresponding to the write method ;
 * @fileName: (type filename): a string that defined the file to write to ;
 * @width: an integer ;
 * @height: an integer ;
 * @functionWait: (allow-none) (closure data) (scope call): a method to call
 * periodically during the dump ;
 * @data: (closure): some pointer on object to be passed to the wait function.
 * @error: a location to store some error (not NULL) ;
 *
 * Call this method to dump the given @window to a file.
 *
 * Returns: TRUE if everything went right.
 */
gboolean visu_ui_rendering_window_dump(VisuUiRenderingWindow *window, VisuDump *format,
                                       const char* fileName, gint width, gint height,
                                       ToolVoidDataFunc functionWait, gpointer data,
                                       GError **error)
{
  GArray *imageData;
  gboolean res;
  double *zoomLevel, gross;

  g_return_val_if_fail(VISU_UI_IS_RENDERING_WINDOW(window), FALSE);
  g_return_val_if_fail(window->currentData, FALSE);
  g_return_val_if_fail(error && !*error, FALSE);
  g_return_val_if_fail(format && fileName, FALSE);

  if (visu_dump_getBitmapStatus(format))
    {
      DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: dumping current OpenGL area.\n");
      DBG_fprintf(stderr, " | requested size %dx%d.\n", width, height);
      imageData = visu_ui_gl_widget_getPixmapData(VISU_UI_GL_WIDGET(window->openGLArea),
                                                  &width, &height, TRUE,
                                                  visu_dump_getAlphaStatus(format));
      /*       visu_gl_view_setViewport(VISU_DATA(window->currentData), */
      /* 			     window->socketWidth, window->socketHeight); */
      /*       visu_ui_gl_widget_setRedraw(VISU_UI_GL_WIDGET(window->openGLArea), */
      /* 			     visu_gl_redraw, window->currentData); */
      DBG_fprintf(stderr, " | allocated size %dx%d.\n", width, height);
      if (!imageData)
	{
	  *error = g_error_new(VISU_ERROR_DUMP, DUMP_ERROR_OPENGL,
			       _("Can't dump OpenGL area to data.\n"));
	  return FALSE;
	}
    }
  else
    imageData = (GArray*)0;

  gross = window->view->camera->gross;
  if (visu_dump_getGlStatus(format) && !visu_dump_getBitmapStatus(format))
    {
      /* We must change the viewport... */
      visu_gl_view_setViewport(window->view,
                               (width > 0)?width:window->socketWidth,
                               (height > 0)?height:window->socketHeight);
      /* We unzoom to allow GL to render out-of-the-box points. */
      zoomLevel = g_malloc(sizeof(double));
      *zoomLevel = MAX(window->view->camera->gross, 1.f);
      g_object_set_data_full(G_OBJECT(format), "zoomLevel", zoomLevel, g_free);
      visu_gl_view_setGross(window->view, MIN(window->view->camera->gross, 1.f));
    }
  /* g_message("%p %s %d %d %p %p %s %p %p",(gpointer)format->fileType, fileName, */
  /* 			  width, height, (gpointer)window->currentData, */
  /* 			  (gpointer)imageData, error, (gpointer)functionWait, (gpointer)data); */
  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: call dump routine.\n");
  res = visu_dump_write(format, fileName, width, height, window->currentData,
                        imageData, functionWait, data, error);

  if (imageData)
    g_array_free(imageData, TRUE);
  if (visu_dump_getGlStatus(format) && !visu_dump_getBitmapStatus(format))
    {
      /* We must put back the viewport... */
      visu_gl_view_setViewport(window->view, window->socketWidth, window->socketHeight);
      /* We must put back the gross also. */
      visu_gl_view_setGross(window->view, gross);
    }
  if (res)
    /* Save file as recent. */
    _storeRecent(fileName);
  return res;
}

static void onExport(VisuUiRenderingWindow *window)
{
  GtkWidget *dump;
  char *filename;
  VisuDump *format;
  gboolean res;
  GError *error;
  GdkCursor *cursorWatch;
  GtkProgressBar *dumpBar;

  g_return_if_fail(VISU_UI_IS_RENDERING_WINDOW(window));

  dump = visu_ui_dump_dialog_new(window->currentData,
                                 (GtkWindow*)0, (const gchar*)0,
                                 window->view->window->width,
                                 window->view->window->height);
  if (gtk_dialog_run(GTK_DIALOG(dump)) != GTK_RESPONSE_ACCEPT)
    {
      gtk_widget_destroy(dump);
      return;
    }

  filename = visu_ui_dump_dialog_getFilename(VISU_UI_DUMP_DIALOG(dump));
  format = visu_ui_dump_dialog_getType(VISU_UI_DUMP_DIALOG(dump));
  g_return_if_fail(format && filename);

  DBG_fprintf(stderr, "Gtk VisuUiRenderingWindow: dump image to file '%s' (format : %s)\n",
	      filename, tool_file_format_getName(TOOL_FILE_FORMAT(format)));

  cursorWatch = gdk_cursor_new(GDK_WATCH);
  dumpBar = visu_ui_dump_dialog_getProgressBar(VISU_UI_DUMP_DIALOG(dump));
  visu_ui_dump_dialog_start(VISU_UI_DUMP_DIALOG(dump));
  gtk_progress_bar_set_fraction(dumpBar, 0.);
  if (visu_dump_getBitmapStatus(format))
    gtk_progress_bar_set_text(dumpBar,
                              _("Waiting for generating image in memory..."));
  visu_ui_wait();
  gdk_window_set_cursor(gtk_widget_get_window(GTK_WIDGET(dump)), cursorWatch);

  error = (GError*)0;
  DBG_fprintf(stderr, " | starting dump.\n");
  res = visu_ui_rendering_window_dump(window, format, filename,
                                   visu_ui_dump_dialog_getWidth(VISU_UI_DUMP_DIALOG(dump)),
                                   visu_ui_dump_dialog_getHeight(VISU_UI_DUMP_DIALOG(dump)),
                                      updateDumpProgressBar, (gpointer)dumpBar, &error);

  if (!res && error)
    visu_ui_raiseWarning(_("Saving a file"), error->message, (GtkWindow*)0);
  gdk_window_set_cursor(gtk_widget_get_window(GTK_WIDGET(dump)), NULL);
  if (error)
    g_error_free(error);
  DBG_fprintf(stderr, " | release UI resources.\n");

  gtk_widget_destroy(dump);
}

static void onRaiseButtonClicked(VisuUiRenderingWindow *window, gpointer user_data _U_)
{
  g_signal_emit(G_OBJECT(window), _signals[SHOW_MAIN_PANEL_SIGNAL],
		0 /* details */, NULL);
}

static void _redraw(VisuUiRenderingWindow *window, gboolean forceRedraw)
{
  VisuUiRenderingWindow *current;

  g_return_if_fail(VISU_UI_IS_RENDERING_WINDOW(window));
  if (!gtk_widget_get_visible(GTK_WIDGET(window)))
    return;
  current = VISU_UI_RENDERING_WINDOW_GET_CLASS(window)->redrawWidget;
  if (!current || current != window)
    return;

  if (!visu_gl_getImmediate() && !forceRedraw)
    {
      DBG_fprintf(stderr, "Redraw rejected since drawing is deferred and not forced.\n");
      return;
    }
  DBG_fprintf(stderr, "Redraw accepted let's go...\n");

  visu_ui_gl_widget_redraw(VISU_UI_GL_WIDGET(window->openGLArea));
}
/**
 * visu_ui_rendering_window_setCurrent:
 * @window: a valid #VisuUiRenderingWindow object ;
 * @force: a boolean.
 *
 * Set the OpenGL area as the current rendering area. If @force is TRUE
 * then the context is switched whatever buffered value.
 *
 * Since: 3.5
 */
void visu_ui_rendering_window_setCurrent(VisuUiRenderingWindow *window, gboolean status)
{
  g_return_if_fail(VISU_UI_IS_RENDERING_WINDOW(window));

  visu_ui_gl_widget_setCurrent(VISU_UI_GL_WIDGET(window->openGLArea), status);
  VISU_UI_RENDERING_WINDOW_GET_CLASS(window)->redrawWidget = window;
}

static void onRedraw(VisuUiRenderingWindow *window, gpointer data _U_)
{
  _redraw(window, FALSE);
}
static void onForceRedraw(VisuUiRenderingWindow *window, gpointer data _U_)
{
  _redraw(window, TRUE);
}
static void _onSearch(VisuUiRenderingWindow *window)
{
  if (!window->currentData)
    return;

#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 17
  gtk_widget_show(window->info->infoBar);
  gtk_widget_grab_focus(window->info->searchEntry);
  gtk_entry_set_icon_from_stock(GTK_ENTRY(window->info->searchEntry),
                                GTK_ENTRY_ICON_SECONDARY, NULL);
  gtk_entry_set_text(GTK_ENTRY(window->info->searchEntry), "");
#endif
}
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 17
static void _onSearchClose(GtkInfoBar *bar, gint response, gpointer data)
{
  if (response == GTK_RESPONSE_CLOSE)
    {
      gtk_widget_hide(GTK_WIDGET(bar));
      gtk_widget_grab_focus(VISU_UI_RENDERING_WINDOW(data)->openGLArea);
    }
}
static void _onSearchEdited(GtkEntry *entry, gpointer data)
{
  VisuUiRenderingWindow *window = VISU_UI_RENDERING_WINDOW(data);
  gint i;
  gchar *end;
  const gchar *val;
  VisuNode *node;

  gtk_entry_set_icon_from_stock(entry, GTK_ENTRY_ICON_SECONDARY, NULL);
  val = gtk_entry_get_text(entry);
  DBG_fprintf(stderr, "Gtk RenderingWindow: search on '%s'.\n", val);
  i = strtol(val, &end, 10);
  if (end == val || i <= 0)
    {
      gtk_entry_set_icon_from_stock(entry, GTK_ENTRY_ICON_SECONDARY,
                                    GTK_STOCK_DIALOG_WARNING);
      return;
    }
  i -= 1;
  node = visu_node_array_getFromId(VISU_NODE_ARRAY(window->currentData), i);
  if (!node)
    {
      gtk_entry_set_icon_from_stock(entry, GTK_ENTRY_ICON_SECONDARY,
                                    GTK_STOCK_DIALOG_WARNING);
      return;
    }
  visu_interactive_highlight(inter, window->currentData, i);
}
static gboolean _onSearchEsc(GtkWidget *widget, GdkEventKey *event, gpointer data _U_)
{
  if (event->keyval == GDK_KEY_Escape)
    {
      gtk_info_bar_response(GTK_INFO_BAR(widget), GTK_RESPONSE_CLOSE);
      return TRUE;
    }
  return FALSE;
}
#endif


/**
 * visu_ui_rendering_window_getMarks:
 * @window: a #VisuUiRenderingWindow object ;
 *
 * Get the #VisuGlExtMarks of the given @window.
 *
 * Returns: (transfer none): a #VisuGlExtMarks object owned by V_Sim.
 *
 * Since: 3.6
 */
VisuGlExtMarks* visu_ui_rendering_window_getMarks(VisuUiRenderingWindow *window)
{
  g_return_val_if_fail(VISU_UI_IS_RENDERING_WINDOW(window), (VisuGlExtMarks*)0);

  return window->marks;
}

/**
 * visu_ui_rendering_window_class_getInteractive:
 *
 * The user actions on the rendering area are handled by a
 * #VisuInteractive object.
 * 
 * Since: 3.6
 *
 * Returns: (transfer none): the #VisuInteractive object used by the rendering window of
 * V_Sim. This object is owned by V_Sim.
 */
VisuInteractive* visu_ui_rendering_window_class_getInteractive()
{
  return inter;
}

/**
 * visu_ui_rendering_window_class_setDisplayCoordinatesInReduce:
 * @status: a boolean.
 *
 * If set, the coordinates of picked nodes are displayed in reduced values.
 *
 * Returns: TRUE if the value has been changed.
 *
 * Since: 3.6
 */
gboolean visu_ui_rendering_window_class_setDisplayCoordinatesInReduce(gboolean status)
{
  if (!my_class)
    g_type_class_ref(VISU_UI_TYPE_RENDERING_WINDOW);

  if (status == my_class->useReducedCoordinates)
    return FALSE;
  my_class->useReducedCoordinates = status;
  return TRUE;
}
/**
 * visu_ui_rendering_window_class_getDisplayCoordinatesInReduce:
 *
 * Picked nodes have their coordinates displayed in the status bar of the rendering
 * window. This methods retrieves if they are printed in reduced values or not.
 *
 * Returns: TRUE if the coordinates are displayed in reduced values.
 *
 * Since: 3.6
 */
gboolean visu_ui_rendering_window_class_getDisplayCoordinatesInReduce()
{
  if (!my_class)
    g_type_class_ref(VISU_UI_TYPE_RENDERING_WINDOW);

  return my_class->useReducedCoordinates;
}
/**
 * visu_ui_rendering_window_class_setAutoAdjust:
 * @status: a boolean.
 *
 * If set, the camera auto adjust its zoom capability for the data to be
 * full size at zoom level 1.
 *
 * Returns: TRUE if the value has been changed.
 *
 * Since: 3.6
 */
gboolean visu_ui_rendering_window_class_setAutoAdjust(gboolean status)
{
  if (!my_class)
    g_type_class_ref(VISU_UI_TYPE_RENDERING_WINDOW);

  if (status == my_class->autoAdjust)
    return FALSE;
  my_class->autoAdjust = status;
  return TRUE;
}
/**
 * visu_ui_rendering_window_class_getAutoAdjust:
 *
 * The camera can be set to auto adjust its zoom capability for the data to be
 * full size at zoom level 1.
 *
 * Returns: TRUE if the camera auto adjust on data loading.
 *
 * Since: 3.6
 */
gboolean visu_ui_rendering_window_class_getAutoAdjust()
{
  if (!my_class)
    g_type_class_ref(VISU_UI_TYPE_RENDERING_WINDOW);

  return my_class->autoAdjust;
}

/*************************************/
/* Routines related to config files. */
/*************************************/
static void exportParameters(GString *data, VisuData *dataObj _U_, VisuGlView *view _U_)
{
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_RED_COORD);
  g_string_append_printf(data, "%s[gtk]: %d\n\n", FLAG_PARAMETER_RED_COORD,
			 visu_ui_rendering_window_class_getDisplayCoordinatesInReduce());
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_AUTO_ADJUST);
  g_string_append_printf(data, "%s[gtk]: %d\n\n", FLAG_PARAMETER_AUTO_ADJUST,
			 visu_ui_rendering_window_class_getAutoAdjust());
}
