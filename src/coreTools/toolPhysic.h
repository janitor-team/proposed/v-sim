/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef TOOLELEMENTS_H
#define TOOLELEMENTS_H

gboolean tool_physic_getSymbolFromZ(gchar **name, float *radcov, int zele);
gboolean tool_physic_getZFromSymbol(int *zele, float *radcov, gchar *symbol);

/**
 * ToolUnits:
 * @TOOL_UNITS_UNDEFINED: the units are undefined.
 * @TOOL_UNITS_BOHR: the length are given in Bohr (1ang = 0.529177Bohr);
 * @TOOL_UNITS_ANGSTROEM: the length are given in angstroems ;
 * @TOOL_UNITS_NANOMETER: the length are given in nanometers.
 * @TOOL_UNITS_N_VALUES: private.
 *
 * The possible length units defined in V_Sim. The special case
 * @TOOL_UNITS_UNDEFINED means that a unit must be defined before any
 * conversion operations may be done.
 *
 * Since: 3.5
 */
typedef enum
  {
    TOOL_UNITS_UNDEFINED,
    TOOL_UNITS_BOHR,
    TOOL_UNITS_ANGSTROEM,
    TOOL_UNITS_NANOMETER,
    /*< private >*/
    TOOL_UNITS_N_VALUES
  } ToolUnits;
const gchar** tool_physic_getUnitNames(void);
float tool_physic_getUnitValueInMeter(ToolUnits unit);
ToolUnits tool_physic_getUnitFromName(const gchar *name);


#endif
