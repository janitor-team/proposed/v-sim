/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresses m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef TOOLFORTRAN_H
#define TOOLFORTRAN_H

#include <stdio.h>
#include <glib.h>


/**
 * ToolFortranEndianId:
 * @TOOL_FORTRAN_ENDIAN_KEEP: read a file without inverting records.
 * @TOOL_FORTRAN_ENDIAN_CHANGE: read a file inverting records.
 * 
 * Thiese ids are used when reading a multi-bytes information from a binary file.
 */
typedef enum
  {
    TOOL_FORTRAN_ENDIAN_KEEP,
    TOOL_FORTRAN_ENDIAN_CHANGE
  } ToolFortranEndianId;

/**
 * tool_fortran_readCharacter:
 * @var: an allocated array of char ;
 * @nb: the size of the array @var ;
 * @flux: a pointer on an opened file ;
 * @error: a pointer to an error location ;
 * @endianness: reverse or not the order of multi-bytes ;
 * @testFlag: if TRUE, read start and stop flags and test their values
 * ;
 * @store: a boolean.
 *
 * Read an array of characters from a fortran record. The endianness is required
 * to read the Fortran flag. If argument @store is FALSE, then the
 * file is read and consistency checked but no data is stored. In that
 * case, @var can be not allocated.
 *
 * Returns: TRUE if everything went right.
 */
gboolean tool_fortran_readCharacter(char *var, guint nb, FILE *flux,
				   GError **error, ToolFortranEndianId endianness,
				   gboolean testFlag, gboolean store);
/**
 * tool_fortran_readInteger:
 * @var: an allocated array of int ;
 * @nb: the size of the array @var ;
 * @flux: a pointer on an opened file ;
 * @error: a pointer to an error location ;
 * @endianness: reverse or not the order of multi-bytes ;
 * @testFlag: if TRUE, read start and stop flags and test their
 * values ;
 * @store: a boolean.
 *
 * Read an array of integers from a fortran record. If argument @store
 * is FALSE, then the file is read and consistency checked but no data
 * is stored. In that case, @var can be not allocated.
 *
 * Returns: TRUE if everything went right.
 */
gboolean tool_fortran_readInteger(guint *var, guint nb, FILE *flux,
				 GError **error, ToolFortranEndianId endianness,
				 gboolean testFlag, gboolean store);
/**
 * tool_fortran_readReal:
 * @var: an allocated array of float ;
 * @nb: the size of the array @var ;
 * @flux: a pointer on an opened file ;
 * @error: a pointer to an error location ;
 * @endianness: reverse or not the order of multi-bytes ;
 * @testFlag: if TRUE, read start and stop flags and test their
 * values ;
 * @store: a boolean.
 *
 * Read an array of reals from a fortran record. If argument @store is FALSE, then the
 * file is read and consistency checked but no data is stored. In that
 * case, @var can be not allocated.
 *
 * Returns: TRUE if everything went right.
 */
gboolean tool_fortran_readReal(float *var, guint nb, FILE *flux,
			      GError **error, ToolFortranEndianId endianness,
			      gboolean testFlag, gboolean store);
/**
 * tool_fortran_readDouble:
 * @var: an allocated array of double ;
 * @nb: the size of the array @var ;
 * @flux: a pointer on an opened file ;
 * @error: a pointer to an error location ;
 * @endianness: reverse or not the order of multi-bytes ;
 * @testFlag: if TRUE, read start and stop flags and test their values
 * ;
 * @store: a boolean.
 *
 * Read an array of doubles from a fortran record. If argument @store
 * is FALSE, then the file is read and consistency checked but no data
 * is stored. In that case, @var can be not allocated.
 *
 * Returns: TRUE if everything went right.
 */
gboolean tool_fortran_readDouble(double *var, guint nb, FILE *flux,
				GError **error, ToolFortranEndianId endianness,
				gboolean testFlag, gboolean store);
/**
 * tool_fortran_readFlag:
 * @nb: a location t store the value of the flag ;
 * @flux: a pointer on an opened file ;
 * @error: a pointer to an error location ;
 * @endianness: reverse or not the order of multi-bytes.
 *
 * Read the flag of a record (a 32bits integer).
 *
 * Returns: TRUE if everything went right.
 */
gboolean tool_fortran_readFlag(guint *nb, FILE *flux,
			      GError **error, ToolFortranEndianId endianness);

/**
 * tool_fortran_testEndianness:
 * @nb: the value of the flag to read ;
 * @flux: a pointer on an opened file ;
 * @error: a pointer to an error location ;
 * @endianness: a location to store the endianness.
 *
 * Read a flag and compare the value with @nb for little and big endian.
 * It return the value of endianness to be used after. The file is rewind after the
 * call.
 *
 * Returns: TRUE if everything went right.
 */
gboolean tool_fortran_testEndianness(guint nb, FILE *flux,
				    GError **error, ToolFortranEndianId *endianness);


#endif
