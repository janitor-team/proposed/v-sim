/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include <glib.h>
#include <string.h>

#include "toolPhysic.h"

#include <visu_tools.h>

/**
 * SECTION:toolPhysic
 * @short_description: introduce physical values for the chemical species.
 *
 * This is a data base associating symbol names and atomic
 * numbers. One can also get the covalent radius of chemical
 * species. It is convenient to plot bindings.
 *
 * Since: 3.4
 */

struct periodic_table
{
  gchar *name;
  float radcov;
};

#define NUMBER_OF_ELEMENTS 103
static struct periodic_table eles[NUMBER_OF_ELEMENTS] = {
  {"H", 0.32},
  {"He", 0.93},
  {"Li", 1.23},
  {"Be", 0.90},
  {"B", 0.80},
  {"C", 0.77},
  {"N", 0.74},
  {"O", 0.73},
  {"F", 0.72},
  {"Ne", 0.71},
  {"Na", 1.54},
  {"Mg", 1.36},
  {"Al", 1.18},
  {"Si", 1.11},
  {"P", 1.06},
  {"S", 1.02},
  {"Cl", 0.99},
  {"Ar", 0.98},
  {"K", 2.03},
  {"Ca", 1.74},
  {"Sc", 1.44},
  {"Ti", 1.32},
  {"V", 1.22},
  {"Cr", 1.18},
  {"Mn", 1.17},
  {"Fe", 1.17},
  {"Co", 1.16},
  {"Ni", 1.15},
  {"Cu", 1.17},
  {"Zn", 1.25},
  {"Ga", 1.26},
  {"Ge", 1.22},
  {"As", 1.20},
  {"Se", 1.16},
  {"Br", 1.14},
  {"Kr", 1.12},
  {"Rb", 2.16},
  {"Sr", 1.91},
  {"Y", 1.62},
  {"Zr", 1.45},
  {"Nb", 1.34},
  {"Mo", 1.30},
  {"Tc", 1.27},
  {"Ru", 1.25},
  {"Rh", 1.25},
  {"Pd", 1.28},
  {"Ag", 1.34},
  {"Cd", 1.48},
  {"In", 1.44},
  {"Sn", 1.41},
  {"Sb", 1.40},
  {"Te", 1.36},
  {"I", 1.33},
  {"Xe", 1.31},
  {"Cs", 2.35},
  {"Ba", 1.98},
  {"La", 1.69},
  {"Ce", 1.65},
  {"Pr", 1.65},
  {"Nd", 1.64},
  {"Pm", 1.64},
  {"Sm", 1.62},
  {"Eu", 1.85},
  {"Gd", 1.61},
  {"Tb", 1.59},
  {"Dy", 1.59},
  {"Ho", 1.57},
  {"Er", 1.57},
  {"Tm", 1.56},
  {"Yb", 1.70},
  {"Lu", 1.56},
  {"Hf", 1.44},
  {"Ta", 1.34},
  {"W", 1.30},
  {"Re", 1.28},
  {"Os", 1.26},
  {"Ir", 1.27},
  {"Pt", 1.30},
  {"Au", 1.34},
  {"Hg", 1.49},
  {"Tl", 1.48},
  {"Pb", 1.47},
  {"Bi", 1.46},
  {"Po", 1.46},
  {"At", 1.45},
  {"Rn", 1.45},
  {"Fr", 2.50},
  {"Ra", 2.10},
  {"Ac", 1.85},
  {"Th", 1.65},
  {"Pa", 1.50},
  {"U", 1.42},
  {"Np", 1.42},
  {"Pu", 1.42},
  {"Am", 1.42},
  {"Cm", 1.42},
  {"Bk", 1.42},
  {"Cf", 1.42},
  {"Es", 1.42},
  {"Fm", 1.42},
  {"Md", 1.42},
  {"No", 1.42},
  {"Lr", 1.42}
};


/**
 * tool_physic_getSymbolFromZ:
 * @name: (out) (allow-none): a pointer on an unallocated string (can be NULL) ;
 * @radcov: (out) (allow-none): a pointer on a float (can be NULL) ;
 * @zele: the atomic number.
 *
 * Get the symbol or the covalence radius of the argument @zele.
 *
 * Returns: TRUE if zele is known in the atomic built-in list.
 */
gboolean tool_physic_getSymbolFromZ(gchar **name, float *radcov, int zele)
{
  g_return_val_if_fail(zele > 0 && zele < NUMBER_OF_ELEMENTS + 1, FALSE);

  if (name)
    *name = eles[zele - 1].name;
  if (radcov)
    *radcov = eles[zele - 1].radcov;

  return TRUE;
}
/**
 * tool_physic_getZFromSymbol:
 * @zele: (out) (allow-none): a pointer on an integer (can be NULL) ;
 * @radcov: (out) (allow-none): a pointer on a float (can be NULL) ;
 * @symbol: the symbol of an atom.
 *
 * Get the the covalence radius or the atomic number of a given atomic
 * @symbol.
 *
 * Returns: TRUE if @symbol is known in the atomic built-in list.
 */
gboolean tool_physic_getZFromSymbol(int *zele, float *radcov, gchar *symbol)
{
  int i;

  for (i = 0; i < NUMBER_OF_ELEMENTS; i++)
    {
      if (!strcmp(symbol, eles[i].name))
	{
	  if (radcov)
	    *radcov = eles[i].radcov;
	  if (zele)
	    *zele = i + 1;
	  return TRUE;
	}
    }
  return FALSE;
}

static const gchar* unitNames[TOOL_UNITS_N_VALUES + 1] = {"undefined", "bohr", "angstroem", "nanometer", NULL};
static const gchar* unitNamesAll[TOOL_UNITS_N_VALUES + 1][8] =
  {{"undefined", NULL, NULL, NULL, NULL, NULL, NULL, NULL},
   {"bohr", "Bohr", "bohrd0", "Bohrd0", "atomic", "Atomic", "atomicd0", "Atomicd0"},
   {"angstroem", "Angstroem", "angstroemd0", "Angstroemd0", "angstrom", "Angstrom", NULL, NULL},
   {"nanometer", "Nanometer", "nanometerd0", "Nanometerd0", NULL, NULL, NULL, NULL},
   {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}};
/**
 * tool_physic_getUnitNames:
 *
 * It provides the names corresponding to each units.
 *
 * Since: 3.5
 *
 * Returns: (transfer none) (array zero-terminated=1): an array, null
 * terminated of strings. It is owned by V_Sim.
 */
const gchar** tool_physic_getUnitNames(void)
{
  return unitNames;
}
static float unitValues[TOOL_UNITS_N_VALUES] = {-1.f, 5.291772108e-11f, 1e-10f, 1e-9f};
/**
 * tool_physic_getUnitValueInMeter:
 * @unit: a #ToolUnits.
 *
 * It provides the factor used to transform @unit into meters.
 *
 * Since: 3.5
 *
 * Returns: a factor.
 */
float tool_physic_getUnitValueInMeter(ToolUnits unit)
{
  g_return_val_if_fail(unit != TOOL_UNITS_UNDEFINED && unit != TOOL_UNITS_N_VALUES, -1.f);

  DBG_fprintf(stderr, "Visu Tools: get unit (%d) length.\n", unit);
  return unitValues[unit];
}
/**
 * tool_physic_getUnitFromName:
 * @name: a unit name.
 *
 * Find the unit corresponding to the @name. If none is found,
 * #TOOL_UNITS_UNDEFINED is returned.
 *
 * Since: 3.5
 *
 * Returns: a #ToolUnits.
 */
ToolUnits tool_physic_getUnitFromName(const gchar *name)
{
  int i, j;

  for (i = 0; i < TOOL_UNITS_N_VALUES; i++)
    for (j = 0; j < 8 && unitNamesAll[i][j]; j++)
      if (!strcmp(name, unitNamesAll[i][j]))
	return i;
  
  return TOOL_UNITS_UNDEFINED;
}
