/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef TOOLCONFIGFILE_H
#define TOOLCONFIGFILE_H

#include <visu_elements.h>

/**
 * TOOL_CONFIG_FILE_ERROR:
 *
 * Domain used to parse config files.
 */
#define TOOL_CONFIG_FILE_ERROR tool_config_file_getQuark()
GQuark tool_config_file_getQuark();
/**
 * ToolConfigFileError:
 * @TOOL_CONFIG_FILE_ERROR_EMPTY_LINE: error when reading the file, found an empty line,
 *                                where something should have been.
 * @TOOL_CONFIG_FILE_ERROR_BAD_ELEMENT_NAME: error when reading a #VisuElement name.
 * @TOOL_CONFIG_FILE_ERROR_MAX_ELEMENT: error because too much #VisuElement have been created.
 * @TOOL_CONFIG_FILE_ERROR_CREATED_ELEMENT: error when creating a new #VisuElement.
 * @TOOL_CONFIG_FILE_ERROR_VALUE: error of file format, values read are out of bounds.
 * @TOOL_CONFIG_FILE_ERROR_READ: error of file format (can't read variables...).
 * @TOOL_CONFIG_FILE_ERROR_MISSING: error of file format (missing
 * variables...).
 * @TOOL_CONFIG_FILE_ERROR_TAG: error dealing with a tag.
 * @TOOL_CONFIG_FILE_ERROR_MARKUP: error dealing with a markup (unkown
 * one...).
 * @TOOL_CONFIG_FILE_ERROR_NO_FILE: no valid file found on disk.
 *
 * Possible errors when parsing a config file.
 */
typedef enum
  {
    TOOL_CONFIG_FILE_ERROR_EMPTY_LINE,
    TOOL_CONFIG_FILE_ERROR_BAD_ELEMENT_NAME,
    TOOL_CONFIG_FILE_ERROR_MAX_ELEMENT,
    TOOL_CONFIG_FILE_ERROR_CREATED_ELEMENT,
    TOOL_CONFIG_FILE_ERROR_VALUE,
    TOOL_CONFIG_FILE_ERROR_READ,
    TOOL_CONFIG_FILE_ERROR_MISSING,
    TOOL_CONFIG_FILE_ERROR_TAG,
    TOOL_CONFIG_FILE_ERROR_MARKUP,
    TOOL_CONFIG_FILE_ERROR_NO_FILE
  } ToolConfigFileError;

gboolean tool_config_file_readFloatFromTokens(gchar **tokens, int *position, float *values,
                                             guint size, int lineId, GError **error);
gboolean tool_config_file_readFloat(gchar *line, int position, float *values,
                                   guint size, GError **error);
gboolean tool_config_file_readFloatWithElement(gchar *line, int position, float *values,
                                              guint size, VisuElement **ele,
                                              GError **error);
gboolean tool_config_file_readIntegerFromTokens(gchar **tokens, int *position, int *values,
                                               guint size, int lineId, GError **error);
gboolean tool_config_file_readInteger(gchar *line, int position, int *values,
                                     guint size, GError **error);
gboolean tool_config_file_readBooleanFromTokens(gchar **tokens, int *position,
                                               gboolean *values, guint size,
                                               int lineId, GError **error);
gboolean tool_config_file_readBoolean(gchar *line, int position, gboolean *values,
                                     guint size, GError **error);
gboolean tool_config_file_readBooleanWithElement(gchar *line, int position,
                                                gboolean *values, guint size,
                                                VisuElement **ele, GError **error);
gboolean tool_config_file_readStringFromTokens(gchar **tokens, int *position,
                                              gchar ***values, guint size,
                                              int lineId, GError **error);
gboolean tool_config_file_readString(gchar *line, int position, gchar ***values,
                                    guint size, gboolean join, GError **error);
gboolean tool_config_file_readStringWithElement(gchar *line, int position,
                                               gchar ***values, guint size,
                                               VisuElement **ele, GError **error);
gboolean tool_config_file_readElementFromTokens(gchar **tokens, int *position,
                                               VisuElement **values, guint size,
                                               int lineId, GError **error);
gboolean tool_config_file_clampFloat(float *variable, float value, float min, float max);


#endif
