/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "visu_pairs.h"

#include <math.h>
#include <string.h>

#include "visu_tools.h"
#include "visu_object.h"
#include "visu_nodes.h"
#include "openGLFunctions/objectList.h"
#include "openGLFunctions/text.h"
#include "visu_configFile.h"
#include "coreTools/toolConfigFile.h"

/**
 * SECTION:visu_pairs
 * @short_description: V_Sim can draw link between nodes. This part
 * defines a pair object and interface to draw pairs.
 *
 * <para>The visu_pairs.c defines only general methods to draw
 * pairs. It introduces a new object called #VisuPairLink. This stores
 * some characteristics on links between two #VisuElement. The main
 * characteristic is that pairs are drawn only if the length between
 * two nodes is in a specific range. Use visu_pair_link_setDistance() and
 * visu_pair_link_getDistance() to tune this range.</para>
 *
 * <para>This file does not draw any pairs. But it gives some
 * interface to create rendering capabilities. To create a new pair
 * rendering module, called #VisuPairExtension, use
 * visu_pair_extension_new(). Basically, a #VisuPairExtension is characterized
 * by it drawing method. But it can have other methods that are called
 * in different cases. See main() and
 * startStop() prototypes to have more informations.</para>
 */

/**
 * _Property:
 * @name: a pointer to a string, only used when DEBUG is on ;
 * @data: a pointer to an allocated memory area ;
 * @freeFunc: a destroy functon (can be NULL).
 *
 * This container can be used in hashtable to store whatever kind of
 * data that requires specific free functions.
 */
struct _Property
{
  /* A convenient debug pointer on the name. */
  const gchar *name;

  /* This is a pointer to the data stored. */
  gpointer data;
  
  /* This method is called when the table is freed. */
  GDestroyNotify freeFunc;
};
typedef struct _Property Property;

/* This structure is made to store pairs information between two
   elements. */
struct _VisuPair
{
  VisuElement *ele1;
  VisuElement *ele2;

  /* This is a GList of link (VisuPairLink). */
  GList *pairs;

  /* A list of properties. */
  GHashTable *properties;
};

/**
 * VisuPairLinkClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuPairLinkClass structure.
 */
/**
 * VisuPairLink:
 *
 * An opaque structure.
 */
/**
 * VisuPairLinkPrivate:
 * @minMax: storage for the length bounds for drawn pairs ;
 * @drawn: a boolean to say if the pair is drawn or not ;
 * @printLength: a boolean to say if length of pairs are drawn near
 * them ;
 *
 * This structure is used to describe a link between two elements. A
 * link is drawn only its length is between the minimum and the
 * maximum value stored in the minMax array.
 */
struct _VisuPairLinkPrivate
{
  VisuPair *parent;
  gboolean dispose_has_run;

  float minMax[2];
  gboolean drawn;
  gboolean printLength;
  ToolColor color;
};

static void visu_pair_link_finalize(GObject* obj);
static void visu_pair_link_dispose(GObject* obj);

struct foreachPairsData_struct
{
  VisuPairForeachFunc func;
  gpointer userData;
};

#define BONDHISTOGRAM_ID   "bondDistribution_data"
#define BONDHISTOGRAM_STEP 0.1f
#define BONDHISTOGRAM_MIN  0.f
#define BONDHISTOGRAM_MAX  10.f

/* This hashtable as VisuElement* as keys and pointer to other hashtable as value.
   The main idea is to have an hashtable of 2 dimension, each two keys are
   VisuElement* and the final values are PairsData. Nevertheless, this hashtable
   must not be access directly but through the get and put methods. */
static GHashTable *DminDmax = NULL;

/* Parameters */
static ToolColor *defaultPairColor = NULL;

/* Local methods. */
static void freePair(gpointer ele);
static void freeHistoData(gpointer data);
static VisuPair* newPair(VisuElement *ele1, VisuElement *ele2);

/**
 * visu_pair_init: (skip)
 *
 * Initialise the hashtable storing pairs.
 */
static void visu_pair_init()
{
  DminDmax = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, freePair);
}

/*************************/
/* VisuPairLink methods. */
/*************************/
enum {
  PARAMETER_CHANGED_SIGNAL,
  LAST_SIGNAL
};
static guint _signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE(VisuPairLink, visu_pair_link, G_TYPE_OBJECT)

static void visu_pair_link_class_init(VisuPairLinkClass *klass)
{
  float rgbOfPairs[4] = {1.0, 0.6, 0.2, 1.};

  DBG_fprintf(stderr, "Visu Pair Data: creating the class of the object.\n");

  DBG_fprintf(stderr, "                - adding new signals ;\n");
  /**
   * VisuPairLink::ParameterChanged:
   * @data: the object which received the signal ;
   *
   * Gets emitted when some rendering parameter of @data has been changed.
   *
   * Since: 3.7
   */
  _signals[PARAMETER_CHANGED_SIGNAL] =
    g_signal_new("ParameterChanged", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE,
                 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0, NULL);

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_pair_link_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_pair_link_finalize;

  defaultPairColor = tool_color_new(rgbOfPairs);
  tool_color_addColor(defaultPairColor);
}
static void visu_pair_link_init(VisuPairLink *obj)
{
  DBG_fprintf(stderr, "Visu Pair Data: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = g_malloc(sizeof(VisuPairLinkPrivate));
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->parent            = (VisuPair*)0;
  obj->priv->minMax[VISU_PAIR_DISTANCE_MIN] = G_MAXFLOAT;
  obj->priv->minMax[VISU_PAIR_DISTANCE_MAX] = G_MAXFLOAT;
  obj->priv->drawn             = TRUE;
  obj->priv->printLength       = FALSE;
  tool_color_copy(&obj->priv->color, defaultPairColor);
}
static void visu_pair_link_dispose(GObject* obj)
{
  VisuPairLink *data;

  DBG_fprintf(stderr, "Visu Pair Data: dispose object %p.\n", (gpointer)obj);

  data = VISU_PAIR_LINK(obj);
  if (data->priv->dispose_has_run)
    return;
  data->priv->dispose_has_run = TRUE;

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_pair_link_parent_class)->dispose(obj);
}
static void visu_pair_link_finalize(GObject* obj)
{
  VisuPairLink *data;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Visu Pair Data: finalize object %p.\n", (gpointer)obj);

  data = VISU_PAIR_LINK(obj);
  /* Free privs elements. */
  if (data->priv)
    {
      DBG_fprintf(stderr, "Visu Pair Data: free private legend.\n");
      g_free(data->priv);
    }

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Visu Pair Data: chain to parent.\n");
  G_OBJECT_CLASS(visu_pair_link_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Visu Pair Data: freeing ... OK.\n");
}
/**
 * visu_pair_link_getAll:
 * @ele1: a #VisuElement object ;
 * @ele2: a #VisuElement object.
 *
 * There can be one or several links between elements, retrieve them
 * with this routine.
 *
 * Returns: (element-type VisuPairLink*) (transfer none): a list of
 * #VisuPairLink. The list is owned by V_Sim and should not be freed.
 */
GList* visu_pair_link_getAll(VisuElement *ele1, VisuElement *ele2)
{
  VisuPair *pair;
  VisuPairLink *data;

  pair = visu_pair_getPair(ele1, ele2);
  DBG_fprintf(stderr, "Visu Pairs: links for key '%s %s' -> %p.\n",
	      ele1->name, ele2->name, (gpointer)pair->pairs);
  if (!pair->pairs)
    {
      data = VISU_PAIR_LINK(g_object_new(VISU_TYPE_PAIR_LINK, NULL));
      data->priv->minMax[VISU_PAIR_DISTANCE_MIN] = 0.f;
      data->priv->minMax[VISU_PAIR_DISTANCE_MAX] = 0.f;
      data->priv->parent = pair;
      pair->pairs = g_list_append(pair->pairs, (gpointer)data);
    }
  return pair->pairs;
}
/**
 * visu_pair_link_getFromId:
 * @ele1: a #VisuElement object ;
 * @ele2: a #VisuElement object ;
 * @pos: the position in the list of links.
 *
 * A link can also be retrieved by its position.
 *
 * Returns: (transfer none): the #VisuPairLink object associated to the given two
 *          elements and distances. If none exists NULL is returned.
 */
VisuPairLink* visu_pair_link_getFromId(VisuElement *ele1, VisuElement *ele2, guint pos)
{
  VisuPair *pair;

  pair = visu_pair_getPair(ele1, ele2);
  g_return_val_if_fail(pair->pairs, (VisuPairLink*)0);
  return (VisuPairLink*)(g_list_nth(pair->pairs, pos)->data);
}
/**
 * visu_pair_link_new:
 * @ele1: a #VisuElement object ;
 * @ele2: a #VisuElement object ;
 * @minMax: (array fixed-size=2): the two min and max distances.
 *
 * A link between two elements is characterized by its boundary distances.
 *
 * Returns: (transfer none): the #VisuPairLink object associated to the given two
 *          elements and distances. If none exists it is created. The
 *          returned value should not be freed.
 */
VisuPairLink* visu_pair_link_new(VisuElement *ele1, VisuElement *ele2, float minMax[2])
{
  VisuPair *pair;
  GList *tmpLst;
  VisuPairLink *data;

  g_return_val_if_fail(minMax, (VisuPairLink*)0);

  pair = visu_pair_getPair(ele1, ele2);
  g_return_val_if_fail(pair, (VisuPairLink*)0);
  for (tmpLst = pair->pairs; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      data = (VisuPairLink*)tmpLst->data;
      DBG_fprintf(stderr, " | test %p (%g %g).\n", tmpLst->data,
		  data->priv->minMax[0], data->priv->minMax[1]);
      if (data->priv->minMax[0] == minMax[0] && data->priv->minMax[1] == minMax[1])
	return data;
    }
  data = VISU_PAIR_LINK(g_object_new(VISU_TYPE_PAIR_LINK, NULL));
  data->priv->minMax[VISU_PAIR_DISTANCE_MIN] = minMax[VISU_PAIR_DISTANCE_MIN];
  data->priv->minMax[VISU_PAIR_DISTANCE_MAX] = minMax[VISU_PAIR_DISTANCE_MAX];
  data->priv->parent = pair;
  pair->pairs = g_list_append(pair->pairs, (gpointer)data);
  DBG_fprintf(stderr, " | new %p (%g %g).\n", (gpointer)data,
	      data->priv->minMax[0], data->priv->minMax[1]);

  return data;
}
/**
 * visu_pair_link_setDrawn:
 * @data: a #VisuPairLink object ;
 * @drawn: a boolean.
 *
 * A pair can or cannot be drawn, use this method to tune it.
 *
 * Returns: TRUE if parameter has been changed.
 */
gboolean visu_pair_link_setDrawn(VisuPairLink *data, gboolean drawn)
{
  g_return_val_if_fail(VISU_IS_PAIR_LINK(data), FALSE);

  DBG_fprintf(stderr, "Visu Pairs: set drawn status %d (%d) for %p.\n",
	      (int)drawn, (int)data->priv->drawn, (gpointer)data);
  if (data->priv->drawn == drawn)
    return FALSE;

  data->priv->drawn = drawn;
  g_signal_emit(data, _signals[PARAMETER_CHANGED_SIGNAL], 0, NULL);
  return TRUE;
}
/**
 * visu_pair_link_setPrintLength:
 * @data: a #VisuPairLink object ;
 * @status: TRUE to print length near pairs.
 *
 * Set the attribute that controls if the length of pairs are drawn near pairs.
 *
 * Returns: TRUE if parameter has been changed.
 */
gboolean visu_pair_link_setPrintLength(VisuPairLink *data, gboolean status)
{
  g_return_val_if_fail(VISU_IS_PAIR_LINK(data), FALSE);

  DBG_fprintf(stderr, "Visu Pairs: set print length status %d (%d) for %p.\n",
	      (int)status, (int)data->priv->printLength, (gpointer)data);
  if (data->priv->printLength == status)
    return FALSE;

  data->priv->printLength = status;
  if (visu_pair_link_isDrawn(data))
    g_signal_emit(data, _signals[PARAMETER_CHANGED_SIGNAL], 0, NULL);
  return TRUE;
}
/**
 * visu_pair_link_getDrawn:
 * @data: a #VisuPairLink object ;
 *
 * A pair can or cannot be drawn, use this method to retrieve its state.
 *
 * Returns: TRUE if pairs can be drawn.
 */
gboolean visu_pair_link_getDrawn(const VisuPairLink *data)
{
  g_return_val_if_fail(VISU_IS_PAIR_LINK(data), FALSE);
  return data->priv->drawn;
}
/**
 * visu_pair_link_setColor:
 * @data: a #VisuPairLink object ;
 * @destColor: a #ToolColor object.
 *
 * Set the color of the given pair.
 *
 * Returns: TRUE if parameter has been changed.
 */
gboolean visu_pair_link_setColor(VisuPairLink *data, ToolColor* destColor)
{
  g_return_val_if_fail(VISU_IS_PAIR_LINK(data) && destColor, FALSE);

  DBG_fprintf(stderr, "Visu Pairs: set color [%g;%g;%g] for %p.\n",
	      destColor->rgba[0], destColor->rgba[1], destColor->rgba[2],
	      (gpointer)data);

  if (tool_color_equal(&data->priv->color, destColor))
    return FALSE;

  /* Copy values of dest to current color. */
  tool_color_copy(&data->priv->color, destColor);
  if (visu_pair_link_isDrawn(data))
    g_signal_emit(data, _signals[PARAMETER_CHANGED_SIGNAL], 0, NULL);
  return TRUE;
}
/**
 * visu_pair_link_getColor:
 * @data: a #VisuPairLink object.
 *
 * Look for the properties of the pair @data to find if a colour has
 * been defined. If none, the default colour is returned instead.
 *
 * Returns: (transfer none): a colour (don't free it).
 */
ToolColor* visu_pair_link_getColor(const VisuPairLink *data)
{
  g_return_val_if_fail(VISU_IS_PAIR_LINK(data), defaultPairColor);

  return &data->priv->color;
}
/**
 * visu_pair_link_setDistance:
 * @val: a floating point value ;
 * @data: a #VisuPairLink object ;
 * @minOrMax: #VISU_PAIR_DISTANCE_MAX or #VISU_PAIR_DISTANCE_MIN.
 *
 * Set the minimum or the maximum length for the given pair.
 *
 * Returns: TRUE if parameter has been changed.
 */
gboolean visu_pair_link_setDistance(VisuPairLink *data, float val, int minOrMax)
{
  g_return_val_if_fail(VISU_IS_PAIR_LINK(data) &&
                       (minOrMax == VISU_PAIR_DISTANCE_MIN ||
                        minOrMax == VISU_PAIR_DISTANCE_MAX), FALSE);

  if (data->priv->minMax[minOrMax] == val)
    return FALSE;

  data->priv->minMax[minOrMax] = val;
  if (visu_pair_link_isDrawn(data))
    g_signal_emit(data, _signals[PARAMETER_CHANGED_SIGNAL], 0, NULL);
  return TRUE;
}
/**
 * visu_pair_link_getPrintLength:
 * @data: a #VisuPairLink object.
 * 
 * Get the print length parameter of a pair. This parameter is used to tell if
 * length should be drawn near pairs of this kind.
 *
 * Returns: TRUE if length are printed.
 */
gboolean visu_pair_link_getPrintLength(const VisuPairLink *data)
{
  g_return_val_if_fail(VISU_IS_PAIR_LINK(data), FALSE);
  return data->priv->printLength;
}
/**
 * visu_pair_link_getDistance:
 * @data: a #VisuPairLink object ;
 * @minOrMax: #VISU_PAIR_DISTANCE_MIN or #VISU_PAIR_DISTANCE_MAX.
 *
 * A pair between @ele1 and @ele2 is drawn only if its length is between
 * a minimum and a maximum value. This method can get these values.
 *
 * Returns: the minimum or the maximum value for the pair between @ele1 and @ele2.
 */
float visu_pair_link_getDistance(const VisuPairLink *data, int minOrMax)
{
  g_return_val_if_fail(VISU_IS_PAIR_LINK(data), 0.f);
  g_return_val_if_fail(minOrMax == VISU_PAIR_DISTANCE_MIN ||
                       minOrMax == VISU_PAIR_DISTANCE_MAX, 0.);

  return data->priv->minMax[minOrMax];
}
/**
 * visu_pair_link_getPair:
 * @data: a #VisuPairLink object.
 *
 * A link object is always associated to a #VisuPair.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): the #VisuPair this link is related to.
 **/
VisuPair* visu_pair_link_getPair(const VisuPairLink *data)
{
  g_return_val_if_fail(VISU_IS_PAIR_LINK(data), (VisuPair*)0);

  return data->priv->parent;
}
/**
 * visu_pair_link_isDrawn:
 * @data: a #VisuPairLink object.
 *
 * A link is used or not depending on a distance criterion and a flag,
 * see visu_pair_link_setDrawn() and visu_pair_link_setDistance().
 *
 * Since: 3.7
 *
 * Returns: TRUE if the @data is indeed drawn or not.
 **/
gboolean visu_pair_link_isDrawn(const VisuPairLink *data)
{
  g_return_val_if_fail(VISU_IS_PAIR_LINK(data), FALSE);

  return (data->priv->drawn &&
          data->priv->minMax[VISU_PAIR_DISTANCE_MAX] >
          data->priv->minMax[VISU_PAIR_DISTANCE_MIN]);
}

/********************/
/* Visu pair stuff. */
/********************/
/**
 * visu_pair_setProperty:
 * @pair: a #VisuPair object ;
 * @key: a static string ;
 * @value: a pointer to some allocated data ;
 * @freeFunc: a destroying method (can be NULL).
 *
 * Each element/element can have associated data.
 */
void visu_pair_setProperty(VisuPair *pair, const gchar* key,
			  gpointer value, GDestroyNotify freeFunc)
{
  Property *prop;

  g_return_if_fail(pair && key && *key);

  prop           = g_malloc(sizeof(Property));
  prop->name     = key;
  prop->data     = value;
  prop->freeFunc = freeFunc;
  g_hash_table_insert(pair->properties, (gpointer)key, (gpointer)prop);
}
/**
 * visu_pair_getProperty:
 * @pair: a #VisuPair object ;
 * @key: a string.
 *
 * Retrieve the property associated to the @key or NULL if none exist.
 *
 * Returns: (transfer none): the associated data.
 */
gpointer visu_pair_getProperty(VisuPair *pair, const gchar* key)
{
  Property *prop;

  g_return_val_if_fail(pair, (gpointer)0);

  prop = (Property*)g_hash_table_lookup(pair->properties, (gpointer)key);
  if (prop)
    return prop->data;
  else
    return (gpointer)0;
}

static void freePair(gpointer ele)
{
  VisuPair *pair;
  GList *tmpLst;

  pair = (VisuPair*)ele;
  DBG_fprintf(stderr, "Visu Pairs: freeing pair between '%s' and '%s'.\n",
	      pair->ele1->name, pair->ele2->name);
  for (tmpLst = pair->pairs; tmpLst; tmpLst = g_list_next(tmpLst))
    g_object_unref(tmpLst->data);
  g_list_free(pair->pairs);
  g_hash_table_destroy(pair->properties);
}
/* Create a new PairsData structure with default values.
   The newly created structure can be freed by a call to free. */
static VisuPair* newPair(VisuElement *ele1, VisuElement *ele2)
{
  VisuPair *pair;

  g_return_val_if_fail(ele1 && ele2, (VisuPair*)0);

  DBG_fprintf(stderr, "Visu Pairs: create a new pair between '%s' and '%s'.\n",
	      ele1->name, ele2->name);

  pair        = g_malloc(sizeof(VisuPair));
  pair->ele1  = ele1;
  pair->ele2  = ele2;
  pair->pairs = (GList*)0;
  pair->properties = g_hash_table_new_full(g_str_hash, g_str_equal,
					   NULL, g_free);
  g_return_val_if_fail(pair->properties, (VisuPair*)0);

/*   pair->pairs = g_list_prepend(pair->pairs, (gpointer)newPairData(minMax)); */

  return pair;
}
/**
 * visu_pair_getPair:
 * @ele1: a #VisuElement object ;
 * @ele2: a #VisuElement object.
 *
 * The object #VisuPair is used to characterized links between two elements.
 *
 * Returns: (transfer none): the #VisuPair object associated to the
 * given two elements. If none exists it is created. The returned
 * value should not be freed.
 */
VisuPair* visu_pair_getPair(VisuElement *ele1, VisuElement *ele2)
{
  VisuPair *pair;
  gchar *key;

  g_return_val_if_fail(ele1 && ele2, (VisuPair*)0);

  if (!DminDmax)
    visu_pair_init();

  if (strcmp(ele1->name, ele2->name) < 0)
    key = g_strdup_printf("%s %s", ele1->name, ele2->name);
  else
    key = g_strdup_printf("%s %s", ele2->name, ele1->name);
  pair = (VisuPair*)g_hash_table_lookup(DminDmax, (gpointer)key);
  DBG_fprintf(stderr, "Visu Pairs: test key '%s' -> %p.\n", key, (gpointer)pair);
  if (!pair)
    {
      /* Ok, create one if none found. */
      pair = newPair(ele1, ele2);
      g_hash_table_insert(DminDmax, (gpointer)key, (gpointer)pair);
    }
  else
    g_free(key);

  return pair;
}
/**
 * visu_pair_removePairLink:
 * @ele1: a #VisuElement object ;
 * @ele2: a #VisuElement object ;
 * @data: a link object.
 *
 * Delete the given link.
 *
 * Returns: TRUE if the link exists and has been successfully removed.
 */
gboolean visu_pair_removePairLink(VisuElement *ele1, VisuElement *ele2, VisuPairLink *data)
{
  VisuPair *pair;
  GList *tmpLst;

  g_return_val_if_fail(data, FALSE);

  pair = visu_pair_getPair(ele1, ele2);
  g_return_val_if_fail(pair, FALSE);
  tmpLst = g_list_find(pair->pairs, data);
  if (tmpLst)
    pair->pairs = g_list_delete_link(pair->pairs, tmpLst);
  g_signal_emit_by_name(G_OBJECT(data), "ParameterChanged", NULL);
  g_object_unref(data);
  return (tmpLst != (GList*)0);
}
/**
 * visu_pair_getElements:
 * @pair: a #VisuPair object.
 * @ele1: (out) (allow-none) (transfer none): a location to store a
 * #VisuElement object pointer.
 * @ele2: (out) (allow-none) (transfer none): a location to store a
 * #VisuElement object pointer.
 *
 * Retrieve the #VisuElement constituting the pair.
 *
 * Since: 3.7
 **/
void visu_pair_getElements(const VisuPair *pair, VisuElement **ele1, VisuElement **ele2)
{
  g_return_if_fail(pair);

  if (ele1)
    *ele1 = pair->ele1;
  if (ele2)
    *ele2 = pair->ele2;
}

static void freeHistoData(gpointer data)
{
  DBG_fprintf(stderr, "Visu Pairs: free '%s' data.\n", BONDHISTOGRAM_ID);
  g_free(((VisuPairDistribution*)data)->histo);
  g_free(data);
}
/**
 * visu_pair_getDistanceDistribution:
 * @pair: a #VisuPair ;
 * @dataObj: a #VisuData ;
 * @step: a float for the distance mesh (negative value to use
 * built-in default) ;
 * @min: a float for the minimum scanning value (negative value to use
 * built-in default).
 * @max: a float for the maximum scanning value (negative value to use
 * built-in default).
 * 
 * This will compute the distnace distribution of nodes for the given
 * @pair.
 *
 * Returns: a structure defining the distance distribution. This
 * structure is private and should not be freed.
 */
VisuPairDistribution* visu_pair_getDistanceDistribution(VisuPair *pair,
						       VisuData *dataObj,
						       float step,
						       float min, float max)
{
  VisuPairDistribution *dd;
  guint i;
  VisuNodeArrayIter iter1, iter2;
  float d2, inv;
  float xyz1[3], xyz2[3];
#if DEBUG == 1
  guint nRef;
  GTimer *timer;
  gulong fractionTimer;
#endif

  g_return_val_if_fail(pair && VISU_IS_DATA(dataObj), (VisuPairDistribution*)0);

#if DEBUG == 1
  timer = g_timer_new();
  g_timer_start(timer);
#endif

  /* We create the storage structure. */
  dd = (VisuPairDistribution*)
    visu_pair_getProperty(pair, BONDHISTOGRAM_ID);
  if (dd)
    g_free(dd->histo);
  else
    {
      dd = g_malloc(sizeof(VisuPairDistribution));
      visu_pair_setProperty(pair, BONDHISTOGRAM_ID,
			   (gpointer)dd, freeHistoData);
    }
  dd->ele1 = pair->ele1;
  dd->ele2 = pair->ele2;
  dd->nNodesEle1 = 0;
  dd->nNodesEle2 = 0;
  dd->stepValue = (step > 0.f)?step:BONDHISTOGRAM_STEP;
  dd->initValue = (min > 0.f)?min:BONDHISTOGRAM_MIN;
  dd->nValues   = (int)((((max > 0.f)?max:BONDHISTOGRAM_MAX) -
			 dd->initValue) / dd->stepValue) + 1;
  dd->histo     = g_malloc0(sizeof(int) * dd->nValues);

  DBG_fprintf(stderr, "Visu Pairs: compute distance distribution (%p %g %d).\n",
	      (gpointer)dd, dd->stepValue, dd->nValues);

  /* We compute the distribution. */
  visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter1);
  inv = 1.f / dd->stepValue;
  iter1.element = pair->ele1;
  for(visu_node_array_iterRestartNode(VISU_NODE_ARRAY(dataObj), &iter1); iter1.node;
      visu_node_array_iterNextNodeOriginal(VISU_NODE_ARRAY(dataObj), &iter1))
    {
      if (!iter1.node->rendered)
	continue;
      dd->nNodesEle1 += 1;
      visu_data_getNodePosition(dataObj, iter1.node, xyz1);

      visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter2);
      iter2.element = pair->ele2;
/*       fprintf(stderr, "## %d\n", dd->histo[177]); */
      for(visu_node_array_iterRestartNode(VISU_NODE_ARRAY(dataObj), &iter2); iter2.node;
	  visu_node_array_iterNextNode(VISU_NODE_ARRAY(dataObj), &iter2))
	{
	  if (!iter2.node->rendered)
	    continue;
	  /* Don't count the inter element pairs two times. */
	  if (iter1.element == iter2.element &&
	      iter2.node == iter1.node)
	    continue;

	  visu_data_getNodePosition(dataObj, iter2.node, xyz2);
	  d2 = (xyz1[0] - xyz2[0]) * (xyz1[0] - xyz2[0]) + 
	    (xyz1[1] - xyz2[1]) * (xyz1[1] - xyz2[1]) + 
	    (xyz1[2] - xyz2[2]) * (xyz1[2] - xyz2[2]);
	  /* We put the distance into the histogram. */
	  dd->histo[MIN((guint)((sqrt(d2) - dd->initValue) * inv), dd->nValues - 1)] += 1;
/* 	  fprintf(stderr, "%d-%d %d\n", iter1.node->number, iter2.node->number, dd->histo[177]); */
	}
/*       fprintf(stderr, "-> %d\n", dd->histo[177]); */
    }
  for(visu_node_array_iterRestartNode(VISU_NODE_ARRAY(dataObj), &iter2); iter2.node;
      visu_node_array_iterNextNode(VISU_NODE_ARRAY(dataObj), &iter2))
    if (iter2.node->rendered)
      dd->nNodesEle2 += 1;
  if (iter1.element == iter2.element)
    for (i = 0; i < dd->nValues; i++)
      dd->histo[i] /= 2;

#if DEBUG == 1
  g_timer_stop(timer);

  for (nRef = 0; nRef < dd->nValues; nRef++)
    fprintf(stderr, " | %03d -> %6.3f, %5d\n", nRef,
            dd->initValue + dd->stepValue * nRef, dd->histo[nRef]);
  fprintf(stderr, "Visu Pairs: distances analysed in %g micro-s.\n",
	  g_timer_elapsed(timer, &fractionTimer)/1e-6);

  g_timer_destroy(timer);
#endif

  return dd;
}

/**
 * visu_pair_distribution_getNextPick:
 * @dd: a #VisuPairDistribution object.
 * @startStopId: two ids.
 * @integral: a location for a guint value, can be NULL.
 * @max: a location to store the value ;
 * @posMax: a location to store the position of the pick.
 *
 * Try to find the next pick in the distribution. A pick is a group of
 * consecutive non-null values, with a significant integral. On enter,
 * @startStopId contains the span to look into for the pick, and on
 * output, it contains the span of the pick itself.
 * 
 * Since: 3.6
 * 
 * Returns: TRUE if a pick is found.
 */
gboolean visu_pair_distribution_getNextPick(VisuPairDistribution *dd,
                                            guint startStopId[2], guint *integral,
                                            guint *max, guint *posMax)
{
  float min, start, stop;
  guint i, iStart, iStop, sum, _posMax, _max;

  g_return_val_if_fail(dd, FALSE);
  g_return_val_if_fail(startStopId[1] < dd->nValues, FALSE);

  iStart = startStopId[0];
  iStop  = startStopId[1];
  _max = 0;
  _posMax = 0;
  min = 1.5f * MIN(dd->nNodesEle1, dd->nNodesEle2);
  DBG_fprintf(stderr, "Visu Pairs: look for one pick in %d-%d.\n",
              startStopId[0], startStopId[1]);
  do
    {
      min  *= 0.5f;
      start = -1.f;
      stop  = -1.f;
      sum   = 0;
      for (i = startStopId[0] ; i < startStopId[1]; i++)
        {
          if (start < 0.f && dd->histo[i] > 0)
            {
              start = dd->stepValue * i + dd->initValue;
              sum = dd->histo[i];
              iStart = i;
              _max = dd->histo[i];
              _posMax = i;
            }
          else if (start > 0.f)
            {
              if (dd->histo[i] == 0)
                {
                  if (sum >= min)
                    {
                      stop = dd->stepValue * i + dd->initValue;
                      iStop = i;
                      break;
                    }
                  else
                    start = -1.f;
                }
              else
                {
                  sum += dd->histo[i];
                  if (dd->histo[i] > _max)
                    {
                      _max = dd->histo[i];
                      _posMax = i;
                    }
                }
            }
        }
      DBG_fprintf(stderr, "Visu Pairs: found one pick at %d-%d (%d).\n",
                  iStart, iStop, sum);
    }
  while (start < 0.f && min > 0.1f * MIN(dd->nNodesEle1, dd->nNodesEle2));
  DBG_fprintf(stderr, "Visu Pairs: set start and stop at %f, %f (%f %d).\n",
              start, stop, min, sum);
  if (start <= 0.f || stop <= 0.f)
    return FALSE;

  startStopId[0] = iStart;
  startStopId[1] = iStop;
  if (integral)
    *integral = sum;
  if (max)
    *max = _max;
  if (posMax)
    *posMax = _posMax;

  return TRUE;
}


static void foreachLevel2(gpointer key _U_, gpointer value, gpointer userData)
{
  VisuPair *pair;
  GList *tmpLst;
  struct foreachPairsData_struct *storage;

  pair = (VisuPair*)value;
  storage = (struct foreachPairsData_struct *)userData;
  for (tmpLst = pair->pairs; tmpLst; tmpLst = g_list_next(tmpLst))
    storage->func(pair->ele1, pair->ele2,
		  (VisuPairLink*)tmpLst->data, storage->userData);
}
/**
 * visu_pair_foreach:
 * @whatToDo: (scope call): a VisuPairForeachFunc() method ;
 * @user_data: some user defined data.
 *
 * The way #VisuPairLink are stored in V_Sim is private and could changed between version.
 * This method is used to apply some method each pairs.
 */
void visu_pair_foreach(VisuPairForeachFunc whatToDo, gpointer user_data)
{
  struct foreachPairsData_struct storage;

  if (!DminDmax)
    visu_pair_init();

  storage.func = whatToDo;
  storage.userData = user_data;
  g_hash_table_foreach(DminDmax, (GHFunc)foreachLevel2, (gpointer)(&storage));
}
