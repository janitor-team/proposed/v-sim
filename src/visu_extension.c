/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "visu_extension.h"
#include "visu_configFile.h"
#include "coreTools/toolConfigFile.h"
#include "openGLFunctions/objectList.h"

#include <stdlib.h>
#include <string.h>
#include <GL/gl.h>

#include "visu_tools.h"

/**
 * SECTION:visu_extension
 * @short_description: All objects drawn by V_Sim are defined in by a
 * #VisuGlExt object
 *
 * <para>All objects that are drawn by V_Sim are handled by a
 * #VisuGlExt object. Such an object has an OpenGL list. This
 * list is only COMPILED. When V_Sim receives the 'OpenGLAskForReDraw'
 * or the 'OpenGLForceReDraw' signals, each list of all known
 * #VisuGlExt are excecuted. This excecution can be canceled if
 * the used flag of the #VisuGlExt object is set to FALSE. The
 * order in which the lists are called depends on the priority of the
 * #VisuGlExt object. This priority is set to
 * #VISU_GL_EXT_PRIORITY_NORMAL as default value, but it can be
 * tune by a call to visu_gl_ext_setPriority(). This priority is
 * an integer, the lower it is, the sooner the list is
 * excecuted.</para>
 *
 * <para>The method registerVisuGlExt() is used to declare to
 * V_Sim that there is a new #VisuGlExt object available. This
 * allows to create extension when V_Sim is already
 * running. Nevertheless, an extension must be initialized in the
 * initialisation process, it is better to add an
 * #initVisuGlExtFunc method in the listInitExtensionFunc array
 * declared in extensions/externalVisuGlExts.h.</para>
 *
 * <para>Once again, the OpenGL list corresponding to an OpenGL
 * extension is COMPILE only. Then, OpenGL methods like glIsenabled()
 * are totally unusefull because it is called when the list is
 * compiled not when the list is called. If the extension needs to
 * alter some OpenGL state, such as desable GL_LIGHTING, it needs to
 * set a flag for the extension. With this flag, V_Sim will save the
 * OpenGL states and restore it when the list is called. Use
 * visu_gl_ext_setSaveState() to set this flag.</para>
 */

#define FLAG_PARAMETER_MODE "extension_render"
#define DESC_PARAMETER_MODE "Rules the way OpenGl draws extensions (see opengl_render); name (string) value (string)"
static gboolean readExtensionRendering(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				       VisuData *dataObj, VisuGlView *view,
                                       GError **error);
static void exportParametersRendering(GString *data, VisuData *dataObj, VisuGlView *view);

/* Method used to compare the priority of two extensions. */
static gint compareExtensionPriority(gconstpointer a, gconstpointer b);

static void callList(GList *lst, VisuGlRenderingMode *renderingMode,
		     VisuGlRenderingMode globalRenderingMode);

enum
  {
    PROP_0,
    NAME_PROP,
    ACTIVE_PROP,
    LABEL_PROP,
    DESCRIPTION_PROP,
    PRIORITY_PROP,
    SAVE_STATE_PROP,
    NGLOBJ_PROP
  };

struct _VisuGlExtClassPrivate
{
  /* This flag is TRUE when some priorities are new or have changed, and 
     the list should be reordered. */
  gboolean reorderingNeeded;

  /* A GList to store all the available OpenGL extensions
     in the system. */
  GList *allExtensions;
};

struct _VisuGlExtPrivate
{
  gboolean dispose_has_run;

  /* Some variable to describe this OpenGL extension.
     The attribute name is mandatory since it is
     used to identify the method. */
  gchar *name, *nameI18n;
  gchar *description;

  /* The id of the possible objects list brings by
     the extension is refered by this int. */
  guint nGlObj;
  int objectListId;

  /* A priority for the extension. */
  guint priority;

  /* If set, V_Sim save the OpenGL state before the list
     id is called and restore all states after. */
  gboolean saveState;

  /* Fine tune of rendering mode (VISU_GL_RENDERING_WIREFRAME, smooth...).
     The flag isSensitiveToRenderingMode define is the
     extension cares about rendering mode. This flag is FALSE
     by default. When FALSE, the global value for rendering
     mode is used. Otherwise the value is stored in
     preferedRenderingMode. */
  gboolean isSensitiveToRenderingMode;
  VisuGlRenderingMode preferedRenderingMode;

  /* A boolean to know if this extension is actually used
     or not. */
  gboolean used;
};

static VisuGlExtClass *my_class = NULL;

static void visu_gl_ext_dispose     (GObject* obj);
static void visu_gl_ext_finalize    (GObject* obj);
static void visu_gl_ext_get_property(GObject* obj, guint property_id,
                                     GValue *value, GParamSpec *pspec);
static void visu_gl_ext_set_property(GObject* obj, guint property_id,
                                     const GValue *value, GParamSpec *pspec);

G_DEFINE_TYPE(VisuGlExt, visu_gl_ext, G_TYPE_OBJECT)

static void visu_gl_ext_class_init(VisuGlExtClass *klass)
{
  VisuConfigFileEntry *confEntry;

  DBG_fprintf(stderr, "Visu Extension: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_finalize;
  G_OBJECT_CLASS(klass)->set_property = visu_gl_ext_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_gl_ext_get_property;
  /* Default methods. */
  klass->rebuild = NULL;

  /**
   * VisuGlExt::name:
   *
   * The name of the extension (used as an id).
   *
   * Since: 3.7
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), NAME_PROP,
     g_param_spec_string("name", "Name", "name (id) of extension", "",
                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE |
                         G_PARAM_STATIC_STRINGS));
  /**
   * VisuGlExt::active:
   *
   * The extension is used or not.
   *
   * Since: 3.7
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), ACTIVE_PROP,
     g_param_spec_boolean("active", "Active", "extension is used or not", FALSE,
                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  /**
   * VisuGlExt::label:
   *
   * The label of extension (translated).
   *
   * Since: 3.7
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), LABEL_PROP,
     g_param_spec_string("label", "Label", "label (translated) of extension", "",
                         G_PARAM_CONSTRUCT | G_PARAM_READWRITE |
                         G_PARAM_STATIC_STRINGS));
  /**
   * VisuGlExt::description:
   *
   * The description of the extension.
   *
   * Since: 3.7
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), DESCRIPTION_PROP,
     g_param_spec_string("description", "Description", "description of extension", "",
                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  /**
   * VisuGlExt::priority:
   *
   * The drawing priority of the extension.
   *
   * Since: 3.7
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), PRIORITY_PROP,
     g_param_spec_uint("priority", "Priority", "drawing priority of extension",
                       VISU_GL_EXT_PRIORITY_BACKGROUND,
                       VISU_GL_EXT_PRIORITY_LAST,
                       VISU_GL_EXT_PRIORITY_NORMAL,
                       G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  /**
   * VisuGlExt::saveState:
   *
   * When set, save the OpenGL state, so the extension can modify
   * OpenGL parameters like light, depth test...
   *
   * Since: 3.7
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), SAVE_STATE_PROP,
     g_param_spec_boolean("saveState", "Save state", "save OpenGL state", FALSE,
                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  /**
   * VisuGlExt::nGlObj:
   *
   * The number of GL list the extension is dealing with.
   *
   * Since: 3.7
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), NGLOBJ_PROP,
     g_param_spec_uint("nGlObj", "N GL objects", "number of GL lists dealt with",
                       1, 2048, 1,
                       G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE));
  
  klass->priv = g_malloc(sizeof(VisuGlExtClassPrivate));
  klass->priv->reorderingNeeded = FALSE;
  klass->priv->allExtensions = (GList*)0;

  confEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
                                       FLAG_PARAMETER_MODE,
                                       DESC_PARAMETER_MODE,
                                       1, readExtensionRendering);
  visu_config_file_entry_setVersion(confEntry, 3.4f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_PARAMETER,
                                     exportParametersRendering);

  g_type_class_add_private(klass, sizeof(VisuGlExtPrivate));

  my_class = klass;
}

static void visu_gl_ext_init(VisuGlExt *ext)
{
  VisuGlExtClass *klass;

  DBG_fprintf(stderr, "Visu Extension: initializing a new object (%p).\n",
	      (gpointer)ext);
  ext->priv = G_TYPE_INSTANCE_GET_PRIVATE(ext, VISU_TYPE_GL_EXT, VisuGlExtPrivate);
  ext->priv->dispose_has_run = FALSE;

  /* Set-up all not parameters attributes. */
  ext->priv->used = TRUE;
  ext->priv->objectListId = 0;
  ext->priv->isSensitiveToRenderingMode = FALSE;
  ext->priv->preferedRenderingMode = VISU_GL_RENDERING_FOLLOW;

  /* We add it to the known extension list. */
  klass = VISU_GL_EXT_GET_CLASS(ext);
  klass->priv->allExtensions = g_list_append(klass->priv->allExtensions, (gpointer)ext);
  klass->priv->reorderingNeeded = TRUE;
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_gl_ext_dispose(GObject* obj)
{
  VisuGlExt *ext;

  DBG_fprintf(stderr, "Visu Extension: dispose object %p.\n", (gpointer)obj);

  ext = VISU_GL_EXT(obj);
  if (ext->priv->dispose_has_run)
    return;
  ext->priv->dispose_has_run = TRUE;

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_gl_ext_finalize(GObject* obj)
{
  VisuGlExtClass *klass;
  VisuGlExtPrivate *ext;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Visu Extension: finalize object %p.\n", (gpointer)obj);

  ext = VISU_GL_EXT(obj)->priv;
  if (ext->name)
    g_free(ext->name);
  if (ext->nameI18n)
    g_free(ext->nameI18n);
  if (ext->description)
    g_free(ext->description);
  glDeleteLists(ext->objectListId, ext->nGlObj);

  klass = VISU_GL_EXT_GET_CLASS(obj);
  klass->priv->allExtensions = g_list_remove_all(klass->priv->allExtensions, (gpointer)obj);

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Visu Extension: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Visu Extension: freeing ... OK.\n");
}
static void visu_gl_ext_get_property(GObject* obj, guint property_id,
                                        GValue *value, GParamSpec *pspec)
{
  VisuGlExtPrivate *self = VISU_GL_EXT(obj)->priv;

  DBG_fprintf(stderr, "Visu Extension: get property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case NAME_PROP:
      g_value_set_string(value, self->name);
      DBG_fprintf(stderr, "%s.\n", self->name);
      break;
    case LABEL_PROP:
      g_value_set_string(value, self->nameI18n);
      DBG_fprintf(stderr, "%s.\n", self->nameI18n);
      break;
    case DESCRIPTION_PROP:
      g_value_set_string(value, self->description);
      DBG_fprintf(stderr, "%s.\n", self->description);
      break;
    case ACTIVE_PROP:
      g_value_set_boolean(value, self->used);
      DBG_fprintf(stderr, "%d.\n", self->used);
      break;
    case SAVE_STATE_PROP:
      g_value_set_boolean(value, self->saveState);
      DBG_fprintf(stderr, "%d.\n", self->saveState);
      break;
    case PRIORITY_PROP:
      g_value_set_uint(value, self->priority);
      DBG_fprintf(stderr, "%d.\n", self->priority);
      break;
    case NGLOBJ_PROP:
      g_value_set_uint(value, self->nGlObj);
      DBG_fprintf(stderr, "%d.\n", self->nGlObj);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_gl_ext_set_property(GObject* obj, guint property_id,
                                        const GValue *value, GParamSpec *pspec)
{
  VisuGlExtPrivate *self = VISU_GL_EXT(obj)->priv;

  DBG_fprintf(stderr, "Visu Extension: set property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case NAME_PROP:
      self->name = g_value_dup_string(value);
      DBG_fprintf(stderr, "%s.\n", self->name);
      break;
    case LABEL_PROP:
      self->nameI18n = g_value_dup_string(value);
      DBG_fprintf(stderr, "%s.\n", self->nameI18n);
      break;
    case DESCRIPTION_PROP:
      self->description = g_value_dup_string(value);
      DBG_fprintf(stderr, "%s.\n", self->description);
      break;
    case ACTIVE_PROP:
      self->used = g_value_get_boolean(value);
      DBG_fprintf(stderr, "%d.\n", self->used);
      break;
    case SAVE_STATE_PROP:
      self->saveState = g_value_get_boolean(value);
      DBG_fprintf(stderr, "%d.\n", self->saveState);
      break;
    case PRIORITY_PROP:
      self->priority = g_value_get_uint(value);
      my_class->priv->reorderingNeeded = TRUE;
      DBG_fprintf(stderr, "%d.\n", self->priority);
      break;
    case NGLOBJ_PROP:
      self->nGlObj = g_value_get_uint(value);
      DBG_fprintf(stderr, "%d.\n", self->nGlObj);
      self->objectListId = visu_gl_objectlist_new(self->nGlObj);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}

/**
 * visu_gl_ext_getActive:
 * @extension: the extension.
 *
 * Get if the extension is used or not. If not its ObjectList
 * is not rendered.
 *
 * Returns: TRUE if used, FALSE otherwise.
 */
gboolean visu_gl_ext_getActive(VisuGlExt* extension)
{
  if (extension)
    return extension->priv->used;
  else
    return FALSE;
}
/**
 * visu_gl_ext_setActive:
 * @extension: the extension,
 * @value: the new value.
 *
 * Set if an extension is actually used or not.
 */
gboolean visu_gl_ext_setActive(VisuGlExt* extension, gboolean value)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_TYPE(extension), FALSE);

  if (extension->priv->used == value)
    return FALSE;

  g_object_set(G_OBJECT(extension), "active", value, NULL);
  return TRUE;
}
/**
 * visu_gl_ext_setPriority:
 * @extension: a #VisuGlExt object ;
 * @priority: an integer value.
 *
 * Extentions are drawn in an order that depends on their priority.
 * The lower is the number, the sooner the extension is drawn. Flags,
 * such as #VISU_GL_EXT_PRIORITY_NORMAL or #VISU_GL_EXT_PRIORITY_LOW,
 * can be used or user defined values are also possible.
 */
void visu_gl_ext_setPriority(VisuGlExt* extension, guint priority)
{
  g_return_if_fail(VISU_IS_GL_EXT_TYPE(extension));

  g_object_set(G_OBJECT(extension), "priority", priority, NULL);
}

/**
 * visu_gl_ext_setSaveState:
 * @extension: a #VisuGlExt object ;
 * @saveState: an boolean value.
 *
 * If the extension needs to change some OpenGL state (to disable the fog for example,
 * or the cullface), a flag should be set to enable V_Sim to restore the right values after
 * the extensio have been called. Because the OpenGL list of an extension is just GL_COMPILE
 * the extension can't just save and restore state itself because when the list is called,
 * the state can have been changed.
 */
void visu_gl_ext_setSaveState(VisuGlExt *extension, gboolean saveState)
{
  g_return_if_fail(VISU_IS_GL_EXT_TYPE(extension));

  g_object_set(G_OBJECT(extension), "saveState", saveState, NULL);
}

/**
 * visu_gl_ext_setSensitiveToRenderingMode:
 * @extension: a #VisuGlExt object ;
 * @status: an boolean value.
 *
 * If @status is TRUE, when the extension is rendered, OpenGL context is
 * switched to the rendering mode preferd for the extension. Use
 * visu_gl_ext_setPreferedRenderingMode() to choose one.
 */
void visu_gl_ext_setSensitiveToRenderingMode(VisuGlExt* extension,
                                                gboolean status)
{
  g_return_if_fail(extension);
  extension->priv->isSensitiveToRenderingMode = status;
}
/**
 * visu_gl_ext_getSensitiveToRenderingMode:
 * @extension: a #VisuGlExt object.
 *
 * Each #VisuGlExt method can follow or not the global setting for
 * the rendering mode, see #VisuGlRenderingMode. See also
 * visu_gl_ext_setSensitiveToRenderingMode().
 *
 * Since: 3.7
 *
 * Returns: TRUE if @extension can change its rendering mode according
 * to global settings.
 */
gboolean visu_gl_ext_getSensitiveToRenderingMode(VisuGlExt* extension)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_TYPE(extension), FALSE);
  return extension->priv->isSensitiveToRenderingMode;
}
/**
 * visu_gl_ext_setPreferedRenderingMode:
 * @extension: a #VisuGlExt object ;
 * @value: see #VisuGlRenderingMode to choose one.
 *
 * This method is used to specify the rendering mode that the extension should use
 * to be drawn (if the sensitive flag has been set, see
 * visu_gl_ext_setSensitiveToRenderingMode()). If the @value is set
 * to VISU_GL_RENDERING_FOLLOW, the extension follows the global setting
 * for rendering mode.
 *
 * Returns: TRUE if the "OpenGLAskForReDraw" should be emitted.
 */
gboolean visu_gl_ext_setPreferedRenderingMode(VisuGlExt* extension,
                                                 VisuGlRenderingMode value)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_TYPE(extension), FALSE);
  g_return_val_if_fail(value < VISU_GL_RENDERING_N_MODES ||
		       value == VISU_GL_RENDERING_FOLLOW, FALSE);

  if (extension->priv->preferedRenderingMode == value)
    return FALSE;

  extension->priv->preferedRenderingMode = value;

  return TRUE;
}
/**
 * visu_gl_ext_getPreferedRenderingMode:
 * @extension: a #VisuGlExt method.
 *
 * Each #VisuGlExt method can draw in a mode different from the
 * global one, see #VisuGlRenderingMode. See also
 * visu_gl_ext_setPreferedRenderingMode().
 *
 * Since: 3.7
 *
 * Returns: the prefered rendering mode of this @extension.
 **/
VisuGlRenderingMode visu_gl_ext_getPreferedRenderingMode(VisuGlExt* extension)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_TYPE(extension), VISU_GL_RENDERING_FOLLOW);
  return extension->priv->preferedRenderingMode;
}
/**
 * visu_gl_ext_getGlList:
 * @extension: a #VisuGlExt method.
 *
 * All #VisuGlExt objects have a master OpenGL list to draw
 * to. This routine gets the identifier of this list.
 *
 * Since: 3.7
 *
 * Returns: an OpenGL identifier id for @extension.
 **/
guint visu_gl_ext_getGlList(VisuGlExt *extension)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_TYPE(extension), 0);
  return extension->priv->objectListId;
}

/********************/
/* Class functions. */
/********************/

/**
 * visu_gl_ext_getFromName:
 * @name: a string identifying a #VisuGlExt object.
 *
 * #VisuGlExt objects are identified by an id.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuGlExt object matching @name.
 **/
VisuGlExt* visu_gl_ext_getFromName(const gchar* name)
{
  GList *pnt;
  VisuGlExt *ext;

  DBG_fprintf(stderr, "Visu Extension: get '%s' from list.\n", name);

  if (!my_class)
    g_type_class_ref(VISU_TYPE_GL_EXT);

  for (pnt = my_class->priv->allExtensions; pnt; pnt = g_list_next(pnt))
    {
      ext = (VisuGlExt*)pnt->data;
      if (!strcmp(ext->priv->name, name))
	return ext;
    }
  return (VisuGlExt*)0;
}

static void mayReorder()
{
  if (!my_class)
    g_type_class_ref(VISU_TYPE_GL_EXT);

  if (my_class->priv->reorderingNeeded)
    {
      DBG_fprintf(stderr, "Visu Extension: sorting known extension"
                  " depending on their priority.\n");
      my_class->priv->allExtensions = g_list_sort(my_class->priv->allExtensions,
                                                  compareExtensionPriority);
      my_class->priv->reorderingNeeded = FALSE;
    }
}

/**
 * visu_gl_ext_call:
 * @extension: a #VisuGlExt object.
 * @lastOnly: a boolean.
 *
 * Select the #VisuGlExt matching the given @name and call
 * it. The call is indeed done only if the extension is used. If
 * @lastOnly is TRUE, the list is called only if it has a
 * #VISU_GL_EXT_PRIORITY_LAST priority. On the contrary the list
 * is called only if its priority is lower than
 * #VISU_GL_EXT_PRIORITY_LAST.
 */
void visu_gl_ext_call(VisuGlExt *ext, gboolean lastOnly)
{
  VisuGlRenderingMode renderingMode, globalRenderingMode;
  GList lst;

  g_return_if_fail(VISU_IS_GL_EXT_TYPE(ext));
  /* DBG_fprintf(stderr, "Visu Extension: call '%s' list.\n", ext->priv->name); */
  /* DBG_fprintf(stderr, "|  has %d ref counts.\n", G_OBJECT(ext)->ref_count); */

  globalRenderingMode = visu_gl_rendering_getGlobalMode();
  renderingMode = globalRenderingMode;
  if (ext->priv->used &&
      ((lastOnly && ext->priv->priority == VISU_GL_EXT_PRIORITY_LAST) ||
       (!lastOnly && ext->priv->priority < VISU_GL_EXT_PRIORITY_LAST)) &&
      ext->priv->objectListId > 1000)
    {
      lst.data = (gpointer)ext;
      lst.next = lst.prev = (GList*)0;
      callList(&lst, &renderingMode, globalRenderingMode);
      if (renderingMode != globalRenderingMode)
	/* Return the rendering mode to normal. */
	visu_gl_rendering_applyMode(globalRenderingMode);
    }
}
/**
 * visu_gl_ext_rebuildAll:
 *
 * For each registered extension that has a valid rebuild method,
 * it calls it.
 */
void visu_gl_ext_rebuildAll()
{
  GList *pnt;

  mayReorder();

  DBG_fprintf(stderr, "Visu Extension: Rebuilding all lists...\n");
  for (pnt = my_class->priv->allExtensions; pnt; pnt = g_list_next(pnt))
    visu_gl_ext_rebuild(VISU_GL_EXT(pnt->data));
}
/**
 * visu_gl_ext_rebuild:
 * @self: a #VisuGlExt object.
 *
 * This routine does not sort the extension on their priority and
 * should be used only to draw some selected extensions. To draw all
 * of them, use visu_gl_ext_rebuildAll() instead.
 */
void visu_gl_ext_rebuild(VisuGlExt *self)
{
  g_return_if_fail(VISU_IS_GL_EXT_TYPE(self));

  DBG_fprintf(stderr, "Visu Extension: rebuilding '%s' list.\n", self->priv->name);

  if (self->priv->used && VISU_GL_EXT_GET_CLASS(self)->rebuild)
    VISU_GL_EXT_GET_CLASS(self)->rebuild(self);
}
/**
 * visu_gl_ext_getAll:
 *
 * This method is used to get the list of all registered VisuGlExt.
 * This list is own by V_Sim and should not be freed.
 *
 * Returns: (element-type VisuGlExt*) (transfer none): the list of
 * all #VisuGlExt.
 */
GList* visu_gl_ext_getAll(void)
{
  mayReorder();

  return my_class->priv->allExtensions;
}


/****************/
/* Private area */
/****************/
static void callList(GList *lst, VisuGlRenderingMode *renderingMode,
		     VisuGlRenderingMode globalRenderingMode)
{
  VisuGlExt *ext;
  GTimer *timer;
  gulong fractionTimer;

#if DEBUG == 1
  timer = g_timer_new();
  g_timer_start(timer);
#endif

  for (; lst; lst = g_list_next(lst))
    {
      ext = (VisuGlExt*)lst->data;
      /* The extension needs its own rendering mode. */
      if (ext->priv->isSensitiveToRenderingMode &&
	  ext->priv->preferedRenderingMode < VISU_GL_RENDERING_N_MODES)
	{
	  if (ext->priv->preferedRenderingMode != *renderingMode)
	    {
	      visu_gl_rendering_applyMode(ext->priv->preferedRenderingMode);
	      *renderingMode = ext->priv->preferedRenderingMode;
	    }
	}
      else
	{
	  if (*renderingMode != globalRenderingMode)
	    {
	      visu_gl_rendering_applyMode(globalRenderingMode);
	      *renderingMode = globalRenderingMode;
	    }
	}
      /* Save OpenGL state if necessary. */
      if (ext->priv->saveState)
	{
	  DBG_fprintf(stderr, "Visu Extension: save state.\n");
	  glPushAttrib(GL_ENABLE_BIT);
	}

      if (ext->priv->isSensitiveToRenderingMode &&
          *renderingMode == VISU_GL_RENDERING_SMOOTH_AND_EDGE)
        {
          glPushAttrib(GL_ENABLE_BIT);
          glEnable(GL_POLYGON_OFFSET_FILL);
          glPolygonOffset(1.0, 1.0);
        }

      /* Call the compiled list. */
      DBG_fprintf(stderr, "Visu Extension: call list %d (%s)",
                  ext->priv->objectListId, ext->priv->name);
      glCallList(ext->priv->objectListId);
      DBG_fprintf(stderr, " at %g micro-s",
                  g_timer_elapsed(timer, &fractionTimer)*1e6);
      DBG_fprintf(stderr, ".\n");

      /* Add a wireframe draw if renderingMode is VISU_GL_RENDERING_SMOOTH_AND_EDGE. */
      if (ext->priv->isSensitiveToRenderingMode &&
          *renderingMode == VISU_GL_RENDERING_SMOOTH_AND_EDGE)
        {
          glDisable(GL_POLYGON_OFFSET_FILL);
          glDisable(GL_LIGHTING);
          glColor3f (0.0, 0.0, 0.0);
          glLineWidth(1);
          glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
          glCallList(ext->priv->objectListId);
          glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
          glPopAttrib();
        }

      if (ext->priv->saveState)
	{
	  DBG_fprintf(stderr, "Visu Extension: restore state.\n");
	  glPopAttrib();
	}
    }

#if DEBUG == 1
  g_timer_stop(timer);
  g_timer_destroy(timer);
#endif
}

static gint compareExtensionPriority(gconstpointer a, gconstpointer b)
{
  if (((VisuGlExt*)a)->priv->priority < ((VisuGlExt*)b)->priv->priority)
    return (gint)-1;
  else if (((VisuGlExt*)a)->priv->priority > ((VisuGlExt*)b)->priv->priority)
    return (gint)+1;
  else
    return (gint)0;
}

static gboolean readExtensionRendering(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				       VisuData *dataObj _U_, VisuGlView *view _U_,
                                       GError **error)
{
  gchar **val;
  VisuGlExt *ext;
  VisuGlRenderingMode id;
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readString(lines[0], position, &val, 2, FALSE, error))
    return FALSE;
  ext = visu_gl_ext_getFromName(val[0]);
  if (!ext)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: the extension"
			     " '%s' is unknown.\n"), position, val[0]);
      g_strfreev(val);
      return FALSE;
    }
  if (!visu_gl_rendering_getModeFromName(val[1], &id))
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: the rendering mode"
			     " '%s' is unknown.\n"), position, val[1]);
      g_strfreev(val);
      return FALSE;
    }
  g_strfreev(val);
  visu_gl_ext_setPreferedRenderingMode(ext, id);

  return TRUE;
}
static void exportParametersRendering(GString *data,
                                      VisuData *dataObj _U_, VisuGlView *view _U_)
{
  GList *tmp;
  VisuGlExt *ext;
  const char **names;

  if (!my_class)
    g_type_class_ref(VISU_TYPE_GL_EXT);

  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_MODE);

  names = visu_gl_rendering_getAllModes();
  for (tmp = my_class->priv->allExtensions; tmp; tmp = g_list_next(tmp))
    {
      ext = (VisuGlExt*)tmp->data;
      if (ext->priv->isSensitiveToRenderingMode &&
	  ext->priv->preferedRenderingMode < VISU_GL_RENDERING_N_MODES)
	g_string_append_printf(data, "%s: %s %s\n", FLAG_PARAMETER_MODE,
			       ext->priv->name, names[ext->priv->preferedRenderingMode]);
    }
  g_string_append_printf(data, "\n");
}
