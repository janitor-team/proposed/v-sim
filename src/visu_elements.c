/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "visu_elements.h"
#include "visu_object.h"
#include "visu_configFile.h"

#include <stdlib.h>
#include <string.h>

#include "openGLFunctions/objectList.h"
#include "opengl.h"
#include "coreTools/toolColor.h"
#include "coreTools/toolConfigFile.h"

#include <GL/gl.h>
#include <GL/glu.h>

/**
 * SECTION:visu_elements
 * @short_description: defines methods to create and acccess to
 * #VisuElement.
 *
 * <para>V_Sim is used to rendered at given position several object of
 * the same kind. The #VisuElement object is used to control that
 * kind. Typically, it corresponds to chemical element. It can
 * represent the silicon, the iron...</para>
 *
 * <para>#VisuElement are defined by their name and have some
 * characteristic like their color or if they are rendered or not. The
 * color characteristic is defined by an RGBA array and different
 * value for the behavior of the light, as defined in OpenGL :
 * diffusivity, shiningness, emissivity, specular and ambient. These
 * values can be controlled with the following methods :
 * visu_element_setAllColorValues(), visu_element_setAllRGBValues(),
 * visu_element_setRGBValue(), visu_element_setAllMaterialValues() and
 * visu_element_setMaterialValue().</para>
 *
 * <para>If the OpenGL representation of one element is not dependent
 * of its position, it is recommended to use the OpenGL list
 * associated to each #VisuElement that can be accessed by a call to
 * visu_element_getMaterialId().</para>
 */

/**
 * VisuElement:
 *
 * Structure to stores #VisuElement objects.
 */

/**
 * _VisuElement:
 * @parent: the parent.
 * @name: Name of the key used in the hashtable to find
 *   this element. The int variable is the number
 *   of this type.
 * @typeNumber: An integer unique for each VisuElement, it is
 *   used as a name for the opengl material associated
 *   with it.
 * @rgb: main color of the element  in [r, g, b, a] format.
 * @material: lighting effects for material in [amb, dif, shi, spe,
 * emi] format.
 * @glMaterialId: the identifier for the material of this element.
 * @materialIsUpToDate: A flag that is TRUE when the OpenGL list of
 *   the  material is up to date.
 * @rendered: A flag to store if all nodes of this element are rendered or not.
 *   Its default value is TRUE.
 * @sensitiveToPlanes: a flag to say if nodes of this element are sensitive
 *   to the masking effect of planes (default is TRUE).
 * @physical: TRUE if the element is a physical one.
 * @dispose_has_run: internal.
 *
 * Structure to store the description of an element.
 */

enum {
  ELEMENT_NEW_SIGNAL,
  ELEMENT_VISIBILITY_CHANGED_SIGNAL,
  ELEMENT_MATERIAL_CHANGED_SIGNAL,
  ELEMENT_RENDERING_CHANGED_SIGNAL,
  ELEMENT_VISU_PLANE_CHANGED_SIGNAL,
  LAST_SIGNAL
};

struct _VisuElementClass
{
  GObjectClass parent;

  /* This hashtable contains a list of all different
     elements loaded in visu. */
  GHashTable *allElements_table;
  GList *allElements_list;

  /* This int stores how many user functions have needs
     to recreate the nodes of an element when its material
     has been changed. */
  int flagCreateNodesAfterMaterialChange;
};

#define FLAG_ELEMENT_COLOR      "element_color"
#define DESC_ELEMENT_COLOR      "Codes the main color in RedGreenBlueAlpha format" \
  "and the light effects on material, nine floats between 0. and 1."
#define FLAG_ELEMENT_PROPERTIES "element_properties"
#define DESC_ELEMENT_PROPERTIES "Define some properties ; rendered (0 or 1) masked" \
  "(0 or 1)."

/* These functions detail how to read the RGB resource and
   the material resource introduced by this part.
   These routines are obsolete but kept for backward compatibility. */
static gboolean readMaterial(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			     VisuData *dataObj, VisuGlView *view, GError **error);
static gboolean readRendered(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			     VisuData *dataObj, VisuGlView *view, GError **error);

/* Read routines for the config file. */
static gboolean readElementColor(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				 VisuData *dataObj, VisuGlView *view, GError **error);
static gboolean readElementProperties(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				      VisuData *dataObj, VisuGlView *view,
                                      GError **error);


/* These functions write all the element list to export there associated resources. */
static void exportResourcesRenderingBase(GString *data,
                                         VisuData *dataObj, VisuGlView *view);
static void visu_element_dispose(GObject* obj);
static void visu_element_finalize(GObject* obj);

static VisuElementClass *my_class = (VisuElementClass*)0;
static guint visu_element_signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE(VisuElement, visu_element, G_TYPE_OBJECT)

static void visu_element_class_init(VisuElementClass *klass)
{
  VisuConfigFileEntry *resourceEntry, *oldEntry;

  DBG_fprintf(stderr, "Visu Element: creating the class of the object.\n");
  DBG_fprintf(stderr, "                - adding new signals ;\n");
  /**
   * VisuElement::ElementVisibilityChanged:
   * @element: the object which emit the signal ;
   *
   * Gets emitted when one of the rendered #VisuElement is made
   * visible or hidden.
   *
   * Since: 3.6
   */
  visu_element_signals[ELEMENT_VISIBILITY_CHANGED_SIGNAL] =
    g_signal_new("ElementVisibilityChanged",
                 G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE,
                 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);

  /**
   * VisuElement::ElementMaterialChanged:
   * @element: the object which emit the signal ;
   *
   * Gets emitted when the material or color properties of a VisuElement
   * are modified.
   *
   * Since: 3.6
   */
  visu_element_signals[ELEMENT_MATERIAL_CHANGED_SIGNAL] =
    g_signal_new("ElementMaterialChanged",
                 G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE,
                 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);

  /**
   * VisuElement::ElementRenderingChanged:
   * @element: the object which emit the signal ;
   *
   * Gets emitted when the rendering characteristics of an element are
   * changed, like their shape or specific colour. For a signal on
   * generic colours (like the element colour), listen to
   * VisuElement::ElementMaterialChanged signal instead.
   *
   * Since: 3.7
   */
  visu_element_signals[ELEMENT_RENDERING_CHANGED_SIGNAL] =
    g_signal_new("ElementRenderingChanged",
                 G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE,
                 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);

  /**
   * VisuElement::ElementPlaneChanged:
   * @element: the object which emit the signal ;
   *
   * Gets emitted when the property of masking by plane is modified.
   *
   * Since: 3.6
   */
  visu_element_signals[ELEMENT_VISU_PLANE_CHANGED_SIGNAL] =
    g_signal_new("ElementPlaneChanged",
                 G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE,
                 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);

  /**
   * VisuElement::ElementNew:
   * @element: the object emitting the signal.
   *
   * A new element is available.
   *
   * Since: 3.6
   */
  visu_element_signals[ELEMENT_NEW_SIGNAL] = 
    g_signal_new("ElementNew",
                 G_TYPE_FROM_CLASS (klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE,
                 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);


  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_element_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_element_finalize;

  /* Set internal parameters. */
  klass->allElements_table =
    g_hash_table_new_full(g_str_hash, g_str_equal,
			  NULL, (GDestroyNotify)g_object_unref);
  klass->allElements_list = (GList*)0;

  /* Create a VisuModule to registered the new resources as
     rgb and material. */
  oldEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
				     "material",
				     "Obsolete entry for element_color",
				     1, readMaterial);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_ELEMENT_COLOR,
					  DESC_ELEMENT_COLOR,
					  1, readElementColor);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  visu_config_file_entry_setReplace(resourceEntry, oldEntry);
  oldEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
				     "element_is_rendered",
				     "Obsolete entry included in element_properties",
				     1, readRendered);
  visu_config_file_entry_setVersion(resourceEntry, 3.1f);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_ELEMENT_PROPERTIES,
					  DESC_ELEMENT_PROPERTIES,
					  1, readElementProperties);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  visu_config_file_entry_setReplace(resourceEntry, oldEntry);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
				   exportResourcesRenderingBase);


  /* Get an OpenGL identifier to store all the materials. */
  klass->flagCreateNodesAfterMaterialChange = 0;

  my_class = klass;
}

static void visu_element_init(VisuElement *ele)
{
  int i;

  DBG_fprintf(stderr, "Visu Element: initializing a new object (%p).\n",
	      (gpointer)ele);
  ele->materialIsUpToDate       = FALSE;
  ele->rendered                 = TRUE;
  ele->sensitiveToPlanes = TRUE;
  ele->physical                 = TRUE;

  ele->glMaterialId = visu_gl_objectlist_new(1);

  for (i = 0; i < 4; i++)
    ele->rgb[i] = 1.;
  for (i = 0; i < 5; i++)
    ele->material[i] = 0.25;
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_element_dispose(GObject* obj)
{
  DBG_fprintf(stderr, "Visu Element: dispose object %p.\n", (gpointer)obj);

  if (VISU_ELEMENT(obj)->dispose_has_run)
    return;
  VISU_ELEMENT(obj)->dispose_has_run = TRUE;

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_element_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_element_finalize(GObject* obj)
{
  VisuElement *ele;
  VisuElementClass *klass;

  g_return_if_fail(obj);
  DBG_fprintf(stderr, "Visu Element: finalize object %p.\n", (gpointer)obj);

  ele = VISU_ELEMENT(obj);
  klass = VISU_ELEMENT_GET_CLASS(ele);
  g_free(ele->name);
  g_hash_table_steal(klass->allElements_table, ele);
  klass->allElements_list = g_list_remove(klass->allElements_list, ele);
  glDeleteLists(ele->glMaterialId, 1);

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Visu Element: chain to parent.\n");
  G_OBJECT_CLASS(visu_element_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Visu Element: freeing ... OK.\n");
}

/**
 * visu_element_new:
 * @key: the name of the new element to create.
 *
 * Allocate a new visuElement with the specified name. Remember
 * that names must be unique since they identify the element.
 *
 * Returns: (transfer none): the newly created VisuElement or 0 if something goes
 * wrong in the process (if the name already exist for example).
 */
VisuElement *visu_element_new(const char *key)
{
  VisuElement *ele;

  if (!my_class)
    g_type_class_ref(VISU_TYPE_ELEMENT);

  ele = visu_element_lookup(key);
  if (ele)
    {
      g_warning("Element '%s' already exists.", key);
      return ele;
    }

  ele = VISU_ELEMENT(g_object_new(VISU_TYPE_ELEMENT, NULL));
  ele->name       = g_strdup((key[0] == '%')?key + 1:key);
  ele->physical   = (key[0] != '%') && strcmp(key, "g") && strcmp(key, "G");
  g_hash_table_insert(my_class->allElements_table,
                      (gpointer)ele->name, (gpointer)ele);
  my_class->allElements_list = g_list_append(my_class->allElements_list, (gpointer)ele);

  DBG_fprintf(stderr, "Visu Elements: create a new VisuElement '%s' -> %p.\n",
	      key, (gpointer)ele);  
  g_signal_emit(G_OBJECT(ele), visu_element_signals[ELEMENT_NEW_SIGNAL], 0, NULL);
  DBG_fprintf(stderr, "Visu Elements: new element signal OK.\n");

  return ele;
}

/**
 * visu_element_getAllElements:
 *
 * This method returns a list of all the registered #VisuElement.
 * The returned list is read-only.
 *
 * Returns: (element-type VisuElement) (transfer none): the list of
 * all known #VisuElement.
 */
const GList *visu_element_getAllElements(void)
{
  if (!my_class)
    g_type_class_ref(VISU_TYPE_ELEMENT);

  return my_class->allElements_list;
}

/**
 * visu_element_retrieveFromName:
 * @name: a string that identify the #VisuElement (in UTF8) ;
 * @nw: (out caller-allocates): a location to store a boolean.
 *
 * Try to find a #VisuElement already associated to that @name or
 * create a new one if none has been found. If @nw is not NULL it is
 * set to FALSE if @name was found.
 *
 * Returns: (transfer none): a #VisuElement associated to this @name.
 */
VisuElement *visu_element_retrieveFromName(const gchar *name, gboolean *nw)
{
  VisuElement *ele;

  if (!my_class)
    g_type_class_ref(VISU_TYPE_ELEMENT);

  DBG_fprintf(stderr, "Visu Element: retrieve '%s' (%d).\n", name,
              g_hash_table_size(my_class->allElements_table));

  if (nw)
    *nw = FALSE;

  ele = g_hash_table_lookup(my_class->allElements_table,
                            (name[0] == '%')?name + 1:name);
  if (ele)
    return ele;

  if (nw)
    *nw = TRUE;

  return visu_element_new(name);
}

/**
 * visu_element_lookup:
 * @name: a string.
 *
 * Lookup for element @name in the base. Do not create it if not
 * found. To do this, use visu_element_retrieveFromName().
 *
 * Since: 3.6
 *
 * Returns: (transfer none): the found #VisuElement or NULL.
 */
VisuElement *visu_element_lookup(const gchar *name)
{
  if (!my_class)
    g_type_class_ref(VISU_TYPE_ELEMENT);

  DBG_fprintf(stderr, "Visu Element: lookup '%s' (%d).\n",
              name, g_hash_table_size(my_class->allElements_table));
  return g_hash_table_lookup(my_class->allElements_table,
                             (name[0] == '%')?name + 1:name);
}

/**
 * visu_element_getName:
 * @ele: a #VisuElement object.
 *
 * This routines returns the name of the given @ele.
 *
 * Since: 3.7
 *
 * Returns: a string owned by V_Sim.
 */
const gchar* visu_element_getName(const VisuElement *ele)
{
  g_return_val_if_fail(VISU_IS_ELEMENT_TYPE(ele), (const gchar*)0);

  return ele->name;
}
/**
 * visu_element_getPhysical:
 * @ele: a #VisuElement object.
 *
 * This routine gets if @ele is physical or not. A not physical
 * element can be used for instance to represent specific points...
 *
 * Since: 3.7
 *
 * Returns: TRUE if @ele is indeed physical.
 */
gboolean visu_element_getPhysical(VisuElement *ele)
{
  g_return_val_if_fail(VISU_IS_ELEMENT_TYPE(ele), FALSE);

  return ele->physical;
}
/**
 * visu_element_setAllColorValues:
 * @ele: the element of which the color must be changed ;
 * @rgb:  (in) (array fixed-size=4): the new color given by a {red, green, blue} array ;
 * @material:  (in) (array fixed-size=5): the new values to define the lighting.
 *
 * This method is used to set all the values that define the color
 * and the lighting of the given element.
 *
 * Returns: > 0 if values for @ele have changed.
 */
gint visu_element_setAllColorValues(VisuElement* ele, float rgb[4], float material[5])
{
  int chgt;

  chgt = (ele->rgb[0] != rgb[0]) || (ele->rgb[1] != rgb[1]) ||
    (ele->rgb[2] != rgb[2]) || (ele->rgb[3] != rgb[3]);
  ele->rgb[0] = rgb[0];
  ele->rgb[1] = rgb[1];
  ele->rgb[2] = rgb[2];
  ele->rgb[3] = rgb[3];
  chgt = chgt || (ele->material[0] != material[0]) || (ele->material[1] != material[1]) ||
    (ele->material[2] != material[2]) || (ele->material[3] != material[3]) ||
    (ele->material[4] != material[4]);
  ele->material[0] = material[0];
  ele->material[1] = material[1];
  ele->material[2] = material[2];
  ele->material[3] = material[3];
  ele->material[4] = material[4];

  if (chgt)
    {
      visu_element_createMaterial(ele);
      DBG_fprintf(stderr, "Visu Element: %p(%s) emit the 'ElementMaterialChanged'.\n",
                  (gpointer)ele, ele->name);
      g_signal_emit(ele, visu_element_signals[ELEMENT_MATERIAL_CHANGED_SIGNAL],
                    0, NULL);
    }

  if (chgt && ele->rendered)
    return 2 * VISU_ELEMENT_GET_CLASS(ele)->flagCreateNodesAfterMaterialChange - 1;
  else
    return 0;
}

/**
 * visu_element_setAllRGBValues:
 * @ele: the element of which the color must be changed,
 * @rgb:  (in) (array fixed-size=4): the new color given by a {red, green, blue} array.
 *
 * It saves the values of rgb in the specified VisuElement.
 *
 * Returns: > 0 if values for @ele have changed.
 */
gint visu_element_setAllRGBValues(VisuElement* ele, float rgb[4])
{
  g_return_val_if_fail(ele, 0);

  DBG_fprintf(stderr, "Visu Element: '%s' set color (%f, %f, %f, %f)\n",
	      ele->name, ele->rgb[0], ele->rgb[1], ele->rgb[2], ele->rgb[3]);

  if (ele->rgb[0] == rgb[0] &&
      ele->rgb[1] == rgb[1] &&
      ele->rgb[2] == rgb[2] &&
      ele->rgb[3] == rgb[3])
    return 0;

  DBG_fprintf(stderr, "                      -> (%f, %f, %f, %f).\n",
	      rgb[0], rgb[1], rgb[2], rgb[3]);

  ele->rgb[0] = rgb[0];
  ele->rgb[1] = rgb[1];
  ele->rgb[2] = rgb[2];
  ele->rgb[3] = rgb[3];

  visu_element_createMaterial(ele);

  DBG_fprintf(stderr, "Visu Element: %p(%s) emit the 'ElementMaterialChanged'.\n",
              (gpointer)ele, ele->name);
  g_signal_emit(ele, visu_element_signals[ELEMENT_MATERIAL_CHANGED_SIGNAL],
                0, NULL);

  if (ele->rendered)
    return 2 * VISU_ELEMENT_GET_CLASS(ele)->flagCreateNodesAfterMaterialChange - 1;
  else
    return 0;
}

/**
 * visu_element_setRGBValue:
 * @ele: the element of which the color must be changed,
 * @rgb: the component to change, 0 for red, 1 for green and 2
 * for blue,
 * @value: the value for one of the red, green or blue component.
 *
 * It saves the specific value of rgb (0 for red, 1 for green and 2
 * for bluein the specified VisuElement.
 *
 * Returns: > 0 if values for @ele have changed.
 */
gint visu_element_setRGBValue(VisuElement* ele, int rgb, float value)
{
  if (rgb < 0 || rgb > 3)
    return 0;

  if (value == ele->rgb[rgb])
    return 0;

  ele->rgb[rgb] = value;
  visu_element_createMaterial(ele);

  DBG_fprintf(stderr, "Visu Element: %p(%s) emit the 'ElementMaterialChanged'.\n",
              (gpointer)ele, ele->name);
  g_signal_emit(ele, visu_element_signals[ELEMENT_MATERIAL_CHANGED_SIGNAL],
                0, NULL);

  if (ele->rendered)
    return 2 * VISU_ELEMENT_GET_CLASS(ele)->flagCreateNodesAfterMaterialChange - 1;
  else
    return 0;
}

/**
 * visu_element_setAllMaterialValues:
 * @ele: the element of which the color must be changed,
 * @material:  (in) (array fixed-size=5): the new values to define the lighting.
 *
 * It saves the values of material in the specified VisuElement.
 *
 * Returns: > 0 if values for @ele have changed.
 */
gint visu_element_setAllMaterialValues(VisuElement* ele, float material[5])
{
  if (!ele)
    return 0;

  ele->material[0] = material[0];
  ele->material[1] = material[1];
  ele->material[2] = material[2];
  ele->material[3] = material[3];
  ele->material[4] = material[4];

  visu_element_createMaterial(ele);

  DBG_fprintf(stderr, "Visu Element: %p(%s) emit the 'ElementMaterialChanged'.\n",
              (gpointer)ele, ele->name);
  g_signal_emit(ele, visu_element_signals[ELEMENT_MATERIAL_CHANGED_SIGNAL],
                0, NULL);

  if (ele->rendered)
    return 2 * VISU_ELEMENT_GET_CLASS(ele)->flagCreateNodesAfterMaterialChange - 1;
  else
    return 0;
}

/**
 * visu_element_setMaterialValue:
 * @ele: the element of which the color must be changed,
 * @material: the component to change,
 * @value: the new value of the specified component.
 *
 * It saves the specific value of material (use the enum as the parameter
 * material) in the specified VisuElement.
 *
 * Returns: > 0 if values for @ele have changed.
 */
gboolean visu_element_setMaterialValue(VisuElement* ele, int material, float value)
{
  if (material < 0 || material > 4)
    return 0;

  if (value == ele->material[material])
    return 0;

  ele->material[material] = value;
  visu_element_createMaterial(ele);

  DBG_fprintf(stderr, "Visu Element: %p(%s) emit the 'ElementMaterialChanged'.\n",
              (gpointer)ele, ele->name);
  g_signal_emit(ele, visu_element_signals[ELEMENT_MATERIAL_CHANGED_SIGNAL],
                0, NULL);

  if (ele->rendered)
    return 2 * VISU_ELEMENT_GET_CLASS(ele)->flagCreateNodesAfterMaterialChange - 1;
  else
    return 0;
}

/**
 * visu_element_setSensitiveToPlanes:
 * @element: a #VisuElement object ;
 * @status: TRUE or FALSE.
 *
 * This method sets the private attribute 'sensitiveToPlanes' to TRUE or FALSE
 * for the specified #visuElement. If TRUE, all the nodes of that #VisuElement
 * are not sensitive to the masking property of planes.
 *
 * Returns: TRUE if values for @ele have changed.
 */
gboolean visu_element_setSensitiveToPlanes(VisuElement *element, gboolean status)
{
  g_return_val_if_fail(element, FALSE);

  DBG_fprintf(stderr, "Visu Element: '%s' set sensitiveToPlanes %d\n",
	      element->name, element->sensitiveToPlanes);

  if (element->sensitiveToPlanes == status)
    return FALSE;

  element->sensitiveToPlanes = status;
  DBG_fprintf(stderr, " -> %d\n", status);

  DBG_fprintf(stderr, "Visu Element: %p(%s) emit the 'ElementPlaneChanged'.\n",
              (gpointer)element, element->name);
  g_signal_emit(element, visu_element_signals[ELEMENT_VISU_PLANE_CHANGED_SIGNAL],
                0, NULL);

  return TRUE;
}

/**
 * visu_element_getSensitiveToPlanes:
 * @element: a #VisuElement object.
 *
 * This method is used to retrieve if nodes of the element are sensitive to
 * the masking property of planes, or not.
 *
 * Returns: TRUE if nodes are masked by planes.
 */
gboolean visu_element_getSensitiveToPlanes(VisuElement *element)
{
  g_return_val_if_fail(element, TRUE);
  
  return element->sensitiveToPlanes;
}

/**
 * visu_element_createMaterial:
 * @ele: a pointer to a valid %VisuElement.
 *
 * Create a list whose number is defined by @ele->glMaterialId
 * that stores the definition of light and color for this #VisuElement. The previous
 * OpenGL object list with the same identifier is deleted.
 */
void visu_element_createMaterial(VisuElement *ele)
{
  g_return_if_fail(ele);

  DBG_fprintf(stderr, "Visu Element: create material for %s (OpenGL id %d)\n",
              ele->name, ele->glMaterialId);
/*   fprintf(stderr, "%f %f %f - %f %f %f %f %f\n", ele->rgb[0], ele->rgb[1], ele->rgb[2], ele->material[0], ele->material[1], ele->material[2], ele->material[3], ele->material[4]); */

  glDeleteLists(ele->glMaterialId, 1);
  glNewList(ele->glMaterialId, GL_COMPILE);
  visu_gl_setColor(ele->material, ele->rgb);
  glEndList();
  ele->materialIsUpToDate = TRUE;
}

/**
 * visu_element_getMaterialId:
 * @ele: an element.
 *
 * This method is useful for the rendering method to get the
 * OpenGl identifier of the material of the specified element.
 *
 * Returns: the OpenGL identifier of the specified #VisuElement.
 */
int visu_element_getMaterialId(VisuElement *ele)
{
  if (!ele)
    return 0;
/*   if (!ele->materialIsUpToDate) */
/*     visu_element_createMaterial(ele); */

  return ele->glMaterialId;
}

/**
 * visu_element_setRendered:
 * @element: a #VisuElement object ;
 * @rendered: TRUE or FALSE.
 *
 * This method sets the private attribute 'rendered' to TRUE or FALSE
 * for the specified #visuElement. If FALSE, all the nodes of that #VisuElement
 * are not included in the nodes OpenGL list.
 *
 * Returns: TRUE if values for @ele have changed.
 */

gboolean visu_element_setRendered(VisuElement *element, gboolean rendered)
{
  g_return_val_if_fail(element, FALSE);

  if (element->rendered == rendered)
    return FALSE;

  element->rendered = rendered;
  DBG_fprintf(stderr, "Visu Element: %p(%s) emit the 'ElementVisibilityChanged'.\n",
              (gpointer)element, element->name);
  g_signal_emit(element, visu_element_signals[ELEMENT_VISIBILITY_CHANGED_SIGNAL],
		0, NULL);
  return TRUE;
}

/**
 * visu_element_getRendered:
 * @element: a #VisuElement object.
 *
 * This gets the value of the private attribute 'rendered' for
 * the specified #VisuElement.
 *
 * Returns: the value of attribute 'rendered'.
 */
gboolean visu_element_getRendered(VisuElement *element)
{
  g_return_val_if_fail(element, FALSE);

  return element->rendered;
}

/**
 * visu_element_setUpdateNodesOnMaterialChange:
 *
 * If this method is called, whenever a change occurs to a #VisuElement material
 * description (color and light) the corresponding OpenGl list of nodes of the
 * currentVisuData is rebuilt. This is usefull if the nodes can't use the list material
 * because the color is node dependant for example. Use
 * visu_element_unsetUpdateNodesOnMaterialChange() to return to the normal behavior.
 */
void visu_element_setUpdateNodesOnMaterialChange(void)
{
  if (!my_class)
    g_type_class_ref(VISU_TYPE_ELEMENT);

  my_class->flagCreateNodesAfterMaterialChange += 1;
  DBG_fprintf(stderr, "Visu Element : set flagCreateNodesAfterMaterialChange to %d.\n",
	      my_class->flagCreateNodesAfterMaterialChange);
}

/**
 * visu_element_unsetUpdateNodesOnMaterialChange:
 *
 * If this method is called (and no other method has used
 * visu_element_setUpdateNodesOnMaterialChange(), a changement
 * in the material description only change the list of the material.
 */
void visu_element_unsetUpdateNodesOnMaterialChange(void)
{
  if (!my_class)
    g_type_class_ref(VISU_TYPE_ELEMENT);

  my_class->flagCreateNodesAfterMaterialChange -= 1;
  if (my_class->flagCreateNodesAfterMaterialChange < 0)
    my_class->flagCreateNodesAfterMaterialChange = 0;
  DBG_fprintf(stderr, "Visu Element : set flagCreateNodesAfterMaterialChange to %d.\n",
	      my_class->flagCreateNodesAfterMaterialChange);
}

/**
 * visu_element_getUpdateNodesOnMaterialChange:
 *
 * Retrieve if one the #VisuElement is sensitive to the material
 * values. See visu_element_setUpdateNodesOnMaterialChange().
 *
 * Returns: TRUE if a #VisuGlExtNodes object should rebuild its nodes when the
 * material values are changed.
 */
gboolean visu_element_getUpdateNodesOnMaterialChange(void)
{
  if (!my_class)
    g_type_class_ref(VISU_TYPE_ELEMENT);

  return (my_class->flagCreateNodesAfterMaterialChange > 0);
}

/******************/
/* Resources part */
/******************/
static gboolean readElementColor(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				 VisuData *dataObj _U_, VisuGlView *view _U_,
                                 GError **error)
{
  VisuElement* ele;
  float all[9];

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readFloatWithElement(lines[0], position, all, 9, &ele, error))
    return FALSE;
  ele->rgb[0] = CLAMP(all[0], 0., 1.);
  ele->rgb[1] = CLAMP(all[1], 0., 1.);
  ele->rgb[2] = CLAMP(all[2], 0., 1.);
  ele->rgb[3] = CLAMP(all[3], 0., 1.);
  ele->material[0] = CLAMP(all[4], 0., 1.);
  ele->material[1] = CLAMP(all[5], 0., 1.);
  ele->material[2] = CLAMP(all[6], 0., 1.);
  ele->material[3] = CLAMP(all[7], 0., 1.);
  ele->material[4] = CLAMP(all[8], 0., 1.);
  tool_color_addFloatRGBA(ele->rgb, (int*)0);
  
  return TRUE;
}
static gboolean readElementProperties(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                      VisuData *dataObj _U_, VisuGlView *view _U_,
                                      GError **error)
{
  VisuElement* ele;
  float values[2];

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readFloatWithElement(lines[0], position, values, 2, &ele, error))
    return FALSE;
  visu_element_setRendered(ele, (gboolean)values[0]);
  visu_element_setSensitiveToPlanes(ele, (gboolean)values[1]);
  
  return TRUE;
}

static gboolean readMaterial(VisuConfigFileEntry *entry, gchar **lines, int nbLines, int position,
			     VisuData *dataObj, VisuGlView *view, GError **error)
{
  return readElementColor(entry, lines, nbLines, position, dataObj, view, error);
}
static gboolean readRendered(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			     VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  VisuElement* ele;
  float value;

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readFloatWithElement(lines[0], position, &value, 1, &ele, error))
    return FALSE;
  visu_element_setRendered(ele, (gboolean)value);
  
  return TRUE;
}

/* These functions read all the element list to export there associated resources. */
static void exportResourcesRenderingBase(GString *data,
                                         VisuData *dataObj, VisuGlView *view _U_)
{
  GList *pos, *eleList;
  VisuNodeArrayIter iter;

  visu_config_file_exportComment(data, DESC_ELEMENT_COLOR);
  /* We create a list of elements, or get the whole list. */
  if (dataObj)
    {
      eleList = (GList*)0;
      visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter);
      for (visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter); iter.element;
           visu_node_array_iterNextElement(VISU_NODE_ARRAY(dataObj), &iter))
        eleList = g_list_prepend(eleList, (gpointer)iter.element);
    }
  else
    eleList = g_list_copy((GList*)visu_element_getAllElements());
  for (pos = eleList; pos; pos = g_list_next(pos))
    visu_config_file_exportEntry(data, FLAG_ELEMENT_COLOR, ((VisuElement*)pos->data)->name,
                                 "%4.3f %4.3f %4.3f %4.3f   %4.2f %4.2f %4.2f %4.2f %4.2f",
                                 ((VisuElement*)pos->data)->rgb[0],
                                 ((VisuElement*)pos->data)->rgb[1],
                                 ((VisuElement*)pos->data)->rgb[2],
                                 ((VisuElement*)pos->data)->rgb[3],
                                 ((VisuElement*)pos->data)->material[0],
                                 ((VisuElement*)pos->data)->material[1],
                                 ((VisuElement*)pos->data)->material[2],
                                 ((VisuElement*)pos->data)->material[3],
                                 ((VisuElement*)pos->data)->material[4]);
  visu_config_file_exportComment(data, DESC_ELEMENT_PROPERTIES);
  for (pos = eleList; pos; pos = g_list_next(pos))
    visu_config_file_exportEntry(data, FLAG_ELEMENT_PROPERTIES,
                                 ((VisuElement*)pos->data)->name,
                                 "%d %d", ((VisuElement*)pos->data)->rendered,
                                 ((VisuElement*)pos->data)->sensitiveToPlanes);
  g_list_free(eleList);
  visu_config_file_exportComment(data, "");
}
