/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_DRAWING_H
#define VISU_DRAWING_H

#include <glib.h>
#include <gio/gio.h>
#include <stdio.h>

#include "visu_data.h"
#include "openGLFunctions/view.h"
#include "coreTools/toolFileFormat.h"

G_BEGIN_DECLS

/* The way visu renders its data is done by modules. They are called
   rendering methods and they describes how data are drawn on
   the screen. Many can be defined but only one is used at a
   time to render the data.
   One or more file type are associated with a rendering method. And
   a rendering method must specify the way to load the data it needs.
   A rendering method can introduced new resources and it must
   implement methods and create variable to deal with them :
    - one GHashTable that store as keys the name of all
    the resources introduced by the method and as value
    a description that is used as a commentary when the
    resources are exported to a file.
    - one method to read from a line the values of a specified
    resource.
    - one method to write to a file the values of a specified
    resource. */

/**
 * VISU_ERROR_RENDERING: (skip)
 *
 * Internal function for error handling.
 */
#define VISU_ERROR_RENDERING visu_rendering_class_getErrorQuark()
GQuark visu_rendering_class_getErrorQuark(void);
/**
 * VisuRenderingErrorFlag:
 * @RENDERING_ERROR_METHOD: Error from the rendering method.
 * @RENDERING_ERROR_FILE: Error when opening.
 * @RENDERING_ERROR_FORMAT: Wrongness in format.
 * @RENDERING_ERROR_CANCEL: the rendering operation has been cancelled.
 *
 * Thiese are flags used when reading a file with a rendering method.
 */
typedef enum
  {
    RENDERING_ERROR_METHOD,   /* Error from the rendering method. */
    RENDERING_ERROR_FILE,     /* Error when opening. */
    RENDERING_ERROR_FORMAT,   /* Wrongness in format. */
    RENDERING_ERROR_CANCEL    /* Cancellation asked. */
  } VisuRenderingErrorFlag;

/**
 * VISU_TYPE_RENDERING:
 *
 * return the type of #VisuRendering.
 */
#define VISU_TYPE_RENDERING	     (visu_rendering_get_type ())
/**
 * VISU_RENDERING:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuRendering type.
 */
#define VISU_RENDERING(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_RENDERING, VisuRendering))
/**
 * VISU_RENDERING_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuRenderingClass.
 */
#define VISU_RENDERING_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_RENDERING, VisuRenderingClass))
/**
 * VISU_IS_RENDERING_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuRendering object.
 */
#define VISU_IS_RENDERING_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_RENDERING))
/**
 * VISU_IS_RENDERING_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuRenderingClass class.
 */
#define VISU_IS_RENDERING_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_RENDERING))
/**
 * VISU_RENDERING_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_RENDERING_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_RENDERING, VisuRenderingClass))

/**
 * VisuRenderingLoadFormatFunc:
 * @data: (transfer full): a #VisuData object ;
 * @filename: the access to the file to load ;
 * @format: a #ToolFileFormat object (can be NULL) ;
 * @nSet: an integer ;
 * @cancel: a cancellable.
 * @error: a pointer to store possible errors.
 * 
 * This is an interface for a generic load method. This method read the file
 * positionned on @filename and populate or change the arrays in
 * @data. When enter this method, the @data argument is already
 * allocated but its arrays may be empty and unallocated (depending on
 * context). If the load method fails (because the format is wrong or
 * anything else), the @data argument should not be modified. If some
 * errors occur, the pointer @error will be instanciated. A
 * #VisuRendering can have several #VisuRenderingLoadFormatFunc methods
 * for each format they support. The @nSet argument is used to load a
 * specific set of nodes if the input format supports it. If @nSet is
 * 0, then the default set of nodes is loaded.
 *
 * Returns: FALSE if @data is unchanged (wrong format), TRUE otherwise (even if
 *          some minor errors have happened).
 */
typedef gboolean (*VisuRenderingLoadFormatFunc) (VisuData *data, const gchar* filename,
                                                 ToolFileFormat *format, int nSet,
                                                 GCancellable *cancel, GError **error);
/**
 * VisuRenderingCreateElementFunc:
 * @ele: a #VisuElement object ;
 * @view: a #VisuGlView object.
 *
 * Such functions are called whenever a newElement is registered.
 *
 * Returns: an id representing an OpenGL list in which the element
 *          has been created.
 */
typedef int (*VisuRenderingCreateElementFunc) (VisuElement* ele, VisuGlView *view);
/**
 * VisuRenderingCreateNodeFunc:
 * @visuData: a #VisuData object ;
 * @node: a #VisuElement ;
 * @ele: a #VisuElement ;
 * @eleGlId: an identifier.
 *
 * Such functions are called to draw @node at its right position.
 * The @ele parameter is the #VisuElement of the given @node and the @visuData one
 * points to the #VisuData object that contains this node. @eleGlId
 * additionnal parameter represent an identifier to the OpenGL list
 * @ele is represented by. One can use this parameter to avoid calling
 * visu_rendering_getElementGlId() for each node.
 */
typedef void (*VisuRenderingCreateNodeFunc) (VisuData *visuData, VisuNode* node,
                                             VisuElement* ele, int eleGlId);
/**
 * VisuRenderingGetNodeExtendFunc:
 * @ele: a @VisuElement.
 *
 * This function is required to inform the OpenGL drawer
 * and to adapt the maximum size of the drawing area.
 *
 * Returns: the geometrical size of the element.
 */
typedef float (*VisuRenderingGetNodeExtendFunc) (VisuElement* ele);

/**
 * VisuRenderingGetElementGlId:
 * @ele: a @VisuElement.
 *
 * This function is used to get the OpenGL list identifier for @ele.
 *
 * Since: 3.7
 *
 * Returns: an id.
 */
typedef int (*VisuRenderingGetElementGlId) (VisuElement* ele);

/**
 * VisuRenderingPrivate:
 *
 * An opaque structure.
 *
 * Since: 3.6
 */
typedef struct _VisuRenderingPrivate VisuRenderingPrivate;

typedef struct _VisuRendering VisuRendering;
struct _VisuRendering
{
  GObject parent;

  VisuRenderingPrivate *priv;
};
/**
 * VisuRenderingClass:
 * @parent: the parent class.
 * @createElement: a virtual method to create OpenGL shape for a given
 * #VisuElement.
 * @getElementGlId: a virtual method to get the OpenGL list id used
 * for elements.
 * @createNode: a virtual method to position and draw a specific
 * #VisuNode.
 * @getNodeExtend: a virtual method to get the size of a given #VisuElement.
 *
 * The structure for the #VisuRenderingClass class.
 */
typedef struct _VisuRenderingClass VisuRenderingClass;
struct _VisuRenderingClass
{
  GObjectClass parent;

  VisuRenderingCreateElementFunc createElement;
  VisuRenderingGetElementGlId    getElementGlId;
  VisuRenderingCreateNodeFunc    createNode;
  VisuRenderingGetNodeExtendFunc getNodeExtend;
};


/**
 * visu_rendering_get_type:
 *
 * This method returns the type of #VisuRendering, use VISU_TYPE_RENDERING instead.
 *
 * Returns: the type of #VisuRendering.
 */
GType visu_rendering_get_type(void);




/***************/
/* Public area */
/***************/
void visu_rendering_addFileFormat(VisuRendering *method, guint fileType,
                                  ToolFileFormat *fmt, guint priority,
                                  VisuRenderingLoadFormatFunc loadFunc);
void visu_rendering_setFileTypeLabel(VisuRendering *method,
                                     guint fileType, const gchar *name);
void visu_rendering_setFileFormat(VisuRendering *meth, guint fileType,
                                  VisuRendering *from);
void visu_rendering_setIcon(VisuRendering *method, const gchar *path);

gboolean visu_rendering_load(VisuRendering *method, VisuData *data,
                             int nSet, GCancellable *cancel, GError **error);

const gchar* visu_rendering_getName(VisuRendering *method, gboolean UTF8);
const gchar* visu_rendering_getDescription(VisuRendering *method);
guint visu_rendering_getNFileTypes(VisuRendering *method);
GList* visu_rendering_getFileFormat(VisuRendering *method, guint fileType);
const gchar* visu_rendering_getFileTypeName(VisuRendering *method, guint fileType);
const gchar* visu_rendering_getIconPath(VisuRendering *method);

int visu_rendering_getElementGlId(VisuRendering *method, VisuElement *ele);
float visu_rendering_getSizeOfElement(VisuRendering *method, VisuElement *ele);
int visu_rendering_createElement(VisuRendering *method, VisuElement *element,
                                 VisuGlView *view);
void visu_rendering_createNode(VisuRendering *method, VisuData *data,
                               VisuNode *node, VisuElement *ele, int eleGlId);

GList* visu_rendering_getAllObjects(void);
VisuRendering* visu_rendering_getByName(const gchar *name);

G_END_DECLS


#endif
