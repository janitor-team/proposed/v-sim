/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "paths.h"

#include <openGLFunctions/text.h>
#include <openGLFunctions/objectList.h>

/**
 * SECTION:paths
 * @short_description: Defines methods to draw paths.
 *
 * <para>Create a #VisuGlExt object to handle #VisuPaths drawing.</para>
 */

/**
 * VisuGlExtPathsClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtPathsClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtPaths:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtPathsPrivate:
 *
 * Private fields for #VisuGlExtPaths objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtPathsPrivate
{
  gboolean dispose_has_run;
  gboolean areBuilt;

  /* Related objects. */
  VisuPaths *obj;
};

static void visu_gl_ext_paths_finalize(GObject* obj);
static void visu_gl_ext_paths_dispose(GObject* obj);
static void visu_gl_ext_paths_rebuild(VisuGlExt *ext);

/* Local callbacks */

G_DEFINE_TYPE(VisuGlExtPaths, visu_gl_ext_paths, VISU_TYPE_GL_EXT)

static void visu_gl_ext_paths_class_init(VisuGlExtPathsClass *klass)
{
  DBG_fprintf(stderr, "Extension Paths: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_paths_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_paths_finalize;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_paths_rebuild;
}

static void visu_gl_ext_paths_init(VisuGlExtPaths *obj)
{
  DBG_fprintf(stderr, "Extension Paths: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = g_malloc(sizeof(VisuGlExtPathsPrivate));
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->areBuilt = FALSE;
  obj->priv->obj      = (VisuPaths*)0;
}
static void visu_gl_ext_paths_dispose(GObject* obj)
{
  VisuGlExtPaths *paths;

  DBG_fprintf(stderr, "Extension Paths: dispose object %p.\n", (gpointer)obj);

  paths = VISU_GL_EXT_PATHS(obj);
  if (paths->priv->dispose_has_run)
    return;
  paths->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_paths_set(paths, (VisuPaths*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_paths_parent_class)->dispose(obj);
}
static void visu_gl_ext_paths_finalize(GObject* obj)
{
  VisuGlExtPaths *paths;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Extension Paths: finalize object %p.\n", (gpointer)obj);

  paths = VISU_GL_EXT_PATHS(obj);
  if (paths->priv)
    {
      DBG_fprintf(stderr, "Extension Paths: free private paths.\n");
      g_free(paths->priv);
    }

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Extension Paths: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_paths_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Extension Paths: freeing ... OK.\n");
}

/**
 * visu_gl_ext_paths_new:
 * @name: (allow-none): the name to give to the extension (default is #VISU_GL_EXT_PATHS_ID).
 *
 * Creates a new #VisuGlExt to draw paths.
 *
 * Since: 3.7
 *
 * Returns: (transfer full): a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtPaths* visu_gl_ext_paths_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_PATHS_ID;
  char *description = _("Representation of paths.");
  VisuGlExt *extensionPaths;

  DBG_fprintf(stderr,"Extension Paths: new object.\n");
  
  extensionPaths = VISU_GL_EXT(g_object_new(VISU_TYPE_GL_EXT_PATHS,
                                            "name", (name)?name:name_, "label", _(name),
                                            "description", description, "nGlObj", 1,
                                            "priority", VISU_GL_EXT_PRIORITY_LAST - 1, NULL));

  return VISU_GL_EXT_PATHS(extensionPaths);
}

/**
 * visu_gl_ext_paths_set:
 * @paths: the #VisuGlExtPaths object to modify.
 * @obj: (allow-none) (transfer none): a #VisuPaths object.
 *
 * Set the #VisuPaths to be drawn.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_paths_draw() should be called and then 'OpenGLAskForReDraw'
 *          signal be emitted.
 **/
gboolean visu_gl_ext_paths_set(VisuGlExtPaths *paths, VisuPaths *obj)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_PATHS(paths), FALSE);

  DBG_fprintf(stderr, "Extension Paths: set a new path %p (%p).\n",
              (gpointer)obj, (gpointer)paths->priv->obj);
  if (obj == paths->priv->obj)
    return FALSE;

  if (paths->priv->obj)
    visu_paths_unref(paths->priv->obj);
  paths->priv->obj = obj;
  if (obj)
    visu_paths_ref(obj);

  paths->priv->areBuilt = FALSE;
  return TRUE;
}
/**
 * visu_gl_ext_paths_setDirty:
 * @paths: a #VisuGlExtPaths object.
 *
 * Currently, #VisuPath are not objects, so @paths cannot react to a
 * change on them. Callers have to set by hand that @paths should be
 * redrawn with this routine.
 *
 * Since: 3.7
 **/
void visu_gl_ext_paths_setDirty(VisuGlExtPaths *paths)
{
  g_return_if_fail(VISU_IS_GL_EXT_PATHS(paths));

  paths->priv->areBuilt = FALSE;
}

/****************/
/* Private part */
/****************/
static void visu_gl_ext_paths_rebuild(VisuGlExt *ext)
{
  VisuGlExtPaths *paths = VISU_GL_EXT_PATHS(ext);

  paths->priv->areBuilt = FALSE;
  visu_gl_ext_paths_draw(paths);
}

/**
 * visu_gl_ext_paths_draw:
 * @paths: the #VisuBox object to build paths for.
 *
 * This method creates a compiled list that draws paths.
 */
void visu_gl_ext_paths_draw(VisuGlExtPaths *paths)
{
  g_return_if_fail(VISU_IS_GL_EXT_PATHS(paths));

  /* Nothing to draw; */
  if(!visu_gl_ext_getActive(VISU_GL_EXT(paths)) || paths->priv->areBuilt) return;

  glNewList(visu_gl_ext_getGlList(VISU_GL_EXT(paths)), GL_COMPILE);
  if (paths->priv->obj)
    visu_paths_draw(paths->priv->obj);
  glEndList();

  paths->priv->areBuilt = TRUE;
}
