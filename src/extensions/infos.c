/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2013)
  
	Adresse m�l :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2013)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#include "infos.h"

#include <GL/gl.h>

#include <visu_object.h>
#include <visu_rendering.h>
#include <openGLFunctions/objectList.h>
#include <openGLFunctions/text.h>

/**
 * SECTION:infos
 * @short_description: give the capability to draw some information
 * near each node.
 *
 * <para>This part is used to draw some information near the
 * nodes. This information can be the one of a #VisuNodeProperty or
 * something else. When read from a #VisuNodeProperty, just giving the
 * name will produce the right output. In other cases a print routine
 * must be given.</para>
 */

/* Interface for a routine to draw the informations into a label. */
typedef void (*DrawInfosFunc)(VisuData *data, VisuElement *element,
			      VisuNode *node, VisuDataNode *dataNode);
static void drawNumber(VisuData *data, VisuElement *element,
		       VisuNode *node, VisuDataNode *dataNode);
static void drawElement(VisuData *data, VisuElement *element,
			VisuNode *node, VisuDataNode *dataNode);
static void drawInfos(VisuData *data, VisuElement *element,
		      VisuNode *node, VisuDataNode *dataNode);

/**
 * VisuGlExtInfosClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtInfosClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtInfos:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtInfosPrivate:
 *
 * Private fields for #VisuGlExtInfos objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtInfosPrivate
{
  gboolean dispose_has_run;
  gboolean isBuilt;

  /* What to draw. */
  int *nodes;
  DrawInfosFunc draw;
  
  /* Object signals. */
  VisuData *dataObj;
  gulong popDec, popInc, posChg, rndChg, matChg;
  VisuGlView *view;
  gulong angChg;
  VisuDataNode *dataNode;
  gulong propUsed, propUnsd, propChgd;
};

static VisuGlExtInfos *defaultInfos = NULL;

static void visu_gl_ext_infos_finalize(GObject* obj);
static void visu_gl_ext_infos_dispose(GObject* obj);
static void visu_gl_ext_infos_rebuild(VisuGlExt *ext);

/* Callbacks. */
static void onPopulationIncrease(VisuData *data, int *newNodes, gpointer user_data);
static void onPopulationDecrease(VisuData *data, int *oldNodes, gpointer user_data);
static void onPositionChanged(VisuData *data, VisuElement *ele, gpointer user_data);
static void onNodeChanged(VisuData *data, gpointer user_data);
static void onCameraMoved(VisuGlView *view, gpointer user_data);
static void onProperty(VisuDataNode *dataNode, VisuData *dataObj, gpointer user_data);
static void onPropertyChanged(VisuDataNode *dataNode, VisuData *dataObj, gpointer user_data);

static void _setDataNode(VisuGlExtInfos *infos, VisuDataNode *dataNode);

G_DEFINE_TYPE(VisuGlExtInfos, visu_gl_ext_infos, VISU_TYPE_GL_EXT)

static void visu_gl_ext_infos_class_init(VisuGlExtInfosClass *klass)
{
  DBG_fprintf(stderr, "Extension Infos: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  /* DBG_fprintf(stderr, "                - adding new resources ;\n"); */

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_infos_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_infos_finalize;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_infos_rebuild;
}

static void visu_gl_ext_infos_init(VisuGlExtInfos *obj)
{
  DBG_fprintf(stderr, "Extension Infos: initializing a new object (%p).\n",
	      (gpointer)obj);

  obj->priv = g_malloc(sizeof(VisuGlExtInfosPrivate));
  obj->priv->dispose_has_run = FALSE;
  obj->priv->isBuilt         = FALSE;

  /* Private data. */
  obj->priv->nodes    = (int*)0;
  obj->priv->draw     = (DrawInfosFunc)0;
  obj->priv->dataNode = (VisuDataNode*)0;
  obj->priv->dataObj  = (VisuData*)0;
  obj->priv->view     = (VisuGlView*)0;
  obj->priv->popDec   = 0;
  obj->priv->popInc   = 0;
  obj->priv->rndChg   = 0;
  obj->priv->posChg   = 0;
  obj->priv->matChg   = 0;
  obj->priv->angChg   = 0;
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_gl_ext_infos_dispose(GObject* obj)
{
  VisuGlExtInfos *infos;

  DBG_fprintf(stderr, "Extension Infos: dispose object %p.\n", (gpointer)obj);

  infos = VISU_GL_EXT_INFOS(obj);
  if (infos->priv->dispose_has_run)
    return;
  infos->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_infos_setGlView(infos, (VisuGlView*)0);
  visu_gl_ext_infos_setData(infos, (VisuData*)0);
  _setDataNode(infos, (VisuDataNode*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_infos_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_gl_ext_infos_finalize(GObject* obj)
{
  VisuGlExtInfos *infos;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Extension Infos: finalize object %p.\n", (gpointer)obj);

  infos = VISU_GL_EXT_INFOS(obj);
  /* Free privs elements. */
  if (infos->priv)
    {
      DBG_fprintf(stderr, "Extension Infos: free private infos.\n");
      g_free(infos->priv->nodes);
      g_free(infos->priv);
    }

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Extension Infos: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_infos_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Extension Infos: freeing ... OK.\n");
}
/**
 * visu_gl_ext_infos_new:
 * @name: (allow-none): the name of the #VisuGlExt.
 *
 * Create a new #VisuGlExt to represent information on nodes.
 *
 * Since: 3.7
 *
 * Returns: a new #VisuGlExtInfos object.
 **/
VisuGlExtInfos* visu_gl_ext_infos_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_INFOS_ID;
  char *description = _("Draw informations on nodes.");
  VisuGlExt *ext;

  DBG_fprintf(stderr,"Extension Infos: new object.\n");
  
  ext = VISU_GL_EXT(g_object_new(VISU_TYPE_GL_EXT_INFOS,
                                    "name", (name)?name:name_, "label", _(name),
                                    "description", description,
                                    "nGlObj", 1, NULL));
  visu_gl_ext_setPriority(ext, VISU_GL_EXT_PRIORITY_HIGH);

  return VISU_GL_EXT_INFOS(ext);
}

/********************/
/* Public routines. */
/********************/
/**
 * visu_gl_ext_infos_setGlView:
 * @infos: The #VisuGlExtInfos to attached to.
 * @view: the nodes to get the population of.
 *
 * Attach an #VisuGlView to render to and setup the infos.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_infos_draw() should be called and
 * then 'OpenGLAskForReDraw' signal be emitted.
 **/
gboolean visu_gl_ext_infos_setGlView(VisuGlExtInfos *infos, VisuGlView *view)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_INFOS(infos), FALSE);

  if (view == infos->priv->view)
    return FALSE;

  if (infos->priv->view)
    {
      g_signal_handler_disconnect(G_OBJECT(infos->priv->view), infos->priv->angChg);
      g_object_unref(infos->priv->view);
    }
  if (view)
    {
      g_object_ref(view);
      infos->priv->angChg =
        g_signal_connect(G_OBJECT(view), "ThetaPhiOmegaChanged",
                         G_CALLBACK(onCameraMoved), (gpointer)infos);
    }
  else
    infos->priv->angChg = 0;
  infos->priv->view = view;

  infos->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(infos));
}
/**
 * visu_gl_ext_infos_setData:
 * @infos: The #VisuGlExtInfos to attached to.
 * @data: the nodes to get the population of.
 *
 * Attach an #VisuData to render to and setup the infos.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_infos_draw() should be called and
 * then 'OpenGLAskForReDraw' signal be emitted.
 **/
gboolean visu_gl_ext_infos_setData(VisuGlExtInfos *infos, VisuData *data)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_INFOS(infos), FALSE);

  if (data == infos->priv->dataObj)
    return FALSE;

  if (infos->priv->dataObj)
    {
      g_signal_handler_disconnect(G_OBJECT(infos->priv->dataObj), infos->priv->popDec);
      g_signal_handler_disconnect(G_OBJECT(infos->priv->dataObj), infos->priv->popInc);
      g_signal_handler_disconnect(G_OBJECT(infos->priv->dataObj), infos->priv->posChg);
      g_signal_handler_disconnect(G_OBJECT(infos->priv->dataObj), infos->priv->rndChg);
      g_signal_handler_disconnect(G_OBJECT(infos->priv->dataObj), infos->priv->matChg);
      g_object_unref(infos->priv->dataObj);
    }
  if (data)
    {
      g_object_ref(data);
      infos->priv->popDec = g_signal_connect(G_OBJECT(data), "PopulationDecrease",
                                             G_CALLBACK(onPopulationDecrease), (gpointer)infos);
      infos->priv->popInc = g_signal_connect(G_OBJECT(data), "PopulationIncrease",
                                             G_CALLBACK(onPopulationIncrease), (gpointer)infos);
      infos->priv->posChg = g_signal_connect(G_OBJECT(data), "PositionChanged",
                                             G_CALLBACK(onPositionChanged), (gpointer)infos);
      infos->priv->rndChg = g_signal_connect(G_OBJECT(data), "VisibilityChanged",
                                             G_CALLBACK(onNodeChanged), (gpointer)infos);
      infos->priv->matChg = g_signal_connect(G_OBJECT(data), "MaterialChanged",
                                             G_CALLBACK(onNodeChanged), (gpointer)infos);
    }
  else
    {
      infos->priv->popDec = 0;
      infos->priv->popInc = 0;
      infos->priv->posChg = 0;
      infos->priv->rndChg = 0;
      infos->priv->matChg = 0;
    }
  infos->priv->dataObj = data;

  infos->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(infos));
}
static void _setDataNode(VisuGlExtInfos *infos, VisuDataNode *dataNode)
{
  g_return_if_fail(VISU_IS_GL_EXT_INFOS(infos));

  if (dataNode == infos->priv->dataNode)
    return;

  if (infos->priv->dataNode)
    {
      g_signal_handler_disconnect(G_OBJECT(infos->priv->dataNode), infos->priv->propUsed);
      g_signal_handler_disconnect(G_OBJECT(infos->priv->dataNode), infos->priv->propUnsd);
      g_signal_handler_disconnect(G_OBJECT(infos->priv->dataNode), infos->priv->propChgd);
      g_object_unref(infos->priv->dataNode);
    }
  if (dataNode)
    {
      g_object_ref(dataNode);
      infos->priv->propUsed = g_signal_connect(G_OBJECT(dataNode), "propertyUsed",
                                               G_CALLBACK(onProperty), (gpointer)infos);
      infos->priv->propUnsd = g_signal_connect(G_OBJECT(dataNode), "propertyUnused",
                                               G_CALLBACK(onProperty), (gpointer)infos);
      infos->priv->propChgd = g_signal_connect(G_OBJECT(dataNode), "valueChanged",
                                               G_CALLBACK(onPropertyChanged), (gpointer)infos);
    }
  else
    {
      infos->priv->propUsed = 0;
      infos->priv->propUnsd = 0;
      infos->priv->propChgd = 0;
    }
  infos->priv->dataNode = dataNode;
}
/**
 * visu_gl_ext_infos_drawIds:
 * @infos: the #VisuGlExtInfos object to update.
 * @nodes: (array zero-terminated=1) (transfer full): an integer list,
 * terminated with a negative number.
 *
 * With this extension,
 * some the number of nodes will be drawn on them. Numbers can be drawn and
 * all nodes (set @nodes to a NULL pointer), or to a restricted list of nodes
 * represented by their numbers. In this case, @nodes can have whatever length
 * but must be terminated by a negative integer. This array is then owned by the
 * extension and should not be freed.
 *
 * Returns: TRUE if a call to visu_gl_ext_infos_draw() is needed.
 */
gboolean visu_gl_ext_infos_drawIds(VisuGlExtInfos *infos, int *nodes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_INFOS(infos), FALSE);

  g_free(infos->priv->nodes);
  infos->priv->nodes    = nodes;
  infos->priv->draw     = drawNumber;
  _setDataNode(infos, (VisuDataNode*)0);

  infos->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(infos));
}
/**
 * visu_gl_ext_infos_drawElements:
 * @infos: the #VisuGlExtInfos object to update.
 * @nodes: (array zero-terminated=1) (transfer full): an integer list,
 * terminated with a negative number.
 *
 * As visu_gl_ext_infos_drawIds(), but draw the names of elements instead of their
 * numbers.
 *
 * Returns: TRUE if a call to visu_gl_ext_infos_draw() is needed.
 */
gboolean visu_gl_ext_infos_drawElements(VisuGlExtInfos *infos, int *nodes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_INFOS(infos), FALSE);

  g_free(infos->priv->nodes);
  infos->priv->nodes    = nodes;
  infos->priv->draw     = drawElement;
  _setDataNode(infos, (VisuDataNode*)0);

  infos->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(infos));
}
/**
 * visu_gl_ext_infos_drawData:
 * @infos: the #VisuGlExtInfos object to update.
 * @dataNode: a #VisuDataNode object ;
 * @nodes: (array zero-terminated=1) (transfer full): an integer list,
 * terminated with a negative number.
 *
 * As visu_gl_ext_infos_drawIds(), but draw some informations instead of their
 * numbers. The informations are defined by the @dataNode argument.
 *
 * Returns: TRUE if a call to visu_gl_ext_infos_draw() is needed.
 */
gboolean visu_gl_ext_infos_drawData(VisuGlExtInfos *infos, VisuDataNode *dataNode, int *nodes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_INFOS(infos) &&
                       VISU_IS_DATA_NODE_TYPE(dataNode), FALSE);

  g_free(infos->priv->nodes);
  infos->priv->nodes    = nodes;
  infos->priv->draw     = drawInfos;
  _setDataNode(infos, dataNode);

  infos->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(infos));
}

/**
 * visu_gl_ext_infos_draw:
 * @infos: a #VisuGlExtInfos object.
 *
 * Compile the list representing information displayed on nodes.
 *
 * Since: 3.7
 **/
void visu_gl_ext_infos_draw(VisuGlExtInfos *infos)
{
  VisuRendering *currentRenderingMethod;
  float modelView[16];
  float delta[3], xyz[3], size, rgba[4];
  VisuNodeArray *nodes;
  VisuNodeArrayIter iter;
  int i;

  g_return_if_fail(VISU_IS_GL_EXT_INFOS(infos));

  /* Nothing to draw; */
  if(!infos->priv->view || !infos->priv->dataObj ||
     !visu_gl_ext_getActive(VISU_GL_EXT(infos)) ||
     infos->priv->isBuilt) return;

  currentRenderingMethod = visu_object_getRendering(VISU_OBJECT_INSTANCE);
  g_return_if_fail(currentRenderingMethod);

  /* Get the camera orientation. */
  glGetFloatv(GL_MODELVIEW_MATRIX, modelView);

  visu_gl_text_initFontList();
  
  glNewList(visu_gl_ext_getGlList(VISU_GL_EXT(infos)), GL_COMPILE);
  glPushAttrib(GL_ENABLE_BIT);
  glDisable(GL_LIGHTING);

  /* If infos->nodes is NULL, we draw for all nodes. */
  nodes = VISU_NODE_ARRAY(infos->priv->dataObj);
  if (!infos->priv->nodes)
    {
      DBG_fprintf(stderr, " | use all the nodes.\n");
      visu_node_array_iterNew(nodes, &iter);
      for (visu_node_array_iterStart(nodes, &iter); iter.element;
	   visu_node_array_iterNextElement(nodes, &iter))
	{
	  if (iter.element->rendered)
	    {
	      DBG_fprintf(stderr, "Extension Infos: creating glObjectList of node"
			  " names for '%s'.\n", iter.element->name);
	      rgba[0] = 1.f - iter.element->rgb[0];
	      rgba[1] = 1.f - iter.element->rgb[1];
	      rgba[2] = 1.f - iter.element->rgb[2];
	      rgba[3] = iter.element->rgb[3];
	      glColor4fv(rgba);
	      size = visu_rendering_getSizeOfElement(currentRenderingMethod,
                                                     iter.element);
	      delta[0] = size * modelView[2];
	      delta[1] = size * modelView[6];
	      delta[2] = size * modelView[10];
	      for(visu_node_array_iterRestartNode(nodes, &iter); iter.node;
		  visu_node_array_iterNextNode(nodes, &iter))
		{
		  if (iter.node->rendered)
		    {
		      visu_data_getNodePosition(infos->priv->dataObj, iter.node, xyz);
		      glRasterPos3f(xyz[0] + delta[0],
				    xyz[1] + delta[1],
				    xyz[2] + delta[2]);
		      infos->priv->draw(infos->priv->dataObj, iter.element, iter.node,
                                        infos->priv->dataNode);
		    }
		}
	    }
	}
    }
  else
    {
      DBG_fprintf(stderr, " | use a restricted list of nodes.\n");
      /* infos->nodes is not NULL, we draw for the given infos->nodes only. */
      for (i = 0; infos->priv->nodes[i] >= 0; i++)
	{
          DBG_fprintf(stderr, " | %d\n", infos->priv->nodes[i]);
	  iter.node = visu_node_array_getFromId(nodes, infos->priv->nodes[i]);
	  g_return_if_fail(iter.node);
          iter.element = visu_node_array_getElement(nodes, iter.node);
	  if (iter.element->rendered && iter.node->rendered)
	    {
	      rgba[0] = 1.f - iter.element->rgb[0];
	      rgba[1] = 1.f - iter.element->rgb[1];
	      rgba[2] = 1.f - iter.element->rgb[2];
	      rgba[3] = iter.element->rgb[3];
	      glColor4fv(rgba);
	      size = visu_rendering_getSizeOfElement(currentRenderingMethod,
						    iter.element);
	      delta[0] = size * modelView[2];
	      delta[1] = size * modelView[6];
	      delta[2] = size * modelView[10];
	      visu_data_getNodePosition(infos->priv->dataObj, iter.node, xyz);
	      glRasterPos3f(xyz[0] + delta[0],
			    xyz[1] + delta[1],
			    xyz[2] + delta[2]);
	      infos->priv->draw(infos->priv->dataObj, iter.element, iter.node,
                                infos->priv->dataNode);
	    }
	}
    }
  glPopAttrib();
  glEndList();

  infos->priv->isBuilt = TRUE;
}

/**
 * visu_gl_ext_infos_getDefault:
 *
 * V_Sim is using a default infos object.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuGlExtInfos object used by default.
 **/
VisuGlExtInfos* visu_gl_ext_infos_getDefault()
{
  if (!defaultInfos)
    {
      defaultInfos = visu_gl_ext_infos_new((gchar*)0);
      visu_gl_ext_setActive(VISU_GL_EXT(defaultInfos), FALSE);
    }
  return defaultInfos;
}

/*********************/
/* Private routines. */
/*********************/
static void visu_gl_ext_infos_rebuild(VisuGlExt *ext)
{
  VisuGlExtInfos *infos = VISU_GL_EXT_INFOS(ext);

  visu_gl_text_rebuildFontList();
  infos->priv->isBuilt = FALSE;
  visu_gl_ext_infos_draw(infos);
}
static void drawNumber(VisuData *data _U_, VisuElement *element _U_,
		       VisuNode *node, VisuDataNode *dataNode _U_)
{
  gchar str[10];

  sprintf(str, "%d", node->number + 1);
  visu_gl_text_drawChars(str, VISU_GL_TEXT_NORMAL);
}
static void drawElement(VisuData *data _U_, VisuElement *element,
			VisuNode *node _U_, VisuDataNode *dataNode _U_)
{
  visu_gl_text_drawChars(element->name, VISU_GL_TEXT_NORMAL);
}
static void drawInfos(VisuData *data, VisuElement *element _U_,
		      VisuNode *node, VisuDataNode *dataNode)
{
  gchar *label;

  label = visu_data_node_getValueAsString(dataNode, data, node);
  visu_gl_text_drawChars(label, VISU_GL_TEXT_NORMAL);
  g_free(label);
}

/*************/
/* Callbacks */
/*************/
static void onPopulationIncrease(VisuData *data _U_, int *newNodes _U_,
				 gpointer user_data)
{
  VisuGlExtInfos *infos = VISU_GL_EXT_INFOS(user_data);

  /* If we draw all nodes, then we must redraw. */
  infos->priv->isBuilt = (infos->priv->nodes != (int*)0);
  visu_gl_ext_infos_draw(infos);
}
static void onPopulationDecrease(VisuData *data _U_, int *oldNodes, gpointer user_data)
{
  VisuGlExtInfos *infos = VISU_GL_EXT_INFOS(user_data);
  gboolean redraw;
  int i, j, size;

  DBG_fprintf(stderr, "Extension Informations: caught the 'PopulationDecrease'"
	      " signal, update and rebuild in list nodes case.\n");
  /* If we draw a list of nodes, then we must remove some and redraw. */

  redraw = FALSE;
  /* We remove all old nodes. */
  if (infos->priv->nodes)
    {
      for (size = 0; infos->priv->nodes[size] >= 0; size++);

      for (i = 0; oldNodes[i] >= 0; i++)
        {
          /* We look for oldNodes[i] in infos->nodes. */
          for (j = 0; infos->priv->nodes[j] >= 0 &&
                 infos->priv->nodes[j] != oldNodes[i]; j++);
          if (infos->priv->nodes[j] >= 0)
            {
              /* OK, found. */
              redraw = TRUE;
              size -= 1;
              infos->priv->nodes[j] = infos->priv->nodes[size];
              infos->priv->nodes[size] = -1;
            }
        }
    }
  else
    redraw = TRUE;
  infos->priv->isBuilt = !redraw;
  visu_gl_ext_infos_draw(infos);
}
static void onNodeChanged(VisuData *data _U_, gpointer user_data)
{
  VisuGlExtInfos *infos = VISU_GL_EXT_INFOS(user_data);

  DBG_fprintf(stderr, "Extension Informations: caught the 'VisibilityChanged' or "
	      "'MaterialChanged' signal, rebuild.\n");
  infos->priv->isBuilt = FALSE;
  visu_gl_ext_infos_draw(infos);
}
static void onPositionChanged(VisuData *data _U_, VisuElement *ele _U_, gpointer user_data)
{
  VisuGlExtInfos *infos = VISU_GL_EXT_INFOS(user_data);

  DBG_fprintf(stderr, "Extension Informations: caught the 'PositionChanged'"
              " signal, rebuild.\n");
  infos->priv->isBuilt = FALSE;
  visu_gl_ext_infos_draw(infos);
}
static void onCameraMoved(VisuGlView *view _U_, gpointer user_data)
{
  VisuGlExtInfos *infos = VISU_GL_EXT_INFOS(user_data);

  DBG_fprintf(stderr, "Extension Informations: caught the 'ThetaPhiOmegaChanged'"
	      " signal, rebuild.\n");
  infos->priv->isBuilt = FALSE;
  visu_gl_ext_infos_draw(infos);
}
static void onProperty(VisuDataNode *dataNode, VisuData *dataObj, gpointer user_data)
{
  VisuGlExtInfos *infos = VISU_GL_EXT_INFOS(user_data);
  
  if (dataObj != infos->priv->dataObj)
    return;

  visu_gl_ext_setActive(VISU_GL_EXT(infos), visu_data_node_getUsed(dataNode, dataObj));
}
static void onPropertyChanged(VisuDataNode *dataNode _U_,
                              VisuData *dataObj, gpointer user_data)
{
  VisuGlExtInfos *infos = VISU_GL_EXT_INFOS(user_data);
  
  if (dataObj != infos->priv->dataObj)
    return;
  infos->priv->isBuilt = FALSE;
  visu_gl_ext_infos_draw(infos);
}
