/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2011-2011)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2011-2011)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "node_vectors.h"

#include <GL/gl.h>
#include <GL/glu.h> 
#include <math.h>

#include <visu_object.h>
#include <opengl.h>
#include <openGLFunctions/text.h>
#include <coreTools/toolMatrix.h>

#include <visu_configFile.h>
#include <coreTools/toolConfigFile.h>

#define FLAG_RESOURCE_ARROW "nodeDisplacement_arrow"
#define DESC_RESOURCE_ARROW "Describe the arrow to be drawn ; four floats (tail lg. tail rd. head lg. head rd., negative values means auto)"
static gboolean readArrow(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			  VisuData *dataObj, VisuGlView *view, GError **error);
#define FLAG_RESOURCE_RATIO_MIN "nodeDisplacement_minThreshold"
#define DESC_RESOURCE_RATIO_MIN "Choose the minimum value for drawn arrows in geometry differences ; float (ratio threshold if between -1 and 0)"
#define DEFT_RESOURCE_RATIO_MIN -0.1f
static gboolean readRatioMin(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			     VisuData *dataObj, VisuGlView *view, GError **error);
#define FLAG_RESOURCE_RATIO_STR "nodeDisplacement_lblThreshold"
#define DESC_RESOURCE_RATIO_STR "Choose the minimum value for labels in geometry differences ; float (ratio threshold if between -1 and 0)"
#define DEFT_RESOURCE_RATIO_STR -0.9f
static gboolean readRatioStr(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			     VisuData *dataObj, VisuGlView *view, GError **error);
#define FLAG_RESOURCE_MULT "nodeDisplacement_factor"
#define DESC_RESOURCE_MULT "Choose the factor to draw arrows in geometry differences ; float (negative means auto)"
#define DEFT_RESOURCE_MULT -1.f
static gboolean readMult(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			 VisuData *dataObj, VisuGlView *view, GError **error);
#define DEFT_RESOURCE_SCAL -4.f
static void exportResourcesDiff(GString *data, VisuData *dataObj, VisuGlView *view);

/**
 * SECTION:node_vectors
 * @short_description: Draw arrows at each node to represent forces,
 * displacements, vibrations...
 *
 * <para>A generic #VisuGlExt to represent vectors on nodes.</para>
 */

enum
  {
    TAIL_LENGTH,
    TAIL_RADIUS,
    TAIL_NLAT,
    HEAD_LENGTH,
    HEAD_RADIUS,
    HEAD_NLAT,
    N_ARROW_DEFS
  };

/**
 * VisuGlExtNodeVectorsClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtNodeVectorsClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtNodeVectors:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtNodeVectorsPrivate:
 *
 * Private fields for #VisuGlExtNodeVectors objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtNodeVectorsPrivate
{
  gboolean dispose_has_run;
  gboolean isBuilt;
  gchar *propId;

  /* Node definitions. */
  VisuData *dataObj;
  VisuNodeProperty *prop;
  gulong popInc_signal, popDec_signal, posChg_signal, visChg_signal, eleSize_hook;

  /* Rendering definitions. */
  gboolean followElementColor;
  float arrow[N_ARROW_DEFS];
  float normFactor, scale;
  VisuGlArrowCentering centering;
  guint nodeOrig;
  float translation, addLength;
  float ratioMin, ratioMinLabel;
};

enum
  {
    PROP_0,
    PROPID_PROP,
    NGLOBJ_PROP
  };

static void visu_gl_ext_node_vectors_finalize(GObject* obj);
static void visu_gl_ext_node_vectors_dispose(GObject* obj);
static void visu_gl_ext_node_vectors_rebuild(VisuGlExt *ext);
static void visu_gl_ext_node_vectors_get_property(GObject* obj, guint property_id,
                                                  GValue *value, GParamSpec *pspec);
static void visu_gl_ext_node_vectors_set_property(GObject* obj, guint property_id,
                                                  const GValue *value, GParamSpec *pspec);

/* Local callbacks. */
static void onNodePopulationChanged(VisuNodeArray *array, gint *ids, gpointer data);
static void onNodePositionChanged(VisuNodeArray *array, VisuElement *ele, gpointer data);
static void onNodeVisibilityChanged(VisuNodeArray *array, gpointer data);
static gboolean onElementSize(GSignalInvocationHint *ihint, guint nvalues,
                              const GValue *param_values, gpointer data);

G_DEFINE_TYPE(VisuGlExtNodeVectors, visu_gl_ext_node_vectors, VISU_TYPE_GL_EXT)

static void visu_gl_ext_node_vectors_class_init(VisuGlExtNodeVectorsClass *klass)
{
  VisuConfigFileEntry *resourceEntry;

  DBG_fprintf(stderr, "Visu GlExt NodeVectors: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  /* Resources for geodiff extensions. */
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
                                            FLAG_RESOURCE_ARROW,
                                            DESC_RESOURCE_ARROW,
                                            1, readArrow);
  visu_config_file_entry_setVersion(resourceEntry, 3.5f);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
                                            FLAG_RESOURCE_RATIO_MIN,
                                            DESC_RESOURCE_RATIO_MIN,
                                            1, readRatioMin);
  visu_config_file_entry_setVersion(resourceEntry, 3.5f);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
                                            FLAG_RESOURCE_RATIO_STR,
                                            DESC_RESOURCE_RATIO_STR,
                                            1, readRatioStr);
  visu_config_file_entry_setVersion(resourceEntry, 3.5f);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
                                            FLAG_RESOURCE_MULT,
                                            DESC_RESOURCE_MULT,
                                            1, readMult);
  visu_config_file_entry_setVersion(resourceEntry, 3.5f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                     exportResourcesDiff);

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_node_vectors_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_node_vectors_finalize;
  G_OBJECT_CLASS(klass)->set_property = visu_gl_ext_node_vectors_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_gl_ext_node_vectors_get_property;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_node_vectors_rebuild;

  /**
   * VisuGlExtNodeVectors::propId:
   *
   * The name of the extension (used as an id).
   *
   * Since: 3.7
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), PROPID_PROP,
     g_param_spec_string("propId", "Property id", "property name to get vectors from", "",
                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE |
                         G_PARAM_STATIC_STRINGS));
}

static void visu_gl_ext_node_vectors_init(VisuGlExtNodeVectors *obj)
{
  DBG_fprintf(stderr, "Visu GlExt NodeVectors: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = g_malloc(sizeof(VisuGlExtNodeVectorsPrivate));
  obj->priv->dispose_has_run = FALSE;

  /* Overload parent data. */
  visu_gl_ext_setPriority(VISU_GL_EXT(obj), VISU_GL_EXT_PRIORITY_NODE_DECORATIONS);

  /* Private data. */
  obj->priv->isBuilt       = FALSE;
  obj->priv->propId        = (gchar*)0;
  obj->priv->dataObj       = (VisuData*)0;
  obj->priv->popInc_signal = 0;
  obj->priv->popDec_signal = 0;
  obj->priv->posChg_signal = 0;
  obj->priv->visChg_signal = 0;
  obj->priv->followElementColor = TRUE;
  obj->priv->arrow[TAIL_LENGTH] = 0.7f;
  obj->priv->arrow[TAIL_RADIUS] = 0.1f;
  obj->priv->arrow[TAIL_NLAT]   = 10.f;
  obj->priv->arrow[HEAD_LENGTH] = 0.3f;
  obj->priv->arrow[HEAD_RADIUS] = 0.15f;
  obj->priv->arrow[HEAD_NLAT]   = 10.f;
  obj->priv->normFactor         = -1.f;
  obj->priv->scale              = -2.f;
  obj->priv->centering          = VISU_GL_ARROW_BOTTOM_CENTERED;
  obj->priv->translation        = 0.f;
  obj->priv->addLength          = 0.f;
  obj->priv->ratioMin           = 0.f;
  obj->priv->ratioMinLabel      = G_MAXFLOAT;
  obj->priv->nodeOrig           = 0;
  obj->priv->eleSize_hook       = 0;
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_gl_ext_node_vectors_dispose(GObject* obj)
{
  VisuGlExtNodeVectors *vect;

  DBG_fprintf(stderr, "Visu GlExt NodeVectors: dispose object %p.\n", (gpointer)obj);

  vect = VISU_GL_EXT_NODE_VECTORS(obj);
  if (vect->priv->dispose_has_run)
    return;
  vect->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_node_vectors_setData(vect, (VisuData*)0);
  if (vect->priv->eleSize_hook)
    g_signal_remove_emission_hook(g_signal_lookup("elementSizeChanged", VISU_TYPE_RENDERING),
                                  vect->priv->eleSize_hook);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_node_vectors_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_gl_ext_node_vectors_finalize(GObject* obj)
{
  VisuGlExtNodeVectors *vect;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Visu GlExt NodeVectors: finalize object %p.\n", (gpointer)obj);

  vect = VISU_GL_EXT_NODE_VECTORS(obj);

  /* Free privs elements. */
  if (vect->priv)
    {
      DBG_fprintf(stderr, "Visu GlExt NodeVectors: free private legend.\n");
      g_free(vect->priv->propId);
      g_free(vect->priv);
    }

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Visu GlExt NodeVectors: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_node_vectors_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Visu GlExt NodeVectors: freeing ... OK.\n");
}
static void visu_gl_ext_node_vectors_get_property(GObject* obj, guint property_id,
                                                  GValue *value, GParamSpec *pspec)
{
  VisuGlExtNodeVectorsPrivate *self = VISU_GL_EXT_NODE_VECTORS(obj)->priv;

  DBG_fprintf(stderr, "Extension NodeVectors: get property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case PROPID_PROP:
      g_value_set_string(value, self->propId);
      DBG_fprintf(stderr, "%s.\n", self->propId);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_gl_ext_node_vectors_set_property(GObject* obj, guint property_id,
                                                  const GValue *value, GParamSpec *pspec)
{
  VisuGlExtNodeVectorsPrivate *self = VISU_GL_EXT_NODE_VECTORS(obj)->priv;

  DBG_fprintf(stderr, "Extension NodeVectors: set property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case PROPID_PROP:
      self->propId = g_value_dup_string(value);
      DBG_fprintf(stderr, "%s.\n", self->propId);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}

/**
 * visu_gl_ext_node_vectors_new:
 * @name: (allow-none): the name to give to the extension.
 * @propId: the name of the #VisuNodeProperty that stores the vector representation.
 *
 * Creates a new #VisuGlExt to draw a box.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtNodeVectors* visu_gl_ext_node_vectors_new(const gchar *name, const gchar *propId)
{
  char *name_ = "Node vectors";
  char *description = _("Draw vectors on each nodes.");
  VisuGlExt *ext;

  DBG_fprintf(stderr,"Visu GlExt NodeVectors: new object.\n");
  
  ext = VISU_GL_EXT(g_object_new(VISU_TYPE_GL_EXT_NODE_VECTORS,
                                 "name", (name)?name:name_, "label", _(name),
                                 "description", description,
                                 "nGlObj", 1, "propId", propId, NULL));

  return VISU_GL_EXT_NODE_VECTORS(ext);
}
/**
 * visu_gl_ext_node_vectors_setData:
 * @vect: The #VisuGlExtNodeVectors to attached to.
 * @dataObj: the nodes to get the population of.
 *
 * Attach an #VisuGlView to render to and setup the legend to get the
 * node population also.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_node_vectors_draw() should be called and
 * then 'OpenGLAskForReDraw' signal be emitted.
 **/
gboolean visu_gl_ext_node_vectors_setData(VisuGlExtNodeVectors *vect, VisuData *dataObj)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_NODE_VECTORS(vect), FALSE);

  DBG_fprintf(stderr, "Extension Node Vectors: set VisuData %p.\n", (gpointer)dataObj);
  if (vect->priv->dataObj)
    {
      g_signal_handler_disconnect(G_OBJECT(vect->priv->dataObj), vect->priv->popInc_signal);
      g_signal_handler_disconnect(G_OBJECT(vect->priv->dataObj), vect->priv->popDec_signal);
      g_signal_handler_disconnect(G_OBJECT(vect->priv->dataObj), vect->priv->posChg_signal);
      g_signal_handler_disconnect(G_OBJECT(vect->priv->dataObj), vect->priv->visChg_signal);
      g_object_unref(vect->priv->dataObj);
    }
  vect->priv->prop = (dataObj)?
    visu_node_array_getProperty(VISU_NODE_ARRAY(dataObj), vect->priv->propId):
    (VisuNodeProperty*)0;
  if (dataObj && vect->priv->prop)
    {
      g_object_ref(dataObj);
      vect->priv->popInc_signal =
        g_signal_connect(G_OBJECT(dataObj), "PopulationIncrease",
                         G_CALLBACK(onNodePopulationChanged), (gpointer)vect);
      vect->priv->popDec_signal =
        g_signal_connect(G_OBJECT(dataObj), "PopulationDecrease",
                         G_CALLBACK(onNodePopulationChanged), (gpointer)vect);
      vect->priv->posChg_signal =
        g_signal_connect(G_OBJECT(dataObj), "PositionChanged",
                         G_CALLBACK(onNodePositionChanged), (gpointer)vect);
      vect->priv->visChg_signal =
        g_signal_connect(G_OBJECT(dataObj), "VisibilityChanged",
                         G_CALLBACK(onNodeVisibilityChanged), (gpointer)vect);
      vect->priv->dataObj = dataObj;
    }
  else
    {
      vect->priv->popInc_signal = 0;
      vect->priv->popDec_signal = 0;
      vect->priv->posChg_signal = 0;
      vect->priv->visChg_signal = 0;
      vect->priv->dataObj = (VisuData*)0;
    }

  vect->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(vect));
}
/**
 * visu_gl_ext_node_vectors_getData:
 * @vect: a #VisuGlExtNodeVectors object.
 *
 * Retrieves associated #VisuData, if any.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuData object.
 **/
VisuData* visu_gl_ext_node_vectors_getData(VisuGlExtNodeVectors *vect)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_NODE_VECTORS(vect), (VisuData*)0);

  return vect->priv->dataObj;
}
/**
 * visu_gl_ext_node_vectors_setRenderedSize:
 * @vect: the #VisuGlExtNodeVectors object to modify.
 * @scale: a floating point value.
 *
 * @scale governs how large the node vectors are drawn. For a positive
 * value, a vector with a normalised size of 1 (see
 * visu_gl_ext_node_vectors_setNormalisation()) will be drawn with the size of
 * @scale. For a negative value, a vector of normalised size of 1 will
 * be drawn with a size of -@scale times the maximum element size (see
 * visu_node_array_getMaxElementSize()).
 *
 * Since: 3.7
 *
 * Returns: TRUE if setting has been changed.
 **/
gboolean visu_gl_ext_node_vectors_setRenderedSize(VisuGlExtNodeVectors *vect, gfloat scale)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_NODE_VECTORS(vect), FALSE);

  if (vect->priv->scale == scale)
    return FALSE;

  vect->priv->scale = scale;

  if (vect->priv->eleSize_hook)
    g_signal_remove_emission_hook(g_signal_lookup("elementSizeChanged", VISU_TYPE_RENDERING),
                                  vect->priv->eleSize_hook);  
  if (vect->priv->scale <= 0.f)
    vect->priv->eleSize_hook =
      g_signal_add_emission_hook(g_signal_lookup("elementSizeChanged",
                                                 VISU_TYPE_RENDERING),
                                 0, onElementSize, (gpointer)vect, (GDestroyNotify)0);
  else
    vect->priv->eleSize_hook = 0;
  
  vect->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(vect));
}
/**
 * visu_gl_ext_node_vectors_setNormalisation:
 * @vect: the #VisuGlExtNodeVectors object to modify.
 * @norm: a floating point value.
 *
 * @norm governs how the input node vector field is normalised. With a
 * positive value, all node vectors will be normalised by @norm. With
 * a negative value, all node vectors are normalised with respect to
 * the biggest one.
 *
 * Since: 3.7
 *
 * Returns: TRUE if setting has been changed.
 **/
gboolean visu_gl_ext_node_vectors_setNormalisation(VisuGlExtNodeVectors *vect, float norm)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_NODE_VECTORS(vect), FALSE);

  if (vect->priv->normFactor == norm)
    return FALSE;

  vect->priv->normFactor = norm;
  
  vect->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(vect));
}
/**
 * visu_gl_ext_node_vectors_setTranslation:
 * @vect: the #VisuGlExtNodeVectors object to modify.
 * @trans: a positive floating point value.
 *
 * Defines a translation with respect to the center of each node. The
 * vector is shifted outwards, following the vector direction by an
 * amount given by the product of @trans and the element size
 * currently drawn.
 *
 * Since: 3.7
 *
 * Returns: TRUE if setting has been changed.
 **/
gboolean visu_gl_ext_node_vectors_setTranslation(VisuGlExtNodeVectors *vect, float trans)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_NODE_VECTORS(vect), FALSE);

  if (vect->priv->translation == trans)
    return FALSE;

  vect->priv->translation = MAX(0.f, trans);

  if (vect->priv->eleSize_hook)
    g_signal_remove_emission_hook(g_signal_lookup("elementSizeChanged", VISU_TYPE_RENDERING),
                                  vect->priv->eleSize_hook);  
  if (vect->priv->translation > 0.f)
    vect->priv->eleSize_hook =
      g_signal_add_emission_hook(g_signal_lookup("elementSizeChanged",
                                                 VISU_TYPE_RENDERING),
                                 0, onElementSize, (gpointer)vect, (GDestroyNotify)0);
  else
    vect->priv->eleSize_hook = 0;

  vect->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(vect));
}
/**
 * visu_gl_ext_node_vectors_setColor:
 * @vect: the #VisuGlExtNodeVectors object to modify.
 * @follow: a boolean.
 *
 * If @follow is TRUE, the vectors are drawn with the color of the
 * currently drawn #VisuElement. If FALSE, it is drawn with a
 * highlighted color.
 *
 * Since: 3.7
 *
 * Returns: TRUE if setting has been changed.
 **/
gboolean visu_gl_ext_node_vectors_setColor(VisuGlExtNodeVectors *vect, gboolean follow)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_NODE_VECTORS(vect), FALSE);

  if (vect->priv->followElementColor == follow)
    return FALSE;

  vect->priv->followElementColor = follow;
  
  vect->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(vect));
}
/**
 * visu_gl_ext_node_vectors_setCentering:
 * @vect: the #VisuGlExtNodeVectors object to modify.
 * @centering: a #VisuGlArrowCentering id.
 *
 * Change how vectors are position with respect to to center of each node.
 *
 * Since: 3.7
 *
 * Returns: TRUE if setting has been changed.
 **/
gboolean visu_gl_ext_node_vectors_setCentering(VisuGlExtNodeVectors *vect,
                                               VisuGlArrowCentering centering)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_NODE_VECTORS(vect), FALSE);

  if (vect->priv->centering == centering)
    return FALSE;

  vect->priv->centering = centering;
  
  vect->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(vect));
}
/**
 * visu_gl_ext_node_vectors_setVectorThreshold:
 * @vect: the #VisuGlExtNodeVectors object to modify.
 * @val: a value.
 *
 * Vectors are indeed drawn if a threshold value is reach. If @val is
 * strictly positive, the norm of each vector is compared to @val. If
 * @val is negative, the normalised [0;1] norm is compared to -@val.
 *
 * Since: 3.7
 *
 * Returns: TRUE if setting has been changed.
 **/
gboolean visu_gl_ext_node_vectors_setVectorThreshold(VisuGlExtNodeVectors *vect, float val)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_NODE_VECTORS(vect), FALSE);

  if (vect->priv->ratioMin == val)
    return FALSE;

  vect->priv->ratioMin = val;
  
  vect->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(vect));
}
/**
 * visu_gl_ext_node_vectors_setLabelThreshold:
 * @vect: the #VisuGlExtNodeVectors object to modify.
 * @val: a value.
 *
 * Vector norms can be drawn if a threshold value is reach. If @val is
 * strictly positive, the norm of each vector is compared to @val. If
 * @val is negative, the normalised [0;1] norm is compared to -@val.
 *
 * Since: 3.7
 *
 * Returns: TRUE if setting has been changed.
 **/
gboolean visu_gl_ext_node_vectors_setLabelThreshold(VisuGlExtNodeVectors *vect, float val)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_NODE_VECTORS(vect), FALSE);

  if (vect->priv->ratioMinLabel == val)
    return FALSE;

  vect->priv->ratioMinLabel = val;
  
  vect->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(vect));
}
/**
 * visu_gl_ext_node_vectors_setArrow:
 * @vect: the #VisuGlExtNodeVectors object to modify.
 * @tailLength: the length for the tail part of the vector.
 * @tailRadius: the radius for the tail part of the vector.
 * @tailN: the number of polygons to draw the tail part of the vector.
 * @headLength: the length for the head part of the vector.
 * @headRadius: the radius for the head part of the vector.
 * @headN: the number of polygons to draw the head part of the vector.
 *
 * Defines the profile of the arrows representing the vectors.
 *
 * Since: 3.7
 *
 * Returns: TRUE if setting has been changed.
 **/
gboolean visu_gl_ext_node_vectors_setArrow(VisuGlExtNodeVectors *vect, float tailLength,
                                           float tailRadius, guint tailN,
                                           float headLength, float headRadius, guint headN)
{
  gboolean diff;
  float fact;

  g_return_val_if_fail(VISU_IS_GL_EXT_NODE_VECTORS(vect), FALSE);

  diff = FALSE;
  diff = diff || (vect->priv->arrow[TAIL_LENGTH] != tailLength);
  diff = diff || (vect->priv->arrow[TAIL_RADIUS] != tailRadius);
  diff = diff || (vect->priv->arrow[TAIL_NLAT] != tailN);
  diff = diff || (vect->priv->arrow[HEAD_LENGTH] != headLength);
  diff = diff || (vect->priv->arrow[HEAD_RADIUS] != headRadius);
  diff = diff || (vect->priv->arrow[HEAD_NLAT] != headN);
  if (!diff)
    return FALSE;

  fact = 1.f / (tailLength + headLength);
  vect->priv->arrow[TAIL_LENGTH] = tailLength * fact;
  vect->priv->arrow[TAIL_RADIUS] = tailRadius * fact;
  vect->priv->arrow[TAIL_NLAT] = tailN;
  vect->priv->arrow[HEAD_LENGTH] = headLength * fact;
  vect->priv->arrow[HEAD_RADIUS] = headRadius * fact;
  vect->priv->arrow[HEAD_NLAT] = headN;
  
  vect->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(vect));
}

/**
 * visu_gl_ext_node_vectors_getScale:
 *
 * The forces can be scaled manually or automatically, see
 * visu_gl_ext_forces_setScale() and visu_rendering_atomic_drawForces().
 *
 * Since: 3.7
 *
 * Returns: the scaling factor, -1 if automatic.
 **/
/* gfloat visu_gl_ext_forces_getScale() */
/* { */
/*   return scale; */
/* } */
/**
 * visu_gl_ext_node_vectors_getNormalisation:
 * @vect: the #VisuGlExtNodeVectors object to inquire.
 *
 * Gets the normalisation factor, see visu_gl_ext_node_vectors_setNormalisation().
 *
 * Since: 3.7
 *
 * Returns: the normalisation factor used by @vact.
 **/
float visu_gl_ext_node_vectors_getNormalisation(VisuGlExtNodeVectors *vect)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_NODE_VECTORS(vect), -1.f);
  
  return vect->priv->normFactor;
}

/********************/
/* Local callbacks. */
/********************/
static void onNodePopulationChanged(VisuNodeArray *array _U_, gint *ids _U_, gpointer data)
{
  VISU_GL_EXT_NODE_VECTORS(data)->priv->isBuilt = FALSE;
  visu_gl_ext_node_vectors_draw(VISU_GL_EXT_NODE_VECTORS(data));
}
static void onNodePositionChanged(VisuNodeArray *array _U_, VisuElement *ele _U_, gpointer data)
{
  VISU_GL_EXT_NODE_VECTORS(data)->priv->isBuilt = FALSE;
  visu_gl_ext_node_vectors_draw(VISU_GL_EXT_NODE_VECTORS(data));
}
static void onNodeVisibilityChanged(VisuNodeArray *array _U_, gpointer data)
{
  VISU_GL_EXT_NODE_VECTORS(data)->priv->isBuilt = FALSE;
  visu_gl_ext_node_vectors_draw(VISU_GL_EXT_NODE_VECTORS(data));
}
static gboolean onElementSize(GSignalInvocationHint *ihint _U_, guint nvalues _U_,
                              const GValue *param_values _U_, gpointer data)
{
  VISU_GL_EXT_NODE_VECTORS(data)->priv->isBuilt = FALSE;
  visu_gl_ext_node_vectors_draw(VISU_GL_EXT_NODE_VECTORS(data));

  return TRUE;
}

/***********/
/* OpenGL. */
/***********/

/**
 * visu_gl_ext_node_vectors_draw:
 * @vect: a #VisuGlExtNodeVectors object.
 *
 * Compile the OpenGL list that draw arrows on nodes.
 *
 * Since: 3.7
 **/
void visu_gl_ext_node_vectors_draw(VisuGlExtNodeVectors *vect)
{
  VisuRendering *method;
  VisuNodeArray *nodes;
  VisuNodeArrayIter iter;
  GValue val = {0, {{0}, {0}}};
  GLUquadricObj *obj;
  float *values, fact, scale, eleSize;
  float rMult, r, sMult, s, l, headLength, tailLength;
  float xyz[3];
  gchar *maxPropId;
  char distStr[10];

  /* Nothing to draw; */
  g_return_if_fail(VISU_IS_GL_EXT_NODE_VECTORS(vect));
  if(!visu_gl_ext_getActive(VISU_GL_EXT(vect)) || vect->priv->isBuilt) return;
  glDeleteLists(visu_gl_ext_getGlList(VISU_GL_EXT(vect)), 1);

  vect->priv->isBuilt = TRUE;

  DBG_fprintf(stderr, "Extension Node Vectors: building for nodes %p.\n",
              (gpointer)vect->priv->dataObj);
  if (!vect->priv->dataObj)
    return;

  obj = gluNewQuadric();
  method = visu_object_getRendering(VISU_OBJECT_INSTANCE);
  nodes = VISU_NODE_ARRAY(vect->priv->dataObj);

  /* Global min / max. */
  maxPropId = g_strdup_printf("max_%s", vect->priv->propId);
  values = (float*)g_object_get_data(G_OBJECT(nodes), maxPropId);
  g_free(maxPropId);
  g_return_if_fail(values);

  /* Normalization factor. */
  fact = vect->priv->normFactor;
  if (fact <= 0.f)
    fact = values[1];
  fact = 1.f / fact;

  /* Drawing rule for vectors. */
  rMult = 1.f;
  r     = 1.f;
  if (vect->priv->ratioMin <= 0.f)
    {
      rMult = fact;
      r = -1.f;
    }
  /* Drawing rule for label. */
  sMult = 1.f;
  s     = 1.f;
  if (vect->priv->ratioMinLabel <= 0.f)
    {
      sMult = fact;
      s = -1.f;
    }

  /* Maximum representation size. */
  scale = vect->priv->scale;
  if (scale <= 0.f)
    scale = visu_node_array_getMaxElementSize(nodes) * (-scale);

  glNewList(visu_gl_ext_getGlList(VISU_GL_EXT(vect)), GL_COMPILE);

  eleSize = 0.f;
  g_value_init(&val, G_TYPE_POINTER);
  visu_node_array_iterNew(nodes, &iter);
  for(visu_node_array_iterStart(nodes, &iter); iter.element;
      visu_node_array_iterNextElement(nodes, &iter))
    {
      if (!visu_element_getRendered(iter.element))
        continue;

      if (vect->priv->followElementColor)
        glCallList(visu_element_getMaterialId(iter.element));
      else
        visu_gl_setHighlightColor(iter.element->material,
                                  iter.element->rgb, 1.f);
      eleSize = visu_rendering_getSizeOfElement(method, iter.element);

      for(visu_node_array_iterRestartNode(nodes, &iter); iter.node;
          visu_node_array_iterNextNode(nodes, &iter))
        {
          if (!iter.node->rendered)
            continue;

          visu_node_property_getValue(vect->priv->prop, iter.node, &val);
          values = (float*)g_value_get_pointer(&val);

          if (values[3] * rMult <= vect->priv->ratioMin * r)
            continue;
          l = values[3] * fact;

          visu_data_getNodePosition(vect->priv->dataObj, iter.node, xyz);

          glPushMatrix();
          glTranslatef(xyz[0] - ((vect->priv->nodeOrig)?values[vect->priv->nodeOrig + 0]:0.f),
                       xyz[1] - ((vect->priv->nodeOrig)?values[vect->priv->nodeOrig + 1]:0.f),
                       xyz[2] - ((vect->priv->nodeOrig)?values[vect->priv->nodeOrig + 2]:0.f));
          glRotated(values[5], 0, 0, 1);  
          glRotated(values[4], 0, 1, 0);
          glTranslated(0.f, 0.f, eleSize * vect->priv->translation);
          /* We draw the arrow. */
          switch (vect->priv->centering)
            {
            case (VISU_GL_ARROW_CENTERED):
              glScalef(scale, scale, scale);
              tailLength = MAX(0.f, l - vect->priv->arrow[HEAD_LENGTH]);
              headLength = MIN(l, vect->priv->arrow[HEAD_LENGTH]);
              break;
            case (VISU_GL_ARROW_TAIL_CENTERED):
              glScalef(eleSize, eleSize, eleSize);
              tailLength = l * scale / eleSize + vect->priv->addLength;
              headLength = vect->priv->arrow[HEAD_LENGTH];
              break;
            default:
              glScalef(l * scale, l * scale, l * scale);
              tailLength = vect->priv->arrow[TAIL_LENGTH];
              headLength = vect->priv->arrow[HEAD_LENGTH];
            }
          visu_gl_drawSmoothArrow(obj, -1, vect->priv->centering,
                                  tailLength,
                                  vect->priv->arrow[TAIL_RADIUS],
                                  (guint)vect->priv->arrow[TAIL_NLAT], FALSE,
                                  headLength,
                                  vect->priv->arrow[HEAD_RADIUS],
                                  (guint)vect->priv->arrow[HEAD_NLAT], FALSE);
          /* We draw the value. */
          if (values[3] * sMult > vect->priv->ratioMinLabel * s)
            {
              glRasterPos3f(0.f, 0.f, 0.f);
              sprintf(distStr, "%6.3f", values[3]);
              visu_gl_text_drawChars(distStr, VISU_GL_TEXT_NORMAL);
            }
          glPopMatrix();
        }
    }

  glEndList();

  gluDeleteQuadric(obj);
}
static void visu_gl_ext_node_vectors_rebuild(VisuGlExt *ext)
{
  DBG_fprintf(stderr, "Visu GlExt NodeVectors: rebuilding vectors %p.\n", (gpointer)ext);
  visu_gl_text_rebuildFontList();
  VISU_GL_EXT_NODE_VECTORS(ext)->priv->isBuilt = FALSE;
  visu_gl_ext_node_vectors_draw(VISU_GL_EXT_NODE_VECTORS(ext));
}



/**********************/
/* Geodiff extension. */
/**********************/
#include <extraFunctions/geometry.h>
static float ratioMin = DEFT_RESOURCE_RATIO_MIN;
static float ratioStr = DEFT_RESOURCE_RATIO_STR;
static float mult     = DEFT_RESOURCE_MULT;
static float scal     = DEFT_RESOURCE_SCAL;
static float arrow[4] = {4.f, .2f, .5f, .3f};

static VisuGlExtNodeVectors *geoDiff = NULL;
/**
 * visu_gl_ext_geodiff_getDefault:
 *
 * V_Sim has a private internal #VisuGlExtNodeVectors object to
 * represent geometry differences.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): the default #VisuGlExtNodeVectors object
 * used for geometry differences.
 **/
VisuGlExtNodeVectors* visu_gl_ext_geodiff_getDefault()
{
  if (!geoDiff)
    {
      geoDiff = visu_gl_ext_node_vectors_new("Geometry", VISU_GEODIFF_ID);
      visu_gl_ext_node_vectors_setCentering(geoDiff, VISU_GL_ARROW_CENTERED);
      visu_gl_ext_node_vectors_setRenderedSize(geoDiff, scal);
      visu_gl_ext_node_vectors_setNormalisation(geoDiff, mult);
      visu_gl_ext_node_vectors_setColor(geoDiff, FALSE);
      visu_gl_ext_node_vectors_setArrow(geoDiff, arrow[0], arrow[1], 10,
                                        arrow[2], arrow[3], 10);
    }
  return geoDiff;
}
static gboolean readArrow(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			  VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  float vals[4];

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readFloat(lines[0], position, vals, 4, error))
    return FALSE;
  arrow[0] = (vals[0] > 0.f)?vals[0]:4.f;
  arrow[1] = (vals[1] > 0.f)?vals[1]:.2f;
  arrow[2] = (vals[2] > 0.f)?vals[2]:.5f;
  arrow[3] = (vals[3] > 0.f)?vals[3]:.3f;
  visu_gl_ext_node_vectors_setArrow(visu_gl_ext_geodiff_getDefault(),
                                    arrow[0], arrow[1], 10,
                                    arrow[2], arrow[3], 10);
  visu_gl_ext_node_vectors_setRenderedSize(visu_gl_ext_geodiff_getDefault(),
                                           (vals[0] > 0.f)?vals[0]:-4.f);

  return TRUE;
}
static gboolean readRatioMin(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			     VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  float val;

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readFloat(lines[0], position, &val, 1, error))
    return FALSE;
  visu_gl_ext_node_vectors_setVectorThreshold(visu_gl_ext_geodiff_getDefault(),
                                              val);
  ratioMin = val;

  return TRUE;
}
static gboolean readRatioStr(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			     VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  float val;

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readFloat(lines[0], position, &val, 1, error))
    return FALSE;
  visu_gl_ext_node_vectors_setLabelThreshold(visu_gl_ext_geodiff_getDefault(),
                                             val);
  ratioStr = val;

  return TRUE;
}
static gboolean readMult(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			 VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  float val;

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readFloat(lines[0], position, &val, 1, error))
    return FALSE;
  visu_gl_ext_node_vectors_setNormalisation(visu_gl_ext_geodiff_getDefault(),
                                            val);
  mult = val;

  return TRUE;
}
static void exportResourcesDiff(GString *data, VisuData *dataObj _U_, VisuGlView *view _U_)
{
  visu_config_file_exportComment(data, DESC_RESOURCE_ARROW);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_ARROW, NULL,
                               "%f %f %f %f", arrow[0], arrow[1], arrow[2], arrow[3]);

  visu_config_file_exportComment(data, DESC_RESOURCE_MULT);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_MULT, NULL,
                               "%f", mult);

  visu_config_file_exportComment(data, DESC_RESOURCE_RATIO_MIN);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_RATIO_MIN, NULL,
                               "%f", ratioMin);

  visu_config_file_exportComment(data, DESC_RESOURCE_RATIO_STR);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_RATIO_STR, NULL,
                               "%f", ratioStr);

  visu_config_file_exportComment(data, "");
}

/************************/
/* Vibration extension. */
/************************/
#include <extraFunctions/vibration.h>

static VisuGlExtNodeVectors *vib = NULL;
/**
 * visu_gl_ext_vibration_getDefault:
 *
 * V_Sim has a private internal #VisuGlExtNodeVectors object to
 * represent phonons with arrows.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): the default #VisuGlExtNodeVectors object
 * used for representing phonons with arrows.
 **/
VisuGlExtNodeVectors* visu_gl_ext_vibration_getDefault()
{
  if (!vib)
    {
      vib = visu_gl_ext_node_vectors_new("Vibration", VISU_VIBRATION_ID);
      visu_gl_ext_setActive(VISU_GL_EXT(vib), FALSE);
      visu_gl_ext_node_vectors_setCentering(vib, VISU_GL_ARROW_TAIL_CENTERED);
      visu_gl_ext_node_vectors_setRenderedSize(vib, 1.f);
      visu_gl_ext_node_vectors_setNormalisation(vib, -1.f);
      visu_gl_ext_node_vectors_setColor(vib, FALSE);
      visu_gl_ext_node_vectors_setArrow(vib, 0.5f, 0.2f, 10,
                                        0.5f, 0.3f, 10);
      visu_gl_ext_node_vectors_setVectorThreshold(vib, -0.05f); /* Value
                                                                   in percentage. */
      vib->priv->addLength = 2.5f;
      vib->priv->nodeOrig  = 9;
    }
  return vib;
}
