/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#include "planes.h"

#include <visu_object.h>
#include <openGLFunctions/objectList.h>
#include <coreTools/toolColor.h>

/**
 * SECTION:planes
 * @short_description: Draw a list of #VisuPlane.
 *
 * <para>This extension draws a list of #VisuPlane. Planes are
 * outlined with a black line and also the intersections of planes.</para>
 *
 * Since: 3.7
 */

/**
 * VisuGlExtPlanesClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtPlanesClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtPlanes:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtPlanesPrivate:
 *
 * Private fields for #VisuGlExtPlanes objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtPlanesPrivate
{
  gboolean dispose_has_run;
  gboolean isBuilt;

  /* Planes definition. */
  GList *planes;
};
static VisuGlExtPlanes* defaultPlanes;

typedef struct _PlaneHandleStruct
{
  VisuPlane *plane;
  gulong move_signal, rendering_signal;
} _PlaneHandle;

static void visu_gl_ext_planes_finalize(GObject* obj);
static void visu_gl_ext_planes_dispose(GObject* obj);
static void visu_gl_ext_planes_rebuild(VisuGlExt *ext);

/* Local callbacks. */
static void onPlaneMoved(VisuPlane *plane, gpointer data);
static void onPlaneRendering(VisuPlane *plane, gpointer data);

/* Local routines. */
static void _freePlaneHandle(gpointer obj)
{
  _PlaneHandle *phd;

  phd = (_PlaneHandle*)obj;
  g_signal_handler_disconnect(G_OBJECT(phd->plane), phd->move_signal);
  g_signal_handler_disconnect(G_OBJECT(phd->plane), phd->rendering_signal);
  g_object_unref(phd->plane);
#if GLIB_MINOR_VERSION > 9
  g_slice_free1(sizeof(_PlaneHandle), obj);
#else
  g_free(obj);
#endif
}
static gpointer _newPlaneHandle(VisuGlExtPlanes *planes, VisuPlane *plane)
{
  _PlaneHandle *phd;

  g_object_ref(plane);
#if GLIB_MINOR_VERSION > 9
  phd = g_slice_alloc(sizeof(_PlaneHandle));
#else
  phd = g_malloc(sizeof(_PlaneHandle));
#endif
  phd->plane = plane;
  phd->move_signal = g_signal_connect(G_OBJECT(plane), "moved",
                                      G_CALLBACK(onPlaneMoved), (gpointer)planes);
  phd->rendering_signal = g_signal_connect(G_OBJECT(plane), "rendering",
                                           G_CALLBACK(onPlaneRendering), (gpointer)planes);
  return (gpointer)phd;
}
static gint _cmpPlaneHandle(gconstpointer a, gconstpointer b)
{
  _PlaneHandle *phd_a = (_PlaneHandle*)a;
  
  if (phd_a->plane == b)
    return 0;
  return 1;
}
#define _getPlane(H) ((_PlaneHandle*)H)->plane

G_DEFINE_TYPE(VisuGlExtPlanes, visu_gl_ext_planes, VISU_TYPE_GL_EXT)

static void visu_gl_ext_planes_class_init(VisuGlExtPlanesClass *klass)
{
  DBG_fprintf(stderr, "Extension Planes: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  defaultPlanes = (VisuGlExtPlanes*)0;

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_planes_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_planes_finalize;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_planes_rebuild;
}

static void visu_gl_ext_planes_init(VisuGlExtPlanes *obj)
{
  DBG_fprintf(stderr, "Extension Planes: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = g_malloc(sizeof(VisuGlExtPlanesPrivate));
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->isBuilt     = FALSE;
  obj->priv->planes      = (GList*)0;
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_gl_ext_planes_dispose(GObject* obj)
{
  VisuGlExtPlanes *planes;
  GList *lst;

  DBG_fprintf(stderr, "Extension Planes: dispose object %p.\n", (gpointer)obj);

  planes = VISU_GL_EXT_PLANES(obj);
  if (planes->priv->dispose_has_run)
    return;
  planes->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  for (lst = planes->priv->planes; lst; lst = g_list_next(lst))
    _freePlaneHandle(lst->data);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_planes_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_gl_ext_planes_finalize(GObject* obj)
{
  VisuGlExtPlanes *planes;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Extension Planes: finalize object %p.\n", (gpointer)obj);

  planes = VISU_GL_EXT_PLANES(obj);

  /* Free privs elements. */
  if (planes->priv)
    {
      DBG_fprintf(stderr, "Extension Planes: free private planes.\n");
      g_list_free(planes->priv->planes);
      g_free(planes->priv);
    }

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Extension Planes: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_planes_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Extension Planes: freeing ... OK.\n");
}

/**
 * visu_gl_ext_planes_new:
 * @name: (allow-none): the name to give to the extension (default is #VISU_GL_EXT_PLANES_ID).
 *
 * Creates a new #VisuGlExt to draw a list of planes.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtPlanes* visu_gl_ext_planes_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_PLANES_ID;
  char *description = _("Draw some planes.");
  VisuGlExt *planes;
#define PLANES_HEIGHT 30

  DBG_fprintf(stderr,"Extension Planes: new object.\n");
  
  planes = VISU_GL_EXT(g_object_new(VISU_TYPE_GL_EXT_PLANES,
                                       "name", (name)?name:name_, "label", _(name),
                                       "description", description,
                                       "nGlObj", 1, NULL));
  visu_gl_ext_setPriority(planes, VISU_GL_EXT_PRIORITY_NORMAL + 1);
  visu_gl_ext_setSensitiveToRenderingMode(planes, TRUE);

  return VISU_GL_EXT_PLANES(planes);
}
/**
 * visu_gl_ext_planes_add:
 * @planes: a #VisuGlExtPlanes object.
 * @plane: (transfer full): a #VisuPlane object.
 *
 * Adds a @plane to the list of drawn planes.
 *
 * Since: 3.7
 *
 * Returns: FALSE if @plane was already registered.
 **/
gboolean visu_gl_ext_planes_add(VisuGlExtPlanes *planes, VisuPlane *plane)
{
  GList *lst;

  g_return_val_if_fail(VISU_IS_GL_EXT_PLANES(planes), FALSE);

  lst = g_list_find_custom(planes->priv->planes, plane, _cmpPlaneHandle);
  if (lst)
    return FALSE;

  planes->priv->planes = g_list_prepend(planes->priv->planes,
                                        _newPlaneHandle(planes, plane));

  planes->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(planes));
}
/**
 * visu_gl_ext_planes_remove:
 * @planes: a #VisuGlExtPlanes object.
 * @plane: a #VisuPlane object.
 *
 * Remove @plane from the list of drawn planes.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_planes_draw() should be called.
 **/
gboolean visu_gl_ext_planes_remove(VisuGlExtPlanes *planes, VisuPlane *plane)
{
  GList *lst;

  g_return_val_if_fail(VISU_IS_GL_EXT_PLANES(planes), FALSE);
  
  lst = g_list_find_custom(planes->priv->planes, plane, _cmpPlaneHandle);
  if (!lst)
    return FALSE;

  _freePlaneHandle(lst->data);
  planes->priv->planes = g_list_delete_link(planes->priv->planes, lst);

  planes->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(planes));
}

/**
 * visu_gl_ext_planes_getDefault:
 *
 * V_Sim is using a default planes object.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuGlExtPlanes object used by default.
 **/
VisuGlExtPlanes* visu_gl_ext_planes_getDefault()
{
  if (!defaultPlanes)
    {
      defaultPlanes = visu_gl_ext_planes_new((gchar*)0);
      visu_gl_ext_setActive(VISU_GL_EXT(defaultPlanes), FALSE);
    }
  return defaultPlanes;
}

static void visu_gl_ext_planes_rebuild(VisuGlExt *ext)
{
  VISU_GL_EXT_PLANES(ext)->priv->isBuilt = FALSE;
  visu_gl_ext_planes_draw(VISU_GL_EXT_PLANES(ext));
}

static void onPlaneMoved(VisuPlane *plane, gpointer data)
{
  VisuGlExtPlanes *planes;
  
  planes = VISU_GL_EXT_PLANES(data);
  planes->priv->isBuilt = !visu_plane_getRendered(plane);
  visu_gl_ext_planes_draw(planes);
}
static void onPlaneRendering(VisuPlane *plane, gpointer data)
{
  VisuGlExtPlanes *planes;
  
  planes = VISU_GL_EXT_PLANES(data);
  planes->priv->isBuilt = !visu_plane_getRendered(plane);
  visu_gl_ext_planes_draw(planes);
}

static void visu_plane_draw(VisuPlane* plane)
{
  GList *inter, *tmpLst;

  inter = visu_plane_getIntersection(plane);
  if (inter && visu_plane_getRendered(plane))
    {
      DBG_fprintf(stderr, " | plane %p\n", (gpointer)plane);
      glLineWidth(1.f);
      glColor3f(0.f, 0.f, 0.f);
      glBegin(GL_LINE_LOOP);
      for (tmpLst = inter; tmpLst; tmpLst = g_list_next(tmpLst))
        glVertex3fv((float*)tmpLst->data);
      glEnd();

      glDisable(GL_CULL_FACE);

      glColor4fv(visu_plane_getColor(plane)->rgba);
      glBegin(GL_POLYGON);
      for (tmpLst = inter; tmpLst; tmpLst = g_list_next(tmpLst))
        glVertex3fv((float*)tmpLst->data);
      glEnd();

      glEnable(GL_CULL_FACE);
      glCullFace(GL_BACK);
    }
}
/**
 * visu_gl_ext_planes_draw:
 * @planes: a #VisuGlExtPlanes object.
 *
 * Compile the OpenGL list representing planes.
 *
 * Since: 3.7
 **/
void visu_gl_ext_planes_draw(VisuGlExtPlanes *planes)
{
  float A[3], B[3];
  GList *lst1, *lst2;

  g_return_if_fail(VISU_IS_GL_EXT_PLANES(planes));

  glDeleteLists(visu_gl_ext_getGlList(VISU_GL_EXT(planes)), 1);

  if (!planes->priv->planes)
    return;

  glNewList(visu_gl_ext_getGlList(VISU_GL_EXT(planes)), GL_COMPILE);

  /* Set blend if not present. */
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_LIGHTING);
  glDisable(GL_DITHER);

  glLineWidth(1.f);
  glColor3f(0.f, 0.f, 0.f);
  glBegin(GL_LINES);
  for (lst1 = planes->priv->planes; lst1; lst1 = g_list_next(lst1))
    for (lst2 = lst1->next; lst2; lst2 = g_list_next(lst2))
      if (visu_plane_getRendered(_getPlane(lst1->data)) &&
          visu_plane_getRendered(_getPlane(lst2->data)) &&
          visu_plane_getPlaneIntersection(_getPlane(lst1->data),
                                          _getPlane(lst2->data), A, B))
        {
          glVertex3fv(A);
          glVertex3fv(B);
        }
  glEnd();

  DBG_fprintf(stderr, "VisuPlane: drawing list of planes.\n");
  for (lst1 = planes->priv->planes; lst1; lst1 = g_list_next(lst1))
    if (visu_plane_getColor(_getPlane(lst1->data))->rgba[3] == 1.f)
      visu_plane_draw(_getPlane(lst1->data));
  for (lst1 = planes->priv->planes; lst1; lst1 = g_list_next(lst1))
    if (visu_plane_getColor(_getPlane(lst1->data))->rgba[3] < 1.f)
      visu_plane_draw(_getPlane(lst1->data));

  glEnable(GL_LIGHTING);
  glEnable(GL_DITHER); /* WARNING: it is the default! */
  glEndList();
}
