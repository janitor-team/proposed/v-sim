/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "fogAndBGColor.h"

#include <string.h>

#include <opengl.h>
#include <visu_object.h>
#include <visu_tools.h>
#include <visu_configFile.h>
#include <openGLFunctions/objectList.h>
#include <openGLFunctions/text.h>
#include <coreTools/toolColor.h>
#include <coreTools/toolConfigFile.h>

#include <GL/gl.h>
#include <GL/glu.h>

/**
 * SECTION:fogAndBGColor
 * @short_description: Handle the background colour and the fog.
 *
 * This module is used to support a background colour and to tune the
 * fog. This last one can be turn on or off and its colour can be
 * either a user defined one or the one of the background. The fog is
 * a linear blending into the fog colour. It starts at a given z
 * position (in the camera basis set) and ends at a lower z.
 */

/* Parameters & resources*/
/* A resource to control the color of the background. */
#define FLAG_RESOURCE_BG_COLOR   "backgroundColor_color"
#define DESC_RESOURCE_BG_COLOR   "Set the background of the background ; four floating point values (0. <= v <= 1.)"
static float bgRGBDefault[4] = {0., 0., 0., 1.};
static void exportResourcesBg(GString *data, VisuData *dataObj, VisuGlView *view);

/**
 * VisuGlExtBgClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtBgClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtBg:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtBgPrivate:
 *
 * Private fields for #VisuGlExtBg objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtBgPrivate
{
  gboolean dispose_has_run;
  gboolean isBuilt;

  /* Handling the background color. */
  float bgRGB[4];

  /* Handling the background image. */
  guchar *bgImage;
  gboolean bgImageAlpha, bgImageFit, bgImageFollowZoom;
  guint bgImageW, bgImageH;
  gchar *bgImageTitle;
  float bgImageZoomInit, bgImageZoom, bgImageZoomRatioInit;
  float bgImageXsInit, bgImageXs, bgImageXs0;
  float bgImageYsInit, bgImageYs, bgImageYs0;

  /* Signals for the current view. */
  VisuGlView *view;
  gulong widthHeight_signal;
};
static GLuint texName = 0;

static VisuGlExtBg* defaultBg;

static void visu_gl_ext_bg_finalize(GObject* obj);
static void visu_gl_ext_bg_dispose(GObject* obj);
static void visu_gl_ext_bg_rebuild(VisuGlExt *ext);

/* Local callbacks */
static void onBgImageRescale(VisuGlView *view, gpointer data);
static void onEntryColor(VisuGlExtBg *bg, gchar *key, VisuObject *obj);

/* Local methods */
static void createBgImage(VisuGlExtBg *bg);
static void createBgChess(VisuGlExtBg *bg);

G_DEFINE_TYPE(VisuGlExtBg, visu_gl_ext_bg, VISU_TYPE_GL_EXT)

static void visu_gl_ext_bg_class_init(VisuGlExtBgClass *klass)
{
  float rgColor[2] = {0.f, 1.f};
  DBG_fprintf(stderr, "Extension Bg: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  DBG_fprintf(stderr, "                - adding new resources ;\n");
  visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                      FLAG_RESOURCE_BG_COLOR,
                                      DESC_RESOURCE_BG_COLOR,
                                      4, bgRGBDefault, rgColor);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                     exportResourcesBg);

  defaultBg = (VisuGlExtBg*)0;

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_bg_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_bg_finalize;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_bg_rebuild;
}

static void visu_gl_ext_bg_init(VisuGlExtBg *obj)
{
  DBG_fprintf(stderr, "Extension Bg: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = g_malloc(sizeof(VisuGlExtBgPrivate));
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->isBuilt  = FALSE;
  obj->priv->bgRGB[0] = bgRGBDefault[0];
  obj->priv->bgRGB[1] = bgRGBDefault[1];
  obj->priv->bgRGB[2] = bgRGBDefault[2];
  obj->priv->bgRGB[3] = bgRGBDefault[3];
  obj->priv->bgImage      = (guchar*)0;
  obj->priv->bgImageTitle = (gchar*)0;
  obj->priv->bgImageFollowZoom = FALSE;
  obj->priv->bgImageZoomInit = obj->priv->bgImageZoom = -1.f;
  obj->priv->bgImageZoomRatioInit = 1.f;
  obj->priv->bgImageXsInit = obj->priv->bgImageXs = 0.5f;
  obj->priv->bgImageXs0 = 0.f;
  obj->priv->bgImageYsInit = obj->priv->bgImageYs = 0.5f;
  obj->priv->bgImageYs0 = 0.f;
  obj->priv->view               = (VisuGlView*)0;
  obj->priv->widthHeight_signal = 0;

  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_BG_COLOR,
                          G_CALLBACK(onEntryColor), (gpointer)obj, G_CONNECT_SWAPPED);
}
static void visu_gl_ext_bg_dispose(GObject* obj)
{
  VisuGlExtBg *bg;

  DBG_fprintf(stderr, "Extension Bg: dispose object %p.\n", (gpointer)obj);

  bg = VISU_GL_EXT_BG(obj);
  if (bg->priv->dispose_has_run)
    return;
  bg->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_bg_setGlView(bg, (VisuGlView*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_bg_parent_class)->dispose(obj);
}
static void visu_gl_ext_bg_finalize(GObject* obj)
{
  VisuGlExtBg *bg;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Extension Bg: finalize object %p.\n", (gpointer)obj);

  bg = VISU_GL_EXT_BG(obj);

  /* Free privs elements. */
  if (bg->priv)
    {
      DBG_fprintf(stderr, "Extension Bg: free private bg.\n");
      g_free(bg->priv->bgImage);
      g_free(bg->priv->bgImageTitle);
      g_free(bg->priv);
    }

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Extension Bg: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_bg_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Extension Bg: freeing ... OK.\n");
}
/**
 * visu_gl_ext_bg_new:
 * @name: (allow-none): the name to give to the extension (default is #VISU_GL_EXT_BG_ID).
 *
 * Creates a new #VisuGlExt to draw bg.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtBg* visu_gl_ext_bg_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_BG_ID;
  char *description = _("Set the color of the background.");
  VisuGlExt *extensionBg;

  DBG_fprintf(stderr,"Extension Bg: new object.\n");
  
  extensionBg = VISU_GL_EXT(g_object_new(VISU_TYPE_GL_EXT_BG,
                                              "name", (name)?name:name_, "label", _(name),
                                              "description", description, "nGlObj", 3,
                                              "priority", VISU_GL_EXT_PRIORITY_BACKGROUND,
                                              "saveState", TRUE, NULL));

  return VISU_GL_EXT_BG(extensionBg);
}

/**
 * visu_gl_ext_bg_setGlView:
 * @bg: a #VisuGlExtBg object.
 * @view: (transfer full) (allow-none): a #VisuGlView object.
 *
 * Set the @view @bg is drawn to. It is necessary if @bg has a
 * background image or is using transparency background colour.
 *
 * Since: 3.7
 *
 * Returns: TRUE is @view is new.
 **/
gboolean visu_gl_ext_bg_setGlView(VisuGlExtBg *bg, VisuGlView *view)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BG(bg), FALSE);

  /* No change to be done. */
  if (view == bg->priv->view)
    return FALSE;

  if (bg->priv->view)
    {
      g_signal_handler_disconnect(G_OBJECT(bg->priv->view), bg->priv->widthHeight_signal);
      g_object_unref(bg->priv->view);
    }
  if (view)
    {
      g_object_ref(view);
      bg->priv->widthHeight_signal =
        g_signal_connect(G_OBJECT(view), "WidthHeightChanged",
                         G_CALLBACK(onBgImageRescale), (gpointer)bg);
    }
  else
    {
      bg->priv->widthHeight_signal = 0;
    }
  bg->priv->view = view;

  /* Set the bg color for this view. */
  glClearColor(bg->priv->bgRGB[0], bg->priv->bgRGB[1],
               bg->priv->bgRGB[2], bg->priv->bgRGB[3]);

  bg->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(bg));
}

/* Method used to change the value of the parameter backgroundColor. */
/**
 * visu_gl_ext_bg_setRGBA:
 * @bg: a #VisuGlExtBg object.
 * @rgba: a three floats array with values (0 <= values <= 1) for the
 * red, the green and the blue color. Only values specified by the mask
 * are really relevant.
 * @mask: use #TOOL_COLOR_MASK_R, #TOOL_COLOR_MASK_G, #TOOL_COLOR_MASK_B, #TOOL_COLOR_MASK_RGBA or a
 * combinaison to indicate what values in the rgb array must be taken
 * into account.
 *
 * Method used to change the value of the parameter background_color.
 *
 * Returns: TRUE if visu_gl_ext_bg_draw() should be aclled.
 */
gboolean visu_gl_ext_bg_setRGBA(VisuGlExtBg *bg, float rgba[4], int mask)
{
  gboolean diff;

  g_return_val_if_fail(VISU_IS_GL_EXT_BG(bg), FALSE);

  if (mask & TOOL_COLOR_MASK_R && bg->priv->bgRGB[0] != rgba[0])
    {
      bg->priv->bgRGB[0] = rgba[0];
      diff = TRUE;
    }
  if (mask & TOOL_COLOR_MASK_G && bg->priv->bgRGB[1] != rgba[1])
    {
      bg->priv->bgRGB[1] = rgba[1];
      diff = TRUE;
    }
  if (mask & TOOL_COLOR_MASK_B && bg->priv->bgRGB[2] != rgba[2])
    {
      bg->priv->bgRGB[2] = rgba[2];
      diff = TRUE;
    }
  if (mask & TOOL_COLOR_MASK_A && bg->priv->bgRGB[3] != rgba[3])
    {
      bg->priv->bgRGB[3] = rgba[3];
      diff = TRUE;
    }
  if (!diff)
    return FALSE;

  if (bg->priv->view)
    glClearColor(bg->priv->bgRGB[0], bg->priv->bgRGB[1],
                 bg->priv->bgRGB[2], bg->priv->bgRGB[3]);
  /* Update the fog color */
  visu_gl_ext_fog_create_color();

  bg->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(bg));
}

/* Get methods. */
/**
 * visu_gl_ext_bg_getRGBA:
 * @bg: a #VisuGlExtBg object.
 * @rgba: (array fixed-size=4) (out): a storage for four values.
 *
 * Read the RGBA value of the specific background colour (in [0;1]).
 */
void visu_gl_ext_bg_getRGBA(VisuGlExtBg *bg, float rgba[4])
{
  g_return_if_fail(VISU_IS_GL_EXT_BG(bg));

  memcpy(rgba, bg->priv->bgRGB, sizeof(float) * 4);
}

/**
 * visu_gl_ext_bg_setImage:
 * @bg: a #VisuGlExtBg object.
 * @imageData: (allow-none): raw image data in RGB or RGBA format ;
 * @width: the width ;
 * @height: the height ;
 * @alpha: TRUE if the image is RGBA ;
 * @title: (allow-none): an optional title (can be NULL).
 * @fit: a boolean (default is TRUE).
 *
 * Draw the @imageData on the background. The image is scaled to the
 * viewport dimensions, keeping the width/height ratio, if @fit is set
 * to TRUE. If @title is not NULL, the title is also printed on the
 * background. The image data are copied and can be free after this
 * call.
 */
void visu_gl_ext_bg_setImage(VisuGlExtBg *bg,
                             const guchar *imageData, guint width, guint height,
                             gboolean alpha, const gchar *title, gboolean fit)
{
  guint n;

  g_return_if_fail(VISU_IS_GL_EXT_BG(bg));

  g_free(bg->priv->bgImage);
  bg->priv->bgImage = (guchar*)0;
  g_free(bg->priv->bgImageTitle);
  bg->priv->bgImageTitle = (gchar*)0;

  bg->priv->isBuilt = FALSE;

  if (!imageData)
    return;

  DBG_fprintf(stderr, "Extension bg: copy image to memory buffer.\n");
  /* We copy the image to some correct size buffer. */
  bg->priv->bgImageW = width;
  bg->priv->bgImageH = height;
  n = (alpha)?4:3;
  bg->priv->bgImage = g_memdup(imageData, sizeof(guchar) * bg->priv->bgImageW *
                               bg->priv->bgImageH * n);
  bg->priv->bgImageAlpha = alpha;
  if (title)
    bg->priv->bgImageTitle = g_strdup_printf(_("Background: %s"), title);
  bg->priv->bgImageFit = fit;
  bg->priv->bgImageZoomInit = bg->priv->bgImageZoom = -1.f;
  bg->priv->bgImageZoomRatioInit = 1.f;
  bg->priv->bgImageXsInit = bg->priv->bgImageXs = 0.5f;
  bg->priv->bgImageXs0 = 0.f;
  bg->priv->bgImageYsInit = bg->priv->bgImageYs = 0.5f;
  bg->priv->bgImageYs0 = 0.f;
}
/**
 * visu_gl_ext_bg_setFollowCamera:
 * @bg: a #VisuGlExtBg object.
 * @follow: a boolean.
 * @zoomInit: a floating point value.
 * @xs: a floating point value.
 * @ys: a floating point value.
 *
 * When @follow is TRUE, the size and the position of the background
 * image is adjusted with every camera change.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the following status has been changed.
 */
gboolean visu_gl_ext_bg_setFollowCamera(VisuGlExtBg *bg, gboolean follow, float zoomInit,
                                        float xs, float ys)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BG(bg), FALSE);

  if (follow == bg->priv->bgImageFollowZoom)
    return FALSE;

  bg->priv->bgImageFollowZoom = follow;
  if (follow)
    {
      bg->priv->bgImageZoomInit = bg->priv->bgImageZoom = zoomInit;
      bg->priv->bgImageXsInit = bg->priv->bgImageXs = xs;
      bg->priv->bgImageYsInit = bg->priv->bgImageYs = ys;
    }
  else
    {
      bg->priv->bgImageZoomRatioInit *= bg->priv->bgImageZoom / bg->priv->bgImageZoomInit;
      bg->priv->bgImageXs0 -= bg->priv->bgImageXs - bg->priv->bgImageXsInit;
      bg->priv->bgImageYs0 -= bg->priv->bgImageYs - bg->priv->bgImageYsInit;
    }

  bg->priv->isBuilt = FALSE;
  return TRUE;
}
/**
 * visu_gl_ext_bg_setCamera:
 * @bg: a #VisuGlExtBg object.
 * @zoom: a floating point value.
 * @xs: a floating point value.
 * @ys: a floating point value.
 *
 * If the background image is in follow mode, see
 * visu_gl_ext_bg_setFollowCamera(), this routine is used to update
 * the current camera settings of the background image.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the settings are indeed changed.
 */
gboolean visu_gl_ext_bg_setCamera(VisuGlExtBg *bg, float zoom, float xs, float ys)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BG(bg), FALSE);

  if (zoom == bg->priv->bgImageZoom && xs == bg->priv->bgImageXs && ys == bg->priv->bgImageYs)
    return FALSE;

  if (bg->priv->bgImageFollowZoom)
    {
      bg->priv->bgImageZoom = zoom;
      bg->priv->bgImageXs = xs;
      bg->priv->bgImageYs = ys;
    }

  return bg->priv->bgImageFollowZoom;
}

/**
 * visu_gl_ext_bg_draw:
 * @bg: a #VisuGlExtBg object.
 *
 * Creates the OpenGL lists used by @bg (like the background image).
 *
 * Since: 3.7
 **/
void visu_gl_ext_bg_draw(VisuGlExtBg *bg)
{
  g_return_if_fail(VISU_IS_GL_EXT_BG(bg));

  /* Nothing to draw; */
  if(!bg->priv->view ||
     !visu_gl_ext_getActive(VISU_GL_EXT(bg)) ||
     bg->priv->isBuilt) return;

  if (bg->priv->bgRGB[3] < 1.f)
    createBgChess(bg);
  else
    glDeleteLists(visu_gl_ext_getGlList(VISU_GL_EXT(bg)) + 1, 1);  

  if (bg->priv->bgImage)
    createBgImage(bg);
  else
    glDeleteLists(visu_gl_ext_getGlList(VISU_GL_EXT(bg)) + 2, 1);

  glNewList(visu_gl_ext_getGlList(VISU_GL_EXT(bg)), GL_COMPILE);
  glCallList(visu_gl_ext_getGlList(VISU_GL_EXT(bg)) + 1);
  glCallList(visu_gl_ext_getGlList(VISU_GL_EXT(bg)) + 2);
  glEndList();
}

/**
 * visu_gl_ext_bg_getDefault:
 *
 * V_Sim is using a default bg object.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuGlExtBg object used by default.
 **/
VisuGlExtBg* visu_gl_ext_bg_getDefault()
{
  if (!defaultBg)
    defaultBg = visu_gl_ext_bg_new((gchar*)0);
  return defaultBg;
}

/****************/
/* Private part */
/****************/
static void visu_gl_ext_bg_rebuild(VisuGlExt *ext)
{
  VisuGlExtBg *bg = VISU_GL_EXT_BG(ext);

  DBG_fprintf(stderr, "Fog & Bg: rebuild extension.\n");
  glClearColor(bg->priv->bgRGB[0], bg->priv->bgRGB[1],
               bg->priv->bgRGB[2], bg->priv->bgRGB[3]);
  visu_gl_ext_bg_draw(bg);
  glNewList(visu_gl_ext_getGlList(ext), GL_COMPILE);
  glCallList(visu_gl_ext_getGlList(ext) + 2);
  glEndList();

  /* To be moved... */
  if (bg->priv->view && visu_gl_ext_fog_getOn())
    {
      glEnable(GL_FOG);
      glFogi(GL_FOG_MODE, GL_LINEAR);
      visu_gl_ext_fog_create_color();
      visu_gl_ext_fog_create(bg->priv->view, visu_boxed_getBox(VISU_BOXED(bg->priv->view)));
    }
}
static void onBgImageRescale(VisuGlView *view _U_, gpointer data)
{
  DBG_fprintf(stderr, "Extension Bg: caught the 'WidthHeightChanged' signal.\n");

  if (VISU_GL_EXT_BG(data)->priv->bgImage)
    createBgImage(VISU_GL_EXT_BG(data));
  if (VISU_GL_EXT_BG(data)->priv->bgRGB[3] < 1.f)
    createBgChess(VISU_GL_EXT_BG(data));
}
static void onEntryColor(VisuGlExtBg *bg, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_bg_setRGBA(bg, bgRGBDefault, TOOL_COLOR_MASK_RGBA);
}
static void createBgImage(VisuGlExtBg *bg)
{
  int viewport[4];
  float x, y;
  float zoom;

  DBG_fprintf(stderr, "Extension bg: set background image.\n");

  g_return_if_fail(VISU_IS_GL_EXT_BG(bg) && bg->priv->bgImage);

  visu_gl_text_initFontList();
  
  glGetIntegerv(GL_VIEWPORT, viewport);
  if (bg->priv->bgImageFit)
    {
      x = (float)viewport[2] / (float)bg->priv->bgImageW;
      y = (float)viewport[3] / (float)bg->priv->bgImageH;
    }
  else
    {
      x = y = 1.f;
    }
  DBG_fprintf(stderr, "Extension bg: use follow zoom %d (%g / %g).\n",
              bg->priv->bgImageFollowZoom, bg->priv->bgImageZoom, bg->priv->bgImageZoomInit);
  zoom = MIN(x, y) * bg->priv->bgImageZoomRatioInit * bg->priv->bgImageZoom / bg->priv->bgImageZoomInit;
  x = ((float)viewport[2] - zoom * (float)bg->priv->bgImageW) / 2.f;
  x += (float)viewport[2] * (bg->priv->bgImageXs - bg->priv->bgImageXsInit - bg->priv->bgImageXs0);
  y = ((float)viewport[3] - zoom * (float)bg->priv->bgImageH) / 2.f;
  y -= (float)viewport[3] * (bg->priv->bgImageYs - bg->priv->bgImageYsInit - bg->priv->bgImageYs0);

  glNewList(visu_gl_ext_getGlList(VISU_GL_EXT(bg)) + 2, GL_COMPILE);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluOrtho2D(0., (float)viewport[2], 0., (float)viewport[3]);   
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  glDepthMask(0);
  glRasterPos2i(0, 0);
  glBitmap(0, 0, 0, 0, x, viewport[3] - y, NULL);
  glPixelZoom(zoom, -zoom);
  if (bg->priv->bgImageAlpha)
    glDrawPixels(bg->priv->bgImageW, bg->priv->bgImageH, GL_RGBA, GL_UNSIGNED_BYTE, bg->priv->bgImage);
  else
    glDrawPixels(bg->priv->bgImageW, bg->priv->bgImageH, GL_RGB, GL_UNSIGNED_BYTE, bg->priv->bgImage);
  glPixelZoom(1., 1.);

  if (bg->priv->bgImageTitle)
    {
      glDisable(GL_LIGHTING);
      glColor4f(1.f - bg->priv->bgRGB[0], 1.f - bg->priv->bgRGB[1], 1.f - bg->priv->bgRGB[2], 1.f);
      glRasterPos2f(5.f, 5.f);
      visu_gl_text_drawChars(bg->priv->bgImageTitle, VISU_GL_TEXT_NORMAL); 
    }
  glDepthMask(1);

  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);

  glEndList();
}
static void createBgChess(VisuGlExtBg *bg)
{
  GLubyte chessboard[32][32][3];
  int viewport[4];
  int i, j, c;

  DBG_fprintf(stderr, "Extension bg: set background chess board (alpha = %g).\n",
              bg->priv->bgRGB[3]);

  if (texName == 0)
    glGenTextures(1, &texName);

  /* We create the chessboard texture with the right colour. */
  for (i = 0; i < 32; i++)
    for (j = 0; j < 32; j++)
      {
        c = 128 + ( ((i&0x10)==0) ^ ((j&0x10) == 0) ) * 64;
        chessboard[i][j][0] =
          (GLubyte)(255.f * bg->priv->bgRGB[0] * bg->priv->bgRGB[3] +
                    (1.f - bg->priv->bgRGB[3]) * c);
        chessboard[i][j][1] =
          (GLubyte)(255.f * bg->priv->bgRGB[1] * bg->priv->bgRGB[3] +
                    (1.f - bg->priv->bgRGB[3]) * c);
        chessboard[i][j][2] =
          (GLubyte)(255.f * bg->priv->bgRGB[2] * bg->priv->bgRGB[3] +
                    (1.f - bg->priv->bgRGB[3]) * c);
      }
  /* We bind the texture. */
  glBindTexture(GL_TEXTURE_2D, texName);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 32, 32, 0,
               GL_RGB, GL_UNSIGNED_BYTE, chessboard);

  glGetIntegerv(GL_VIEWPORT, viewport);

  glNewList(visu_gl_ext_getGlList(VISU_GL_EXT(bg)) + 1, GL_COMPILE);
  glDisable(GL_CULL_FACE);
  glDisable(GL_LIGHTING);

  glEnable(GL_TEXTURE_2D);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
  glBindTexture(GL_TEXTURE_2D, texName);

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluOrtho2D(0., (float)viewport[2], 0., (float)viewport[3]);   
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  glDepthMask(0);
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.0, 0.0, 0.0);
  glTexCoord2f(0.0, (float)viewport[3] / 32.f);
  glVertex3f(0.0, (float)viewport[3], 0.0);
  glTexCoord2f((float)viewport[2] / 32.f, (float)viewport[3] / 32.f);
  glVertex3f((float)viewport[2], (float)viewport[3], 0.0);
  glTexCoord2f((float)viewport[2] / 32.f, 0.0);
  glVertex3f((float)viewport[2], 0.0, 0.0);
  glEnd();
  glDepthMask(1);

  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);

  glDisable(GL_TEXTURE_2D);
  glEndList();
}

/* Export function that is called by visu_module to write the
   values of resources to a file. */
static void exportResourcesBg(GString *data, VisuData *dataObj _U_, VisuGlView *view _U_)
{
  if (!defaultBg)
    return;

  visu_config_file_exportComment(data, DESC_RESOURCE_BG_COLOR);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_BG_COLOR, NULL,
                               "%4.3f %4.3f %4.3f %4.3f",
                               defaultBg->priv->bgRGB[0], defaultBg->priv->bgRGB[1],
                               defaultBg->priv->bgRGB[2], defaultBg->priv->bgRGB[3]);

  visu_config_file_exportComment(data, "");
}



/*********************/
/* Dealing with fog. */
/*********************/
/* This is a boolean to control is the axes is render or not. */
#define FLAG_RESOURCE_FOG_USED   "fog_is_on"
#define DESC_RESOURCE_FOG_USED   "Control if the fog is used ; boolean (0 or 1)"
#define RESOURCE_FOG_USED_DEFAULT 0
static gboolean readFogIsOn(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                            VisuData *dataObj, VisuGlView *view, GError **error);
#define FLAG_RESOURCE_FOG_SPECIFIC   "fog_color_is_specific"
#define DESC_RESOURCE_FOG_SPECIFIC   "Control if the fog uses a specific color ; boolean (0 or 1)"
#define RESOURCE_FOG_SPECIFIC_DEFAULT 0
static int fogColorSpecific;
static gboolean readFogSpecific(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                VisuData *dataObj, VisuGlView *view, GError **error);
/* A resource to control the color used to render the lines of the axes. */
#define FLAG_RESOURCE_FOG_COLOR   "fog_specific_color"
#define DESC_RESOURCE_FOG_COLOR   "Define the color of the fog ; four floating point values (0. <= v <= 1.)"
static float fogRGB[4];
static gboolean readFogColor(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                             VisuData *dataObj, VisuGlView *view, GError **error);
#define FLAG_RESOURCE_FOG_STARTEND "fog_start_end"
#define DESC_RESOURCE_FOG_STARTEND "Define the position of the fog ; two floating point values (0. <= v <= 1.)"
static float fog_start, fog_end;
static gboolean readFogStartEnd(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                VisuData *dataObj, VisuGlView *view, GError **error);
/* Export function that is called by visu_module to write the
   values of resources to a file. */
static void exportResourcesFog(GString *data, VisuData *dataObj, VisuGlView *view);

/* Local variables. */
static float fogRGBDefault[4] = {0., 0., 0., 1.};
static float fogStartEndDefault[2] = {0.3, 0.7};
static gboolean fogIsOn;
static gboolean fogHasBeenBuilt;
static gulong nearFar_signal = 0;

/* Callbacks. */
static void rebuildFogOnResources(GObject *obj, VisuData *dataObj, gpointer data);
static void onDataReadySignal(GObject *visu, VisuData *dataObj,
                              VisuGlView *view, gpointer data);
static void onDataNotReadySignal(GObject *visu, VisuData *dataObj,
                                 VisuGlView *view, gpointer data);
static void onFogParameterChanged(VisuGlView *view, gpointer data);

/**
 * visu_gl_ext_fog_init: (skip)
 *
 * It initialises all variables of the OpenGL extension.
 * It creates all resources and parameters introduced by
 * this extension.
 *
 * Returns: a pointer to the VisuGlExt it created or
 * NULL otherwise.
 */
void visu_gl_ext_fog_init()
{
  visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
                           FLAG_RESOURCE_FOG_USED,
                           DESC_RESOURCE_FOG_USED,
                           1, readFogIsOn);
  visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
                           FLAG_RESOURCE_FOG_SPECIFIC,
                           DESC_RESOURCE_FOG_SPECIFIC,
                           1, readFogSpecific);
  visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
                           FLAG_RESOURCE_FOG_COLOR,
                           DESC_RESOURCE_FOG_COLOR,
                           1, readFogColor);
  visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
                           FLAG_RESOURCE_FOG_STARTEND,
                           DESC_RESOURCE_FOG_STARTEND,
                           1, readFogStartEnd);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                    exportResourcesFog);

  /* Initialisation des valeurs par d�faut. */
  fogIsOn            = FALSE;
  fogHasBeenBuilt    = FALSE;
  fogColorSpecific   = RESOURCE_FOG_SPECIFIC_DEFAULT;
  memcpy(fogRGB, fogRGBDefault, 4 * sizeof(float));
  fog_start = fogStartEndDefault[0];
  fog_end   = fogStartEndDefault[1];

  g_signal_connect(VISU_OBJECT_INSTANCE, "resourcesLoaded",
		   G_CALLBACK(rebuildFogOnResources), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataRendered",
		   G_CALLBACK(onDataReadySignal), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataUnRendered",
		   G_CALLBACK(onDataNotReadySignal), (gpointer)0);
}

/**
 * visu_gl_ext_fog_setValues:
 * @rgba: (in) (array fixed-size=4): a three floats array with
 * values (0 <= values <= 1) for the red, the green and the blue
 * color. Only values specified by the mask are really relevant.
 * @mask: use #TOOL_COLOR_MASK_R, #TOOL_COLOR_MASK_G,
 * #TOOL_COLOR_MASK_B, #TOOL_COLOR_MASK_RGBA or a combinaison to
 * indicate what values in the rgb array must be taken into account.
 *
 * Method used to change the value of the parameter fog_specific_color.
 * This color is actually used only if fog_color_is_specific is set to 1,
 * use setFogColorSpecific() to do that.
 *
 * Returns: 0 if redrawing is not necessary.
 * 1 if the rendering window must be redrawn.
 */
gboolean visu_gl_ext_fog_setValues(float rgba[4], int mask)
{
  int diff = 0;
  
  if (mask & TOOL_COLOR_MASK_R && fogRGB[0] != rgba[0])
    {
      fogRGB[0] = rgba[0];
      diff = 1;
    }
  if (mask & TOOL_COLOR_MASK_G && fogRGB[1] != rgba[1])
    {
      fogRGB[1] = rgba[1];
      diff = 1;
    }
  if (mask & TOOL_COLOR_MASK_B && fogRGB[2] != rgba[2])
    {
      fogRGB[2] = rgba[2];
      diff = 1;
    }
  if (mask & TOOL_COLOR_MASK_A && fogRGB[3] != rgba[3])
    {
      fogRGB[3] = rgba[3];
      diff = 1;
    }
  if (!diff)
    return FALSE;

  /* The method which calls the set method should ask for redraw. */
  if (fogColorSpecific)
    glFogfv(GL_FOG_COLOR, fogRGB);

  return (fogColorSpecific && fogIsOn);
}

/* Get methods. */
void visu_gl_ext_fog_getValues(float rgba[4])
{
  memcpy(rgba, fogRGB, sizeof(float) * 4);
}
/**
 * visu_gl_ext_fog_setOn:
 * @value: TRUE if fog must be rendered, FALSE otherwise.
 *
 * Method used to change the value of the parameter fog_is_on.
 *
 * Returns: FALSE if redrawing is not necessary.
 * TRUE if the rendering window must be redrawn.
 */
gboolean visu_gl_ext_fog_setOn(gboolean value)
{
  if (value == fogIsOn)
    return FALSE;

  fogIsOn = value;
  if (value)
    {
      glEnable(GL_FOG);
      glFogi(GL_FOG_MODE, GL_LINEAR);
/*       glFogi(GL_FOG_MODE, GL_EXP);  */
/*       glFogf(GL_FOG_DENSITY, 0.03f);  */
    }
  else
    glDisable(GL_FOG);
    
  return (value && !fogHasBeenBuilt);
}
gboolean visu_gl_ext_fog_getOn()
{
  return fogIsOn;
}
/**
 * visu_gl_ext_fog_setUseSpecificColor:
 * @value: TRUE if fog is rendered with its own color specified
 * by setFogRGBValues() or FALSE if the fog uses the background
 * color.
 *
 * Method used to change the value of the parameter fog_color_is_specific.
 *
 * Returns: TRUE if visu_gl_ext_fog_create() should be called. In all cases, 'OpenGLAskForReDraw'
 *          signal should then be emitted.
 */
gboolean visu_gl_ext_fog_setUseSpecificColor(gboolean value)
{
  if (value == fogColorSpecific)
    return FALSE;

  fogColorSpecific = value;
  visu_gl_ext_fog_create_color();
    
  return fogIsOn;
}
gboolean visu_gl_ext_fog_getUseSpecificColor()
{
  return fogColorSpecific;
}
/**
 * visu_gl_ext_fog_setStartEndValues:
 * @startEnd: (in) (array fixed-size=2): a two floats array with
 * values (0 <= values <= 1) for the beging and the ending of the fog
 * position. Only values specified by the mask are really relevant.
 * @mask: use #VISU_GL_EXT_FOG_MASK_START, #VISU_GL_EXT_FOG_MASK_END
 * to indicate what values in the startEnd array must be taken into
 * account.
 *
 * Method used to change the value of the parameters fog_start and
 * fog_end.
 *
 * Returns: TRUE if visu_gl_ext_fog_create() should be called and then 'OpenGLAskForReDraw'
 *          signal be emitted.
 */
gboolean visu_gl_ext_fog_setStartEndValues(float startEnd[2], int mask)
{
  int diff = 0;
  
  if (mask & VISU_GL_EXT_FOG_MASK_START && fog_start != startEnd[0])
    {
      fog_start = CLAMP(startEnd[0], 0., 1.);
      if (mask & VISU_GL_EXT_FOG_MASK_END)
	{
	  if (fog_start >= startEnd[1])
	    fog_start = startEnd[1] - 0.001;
	}
      else
	if (fog_start >= fog_end)
	  fog_start = fog_end - 0.001;
      diff = 1;
    }
  if (mask & VISU_GL_EXT_FOG_MASK_END && fog_end != startEnd[1])
    {
      fog_end = CLAMP(startEnd[1], 0., 1.);
      if (fog_end <= fog_start)
	fog_end = fog_start + 0.001;
      diff = 1;
    }
  if (!diff)
    return FALSE;
  
  /* The method which calls the set method should ask for redraw. */
/*   visu_gl_ext_fog_create((GObject*)0, (gpointer)0); */
  fogHasBeenBuilt = FALSE;
  
  return (fogIsOn);
}
float visu_gl_ext_fog_getStart()
{
  return fog_start;
}
float visu_gl_ext_fog_getEnd()
{
  return fog_end;
}
/**
 * visu_gl_ext_fog_create:
 * @view: the #VisuGlView object the fog apply to.
 * @box: the #VisuBox object to delimit the area start and end.
 *
 * Call the OpenGL routine to enable and initialise the fog. The fog
 * is created in the not duplicated cell of @box.
 */
void visu_gl_ext_fog_create(VisuGlView *view, VisuBox *box)
{
  float start, stop, bSize, centre;

  g_return_if_fail(view);

  bSize = visu_box_getGlobalSize(box, FALSE);
  centre = ((view->camera->d_red > 100.f)?100.f:view->camera->d_red) * view->camera->length0;

  start = centre - bSize + 2.f * bSize * fog_start;
  stop  = centre - bSize + 2.f * bSize * fog_end;

/*   start = visuBox->extens * visuCamera->d_red * (1. - fog_start); */
/*   stop = visuBox->extens * visuCamera->d_red * (1. + fog_end); */
/*   start = visuBox->extens * visuCamera->d_red * (1. - 1 / 1.1); */
/*   stop = visuBox->extens * visuCamera->d_red * (1. + 1 / 1.1); */
/*   fprintf(stderr, "----------> %f %f %f %f\n", (float)(view->window->near + */
/* 						       (view->window->far - view->window->near) * fog_start), (float)(view->window->near + */
/* 														  (view->window->far - view->window->near) * fog_end), start, stop); */
  glFogf(GL_FOG_START, start);
  glFogf(GL_FOG_END, stop);

  fogHasBeenBuilt = TRUE;
}
void visu_gl_ext_fog_create_color()
{
  float bgRGB[4];

  if (fogColorSpecific)
    glFogfv(GL_FOG_COLOR, fogRGB);
  else
    {
      visu_gl_ext_bg_getRGBA(visu_gl_ext_bg_getDefault(), bgRGB);
      glFogfv(GL_FOG_COLOR, bgRGB);
    }
}

/* Callbacks. */
static void rebuildFogOnResources(GObject *obj _U_, VisuData *dataObj _U_,
				  gpointer data _U_)
{
  DBG_fprintf(stderr, "Extension Fog: caught the 'resourcesLoaded' signal, rebuilding"
	      " fog.\n");
  /* g_message("TODO: add a way to update fog when loading resources.\n"); */
}
static void onDataReadySignal(GObject *visu _U_, VisuData *dataObj _U_,
                              VisuGlView *view, gpointer data _U_)
{
  VisuBox *box;

  box = visu_boxed_getBox(VISU_BOXED(view));
  if (view && box)
    {
      visu_gl_ext_fog_create(view, box);
      if (!nearFar_signal)
        nearFar_signal =
          g_signal_connect(G_OBJECT(view), "NearFarChanged",
                           G_CALLBACK(onFogParameterChanged), (gpointer)0);
    }
}
static void onDataNotReadySignal(GObject *visu _U_, VisuData *dataObj _U_,
                                 VisuGlView *view, gpointer data _U_)
{
  g_signal_handler_disconnect(G_OBJECT(view), nearFar_signal);
  nearFar_signal = 0;
}
static void onFogParameterChanged(VisuGlView *view, gpointer data _U_)
{
  DBG_fprintf(stderr, "Extension Fog: caught the 'NearFarChanged' signal, rebuilding"
	      " fog.\n");
  visu_gl_ext_fog_create(view, visu_boxed_getBox(VISU_BOXED(view)));
}

/* Resources. */
/* This is a boolean to control is the fog is render or not. */
static gboolean readFogIsOn(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
			    int position, VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  gboolean val;
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readBoolean(lines[0], position, &val, 1, error))
    return FALSE;
  visu_gl_ext_fog_setOn(val);

  return TRUE;
}
/* This is a boolean to control the color used by the fog : a
   specific one or the background color. */
static gboolean readFogSpecific(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				int position, VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  gboolean val;
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readBoolean(lines[0], position, &val, 1, error))
    return FALSE;
  visu_gl_ext_fog_setUseSpecificColor(val);

  return TRUE;
}
/* A resource to control the color used for the background. */
static gboolean readFogColor(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
			     int position, VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  float val[4];
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readFloat(lines[0], position, val, 4, error))
    {
      if (*error)
	g_error_free(*error);
      *error = (GError*)0;
      /* Read old 3 values. */
      if (!tool_config_file_readFloat(lines[0], position, val, 3, error))
	return FALSE;
      val[3] = 0.f;
    }
  visu_gl_ext_fog_setValues(val, TOOL_COLOR_MASK_RGBA);
  return TRUE;
}
/* A resource to control the color used for the background. */
static gboolean readFogStartEnd(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				int position, VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  float val[2];
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readFloat(lines[0], position, val, 2, error))
    return FALSE;
  visu_gl_ext_fog_setStartEndValues(val, VISU_GL_EXT_FOG_MASK_START | VISU_GL_EXT_FOG_MASK_END);
  return TRUE;
}
/* Export function that is called by visu_module to write the
   values of resources to a file. */
static void exportResourcesFog(GString *data, VisuData *dataObj _U_, VisuGlView *view _U_)
{
  visu_config_file_exportComment(data, DESC_RESOURCE_FOG_USED);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_FOG_USED, NULL,
                               "%d", fogIsOn);

  visu_config_file_exportComment(data, DESC_RESOURCE_FOG_SPECIFIC);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_FOG_SPECIFIC, NULL,
                               "%d", fogColorSpecific);

  visu_config_file_exportComment(data, DESC_RESOURCE_FOG_COLOR);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_FOG_COLOR, NULL,
                               "%4.3f %4.3f %4.3f %4.3f",
                               fogRGB[0], fogRGB[1], fogRGB[2], fogRGB[3]);

  visu_config_file_exportComment(data, DESC_RESOURCE_FOG_STARTEND);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_FOG_STARTEND, NULL,
                               "%4.3f %4.3f", fog_start, fog_end);

  visu_config_file_exportComment(data, "");
}
