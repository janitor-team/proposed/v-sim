/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "axes.h"
#include "../renderingMethods/renderingAtomic.h"
#include "../renderingMethods/renderingSpin.h"

#include <GL/gl.h>
#include <GL/glu.h> 

#include <math.h>
#include <string.h>

#include <opengl.h>
#include <openGLFunctions/text.h>
#include <visu_object.h>
#include <visu_tools.h>
#include <visu_configFile.h>
#include <openGLFunctions/objectList.h>
#include <coreTools/toolColor.h>
#include <coreTools/toolConfigFile.h>

/**
 * SECTION:axes
 * @short_description: Defines methods to draw axes.
 *
 * <para>The axes are the X, Y and Z lines drawn on the bottom right of the
 * screen defining a given orthogonal basis set in which the box is
 * projected.</para>
 * <para>The axis may be different, depending on the rendering method
 * currently used. For instance, when the spin is used, a projection
 * of the colour scheme is added to the simple lines of the basis
 * set. Besides that, axes are defined by their width (see
 * visu_gl_ext_axes_setLineWidth()) and their colour (see
 * visu_gl_ext_axes_setRGB()).</para>
 */

/* Parameters & resources*/
/* This is a boolean to control is the axes is render or not. */
#define FLAG_RESOURCE_AXES_USED   "axes_are_on"
#define DESC_RESOURCE_AXES_USED   "Control if the axes are drawn ; boolean (0 or 1)"
static gboolean RESOURCE_AXES_USED_DEFAULT = FALSE;
/* A resource to control the color used to render the lines of the axes. */
#define FLAG_RESOURCE_AXES_COLOR   "axes_color"
#define DESC_RESOURCE_AXES_COLOR   "Define the color of the axes ; three floating point values (0. <= v <= 1.)"
static float rgbDefault[3] = {1.0, 0.5, 0.1};
/* A resource to control the width to render the lines of the axes. */
#define FLAG_RESOURCE_AXES_LINE   "axes_line_width"
#define DESC_RESOURCE_AXES_LINE   "Define the width of the lines of the axes ; one floating point values (1. <= v <= 10.)"
static float LINE_WIDTH_DEFAULT = 1.f;
/* A resource to control the stipple to render the lines of the axes. */
#define FLAG_RESOURCE_AXES_STIPPLE   "axes_line_stipple"
#define DESC_RESOURCE_AXES_STIPPLE   "Dot scheme detail for the lines of the axes ; 0 < integer < 2^16"
static guint16 LINE_STIPPLE_DEFAULT = 65535;
static gboolean readAxesLineStipple(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                    VisuData *dataObj, VisuGlView *view, GError **error);
/* A resource to control the position to render the axes. */
#define FLAG_RESOURCE_AXES_POSITION   "axes_position"
#define DESC_RESOURCE_AXES_POSITION   "Position of the representation of the axes ; two floating point values (0. <= v <= 1.)"
static float POSITION_DEFAULT[2] = {1.f, 1.f};

/* Export function that is called by visu_module to write the
   values of resources to a file. */
static void exportResourcesAxes(GString *data, VisuData *dataObj, VisuGlView *view);

/**
 * VisuGlExtAxesClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtAxesClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtAxes:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtAxesPrivate:
 *
 * Private fields for #VisuGlExtAxes objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtAxesPrivate
{
  gboolean dispose_has_run;
  gboolean areBuilt;

  /* Basis definition. */
  double matrix[3][3];
  VisuBox *box;
  gulong box_signal;

  /* Rendenring parameters. */
  float xpos, ypos;
  float rgb[3];
  float lineWidth;
  guint16 lineStipple;

  /* Signals for the current view. */
  VisuGlView *view;
  gulong widthHeight_signal, nearFar_signal, refLength_signal, color_signal;
};

static VisuGlExtAxes* defaultAxes;

static void visu_gl_ext_axes_finalize(GObject* obj);
static void visu_gl_ext_axes_dispose(GObject* obj);
static void visu_gl_ext_axes_rebuild(VisuGlExt *ext);
static void _setBox(VisuGlExtAxes *axes, VisuBox *box);

/* Local callbacks */
static void onAxesParametersChange(VisuGlView *view, gpointer data);
static void onAxesConfigChange(VisuRenderingSpin *render, gpointer data);
static void onBoxChange(VisuBox *box, float extens, gpointer data);
static void onEntryUsed(VisuGlExtAxes *axes, gchar *key, VisuObject *obj);
static void onEntryColor(VisuGlExtAxes *axes, gchar *key, VisuObject *obj);
static void onEntryWidth(VisuGlExtAxes *axes, gchar *key, VisuObject *obj);
static void onEntryStipple(VisuGlExtAxes *axes, gchar *key, VisuObject *obj);
static void onEntryPosition(VisuGlExtAxes *axes, gchar *key, VisuObject *obj);

G_DEFINE_TYPE(VisuGlExtAxes, visu_gl_ext_axes, VISU_TYPE_GL_EXT)

static void visu_gl_ext_axes_class_init(VisuGlExtAxesClass *klass)
{
  float rgColor[2] = {0.f, 1.f};
  float rgWidth[2] = {0.f, 10.f};
  VisuConfigFileEntry *resourceEntry;

  DBG_fprintf(stderr, "Extension Axes: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  DBG_fprintf(stderr, "                - adding new resources ;\n");
  resourceEntry = visu_config_file_addBooleanEntry(VISU_CONFIG_FILE_RESOURCE,
                                                   FLAG_RESOURCE_AXES_USED,
                                                   DESC_RESOURCE_AXES_USED,
                                                   &RESOURCE_AXES_USED_DEFAULT);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_AXES_COLOR,
                                                      DESC_RESOURCE_AXES_COLOR,
                                                      3, rgbDefault, rgColor);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_AXES_LINE,
                                                      DESC_RESOURCE_AXES_LINE,
                                                      1, &LINE_WIDTH_DEFAULT, rgWidth);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_RESOURCE_AXES_STIPPLE,
					  DESC_RESOURCE_AXES_STIPPLE,
					  1, readAxesLineStipple);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_AXES_POSITION,
                                                      DESC_RESOURCE_AXES_POSITION,
                                                      2, POSITION_DEFAULT, rgColor);
  visu_config_file_entry_setVersion(resourceEntry, 3.7f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                     exportResourcesAxes);

  defaultAxes = (VisuGlExtAxes*)0;

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_axes_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_axes_finalize;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_axes_rebuild;
}

static void visu_gl_ext_axes_init(VisuGlExtAxes *obj)
{
  DBG_fprintf(stderr, "Extension Axes: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = g_malloc(sizeof(VisuGlExtAxesPrivate));
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->areBuilt  = FALSE;
  obj->priv->rgb[0]      = rgbDefault[0];
  obj->priv->rgb[1]      = rgbDefault[1];
  obj->priv->rgb[2]      = rgbDefault[2];
  obj->priv->lineWidth   = LINE_WIDTH_DEFAULT;
  obj->priv->lineStipple = LINE_STIPPLE_DEFAULT;
  obj->priv->xpos        = POSITION_DEFAULT[0];
  obj->priv->ypos        = POSITION_DEFAULT[1];
  obj->priv->view               = (VisuGlView*)0;
  obj->priv->widthHeight_signal = 0;
  obj->priv->nearFar_signal     = 0;
  obj->priv->refLength_signal   = 0;
  obj->priv->color_signal       = 0;
  obj->priv->matrix[0][0] = 1.;
  obj->priv->matrix[0][1] = 0.;
  obj->priv->matrix[0][2] = 0.;
  obj->priv->matrix[1][0] = 0.;
  obj->priv->matrix[1][1] = 1.;
  obj->priv->matrix[1][2] = 0.;
  obj->priv->matrix[2][0] = 0.;
  obj->priv->matrix[2][1] = 0.;
  obj->priv->matrix[2][2] = 1.;
  obj->priv->box          = (VisuBox*)0;
  obj->priv->box_signal   = 0;

  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_AXES_USED,
                          G_CALLBACK(onEntryUsed), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_AXES_COLOR,
                          G_CALLBACK(onEntryColor), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_AXES_LINE,
                          G_CALLBACK(onEntryWidth), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_AXES_STIPPLE,
                          G_CALLBACK(onEntryStipple), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_AXES_POSITION,
                          G_CALLBACK(onEntryPosition), (gpointer)obj, G_CONNECT_SWAPPED);
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_gl_ext_axes_dispose(GObject* obj)
{
  VisuGlExtAxes *axes;

  DBG_fprintf(stderr, "Extension Axes: dispose object %p.\n", (gpointer)obj);

  axes = VISU_GL_EXT_AXES(obj);
  if (axes->priv->dispose_has_run)
    return;
  axes->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_axes_setGlView(axes, (VisuGlView*)0);
  _setBox(axes, (VisuBox*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_axes_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_gl_ext_axes_finalize(GObject* obj)
{
  VisuGlExtAxes *axes;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Extension Axes: finalize object %p.\n", (gpointer)obj);

  axes = VISU_GL_EXT_AXES(obj);

  /* Free privs elements. */
  if (axes->priv)
    {
      DBG_fprintf(stderr, "Extension Axes: free private axes.\n");
      g_free(axes->priv);
    }
  /* The free is called by g_type_free_instance... */
/*   g_free(axes); */

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Extension Axes: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_axes_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Extension Axes: freeing ... OK.\n");
}
static void _setBox(VisuGlExtAxes *axes, VisuBox *box)
{
  if (axes->priv->box)
    {
      g_signal_handler_disconnect(G_OBJECT(axes->priv->box), axes->priv->box_signal);
      g_object_unref(axes->priv->box);
    }
  if (box)
    {
      g_object_ref(box);
      axes->priv->box_signal =
        g_signal_connect(G_OBJECT(box), "SizeChanged",
                         G_CALLBACK(onBoxChange), (gpointer)axes);
    }
  else
    axes->priv->box_signal = 0;
  axes->priv->box = box;
}

/**
 * visu_gl_ext_axes_new:
 * @name: (allow-none): the name to give to the extension (default is #VISU_GL_EXT_AXES_ID).
 *
 * Creates a new #VisuGlExt to draw axes.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtAxes* visu_gl_ext_axes_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_AXES_ID;
  char *description = _("Draw {x,y,z} axes.");
  VisuGlExt *extensionAxes;

  DBG_fprintf(stderr,"Extension Axes: new object.\n");
  
  extensionAxes = VISU_GL_EXT(g_object_new(VISU_TYPE_GL_EXT_AXES,
                                              "name", (name)?name:name_, "label", _(name),
                                              "description", description, "nGlObj", 1,
                                              "priority", VISU_GL_EXT_PRIORITY_LAST,
                                              "saveState", TRUE, NULL));

  return VISU_GL_EXT_AXES(extensionAxes);
}

/**
 * visu_gl_ext_axes_setRGB:
 * @axes: the #VisuGlExtAxes object to modify.
 * @rgb: a three floats array with values (0 <= values <= 1) for the
 * red, the green and the blue color. Only values specified by the mask
 * are really relevant.
 * @mask: use #TOOL_COLOR_MASK_R, #TOOL_COLOR_MASK_G, #TOOL_COLOR_MASK_B, #TOOL_COLOR_MASK_RGBA or a
 * combinaison to indicate what values in the rgb array must be taken
 * into account.
 *
 * Method used to change the value of the parameter axes_color.
 *
 * Returns: TRUE if visu_gl_ext_axes_draw() should be called and then 'OpenGLAskForReDraw'
 *          signal be emitted.
 */
gboolean visu_gl_ext_axes_setRGB(VisuGlExtAxes *axes, float rgb[3], int mask)
{
  gboolean diff;

  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);
  
  diff = FALSE;
  if (mask & TOOL_COLOR_MASK_R && axes->priv->rgb[0] != rgb[0])
    {
      axes->priv->rgb[0] = rgb[0];
      diff = TRUE;
    }
  if (mask & TOOL_COLOR_MASK_G && axes->priv->rgb[1] != rgb[1])
    {
      axes->priv->rgb[1] = rgb[1];
      diff = TRUE;
    }
  if (mask & TOOL_COLOR_MASK_B && axes->priv->rgb[2] != rgb[2])
    {
      axes->priv->rgb[2] = rgb[2];
      diff = TRUE;
    }
  if (!diff)
    return FALSE;

  axes->priv->areBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(axes));
}
/**
 * visu_gl_ext_axes_setLineWidth:
 * @axes: the #VisuGlExtAxes object to modify.
 * @width: value of the desired axe width.
 *
 * Method used to change the value of the parameter axes_line_width.
 *
 * Returns: TRUE if visu_gl_ext_axes_draw() should be called and then 'OpenGLAskForReDraw'
 *          signal be emitted.
 */
gboolean visu_gl_ext_axes_setLineWidth(VisuGlExtAxes *axes, float width)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);
  
  if (width < 1. || width > 10. || width == axes->priv->lineWidth)
    return FALSE;

  axes->priv->lineWidth = width;
  axes->priv->areBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(axes));
}
/**
 * visu_gl_ext_axes_setLineStipple:
 * @axes: the #VisuGlExtAxes object to modify.
 * @stipple: value of the desired pattern.
 *
 * Method used to change the value of the parameter axes_line_stipple.
 *
 * Returns: TRUE if visu_gl_ext_axes_draw() should be called and then 'OpenGLAskForReDraw'
 *          signal be emitted.
 */
gboolean visu_gl_ext_axes_setLineStipple(VisuGlExtAxes *axes, guint16 stipple)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);
  
  if (stipple == axes->priv->lineStipple)
    return FALSE;

  axes->priv->lineStipple = stipple;
  axes->priv->areBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(axes));
}
static void _setBasis(VisuGlExtAxes *axes, double matrix[3][3])
{
  double sum2;

  DBG_fprintf(stderr, "Extension Axes: %8g %8g %8g\n"
              "                %8g %8g %8g\n"
	      "                %8g %8g %8g\n",
	      matrix[0][0], matrix[0][1], matrix[0][2],
	      matrix[1][0], matrix[1][1], matrix[1][2],
	      matrix[2][0], matrix[2][1], matrix[2][2]);
  /* We normalise it and copy it. */
  sum2 = 1. / sqrt(matrix[0][0] * matrix[0][0] +
                   matrix[1][0] * matrix[1][0] +
                   matrix[2][0] * matrix[2][0]);
  axes->priv->matrix[0][0] = matrix[0][0] * sum2;
  axes->priv->matrix[0][1] = matrix[1][0] * sum2;
  axes->priv->matrix[0][2] = matrix[2][0] * sum2;
  sum2 = 1. / sqrt(matrix[0][1] * matrix[0][1] +
                   matrix[1][1] * matrix[1][1] +
                   matrix[2][1] * matrix[2][1]);
  axes->priv->matrix[1][0] = matrix[0][1] * sum2;
  axes->priv->matrix[1][1] = matrix[1][1] * sum2;
  axes->priv->matrix[1][2] = matrix[2][1] * sum2;
  sum2 = 1. / sqrt(matrix[0][2] * matrix[0][2] +
                   matrix[1][2] * matrix[1][2] +
                   matrix[2][2] * matrix[2][2]);
  axes->priv->matrix[2][0] = matrix[0][2] * sum2;
  axes->priv->matrix[2][1] = matrix[1][2] * sum2;
  axes->priv->matrix[2][2] = matrix[2][2] * sum2;

  axes->priv->areBuilt = FALSE;
}
/**
 * visu_gl_ext_axes_setBasis:
 * @axes: the #VisuGlExtAxes object to modify.
 * @matrix: the definition of the three basis axis.
 *
 * The @axes can represent an arbitrary basis-set, provided by
 * @matrix. @matrix[{0,1,2}] represents the {x,y,z} axis vector in a
 * cartesian basis-set. See visu_gl_ext_axes_setBasisFromBox() if the
 * basis-set should follow the one of a given #VisuBox.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_axes_draw() should be called and then 'OpenGLAskForReDraw'
 *          signal be emitted.
 **/
gboolean visu_gl_ext_axes_setBasis(VisuGlExtAxes *axes, double matrix[3][3])
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);

  _setBox(axes, (VisuBox*)0);
  _setBasis(axes, matrix);

  return visu_gl_ext_getActive(VISU_GL_EXT(axes));
}
/**
 * visu_gl_ext_axes_setBasisFromBox:
 * @axes: the #VisuGlExtAxes object to modify.
 * @box: (allow-none): the #VisuBox to use as basis-set.
 *
 * The @axes can follow the basis-set defined by @box. If NULL is
 * passed, then the orthorombic default basis-set is used.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_axes_draw() should be called and then 'OpenGLAskForReDraw'
 *          signal be emitted.
 **/
gboolean visu_gl_ext_axes_setBasisFromBox(VisuGlExtAxes *axes, VisuBox *box)
{
  double m[3][3];

  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);

  if (box)
    visu_box_getCellMatrix(box, m);
  else
    {
      memset(m, '\0', sizeof(double) * 9);
      m[0][0] = 1.;
      m[1][1] = 1.;
      m[2][2] = 1.;
    }
  _setBox(axes, box);
  _setBasis(axes, m);

  return visu_gl_ext_getActive(VISU_GL_EXT(axes));
}
/**
 * visu_gl_ext_axes_setPosition:
 * @axes: the #VisuGlExtAxes object to modify.
 * @xpos: the reduced x position (1 to the right).
 * @ypos: the reduced y position (1 to the bottom).
 *
 * Change the position of the axes representation.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_axes_draw() should be called and then 'OpenGLAskForReDraw'
 *          signal be emitted.
 **/
gboolean visu_gl_ext_axes_setPosition(VisuGlExtAxes *axes, float xpos, float ypos)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);

  if (xpos == axes->priv->xpos && ypos == axes->priv->ypos)
    return FALSE;
  axes->priv->xpos = CLAMP(xpos, 0.f, 1.f);
  axes->priv->ypos = CLAMP(ypos, 0.f, 1.f);

  axes->priv->areBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(axes));
}
/**
 * visu_gl_ext_axes_setGlView:
 * @axes: the #VisuGlExtAxes object to attached to rendering view.
 * @view: (transfer full) (allow-none): a #VisuGlView object.
 *
 * Attach @axes to @view, so it can be rendered there. See visu_gl_ext_axes_draw().
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_axes_draw() should be called and then 'OpenGLAskForReDraw'
 *          signal be emitted.
 **/
gboolean visu_gl_ext_axes_setGlView(VisuGlExtAxes *axes, VisuGlView *view)
{
  VisuRendering *spin;

  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), FALSE);

  /* No change to be done. */
  if (view == axes->priv->view)
    return FALSE;

  spin = visu_rendering_getByName(VISU_RENDERING_SPIN_NAME);
  if (axes->priv->view)
    {
      g_signal_handler_disconnect(G_OBJECT(axes->priv->view), axes->priv->nearFar_signal);
      g_signal_handler_disconnect(G_OBJECT(axes->priv->view), axes->priv->refLength_signal);
      g_signal_handler_disconnect(G_OBJECT(axes->priv->view), axes->priv->widthHeight_signal);
      if (spin)
        g_signal_handler_disconnect(G_OBJECT(spin), axes->priv->color_signal);
      g_object_unref(axes->priv->view);
    }
  if (view)
    {
      g_object_ref(view);
      axes->priv->nearFar_signal =
        g_signal_connect(G_OBJECT(view), "NearFarChanged",
                         G_CALLBACK(onAxesParametersChange), (gpointer)axes);
      axes->priv->refLength_signal =
        g_signal_connect(G_OBJECT(view), "RefLengthChanged",
                         G_CALLBACK(onAxesParametersChange), (gpointer)axes);
      axes->priv->widthHeight_signal =
        g_signal_connect(G_OBJECT(view), "WidthHeightChanged",
                         G_CALLBACK(onAxesParametersChange), (gpointer)axes);
      if (spin)
        axes->priv->color_signal =
          g_signal_connect(G_OBJECT(spin), "colorisationChange",
                           G_CALLBACK(onAxesConfigChange), (gpointer)axes);
      else
        axes->priv->color_signal = 0;
    }
  else
    {
      axes->priv->nearFar_signal     = 0;
      axes->priv->widthHeight_signal = 0;
      axes->priv->color_signal       = 0;
    }
  axes->priv->view = view;

  return TRUE;
}

/* Get methods. */
/**
 * visu_gl_ext_axes_getRGB:
 * @axes: the #VisuGlExtAxes object to inquire.
 *
 * Read all the colour components of axes (in [0;1]). 
 *
 * Returns: (array fixed-size=3) (transfer none): three RGB values,
 * private from V_Sim, read only.
 */
float* visu_gl_ext_axes_getRGB(VisuGlExtAxes *axes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), rgbDefault);
  
  return axes->priv->rgb;
}
/**
 * visu_gl_ext_axes_getLineWidth:
 * @axes: the #VisuGlExtAxes object to inquire.
 *
 * Read the line width used to draw axes.
 *
 * Returns: the value of current axes width.
 */
float visu_gl_ext_axes_getLineWidth(VisuGlExtAxes *axes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), LINE_WIDTH_DEFAULT);
  
  return axes->priv->lineWidth;
}
/**
 * visu_gl_ext_axes_getLineStipple:
 * @axes: the #VisuGlExtAxes object to inquire.
 *
 * Read the line pattern used to draw axes.
 *
 * Returns: the value of current axes pattern.
 */
guint16 visu_gl_ext_axes_getLineStipple(VisuGlExtAxes *axes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_AXES(axes), LINE_STIPPLE_DEFAULT);
  
  return axes->priv->lineStipple;
}
/**
 * visu_gl_ext_axes_getPosition:
 * @axes: the #VisuGlExtAxes object to inquire.
 * @xpos: (out) (allow-none): a location to store the x position.
 * @ypos: (out) (allow-none): a location to store the y position.
 *
 * Inquire the position of the representation of tha axes.
 *
 * Since: 3.7
 **/
void visu_gl_ext_axes_getPosition(VisuGlExtAxes *axes, float *xpos, float *ypos)
{
  g_return_if_fail(VISU_IS_GL_EXT_AXES(axes));

  if (xpos)
    *xpos = axes->priv->xpos;
  if (ypos)
    *ypos = axes->priv->ypos;
}
/**
 * visu_gl_ext_axes_getDefault:
 *
 * V_Sim is using a default axes object.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuGlExtAxes object used by default.
 **/
VisuGlExtAxes* visu_gl_ext_axes_getDefault()
{
  if (!defaultAxes)
    {
      defaultAxes = visu_gl_ext_axes_new((gchar*)0);
      visu_gl_ext_setActive(VISU_GL_EXT(defaultAxes), RESOURCE_AXES_USED_DEFAULT);
    }
  return defaultAxes;
}

/****************/
/* Private part */
/****************/
static void visu_gl_ext_axes_rebuild(VisuGlExt *ext)
{
  VisuGlExtAxes *axes = VISU_GL_EXT_AXES(ext);

  visu_gl_text_rebuildFontList();
  axes->priv->areBuilt = FALSE;
  visu_gl_ext_axes_draw(axes);
}
static void onAxesConfigChange(VisuRenderingSpin *render _U_, gpointer data)
{
  VisuGlExtAxes *axes = VISU_GL_EXT_AXES(data);

  DBG_fprintf(stderr, "Extension Axes: caught change on axes config.\n");
  axes->priv->areBuilt = FALSE;
  visu_gl_ext_axes_draw(axes);
}
static void onAxesParametersChange(VisuGlView *view _U_, gpointer data)
{
  VisuGlExtAxes *axes = VISU_GL_EXT_AXES(data);

  DBG_fprintf(stderr, "Extension Axes: caught change on view.\n");
  axes->priv->areBuilt = FALSE;
  visu_gl_ext_axes_draw(axes);
}
static void onBoxChange(VisuBox *box, float extens _U_, gpointer data)
{
  VisuGlExtAxes *axes = VISU_GL_EXT_AXES(data);
  double m[3][3];

  /* We copy the definition of the box. */
  visu_box_getCellMatrix(box, m);
  _setBasis(axes, m);
  visu_gl_ext_axes_draw(axes);
}
static void onEntryUsed(VisuGlExtAxes *axes, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_setActive(VISU_GL_EXT(axes), RESOURCE_AXES_USED_DEFAULT);
}
static void onEntryColor(VisuGlExtAxes *axes, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_axes_setRGB(axes, rgbDefault, TOOL_COLOR_MASK_RGBA);
}
static void onEntryWidth(VisuGlExtAxes *axes, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_axes_setLineWidth(axes, LINE_WIDTH_DEFAULT);
}
static void onEntryStipple(VisuGlExtAxes *axes, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_axes_setLineStipple(axes, LINE_STIPPLE_DEFAULT);
}
static void onEntryPosition(VisuGlExtAxes *axes, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_axes_setPosition(axes, POSITION_DEFAULT[0], POSITION_DEFAULT[1]);
}

static void drawAxes(float length, double matrix[3][3], GLsizei w, GLsizei h, float width,
		     float rgb[3], char *legend, gboolean long_axes) 
{
  double orig[3][3];

  if (long_axes)
    {
      orig[0][0] = -matrix[0][0];
      orig[0][1] = -matrix[0][1];
      orig[0][2] = -matrix[0][2];
      orig[1][0] = -matrix[1][0];
      orig[1][1] = -matrix[1][1];
      orig[1][2] = -matrix[1][2];
      orig[2][0] = -matrix[2][0];
      orig[2][1] = -matrix[2][1];
      orig[2][2] = -matrix[2][2];
    }
  else
    memset(orig, '\0',  sizeof(double) * 9);

  glLineWidth(width);
  glColor3fv(rgb);
  glPushMatrix();
  glScalef(length, length, length);
  glBegin(GL_LINES);
  glVertex3dv(orig[0]); glVertex3dv(matrix[0]);
  glVertex3dv(orig[1]); glVertex3dv(matrix[1]);
  glVertex3dv(orig[2]); glVertex3dv(matrix[2]);
  glEnd();

  glRasterPos3dv(matrix[0]); visu_gl_text_drawChars("x", VISU_GL_TEXT_NORMAL); 
  glRasterPos3dv(matrix[1]); visu_gl_text_drawChars("y", VISU_GL_TEXT_NORMAL); 
  glRasterPos3dv(matrix[2]); visu_gl_text_drawChars("z", VISU_GL_TEXT_NORMAL);

  glPopMatrix();

  if(legend != NULL)
    {
      glMatrixMode (GL_PROJECTION);
      glPushMatrix();
      glLoadIdentity ();  
      gluOrtho2D (0, MIN(w,h), 0, MIN(h,w));
      glMatrixMode (GL_MODELVIEW);
      glPushMatrix();
      glLoadIdentity ();  
      glRasterPos3f(20., 5., 0.9); visu_gl_text_drawChars(legend, VISU_GL_TEXT_NORMAL);
      glPopMatrix();
      glMatrixMode(GL_PROJECTION);
      glPopMatrix();
      glMatrixMode(GL_MODELVIEW);
    }
}

/**
 * draw_coloured_cone:
 * @r: the radius of the cone
 * @h: the semi-height of the cone
 * @n: the precision used to draw the sphere
 *
 * Draws a coloured double cone at the given position.
 */ 
static void draw_coloured_cone(double r, double h, int n, float phi_prime_zero)
{
  float hsv[3], rgb[3];
  int i,j;
  double theta1,theta2,theta3;
  float e_x, e_y, e_z, p_x, p_y, p_z;

  g_return_if_fail(r >= 0 && n >= 0);
  
  if (n < 4 || r <= 0) 
    {
      glBegin(GL_POINTS);
      glVertex3f(0,0,0);
      glEnd();
      return;
    }

  glFrontFace(GL_CW);

  glPushMatrix();
  glRotatef(phi_prime_zero, 0, 0, 1);

  glRotatef(-90, 1, 0, 0);

  hsv[1] = 0;
  hsv[2] = 1;

  for (j=0;j<n/2;j++) 
    {
      theta1 = j * 2 * G_PI / n - G_PI_2;
      theta2 = (j + 1) * 2 * G_PI / n - G_PI_2;
      
      glBegin(GL_QUAD_STRIP);
      for (i=0;i<=n;i++) 
	{
	  theta3 = i * 2 * G_PI / n;
	  
	  hsv[0] = /*1-*/(float)i/(float)n;

	  hsv[1] = 2*(float)(1+j)/(float)(n/2);
	  if(hsv[1] > 1) hsv[1] = 1;
      
	  hsv[2] = 2-2*(float)(1+j)/(float)(n/2); 
	  if(hsv[2] > 1) hsv[2] = 1; 

	  e_x = /*cos(theta2) **/hsv[1]*hsv[2]* cos(theta3);
	  e_y = sin(theta2);
	  e_z = /*cos(theta2) **/hsv[1]*hsv[2]* sin(theta3);
	  p_x = r * e_x;
	  p_y = /*r * e_y*/h*(hsv[1] - hsv[2]);
	  p_z = r * e_z;
	  
	  tool_color_convertHSVtoRGB(rgb, hsv);
	  glColor3f(rgb[0], rgb[1], rgb[2]); 
	  glNormal3f(e_x,e_y,e_z);
	  glVertex3f(p_x,p_y,p_z);
	  
	  hsv[0] = /*1-*/(float)i/(float)n;

	  hsv[1] = 2*(float)j/(float)(n/2);
	  if(hsv[1] > 1) hsv[1] = 1;
      
	  hsv[2] = 2-2*(float)j/(float)(n/2); 
	  if(hsv[2] > 1) hsv[2] = 1; 

	  e_x = /*cos(theta1) **/hsv[1]*hsv[2]* cos(theta3);
       	  e_y = sin(theta1);
	  e_z = /*cos(theta1) **/hsv[1]*hsv[2]* sin(theta3);
	  p_x = r * e_x;
	  p_y = /*r * e_y*/h*(hsv[1] - hsv[2]);
	  p_z = r * e_z;
	
	  tool_color_convertHSVtoRGB(rgb, hsv);
	  glColor3f(rgb[0], rgb[1], rgb[2]); 
	  glNormal3f(e_x,e_y,e_z);
	  glVertex3f(p_x,p_y,p_z);

	}
      glEnd();
    }
  glPopMatrix();
  glFrontFace(GL_CCW);
  
}

/* void drawConeCircle(/\*XYZ c,*\/ double r, int n)  */
/* { */
/*   /\*  XYZ e,p;*\/ */
/*   GLboolean antialiasing_was_on; */
/*   GLUquadric* trash = gluNewQuadric(); */
/*   int i; */
/*   double theta, height = 1; */

/* /\*   glEnable (GL_POLYGON_SMOOTH); *\/ */
/* /\*   glDisable (GL_DEPTH_TEST); *\/ */

/*   antialiasing_was_on = enableGlFeature(GL_LINE_SMOOTH); */
/*   glPushMatrix(); */
/*   glRotatef(90, 1, 0, 0); */
/*   glTranslatef(0, 0, -height/2); */
/*   glLineWidth(lineWidth); */
/*   glColor3f(0, 0, 0);  */
/*   gluQuadricOrientation(trash, GLU_INSIDE); */
/*   gluCylinder(trash, 0.95*r, 0.95*r, height, n, 1); */
/* /\*   glBegin(GL_LINE_LOOP); *\/ */
/* /\*   for (i=0;i<=n;i++)  *\/ */
/* /\*     { *\/ */
/* /\*       theta = i * TWOPI / n; *\/ */
/* /\*       e.x = cos(theta); *\/ */
/* /\*       e.z = sin(theta); *\/ */
/* /\*       p.x = 0.8*(c.x +  r * e.x); *\/ */
/* /\*       p.z = 0.8*(c.z +  r * e.z); *\/ */
/* /\*       glVertex3f(p.x, -0.6, p.z); *\/ */
/* /\*       glVertex3f(p.x, 0.6, p.z); *\/ */
/* /\*     } *\/ */
/* /\*   glEnd(); *\/ */
/*   glPopMatrix(); */
/*   restoreGlFeature(GL_LINE_SMOOTH, antialiasing_was_on); */
/* /\*   glEnable (GL_DEPTH_TEST); *\/ */
/* /\*   glDisable(GL_POLYGON_SMOOTH); *\/ */

/* } */

/**
 * visu_gl_ext_axes_draw:
 * @axes: the #VisuBox object to build axes for.
 *
 * This method creates a compiled list that draws axes.
 */
void visu_gl_ext_axes_draw(VisuGlExtAxes *axes)
{
  float length;
  GLsizei w, h;
  GLint xx, yy;
  double mini, maxi, near, far;
  float length0;
  VisuRendering *spin;

  g_return_if_fail(VISU_IS_GL_EXT_AXES(axes));

  /* Nothing to draw; */
  if(!axes->priv->view ||
     !visu_gl_ext_getActive(VISU_GL_EXT(axes)) ||
     axes->priv->areBuilt) return;

  DBG_fprintf(stderr, "Extension axes: creating axes in (%dx%d).\n",
	      axes->priv->view->window->width, axes->priv->view->window->height);

  length0 = visu_gl_camera_getRefLength(axes->priv->view->camera, (ToolUnits*)0);
  DBG_fprintf(stderr, " | refLength = %g\n", length0);
  DBG_fprintf(stderr, " | window = %dx%d\n",
              axes->priv->view->window->width, axes->priv->view->window->height);
  w = 0.16f * MIN(axes->priv->view->window->width, axes->priv->view->window->height);
  h = w;
  xx = (axes->priv->view->window->width - w) * axes->priv->xpos;
  yy = (axes->priv->view->window->height - h) * (1.f - axes->priv->ypos);
  mini = -0.5f * length0 *
    (axes->priv->view->camera->d_red - 1.f) / axes->priv->view->camera->d_red;
  maxi = -mini;
  near = far = axes->priv->view->camera->d_red * length0;
  near -= length0;
  far  += length0;
  DBG_fprintf(stderr, " | near/far = %g %g\n", near, far);

  visu_gl_text_initFontList();
  
  glDeleteLists(visu_gl_ext_getGlList(VISU_GL_EXT(axes)), 1);
  glNewList(visu_gl_ext_getGlList(VISU_GL_EXT(axes)), GL_COMPILE);

  /* D�sactivation de la lumi�re et du brouillard et activation du culling. */
  glEnable(GL_CULL_FACE);
  glDisable(GL_LIGHTING);
  glDisable(GL_FOG);

  if (axes->priv->lineStipple != 65535)
    {
      glEnable(GL_LINE_STIPPLE);
      glLineStipple(1, axes->priv->lineStipple);
    }

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  DBG_fprintf(stderr, "Extension axes: frustum is %fx%f %fx%f %fx%f.\n",
	      mini, maxi, mini, maxi, near, far);
  glFrustum(mini, maxi, mini, maxi, near, far);
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  DBG_fprintf(stderr, "Extension Axes: new view port at %dx%d, size %dx%d.\n",
              xx, yy, w, h);
  glViewport(xx, yy, w, h);

  length = 0.33 * length0;
  spin = visu_rendering_getByName(VISU_RENDERING_SPIN_NAME);
  if(visu_object_getRendering(VISU_OBJECT_INSTANCE) == spin)
    {
      float phi_prime_zero, phi_a, theta_a;

      g_object_get(G_OBJECT(spin), "cone-omega", &phi_prime_zero,
                   "cone-phi", &phi_a, "cone-theta", &theta_a, NULL);

      /* Resetting the depth buffer. */
      glClear(GL_DEPTH_BUFFER_BIT);
      glEnable(GL_DEPTH_TEST);

      /* Draw the first color cone */
      glPushMatrix();
      glRotatef(phi_a, 0, 0, 1);
      glRotatef(theta_a, 0, 1, 0);
      draw_coloured_cone(length, 1.2*length, 16, phi_prime_zero);
      glPopMatrix();

      drawAxes(1.5*length, axes->priv->matrix, w, h, axes->priv->lineWidth, axes->priv->rgb, _("front"), TRUE);

      glViewport(xx, yy+h, w, h);

      /* Enabling front culling and drawing the second color cone */
      glPushMatrix();
      glRotatef(phi_a, 0, 0, 1);
      glRotatef(theta_a, 0, 1, 0);
      glCullFace(GL_FRONT);
      draw_coloured_cone(length, 1.2*length, 16, phi_prime_zero);    
      glCullFace(GL_BACK);
      glPopMatrix();

      drawAxes(1.5*length, axes->priv->matrix, w, h, axes->priv->lineWidth, axes->priv->rgb, _("back"), TRUE);
    }
  else if(visu_object_getRendering(VISU_OBJECT_INSTANCE) ==
          visu_rendering_getByName(VISU_RENDERING_ATOMIC_NAME))
    {
      glDisable(GL_DEPTH_TEST);
      drawAxes(length, axes->priv->matrix, w, h, axes->priv->lineWidth, axes->priv->rgb, NULL, FALSE);
      glEnable(GL_DEPTH_TEST);
    }
  glPushMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  /* Back to the main viewport. */
  glViewport(0, 0, axes->priv->view->window->width, axes->priv->view->window->height);

  glEndList();

  axes->priv->areBuilt = TRUE;
}

/*************************/
/* Parameters & resources*/
/*************************/
/* A resource to control the width to render the lines of the axes. */
static gboolean readAxesLineStipple(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				    int position, VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  gint stipple;
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readInteger(lines[0], position, &stipple, 1, error))
    return FALSE;
  LINE_STIPPLE_DEFAULT = (guint16)stipple;

  return TRUE;
}

/* Export function that is called by visu_module to write the
   values of resources to a file. */
static void exportResourcesAxes(GString *data,
                                VisuData *dataObj _U_, VisuGlView *view _U_)
{
  if (!defaultAxes)
    return;

  visu_config_file_exportComment(data, DESC_RESOURCE_AXES_USED);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_AXES_USED, NULL,
                               "%d", visu_gl_ext_getActive(VISU_GL_EXT(defaultAxes)));

  visu_config_file_exportComment(data, DESC_RESOURCE_AXES_COLOR);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_AXES_COLOR, NULL,
                               "%4.3f %4.3f %4.3f", defaultAxes->priv->rgb[0],
                               defaultAxes->priv->rgb[1], defaultAxes->priv->rgb[2]);

  visu_config_file_exportComment(data, DESC_RESOURCE_AXES_LINE);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_AXES_LINE, NULL,
                               "%4.0f", defaultAxes->priv->lineWidth);

  visu_config_file_exportComment(data, DESC_RESOURCE_AXES_STIPPLE);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_AXES_STIPPLE, NULL,
                               "%d", defaultAxes->priv->lineStipple);

  visu_config_file_exportComment(data, DESC_RESOURCE_AXES_POSITION);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_AXES_POSITION, NULL,
                               "%4.3f %4.3f", defaultAxes->priv->xpos,
                               defaultAxes->priv->ypos);

  visu_config_file_exportComment(data, "");
}
