/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_DATA_H
#define VISU_DATA_H

#include <glib.h>
#include <glib-object.h>

#include "visu_tools.h"
#include "visu_elements.h"
#include "visu_nodes.h"
#include "visu_box.h"
#include "iface_boxed.h"
#include "coreTools/toolPhysic.h"
#include "coreTools/toolFileFormat.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_DATA:
 *
 * return the type of #VisuData.
 */
#define VISU_TYPE_DATA	     (visu_data_get_type ())
/**
 * VISU_DATA:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuData type.
 */
#define VISU_DATA(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_DATA, VisuData))
/**
 * VISU_DATA_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuDataClass.
 */
#define VISU_DATA_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_DATA, VisuDataClass))
/**
 * VISU_IS_DATA:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuData object.
 */
#define VISU_IS_DATA(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_DATA))
/**
 * VISU_IS_DATA_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuDataClass class.
 */
#define VISU_IS_DATA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_DATA))
/**
 * VISU_DATA_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_DATA_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_DATA, VisuDataClass))

typedef struct _VisuDataPrivate VisuDataPrivate;
typedef struct _VisuData VisuData;
struct _VisuData
{
  VisuNodeArray parent;

  VisuDataPrivate *priv;
};

/**
 * VisuDataClass:
 * @parent: the parent class.
 *
 * A short way to identify #_VisuDataClass structure.
 */
typedef struct _VisuDataClass VisuDataClass;
struct _VisuDataClass
{
  VisuNodeArrayClass parent;
};

/**
 * VisuDataColorFunc:
 * @visuData: a pointer to the calling object ;
 * @ele: a #VisuElement ;
 * @node: a #VisuNode ;
 * @rgba: (in) (array fixed-size=4): an 4 allocated float area to store the return values.
 *
 * This prototype is used to specify an optional method
 * to associate a color with external values to each node.
 *
 * Returns: TRUE if the node should take colour read in @rgba.
 */
typedef gboolean (*VisuDataColorFunc)(VisuData *visuData, float rgba[4], VisuElement *ele, VisuNode* node);
/**
 * VisuDataScalingFunc:
 * @visuData: a pointer to the calling object ;
 * @node: a #VisuNode ;
 *
 * Interface for routine that need to rescale @node before drawing
 * them.
 *
 * Returns: the scaling factor.
 */
typedef float (*VisuDataScalingFunc)(VisuData *visuData, VisuNode* node);

/**
 * visu_data_get_type:
 *
 * This method returns the type of #VisuData, use VISU_TYPE_DATA instead.
 *
 * Returns: the type of #VisuData.
 */
GType visu_data_get_type(void);

VisuData* visu_data_new(void);
VisuData* visu_data_new_withFiles(const gchar **files);
void visu_data_freePopulation(VisuData *data);

/* Methods to deal with file representation of VisuData objects. */
void visu_data_addFile(VisuData *data, const gchar* file,
                       int kind, ToolFileFormat *format);
void visu_data_removeAllFiles(VisuData *data);
const gchar* visu_data_getFile(VisuData *data, int kind,
                               ToolFileFormat **format);
gchar* visu_data_getFilesAsLabel(const VisuData *data);

void visu_data_setFileCommentary(VisuData *data, gchar* commentary, gint iSet);
gchar* visu_data_getFileCommentary(VisuData *data, gint iSet);

void visu_data_setNSubset(VisuData *data, int nSet);
int visu_data_getNSubset(VisuData *data);
void visu_data_setISubset(VisuData *data, int iSet);
int visu_data_getISubset(VisuData *data);

void visu_data_setChangeElementFlag(VisuData *data, gboolean changeElement);
gboolean visu_data_getChangeElementFlag(VisuData *data);

gfloat visu_data_getAllNodeExtens(VisuData *dataObj, VisuBox *box);

VisuNode* visu_data_addNodeFromElement(VisuData *data, VisuElement *ele,
                                       float xyz[3], gboolean reduced,
                                       gboolean emitSignal);
VisuNode* visu_data_addNodeFromElementName(VisuData *data, const gchar *name,
                                           float xyz[3], gboolean reduced,
                                           gboolean emitSignal);
VisuNode* visu_data_addNodeFromIndex(VisuData *data, guint position,
                                     float xyz[3], gboolean reduced, gboolean emitSignal);

void visu_data_setColorFunc(VisuData *data, VisuDataColorFunc func);
gboolean visu_data_hasUserColorFunc(VisuData *data);
gboolean visu_data_getUserColor(VisuData *data, VisuElement *ele,
				VisuNode *node, float rgba[4]);

float* visu_data_getXYZtranslation(VisuData* data);
gboolean visu_data_setXYZtranslation(VisuData* data, float xyz[3]);
gboolean visu_data_forceXYZtranslation(VisuData* data, float xyz[3]);
gboolean visu_data_getTranslationStatus(VisuData *data);

VisuBox* visu_data_setTightBox(VisuData *data);

gboolean visu_data_getNodeBoxFromNumber(VisuData *data, guint nodeId, int nodeBox[3]);
gboolean visu_data_getNodeBoxFromCoord(VisuData *data, float xcart[3], int nodeBox[3]);

gboolean visu_data_constrainedElementInTheBox(VisuData *data, VisuElement *element);
gboolean visu_data_constrainedInTheBox(VisuData *data);
gboolean visu_data_constrainedFree(VisuData *data);

gboolean visu_data_replicate(VisuData *data, float extension[3]);
gboolean visu_data_restore(VisuData *data);

void visu_data_getNodeCoordinates(VisuData *data, VisuNode *node, float *x, float *y, float *z);
void visu_data_getNodePosition(VisuData *data, VisuNode *node, float coord[3]);
void visu_data_getNodeUserPosition(VisuData *data, VisuNode *node, float coord[3]);


VisuNodeInfo* visu_data_getDistanceList(VisuData *data, guint nodeId, float *minVal);
gboolean visu_data_setNewBasisFromNodes(VisuData *data, guint nO, guint nA, guint nB, guint nC);
gboolean visu_data_setNewBasis(VisuData *data, float matA[3][3], float O[3]);
gboolean visu_data_reorder(VisuData *data, VisuData *dataRef);

void visu_data_setNodeScalingFunc(VisuData *data, VisuDataScalingFunc scaling);
float visu_data_getNodeScalingFactor(VisuData *data, VisuNode *node);

guint visu_data_addTimeout(VisuData *data, guint time, GSourceFunc func, gpointer user_data);
gboolean visu_data_removeTimeout(VisuData *data, guint timeoutId);

GList* visu_data_class_getAllObjects(void);

G_END_DECLS

#endif
