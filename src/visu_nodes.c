/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "visu_nodes.h"

#include <stdlib.h>
#include <string.h>

#include "visu_tools.h"
#include "visu_rendering.h"
#include "visu_object.h"

/**
 * SECTION:visu_nodes
 * @short_description: Defines the elementary structure to store
 * informations about an element in a box.
 *
 * <para>In V_Sim, elements are drawn in a box. The #VisuNode
 * structure is used to represent an instance of an element position
 * somewhere in the box. This element can have several characteristics
 * such as its translation or its visibility.</para>
 *
 * <para>All nodes are stored in a #VisuData object in a two
 * dimensional array. The first dimension is indexed by the
 * #VisuElement of the node and the second corresponds to the number
 * of this node for this element. When a node is own by a #VisuData,
 * the two integers, that control the indexes in this array, are not
 * negative. See the #VisuNode structure for further
 * explanations.</para>
 *
 * <para>The only basic informations own by the #VisuNode structure is
 * basicaly its position. To add further informations (such as
 * orientation for the spin), define a node property using
 * visu_node_array_property_newPointer().</para>
 */

/**
 * VisuNode:
 * @xyz: (in) (array fixed-size=3): an array of three floating point values that positions the node in (x, y, z) ;
 * @translation: (in) (array fixed-size=3): an array of three floating point values that translates the
 *               node to its drawn position from (x, y, z) ;
 * @number: an integer that corresponds to its position in the entry file, it references
 *          also the node itself in the array 'fromNumberToVisuNode' of the #VisuData
 *          that contains the node ;
 * @posElement: an integer that is the position of the #VisuElement of the node
 *              in the array 'fromIntToVisuElement' of the #VisuData object that
 *              contains the node ;
 * @posNode: an integer that is the position of the node itself in the array
 *           'nodes' of the #VisuData object that contains the node ;
 * @rendered: a boolean to store if the node is drwn or not.
 *
 * Structure to store primary data of a node.
 */

/* Local routines. */
static gpointer node_copy(gpointer boxed);
static void freeNodePropStruct(gpointer data);
static void removeNodeProperty(gpointer key, gpointer value, gpointer data);
static void removeNodePropertyForElement(gpointer key, gpointer value, gpointer data);
static void reallocNodeProperty(gpointer key, gpointer value, gpointer data);
static void allocateNodeProp(gpointer key, gpointer value, gpointer data);
static void createNodeproperty(gpointer key, gpointer value, gpointer data);
static VisuNode* newOrCopyNode(VisuNodeArray *nodeArray, int iEle, int oldNodeId);
static void freeElePropStruct(gpointer data);

/* The number of nodes to be reallocated each time the visu_node_array_getNewNode()
   is called. */
#define REALLOCATION_STEP 100

/* The key of the original node property. */
#define ORIGINAL_ID       "originalId"

struct twoNodes
{
  VisuNode *oldNode;
  VisuNode *newNode;
};

struct _VisuNodeProperty
{
  /* A label to define the property. */
  gchar *name;

  /* A pointer to the array of nodes these properties are related to. */
  VisuNodeArray *array;

  /* The type of the property. */
  GType gtype;

  /* This table has the same size and structure
     than the node array object it is related to.
     Only one of the following data array is allocated. */
  gpointer **data_pointer;
  int **data_int;

  /* In the case of pointer data, one can give the new, copy and free routine. */
  /* This method is called for each stored token,
     if not NULL when the table is freed. */
  GFunc freeTokenFunc;
  /* This method is used to create/copy a token of the data array. */
  GCopyFunc newOrCopyTokenFunc;
  /* This value stores a pointer on a user data given when
     the object is created. This pointer is given to the copy
     or the free function. */
  gpointer user_data;
};
struct _ElementProperty
{
  VisuNodeArrayElementPropertyInit init;
  GValueArray *array;
};

GType visu_node_get_type(void)
{
  static GType g_define_type_id = 0;

  if (g_define_type_id == 0)
    g_define_type_id = g_boxed_type_register_static("VisuNode", node_copy, g_free);
  return g_define_type_id;
}

/**
 * visu_node_setCoordinates:
 * @node: a #VisuNode object ;
 * @xyz: (array fixed-size=3): new cartesian coordinates.
 *
 * This method is used to change coordinates of @node.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the calling method should emit
 * #VisuNodeArray::PositionChanged signal.
 */
gboolean visu_node_setCoordinates(VisuNode* node, float xyz[3])
{
  g_return_val_if_fail(node, FALSE);

  if (node->xyz[0] == xyz[0] && node->xyz[1] == xyz[1] && node->xyz[2] == xyz[2])
    return FALSE;
  node->xyz[0] = xyz[0];
  node->xyz[1] = xyz[1];
  node->xyz[2] = xyz[2];
  return TRUE;
}

/**
 * visu_node_setVisibility:
 * @node: a #VisuNode object ;
 * @visibility: a boolean.
 *
 * This method is used to turn on or off the drawing of the specified node.
 *
 * Returns: true if the calling method should emit the
 * VisuNodeArray::VisibilityChanged signal.
 */
gboolean visu_node_setVisibility(VisuNode* node, gboolean visibility)
{
  g_return_val_if_fail(node, FALSE);

  if (node->rendered == visibility)
    return FALSE;
  node->rendered = visibility;
  return TRUE;
}

/**
 * visu_node_getVisibility:
 * @node: a #VisuNode object.
 *
 * This method is used get the status of the drawing state of a node.
 *
 * Returns: true if the node is rendered, false otherwise.
 */
gboolean visu_node_getVisibility(VisuNode* node)
{
  g_return_val_if_fail(node, FALSE);

  return node->rendered;
}

/**
 * visu_node_newValues:
 * @node: an allocated #VisuNode object ;
 * @xyz: (in) (array fixed-size=3): the coordinates to set.
 * 
 * Set the coordinates and set all other values to default.
 */
void visu_node_newValues(VisuNode *node, float xyz[3])
{
  g_return_if_fail(node);

  DBG_fprintf(stderr, "Visu Node: set new position for node %d (%g;%g;%g).\n",
              node->number, xyz[0], xyz[1], xyz[2]);
  node->xyz[0]         = xyz[0];
  node->xyz[1]         = xyz[1];
  node->xyz[2]         = xyz[2];
  node->translation[0] = 0.;
  node->translation[1] = 0.;
  node->translation[2] = 0.;
  node->rendered       = TRUE;
}

static gpointer node_copy(gpointer boxed)
{
  VisuNode *node;

  node = g_malloc(sizeof(VisuNode));
  DBG_fprintf(stderr, "Visu Node: copying node %p to %p.\n", boxed, (gpointer)node);
  visu_node_copy(node, (VisuNode*)boxed);

  return (gpointer)node;
}
/**
 * visu_node_copy:
 * @nodeTo: an allocated #VisuNode object ;
 * @nodeFrom: an allocated #VisuNode object.
 * 
 * Copy all attributes of the object @nodeFrom to @nodeTo.
 */
void visu_node_copy(VisuNode *nodeTo, VisuNode *nodeFrom)
{
  g_return_if_fail(nodeTo && nodeFrom);

  *nodeTo = *nodeFrom;
}



/***************/
/* Node Arrays */
/***************/

/**
 * VisuNodeArray:
 *
 * Opaque structure to stores #VisuNodeArray object.
 */

/**
 * VisuNodeArrayClass:
 * @parent: private.
 *
 * Class structure of #VisuNodeArray objects.
 */

typedef struct _eleArr
{
  VisuElement *ele;
  /* Listeners on VisuElement signals. */
  gulong rendered, material, rendering;
  /* Number of nodes allocated (size of the nodes array) and number of
     nodes physically present in the array for this element. */
  guint nNodes, nStoredNodes;
  /* Allocated space for nodes of this element. */
  VisuNode *nodes;
} EleArr;

typedef struct _nodeTable
{
  /* A counter. */
  guint idCounter;
  /* This array gives access to the good VisuNode
     when one has its number. This number is an integer ranging in
     [0;idCounter[. This value is readable in the #VisuNode structure
     as the number attribute. The current allocated size is stored in
     @nNodes. */
  /* GArray *array. */
  VisuNode **array;
  /* The total of allocated VisuNodes. */
  guint nNodes;
  /* The total of stored VisuNodes. */
  guint nStoredNodes;
} NodeTable;

/* Local methods. */
static void onAskForShowHideSignal(VisuNodeArray *nodeArray,
                                   gboolean *redraw, gpointer data);
static void onElementRenderChanged(VisuNodeArray *data, VisuElement *element);
static void onElementMaterialChanged(VisuNodeArray *data, VisuElement *element);
static void onElementRenderingChanged(VisuNodeArray *data, VisuElement *element);
static void _freeNodes(VisuNodeArray *nodeArray);
#define _getEleArr(dataObj, i) (&g_array_index(dataObj->priv->elements, EleArr, i))
#define _getElement(dataObj, i) _getEleArr(dataObj, i)->ele
static void allocateEleProp(gpointer key, gpointer value, gpointer data);
/* static void freeEleProp(gpointer key, gpointer value, gpointer data); */

/**
 * VisuNodeArrayPrivate:
 *
 * Opaque structure to store private attributes of #VisuNodeArray objects.
 */
struct _VisuNodeArrayPrivate
{
  gboolean dispose_has_run;

  /****************/
  /* The elements */
  /****************/
  /* Stores for each element some related informations (see EleArr). */
  GArray *elements;

  /*************/
  /* The nodes */
  /*************/
  NodeTable nodeTable;

  /***********************/
  /* The property arrays */
  /***********************/
  /* Properties of elements. */
  GHashTable *eleProp;
  /* This is a table to store data, reachable with string keys.
     It should be accessed via visu_node_setproperty()
     and visu_node_array_getProperty(). */
  GHashTable *nodeProp;

  /* A convenient pointer on the original node property. */
  VisuNodeProperty *origProp;
  guint nOrigNodes;
};

enum {
  ASK_FOR_SHOW_HIDE_SIGNAL,
  POPULATION_DEFINED_SIGNAL,
  POPULATION_INCREASE_SIGNAL,
  POPULATION_DECREASE_SIGNAL,
  POSITION_CHANGED_SIGNAL,
  VISIBILITY_CHANGED_SIGNAL,
  MATERIAL_CHANGED_SIGNAL,
  RENDERING_CHANGED_SIGNAL,
  ELEMENT_RENDERING_CHANGED_SIGNAL,
  LAST_SIGNAL
};
static guint visu_node_array_signals[LAST_SIGNAL] = { 0 };

static void visu_node_array_dispose     (GObject* obj);
static void visu_node_array_finalize    (GObject* obj);

G_DEFINE_TYPE(VisuNodeArray, visu_node_array, G_TYPE_OBJECT)

static void visu_node_array_class_init(VisuNodeArrayClass *klass)
{
  DBG_fprintf(stderr, "Visu NodeArray: creating the class of the object.\n");
  DBG_fprintf(stderr, "                - adding new signals ;\n");
  /**
   * VisuNodeArray::AskForShowHide:
   * @nodes: the object which received the signal ;
   * @redraw: (out caller-allocates) (type gboolean): a location on a boolean.
   *
   * Gets emitted when external modules should recompute their masking
   * effect on nodes. Location pointed by @redraw must be set to TRUE
   * if the visibility of at least one node is changed.
   *
   * Since: 3.2
   */
  visu_node_array_signals[ASK_FOR_SHOW_HIDE_SIGNAL] =
    g_signal_new("AskForShowHide", G_TYPE_FROM_CLASS (klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0 , NULL, NULL, g_cclosure_marshal_VOID__POINTER,
                 G_TYPE_NONE, 1, G_TYPE_POINTER);
  /**
   * VisuNodeArray::PopulationDefined:
   * @nodes: the object which received the signal ;
   * @nEle: the actual allocated number of #VisuElement of @nodes.
   *
   * Gets emitted when the population of nodes is created or
   * destroyed. It is possible then to associate new #VisuNodeProperty
   * for instance.
   *
   * Since: 3.5
   */
  visu_node_array_signals[POPULATION_DEFINED_SIGNAL] =
    g_signal_new("PopulationDefined", G_TYPE_FROM_CLASS (klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL, NULL, g_cclosure_marshal_VOID__UINT,
                 G_TYPE_NONE, 1, G_TYPE_UINT);
  /**
   * VisuNodeArray::PopulationDecrease:
   * @nodes: the object which received the signal ;
   * @ids: (element-type gint) (array): an array of #VisuNode ids.
   *
   * Gets emitted when the number of nodes has changed,
   * decreasing. @ids contains all removed ids and is -1 terminated.
   * When emitted, nodes have already been removed, so no external
   * routines should keep pointers on these nodes.
   *
   * Since: 3.4
   */
  visu_node_array_signals[POPULATION_DECREASE_SIGNAL] =
    g_signal_new("PopulationDecrease", G_TYPE_FROM_CLASS (klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL, NULL, g_cclosure_marshal_VOID__POINTER,
                 G_TYPE_NONE, 1, G_TYPE_POINTER);
  /**
   * VisuNodeArray::PopulationIncrease:
   * @nodes: the object which received the signal ;
   * @ids: (element-type gint) (array): an array of #VisuNode ids.
   *
   * Gets emitted when the number of nodes has changed,
   * increasing. @ids contains all new ids and is -1 terminated.
   *
   * Since: 3.4
   */
  visu_node_array_signals[POPULATION_INCREASE_SIGNAL] =
    g_signal_new("PopulationIncrease", G_TYPE_FROM_CLASS (klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL, NULL, g_cclosure_marshal_VOID__POINTER,
                 G_TYPE_NONE, 1, G_TYPE_POINTER);
  /**
   * VisuNodeArray::PositionChanged:
   * @nodes: the object which received the signal ;
   * @ele: (allow-none): the #VisuElement nodes have moved or NULL for
   * all elements.
   *
   * Gets emitted when one or more nodes have moved, because of
   * translations or because the user has moved them manually.
   *
   * Since: 3.2
   */
  visu_node_array_signals[POSITION_CHANGED_SIGNAL] =
    g_signal_new("PositionChanged", G_TYPE_FROM_CLASS (klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
                 G_TYPE_NONE, 1, G_TYPE_OBJECT, NULL);
  /**
   * VisuNodeArray::VisibilityChanged:
   * @nodes: the object which received the signal ;
   *
   * Gets emitted when one or more nodes have changed of
   * visibility. Some may have appeared, some may have disappeared.
   *
   * Since: 3.2
   */
  visu_node_array_signals[VISIBILITY_CHANGED_SIGNAL] =
    g_signal_newv("VisibilityChanged", G_TYPE_FROM_CLASS(klass),
		  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		  NULL, NULL, NULL, g_cclosure_marshal_VOID__VOID,
		  G_TYPE_NONE, 0, NULL);
  /**
   * VisuNodeArray::MaterialChanged:
   * @nodes: the object which received the signal ;
   *
   * Gets emitted when one or more nodes have changed of
   * color or material.
   *
   * Since: 3.6
   */
  visu_node_array_signals[MATERIAL_CHANGED_SIGNAL] =
    g_signal_newv("MaterialChanged", G_TYPE_FROM_CLASS(klass),
		  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		  NULL, NULL, NULL, g_cclosure_marshal_VOID__VOID,
		  G_TYPE_NONE, 0, NULL);
  /**
   * VisuNodeArray::RenderingChanged:
   * @nodes: the object which received the signal ;
   * @ele: (allow-none): the #VisuElement nodes have changed or NULL for
   * all elements.
   *
   * Gets emitted when the rendering characteristic of nodes are
   * changed, like their shape or specific colour. For a signal on
   * generic colours (like the inherited element colour), listen to
   * VisuNodeArray::MaterialChanged signal instead.
   *
   * Since: 3.7
   */
  visu_node_array_signals[RENDERING_CHANGED_SIGNAL] =
    g_signal_new("RenderingChanged", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
                 G_TYPE_NONE, 1, G_TYPE_OBJECT, NULL);

  /**
   * VisuNodeArray::ElementRenderingChanged:
   * @nodes: the object which received the signal ;
   * @ele: (allow-none): the #VisuElement nodes have changed or NULL for
   * all elements.
   *
   * Gets emitted when the rendering characteristic of elements are
   * changed, like their shape or specific colour.
   *
   * Since: 3.7
   */
  visu_node_array_signals[ELEMENT_RENDERING_CHANGED_SIGNAL] =
    g_signal_new("ElementRenderingChanged", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
                 G_TYPE_NONE, 1, G_TYPE_OBJECT, NULL);

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_node_array_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_node_array_finalize;

  g_type_class_add_private(klass, sizeof(VisuNodeArrayPrivate));

  /* Create the boxed type of nodes. */
  visu_node_get_type();
}

static void visu_node_array_init(VisuNodeArray *array)
{
  DBG_fprintf(stderr, "Visu NodeArray: initializing a new object (%p).\n",
	      (gpointer)array);
  array->priv = G_TYPE_INSTANCE_GET_PRIVATE(array, VISU_TYPE_NODE_ARRAY, VisuNodeArrayPrivate);
  array->priv->dispose_has_run = FALSE;

  array->priv->elements      = g_array_new(FALSE, FALSE, sizeof(EleArr));

  array->priv->nodeTable.idCounter    = 0;
  array->priv->nodeTable.nNodes       = 0;
  array->priv->nodeTable.nStoredNodes = 0;
  array->priv->nodeTable.array        = (VisuNode**)0;

  array->priv->eleProp       = g_hash_table_new_full(g_str_hash, g_str_equal,
                                                     NULL, freeElePropStruct);
  array->priv->nodeProp      = g_hash_table_new_full(g_str_hash, g_str_equal,
                                                     NULL, freeNodePropStruct);

  /* We add a node property that is > 0 if the node is a duplicate
     node and negative if not. The value is the node id that the
     duplicate refers to. */
  DBG_fprintf(stderr, " | create the original node property.\n");
  array->priv->origProp   = visu_node_array_property_newInteger(array, ORIGINAL_ID);
  array->priv->nOrigNodes = 0;

  /* Connect a method to make all nodes rendered attribute to TRUE
     when the AskForShowHide signal is emited. */
  g_signal_connect(G_OBJECT(array), "AskForShowHide",
		   G_CALLBACK(onAskForShowHideSignal), (gpointer)0);
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_node_array_dispose(GObject* obj)
{
  VisuNodeArray *nodes;
  guint i;
  EleArr *ele;

  DBG_fprintf(stderr, "Visu NodeArray: dispose object %p.\n", (gpointer)obj);

  nodes = VISU_NODE_ARRAY(obj);
  if (nodes->priv->dispose_has_run)
    return;
  nodes->priv->dispose_has_run = TRUE;

  for (i = 0; i < nodes->priv->elements->len; i++)
    {
      ele = _getEleArr(nodes, i);
      g_signal_handler_disconnect(G_OBJECT(ele->ele), ele->rendered);
      g_signal_handler_disconnect(G_OBJECT(ele->ele), ele->material);
    }

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_node_array_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_node_array_finalize(GObject* obj)
{
  VisuNodeArrayPrivate *nodeArray;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Visu NodeArray: finalize object %p.\n", (gpointer)obj);

  nodeArray = VISU_NODE_ARRAY(obj)->priv;

  /* We first remove the properties, before the node description because
     these descriptions may be relevant. */
  if (nodeArray->nodeProp)
    g_hash_table_destroy(nodeArray->nodeProp);
  if (nodeArray->eleProp)
    g_hash_table_destroy(nodeArray->eleProp);
  _freeNodes(VISU_NODE_ARRAY(obj));
  g_array_free(nodeArray->elements, TRUE);

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Visu NodeArray: chain to parent.\n");
  G_OBJECT_CLASS(visu_node_array_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Visu NodeArray: freeing ... OK.\n");
}

#if DEBUG == 1
static void _checkNodeTable(NodeTable *table)
{
  guint j;

  for (j = 0; j < table->idCounter; j++)
    if (table->array[j] && table->array[j]->number != j)
      g_warning("inconsistency on node number %d.", j);
}
#endif
static gboolean _validNodeTableId(NodeTable *table, guint id)
{
  g_return_val_if_fail(table, FALSE);

  return id < table->idCounter;
}
static void _increaseNodeTable(NodeTable *table, guint delta)
{
  table->nNodes += delta;
  table->array = g_realloc(table->array, sizeof(VisuNode*) * table->nNodes);
  memset(table->array + table->nNodes - delta, '\0', sizeof(VisuNode*) * delta);
}
static void _compactNodeTable(NodeTable *table)
{
  DBG_fprintf(stderr, "Visu NodeArray: compaction required (%d %d).\n",
              table->idCounter, table->nNodes);
  g_return_if_fail(table && table->idCounter <= table->nNodes);

  /* We get the last non NULL node pointer in the array array to make the
     idCounter having this value to avoid to much reallocation of the array
     array. */
  for (; table->idCounter > 0 && !table->array[table->idCounter - 1];
       table->idCounter -= 1);
  DBG_fprintf(stderr, "Visu NodeArray: idCounter is set to %d.\n", table->idCounter);
}
static VisuNode* _getFromId(NodeTable *table, guint number)
{
  g_return_val_if_fail(table && number < table->nNodes, (VisuNode*)0);

  return table->array[number];
}
static void _setAtId(NodeTable *table, guint number, VisuNode *node)
{
  g_return_if_fail(table && number < table->nNodes);

  if (node && !table->array[number])
    table->nStoredNodes += 1;
  else if (!node && table->array[number])
    table->nStoredNodes -= 1;
  table->array[number] = node;
}
static void _addNodeTable(NodeTable *table, VisuNode *node)
{
  g_return_if_fail(table && node);

  if (table->idCounter == table->nNodes)
    _increaseNodeTable(table, REALLOCATION_STEP);

  node->number = table->idCounter;
  _setAtId(table, node->number, node);
  table->idCounter += 1;
}

/**
 * visu_node_array_allocate:
 * @array: a #VisuNodeArray object.
 * @elements: (in) (element-type VisuElement*):the size of nNodes.
 * @nNodes: (in) (element-type guint): an array giving the number of nodes per element.
 *
 * Reallocate the internal arrays to match @elements and @nNodes.
 */
void visu_node_array_allocate(VisuNodeArray *array,
                              GArray *elements, GArray *nNodes)
{
  guint i, j;
  EleArr ele;
  VisuElement *element;

  g_return_if_fail(VISU_IS_NODE_ARRAY(array));
  g_return_if_fail(nNodes && nNodes->len > 0);
  g_return_if_fail(elements && elements->len > 0);
  g_return_if_fail(nNodes->len == elements->len);

  DBG_fprintf(stderr, "Visu NodeArray: allocating VisuElement data (+%d elements).\n",
              elements->len);

  for(i = 0; i < elements->len; i++)
    {
      element = g_array_index(elements, VisuElement*, i);
      if (visu_node_array_getElementId(array, element) < 0)
        {
          /* Create new element. */
          ele.ele = element;
          ele.rendered =
            g_signal_connect_swapped(G_OBJECT(element), "ElementVisibilityChanged",
                                     G_CALLBACK(onElementRenderChanged), array);
          ele.material =
            g_signal_connect_swapped(G_OBJECT(element), "ElementMaterialChanged",
                                     G_CALLBACK(onElementMaterialChanged), array);
          ele.rendering =
            g_signal_connect_swapped(G_OBJECT(element), "ElementRenderingChanged",
                                     G_CALLBACK(onElementRenderingChanged), array);
          ele.nNodes = g_array_index(nNodes, guint, i);
          ele.nStoredNodes = 0;
          ele.nodes = g_malloc(sizeof(VisuNode) * ele.nNodes);
          for (j = 0; j < ele.nNodes; j++)
            {
              ele.nodes[j].posElement = array->priv->elements->len;
              ele.nodes[j].posNode    = j;
            }

          array->priv->elements = g_array_append_val(array->priv->elements, ele);

          /* Increase the node table. */
          _increaseNodeTable(&array->priv->nodeTable, ele.nNodes);

          /* Expand element properties for new element. */
          g_hash_table_foreach(array->priv->eleProp, allocateEleProp, (gpointer)ele.ele);
          /* Expand node properties for new element. */
          g_hash_table_foreach(array->priv->nodeProp, allocateNodeProp, (gpointer)0);

          DBG_fprintf(stderr, " | add VisuElement '%p' -> '%s' (%ld %ld) at %p.\n",
                      (gpointer)element, element->name, ele.rendered, ele.material,
                      (gpointer)_getEleArr(array, array->priv->elements->len - 1));
          DBG_fprintf(stderr, " | %d nodes allocated.\n", ele.nNodes);
        }
      else
        /* Reallocate existing element. */
        visu_node_array_allocateNodesForElement(array, i, g_array_index(nNodes, guint, i));
    }
  DBG_fprintf(stderr, " | has now %d elements.\n", array->priv->elements->len);
  DBG_fprintf(stderr, " | total number of allocated nodes %d.\n",
	      array->priv->nodeTable.nNodes);

  DBG_fprintf(stderr, " | size = %do\n",
	      (int)(array->priv->elements->len * sizeof(EleArr) + sizeof(GArray) +
                    sizeof(VisuNode) * array->priv->nodeTable.nNodes +
		    sizeof(VisuNode*) * array->priv->nodeTable.nNodes + sizeof(VisuNodeArray)));

  DBG_fprintf(stderr, "Visu NodeArray: emit PopulationDefined.\n");
  g_signal_emit(G_OBJECT(array), visu_node_array_signals[POPULATION_DEFINED_SIGNAL],
		0, array->priv->elements->len, NULL);
  DBG_fprintf(stderr, "Visu NodeArray: emission done (PopulationDefined).\n");
}
/**
 * visu_node_array_allocateByNames:
 * @array: a #VisuNodeArray object;
 * @nNodesPerElement: (in) (element-type guint): number of #VisuNode per VisuElement;
 * @elementNames: (in) (element-type utf8): names of elements;
 *
 * This method allocates the storing part of the given #VisuNodeArray structure and
 * store all the #VisuNodes.
 */
void visu_node_array_allocateByNames(VisuNodeArray *array,
                                     GArray *nNodesPerElement,
                                     GArray *elementNames)
{
  GArray *elements;
  guint i;
  VisuElement *ele;

  DBG_fprintf(stderr, "Visu NodeArray: allocating %p from names.\n", (gpointer)array);

  elements = g_array_sized_new(FALSE, FALSE, sizeof(VisuElement*), elementNames->len);
  for (i = 0; i < elementNames->len; i++)
    {
      ele = visu_element_retrieveFromName(g_array_index(elementNames, gchar*, i),
                                          (gboolean*)0);
      g_array_append_val(elements, ele);
    }
  visu_node_array_allocate(array, elements, nNodesPerElement);
  g_array_free(elements, TRUE);
}
static void _freeNodes(VisuNodeArray *nodeArray)
{
  guint i;
  EleArr *ele;

  DBG_fprintf(stderr, "Visu NodeArray: free storages of %p.\n", (gpointer)nodeArray);

  if (nodeArray->priv->elements)
    {
      for (i = 0; i < nodeArray->priv->elements->len; i++)
        {
          ele = _getEleArr(nodeArray, i);
          if (!nodeArray->priv->dispose_has_run)
            {
              g_signal_handler_disconnect(G_OBJECT(ele->ele), ele->rendered);
              g_signal_handler_disconnect(G_OBJECT(ele->ele), ele->material);
            }
          g_free(ele->nodes);
        }
      g_array_set_size(nodeArray->priv->elements, 0);
    }

  if (nodeArray->priv->nodeTable.array)
    g_free(nodeArray->priv->nodeTable.array);
  nodeArray->priv->nodeTable.array = (VisuNode**)0;
  nodeArray->priv->nodeTable.idCounter      = 0;
  nodeArray->priv->nodeTable.nNodes         = 0;
  nodeArray->priv->nodeTable.nStoredNodes   = 0;

  DBG_fprintf(stderr, "Visu NodeArray: freeing node array ... OK.\n");
}

/**
 * visu_node_array_freeNodes:
 * @nodeArray: a #VisuNodeArray object.
 *
 * Deallocate all nodes of the object and related properties but keep
 * the object alive.
 **/
void visu_node_array_freeNodes(VisuNodeArray *nodeArray)
{
  /* We first remove the properties, before the node description because
     these descriptions may be relevant. */
  if (nodeArray->priv->nodeProp)
    g_hash_table_remove_all(nodeArray->priv->nodeProp);
  if (nodeArray->priv->eleProp)
    g_hash_table_remove_all(nodeArray->priv->eleProp);

  _freeNodes(nodeArray);

  nodeArray->priv->origProp   = visu_node_array_property_newInteger(nodeArray, ORIGINAL_ID);
  nodeArray->priv->nOrigNodes = 0;
}

/**
 * visu_node_array_removeNodes:
 * @nodeArray: a #VisuNodeArray object.
 * @nodeNumbers: (in) (array): an array of integers (negative terminated).
 *
 * Remove the given #VisuNode from the @nodeArray. The properties
 * are also updated.
 */
void visu_node_array_removeNodes(VisuNodeArray *nodeArray, int *nodeNumbers)
{
  VisuNode *node;
  guint i, iEle, iNode;
  EleArr *ele;

  g_return_if_fail(nodeArray && nodeNumbers);

  /* For each element in the given node array, we take the last node
     of the same element and it takes it position. */
  DBG_fprintf(stderr, "Visu Node: removing nodes from array %p.\n",
	      (gpointer)nodeArray);
  for (i = 0; nodeNumbers[i] >= 0; i++)
    {
      node = _getFromId(&nodeArray->priv->nodeTable, nodeNumbers[i]);
      if (!node)
        continue;
      g_return_if_fail(node->number == (guint)nodeNumbers[i]);

      iEle  = node->posElement;
      iNode = node->posNode;

      ele = _getEleArr(nodeArray, iEle);
      ele->nStoredNodes -= 1;

      if (ele->nStoredNodes > 0)
	{
	  /* First, we copy the properties following the same scheme,
	     the last node of the given element is copy instead of
	     the given one. */
	  g_hash_table_foreach(nodeArray->priv->nodeProp, removeNodeProperty,
                               ele->nodes + iNode);

	  /* Then, we copy the node values themselves. */
	  visu_node_copy(ele->nodes + iNode, ele->nodes + ele->nStoredNodes);
	  /* We update the index values. */
	  ele->nodes[iNode].posNode = iNode;
	  ele->nodes[iNode].number  = ele->nodes[ele->nStoredNodes].number;
          _setAtId(&nodeArray->priv->nodeTable, ele->nodes[iNode].number, ele->nodes + iNode);
	}
      /* Nullify the removed node. */
      _setAtId(&nodeArray->priv->nodeTable, nodeNumbers[i], (VisuNode*)0);

      DBG_fprintf(stderr, "Visu NodeArray: %d removed, population for element %d is now:",
                  nodeNumbers[i], iEle);
      DBG_fprintf(stderr, " %d/%d # %d/%d\n", nodeArray->priv->nodeTable.nStoredNodes,
		  nodeArray->priv->nodeTable.nNodes, ele->nStoredNodes, ele->nNodes);
    }
  _compactNodeTable(&nodeArray->priv->nodeTable);
  DBG_fprintf(stderr, " | size = %do\n",
	      (int)(nodeArray->priv->elements->len * sizeof(EleArr) + sizeof(GArray) +
                    sizeof(VisuNode) * nodeArray->priv->nodeTable.nNodes +
		    sizeof(VisuNode*) * nodeArray->priv->nodeTable.nNodes +
                    sizeof(VisuNodeArray)));

  DBG_fprintf(stderr, "Visu NodeArray: emit a 'NodePopulationDecrease' signal.\n");
  g_signal_emit(G_OBJECT(nodeArray), visu_node_array_signals[POPULATION_DECREASE_SIGNAL],
		0, (gpointer)nodeNumbers, NULL);
}

/**
 * visu_node_array_removeNodesOfElement:
 * @nodeArray: a #VisuNodeArray object.
 * @element: a #VisuElement object.
 *
 * Remove all the #VisuNode from the element @element. The properties
 * are also updated.
 *
 * Since: 3.7
 */
void visu_node_array_removeNodesOfElement(VisuNodeArray *nodeArray, VisuElement *element)
{
  guint i;
  gint iEle, stop = -1;
  EleArr *ele;
  GArray *nodeNumbers;

  g_return_if_fail(nodeArray && element);

  iEle = visu_node_array_getElementId(nodeArray, element);
  if (iEle < 0)
    return;

  /* We keep the allocation to avoid deallocating, reallocating. */

  /* Begin by removing the the node properties. */
  g_hash_table_foreach(nodeArray->priv->nodeProp,
                       removeNodePropertyForElement, GINT_TO_POINTER(iEle));

  ele = _getEleArr(nodeArray, iEle);
  /* We update the node table. */
  nodeNumbers = g_array_new(FALSE, FALSE, sizeof(gint));
  for (i = 0; i < ele->nStoredNodes; i++)
    {
      g_array_append_val(nodeNumbers, ele->nodes[i].number);
      _setAtId(&nodeArray->priv->nodeTable, ele->nodes[i].number, (VisuNode*)0);
    }
  g_array_append_val(nodeNumbers, stop);
  ele->nStoredNodes = 0;
  _compactNodeTable(&nodeArray->priv->nodeTable);

  DBG_fprintf(stderr, "Visu NodeArray: emit a 'NodePopulationDecrease' signal.\n");
  g_signal_emit(G_OBJECT(nodeArray), visu_node_array_signals[POPULATION_DECREASE_SIGNAL],
		0, (gpointer)nodeNumbers->data, NULL);
  g_array_free(nodeNumbers, TRUE);
}

/**
 * visu_node_array_removeAllDuplicateNodes:
 * @nodeArray: a #VisuNodeArray object.
 *
 * Remove all nodes that are not original in the box.
 *
 * Returns: TRUE if some nodes have been removed.
 */
gboolean visu_node_array_removeAllDuplicateNodes(VisuNodeArray *nodeArray)
{
  GArray *tmp;
  guint i, j;
  EleArr *ele;
  int stop;

  g_return_val_if_fail(VISU_IS_NODE_ARRAY(nodeArray), FALSE);

  tmp = g_array_new(FALSE, FALSE, sizeof(int));
  for (i = 0; i < nodeArray->priv->elements->len; i++)
    {
      ele = _getEleArr(nodeArray, i);
      for (j = 0; j < ele->nStoredNodes; j++)
        if (nodeArray->priv->origProp->data_int[i][j] >= 0)
          tmp = g_array_append_val(tmp, ele->nodes[j].number);
    }

  if (tmp->len > 0)
    {
      stop = -1;
      tmp = g_array_append_val(tmp, stop);
      visu_node_array_removeNodes(nodeArray, (int*)tmp->data);
      g_array_free(tmp, TRUE);
      return TRUE;
    }

  g_array_free(tmp, TRUE);
  return FALSE;
}

/**
 * visu_node_array_getFromId:
 * @array: a #VisuNodeArray structure which stores the nodes.
 * @number: an integer.
 *
 * This methods retrieves the #VisuNode identified by the integer @number.
 * The number must be strictly positive. No error is raised if no node corresponds
 * to the given number.
 *
 * Returns: (transfer none): the found #VisuNode or NULL if none corresponds to number.
 */
VisuNode* visu_node_array_getFromId(VisuNodeArray *array, guint number)
{
  g_return_val_if_fail(VISU_IS_NODE_ARRAY(array), (VisuNode*)0);

  DBG_fprintf(stderr, "Visu NodeArray: get VisuNode from number %d.\n", number);
  if (_validNodeTableId(&array->priv->nodeTable, number))
    return _getFromId(&array->priv->nodeTable, number);
  else
    return (VisuNode*)0;
}

/**
 * visu_node_array_getOriginal:
 * @nodeArray: a #VisuNodeArray object.
 * @nodeId: a node id.
 *
 * Test if the given @nodeId is an original or a replica for the
 * periodisation.
 *
 * Returns: TRUE for an original node.
 */
gint visu_node_array_getOriginal(VisuNodeArray *nodeArray, guint nodeId)
{
  VisuNode *node;
  gint orig;

  g_return_val_if_fail(nodeArray && nodeArray->priv->origProp, -1);
  g_return_val_if_fail(_validNodeTableId(&nodeArray->priv->nodeTable, nodeId), -1);

  orig = (gint)nodeId;
  do
    {
      node = _getFromId(&nodeArray->priv->nodeTable, orig);
      orig = nodeArray->priv->origProp->data_int[node->posElement][node->posNode];
    }
  while (orig >= 0);
/*   DBG_fprintf(stderr, "Visu Node: get original from %d: %d (%d).\n", */
/* 	      nodeId, node->number, */
/* 	      nodeArray->priv->origProp->data_int[node->posElement][node->posNode]); */
  return (node->number == nodeId)?-1:(gint)node->number;
}

/**
 * visu_node_array_setOriginal:
 * @nodeArray: a #VisuNodeArray object.
 * @nodeId: a node id.
 *
 * Test if the given @nodeId is an original or a replica for the
 * periodisation.
 *
 * Returns: TRUE for an original node.
 */
gboolean visu_node_array_setOriginal(VisuNodeArray *nodeArray, guint nodeId)
{
  VisuNode *node;
  gint orig;

  g_return_val_if_fail(nodeArray && nodeArray->priv->origProp, -1);
  g_return_val_if_fail(_validNodeTableId(&nodeArray->priv->nodeTable, nodeId), -1);

  node = _getFromId(&nodeArray->priv->nodeTable, nodeId);
  g_return_val_if_fail(node, FALSE);

  orig = nodeArray->priv->origProp->data_int[node->posElement][node->posNode];
  nodeArray->priv->origProp->data_int[node->posElement][node->posNode] = -1;

  DBG_fprintf(stderr, "Visu Node: set original for %d (%d).\n",
	      nodeId, orig);
  return (orig != -1);
}

/**
 * visu_node_array_switchNumber:
 * @nodeArray: a #VisuNodeArray object.
 * @from: a node id.
 * @to: another node id.
 *
 * Two nodes of @nodeArray switches their number.
 *
 * Since: 3.6
 *
 * Returns: TRUE if number is switched.
 */
gboolean visu_node_array_switchNumber(VisuNodeArray *nodeArray, guint from, guint to)
{
  VisuNode *nodeFrom, *nodeTo;

  if (from == to)
    return FALSE;

  nodeFrom = _getFromId(&nodeArray->priv->nodeTable, from);
  nodeTo   = _getFromId(&nodeArray->priv->nodeTable, to);
  _setAtId(&nodeArray->priv->nodeTable, from, nodeTo);
  _setAtId(&nodeArray->priv->nodeTable, to, nodeFrom);
  nodeFrom->number = to;
  nodeTo->number   = from;
  return TRUE;
}

/**
 * visu_node_array_compareElements:
 * @data1: a #VisuData object ;
 * @data2: an other #VisuData object.
 *
 * This method is used to compare the composition of the given two #VisuData objects.
 * The test is only done on #VisuElement lists.
 *
 * Returns: TRUE if the two objects contains exactly the same #VisuElement objects (not
 *          one more or one less or one different), FALSE otherwise.
 */
gboolean visu_node_array_compareElements(VisuNodeArray *data1, VisuNodeArray *data2)
{
  guint i, j;
  gboolean found;

  g_return_val_if_fail(data1 && data2, FALSE);

  DBG_fprintf(stderr, "Visu NodeArray: comparing composition of '%p' and '%p'.\n",
	      (gpointer)data1, (gpointer)data2);
  if (data1 == data2)
    return TRUE;

  if (data1->priv->elements->len != data2->priv->elements->len)
    return FALSE;

  for (i = 0; i < data1->priv->elements->len; i++)
    {
      found = FALSE;
      for (j = 0; !found && j < data2->priv->elements->len; j++)
        found = (_getElement(data1, i) == _getElement(data2, j));
      if (!found)
        return FALSE;
    }

  return TRUE;
}
/**
 * visu_node_array_getMaxElementSize:
 * @nodes: a #VisuNodeArray object.
 *
 * Calculate the maximum size of all #VisuElement used in these
 * @nodes.
 *
 * Returns: a positive size.
 */
float visu_node_array_getMaxElementSize(VisuNodeArray *nodes)
{
  float ext, ext_;
  VisuNodeArrayIter iter;
  VisuRendering *method;

  g_return_val_if_fail(VISU_IS_NODE_ARRAY(nodes), 0.f);
  
  DBG_fprintf(stderr, "Visu NodeArray: get extens of all elements.\n");
  /* Compute the max size of a drawn node. */
  method = visu_object_getRendering(VISU_OBJECT_INSTANCE);
  g_return_val_if_fail(VISU_IS_RENDERING_TYPE(method), 0.f);
  ext = 0.f;
  visu_node_array_iterNew(nodes, &iter);
  for (visu_node_array_iterStart(nodes, &iter); iter.element;
       visu_node_array_iterNextElement(nodes, &iter))
    {
      ext_ = visu_rendering_getSizeOfElement(method, iter.element);
      ext  = MAX(ext, ext_);
    }
  DBG_fprintf(stderr, " | %g.\n", ext);
  return ext;
}
/**
 * visu_node_array_getElement:
 * @data: a #VisuNodeArray object ;
 * @node: a #VisuNode of this array.
 *
 * This routine gets the #VisuElement the @node belongs to.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuElement, owned by V_Sim.
 */
VisuElement* visu_node_array_getElement(VisuNodeArray *data, VisuNode *node)
{
  g_return_val_if_fail(VISU_IS_NODE_ARRAY(data) && node, (VisuElement*)0);

  return _getElement(data, node->posElement);
}
/**
 * visu_node_array_getElementId:
 * @array: a #VisuNodeArray object ;
 * @element: a #VisuElement object.
 *
 * This routines returns the internal id used to represent @element,
 * or -1 if not found.
 *
 * Since: 3.7
 *
 * Returns: a positive number or -1 if not found.
 */
gint visu_node_array_getElementId(VisuNodeArray *array, VisuElement *element)
{
  guint i;

  g_return_val_if_fail(VISU_IS_NODE_ARRAY(array), -1);

  for (i = 0; i < array->priv->elements->len; i++)
    if (_getElement(array, i) == element)
      return i;
  return -1;
}
/**
 * visu_node_array_getNNodes:
 * @array: a #VisuNodeArray object.
 *
 * This routines returns the number of #VisuNode stored in @array.
 * 
 * Since: 3.7
 *
 * Returns: a positive number.
 */
guint visu_node_array_getNNodes(VisuNodeArray *array)
{
  g_return_val_if_fail(VISU_IS_NODE_ARRAY(array), 0);
  
  return array->priv->nodeTable.nStoredNodes;
}
/**
 * visu_node_array_getNElements:
 * @array: a #VisuNodeArray object ;
 * @physical: a boolean.
 *
 * The parameter @array stores several #VisuNode of #VisuElement. This
 * routine is used to get the number of #VisuElement that are used by
 * this @array. Depending on @physical value, the number of
 * #VisuElement representing physical element or not is retrieved. The
 * actual returned number of #VisuElement take into account only
 * elements with a positive number of nodes.
 *
 * Since: 3.7
 *
 * Returns: a positive number.
 */
guint visu_node_array_getNElements(VisuNodeArray *array, gboolean physical)
{
  guint nEle, i;

  g_return_val_if_fail(VISU_IS_NODE_ARRAY(array), 0);

  nEle = 0;
  for (i = 0; i < array->priv->elements->len; i++)
    if (_getEleArr(array, i)->nStoredNodes > 0)
      {
        if (physical && visu_element_getPhysical(_getElement(array, i)))
          nEle += 1;
        else if (!physical)
          nEle += 1;
      }
  
  return nEle;
}

/**
 * visu_node_array_allocateNodesForElement:
 * @array: a #VisuNodeArray object ;
 * @eleId: an internal #VisuElement id ;
 * @nNodes: a positive number of nodes.
 *
 * This routine is used to allocate space for @nNodes of a
 * #VisuElement. This #VisuElement is identified by its internal id,
 * see visu_node_array_getElementId(). If this #VisuElement has
 * already enough space in this @array, nothing is done, otherwise
 * space is reallocated.
 *
 * Since: 3.7
 */
void visu_node_array_allocateNodesForElement(VisuNodeArray *array, guint eleId, guint nNodes)
{
  guint j, delta;
  VisuNode *oldNodeList;
  EleArr *ele;

  g_return_if_fail(VISU_IS_NODE_ARRAY(array) && eleId < array->priv->elements->len);

  ele = _getEleArr(array, eleId);
  if (ele->nNodes >= nNodes)
    return;

  delta = nNodes - ele->nNodes;
  oldNodeList = ele->nodes;
  DBG_fprintf(stderr, "Visu Node: reallocation needed for element "
	      "%d with %d nodes.\n", eleId, delta);
  ele->nNodes = nNodes;
  ele->nodes  = g_realloc(ele->nodes, sizeof(VisuNode) * ele->nNodes);
  _increaseNodeTable(&array->priv->nodeTable, delta);
  DBG_fprintf(stderr, " | (all)%d/%d # (%d)%d/%d\n",
	      array->priv->nodeTable.nStoredNodes, array->priv->nodeTable.nNodes,
              eleId, ele->nStoredNodes, ele->nNodes);
  /* We set the default values for the new nodes. */
  for (j = ele->nStoredNodes; j < ele->nNodes; j++)
    {
      ele->nodes[j].posElement = eleId;
      ele->nodes[j].posNode    = j;
    }
  /* If the node list has been moved, we need to reassign pointers
     of array nodeTable. */
  if (oldNodeList != ele->nodes)
    for (j = 0; j < ele->nStoredNodes; j++)
      _setAtId(&array->priv->nodeTable, ele->nodes[j].number, ele->nodes + j);
  /* We reallocate the table properties. */
  g_hash_table_foreach(array->priv->nodeProp, reallocNodeProperty,
		       GINT_TO_POINTER(eleId));

  /* Test part for the nodeTable array. */
#if DEBUG == 1
  _checkNodeTable(&array->priv->nodeTable);
#endif
  DBG_fprintf(stderr, "Visu Node: reallocation OK.\n");
  DBG_fprintf(stderr, " | size = %do\n",
	      (int)(array->priv->elements->len * sizeof(EleArr) + sizeof(GArray) +
                    sizeof(VisuNode) * array->priv->nodeTable.nNodes +
		    sizeof(VisuNode*) * array->priv->nodeTable.nNodes + sizeof(VisuNodeArray)));
}

/**
 * visu_node_array_getNewNode:
 * @nodeArray: a #VisuNodeArray object ;
 * @iEle: an integer between 0 and @nodeArray->priv->elements->lens - 1.
 *
 * Return the location of an unstored node for the given #VisuElement.
 * The returned node is then added in the list of used nodes.
 *
 * Returns: (transfer none): the location of a newly used node.
 */
VisuNode* visu_node_array_getNewNode(VisuNodeArray *nodeArray, guint iEle)
{
  DBG_fprintf(stderr, "Visu Node: create a new node of element %d.\n", iEle);
  return newOrCopyNode(nodeArray, iEle, -1);
}

/**
 * visu_node_array_getCopyNode:
 * @nodeArray: a #VisuNodeArray object ;
 * @node: a node of the given #VisuNodeArray.
 *
 * Return the location of an unstored node that is the deep copy of the given node.
 * The returned node is then added in the list of used nodes.
 *
 * Returns: (transfer none): the location of a newly used node.
 */
VisuNode* visu_node_array_getCopyNode(VisuNodeArray *nodeArray, VisuNode *node)
{
  VisuNode* out;

  DBG_fprintf(stderr, "Visu Node: copy a new node from node %d (%d-%d).\n",
	      node->number, node->posElement, node->posNode);
  out = newOrCopyNode(nodeArray, node->posElement, node->number);
  DBG_fprintf(stderr, "Visu Node: copy a new node from node -> %d.\n", out->number);
  return out;
}

static VisuNode* newOrCopyNode(VisuNodeArray *nodeArray, int iEle,
			       int oldNodeId)
{
  EleArr *ele;
  VisuNode *node, *oldNode;
  int j;
  struct twoNodes nodes;
  GValue idValue = {0, {{0}, {0}}};

  g_return_val_if_fail(nodeArray, (VisuNode*)0);
  g_return_val_if_fail((oldNodeId >= 0 &&
                        _validNodeTableId(&nodeArray->priv->nodeTable, (guint)oldNodeId)) ||
		       (iEle >= 0 && iEle < (int)nodeArray->priv->elements->len),
		       (VisuNode*)0);

  ele = _getEleArr(nodeArray, iEle);
  if (ele->nStoredNodes == ele->nNodes)
    /* We need to realloc... */
    visu_node_array_allocateNodesForElement(nodeArray, iEle, ele->nNodes + REALLOCATION_STEP);

  /* Get the new node. */
  node = ele->nodes + ele->nStoredNodes;
  _addNodeTable(&nodeArray->priv->nodeTable, node);
  /* Update the node internal values. */
  ele->nStoredNodes += 1;

  /* We copy the values from oldNode. */
  oldNode = (VisuNode*)0;
  if (oldNodeId >= 0)
    {
      oldNode = _getFromId(&nodeArray->priv->nodeTable, oldNodeId);
      for ( j = 0; j < 3; j++)
	{
	  node->xyz[j]         = oldNode->xyz[j];
	  node->translation[j] = oldNode->translation[j];
	}
      node->rendered           = oldNode->rendered;
    }

  /* Create new properties for the node. */
  nodes.newNode = node;
  nodes.oldNode = oldNode;
  g_hash_table_foreach(nodeArray->priv->nodeProp, createNodeproperty, (gpointer)&nodes);

  /* If we have an old node, we use it as original node id, or we put
     -1 if not. */
  g_value_init(&idValue, G_TYPE_INT);
  g_value_set_int(&idValue, oldNodeId);
  visu_node_property_setValue(nodeArray->priv->origProp, node, &idValue);
  nodeArray->priv->nOrigNodes += (oldNodeId < 0)?1:0;

  return node;
}
/**
 * visu_node_array_askForShowHide:
 * @array: a #VisuNodeArray object.
 *
 * This function emits the "AskForShowHide" signal of @array. This
 * signal will make all the nodes visible and each hiding objects will
 * have to switch off nodes.
 *
 * Since: 3.7
 *
 * Returns: TRUE if "VisibilityChanged" signal should be emitted.
 **/
gboolean visu_node_array_askForShowHide(VisuNodeArray *array)
{
  gboolean redraw;

  g_signal_emit_by_name(G_OBJECT(array), "AskForShowHide", &redraw, NULL);

  return redraw;
}

/*******************/
/* Local routines. */
/*******************/
static void onAskForShowHideSignal(VisuNodeArray *nodeArray, gboolean *redraw,
				   gpointer data _U_)
{
  guint i, j;
  EleArr *ele;

  DBG_fprintf(stderr, "Visu NodeArray: caught the 'AskForShowHide' signal,"
	      " setting all node rendered attribute to TRUE.\n");

  for (i = 0; i < nodeArray->priv->elements->len; i++)
    for(ele = _getEleArr(nodeArray, i), j = 0; j < ele->nStoredNodes; j++)
      *redraw = visu_node_setVisibility(ele->nodes + j, TRUE) || *redraw;
  DBG_fprintf(stderr, "             - returned redraw value : %d.\n", *redraw);
}
static void onElementRenderChanged(VisuNodeArray *data, VisuElement *element _U_)
{
  DBG_fprintf(stderr, "Visu NodeArray: caught the 'ElementVisibilityChanged' signal,"
	      " emitting node render signal.\n");
  g_signal_emit(G_OBJECT(data), visu_node_array_signals[VISIBILITY_CHANGED_SIGNAL],
        	0, NULL);
}
static void onElementMaterialChanged(VisuNodeArray *data, VisuElement *element _U_)
{
  DBG_fprintf(stderr, "Visu NodeArray: caught the 'ElementMaterialChanged' signal,"
	      " emitting node material signal.\n");
  g_signal_emit(G_OBJECT(data), visu_node_array_signals[MATERIAL_CHANGED_SIGNAL],
		0, NULL);
  DBG_fprintf(stderr, "Visu NodeArray: emittion node material done.\n");
}
static void onElementRenderingChanged(VisuNodeArray *data, VisuElement *element)
{
  DBG_fprintf(stderr, "Visu NodeArray: caught the 'ElementRenderingChanged' signal,"
	      " emitting node material signal.\n");
  g_signal_emit(G_OBJECT(data), visu_node_array_signals[RENDERING_CHANGED_SIGNAL],
		0, element, NULL);
  DBG_fprintf(stderr, "Visu NodeArray: emittion node material done.\n");
}



/**************************/
/* The property routines. */
/**************************/
static void freeNodePropStruct(gpointer data)
{
  VisuNodeProperty *prop;
  guint i, j;
  EleArr *ele;

  prop = (VisuNodeProperty*)data;
  DBG_fprintf(stderr, "Visu Node: freeing node property '%s'.\n", prop->name);

  g_free(prop->name);
  /* The pointer case. */
  if (prop->data_pointer)
    {
      for (i = 0; i < prop->array->priv->elements->len; i++)
	{
	  for (ele = _getEleArr(prop->array, i), j = 0; j < ele->nNodes; j++)
            if (prop->data_pointer[i][j])
              {
                if (prop->freeTokenFunc)
                  prop->freeTokenFunc(prop->data_pointer[i][j], prop->user_data);
                else
                  g_free(prop->data_pointer[i][j]);
              }
	  g_free(prop->data_pointer[i]);
	}
      g_free(prop->data_pointer);
    }
  /* The integer case */
  if (prop->data_int)
    {
      for (i = 0; i < prop->array->priv->elements->len; i++)
	g_free(prop->data_int[i]);
      g_free(prop->data_int);
    }
  g_free(prop);
  DBG_fprintf(stderr, "Visu Node: freeing property ... OK.\n");
}

/* Remove the property of all nodes of the element given in data. */
static void removeNodePropertyForElement(gpointer key, gpointer value, gpointer data)
{
  gint iEle;
  guint j;
  EleArr *ele;
  VisuNodeProperty *prop;
  
  prop = (VisuNodeProperty*)value;
  iEle= GPOINTER_TO_INT(data);
  ele = _getEleArr(prop->array, iEle);

  DBG_fprintf(stderr, "Visu Node: remove node property '%s' for all nodes of element '%s'.\n",
	      (gchar*)key, ele->ele->name);
  /* We first remove the property tokens. */
  switch (prop->gtype)
    {
    case G_TYPE_POINTER:
      for (j = 0; j < ele->nNodes; j++)
        if (prop->data_pointer[iEle][j])
          {
            if (prop->freeTokenFunc)
              prop->freeTokenFunc(prop->data_pointer[iEle][j], prop->user_data);
            else
              g_free(prop->data_pointer[iEle][j]);
            prop->data_pointer[iEle][j] = (gpointer)0;
          }
      break;
    case G_TYPE_INT:
      for (j = 0; j < ele->nNodes; j++)
        prop->data_int[iEle][j] = 0;
      break;
    default:
      g_warning("Unsupported GValue type for property '%s'.", prop->name);
    }
}

/* Remove the property of the node given in data and move the
   last property of this element at the place of the removed node. */
static void removeNodeProperty(gpointer key, gpointer value, gpointer data)
{
  EleArr *ele;
  VisuNode *node;
  VisuNodeProperty *prop;
  
  node = (VisuNode*)data;
  prop = (VisuNodeProperty*)value;
  ele = _getEleArr(prop->array, node->posElement);
  g_return_if_fail(ele->nStoredNodes > 0);

  DBG_fprintf(stderr, "Visu Node: remove node property '%s' from %d %d.\n",
	      (gchar*)key, node->posElement, node->posNode);
  /* We first remove the property token. */
  switch (prop->gtype)
    {
    case G_TYPE_POINTER:
      if (prop->data_pointer[node->posElement][node->posNode])
        {
          if (prop->freeTokenFunc)
            prop->freeTokenFunc(prop->data_pointer[node->posElement][node->posNode],
                                prop->user_data);
          else
            g_free(prop->data_pointer[node->posElement][node->posNode]);
        }
      break;
    case G_TYPE_INT:
      prop->data_int[node->posElement][node->posNode] = 0;
      break;
    default:
      g_warning("Unsupported GValue type for property '%s'.", prop->name);
    }

  /* Then we copy the pointer from the last position to the given one.
     The last position is given by nStoredNodesPerEle since this counter
     has already been lowered. */
  switch (prop->gtype)
    {
    case G_TYPE_POINTER:
      prop->data_pointer[node->posElement][node->posNode] =
	prop->data_pointer[node->posElement][ele->nStoredNodes];
      prop->data_pointer[node->posElement][ele->nStoredNodes] = (gpointer)0;
      break;
    case G_TYPE_INT:
      prop->data_int[node->posElement][node->posNode] =
	prop->data_int[node->posElement][ele->nStoredNodes];
      prop->data_int[node->posElement][ele->nStoredNodes] = 0;
      break;
    default:
      g_warning("Unsupported GValue type for property '%s'.", prop->name);
    }
}

static void reallocNodeProperty(gpointer key, gpointer value, gpointer data)
{
  EleArr *ele;
  VisuNodeProperty *prop;
  guint iEle, j;

  iEle = (guint)GPOINTER_TO_INT(data);
  prop = (VisuNodeProperty*)value;
  DBG_fprintf(stderr, "Visu Node: realloc node property '%s' for element %d.\n",
	      (gchar*)key, iEle);

  g_return_if_fail(iEle < prop->array->priv->elements->len);

  ele = _getEleArr(prop->array, iEle);
  switch (prop->gtype)
    {
    case G_TYPE_POINTER:
      prop->data_pointer[iEle] = g_realloc(prop->data_pointer[iEle],
					   sizeof(gpointer) * ele->nNodes);
      /* We nullify the newly created properties. */
      for (j = ele->nStoredNodes; j < ele->nNodes; j++)
	prop->data_pointer[iEle][j] = (gpointer)0;
      break;
    case G_TYPE_INT:
      prop->data_int[iEle] = g_realloc(prop->data_int[iEle],
				       sizeof(int) * ele->nNodes);
      /* We nullify the newly created properties. */
      for (j = ele->nStoredNodes; j < ele->nNodes; j++)
	prop->data_int[iEle][j] = 0;
      break;
    default:
      g_warning("Unsupported GValue type for property '%s'.", prop->name);
    }
}

static void allocateNodeProp(gpointer key, gpointer value, gpointer data _U_)
{
  guint i;
  VisuNodeProperty *prop;

  prop = (VisuNodeProperty*)value;
  DBG_fprintf(stderr, "Visu Node: realloc node property '%s' for 1 new element.\n",
	      (gchar*)key);

  switch (prop->gtype)
    {
    case G_TYPE_POINTER:
      prop->data_pointer =
        g_realloc(prop->data_pointer, sizeof(gpointer*) * prop->array->priv->elements->len);
      i = prop->array->priv->elements->len - 1;
      prop->data_pointer[i] = g_malloc0(sizeof(gpointer) * _getEleArr(prop->array, i)->nNodes);
      break;
    case G_TYPE_INT:
      prop->data_int =
        g_realloc(prop->data_int, sizeof(int*) * prop->array->priv->elements->len);
      i = prop->array->priv->elements->len - 1;
      prop->data_int[i] = g_malloc0(sizeof(int) * _getEleArr(prop->array, i)->nNodes);
      break;
    default:
      g_warning("Unsupported GValue type for property '%s'.", prop->name);
    }
}

static void createNodeproperty(gpointer key, gpointer value, gpointer data)
{
  VisuNodeProperty *prop;
  struct twoNodes *nodes;

  prop = (VisuNodeProperty*)value;
  nodes = (struct twoNodes*)data;
  DBG_fprintf(stderr, "Visu Node: create/copy node property '%s' for node %d-%d.\n",
	      (gchar*)key, nodes->newNode->posElement, nodes->newNode->posNode);

  switch (prop->gtype)
    {
    case G_TYPE_POINTER:
      if (nodes->oldNode)
	prop->data_pointer[nodes->newNode->posElement][nodes->newNode->posNode] =
	  prop->newOrCopyTokenFunc((gconstpointer)prop->data_pointer[nodes->oldNode->posElement][nodes->oldNode->posNode], prop->user_data);
      else
	prop->data_pointer[nodes->newNode->posElement][nodes->newNode->posNode] =
	  prop->newOrCopyTokenFunc((gconstpointer)0, prop->user_data);
      break;
    case G_TYPE_INT:
      if (nodes->oldNode)
	prop->data_int[nodes->newNode->posElement][nodes->newNode->posNode] =
	  prop->data_int[nodes->oldNode->posElement][nodes->oldNode->posNode];
      else
	prop->data_int[nodes->newNode->posElement][nodes->newNode->posNode] = 0;
      break;
    default:
      g_warning("Unsupported GValue type for property '%s'.", prop->name);
    }
}
/*****************************/
/* Public property routines. */
/*****************************/

/**
 * visu_node_property_setValue:
 * @nodeProp: a #VisuNodeProperty object ;
 * @node: a #VisuNode object ;
 * @value: A GValue pointer this the value to be stored.
 *
 * This method is used to store some values associated with
 * the given @node of the given @nodeArray. These values can be pointers to
 * anything allocated (will be free automatically when the property is deleted) or
 * they can be static values. This depends on the construction of the node property.
 * These values can be retrieved with the visu_node_property_getValue() method.
 *
 * See visu_node_array_getProperty() to get a property by its name.
 */
void visu_node_property_setValue(VisuNodeProperty* nodeProp, VisuNode* node,
                                 GValue *value)
{
  g_return_if_fail(nodeProp && value && nodeProp->gtype == G_VALUE_TYPE(value));
  g_return_if_fail(node && node->posElement < nodeProp->array->priv->elements->len &&
		   node->posNode < _getEleArr(nodeProp->array, node->posElement)->nStoredNodes);

  switch (nodeProp->gtype)
    {
    case G_TYPE_POINTER:
      /* We free previous pointer. */
      if (nodeProp->freeTokenFunc)
	nodeProp->freeTokenFunc(nodeProp->data_pointer[node->posElement][node->posNode],
				nodeProp->user_data);
      else
	g_free(nodeProp->data_pointer[node->posElement][node->posNode]);
      /* We set the value. */
      nodeProp->data_pointer[node->posElement][node->posNode] =
	g_value_get_pointer(value);
      break;
    case G_TYPE_INT:
      nodeProp->data_int[node->posElement][node->posNode] =
	g_value_get_int(value);
      break;
    default:
      g_warning("Unsupported GValue type for property '%s'.", nodeProp->name);
    }
}

/**
 * visu_node_property_getValue:
 * @nodeProp: a #VisuNodeArray object ;
 * @node: a #VisuNode object ;
 * @value: an initialise GValue location.
 *
 * This method is used to retrieve some data associated to
 * the specified @node, stored in the given @data. These return data
 * should not be freed after used. The read value is stored in the given
 * GValue pointer. This GValue must be of the right type, depending on the
 * creation of the #VisuNodeProperty.
 *
 * Returns: some data associated to the key, stored the given GValue location.
 */
GValue* visu_node_property_getValue(VisuNodeProperty* nodeProp, VisuNode* node,
                                    GValue *value)
{
  g_return_val_if_fail(nodeProp && value && nodeProp->gtype == G_VALUE_TYPE(value),
		       value);
  g_return_val_if_fail(node && node->posElement < nodeProp->array->priv->elements->len &&
		       node->posNode < _getEleArr(nodeProp->array, node->posElement)->nStoredNodes, value);

  switch (nodeProp->gtype)
    {
    case G_TYPE_POINTER:
      DBG_fprintf(stderr, "Visu Node: get '%s' for node %d(%d,%d) as pointer %p.\n",
		  nodeProp->name, node->number, node->posElement, node->posNode,
		  (gpointer)nodeProp->data_pointer[node->posElement][node->posNode]);
      g_value_set_pointer(value, nodeProp->data_pointer[node->posElement][node->posNode]);
      return value;
    case G_TYPE_INT:
      DBG_fprintf(stderr, "Visu Node: get property '%s' for node %d as integer %d.\n",
		  nodeProp->name, node->number,
		  nodeProp->data_int[node->posElement][node->posNode]);
      g_value_set_int(value, nodeProp->data_int[node->posElement][node->posNode]);
      return value;
      break;
    default:
      g_warning("Unsupported GValue type for property '%s'.", nodeProp->name);
    }
  return value;
}

/**
 * visu_node_array_getProperty:
 * @nodeArray: a #VisuNodeArray object ;
 * @key: a string.
 *
 * This method is used to retrieve the node property associated to the given @key.
 *
 * Returns: (transfer none): a #VisuNodeProperty.
 */
VisuNodeProperty* visu_node_array_getProperty(VisuNodeArray* nodeArray, const char* key)
{
  VisuNodeProperty *prop;

  g_return_val_if_fail(nodeArray && key, (VisuNodeProperty*)0);

  prop = (VisuNodeProperty*)g_hash_table_lookup(nodeArray->priv->nodeProp, (gpointer)key);
  return prop;
}

/**
 * visu_node_array_freeProperty:
 * @nodeArray: a #VisuNodeArray object.
 * @key: the name of the property to be removed.
 * 
 * This method free the given property and all associated data.
 */
void visu_node_array_freeProperty(VisuNodeArray* nodeArray, const char* key)
{
  g_return_if_fail(nodeArray && key);

  g_hash_table_remove(nodeArray->priv->nodeProp, key);
  DBG_fprintf(stderr, "Visu Node: removing the property called '%s'.\n", key);
}

/**
 * visu_node_array_property_newPointer:
 * @nodeArray: a #VisuNodeArray object ;
 * @key: a string ;
 * @freeFunc: (allow-none) (scope call): a method to free each token (can be NULL).
 * @newAndCopyFunc: (scope call): a method to create or copy each token.
 * @user_data: (closure): a user defined pointer that will be given to
 * the free and copy routine.
 * 
 * This method creates and allocates a new area to store nodes associated data that
 * can be retrieve with the @key. These data are pointers on allocated memory
 * locations. When the property is removed with the #visu_node_freePropertry (or the
 * associated #VisuNodeArray is free) the area is free and @freeFunc is called for
 * each token (or g_free() if @freeFunc is NULL).
 *
 * The method @newAndCopyFunc is used when the number of nodes is increased,
 * if the const gpointer of the GCopyFunc is not NULL, then we require a copy,
 * if it is NULL, then the routine must create a new token with
 * default values.
 *
 * If the property already exists, it is returned.
 *
 * Returns: (transfer none): the newly created #VisuNodeProperty
 * object or the existing one.
 */
VisuNodeProperty* visu_node_array_property_newPointer(VisuNodeArray* nodeArray,
					      const char* key, 
					      GFunc freeFunc,
					      GCopyFunc newAndCopyFunc,
					      gpointer user_data)
{
  VisuNodeProperty *prop;
  EleArr *ele;

  guint i;
  
  g_return_val_if_fail(nodeArray && key && newAndCopyFunc, (VisuNodeProperty*)0);

  prop = (VisuNodeProperty*)g_hash_table_lookup(nodeArray->priv->nodeProp, key);
  if (prop)
    return prop;

  DBG_fprintf(stderr, "Visu Node: adding a new pointer"
	      " property, called '%s'.\n", key);
  prop                = g_malloc(sizeof(VisuNodeProperty));
  prop->gtype         = G_TYPE_POINTER;
  prop->name          = g_strdup(key);
  prop->array         = nodeArray;
  prop->data_pointer  = (gpointer**)0;
  prop->data_int      = (int**)0;
  if (nodeArray->priv->elements->len > 0)
    prop->data_pointer  = g_malloc(sizeof(gpointer*) * nodeArray->priv->elements->len);
  for (i = 0; i < nodeArray->priv->elements->len; i++)
    {
      ele = _getEleArr(nodeArray, i);
      DBG_fprintf(stderr, " | allocate (%d,%d)\n", i, ele->nNodes);
      prop->data_pointer[i] = g_malloc0(sizeof(gpointer) * ele->nNodes);
    }
  prop->freeTokenFunc      = freeFunc;
  prop->newOrCopyTokenFunc = newAndCopyFunc;
  prop->user_data          = user_data;
  g_hash_table_insert(nodeArray->priv->nodeProp, (gpointer)key, (gpointer)prop);
  
  return prop;
}

/**
 * visu_node_array_property_newInteger:
 * @nodeArray: a #VisuNodeArray object ;
 * @key: a string.
 * 
 * This method creates and allocates a new area to store nodes associated integer
 * values. This is the same than visu_node_array_property_newPointer() but for static
 * integers instead of pointers as data.
 *
 * Returns: (transfer none): the newly created #VisuNodeProperty object.
 */
VisuNodeProperty* visu_node_array_property_newInteger(VisuNodeArray* nodeArray,
					  const char* key)
{
  VisuNodeProperty *prop;
  EleArr *ele;

  guint i;
  
  g_return_val_if_fail(nodeArray && key, (VisuNodeProperty*)0);

  prop = (VisuNodeProperty*)g_hash_table_lookup(nodeArray->priv->nodeProp, key);
  g_return_val_if_fail(!prop, (VisuNodeProperty*)0);

  DBG_fprintf(stderr, "Visu Node: adding a new int property, called '%s'.\n", key);
  prop                = g_malloc(sizeof(VisuNodeProperty));
  prop->gtype         = G_TYPE_INT;
  prop->name          = g_strdup(key);
  prop->array         = nodeArray;
  prop->data_pointer  = (gpointer**)0;
  prop->data_int      = (int**)0;
  if (nodeArray->priv->elements->len > 0)
    prop->data_int      = g_malloc(sizeof(int*) * nodeArray->priv->elements->len);
  for (i = 0; i < nodeArray->priv->elements->len; i++)
    {
      ele = _getEleArr(nodeArray, i);
      DBG_fprintf(stderr, " | allocate (%d,%d)\n", i, ele->nNodes);
      prop->data_int[i] = g_malloc0(sizeof(int) * ele->nNodes);
    }
  prop->freeTokenFunc      = (GFunc)0;
  prop->newOrCopyTokenFunc = (GCopyFunc)0;
  prop->user_data          = (gpointer)0;
  g_hash_table_insert(nodeArray->priv->nodeProp, (gpointer)key, (gpointer)prop);
  
  return prop;
}

/**
 * visu_node_array_traceProperty:
 * @array: a #VisuNodeArray object ;
 * @id: a property name.
 *
 * This is a debug method. It outputs on stderr the values for all
 * nodes of the property @id.
 */
void visu_node_array_traceProperty(VisuNodeArray *array, const gchar *id)
{
  VisuNodeProperty* prop;
  EleArr *ele;
  guint i, j;
  
  prop = visu_node_array_getProperty(array, id);
  
  fprintf(stderr, "Visu Node: output node property '%s'.\n", id);
  fprintf(stderr, " | type= %d\n", (int)prop->gtype);
  if (prop->data_int)
    {
      for (i = 0; i < prop->array->priv->elements->len; i++)
	for (ele = _getEleArr(array, i), j = 0; j < ele->nStoredNodes; j++)
	  fprintf(stderr, " | %7d %3d %7d -> %d\n", ele->nodes[j].number,
		  i, j, prop->data_int[i][j]);
    }
  if (prop->data_pointer)
    {
      for (i = 0; i < prop->array->priv->elements->len; i++)
	for (ele = _getEleArr(array, i), j = 0; j < ele->nStoredNodes; j++)
	  fprintf(stderr, " | %7d %3d %7d -> %p\n", ele->nodes[j].number,
		  i, j, prop->data_pointer[i][j]);
    }
}

/**********************/
/* Element properties */
/**********************/
static void allocateEleProp(gpointer key, gpointer value, gpointer data)
{
  struct _ElementProperty *prop = (struct _ElementProperty*)value;
  VisuElement *ele = VISU_ELEMENT(data);
  GValue val;

  DBG_fprintf(stderr, "Visu Data: allocate element property '%s'.\n", (gchar*)key);
  memset(&val, '\0', sizeof(GValue));
  prop->init(ele, &val);
  g_value_array_append(prop->array, &val);
  DBG_fprintf(stderr, " | now has %d elements.\n", prop->array->n_values);
}
/* static void freeEleProp(gpointer key, gpointer value, gpointer data _U_) */
/* { */
/*   struct _ElementProperty *prop = (struct _ElementProperty*)value; */
/*   gint i; */

/*   DBG_fprintf(stderr, "Visu Data: free element property '%s'.\n", (gchar*)key); */
/*   for (i = prop->array->priv->n_values - 1; i >= 0; i--) */
/*     g_value_array_remove(prop->array, i); */
/* } */
static void freeElePropStruct(gpointer data)
{
  struct _ElementProperty *prop = (struct _ElementProperty*)data;

  g_value_array_free(prop->array);
  g_free(data);
}
/**
 * visu_node_array_setElementProperty:
 * @data: a #VisuNodeArray object.
 * @name: a string to identify the property.
 * @init: (scope call): an init routine.
 *
 * Create a new array to stores properties related to elements. If the
 * property @name already exists the previous one is destroyed. The
 * @init routine is called for each #VisuElement of the @data.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a newly allocated array.
 **/
GValueArray* visu_node_array_setElementProperty(VisuNodeArray *data, const gchar *name,
                                                VisuNodeArrayElementPropertyInit init)
{
  struct _ElementProperty *prop;
  guint i;
  GValue val;

  g_return_val_if_fail(VISU_IS_NODE_ARRAY(data), (GValueArray*)0);
  g_return_val_if_fail(name && name[0] && init, (GValueArray*)0);

  DBG_fprintf(stderr, "Visu Data: add a new element property '%s'.\n", name);
  prop        = g_malloc(sizeof(struct _ElementProperty));
  prop->init  = init;
  prop->array = g_value_array_new(data->priv->elements->len);
  g_hash_table_insert(data->priv->eleProp, (gpointer)name, (gpointer)prop);
  for (i = 0; i < data->priv->elements->len; i++)
    {
      memset(&val, '\0', sizeof(GValue));
      init(_getElement(data, i), &val);
      g_value_array_insert(prop->array, i, &val);
    }

  return prop->array;
}
/**
 * visu_node_array_getElementProperty:
 * @data: a #VisuNodeArray object ;
 * @name: an identifier string.
 *
 * This routine is used to retrieve an array of #GValue for each
 * element of the @data array.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): an array of #GValue, indexed by the id of
 * each #VisuElement of @data.
 */
GValueArray* visu_node_array_getElementProperty(VisuNodeArray *data, const gchar *name)
{
  struct _ElementProperty *prop;

  g_return_val_if_fail(VISU_IS_NODE_ARRAY(data), (GValueArray*)0);

  DBG_fprintf(stderr, "Visu Data: get element property '%s'.\n", name);
  prop = (struct _ElementProperty*)g_hash_table_lookup(data->priv->eleProp, name);
  return prop->array;
}




/****************/
/* The iterator */
/****************/

/**
 * visu_node_array_iterNew:
 * @array: a #VisuNodeArray object ;
 * @iter: (out caller-allocates) (transfer full): an alocated iterator.
 *
 * Set values to a #VisuNodeArrayIter object to iterate over nodes.
 * Its contain is initialised with the array size (number of elements,
 * number of nodes per element...).
 */
void visu_node_array_iterNew(VisuNodeArray *array, VisuNodeArrayIter *iter)
{
  g_return_if_fail(iter);

  iter->nAllStoredNodes = 0;
  iter->nElements       = 0;
  iter->nStoredNodes    = 0;
  iter->node            = (VisuNode*)0;
  iter->element         = (VisuElement*)0;
  iter->type            = ITER_NODES_BY_TYPE;
  iter->init            = FALSE;

  g_return_if_fail(VISU_IS_NODE_ARRAY(array));

  iter->array           = array;
  iter->idMax           = array->priv->nodeTable.idCounter - 1;
  iter->nAllStoredNodes = array->priv->nodeTable.nStoredNodes;
  iter->nElements       = array->priv->elements->len;
  iter->iElement        = -1;
  iter->lst             = (GList*)0;
  iter->itLst           = (GList*)0;

  g_return_if_fail(array->priv->nodeTable.idCounter >= array->priv->nodeTable.nStoredNodes);
}

/**
 * visu_node_array_iterStart:
 * @array: a #VisuNodeArray object ;
 * @iter: a #VisuNodeArrayIter object.
 *
 * Initialise the node and element internal pointers for a run over the nodes.
 */
void visu_node_array_iterStart(VisuNodeArray *array, VisuNodeArrayIter *iter)
{
  EleArr *ele;

  g_return_if_fail(VISU_IS_NODE_ARRAY(array) && iter && array == iter->array);

  iter->init = TRUE;

  iter->iElement = -1;
  iter->node     = (VisuNode*)0;
  iter->element  = (VisuElement*)0;
  if (array->priv->elements->len == 0)
    return;

  ele = _getEleArr(array, 0);
  iter->iElement = 0;
  iter->element  = ele->ele;
  /* We look for an element with stored nodes. */
  while (ele->nStoredNodes == 0)
    {
      iter->iElement += 1;
      if (iter->iElement >= array->priv->elements->len)
	{
	  /* We found nothing. */
	  iter->iElement = -1;
	  iter->element  = (VisuElement*)0;
	  return;
	}
      ele = _getEleArr(array, iter->iElement);
      iter->element      = ele->ele;
      iter->nStoredNodes = ele->nStoredNodes;
    }

  iter->node         = ele->nodes;
  iter->nStoredNodes = ele->nStoredNodes;
}

/**
 * visu_node_array_iterStartNumber:
 * @array: a #VisuNodeArray object ;
 * @iter: a #VisuNodeArrayIter object.
 *
 * Initialise the node and element internal pointers for a run
 * following the node oder.
 */
void visu_node_array_iterStartNumber(VisuNodeArray *array, VisuNodeArrayIter *iter)
{
  guint i;

  g_return_if_fail(VISU_IS_NODE_ARRAY(array) && iter && array == iter->array);
  g_return_if_fail(iter->lst == (GList*)0);

  iter->init = TRUE;

  iter->iElement = -1;
  iter->node     = (VisuNode*)0;
  iter->element  = (VisuElement*)0;
  if (array->priv->elements->len == 0)
    return;

  i = 0;
  iter->node = (VisuNode*)0;
  do
    {
      iter->node = visu_node_array_getFromId(VISU_NODE_ARRAY(array), i);
      i += 1;
    }
  while (!iter->node && i < array->priv->nodeTable.idCounter);
  if (!iter->node)
    return;
  iter->iElement     = iter->node->posElement;
  iter->element      = _getElement(array, iter->iElement);
  iter->nStoredNodes = _getEleArr(array, iter->iElement)->nStoredNodes;
}

/**
 * visu_node_array_iterStartVisible:
 * @array: a #VisuNodeArray object ;
 * @iter: a #VisuNodeArrayIter object.
 *
 * Initialise the node and element internal pointers for a run over the 
 * visible nodes (see visu_node_array_iterNextVisible).
 */
void visu_node_array_iterStartVisible(VisuNodeArray *array, VisuNodeArrayIter *iter)
{
  g_return_if_fail(iter->lst == (GList*)0);

  visu_node_array_iterStart(array, iter);
  if (iter->node && iter->node->rendered && iter->element->rendered)
    /* Ok, first is good. */
    return;

  /* First was not visible, we go next. */
  visu_node_array_iterNextVisible(array, iter);
}

/**
 * visu_node_array_iterStartList:
 * @array: a #VisuNodeArray object ;
 * @iter: (out caller-allocates) (transfer full): an alocated
 * iterator.
 * @lst: (element-type guint) (transfer full): a list of node ids to
 * iterate on.
 *
 * Set values to a #VisuNodeArrayIter object to iterate over nodes of
 * the given list.
 *
 * Since: 3.7
 */
void visu_node_array_iterStartList(VisuNodeArray *array, VisuNodeArrayIter *iter, GList *lst)
{
  GList init;

  g_return_if_fail(iter);

  iter->init = TRUE;
  iter->lst = lst;
  init.next = lst;
  iter->itLst = &init;
  visu_node_array_iterNextList(array, iter);
}

/**
 * visu_node_array_iterRestartNode:
 * @array: a #VisuNodeArray object ;
 * @iter: a #VisuNodeArrayIter object.
 *
 * The element internal pointer must be associated. Then, it returns the
 * node pointer to the first node for this element.
 */
void visu_node_array_iterRestartNode(VisuNodeArray *array, VisuNodeArrayIter *iter)
{
  gint iEle;
  EleArr *ele;

  g_return_if_fail(VISU_IS_NODE_ARRAY(array) && iter && array == iter->array);
  g_return_if_fail(iter->lst == (GList*)0);

  iEle = visu_node_array_getElementId(array, iter->element);
  g_return_if_fail(iEle >= 0);

  iter->init = TRUE;
  iter->iElement = (guint)iEle;
  ele = _getEleArr(array, iEle);
  iter->node = ele->nodes;
  iter->nStoredNodes = ele->nStoredNodes;
}

/**
 * visu_node_array_iterNext:
 * @array: a #VisuNodeArray object ;
 * @iter: a #VisuNodeArrayIter object.
 *
 * Modify node and element internal pointers to the next node, or NULL if
 * none remains.
 */
void visu_node_array_iterNext(VisuNodeArray *array, VisuNodeArrayIter *iter)
{
  guint iNode;
  EleArr *ele;

  g_return_if_fail(VISU_IS_NODE_ARRAY(array) && iter && array == iter->array);
  g_return_if_fail(iter->init && iter->node && iter->iElement == iter->node->posElement);

  ele = _getEleArr(array, iter->iElement);
  iNode = iter->node->posNode + 1;
  if (iNode < ele->nStoredNodes)
    iter->node = ele->nodes + iNode;
  else
    {
      iter->iElement += 1;
      if (iter->iElement >= array->priv->elements->len)
        {
          iter->node     = (VisuNode*)0;
          iter->iElement = -1;
          iter->element  = (VisuElement*)0;
          iter->nStoredNodes = 0;
        }
      else
        {
          ele = _getEleArr(array, iter->iElement);
          iter->node     = ele->nodes;
          iter->element  = ele->ele;
          iter->nStoredNodes = ele->nStoredNodes;
        }
    }
}

/**
 * visu_node_array_iterNextList:
 * @array: a #VisuNodeArray object ;
 * @iter: a #VisuNodeArrayIter object.
 *
 * Modify node and element internal pointers to the next node from the
 * starting list, or NULL if none remains.
 *
 * Since: 3.7
 */
void visu_node_array_iterNextList(VisuNodeArray *array, VisuNodeArrayIter *iter)
{
  EleArr *ele;

  g_return_if_fail(VISU_IS_NODE_ARRAY(array) && iter && array == iter->array);
  g_return_if_fail(iter->init && iter->itLst);

  do
    {
      iter->itLst = g_list_next(iter->itLst);
      iter->node = (iter->itLst)?visu_node_array_getFromId(array, GPOINTER_TO_INT(iter->itLst->data)):(VisuNode*)0;
    }
  while (iter->itLst && !iter->node);
  if (!iter->itLst)
    {
      if (iter->lst)
        g_list_free(iter->lst);
      iter->lst = iter->itLst = (GList*)0;
      iter->node = (VisuNode*)0;
    }
  /* We set additional elements. */
  if (!iter->node)
    {
      iter->iElement = -1;
      iter->element  = (VisuElement*)0;
      iter->nStoredNodes = 0;
    }
  else
    {
      ele = _getEleArr(array, iter->node->posElement);
      iter->element  = ele->ele;
      iter->nStoredNodes = ele->nStoredNodes;
    }
}

/**
 * visu_node_array_iterNextVisible:
 * @array: a #VisuNodeArray object ;
 * @iter: a #VisuNodeArrayIter object.
 *
 * Go to the next rendered node (changing element if required).
 */
void visu_node_array_iterNextVisible(VisuNodeArray *array, VisuNodeArrayIter *iter)
{
  g_return_if_fail(VISU_IS_NODE_ARRAY(array) && iter && array == iter->array);

  /* Get the next node, and test if it is rendered. */
  visu_node_array_iterNext(array, iter);
  if (!iter->node || (iter->element->rendered && iter->node->rendered))
    return;

  /* From the current node, we go next to find one that is rendred. */
  for (; iter->element; visu_node_array_iterNextElement(array, iter))
    if (iter->element->rendered)
      for (; iter->node; visu_node_array_iterNextNode(array, iter))
	if (iter->node->rendered)
	  return;
}

/**
 * visu_node_array_iterNextNode:
 * @array: a #VisuNodeArray object ;
 * @iter: a #VisuNodeArrayIter object.
 *
 * Modify node internal pointer to the next node, or NULL if
 * none remains. Contrary to visu_node_array_iterNext() it does not go to the
 * next element if one exists.
 */
void visu_node_array_iterNextNode(VisuNodeArray *array, VisuNodeArrayIter *iter)
{
  EleArr *ele;

  g_return_if_fail(VISU_IS_NODE_ARRAY(array) && iter && array == iter->array);
  g_return_if_fail(iter->init && iter->node);

  ele = _getEleArr(array, iter->node->posElement);
  if (iter->node->posNode + 1 < ele->nStoredNodes)
    iter->node = iter->node + 1;
  else
    iter->node = (VisuNode*)0;
}
/**
 * visu_node_array_iterNextNodeOriginal:
 * @array: a #VisuNodeArray object ;
 * @iter: a #VisuNodeArrayIter object.
 *
 * Modify node internal pointer to the next original node, or NULL if
 * none remains. Contrary to visu_node_array_iterNext() it does not go to the
 * next element if one exists.
 *
 * Since: 3.6
 */
void visu_node_array_iterNextNodeOriginal(VisuNodeArray *array, VisuNodeArrayIter *iter)
{
  EleArr *ele;

  g_return_if_fail(VISU_IS_NODE_ARRAY(array) && iter && array == iter->array);
  g_return_if_fail(iter->init && iter->node);

  do
    {
      ele = _getEleArr(array, iter->node->posElement);
      if (iter->node->posNode + 1 < ele->nStoredNodes)
	iter->node = iter->node + 1;
      else
	iter->node = (VisuNode*)0;
    }
  while (iter->node && visu_node_array_getOriginal(array, iter->node->number) >= 0);
}

/**
 * visu_node_array_iterNextNodeNumber:
 * @array: a #VisuNodeArray object ;
 * @iter: a #VisuNodeArrayIter object.
 *
 * Modify node internal pointer to the next node, increasing the id of
 * the current node. The element internal pointer is also updated
 * accordingly. If no more nodes exist after the given one, node and
 * element internal pointers are set to NULL.
 */
void visu_node_array_iterNextNodeNumber(VisuNodeArray *array, VisuNodeArrayIter *iter)
{
  guint i;
  EleArr *ele;

  g_return_if_fail(VISU_IS_NODE_ARRAY(array) && iter && array == iter->array);
  g_return_if_fail(iter->init && iter->node);

  for (i = iter->node->number + 1;
       !(iter->node = visu_node_array_getFromId(VISU_NODE_ARRAY(array), i)) &&
	 (i < array->priv->nodeTable.idCounter) ; i++);

  if (iter->node)
    {
      ele = _getEleArr(array, iter->node->posElement);
      iter->iElement = iter->node->posElement;
      iter->element  = ele->ele;
      iter->nStoredNodes = ele->nStoredNodes;
    }
  else
    {
      iter->element = (VisuElement*)0;
      iter->nStoredNodes = 0;
    }
}

/**
 * visu_node_array_iterNextElement:
 * @array: a #VisuNodeArray object ;
 * @iter: a #VisuNodeArrayIter object.
 *
 * Modify element internal pointer to the next element and set node
 * to the first one, or NULL if none remains.
 */
void visu_node_array_iterNextElement(VisuNodeArray *array, VisuNodeArrayIter *iter)
{
  EleArr *ele;

  g_return_if_fail(VISU_IS_NODE_ARRAY(array) && iter && array == iter->array);
  g_return_if_fail(iter->init && iter->iElement < array->priv->elements->len);

  do
    iter->iElement += 1;
  while(iter->iElement < array->priv->elements->len &&
	_getEleArr(array, iter->iElement)->nStoredNodes == 0);

  if (iter->iElement == array->priv->elements->len)
    {
      iter->iElement = -1;
      iter->node     = (VisuNode*)0;
      iter->element  = (VisuElement*)0;
      iter->nStoredNodes = 0;
    }
  else
    {
      ele = _getEleArr(array, iter->iElement);
      iter->node     = ele->nodes;
      iter->element  = ele->ele;
      iter->nStoredNodes = ele->nStoredNodes;
    }
}


/*************************************/
/* Additionnal routines for bindings */
/*************************************/
/**
 * visu_node_array_iter_next:
 * @iter: a #VisuNodeArrayIter object.
 *
 * Run the iterator to go to next item.
 *
 * Since: 3.6
 * 
 * Returns: TRUE if any item is found, FALSE otherwise.
 */
gboolean visu_node_array_iter_next(VisuNodeArrayIter *iter)
{
  if (!iter->init)
    switch (iter->type)
      {
      case ITER_NODES_BY_TYPE:
      case ITER_ELEMENTS:
	visu_node_array_iterStart(iter->array, iter);
	break;
      case ITER_NODES_BY_NUMBER:
      case ITER_NODES_ORIGINAL:
	visu_node_array_iterStartNumber(iter->array, iter);
	break;
      case ITER_NODES_VISIBLE:
	visu_node_array_iterStartVisible(iter->array, iter);
	break;
      }
  else
    switch (iter->type)
      {
      case ITER_NODES_BY_TYPE:
	visu_node_array_iterNext(iter->array, iter);
	break;
      case ITER_NODES_BY_NUMBER:
	visu_node_array_iterNextNodeNumber(iter->array, iter);
	break;
      case ITER_NODES_VISIBLE:
	visu_node_array_iterNextVisible(iter->array, iter);
	break;
      case ITER_NODES_ORIGINAL:
	visu_node_array_iterNextNodeOriginal(iter->array, iter);
	break;
      case ITER_ELEMENTS:
	visu_node_array_iterNextElement(iter->array, iter);
	break;
      }
  
  if (iter->node)
    return TRUE;
  else
    return FALSE;
}
/**
 * visu_node_array_iter_next2:
 * @iter1: a #VisuNodeArrayIter object.
 * @iter2: a #VisuNodeArrayIter object.
 *
 * Iterator to run on a pair of different nodes.
 *
 * Returns: TRUE if any item is found, FALSE otherwise.
 *
 * Since: 3.6
 */
gboolean visu_node_array_iter_next2(VisuNodeArrayIter *iter1, VisuNodeArrayIter *iter2)
{
  if (!iter1->init)
    {
      visu_node_array_iterStart(iter1->array, iter1);
      visu_node_array_iterStart(iter1->array, iter2);
    }
  else
    {
      if (!iter1->node)
        return FALSE;

      /* DBG_fprintf(stderr, "go next %p-%p ->", (gpointer)iter1->node, (gpointer)iter2->node); */
      visu_node_array_iterNext(iter1->array, iter2);
      if (!iter2->node ||
	  iter2->node->posElement > iter1->node->posElement ||
	  (iter2->node->posElement == iter1->node->posElement &&
	   iter2->node->posNode    >= iter1->node->posNode))
	{
	  visu_node_array_iterNext(iter1->array, iter1);
	  if (iter1->node)
	    visu_node_array_iterStart(iter1->array, iter2);
	  else
	    iter2->node = (VisuNode*)0;
	}
      /* DBG_fprintf(stderr, " %p-%p\n", (gpointer)iter1->node, (gpointer)iter2->node); */
    }

  if (!iter1->node && !iter2->node)
    return FALSE;
  else
    return TRUE;
}
