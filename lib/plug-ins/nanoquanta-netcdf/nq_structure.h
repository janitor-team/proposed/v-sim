#ifndef NQ_STRUCTURE_H
#define NQ_STRUCTURE_H

#include <visu_rendering.h>

/**
 * nqStructuralInit:
 * 
 * Routine used to create a new loading method for the ETSF file
 * format.
 *
 * Returns: a newly created rendering method.
 */
void nqStructuralInit(VisuRendering *method);

#endif
