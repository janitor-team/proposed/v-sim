/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include <glib/gstdio.h>
#include <archive.h>
#include <archive_entry.h>

#include <visu_tools.h>
#include <visu_basic.h>
#include <renderingMethods/renderingAtomic.h>
#include <renderingMethods/renderingSpin.h>


#define ARCHIVES_DESCRIPTION _("<span size=\"smaller\">" \
                               "This plug-in adds support for\n" \
                               "compressed input files\n" \
                               "(see <span color=\"blue\"><u>http://code.google.com/p/libarchive/</u></span>).</span>")
#define ARCHIVES_AUTHORS     "Caliste Damien"

/* Local methods */
static void archivesStructuralInit(VisuRendering *method);
static gboolean loadArchivesIn(VisuData *data, const gchar* filename,
                               ToolFileFormat *format, int nSet,
                               GCancellable *cancel, GError **error);
/* Local variables */
static gchar *iconPath;

/* Required methods for a loadable module. */
gboolean archivesInit()
{
  DBG_fprintf(stderr, "Archives: loading plug-in 'archives'...\n");

  iconPath = g_build_filename(V_SIM_PIXMAPS_DIR, "archives.png", NULL);

  DBG_fprintf(stderr, "Archives: declare a new rendering load method.\n");
  archivesStructuralInit(visu_rendering_getByName(VISU_RENDERING_ATOMIC_NAME));

  return TRUE;
}

const char* archivesGet_description()
{
  return ARCHIVES_DESCRIPTION;
}

const char* archivesGet_authors()
{
  return ARCHIVES_AUTHORS;
}

const char* archivesGet_icon()
{
  return iconPath;
}

static void archivesStructuralInit(VisuRendering *method)
{
  const gchar *type[] = {"*.gz", "*.bz2", "*.zip", "*.iso", (char*)0};
  
  visu_rendering_addFileFormat(method, 0,
                               tool_file_format_new(_("Compressed file formats"),
                                                    type),
                               25, loadArchivesIn);
}

static gboolean loadArchivesIn(VisuData *data, const gchar* filename,
                               ToolFileFormat *format _U_, int nSet,
                               GCancellable *cancel, GError **error)
{
  struct archive *a;
  struct archive_entry *entry;
  int r;
  guint nSets;
  GList *lst, *tmpLst;
  const void *buff;
  size_t size, wsize;
  off_t offset;
  GIOChannel *tmpFile;
  gchar *tmpFilename, *tmpBasename;
  gboolean res;

  a = archive_read_new();
  archive_read_support_compression_all(a);
  archive_read_support_format_all(a);
  DBG_fprintf(stderr, "Archives: try to open file with libarchive.\n");
  r = archive_read_open_filename(a, filename, 10240);
  DBG_fprintf(stderr, " | %d %d\n", r, ARCHIVE_OK);
  if (r != ARCHIVE_OK)
    {
      if (archive_read_finish(a))
        g_warning("Can't finish the archive file '%s'.", filename);
      return FALSE;
    }

  nSets = 0;
  lst = (GList*)0;
  while (archive_read_next_header(a, &entry) == ARCHIVE_OK)
    if (archive_entry_size(entry) > 0)
      {
        lst = g_list_prepend(lst, g_strdup(archive_entry_pathname(entry)));
        DBG_fprintf(stderr, " | entry '%s' size %ld in '%s'.\n",
                    archive_entry_pathname(entry), archive_entry_size(entry),
                    filename);
        if (nSets == (guint)nSet)
          {
            /* We create a temp file a extract the data to it. */
            tmpBasename = g_path_get_basename(archive_entry_pathname(entry));
            tmpFilename = g_build_filename(g_get_tmp_dir(), tmpBasename, NULL);
            g_free(tmpBasename);
            DBG_fprintf(stderr, "Archives: create a temporary file '%s' for entry %d.\n",
                        tmpFilename, nSet);
            tmpFile = g_io_channel_new_file(tmpFilename, "w", error);
            if (!tmpFile)
              {
                g_free(tmpFilename);
                if (archive_read_finish(a))
                  g_warning("Can't finish the archive file '%s'.", filename);
                return TRUE;
              }            
            while (archive_read_data_block(a, &buff, &size, &offset) == ARCHIVE_OK)
              {
                if (g_io_channel_write_chars(tmpFile, buff, size, &wsize, error) !=
                    G_IO_STATUS_NORMAL || wsize != size)
                  {
                    g_free(tmpFilename);
                    if (archive_read_finish(a))
                      g_warning("Can't finish the archive file '%s'.", filename);
                    return TRUE;
                  }
              }
            if (g_io_channel_shutdown(tmpFile, TRUE, error) != G_IO_STATUS_NORMAL)
              {
                g_free(tmpFilename);
                if (archive_read_finish(a))
                  g_warning("Can't finish the archive file '%s'.", filename);
                return TRUE;
              }
            /* We try to load the file. */
            visu_data_addFile(data, tmpFilename, 0, (ToolFileFormat*)0);
            res = visu_rendering_load(visu_rendering_getByName(VISU_RENDERING_ATOMIC_NAME),
                                      data, 0, cancel, error);
            /* We erase the file after use. */
            g_unlink(tmpFilename);
            g_free(tmpFilename);
            if (!res)
              {
                if (archive_read_finish(a))
                  g_warning("Can't finish the archive file '%s'.", filename);
                return TRUE;
              }
          }
        nSets += 1;
      }
  if (nSets == 0)
    {
      if (archive_read_finish(a))
        g_warning("Can't finish the archive file '%s'.", filename);
      return FALSE;
    }

  visu_data_addFile(data, filename, 0, (ToolFileFormat*)0);
  visu_data_setNSubset(data, nSets);
  for (tmpLst = lst; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      nSets -= 1;
      visu_data_setFileCommentary(data, (gchar*)tmpLst->data, nSets);
      g_free(tmpLst->data);
    }
  g_list_free(lst);

  if (archive_read_finish(a))
    g_warning("Can't finish the archive file '%s'.", filename);
  return TRUE;
}
