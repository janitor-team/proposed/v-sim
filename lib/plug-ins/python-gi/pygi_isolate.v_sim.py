#!/usr/bin/env python
from gi.repository import v_sim
import cProfile

render = v_sim.uiMainClass_getDefaultRendering()
data = render.getVisuData()

def prox(data, (x0, y0, z0), c, rad):
  (u, v, w) = data.convertXYZToReduced(c)
  for dx in range(-1, 2):
    for dy in range(-1, 2):
      for dz in range(-1, 2):
        (x, y, z) = data.convertReducedToXYZ((u + dx, v + dy, w + dz))
	dist2 = (x - x0)**2 + (y - y0)**2 + (z - z0)**2
	if dist2 <= (rad) ** 2:
	  return True
  return False

def isolate(RAD, NODE):
  coord0 = data.getNodeCoordinates(data.getNodeFromNumber(NODE))
  dataIter = data.iterNew()
  data.iterStart(dataIter)
  while (dataIter.node is not None):
    if prox(data, coord0, data.getNodeCoordinates(dataIter.node), RAD):
      print "Node ",dataIter.node.number," is visible"
      dataIter.node.setVisibility(True)
    else:
      print "Node ",dataIter.node.number," is hidden"
      dataIter.node.setVisibility(False)
    data.iterNext(dataIter)
  data.createAllNodes()


##RAD = input('Choose a radius: ')
##NODE = input('Choose a node id: ')
RAD = 5.
NODE = 21
print "All nodes further than ",RAD," from node ",NODE," will be hidden."
cProfile.run('isolate(RAD, NODE)')
v_sim.object_redraw(None)

#----------------------DUMP----------------------#
outFilename = "diffOut.png"
print '\n\t---DUMP dans du resultat dans %s---\n' % outFilename

##outFilename = "diffOut.ascii"

dumps = v_sim.dump_getAllModules()
for dump in dumps:
  if dump.fileType.match(outFilename):
    render.dump(dump, outFilename, 450, 450, None, None)
