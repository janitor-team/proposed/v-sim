dnl example of use
dnl AC_CHECK_ETSF_IO(
dnl   [
dnl       LIBS="$LIBS $ETSF_IO_LIBS"
dnl       LDFLAGS="$LDFLAGS $ETSF_IO_LDFLAGS"
dnl       CPPFLAGS="$CPPFLAGS $ETSF_IO_CPPFLAGS"
dnl   ],
dnl   [
dnl       echo "*** Use --with-etsf-io for the root etsf_io directory."
dnl       echo "*** Otherwise use --with-etsf-io-include switch for includes directory"
dnl       echo "*** and --with-etsf-io-libdir switch for libraries directory."
dnl       AC_MSG_ERROR([etsf_io library and etsf_io headers are required.])
dnl   ]
dnl )

# Check for the etsf_io library.
# AC_CHECK_ETSF_IO([ACTION-IF-FOUND],[ACTION-IF-NOT-FOUND],[INTERFACE-NR])
# if interface number is given, check for a specific interface
# sets ETSF_IO_LDFLAGS, ETSF_IO_LIBS, and, by calling other macros
# ETSF_IO_CPPFLAGS and maybe ETSF_IO_ETSF_IO_3_CPPFLAG
AC_DEFUN([AC_CHECK_ETSF_IO],
[
  AC_REQUIRE([AC_CHECK_NETCDF])

  AC_ARG_WITH([etsf_io],
            [AS_HELP_STRING([--with-etsf-io=ARG],[etsf_io directory])],
            [ETSF_IO_PATH=$withval], 
            [ETSF_IO_PATH=""])

  AC_ARG_WITH([etsf_io_include],
            [AS_HELP_STRING([--with-etsf-io-include=ARG],[etsf_io include directory])],
            [ETSF_IO_PATH_INC=$withval], 
            [ETSF_IO_PATH_INC=""])

  AC_ARG_WITH([etsf_io_libdir],
            [AS_HELP_STRING([--with-etsf-io-libdir=ARG],[etsf_io library directory])],
            [ETSF_IO_PATH_LIBDIR=$withval], 
            [ETSF_IO_PATH_LIBDIR=""])

  AS_IF([test "z$ETSF_IO_PATH" != "z"],[
    AS_IF([test "z$ETSF_IO_PATH_LIBDIR" = "z"],[ETSF_IO_PATH_LIBDIR="$ETSF_IO_PATH/lib"])
    AS_IF([test "z$ETSF_IO_PATH_INC" = "z"],[ETSF_IO_PATH_INC="$ETSF_IO_PATH/include"])
  ])

  ac_etsf_io_ok='no'
  ETSF_IO_LIBS=

  AC_LANG_PUSH(Fortran)
  AC_FC_SRCEXT(f90)

  LDFLAGS_SVG="$LDFLAGS"
  LIBS_SVG="$LIBS"
  FCFLAGS_SVG="$FCFLAGS"

  AS_IF([test -n "$ETSF_IO_PATH_LIBDIR"], [LDFLAGS="$LDFLAGS -L$ETSF_IO_PATH_LIBDIR"])
  AS_IF([test -n "$ETSF_IO_PATH_INC"], [FCFLAGS="$FCFLAGS -I$ETSF_IO_PATH_INC"])
  AS_IF([test -n "$NC_LDFLAGS"], [LDFLAGS="$LDFLAGS $NC_LDFLAGS"])
  AS_IF([test -n "$NC_CPPFLAGS"], [FCFLAGS="$FCFLAGS $NC_CPPFLAGS"])

  LIBS="$LIBS -letsf_io -lnetcdff $NC_LIBS"
   AC_MSG_CHECKING([for ETSF_IO library])
   AC_LINK_IFELSE([[
program main
  use etsf_io
  
  type(etsf_groups_flags) :: groups
  type(etsf_dims) :: dims
  logical :: lstat
  type(etsf_io_low_error) :: error_data

  call etsf_io_data_init("test", groups, dims, "test", "", lstat, error_data)
end]], ac_etsf_io_ok=yes, ac_etsf_io_ok=no)
   AC_MSG_RESULT([$ac_etsf_io_ok])
   LIBS="$LIBS_SVG"
   LDFLAGS="$LDFLAGS_SVG"
   FCFLAGS="$FCFLAGS_SVG"
   if test "$ac_etsf_io_ok" = "yes"; then
      ETSF_IO_LIBS="-letsf_io_utils -letsf_io -lnetcdff $NC_LIBS"
      AS_IF([test -n "$ETSF_IO_PATH_LIBDIR"], [ETSF_IO_LIBS="-L$ETSF_IO_PATH_LIBDIR $ETSF_IO_LIBS"])
      AC_SUBST([ETSF_IO_LIBS])
   fi

  AC_LANG_POP(Fortran)
])
