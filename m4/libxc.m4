dnl example of use
dnl AC_CHECK_LIBXC(
dnl   [
dnl       LIBS="$LIBS $LIBXC_LIBS"
dnl       LDFLAGS="$LDFLAGS $LIBXC_LDFLAGS"
dnl       CPPFLAGS="$CPPFLAGS $LIBXC_CPPFLAGS"
dnl   ],
dnl   [
dnl       echo "*** Use --with-libxc for the root libxc directory."
dnl       echo "*** Otherwise use --with-libxc-include switch for includes directory"
dnl       echo "*** and --with-libxc-libdir switch for libraries directory."
dnl       AC_MSG_ERROR([libxc library and libxc headers are required.])
dnl   ]
dnl )

# Check for the libxc library.
# AC_CHECK_LIBXC([ACTION-IF-FOUND],[ACTION-IF-NOT-FOUND],[INTERFACE-NR])
# if interface number is given, check for a specific interface
# sets LIBXC_LDFLAGS, LIBXC_LIBS, and, by calling other macros
# LIBXC_CPPFLAGS and maybe LIBXC_LIBXC_3_CPPFLAG
AC_DEFUN([AC_CHECK_LIBXC],
[
  AC_ARG_WITH([libxc],
            [AS_HELP_STRING([--with-libxc=ARG],[libxc directory])],
            [LIBXC_PATH=$withval], 
            [LIBXC_PATH=""])

  AC_ARG_WITH([libxc_include],
            [AS_HELP_STRING([--with-libxc-include=ARG],[libxc include directory])],
            [LIBXC_PATH_INC=$withval], 
            [LIBXC_PATH_INC=""])

  AC_ARG_WITH([libxc_libdir],
            [AS_HELP_STRING([--with-libxc-libdir=ARG],[libxc library directory])],
            [LIBXC_PATH_LIBDIR=$withval], 
            [LIBXC_PATH_LIBDIR=""])

  AS_IF([test "z$LIBXC_PATH" != "z"],[
    AS_IF([test "z$LIBXC_PATH_LIBDIR" = "z"],[LIBXC_PATH_LIBDIR="$LIBXC_PATH/lib"])
    AS_IF([test "z$LIBXC_PATH_INC" = "z"],[LIBXC_PATH_INC="$LIBXC_PATH/include"])
  ])

  ac_libxc_ok='no'
  LIBXC_LIBS=

  AC_LANG_PUSH(Fortran)
  AC_FC_SRCEXT(f90)

  LDFLAGS_SVG="$LDFLAGS"
  LIBS_SVG="$LIBS"
  FCFLAGS_SVG="$FCFLAGS"

  AS_IF([test -n "$LIBXC_PATH_LIBDIR"], [LDFLAGS="$LDFLAGS -L$LIBXC_PATH_LIBDIR"])
  AS_IF([test -n "$LIBXC_PATH_INC"], [FCFLAGS="$FCFLAGS -I$LIBXC_PATH_INC"])
  AS_IF([test -n "$NC_LDFLAGS"], [LDFLAGS="$LDFLAGS $NC_LDFLAGS"])
  AS_IF([test -n "$NC_CPPFLAGS"], [FCFLAGS="$FCFLAGS $NC_CPPFLAGS"])

  LIBS="$LIBS -lxc"
   AC_MSG_CHECKING([for LIBXC library])
   AC_LINK_IFELSE([[
program main

  call xc_f90_family_from_id()
end]], ac_libxc_ok=yes, ac_libxc_ok=no)
   AC_MSG_RESULT([$ac_libxc_ok])
   LIBS="$LIBS_SVG"
   LDFLAGS="$LDFLAGS_SVG"
   FCFLAGS="$FCFLAGS_SVG"
   if test "$ac_libxc_ok" = "yes"; then
      LIBXC_LIBS="-lxc"
      AS_IF([test -n "$LIBXC_PATH_LIBDIR"], [LIBXC_LIBS="-L$LIBXC_PATH_LIBDIR $LIBXC_LIBS"])
      AC_SUBST([LIBXC_LIBS])
   fi

  AC_LANG_POP(Fortran)
])
